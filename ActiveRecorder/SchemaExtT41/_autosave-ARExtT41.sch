EESchema Schematic File Version 5
EELAYER 43 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Connection ~ 4025 2750
Connection ~ 4300 4225
Connection ~ 4300 4725
Connection ~ 4500 1450
Connection ~ 4500 2750
Connection ~ 4500 3375
Connection ~ 4975 3375
Connection ~ 5275 3825
Connection ~ 7900 3600
Connection ~ 8250 3100
Connection ~ 8250 3600
Connection ~ 8425 3600
Connection ~ 8875 3600
NoConn ~ 3850 3950
NoConn ~ 4725 3650
Wire Wire Line
	2325 1450 4500 1450
Wire Wire Line
	2325 1950 4025 1950
Wire Wire Line
	2325 4725 4300 4725
Wire Wire Line
	2325 5250 4600 5250
Wire Wire Line
	2325 5475 4600 5475
Wire Wire Line
	2350 3850 3850 3850
Wire Wire Line
	2350 4100 2775 4100
Wire Wire Line
	2775 4100 2775 4350
Wire Wire Line
	2775 4350 5425 4350
Wire Wire Line
	3750 3600 2350 3600
Wire Wire Line
	3750 3750 3750 3600
Wire Wire Line
	3750 3750 3850 3750
Wire Wire Line
	3825 3350 2350 3350
Wire Wire Line
	3825 3650 3825 3350
Wire Wire Line
	3825 3650 3850 3650
Wire Wire Line
	4025 1950 4025 2750
Wire Wire Line
	4025 2750 4025 3025
Wire Wire Line
	4025 3025 4200 3025
Wire Wire Line
	4100 2750 4025 2750
Wire Wire Line
	4300 3375 4300 3450
Wire Wire Line
	4300 4150 4300 4225
Wire Wire Line
	4300 4225 5500 4225
Wire Wire Line
	4300 4725 4300 4225
Wire Wire Line
	4300 4725 4600 4725
Wire Wire Line
	4400 2750 4500 2750
Wire Wire Line
	4500 1450 4500 2750
Wire Wire Line
	4500 1450 5650 1450
Wire Wire Line
	4500 2750 4500 2825
Wire Wire Line
	4500 2750 4675 2750
Wire Wire Line
	4500 3225 4500 3375
Wire Wire Line
	4500 3375 4300 3375
Wire Wire Line
	4600 5000 2325 5000
Wire Wire Line
	4975 3375 4500 3375
Wire Wire Line
	4975 3450 4975 3375
Wire Wire Line
	4975 3750 4975 3825
Wire Wire Line
	5275 3375 4975 3375
Wire Wire Line
	5275 3450 5275 3375
Wire Wire Line
	5275 3750 5275 3825
Wire Wire Line
	5275 3825 4975 3825
Wire Wire Line
	5275 3825 5500 3825
Wire Wire Line
	5425 3950 4725 3950
Wire Wire Line
	5425 4350 5425 3950
Wire Wire Line
	5500 3825 5500 4225
Wire Wire Line
	7550 3100 7600 3100
Wire Wire Line
	7550 3600 7900 3600
Wire Wire Line
	7900 3600 7900 3400
Wire Wire Line
	8200 3100 8250 3100
Wire Wire Line
	8250 3100 8600 3100
Wire Wire Line
	8250 3250 8250 3100
Wire Wire Line
	8250 3550 8250 3600
Wire Wire Line
	8250 3600 7900 3600
Wire Wire Line
	8425 3200 8425 3600
Wire Wire Line
	8425 3200 8600 3200
Wire Wire Line
	8425 3600 8250 3600
Wire Wire Line
	8500 3300 8600 3300
Wire Wire Line
	8500 3450 8500 3300
Wire Wire Line
	8525 3600 8425 3600
Wire Wire Line
	8825 3600 8875 3600
Wire Wire Line
	8875 3450 8500 3450
Wire Wire Line
	8875 3600 8875 3450
Wire Wire Line
	8875 3600 8950 3600
Wire Wire Line
	9225 3300 9375 3300
Wire Wire Line
	9375 3100 9225 3100
Wire Wire Line
	9375 3600 9250 3600
Text Notes 2325 3350 0    60   ~ 0
CLOCK 48-T4.1
Text Notes 2325 3600 0    60   ~ 0
WordSynch 47-T4.1
Text Notes 2325 3825 0    60   ~ 0
Data 9-T4.1
Text Notes 2325 4100 0    60   ~ 0
Sortie Audio
Text Notes 7100 7075 0    157  ~ 31
Active Recorder V0.3, V0.4, V0.5\nExtension Teensy 4.1
Text Label 2300 5475 0    60   ~ 0
VBAT
Text Label 2325 4725 0    60   ~ 0
GND 39-T4.1
Text Label 2325 5000 0    60   ~ 0
SDA 45-T4.1
Text Label 2325 5250 0    60   ~ 0
SCL
Text Label 2400 1450 0    60   ~ 0
+3.3V
Text Label 2400 1950 0    60   ~ 0
CmdAudio
Text Label 4725 3950 0    60   ~ 0
Sortie_Audio
Text GLabel 4600 4725 2    60   Input ~ 0
GND
Text GLabel 4600 5000 2    60   Input ~ 0
SDA
Text GLabel 4600 5250 2    60   Input ~ 0
SCL
Text GLabel 4600 5475 2    60   Input ~ 0
VBAT
Text GLabel 4675 2750 2    60   Input ~ 0
+3V3
Text GLabel 5650 1450 2    60   Input ~ 0
+3V3
Text GLabel 7550 3100 0    60   Input ~ 0
+3V3
Text GLabel 7550 3600 0    60   Input ~ 0
GND
Text GLabel 9375 3100 2    60   Input ~ 0
SCL
Text GLabel 9375 3300 2    60   Input ~ 0
SDA
Text GLabel 9375 3600 2    60   Input ~ 0
VBAT
$Comp
L Device:R R1
U 1 1 62EEAFDD
P 4250 2750
F 0 "R1" V 4175 2750 50  0000 C CNN
F 1 "20k" V 4250 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4180 2750 50  0001 C CNN
F 3 "" H 4250 2750 50  0001 C CNN
	1    4250 2750
	0    -1   1    0   
$EndComp
$Comp
L Device:R R29
U 1 1 00000000
P 8675 3600
AR Path="/00000000" Ref="R29"  Part="1" 
AR Path="/00000000" Ref="R2"  Part="1" 
F 0 "R2" V 8755 3600 50  0000 C CNN
F 1 "100k" V 8675 3600 50  0000 C CNN
F 2 "Teensy Recorder:R_0805_2012_HandSolder" V 8605 3600 50  0001 C CNN
F 3 "" H 8675 3600 50  0001 C CNN
	1    8675 3600
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R30
U 1 1 00000000
P 9100 3600
AR Path="/00000000" Ref="R30"  Part="1" 
AR Path="/00000000" Ref="R3"  Part="1" 
F 0 "R3" V 9180 3600 50  0000 C CNN
F 1 "100k" V 9100 3600 50  0000 C CNN
F 2 "Teensy Recorder:R_0805_2012_HandSolder" V 9030 3600 50  0001 C CNN
F 3 "" H 9100 3600 50  0001 C CNN
	1    9100 3600
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J2
U 1 1 63727B7C
P 2125 1450
F 0 "J2" H 2275 1450 50  0000 R CNN
F 1 "+BAT-A" V 1998 1362 50  0001 R CNN
F 2 "Alimentations:ConX1SMD" H 2125 1450 50  0001 C CNN
F 3 "~" H 2125 1450 50  0001 C CNN
	1    2125 1450
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J3
U 1 1 00000000
P 2125 1950
F 0 "J3" H 2225 1950 50  0000 C CNN
F 1 "Conn_01x01" H 2125 1832 50  0001 C CNN
F 2 "" H 2125 1950 50  0001 C CNN
F 3 "~" H 2125 1950 50  0001 C CNN
	1    2125 1950
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J7
U 1 1 00000000
P 2125 4725
F 0 "J7" H 2225 4725 50  0000 C CNN
F 1 "Conn_01x01" H 2125 4607 50  0001 C CNN
F 2 "" H 2125 4725 50  0001 C CNN
F 3 "~" H 2125 4725 50  0001 C CNN
	1    2125 4725
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J8
U 1 1 00000000
P 2125 5000
F 0 "J8" H 2250 5000 50  0000 C CNN
F 1 "Conn_01x01" H 2125 4882 50  0001 C CNN
F 2 "" H 2125 5000 50  0001 C CNN
F 3 "~" H 2125 5000 50  0001 C CNN
	1    2125 5000
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J9
U 1 1 00000000
P 2125 5250
F 0 "J9" H 2250 5250 50  0000 C CNN
F 1 "Conn_01x01" H 2125 5132 50  0001 C CNN
F 2 "" H 2125 5250 50  0001 C CNN
F 3 "~" H 2125 5250 50  0001 C CNN
	1    2125 5250
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J10
U 1 1 00000000
P 2125 5475
F 0 "J10" H 2250 5475 50  0000 C CNN
F 1 "Conn_01x01" H 2125 5357 50  0001 C CNN
F 2 "" H 2125 5475 50  0001 C CNN
F 3 "~" H 2125 5475 50  0001 C CNN
	1    2125 5475
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J5
U 1 1 00000000
P 2150 3350
F 0 "J5" H 2250 3350 50  0000 C CNN
F 1 "Conn_01x01" H 2150 3232 50  0001 C CNN
F 2 "" H 2150 3350 50  0001 C CNN
F 3 "~" H 2150 3350 50  0001 C CNN
	1    2150 3350
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J4
U 1 1 00000000
P 2150 3600
F 0 "J4" H 2250 3600 50  0000 C CNN
F 1 "Conn_01x01" H 2150 3482 50  0001 C CNN
F 2 "" H 2150 3600 50  0001 C CNN
F 3 "~" H 2150 3600 50  0001 C CNN
	1    2150 3600
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J1
U 1 1 00000000
P 2150 3850
F 0 "J1" H 2300 3850 50  0000 R CNN
F 1 "+BAT-A" V 2023 3762 50  0001 R CNN
F 2 "Alimentations:ConX1SMD" H 2150 3850 50  0001 C CNN
F 3 "~" H 2150 3850 50  0001 C CNN
	1    2150 3850
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J6
U 1 1 00000000
P 2150 4100
F 0 "J6" H 2250 4100 50  0000 C CNN
F 1 "Conn_01x01" H 2150 3982 50  0001 C CNN
F 2 "" H 2150 4100 50  0001 C CNN
F 3 "~" H 2150 4100 50  0001 C CNN
	1    2150 4100
	-1   0    0    1   
$EndComp
$Comp
L Device:C C1
U 1 1 632011A8
P 4975 3600
F 0 "C1" H 5000 3700 50  0000 L CNN
F 1 "10µF" H 5000 3525 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5013 3450 50  0001 C CNN
F 3 "" H 4975 3600 50  0001 C CNN
	1    4975 3600
	-1   0    0    1   
$EndComp
$Comp
L Device:C C2
U 1 1 63201D39
P 5275 3600
F 0 "C2" H 5300 3700 50  0000 L CNN
F 1 "100nF" H 5275 3525 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5313 3450 50  0001 C CNN
F 3 "" H 5275 3600 50  0001 C CNN
	1    5275 3600
	-1   0    0    1   
$EndComp
$Comp
L Device:C C27
U 1 1 00000000
P 8250 3400
AR Path="/00000000" Ref="C27"  Part="1" 
AR Path="/00000000" Ref="C3"  Part="1" 
F 0 "C3" H 8350 3400 50  0000 L CNN
F 1 "100nF" H 8250 3500 50  0000 L CNN
F 2 "Teensy Recorder:C_0805_HandSoldering" H 8288 3250 50  0001 C CNN
F 3 "" H 8250 3400 50  0001 C CNN
	1    8250 3400
	-1   0    0    1   
$EndComp
$Comp
L Transistor_FET:AO3401A Q1
U 1 1 62EE8F72
P 4400 3025
F 0 "Q1" H 4625 3025 50  0000 C CNN
F 1 "AO3413" H 4675 3150 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4600 2950 50  0001 L CIN
F 3 "http://www.aosmd.com/pdfs/datasheet/AO3401A.pdf" H 4400 3025 50  0001 L CNN
	1    4400 3025
	1    0    0    1   
$EndComp
$Comp
L TeensyRecorder:MCP3221 U12
U 1 1 00000000
P 8925 3200
AR Path="/00000000" Ref="U12"  Part="1" 
AR Path="/00000000" Ref="U3"  Part="1" 
F 0 "U3" H 8975 2975 50  0000 C CNN
F 1 "MCP3221" H 8912 3449 50  0000 C CNN
F 2 "Teensy Recorder:SOT-23-5" H 8925 3475 50  0001 C CNN
F 3 "" H 8925 3475 50  0001 C CNN
	1    8925 3200
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LM1084-3.3 U11
U 1 1 00000000
P 7900 3100
AR Path="/00000000" Ref="U11"  Part="1" 
AR Path="/00000000" Ref="U2"  Part="1" 
F 0 "U2" H 7725 2850 50  0000 C CNN
F 1 "HT7130 LDO 2.8V" H 7600 3250 50  0000 L CNN
F 2 "Teensy Recorder:SOT-23" H 7900 3350 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm1084.pdf" H 7900 3100 50  0001 C CNN
	1    7900 3100
	1    0    0    -1  
$EndComp
$Comp
L ARExtT41-rescue:PT8211-Alimentations U1
U 1 1 62CDAA5A
P 4300 3750
F 0 "U1" H 4000 4000 50  0000 C CNN
F 1 "PT8211" H 4475 4000 50  0000 C CNN
F 2 "Alimentations:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 5100 3450 50  0001 C CNN
F 3 "" H 5100 3450 50  0001 C CNN
	1    4300 3750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
