EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7100 7075 0    157  ~ 31
Active Recorder V0.3, V0.4, V0.5\nExtension Teensy 4.1
Text Label 4725 3950 0    60   ~ 0
Sortie_Audio
$Comp
L Alimentations:PT8211 U1
U 1 1 62CDAA5A
P 4300 3750
F 0 "U1" H 4000 4000 50  0000 C CNN
F 1 "PT8211" H 4475 4000 50  0000 C CNN
F 2 "Alimentations:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 5100 3450 50  0001 C CNN
F 3 "" H 5100 3450 50  0001 C CNN
	1    4300 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3825 3650 3850 3650
Wire Wire Line
	3750 3750 3850 3750
NoConn ~ 3850 3950
Wire Wire Line
	5425 3950 4725 3950
Wire Wire Line
	3675 3850 3850 3850
Wire Wire Line
	4300 4150 4300 4225
NoConn ~ 4725 3650
$Comp
L Transistor_FET:AO3401A Q1
U 1 1 62EE8F72
P 4400 3025
F 0 "Q1" H 4625 3025 50  0000 C CNN
F 1 "AO3413" H 4675 3150 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4600 2950 50  0001 L CIN
F 3 "http://www.aosmd.com/pdfs/datasheet/AO3401A.pdf" H 4400 3025 50  0001 L CNN
	1    4400 3025
	1    0    0    1   
$EndComp
$Comp
L Device:R R1
U 1 1 62EEAFDD
P 4250 2750
F 0 "R1" V 4175 2750 50  0000 C CNN
F 1 "20k" V 4250 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4180 2750 50  0001 C CNN
F 3 "" H 4250 2750 50  0001 C CNN
	1    4250 2750
	0    -1   1    0   
$EndComp
Wire Wire Line
	4100 2750 4025 2750
Wire Wire Line
	4025 2750 4025 3025
Wire Wire Line
	4025 3025 4200 3025
$Comp
L Device:C C1
U 1 1 632011A8
P 4975 3600
F 0 "C1" H 5000 3700 50  0000 L CNN
F 1 "10µF" H 5000 3525 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5013 3450 50  0001 C CNN
F 3 "" H 4975 3600 50  0001 C CNN
	1    4975 3600
	-1   0    0    1   
$EndComp
$Comp
L Device:C C2
U 1 1 63201D39
P 5275 3600
F 0 "C2" H 5300 3700 50  0000 L CNN
F 1 "100nF" H 5275 3525 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5313 3450 50  0001 C CNN
F 3 "" H 5275 3600 50  0001 C CNN
	1    5275 3600
	-1   0    0    1   
$EndComp
Wire Wire Line
	4975 3750 4975 3825
Wire Wire Line
	5275 3750 5275 3825
Wire Wire Line
	5275 3825 4975 3825
Wire Wire Line
	5275 3450 5275 3375
Wire Wire Line
	5275 3375 4975 3375
Wire Wire Line
	4300 3375 4300 3450
Wire Wire Line
	4975 3450 4975 3375
Connection ~ 4975 3375
Wire Wire Line
	4975 3375 4500 3375
Wire Wire Line
	4300 4225 5500 4225
Wire Wire Line
	5275 3825 5500 3825
Connection ~ 5275 3825
Wire Wire Line
	5500 3825 5500 4225
Wire Wire Line
	4400 2750 4500 2750
Wire Wire Line
	4500 2750 4500 2825
Text GLabel 4675 2750 2    60   Input ~ 0
+3V3
Wire Wire Line
	4500 2750 4675 2750
Connection ~ 4500 2750
Wire Wire Line
	4500 3225 4500 3375
Connection ~ 4500 3375
Wire Wire Line
	4500 3375 4300 3375
NoConn ~ 5650 1300
NoConn ~ 5650 1800
NoConn ~ 6550 1800
Wire Wire Line
	6550 1475 6900 1475
Wire Wire Line
	6550 1650 6750 1650
Wire Wire Line
	6550 1300 6725 1300
$Comp
L Alimentations:PCB_MAX17043 U2
U 1 1 63055DEA
P 6100 1550
F 0 "U2" H 6100 2087 60  0000 C CNN
F 1 "PCB_MAX17043" H 6100 1981 60  0000 C CNN
F 2 "Alimentations:ModuleMAX" H 5950 1700 60  0001 C CNN
F 3 "" H 5950 1700 60  0001 C CNN
	1    6100 1550
	1    0    0    -1  
$EndComp
Text Label 2400 1450 0    60   ~ 0
+3.3V
Text Label 2400 1550 0    60   ~ 0
GND
Text Label 2400 1650 0    60   ~ 0
+BAT
Text Label 2400 1750 0    60   ~ 0
SDA
Text Label 2400 1850 0    60   ~ 0
SCL
Text Label 2400 1950 0    60   ~ 0
CmdAudio
Text Label 2400 2050 0    60   ~ 0
Clock
Text Label 2400 2150 0    60   ~ 0
WordSynch
Text Label 2400 2250 0    60   ~ 0
Data
Wire Wire Line
	2400 2250 3675 2250
Wire Wire Line
	3675 2250 3675 3850
Wire Wire Line
	3750 3750 3750 2150
Wire Wire Line
	3750 2150 2400 2150
Wire Wire Line
	3825 3650 3825 2050
Wire Wire Line
	3825 2050 2400 2050
$Comp
L Connector_Generic:Conn_01x10 J1
U 1 1 635F14E5
P 2200 1950
F 0 "J1" H 2118 1225 50  0000 C CNN
F 1 "Conn_01x10" H 2118 1316 50  0000 C CNN
F 2 "Alimentations:ConX10SMD" H 2200 1950 50  0001 C CNN
F 3 "~" H 2200 1950 50  0001 C CNN
	1    2200 1950
	-1   0    0    1   
$EndComp
Wire Wire Line
	2400 1450 4500 1450
Text Label 2400 2350 0    60   ~ 0
AudioOut
Wire Wire Line
	5425 2350 2400 2350
Wire Wire Line
	2400 1950 4025 1950
Wire Wire Line
	4025 1950 4025 2750
Connection ~ 4025 2750
Wire Wire Line
	4500 1450 5650 1450
Connection ~ 4500 1450
Wire Wire Line
	4500 1450 4500 2750
Wire Wire Line
	2400 1550 5375 1550
Wire Wire Line
	5375 1550 5375 1625
Wire Wire Line
	2400 1650 3200 1650
Wire Wire Line
	4275 1650 4275 925 
Wire Wire Line
	4275 925  6725 925 
Wire Wire Line
	6725 925  6725 1300
Wire Wire Line
	2400 1850 4800 1850
Wire Wire Line
	4800 1850 4800 2125
Wire Wire Line
	4800 2125 6750 2125
Wire Wire Line
	6750 1650 6750 2125
Wire Wire Line
	2400 1750 4925 1750
Wire Wire Line
	4925 1750 4925 2025
Wire Wire Line
	4925 2025 6900 2025
Wire Wire Line
	6900 1475 6900 2025
Wire Wire Line
	5500 3825 5500 1625
Wire Wire Line
	5375 1625 5500 1625
Connection ~ 5500 3825
Connection ~ 5500 1625
Wire Wire Line
	5500 1625 5650 1625
Wire Wire Line
	5425 2350 5425 3950
$Comp
L Connector_Generic:Conn_01x01 J2
U 1 1 63727B7C
P 3200 850
F 0 "J2" V 3164 762 50  0000 R CNN
F 1 "+BAT-A" V 3073 762 50  0000 R CNN
F 2 "Alimentations:ConX1SMD" H 3200 850 50  0001 C CNN
F 3 "~" H 3200 850 50  0001 C CNN
	1    3200 850 
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J3
U 1 1 6372819B
P 3750 850
F 0 "J3" V 3714 762 50  0000 R CNN
F 1 "+BAT-B" V 3623 762 50  0000 R CNN
F 2 "Alimentations:ConX1SMD" H 3750 850 50  0001 C CNN
F 3 "~" H 3750 850 50  0001 C CNN
	1    3750 850 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3200 1050 3200 1200
Connection ~ 3200 1650
Wire Wire Line
	3200 1650 4275 1650
Wire Wire Line
	3750 1050 3750 1200
Wire Wire Line
	3750 1200 3200 1200
Connection ~ 3200 1200
Wire Wire Line
	3200 1200 3200 1650
$EndSCHEMATC
