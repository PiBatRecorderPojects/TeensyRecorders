![Active Recorder prototype 01](https://git.framasoft.org/PiBatRecorderPojects/TeensyRecorders/raw/master/Photos/ActiveRecorder.JPG)

**English version below**

ActiveRecorder
==============

**Active Recorder** est un enregistreur actif permettant d'écouter en hétérodyne
et d'enregistrer des fichiers wav jusqu'à 500kHz de fréquence d'échantillonnage
(384kHz en standard). La fonction hétérodyne fonctionne en manuel (comme un
hétérodyne classique) ou en automatique (réglage automatique en fonction de la
FME détectée et ajustement manuel +/-10kHz). Une fonction d'écoute en différé
permet d'écouter les fichiers enregistrés en expansion de temps X10 ou en
hétérodyne manuel. Il propose aussi les mêmes fonctionnalités que Passive
Recorder (enregistrements en automatique, test micro, protocoles vigie chiro).

A mi 2021, environ 290 exemplaires fonctionnent. Ils utilisent le même logiciel
que Passive Recorder.

Le dossier de fabrication est complet avec une carte électronique pré-montée
qu'il est possible de commander chez JLCPCB. Cette solution permet de garder un
prix très bas (\<110€) et simplifie grandement le montage avec aucune soudure
délicate. Pour la boite, il est maintenant possible d'en commander par 30 au
minimum mais il est préférable de commander par 100 pour obtenir un prix
attractif.

Le document de départ pour la fabrication est Fabrication/FabricationAR.pdf.

 

ActiveRecorder
==============

**Active Recorder** is an active recorder for listening in heterodyne and
recording wav files up to 500kHz sample rate (384kHz as standard). The
heterodyne function works in manual (like a classic heterodyne) or in automatic
(automatic adjustment according to the FME detected and manual adjustment +/-
10kHz). A delayed listening function allows you to listen to the recorded files
in time expansion X10 or in manual heterodyne. It also offers the same features
as Passive Recorder (automatic recordings, micro test, protocols vigie chiro).

By mid-2021, around 290 units were in operation. They use the same software as
Passive Recorder.

The manufacturing file is complete with a pre-assembled electronic card that can
be ordered from JLCPCB. This solution makes it possible to keep a very low price
(\<110 €) and greatly simplifies the assembly with no delicate welding. For the
box, it is now possible to order a minimum of 30 but it is preferable to order
per 100 to obtain an attractive price.

The starting document for manufacturing is Fabrication/FabricationAR.pdf.

![PCB pré-montée](https://git.framasoft.org/PiBatRecorderPojects/TeensyRecorders/raw/master/Photos/PCB-AR.JPG)

![AR assemblé](https://git.framasoft.org/PiBatRecorderPojects/TeensyRecorders/raw/master/Photos/PCBCompletAR.JPG)
