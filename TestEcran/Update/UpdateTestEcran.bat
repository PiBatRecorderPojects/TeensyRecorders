Rem Command file to download a new version of the Test Ecran software
Rem After compilation, the TestEcran.ino.bin and TestEcran.ino.bootloader.bin and TestEcran.ino.partitions.bin files are to be recovered from (example):
Rem C:\Users\jeand\AppData\Local\Temp\arduino\sketches\55687ACBD9F9C5253FAE0C182119A546
Rem The best way to find the directory is to check "Compilation" to display the detailed results.
Rem
Rem Use :
Rem Connect the ESP32 with a USB cable to a PC
Rem Locate the port used via the device manager
Rem Via the "Execute" "cmd" command, open a command window
Rem On the command window:
Rem Make a command "cd directory containing the executable"
Rem Make a command "UpdateTestEcranV0-1.bat COM6" Adapt the command according to the port used and the version name
esptool.exe --chip esp32c3 --port %1 --baud 921600 --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 80m --flash_size 4MB 0x0 "%2.bootloader.bin" 0x8000 "%2.partitions.bin" 0xe000 "boot_app0.bin" 0x10000 "%2.bin"
