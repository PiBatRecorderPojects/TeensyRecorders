EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:micro_mems
LIBS:SchemaMicro-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_01x01 J1
U 1 1 5B044FB7
P 4700 1550
F 0 "J1" H 4700 1650 50  0001 C CNN
F 1 "o" H 4700 1450 50  0000 C CNN
F 2 "MicroMEMS:WirePad" H 4700 1550 50  0001 C CNN
F 3 "" H 4700 1550 50  0001 C CNN
	1    4700 1550
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x01 J2
U 1 1 5B0450A0
P 4700 2000
F 0 "J2" H 4700 2100 50  0001 C CNN
F 1 "+" H 4700 1900 50  0000 C CNN
F 2 "MicroMEMS:WirePad" H 4700 2000 50  0001 C CNN
F 3 "" H 4700 2000 50  0001 C CNN
	1    4700 2000
	1    0    0    -1  
$EndComp
$Comp
L MEMS-SPU0410LR5H U1
U 1 1 5B0450C7
P 3000 2000
F 0 "U1" H 3000 1650 60  0000 C CNN
F 1 "MEMS-SPU0410LR5H" H 2850 2500 60  0000 C CNN
F 2 "MicroMEMS:MEMS-SPU0410LR5H" H 2850 1950 60  0001 C CNN
F 3 "" H 2850 1950 60  0001 C CNN
	1    3000 2000
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x01 J3
U 1 1 5B0450F1
P 4700 2500
F 0 "J3" H 4700 2600 50  0001 C CNN
F 1 "G" H 4700 2400 50  0000 C CNN
F 2 "MicroMEMS:WirePad" H 4700 2500 50  0001 C CNN
F 3 "" H 4700 2500 50  0001 C CNN
	1    4700 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 1550 4500 1550
Wire Wire Line
	3750 2000 4500 2000
Wire Wire Line
	3850 2300 3750 2300
Wire Wire Line
	3850 2150 3750 2150
Connection ~ 3850 2300
Wire Wire Line
	3750 1850 3850 1850
Connection ~ 3850 2150
Wire Wire Line
	3750 1700 3850 1700
Connection ~ 3850 1850
$Comp
L C C1
U 1 1 5BDB1574
P 4150 2250
F 0 "C1" H 4175 2350 50  0000 L CNN
F 1 "100nF" H 4175 2150 50  0000 L CNN
F 2 "MicroMEMS:C_0805_HandSoldering" H 4188 2100 50  0001 C CNN
F 3 "" H 4150 2250 50  0001 C CNN
	1    4150 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 2000 4150 2100
Connection ~ 4150 2000
Wire Wire Line
	3850 2500 4500 2500
Wire Wire Line
	4150 2400 4150 2500
Connection ~ 4150 2500
Wire Wire Line
	3850 1700 3850 2800
$Comp
L Conn_01x01 J4
U 1 1 5BDB1CDE
P 4700 2800
F 0 "J4" H 4700 2900 50  0001 C CNN
F 1 "G" H 4700 2700 50  0000 C CNN
F 2 "MicroMEMS:WirePad" H 4700 2800 50  0001 C CNN
F 3 "" H 4700 2800 50  0001 C CNN
	1    4700 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 2800 4500 2800
Connection ~ 3850 2500
$EndSCHEMATC
