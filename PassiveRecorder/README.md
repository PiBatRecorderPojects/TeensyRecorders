![Passive Recorder prototype 01](https://git.framasoft.org/PiBatRecorderPojects/TeensyRecorders/raw/master/Photos/PassiveRecorder.JPG)
**English version below** 

PassiveRecorder
=
**Passive Recorder** est un enregistreur passif permettant d'enregistrer des fichiers wav jusqu'à 500kHz de fréquence d'échantillonnage (384kHz en standard). Le boitier est étanche et permet une utilisation sur plusieurs jours. Outre l'enregistrement automatique avec choix de la bande d'intérêt, du seuil de déclenchement et de la durée min des cris, il offre un test basique des micros et la gestion des protocoles routier et pédestre de Vigie-Chiro. Le prix est d'environ 100€ pour un atelier participatif de 10 appareils.

Ce dossier regroupe l'ensemble des éléments permettant de construire et/ou modifier Passive Recorder

État des lieux :
-

- A min 2020 environ 450 appareils en utilisation ([carte de répartition sur ce lien](https://framacarte.org/fr/map/distribution-of-teensy-recorders_50829)).
- Un nouveau dossier de fabrication V0.6 est disponible dans le répertoire "Fabrication-SMD". A base d'une carte pré-montée, la fabrication est facilitée. Le répertoire "Fabrication" de la version V0.3 reste disponible pour mémoire mais n'est pas conseillé. 
- Choix possible de 3 types de micros: MEMS SPU0410LR5H, MEMS ICS-40730 ou électret FG23329. La version 0.6 ne prend pas en compte le micro électret, c'est possible sur demande.
- Une sonde de température et hygrométrie en standard et coupure de l'alimentation du pré-ampli en veille pour moins consommer.
- Il est possible de réaliser un exemplaire seul dans son garage mais il est plus économique de se regrouper pour, en une journée, réaliser 10 exemplaires lors d'un atelier participatif. Il suffit d'un encadrant ayant de bonnes notions de montage électronique. **Le point d'entrée du tuto de fabrication est "Fabrication-SMD/FabricationPR-SMD.pdf"**.

Description des répertoires
-

- **Etude Technique** regroupe les documents de conception du projet à la racine, les documents de fabrication, manuel d'utilisation et chargement logiciel dans **Documents**, gestion des filtres numériques avec le logiciel **rePhase** (<https://sourceforge.net/projects/rephase/>) dans **FirFilter** et la gestion du logo dans **Logo** à l'aide du logiciel **LCDAssistant.exe** (<http://en.radzio.dxp.pl/bitmap_converter/>).
- **Fabrication-SMD** regroupe les documents de fabrication nécessaires à la réalisation d'un ou de plusieurs exemplaires chez soi ou lors d'un atelier participatif.
- **Schema-SMD-MEMS** est le schéma électronique et le circuit-imprimé de Passive Recorder V0.4 pour micro MEMS réalisés avec l'outil libre KiCad (<http://kicad-pcb.org/>).
- **Schema-SMD-FG** est le schéma électronique et le circuit-imprimé de Passive Recorder V0.4 pour micro FG réalisés avec l'outil libre KiCad.
- **ShemaMicroICS40730B** est le schéma électronique et le petit circuit-imprimé pour un micro MEMS ICS-40730 réalisés avec l'outil libre KiCad.
- **ShemaMicroSPU** est le schéma électronique et le petit circuit-imprimé pour un micro MEMS SPU0410LR5H réalisés avec l'outil libre KiCad.
- **Rappel**, le code source du logiciel est commun aux deux projets TeensyRecorders. Vous trouverez un cran plus haut dans TeensyRecorder le répertoire **Update** avec le manuel d'utilisation ([ManuelTR.pdf](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/blob/master/Update/ManuelTR.pdf)), les binaires à télécharger ainsi que le manuel expliquant comment les télécharger ([UpdateTR.pdf](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/blob/master/Update/UpdateTR.pdf)).

PassiveRecorder
=
**Passive Recorder** is a passive recorder for recording wav files up to 500kHz sample rate (384kHz as standard). The case is waterproof and allows use over several days. In addition to the automatic recording with choice of the band of interest, the triggering threshold and the minimum duration of the cries, it offers a basic test of the microphones and the management of the road and pedestrian protocols of Vigie-Chiro. The price is about 100 € for a participatory workshop of 10 devices.

This folder contains all the elements allowing to build and / or modify Passive Recorder

Project status:
-

- At mid 2021 around 450 devices in use ([distribution map on this link](https://framacarte.org/fr/map/distribution-of-teensy-recorders_50829)).
- A new manufacturing file V0.6 is available in the "Manufacturing-SMD" directory. On the basis of a pre-assembled card, manufacturing is facilitated. The "Manufacturing" directory of version V0.3 remains available for memory but is not recommended.
- Choice of 3 types of microphones: MEMS SPU0410LR5H, MEMS ICS-40730 or electret FG23329. Version 0.6 does not take into account the electret microphone, it is possible on request.
- A temperature and hygrometry probe as standard and cut off the power supply to the pre-amp in standby to consume less.
- It is possible to make a single copy in his garage but it is more economical to gather for one day to make 10 copies during a participatory workshop. Just a supervisor with good notions of electronic editing. **The entry point of the production tutorial is "Fabrication-SMD/FabricationPR-SMD.pdf"**.

Description of the directories
-

- **Etude Technique** includes project design documents at the root, manufacturing documents, user manual and software loading in **Documents**, digital filter management with **rePhase** software (<https://sourceforge.net/projects/rephase/>) in **FirFilter** and logo management in **Logo** using **LCDAssistant.exe** (<http://en.radzio.dxp.pl/bitmap_converter/>).
- **Fabrication-SMD** gathers the manufacturing documents necessary for the realization of one or more copies at either or during a participatory workshop.
- **Schema-SMD-MEMS** is the circuit diagram and circuit-print of Passive Recorder V0.4 with MEMS microphone made with the free tool KiCad (<http://kicad-pcb.org/>).
- **Schema-SMD-FG* is the circuit diagram and circuit-print of Passive Recorder V0.4 with FG microphone made with the free tool KiCad.
- **ShemaMicroICS40730B** is the circuit diagram and the small circuit-printed for a micro MEMS ICS-40730 made with the free tool KiCad.
- **ShemaMicroSPU** is the circuit diagram and the small circuit-printed for a MEMS SPU0410LR5H microphone made with the free tool KiCad.
- **Reminder**, the software source code is common to the two TeensyRecorders projects. You will find a notch higher in TeensyRecorder the ** Update ** directory with the user manual ([ManuelTR.pdf](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/blob/master/Update/ManuelTR.pdf)), the binaries to download and the manual explaining how to download them ([UpdateTR.pdf](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/blob/master/Update/UpdateTR.pdf)).