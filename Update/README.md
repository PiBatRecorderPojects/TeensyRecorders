Passive and Active Recorder
===========================

Executable to download on the Teensy 3.6 or 4.1 card to run a Passive or Active
Recorder.

**Last version V1.00 15/09/2023**

Follow the instructions in the document
[UpdatePR.pdf](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/blob/master/Update/UpdateTR.pdf).

The Teensy Recorder User Manual is to be consulted on the document
[ManuelTR.pdf](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/blob/master/Update/ManuelTR.pdf).

The list of versions with the modifications are to be consulted [on this
page](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/tree/master/TeensyRecorder).

The version comes for 2 processors (Teensy 3.6 and 4.1) and with 3 different processor frequencies for the Teensy 3.6 :
-----------------------------------------------------------------------------------------------------------------------

-   **T4.1 - Use for all versions equipped with a Teensy 4.1**.

-   **T3.6 180MHz - Recommended use for Active Recorder and Passive Recorder
    Stereo**.

    -   Passive Recorder for common use of all features without packet loss at
        500kHz sample rate but with higher power consumption.

    -   Passive Recorder Stereo for common use. Stereo can only be used at this
        frequency, otherwise the PRS is seen as a simple PR with only the right
        mic functional.

    -   Active Recorder for common use of all features.

-   **T3.6 144MHz - Recommended use for Passive Recorder**.

    -   Passive Recorder for common use of all features. At 500kHz sample rate,
        packet loss of 8192 samples is possible but rare.

    -   Active Recorder, heterodyne at 250kHz sample rate, other features like
        Passive Recorder.

-   **T3.6 48MHz - Recommended for automatic recordings at 250kHz sample rate
    over long periods of time**.

    -   Passive Recorder, for half power consumption but with 250kHz maximum
        sample rate.

    -   Active Recorder, heterodyne mode not available and functions similar to
        Passive Recorder.
