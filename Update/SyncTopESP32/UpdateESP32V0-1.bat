Rem Command file to download a new version of the SyncTopESP32 software of the Passive Recorder Stereo Synchro
Rem After compilation, the SyncTopESP32.ino.bin and SyncTopESP32.ino.partitions.bin files are to be recovered from (example):
Rem C:\Users\UTILIS~1\AppData\Local\Temp\arduino_build_12876/
Rem The best way to find the directory is to check "Compilation" to display the detailed results.
Rem
Rem Use :
Rem Connect the ESP32 with a USB cable to a PC
Rem Locate the port used via the device manager
Rem Via the "Execute" "cmd" command, open a command window
Rem On the command window:
Rem Make a command "cd directory containing the executable"
Rem Make a command "UpdateESP32V0-1.bat COM6" Adapt the command according to the port used
esptool.exe --chip esp32 --port %1 --baud 921600 --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 80m --flash_size detect 0xe000 boot_app0.bin 0x1000 bootloader_qio_80m.bin 0x10000 SyncTopESP32V0-1.bin 0x8000 SyncTopESP32V0-1.partitions.bin 