BoitesImpression3D
==================

Ce dossier regroupe tous les fichiers relatifs aux boîtes à imprimer en 3D pour chacun des appareils.
A ce jour vous sont fournis les boîtes pour **Passive Recorder** et **Passive Recorder Stéréo** 
(version 2023 et antérieur ; version 2024 ; alimentation 5V, 12V et sans alimentation externe ; version 2025 compatible avec les PCB de 2024)

**NOUVEAU DESIGN 2025 ! augmentation de la largeur des charnières et verrous pour plus de place pour les doigts (+3mm).**

Vous est également fourni deux versions pour le **BatPlayer** V0.2 (2024) !

**Il est très important de bien lire la notice au format PDF et ne surtout pas hésiter à poser des questions sur le groupe Discord si la réponse n'a pas déjà été fournie.**

Il est à préciser également que l'impression 3D n'est pas une pratique intuitive et facile lorsque l'on ne s'est pas penché 
sur le sujet mais cela s'apprend très bien avec de la pratique. Il est possible aujourd'hui de trouver de très bonnes 
imprimantes 3D à des prix abordables (compter 400-500€). Toutefois il faut s'attendre à des temps d'impression très longs (une 20aine
d'heure pour la boîte et une 10aine d'heure pour le couvercle). Pour espérer réduire le temps d'impression, cela demande des imprimantes
haut de gamme (compter 700 à 2000€).
Vous avez toujours la possibilité de vous rapprocher d'un FabLab par chez vous ou de voir avec les personnes de votre entourage qui possèdent
une imprimante 3D.