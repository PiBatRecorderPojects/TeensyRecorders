**English version below**
![Passive Recorder Stereo](https://git.framasoft.org/PiBatRecorderPojects/TeensyRecorders/raw/master/Photos/PRS.JPG)

PassiveRecorderStero
====================

**Passive Recorder Stereo (PRS)** est un enregistreur passif permettant
d'enregistrer des fichiers wav sur 2 voies à 384kHz de fréquence
d'échantillonnage. Le boitier est étanche et permet une utilisation sur
plusieurs jours. Il possède les mêmes caractéristiques qu'un Passive Recorder
mais avec 2 voies d'acquisitions. Le logiciel est le même que Passive Recorder
avec, pour les fonctionnalités de ce dernier, l'utilisation de la voie de droite
jusqu'à 500kHz de fréquence d'échantillonnage et une fonction d'enregistrement
automatique sur les 2 voies à 384kHz de fréquence d'échantillonnage. Il utilise
le même logiciel TeensyRecorder que les PR.

En option, PRS propose une synchronisation par radio (**PRS-S**) entre plusieurs
enregistreurs de même type pour réaliser des trajectographies. Dans ce type
d'utilisation, un PRS-S maître déclenche l'enregistrement sur n PRS-S esclaves
et un top synchro audio est inscrit toutes les secondes sur les fichiers wav
pour resynchroniser les enregistrements en post-traitement.

A mi 2021, environ 70 PRS sont fabriqués y compris 10 réseaux de 3 PRS-S.

Le document de départ pour la fabrication est
Frabrication/FabricationPRS-SMD.pdf

 

PassiveRecorderStereo
=====================

**Passive Recorder Stereo** is a passive recorder for recording 2-way wav files
at 384kHz sample rate. The case is waterproof and allows use over several days.
It has the same characteristics as a Passive Recorder but with 2 acquisition
channels. The software is the same as Passive Recorder with, for the features of
the latter, the use of the right channel up to 500kHz sampling frequency and an
automatic recording function on the 2 channels at 384kHz sampling frequency.

As an option, PRS offers a radio synchronization (**PRS-S**) between several
recorders of the same type to perform tracking. In this type of use, a master
PRS-S triggers the recording on n slaves PRS-S and an audio sync top is written
every second on the wav records to resynchronize the recordings in
postprocessing.

By mid 2021, around 70 PRS are manufactured including 10 networks of 3 PRS-S.

The manufacturing file is complete with a pre-assembled electronic card that can
be ordered from JLCPCB. This solution makes it possible to keep a very low price
(\<110 €) and greatly simplifies the assembly with no delicate welding.

The starting document for manufacturing is Fabrication/FabricationPRS-SMD.pdf

![PRS assemblé](https://git.framasoft.org/PiBatRecorderPojects/TeensyRecorders/raw/master/Photos/PCB-PRS.JPG)
![Extension synchro](https://git.framasoft.org/PiBatRecorderPojects/TeensyRecorders/raw/master/Photos/ExtPRS-S.JPG)
 

