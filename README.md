**English version below**

Présentation des projets
========================

**TeensyRecorder** regroupe des projets d'enregistreurs ultrason réalisés avec
une carte Teensy 3.6 (avant 2023, cf.
<https://www.pjrc.com/store/teensy36.html>) ou Teensy 4.1 (à partir de 2023,
cf. <https://www.pjrc.com/store/teensy41.html>).

Porté bénévolement par un groupe informel de chiro-bricolos, ce projet a été
fait pour une fabrication à réaliser soi-même. Toutefois, compte tenu de la
technique de fabrication à base de cartes pré-montées, il devient difficile de
fabriquer un exemplaire seul dans son coin. Annuellement, des opérations
d'achats groupés sont organisées pour faciliter les fabrications et
l’organisation d’ateliers aux 4 coins du territoire.

Les objectifs
-------------

-   Être **ouvert** (dans le sens des communs). Les logiciels ainsi que leurs
    codes source, les schémas matériels (montages et circuits électroniques)
    sont mis à disposition de tous gratuitement et librement afin de permettre à
    chacun de fabriquer son détecteur.

-   Être **facilement reproductible** pour faciliter la réalisation par toute
    personne intéressée.

-   Être **économique** pour faciliter l’accès à un matériel d’étude de qualité.
    Le prix final est fonction du nombre d'exemplaires construits. Avec 2 ou 3,
    ce sera de l'ordre de 150 à 160€. A partir de 10 exemplaires et en
    bénéficiant des achats groupés effectués chaque année, le prix descend sous
    la barre des 100€.

-   Être **pertinent pour l’étude des chauve-souris**, en couvrant la bande de
    fréquence et en ayant des résultats comparables aux autres détecteurs
    couramment utilisés. Ils sont aussi utilisable pour des enregistrements
    audio et des pionniers les utilisent pour le suivi migratoire des oiseaux ou
    pour l’étude des insectes. Les 3 modèles sont basés sur la même chaine
    d'acquisition (micro, amplification, numérisation) et vous pouvez consulter
    sur [ce lien une comparaison entre un Passive Recorder et un Audiomoth
    réalisée par Michel
    Barataud](http://ecologieacoustique.fr/wp-content/uploads/Comparatif_Audiomoth-PassiveRecorder_MB-2021.pdf).

-   Être **facilement réparable**. Ne restez pas avec un Teensy Recorder en
    panne dans vos tiroirs. Postez votre problème de préférence sur la liste
    “[FabricationTeensyRecorder](https://framalistes.org/sympa/info/fabricationteensyrecorder)”,
    des spécialistes vous répondront pour vous guider dans la recherche d’une
    solution. Au pire, il suffira d’envoyer l’appareil à l’un de ces
    spécialistes qui effectura la réparation. A votre charge ne seront comptés
    que les frais de ports et les pièces nécessaires. Compte tenu de la
    fabrication, il n’y a pas de garantie mais les pièces détachées ont des prix
    dérisoires. La carte processeur est l’élément le plus couteux avec un prix
    de 30€ et elle tombe très rarement en panne. Pour information, une puce
    micro coute au environ de 5€.

3 modèles
---------

**Tableau récapitulatif des principales caractéristiques :**

| Modèles                                                   | PR  | PRS | AR  |
|-----------------------------------------------------------|-----|-----|-----|
| Micro MEMS déportable (plus de 100m)                      | Oui | Oui | Oui |
| Canaux (1 mono, 2 stéréo)                                 | 1   | 2   | 1   |
| Test micro                                                | Oui | Oui | Oui |
| Fréq. échantillonnage 24, 48, 96, 192, 250, 384kHz        | Oui | Oui | Oui |
| Fréq. échantillonnage 500kHz                              | Oui | Non | Oui |
| Enregistrement passif automatique                         | Oui | Oui | Oui |
| Hétérodyne à 384kHz (écoute au casque)                    | Non | Non | Oui |
| Lecture fichier wav (X1, X10, Hétérodyne)                 | Non | Non | Oui |
| Ecran plus boutons (paramètres modifiable sur le terrain) | Oui | Oui | Oui |
| Batteries internes Li-Ion (plusieurs nuits d'autonomie)   | 2   | 2   | 2   |
| Alimentation externe                                      | 12V | 12V | 5V  |
| Boitier étanche                                           | Oui | Oui | Non |
| Carte micro SD 16-400 Go                                  | 1   | 1   | 1   |

** Bilan des fabrications **
    ![Fabrication Passive Recorder](https://git.framasoft.org/PiBatRecorderPojects/TeensyRecorders/raw/master/Photos/Fabrication.jpg)

-   **Passive Recorder -PR-** est un enregistreur passif permettant d'enregistrer des
    fichiers wav jusqu'à 500kHz de fréquence d'échantillonnage (384kHz en
    standard). Le boitier est étanche et permet une utilisation sur plusieurs
    jours. Outre l'enregistrement automatique avec choix de la bande d'intérêt,
    du seuil de déclenchement et de la durée min des cris, il offre un test des
    micros et la gestion des protocoles routier et pédestre de Vigie-Chiro. Le
    prix est d’environ 100€ par PR pour un atelier participatif de 10 appareils.
    Les fichiers de fabrication sont à retrouver [dans ce
    répertoire](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/tree/master/PassiveRecorder/Fabrication-SMD).
    Le point d’entrée est
    “[FabricationPR-SMD.pdf](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/blob/master/PassiveRecorder/Fabrication-SMD/FabricationPR-SMD.pdf)”.

    ![Passive Recorder](https://git.framasoft.org/PiBatRecorderPojects/TeensyRecorders/raw/master/Photos/PassiveRecorder.JPG)

-   **Active Recorder -AR-** propose les
    mêmes fonctionnalités que le précédent avec en plus une fonction d'écoute
    active en mode hétérodyne et une fonction de lecture des fichiers en
    expansion de temps. Le boitier n'est pas étanche. La nouvelle carte Teensy 4.1 permet un pré-trigger de 1 à 15s par l'ajout d'une mémoire supplémentaire. Le prix est de l’ordre de
    100€ par AR pour un atelier participatif de 10 appareils. Les fichiers de
    fabrication sont à retrouver [dans ce
    répertoire](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/tree/master/ActiveRecorder/Fabrication).
    Le point d’entrée est
    “[FabricationAR.pdf](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/blob/master/ActiveRecorder/Fabrication/FabricationAR.pdf)”.
    Au besoin, l’[AR est évalué par Michel Barataud dans le document de ce
    lien](http://ecologieacoustique.fr/wp-content/uploads/Comparatif_detecteurs-manuels_MB.pdf).

    ![Active Recorder](https://git.framasoft.org/PiBatRecorderPojects/TeensyRecorders/raw/master/Photos/ActiveRecorder.JPG)

-   **Passive Recorder Setero -PRS-** propose
    les mêmes fonctionnalités que PR avec l'enregistrement stéréo à 384kHz en
    standard et, en option, une synchronisation radio de n PRS pour réaliser de
    la trajectographie. Le prix est de l’ordre de 100€ par PRS pour un atelier
    participatif de 10 appareils. Les fichiers de fabrication sont à retrouver
    [dans ce
    répertoire](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/tree/master/PassiveRecorderStereo/Fabrication).
    Le point d’entrée est
    “[FabricationPRS-SMD.pdf](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/blob/master/PassiveRecorderStereo/Fabrication/FabricationPRS-SMD.pdf)”.

    ![Passive Recorder Stereo](https://git.framasoft.org/PiBatRecorderPojects/TeensyRecorders/raw/master/Photos/PRS.JPG)

Un micro en standard et 1 option
---------------------------------

-   **microphone de type MEMS ICS-40730**. Malgré une absence de
    caractéristiques ultrason indiqué par le constructeur, ce micro semble le
    plus généraliste avec un bruit de fond très discret. **C'est le micro
    standard des 3 enregistreurs**.

-   **microphone de type MEMS SPU0410LR5H**. Fabriqués par Knowles, ce capteur
    est plus performant en haut de bande mais avec un bruit de fond légèrement
    supérieur au modèle précédent. Il est proposé en second micro.

Ce [document de Michel
Barataud](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/blob/master/Communications/Barataud_Micro-MEMS-ICSvsSPU_comparaison-seqPpip.pdf)
analyse les différences entre les deux micros MEMS ICS et SPU.

**Attention**, les micros de type MEMS craignent la pluie. [Des protections sont
proposées dans ce
document](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/blob/master/CoqueMicro/ProtectionMicro.pdf)
et [dans cet autre
document](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/blob/master/CoqueMicro/TestTissuAcoustique.pdf).

Le remplacement du circuit puce micro est expliqué [dans ce
document](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/blob/master/PassiveRecorder/Fabrication-SMD/RemplacementMicro.pdf).

Manuel d’utilisation et de mise à jour
--------------------------------------

Le logiciel est commun aux 3 modèles et aux 2 cartes processeur. Des mises à jours sont proposées
régulièrement. Pour être au courant des sorties des nouvelles versions, vous
devez vous abonner à l’une des deux listes proposées ci-dessous ou [consulter
régulièrement ce
lien](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/tree/master/Update).
Les différences entres les versions sont indiquées [sur cette
page](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/tree/master/TeensyRecorder).
**Il est vivement conseillé d’utiliser la dernière version**.

**Le manuel d’utilisation du logiciel** est à [consulter et télécharger sur ce
lien](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/blob/master/Update/ManuelTR.pdf).

**Le manuel expliquant comment mettre à jour le logiciel** est à [consulter et
télécharger sur ce
lien](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/blob/master/Update/UpdateTR.pdf).

Echanges entres utilisateurs :
------------------------------

- **Serveur Discord** Depuis aout 2023 un serveur Discord est disponible pour les échanges avec de nombreuses rubriques.  Vous pouvez le rejoindre via ce lien: https://discord.gg/VCHEMPGnrf.
-   **Liste de discussion** Les utilisateurs de Teensy Recorder sont invités à s'abonner à la liste de
    diffusion "TeensyRecorders" sur Framalistes pour recevoir des avis de
    modification de la part du concepteur ou échanger entre utilisateurs pour
    partager leurs expériences.

Si vous avez une interrogation sur un mode de fonctionnement ou une proposition
d’amélioration, c’est cette liste qu’il faut utiliser.

Si vous avez une panne, vous pouvez l’utiliser aussi, bien que la liste
ci-dessous soit plus adaptée.

Vous trouverez la page d'accueil de cette liste sur ce lien :
<https://framalistes.org/sympa/info/teensyrecorders>.

Pour s'abonner, utilisez les liens ci-dessous :

S'abonner : <https://framalistes.org/sympa/subscribe/teensyrecorders>

Se désabonner : <https://framalistes.org/sympa/sigrequest/teensyrecorders>

Envoyer un message à chaque abonné, utilisez l'adresse :
teensyrecorders\@framalistes.org

Echanges entres constructeurs :
-------------------------------

- **Serveur Discord** Depuis aout 2023 un serveur Discord est disponible pour les échanges avec de nombreuses rubriques.  Vous pouvez le rejoindre via ce lien: https://discord.gg/VCHEMPGnrf.
-   **Liste de discussion** Les constructeurs de Teensy Recorder sont invités à s'abonner à la liste de
    diffusion "FabricationTeensyRecorder" sur Framalistes pour s'organiser ou
    échanger des recettes de construction.

Cette liste est idéale pour décrire une panne et trouver des spécialistes pour
vous aider à la résoudre.

Tout les problèmes de construction sont les bienvenus et, forcément, encore plus
pour les solutions.

**Mais, surtout, quelque soit le résultat, échec ou succès**, merci de le
diffuser pour faire avancer la connaissance des problèmes et solutions.

Vous trouverez la page d'accueil de cette liste sur ce lien :
<https://framalistes.org/sympa/info/fabricationteensyrecorder>.

Pour s'abonner, utilisez les liens ci-dessous :

S'abonner : <https://framalistes.org/sympa/subscribe/fabricationteensyrecorder>

Se désabonner :
<https://framalistes.org/sympa/sigrequest/fabricationteensyrecorder>

Envoyer un message à chaque abonné, utilisez l'adresse :
fabricationteensyrecorder\@framalistes.org

 

 

Presentation of the projects
============================

**TeensyRecorder** brings together ultrasound recorder projects made with a
Teensy 3.6 card (before 2023, cf. <https://www.pjrc.com/teensy/techspecs.html>) or Teensy 4.1 (from 2023, cf. <https://www.pjrc.com/store/teensy41.html>).

Volunteered by an informal group of chiro-do-it-yourselfers, this project was
made for a production to make yourself. However, taking into account the
manufacturing technique based on pre-assembled cards, it becomes difficult to
manufacture a single copy in its corner. Annually, group purchasing operations
are organized to facilitate manufacturing and the organization of workshops
across the country.

**All documentation is in French**. They are relatively stable now. Translators
for English or other versions are welcome.

Ditto for the software, the structure is in place to easily translate operator
messages into languages other than English and French already present.

Goals
-----

-   Be **open** (in the sense of the commons). The software as well as their
    source codes, hardware diagrams (assemblies and electronic circuits) are
    made available to all for free and freely in order to allow everyone to
    manufacture their detector.

-   Be **easily reproducible** to facilitate the realization by any interested
    person.

-   Be **economic** to facilitate access to quality study material. The final
    price depends on the number of units built. With 2 or 3, it will be around
    150 to 160 €. From 10 copies and benefiting from group purchases made each
    year, the price drops below 100 € .

-   Be **relevant for the study of bats**, covering the frequency band and
    having comparable results to other commonly used detectors. They can also be
    used for audio recordings and pioneers used them for migratory monitoring of
    birds or for studying insects. The 3 models are based on the same
    acquisition chain (microphone, amplification, digitization) and you can
    consult on [this link a comparison between a Passive Recorder and an
    Audiomoth made by Michel
    Barataud](http://ecologieacoustique.fr/wp-content/uploads/Comparatif_Audiomoth-PassiveRecorder_MB-2021.pdf).

-   Be **easily repairable**. Don't be left with a broken Teensy Recorder in
    your drawers. Preferably post your problem on the
    "FabricationTeensyRecorder" list, specialists will respond to guide you in
    finding a solution. At worst, it will suffice to send the device to one of
    these specialists for repair. At your expense, only the shipping costs and
    the necessary parts will be counted. Considering the manufacturing, there is
    no warranty but spare parts are very cheap. The processor card is the most
    expensive item at a price of \$ 30 and very rarely fails. For information, a
    micro chip costs around 5 €.

3 models
--------

**Summary table of the main characteristics :**

| Type                                                     | PR  | PRS | AR  |
|----------------------------------------------------------|-----|-----|-----|
| Remote MEMS microphone (30 to 50m max)                   | Yes | Yes | Yes |
| Channels (1 mono, 2 stereo)                              | 1   | 2   | 1   |
| Microphone test                                          | Yes | Yes | Yes |
| Sample frequency 24, 48, 96, 192, 250, 384kHz            | Yes | Yes | Yes |
| Sample frequency 500kHz                                  | Yes | No  | Yes |
| Automatic passive recording                              | Yes | Yes | Yes |
| Heterodyne at 384kHz (listening through headphones)      | No  | No  | Yes |
| Reading wav file (X1, X10, Heterodyne)                   | No  | No  | Yes |
| Screen plus buttons (parameters modifiable in the field) | Yes | Yes | Yes |
| Internal Li-Ion batteries (several nights of autonomy)   | 2   | 2   | 2   |
| External power supply                                    | 12V | 12V | 5V  |
| Waterproof case                                          | Yes | Yes | No  |
| 16-400 GB micro SD card                                  | 1   | 1   | 1   |

-   **Passive Recorder -PR- (around 600 manufactured at the end of 2022, new hardware
    V0.8 for Teensy 4.1 with a choice of 2 types of microphones and a temperature probe
    option)** is a passive recorder for recording wav files up to 500kHz sample
    rate (384kHz as standard). The case is waterproof and allows use over
    several days. In addition to the automatic recording with choice of the band
    of interest, the triggering threshold and the minimum duration of the cries,
    it offers a test of the microphones and the management of the road and
    pedestrian protocols of Vigie-Chiro. The price is about 100 € per PR for a
    participatory workshop of 10 devices. The manufacturing files can be found
    [in this
    directory](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/tree/master/PassiveRecorder/Fabrication-SMD).
    The entry point is
    “[FabricationPR-SMD.pdf](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/blob/master/PassiveRecorder/Fabrication-SMD/FabricationPR-SMD.pdf)”.

-   **Active Recorder -AR- (around 530 manufactured at the end of 2022)** offers the
    same features as the previous one, plus an active listening function in
    heterodyne mode and a function for reading files in time expansion. The case
    is not waterproof.The new Teensy 4.1 card allows a pre-trigger from 1 to 15s by adding additional memory. The price is around € 100 per AR for a participatory
    workshop of 10 devices. The manufacturing files can be found [in this
    directory](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/tree/master/ActiveRecorder/Fabrication).
    The entry point is
    “[FabricationAR.pdf](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/blob/master/ActiveRecorder/Fabrication/FabricationAR.pdf))”.
    If necessary, the [AR is evaluated by Michel Barataud in the document at
    this
    link](http://ecologieacoustique.fr/wp-content/uploads/Comparatif_detecteurs-manuels_MB.pdf).

-   **Passive Recorder Stereo -PRS- (around 110 manufactured at the end of 2022)**
    offers the same functionalities as PR with the addition of a stereo
    recording function at 384kHz and, as an option, a radio synchronization of n
    PRS to carry out trajectography. The price is around € 100 per PRS for a
    participatory workshop of 10 devices. The manufacturing files can be found
    [in this
    directory](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/tree/master/PassiveRecorderStereo/Fabrication).
    The entry point is
    “[FabricationPRS-SMD.pdf](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/blob/master/PassiveRecorderStereo/Fabrication/FabricationPRS-SMD.pdf)”.

A microphone as standard and 1 option
--------------------------------------

-   **microphone type MEMS ICS-40730**. Despite an absence of ultrasound
    characteristics indicated by the manufacturer, this microphone seems the
    most generalist with a very discreet background noise. \*\* This is the
    standard microphone for the 3 recorders \*\*.

-   **microphone type MEMS SPU0410LR5H**. Manufactured by Knowles, this sensor
    is more efficient at the top of the band but with a background noise
    slightly higher than the previous model. It is offered as a second
    microphone.

This [document by Michel
Barataud](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/blob/master/Communications/Barataud_Micro-MEMS-ICSvsSPU_comparaison-seqPpip.pdf)
analyzes the differences between the two MEMS ICS and SPU microphones.

**Warning**, MEMS type microphones fear the rain. [Protections are offered in
this
document](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/blob/master/CoqueMicro/ProtectionMicro.pdf)
or [in this other
document](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/blob/master/CoqueMicro/TestTissuAcoustique.pdf).

The replacement of the micro chip circuit is explained [in this
document](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/blob/master/PassiveRecorder/Fabrication-SMD/RonnementMicro.pdf).

User manual and update
----------------------

The software is common to the 3 models and to the 2 processor boards. Updates are offered regularly. To be
aware of the release of new versions, you must subscribe to one of the two lists
offered below or check [this link
regularly](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/tree/master/Update).
The differences between the versions are indicated [on this
page](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/tree/master/TeensyRecorder).
**We strongly recommend that you use the latest version**. The software user
manual is to [consult and download on this
link](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/blob/master/Update/ManuelTR.pdf).
The manual explaining how to update the software is to [consult and download on
this
link](https://framagit.org/PiBatRecorderPojects/TeensyRecorders/-/blob/master/Update/UpdateTR.pdf).

Exchanges between users:
------------------------

- **Discord server** Since August 2023 a Discord server is available for exchanges with many topics. You can join him via this link: https://discord.gg/VCHEMPGnrf.
-   **Mailing list** Passive Recorder users are invited to subscribe to the
    mailing list "TeensyRecorders" on Framalists to receive notices of change
    from the designer or exchange between users to share their experiences.

If you have a question about a method of operation or a proposal for
improvement, this is the list that should be used.

If you have a fault, you can use it too, although the list below is more
suitable.

You will find the home page of this list on this link:
<https://framalistes.org/sympa/info/teensyrecorders>.

To subscribe, use the links belows :

Subscribe : <https://framalistes.org/sympa/subscribe/teensyrecorders>

Unsubscribe : <https://framalistes.org/sympa/sigrequest/teensyrecorders>

Send a message to each subscriber, use the address :
teensyrecorders\@framalistes.org

Exchanges between manufacturers:
--------------------------------

- **Discord server** Since August 2023 a Discord server is available for exchanges with many topics. You can join him via this link: https://discord.gg/VCHEMPGnrf.
-   **Mailing list** Teensy Recorder's builders invited to subscribe to mailing
    list "FabricationTeensyRecorder" on Framalistes to organize or exchange
    construction recipes.

This list is ideal for describing a fault and finding specialists to help you
resolve it.

All construction problems are welcome and, of course, even more so for
solutions.

**But, above all, whatever the result, failure or success**, thank you for
disseminating it to advance knowledge of problems and solutions.

You will find the home page of this list on this link:
<https://framalistes.org/sympa/info/fabricationteensyrecorder>.

To subscribe, use the links belows :

Subscribe : <https://framalistes.org/sympa/subscribe/fabricationteensyrecorder>

Unsubscribe :
<https://framalistes.org/sympa/sigrequest/fabricationteensyrecorder>

Send a message to each subscriber, use the address :
fabricationteensyrecorder\@framalistes.org

Licence :
---------

**PassiveRecorder Copyright (c) 2018 Vrignault Jean-Do.** All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

-   Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

-   Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

-   Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
