"""
Programme d'analyse complet d'une arborescence de fichiers wav. Il faut sélectionner le répertoire le plus haut
Un répertoire doit contenir uniquement les fichiers wav d'une configuration spécifique
Pour chaque fichier wav :
 - FFT 512 points avec fenêtre de Han et sortie du niveau des canaux en dB
 - Moyenne des résultats sur 26ms (moyenne)
 - Mémorisation du niveau max de chaque canal
 - Moyenne des résultats sur 93ms
 - Mémorisation du niveau min sur chaque canal
Pour chaque répertoire
 - Analyse des fichiers wav présents et moyenne des niveaux max des fichiers wav
 - Moyenne des 3 canaux adjacents
 - Constitution du nom du répertoire du style "DirA-DirB-DirC"
 - Sauvegarde du résultat max dans un fichier CSV NomDirA.csv"
"""

import tkinter as tk
from tkinter.filedialog import askdirectory
import wave
import csv
import numpy as np
import array as arr
import os
from numpy.fft import fft
from struct import unpack

#-------------------------------------------------------
# Variables globales

# Taille de la FFT
nFFT = 512
nFFT2 = (int)(nFFT / 2)
# Nom du répertoire de sortie des résultats
DirNameResult = ""
# Chemin du répertoire des fichiers wav
WaveDir = ""
# Nom et chemin du fichier csv des résultats
CSVFileName = ""
# Tableau de moyennage des résultats du niveau max d'un fichier wav
LstNivMoy = []
# Cumul et moyennage des résultats max des wav d'un répertoire
LstDirMoyA = []
LstDirMoyB = []
# Temps d'intégration du moyennage du niveau max 
# A 384kHz et 512 points, 1 FFT = 1.333ms, 20=26ms, 50=66ms, 70=93ms, 100=133ms, 200=266ms, 500=660ms
# Les tests sur 5 wav enregistrés dans les mêmes conditions démontrent que 20 (26ms) est un bon compromis
Moy = 20

# Temps d'intégration du moyennage du niveau min
MoyN = 70
# Liste des résultats max d'un fichier wav
LstNivMax = []
# Fenêtre de la FFT
FenFFT = np.hanning(nFFT)
#FenFFT = np.hamming(nFFT)
#FenFFT = np.bartlett(nFFT)
#FenFFT = np.blackman(nFFT)
#FenFFT = np.kaiser(nFFT, 0) # Rectangular

#-------------------------------------------------------
# Fonction de calcul FFT avec transformation en dB
def dbfft(x, fs, win=None, ref=32768):
    """
    Calculate spectrum in dB scale
    Args:
        x: input signal
        fs: sampling frequency
        win: vector containing window samples (same length as x).
             If not provided, then rectangular window is used by default.
        ref: reference value used for dBFS scale. 32768 for int16 and 1 for float

    Returns:
        freq: frequency vector
        s_db: spectrum in dB scale
    """

    N = len(x)  # Length of input sequence

    if win is None:
        win = np.ones(N, dtype=int)
    if len(x) != len(win):
            raise ValueError('Signal and window must be of the same length')
    x = x * win

    # Calculate real FFT and frequency vector
    sp = np.fft.rfft(x)
    freq = np.arange((N / 2) + 1) / (float(N) / fs)

    # Scale the magnitude of FFT by window and factor of 2,
    # because we are using half of FFT spectrum.
    s_mag = np.abs(sp) * 2 / np.sum(win)

    # Convert to dBFS
    #s_dbfs = 20 * np.log10(s_mag/ref) Provoque une division par 0 sur certains fichiers wav !
    s_dbfs = []
    for i in range(nFFT2):
        s_dbfs.append(0.0)
        if s_mag[i]/ref == 0:
            s_dbfs[i] = -120.0
        else:
            s_dbfs[i] = 20 * np.log10(s_mag[i]/ref)

    return freq, s_dbfs

#-------------------------------------------------------
# Fonction de lecture d'un fichier wav
def AnalyseWav(wavefile):
    # Ouverture fichier wav
    wavFile = wave.Wave_read(wavefile)
    # Lecture des informations sur le fichier
    length = wavFile.getnframes()
    sample_rate = wavFile.getframerate()
    windows_count = length // sample_rate
    channels = 1 if wavFile.getnchannels() == 1 else 2  # Stereo (2) vs. Mono (1)
    frames = sample_rate * windows_count
    # Calcul nombre de FFT n points
    nbFFT = (int)(length / nFFT)
    #print ("Nb FFT 1024 : " + str(nbFFT))

    # Boucle de lecture des échantillons et FFT
    m = 0
    for i in range(nbFFT):
        # Lecture des échantillons
        LstEchB = wavFile.readframes(nFFT)
        # Transformation en un tableau de short
        LstEchS = unpack('h'*(len(LstEchB)//2),LstEchB)
        # Calcul FFT
        freq, s_dbfs = dbfft(LstEchS, sample_rate, FenFFT, 32768)
        # Traitement des résultats       
        for c in range(nFFT2):
            # Mémorisation des niveaux max dans le tableau de moyennage
            LstNivMoy[c] = LstNivMoy[c] + s_dbfs[c]
        m = m + 1
        if m == Moy:
            # Moyennage des résultats de niveaux max
            for c in range(nFFT2):
                LstNivMoy[c] = LstNivMoy[c] / Moy
            # Mémorisation du niveau max
            for c in range(nFFT2):
                if LstNivMax[c] < LstNivMoy[c]:
                    LstNivMax[c] = LstNivMoy[c]
                LstNivMoy[c] = 0.0
            m = 0
    # Si le reste des FFT calculées approche du temps imparti, sinon le reste n'est pas pris en compte
    if m > Moy-5:
        # Moyennage des résultats de niveau max
        for c in range(nFFT2):
            LstNivMoy[c] = LstNivMoy[c] / m
        # Mémorisation du niveau max
        for c in range(nFFT2):
            if LstNivMax[c] < LstNivMoy[c]:
                LstNivMax[c] = LstNivMoy[c]

#-------------------------------------------------------
# Fonction récursive d'analyse d'un répertoire
# curPath  chemin du répertoire à analyser
# firstDir chemin du répertoire de départ
def AnalyseDir( curPath, firstDir):
    print ("AnalyseDir " + curPath + ", " + firstDir)

    # Nom de la ligne de résultats
    NomLigne = curPath[len(firstDir)+1:len(curPath)]
    NomLigne = NomLigne.replace("\\", "_")
    #print("Ligne " + NomLigne)

    # Enumération des fichiers wav présents dans ce répertoire
    contenu = os.listdir(curPath)
    LstWavFiles = [fichier for fichier in contenu if fichier.endswith((".wav", ".WAV", ".Wav"))]

    # RAZ de la liste des résultats
    for i in range(nFFT2):
        LstDirMoyA[i] = 0.0

    # Pour chaque fichier wav
    nbWav = 0
    for fichier in LstWavFiles:
        nbWav = nbWav + 1
        print("AnalyseWav " + os.path.join(curPath, fichier))
        # RAZ des niveaux max
        for c in range(nFFT2):
            LstNivMax[c] = -150.0
        # Analyse du fichier
        AnalyseWav(os.path.join(curPath, fichier))
        # Cumul des résultats
        for c in range(nFFT2):
            LstDirMoyA[c] = LstDirMoyA[c] + LstNivMax[c]

    if nbWav > 0:
        # Moyennage des résultats du répertoire
        for c in range(nFFT2):
            LstDirMoyA[c] = LstDirMoyA[c] / nbWav
        for c in range(nFFT2):
            """
            # Moyenne 0 canaux
            LstDirMoyB[c] = round(LstDirMoyA[c], 1)
            """
            """
            # Moyenne sur 2 canaux
            if c == 0:
                LstDirMoyB[c] = round(LstDirMoyA[c], 1)
            else:
                LstDirMoyB[c] = round((LstDirMoyA[c-1] + LstDirMoyA[c]) / 2, 1)
            """
            """
            # Moyenne sur 3 canaux
            if c == 0 or c == nFFT2-1:
                LstDirMoyB[c] = round(LstDirMoyA[c], 1)
            else:
                LstDirMoyB[c] = round((LstDirMoyA[c-1] + LstDirMoyA[c] + LstDirMoyA[c+1]) / 3, 1)
            """
            """
            # Moyenne sur 4 canaux
            if c == 0 or c == 1 or c >= nFFT2-2:
                LstDirMoyB[c] = round(LstDirMoyA[c], 1)
            else:
                LstDirMoyB[c] = round((LstDirMoyA[c-1] + LstDirMoyA[c] + LstDirMoyA[c+1] + LstDirMoyA[c+2]) / 4, 1)
            """
            # Moyenne sur 5 canaux
            if c == 0 or c >= nFFT2-3:
                LstDirMoyB[c] = round(LstDirMoyA[c], 1)
            else:
                LstDirMoyB[c] = round((LstDirMoyA[c-2] + LstDirMoyA[c-1] + LstDirMoyA[c] + LstDirMoyA[c+1] + LstDirMoyA[c+2]) / 5, 1)
        # Sauvegarde des résultats dans le fichier CSV des résultats
        TbResMax = [NomLigne]
        for i in range(nFFT2):
            TbResMax.append(str(LstDirMoyB[i]))
            TbResMax[i+1] = TbResMax[i+1].replace(".", ",")
        with open(CSVFileMax, 'a', newline='') as f:
            writer = csv.writer(f, delimiter = '\t')
            writer.writerow(TbResMax)

    # Analyse récursive des répertoires
    for file in os.listdir(curPath):
        d = os.path.join(curPath, file)
        if os.path.isdir(d):
            AnalyseDir(os.path.join(curPath, d), firstDir)

#-------------------------------------------------------
# Fonction principale

# Sélection du répertoire de départ des fichiers wav
root = tk.Tk()
root.withdraw()            # pour ne pas afficher la fenêtre Tk
WaveDir = askdirectory()   # lance la fenêtre
print ("Analyse répertoire " + WaveDir)

# Récupération du nom du répertoire
p = WaveDir.rfind("/")
if p > -1:
    DirNameResult = WaveDir[p+1:]
else:
    DirNameResult = "Results"

# Boucle des canaux et préparation du tableau des résultats
step = 384 / 2 / nFFT2
cnx  = step / 2
TbCnx = ["Fichiers / kHz"]
for i in range(nFFT2):
    TbCnx.append(str(cnx))
    TbCnx[i+1] = TbCnx[i+1].replace(".", ",")
    cnx += step
    LstNivMax.append(0.0)
    LstNivMoy.append(0.0)
    LstDirMoyA.append(0.0)
    LstDirMoyB.append(0.0)

# Création du fichier CSV des niveaux max et ajout de la 1ere ligne avec les fréquences
CSVFileMax = WaveDir + "/" + DirNameResult + ".csv"
with open(CSVFileMax, 'w', newline='') as f:
  writer = csv.writer(f, delimiter = '\t')
  writer.writerow(TbCnx)

# Analyse récursive du répertoire
AnalyseDir(WaveDir, WaveDir)
print("Fin d'analyse répertoire " + WaveDir + "\n\n")
