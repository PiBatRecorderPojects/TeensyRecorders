TeensyRecorder
=
This directory exposes the TeensyRecorder software code common to both the PassiveRecorder and ActiveRecorder projects.
It uses the Arduino IDE (<https://www.arduino.cc/en/main/software>) along with the TeensyDuino extension (<https://www.pjrc.com/teensy/teensyduino.html>) for Teensy cards.

**It mainly uses the following libraries:**

- **SDFat** <https://github.com/greiman/SdFat>
- **U8G2Lib** <https://github.com/olikraus/u8g2>
- **Snooze** <https://github.com/duff2013/Snooze> spécific version for Teensy 4.1
- **RamMonitor** <https://github.com/veonik/Teensy-RAM-Monitor> Not working for Teensy 4.1
- **ModesModifiers** internal library in ModesModifiers directory 

Version V1.00 15/09/2023
-
- Following the corrections of V0.99, correction of an initialization for Teensy 3.6

Version V0.99 15/09/2023 (Does not work on Teensy 3.6)
-
- If SD card unreadable at startup and low batteries, more explicit error message
- PR and PRS, display of the temperature line in the parameters, even in the absence of a probe
- Purging of some warnings
- Using Arduino IDE 2.1.1
- Heterodyne without graph, display the names of the last 3 recorded files
- Max recording time, calculation correction
- Consideration of the MCP3221 for the measurement of internal batteries
- MAX module, taking error mode into account
- Stereo recording, wav file name compatible with the Vigie-Chiro program (PaRecPR4146560_1_AAAAMMJJ_HHMMSSR.wav for Right trig or PaRecPR4146560_0_AAAAMMJJ_HHMMSSR.wav for left)
- Added CPU frequency to copyright screen
- Extended microphone test, reduction in the impact of frequencies above 150kHz on the final note

Version V0.98 06/06/2023
-
- Consideration of the AGC in the modifiable parameters in heterodyne mode
- Added automatic gain control on the heterodyne to reduce noise
- Micro test, on a PRS T3.6 at 48MHz, we use the sampling frequency of 192kHz
- Fixed external power measurement only for PR and PRS
- Fixed battery voltage reading error on some MAX modules
- Fixed MAX module handling (thanks Albert)
- Using TeensyDuino 1.58.1 with specific Snooze library for Teensy 4.1
- AR player, fixed setting to select [X10 Het Del] values

Version V0.97 05/05/2023
-
- AR and PRS with T4.1, button wake-up correction
- With pre-trigger >= 1s, if the min duration is equal to the pre-trigger, it is automatically increased by one second. The max duration is always at least 1s longer than the min duration
- Despite switching to the micro test, retention of the previous recording mode at the end of the micro test
- Teensy 4.1 with 8MB RAM, fixed crash if pre-trigger = 0s
- If memory problem, switch to error mode with an explicit message instead of doing a reset
- Addition of a parameter to correct the voltage measurement of the batteries
- Addition of a parameter to correct the voltage measurement of the external power supply
- Fixed max pre-trigger duration to 10s for 384kHz and 7s for 500kHz
- Using Arduino IDE 2.1.0

Version V0.96 04/04/2023
-
- Fix for loud clicks in heterodyne with Teensy 4.1
- In heterodyne, optional graph to avoid parasitic noises

Version V0.95 23/03/2023
-
- Fixed cyclic reset bug on AR with some T4.1
- Small correction in version management
- Selective Heterodyne function removed (T4.1 only), bug being analyzed

Version V0.94 27/02/2023
-
- Using Arduino IDE 2.0.3
- Using TeensyDuino 1.57.2 with specific Snooze library for Teensy 4.1
- PR and PRS, parameter mode, no temperature line if sensor absent
- Fixed RhinoLogger mode
- Fixed pre-trigger calculation
- Fixed cyclic reset bug on Teensy Recorder with some T4.1
- Using the "Zero padding" technique to improve timing accuracy of RhinoLogger mode with T4.1 or T3.6 with 144MHz

Version V0.93 16/01/2023
-
- Due to the limitation of the maximum file size (4GB), resumption of the maximum durations of the timed recordings according to the sampling frequency
- If the fixed point protocol is selected, this mode is kept, even after a wake or a reset
- Improved battery measurement with the MAX17043
- Parameters, added a line to display temperature and humidity for PR and PRS
- On language change, update of battery level, probe and SD card parameters

Version V0.92 20/11/2022
-
- PRS, right/left correction on microphone test and recording
- Timed recording, correction on the maximum duration depending on the sampling frequency
- Timed recording, displays recording duration and waiting time in HH:MM:SS
- Fixed systematic resetting of interest band to default values
- Improved versioning of settings
- Using TeensyDuino 1.57 with specific Snooze library for Teensy 4.1
- Using U8G2 library V2.33.15
- Heterodyne, added auto play mode

Version V0.91 22/10/2022
-
- Heterodyne and Audio Recorder modes, change threshold modification to avoid unintended changes
- In player mode, added the possibility to delete the selected file and consequently, change of the mode of choice of the type of reading (X1, X10 and Heterodyne)
- Changing Default Profile Names (Default, Bats 384kHz, Bats 500kHz, RhinoLogger, Audio 48kHz)
- SD formatting, some cards require a reset after formatting
- Fixed threshold handling while running heterodyne and audio mode
- At start-up, if the Fixed Point Protocol mode is requested, it automatically switches to this mode
- In micro test mode, a low threshold is imposed
- Stereo with decimation, correct right/left sample
- AR, added more selective heterodyne mode with Teensy 4.1 only
- Adaptation of sleep mode to Teensy 4.1 (specific Snooze library)
- With a Teensy 4.1 equipped with an additional memory of 8MB, management of a pre-trigger modifiable from 1 to 15
- Teensy 4.1 compatible software on PR and AR only
- On a PRS and timed recording, stereo recording if requested.
- On "Audio" profile, put X10 false by default
- Read mode on AR, authorization of heterodyne mode with 500kHz in time expansion
- Simplification of cross-platform SDFat use with use of the SdFs class for SD card management
- Fix sleep in timed recording mode
- Dealing with SD card write errors
- AR, playback mode, taking into account stereo files with mixes of the 2 channels and automatic attenuation to stay in the 12-bit domain

Version V0.90 15/10/2022
-
- Heterodyne and Audio Recorder modes, change threshold modification to avoid unintended changes
- In player mode, added the possibility to delete the selected file and consequently, change of the mode of choice of the type of reading (X1, X10 and Heterodyne)
- Changing Default Profile Names (Default, Bats 384kHz, Bats 500kHz, RhinoLogger, Audio 48kHz)
- SD formatting, some cards require a reset after formatting
- Fixed threshold handling while running heterodyne and audio mode
- At start-up, if the Fixed Point Protocol mode is requested, it automatically switches to this mode
- In micro test mode, a low threshold is imposed
- Stereo with decimation, correct right/left sample
- AR, added more selective heterodyne mode with Teensy 4.1 only
- Adaptation of sleep mode to Teensy 4.1 (specific Snooze library)
- With a Teensy 4.1 equipped with an additional memory of 8MB, management of a pre-trigger modifiable from 1 to 15
- Teensy 4.1 compatible software on PR and AR only
- On a PRS and timed recording, stereo recording if requested.
- On "Audio" profile, put X10 false by default
- Read mode on AR, authorization of heterodyne mode with 500kHz in time expansion
- Simplification of cross-platform SDFat use with use of the SdFs class for SD card management
- Fix sleep in timed recording mode
- Dealing with SD card write errors
- AR, playback mode, taking into account stereo files with mixes of the 2 channels and automatic attenuation to stay in the 12-bit domain

Version V0.90 04/04/2022
-
- Stop recording on loss of buffers. At the beginning of recording, the losses of buffers are frequent, especially on AR. We stop only in debug mode and when the minimum duration is reached.

Version V0.89 04/01/2022
-
- Set Teensyduino V1.56
- In heterodyne mode, if the sampling frequency is less than 250kHz, we force 384 or 250kHz depending on the CPU frequency
- Fixed profile renaming function
- For SF 96, 48 and 24kHz, acquisition at 240kHz, application of an anti-aliasing filter of, respectively, 48, 24 and 12kHz, then decimation to SF 96, 46 or 24kHz
- Profile selection parameter in second position
- Corrections of the saving of the band of interests in the profiles
- Temperature reading from the start of heterodyne mode (after 30s on previous versions)
- Management of the absence of a humidity temperature sensor or BMP280 (temperature only) or BME280 (temperature and humidity)
- Management of an AHT10 type sensor

Version V0.88 09/09/2021
-
- Set SDFAT V2.0.7
- Fixed timing mode recording time modification behavior
- No more RamUsage in the log
- Improved storage in the log of parameters used in audio mode
- Playback mode on AR, taking stereo files into account
- Playback mode on AR, taking into account the sampling frequencies 44100 and 24000kHz
- Heterodyne, elimination of audible pulsation on background noise (in return, some repetitions possible of a block of 42ms at 384kHz)
- FFT, improved magnitude calculations in extended microphone test and noise measurement
- Heterodyne, correction of the activity on the graph
- Switching of the preamp power supply, loop for slowly loading the 470µF capacity on the preamp so as not to disturb the processor and especially access to the SD card
- Road and pedestrian protocols, display of the recording time of the current point
- Periodic reset of the heterodyne frequency to alleviate a problem of heterodyne signal attenuation when the frequency does not change for a long time

Version V0.87 26/05/2021
-
- Set Arduino IDE 1.8.13
- Set SDFAT V2.0.6
- Timed recording, modifiable recording time and standby in hh:mm:ss
- Timed recording, maximum time limitation depending on the sampling frequency
- High pass filter, input type xx.xkHz if the sampling frequency is less than 192kHz
- On a PRS, if profile by default, we force the stereo mode
- First version for PRS-Synchro with SynTopESP32 V0.10
- Management at start-up of a possible AutoTime.ini file for setting the time without button or screen
- Management at start-up of a possible AutoProfiles.ini file for parameter initialization without button or screen
- Time display, correction of delays and untimely jumps of 2 or 3s
- Extended mic test result on PRS, Left / Right reverse
- On a PRS, crash on the sequence of a second extended microphone test via the "Up" key
- On a PRS in microphone test, if the stereo mode is mono L or R, we force the stereo mode
- OK test of a 400 GB SD card
- On AR correction of standby operation in automatic recording mode (problem on V0.86 in french language)

Version V0.86 15/03/2021
-
- Set U8G2 V2.28.10
- Set SDFAT V2.0.4 for working with SDXC in FAT64 (exFAT). Formatting to FAT32 is no longer necessary
- Use sd.format of SDFat for formating SD in FAT32 or exFAT
- Monitoring the status of the SD card and, in the event of a problem, re-initiation of the card
- Management of 4 languages with a single file for translation (ConstChar.c) but just French and English available
- Copyright, addition of the SyncTopESP32 software version in case of PRS-S and addition of the name of the recorder
- Automatic end of error mode after 2mn with a reset in auto recording mode except in case of SD full
- At startup, if the SD card is full (<50MB), switch to error mode
- Recorder type reading delay to correct some cases of erroneous reading on AR
- Stereo mode, L/R inversion of detections and microphone test

Version V0.85 12/12/2020
-
- Recording in the log of the results of the microphone test
- SD write-free test at startup, after standby, at each change of mode and on write wav file
- Automatic end of error mode with a reset in auto recording mode
- Addition of an improved microphone test mode with production of response curves

Version V0.84 15/10/2020
-
- Update SD Size after formating
- Correction right/left channels in stereo
- Adding Mono Synchro PR type
- Adding Synchro parameters
- Adding Synchro mode
- Improved reading of ini files
- Max duration, minimum authorization 1s in reading ini file
- Using setSyncInterval(10s) for better time management
- Set SDFAT 1.1.4 for best 128GO management
- Set TeensyDuino 1.53
- Correction of the processing of the digital low-pass filter too brutally "optimized" on V0.83
- Stereo operation setting only on stereo device
- AR, respect for the change of operating mode (Rec> Player> Parameters) for all operating modes
- Processing of pin D33 to power or not the microphone preamp (with hardware associated, gain 2mA for PR in sleep mode and 4mA pour a PRS)
- Adding 24kHz sample frequency for audio recording
- Set max Recording time to 18000 (5h) in Timed Recording mode
- Addition of a heterodyne bandpass filter but the function is not activated (identical result with or without)
- Fixed a memory problem. The 1st use of a real in a printf function reserves 252 bytes in the heap without freeing it. Call to printf in the setup to keep the heap clean
- In Heterodyne mode, temperature measurement every 30s

Version V0.83 05/07/2020
-
- Stereo recording mode
- For PRS, add Stereo/Mono Left/Mon Right parameter
- For PRS, set A2 (left) and A3 (right) and A0 (batteries)
- Automatic adjustment of the number of buffers (pre-trigger) according to the non allocated memory size
- Adding a Copyright menu
- Correction heterodyne in reading which does not work any more since V0.81
- Adding a visualization while formatting an SD card
- Addition of a parameter to specify the type of microphone used
- Addition of a specific linear filter for the microphone ICS40730 at 384 and 250kHz
- Correction of the crash in playing when there is no file

Version V0.82 03/06/2020
-
- Use SdFatSdioEX for fast write and more stability with a full SD card
- AR, reader mode, limitation to 2000 files if SD card full but without guarantee to view the most recent
- SD card test, writing time display in µs if less than 1 ms
- Added formatting of SD card
- Correction micro test on PR
- Begin stereo mode

Version V0.81 04/05/2020
-
- Correction of a bug of reading/writing the digital gain in an ini file
- Batcorder mode counter, set to 0 only on power-up
- Correction of a random memory bug in Heterodyne mode
- Reader, update of the reading time according to the reading mode (X1, X10 or heterodyne)
- In user level mode "Debug" allows traces of debug
- Correction on the use of the Debug user level
- On DMA error, storage in the log and display in heterodyne mode
- Add CGenericMode::PrePrint to print heterodyne frequency before hour
- Reset the recording buffer before starting the acquisition
- Add Restitution::restart to stop/start restitution in heterodyne when restitution goes too fast to avoid DMA error

Version V0.80 15/04/2020
-
- Add Batcorder type file name mode
- Passage of heterodyne buffers to 4096 instead of 2048 for more stability (acquisition delay)
- Storage of parameters on modification in heterodyne mode
- Addition of user level management (Beginner, Expert, Debug)
- Addition of 5 user profiles
- Possibility to create and read a Profiles.ini files of all the parameters of the 5 profiles
- Correction of a bug on the display of the lines at the end of modification of a parameters
- Player, if no file, print 0.0k on sampling frequency
- Correction bug of start of recording on road and pedestrian protocols

Version V0.75 30/03/2020
-
- Adding Timed Recording Mode
- In microphone test, without low pass filter

Version V0.74 20/03/2020
-
- Correction of the bug of return to 48 kHz sampling frequency on restart

Version V0.73 16/03/2020
-
- Restoring operator parameters after a micro test
- Correction of a difference in calculation of the heterodyne frequency between the heterodyne and read modes
- On heterodyne, adjustment of the decimation by 10 with the FIR buffer of 128
- Heterodyne 384kHz, decimation by 16 (24kHz)
- Stop heterodyne restitution if buffer samples not yet available
- Addition of a parameter for the level of the heterodyne signal
- Add audio recorder mode for Active Recorder with a specific sampling frequency and band of interest
- Numeric gain to 0dB on Audio recorder
- Corrects heterodyne frequency management in auto
- Application of digital gain only on samples of wav files
- Code optimization for heterodyne processing at 384kHz
- Correction bug of modification of the parameters in microphone test
- Read mode, improved display of sampling frequency and heterodyne frequency
 
Version V0.72 04/12/2019 (RhinoLogger test mode)
-
- RhinoLogger mode, add a parameter to keep the display permanently
- Fixed management of the interest band in RhinoLogger mode
- Fixed memory overflow in CFCAnalyzerRL and adding a memory occupancy test for FC type bands
- Remove Crutch to bypass the memory bug of the RhinoLogger mode
- Correction for 128GB SD cards (in SDFat library, SdioTeensy.cpp set BUSY_TIMEOUT_MICROS to 800000)
- Adjust the format of the "SD card" line to take into account 128GB cards
- SD Card Test Mode, size adjustment to 16/32/64/128GO standard sizes
- Adding a parameter for the refresh time of the heterodyne graph
- Fixed minute, hour and day backup in RhinoLogger mode
- Added ability to exit error mode
- Add a message to the log if the SD card is full

Version 0.71 23/09/2019 (RhinoLogger experimental mode)
-
- Fixed wakeup bug with button in the case of reading temperature in sleep mode
- Changing the temperature file backup to avoid writing errors
- No atmospheric pressure, too random measure
- Saving the parameters at the end of each change of a parameter. Use the update function instead of write to optimize the number of writes
- Storing the noise file once and then the device goes back without memorization
- Noise file parameter with confirmation
- Fix a problem initializing the SD card in standby when storing temperatures
- Initialization of the parameters to the default values before reading them in the EEPROM at the start of the program
- Limit frequence sample at 250kHz in hétérodyne if CPU is below 180MHz
- Add parameter to go in bootloader mode in an Active Recorder (reset button not accessible in this model)
- Fix bug impossible to del wav in English language (SD parameter)
- Fix bad initialisation in parameters
- For an Active Recorder, do not refer to the external battery
- Taking into account the current seconds for the return of standby
- Crutch to bypass the memory bug of the RhinoLogger mode when leaving (masked reset for a clean restart)
- Fix buffer overflow in LogFile::AddLog (buffer overflow for long string)
- Test on the EEPROM write error

Version 0.70 09/08/2019 (For tests only)
-
- Adding RhinoLogger mode but not active (bug !)
- Adding reading of SD card files via micro-USB port in parameters mode
- Improvement of the micro test (cursor -120 to -50 instead of -120 to -25 dB)
- Update micro test in 300ms instead of 1s for a faster reaction
- Resume visualization of parameters according to processor frequency, recorder type and recording mode
- Adding the test mode of the SD card
- Adding heterodyne mode in play
- Correcting the name of the first wave file in protocol mode
- One temperature storage file instead of one file per day (Prefix_THLog.csv)
- Memorization of temperature in tenth of a degree
- Memorization of pressure in hPa
- Temperature storage including standby period
- Test the remaining size on the SD card. If size <50MB, end of recordings and error
- Fixed a bug that limited the maximum frequency to 120kHz. The maximum is now 150kHz
- Reading the serial number of the processor board and display in the log and on the start screen
- Taking into account the serial number of the processor card in the files names created
- Possibility of storing a noise measurement file
- Fix bug on protocol mode settings
- Fix a bug on the progress of fixed point protocol mode
  
Version 0.65 17/05/2019
-
- Taking into account the probes without the humidity information
- Migration of the temperature/humidity probe management in the CModeGenericRec class except IsTempSensor in CGenericMode class
- Adding and managing a parameter of the reading period of the temperature/humidity probe (10/3600 second step 10)
- If probe T / H present, information in the log as well as the reading period
- Adding CModeGeneric::PrintRamUsage to monitor memory and storage of the memory information in the log at the start of the recording
- Clarification of the linear filter / low pass
- Using a temporary wave file to speed up the recording start (Open file long time with SD card > 16 GO)
- In Active Recorder, use the same change mode button for recording modes other than heterodyne
- Some hidden parameters in heterodyne mode

Version 0.64 06/05/2019
-
- Resetting the SD card on wake up to allow the SD card to change during sleep
- If MAINTAIN_FREE_CLUSTER_COUNT in SdFatConfig.h, print "SDFat cluster !" in start screen
- Backup temperature / humidity every 10 minutes in place of each wav file
- If invalid temperature or humidity, writing a blank in the memorization file
- Memory optimization between the passive and the active to avoid stack overflow
- Editing .h file comments for Doxygen documentation
- Temperature display in heterodyne mode (if the sensor is present)
- At the end of the sleep mode and switch to parameter mode, operator information to wait in the presence of a large capacity SD card
- If CPU Speed is less than 144MHz, max sample frequency is 250 KHz and linear filter always false
- Delivery of software for 3 processor frequencies: 144MHz for common use, 180MHz for lossless operation at 500kHz and 48MHz for reduced power consumption but limited to 250kHz sampling frequency

Version 0.63 14/04/2019
-
- Fixed English screen saver
- Fixed end of sleep
- Fixed CSDModifier display
- Correction of the memorization of the temperature and hygrometry
- Add CPU Frequency on the Start Screen
- Delivery of two versions, one at 144MHz and the other at 180mHz CPU frequency
- Adding the temperature probe treatment
- Adding LED management for buffering losses
- Remains 30s on the logo display if a key is pressed
- Changing architecture with the use of the ModesModifiers library
- Adding relative or absolute threshold management
- Using the U8g2lib library for screen management
- Management of a screen type SSD1306 or SH1106
- On error mode, checking the battery charge to change the error in case of a big discharge
- Division of the function CRecorder::OnDMA_ISR in several inline functions for more clarity in the code
- Improved launch processing to handle both cases Key_Up = debug mode and Key_Down = screen type change
- Add Heterodyne and Player mode for ActiveRecorder
- Manage bits to now the recorder type
- Changed Executable Name (TeensyRecorder) Now Common to Passive Recoder and Active Recorder

Version 0.53 28/11/2018
-
- Adjustment of Vigie-Chiro protocol parameters according to MNHM recommendations
- Modification of the road protocol to manage up to 20 points and a pause mode
- Add fixed point Vigie-Chiro protocol
- Add log on point management in protocol
- If High pass filter frequency = 0 coefficient = 0.999 (High pass filter 50Hz)
- Text modification "Sample Freq." by "Sample rate"
- Add 500kHz Sample rate
- CPU frequency and SDFat optimization verifications at startup
- Storing recording parameters in LogPR.txt
- Add numeric gain and noise in Auto Record screen and LogPR.txt
- Correct internal names of GAINNUM

Version 0.51 13/10/2018
-
- Correction of some parameter formats
- Correction of 0 on Nb Detections when change Prefix
- Correction High pass filter to apply filter on FFT samples

Version 0.50 12/10/2018 (For tests only)
-
- Add French and English language, parameter "Language: English/Langue : Francais"
- Modification 1st parameter : "Go back to record.../Retour enregistre..."
- Down and Up key active if held pressed
- Changing the « Filtre » setting by "Linear filter/Filtre lineaire"
- Added variable high pass filter (Thanks to Frank Dziock), 0 to 25kHz, 0 no filter "High p. filter 00kHz/Filtre pa.haut 00kHz"
- Wave files name to standard Prefixe-YYYYMMJJ-HHMMSS.wav
- Added protocol abort on "Low" key
- Systematic storage of parameters when leaving the parameter mode, except the operating mode always at "Auto Rec"
- Adding the default setting parameter "Default settings  No/Para. par defaut Non"
- "LED Use" parameter only in debug mode. For Debug mode, held pressed "Up" key and start PR. Red LED blink 5 times and 1 second the last time (4 times without debug)

Version 0.42 14/08/2018
-
- Initial version for prototypes