/* 
 * File:   CModepRecorder.cpp
   PassiveRecorder Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 /*
  * - CModeGenericRec
  * -   CModeAutoRecorder
  * -     CModeProtocolePFixe
  * -     CModeSynchro
  * -   CModeProtocole
  * -     CModeProtocoleRoutier
  * -     CModeProtocolePedestre
  */
#include "SdFat.h"
#include <Wire.h>
#include <U8g2lib.h>
#include "Const.h"
#include "TimeLib.h"
#include "CModeRecorder.h"

// Gestionnaire du fichier LogPR.txt
extern LogFile logPR;

extern "C" volatile uint32_t set_arm_clock(uint32_t frequency);

// Temps d'enregistrement du protocole pédestre 6mn en ms
#define TIMEPROTOPEDESTRE 6*60*1000
//#define TIMEPROTOPEDESTRE 30*1000   // 30s pour les tests

// Chaine d'affichage en fonction de la langue
//                                 12345678901234567890
extern const char *txtErrorWriteSD[];
extern const char *txtErrFullSD[];
extern const char *txtRecordAcq[];
extern const char *txtRecordYes[];
extern const char *txtRecordNo[];
//                                 12345678901234567890
//                                 FE384kHz FL N FPH 20
extern const char *txtRecordSFFLT[];
extern const char *txtRecordSFFLTA[];
//                                 12345678901234567890
//                                 R. T. 16dB 2dt. NG12
extern const char *txtRecordThrR[];
//                                 12345678901234567890
//                                 A.T.-100dB 2dt. NG12
extern const char *txtRecordThrA[];
//                                 12345678901234567890
//                                 Bd. Freq. 20-120kHz
extern const char *txtRecordBFr[];
extern const char *txtRecordBFrA[];
extern const char *txtRecordWSD[];
extern const char *txtRecordNoise[];
//                                 12345678901234567890
extern const char *txtProtRoutier[];
extern const char *txtProtPed[];
extern const char *txtProtFixe[];
extern const char *txtProtParcours[];
extern const char *txtProtParcoursR[];
extern const char *txtProtPoint[];
extern const char *txtProtPointR[];
extern const char *txtProtParcoursP[];
extern const char *txtProtEnCoursR[];
extern const char *txtProtEnCoursP[];
extern const char *txtProtStart[];
extern const char *txtProtRestart[];
extern const char *txtProtStop[];
extern const char *txtProtStopP[];
extern const char *txtProtPause[];
extern const char *txtAbandon[];
extern const char *txtProtEnd[];
extern const char *txtProtClicEnd[];
extern const char *txtProtPFixA[];
extern const char *txtProtPFixB[];
extern const char *txtProtPFixC[];
extern const char *txtProtPFRec[];
extern const char *txtProtPFEnd[];
extern const char *txtProtPFVeille[];
extern const char *txtProtPFStart[];
//                                 Est. AG+20dB, Noise -110dB -110/-108
extern const char *txtGANoise[];
extern const char *txtParameters[];
extern const char *txtProbeTH[];
extern const char *txtNoProbeTH[];
extern const char *txtLogSDPbSize[];
extern const char *txtLogSDErrSize[];
// Chaines des Master ou Slaves
extern const char *txtMasterSlave[MAXLANGUAGE][10];
extern const char *txtBatteries[];
extern const char *txtBatteriesMAX[];
extern const char *txtBatteriesMCP[];

// Gestionnaire de carte SD
extern SdFs sd;

// Screen manager
extern U8G2 *pDisplay;

/*-----------------------------------------------------------------------------
 Classe générique de gestion du mode enregistrement
-----------------------------------------------------------------------------*/

//-------------------------------------------------------------------------
// Constructeur
CModeGenericRec::CModeGenericRec()
{
  pRecorder = NULL;
  iNbSizeSD = 0;
  uiTimeTemp = 0;
}

//-------------------------------------------------------------------------
// Destructeur
CModeGenericRec::~CModeGenericRec()
{
  //Serial.println("CModeGenericRec Destructeur");
  if (pRecorder != NULL)
  {
    //Serial.println("CModeGenericRec delete pRecorder");
    delete pRecorder;
    pRecorder = NULL;
  }
}

//-------------------------------------------------------------------------
//! \brief Creation du gestionnaire d'enregistrement
void CModeGenericRec::CreateRecorder()
{
  //Serial.println("CModeGenericRec new CRecorder");
  if (GetRecorderType() >= PASSIVE_STEREO)
  {
    //Serial.println("CModeGenericRec new CRecorder Stereo");
    pRecorder = new CRecorderS( &paramsOperateur, NBBUFFERRECORD);
  }
  else
  {
    //Serial.println("CModeGenericRec new CRecorder");
    pRecorder = new CRecorder( &paramsOperateur, NBBUFFERRECORD);
  }
}
  
//-------------------------------------------------------------------------
// Début du mode
void CModeGenericRec::BeginMode()
{
  // Test de la taille restante sur la carte SD
  uiTimeTestSD = 0;
  // Si on est en mode protocole ou test micro, on ne mémorise pas le mode
  int iMemoMode = paramsOperateur.iModeRecord;
  // Appel de la méthode de base avec lecture paramètre et iModeRecord toujours à RECAUTO
  CModeGeneric::BeginMode();
  // On restitue le mode réel
  paramsOperateur.iModeRecord = iMemoMode;
#if defined(__IMXRT1062__) // Teensy 4.1
  // Ajustement de la fréquence processeur pour consommer moins
  uint32_t iFCPU = 180000000;
  if (paramsOperateur.iModeRecord == AUDIOREC)
    iFCPU = 150000000;
  else if (GetRecorderType() >= PASSIVE_STEREO and paramsOperateur.iStereoRecMode == SMSTEREO)
    iFCPU = 280000000;
  else
  {
     switch (paramsOperateur.uiFe)
    {
    case FE24KHZ :
    case FE48KHZ :
    case FE96KHZ :
    case FE192KHZ:
    case FE250KHZ: iFCPU = 150000000; break;
    case FE384KHZ: iFCPU = 180000000; break;
    case FE500KHZ: iFCPU = 280000000; break;
    }
  }
  set_arm_clock(iFCPU);
  Serial.printf("setup set_arm_clock(%d) F_CPU_ACTUAL %d, F_BUS_ACTUAL %d\n", iFCPU, F_CPU_ACTUAL, F_BUS_ACTUAL);
#endif
  if (paramsOperateur.bBatcorderLog and paramsOperateur.iModeRecord != RECAUTO)
    // Batcorder file mode only in auto recording
    paramsOperateur.bBatcorderLog = false;
  if (IsTempSensor())
  {
    // Présence d'une sonde de température
    bool bMemo = logPR.GetbConsole();
    LogFile::SetbConsole( true);
    if (paramsOperateur.bBatcorderLog)
    {
      // In Batcorder mode, interval of 15 minutes
      paramsOperateur.iPeriodTemp = 15*60;
    }
    LogFile::AddLog( LLOG, txtProbeTH[commonParams.iLanguage], paramsOperateur.iPeriodTemp);
    logPR.SetbConsole( bMemo);
    cTemp = 100.0;
    humidity = 0.0;
    if (paramsOperateur.iModeRecord != PRHINOLOG)
    {
      LectureTemperature();
      MemoTemperature();
      if (paramsOperateur.bBatcorderLog)
      {
        // In Batcorder mode, interval of 15 minutes
        int iMN = minute();
        if (iMN <= 15)
          uiTimeTemp = millis() + (((15-iMN) * 60) - second()) * 1000;
        else if (iMN <= 30)
          uiTimeTemp = millis() + (((30-iMN) * 60) - second()) * 1000;
        else if (iMN <= 45)
          uiTimeTemp = millis() + (((45-iMN) * 60) - second()) * 1000;
        else
          uiTimeTemp = millis() + (((60-iMN) * 60) - second()) * 1000;
        //Serial.printf("Mode Batcorder, mesure temp dans %ds, minutes courantes %d\n", uiTimeTemp/1000, iMN);
      }
      else
        uiTimeTemp = millis() + (unsigned long)paramsOperateur.iPeriodTemp * 1000;
    }
  }
  else
  {
    // Pas de sonde de température
    uiTimeTemp = 0;
    bool bMemo = LogFile::GetbConsole();
    logPR.SetbConsole( true);
    LogFile::AddLog( LLOG, txtNoProbeTH[commonParams.iLanguage]);
    logPR.SetbConsole( bMemo);
  }
  // Mémo des paramètres d'enregistrement dans le Log
  if (paramsOperateur.iModeRecord != PTSTMICRO and paramsOperateur.iModeRecord != SYNCHRO)
    ParamsToLog();
  // Création de gestionnaire d'enregistrement
  CreateRecorder();
  if (pRecorder != NULL)
  {
    // Init du filtre passe haut
    if (paramsOperateur.uiFe < FE192KHZ)
      ((CRecorder *)pRecorder)->SetFreqHighPass( (int)(paramsOperateur.fFreqFiltreHighPass * 1000));
    else
      ((CRecorder *)pRecorder)->SetFreqHighPass( paramsOperateur.iFreqFiltreHighPass * 1000);
  }
}

//-------------------------------------------------------------------------
// Fin du mode
void CModeGenericRec::EndMode()
{
  // Si Acquisition en cours
  if (pRecorder != NULL and pRecorder->IsStarting())
    // On stoppe l'acquisition
    pRecorder->StopAcquisition();
#if defined(__IMXRT1062__) // Teensy 4.1
  // Init de la fréquence processeur à 150MHz pour consommer moins
  set_arm_clock(150000000);
#endif
  // Appel de la méthode de base
  CModeGeneric::EndMode();
}

//-------------------------------------------------------------------------
// Gestion des tâches de fonds
void CModeGenericRec::OnLoop()
{
  //Serial.println("CModeGenericRec::OnLoop A");
  // Gestion du loop du gestionnaire enregistrement
  if (pRecorder != NULL)
    pRecorder->OnLoop();
  //Serial.println("CModeGenericRec::OnLoop B");
  // Test s'il est temps de mesurer la température
  if (uiTimeTemp != 0 and millis() > uiTimeTemp)
  {
    LectureTemperature();
    MemoTemperature();
    if (pRecorder != NULL and paramsOperateur.bBatcorderLog)
      ((CRecorder *)pRecorder)->AddNewTemperatureBatcorderLogFile( cTemp);
    uiTimeTemp = millis() + (unsigned long)paramsOperateur.iPeriodTemp * 1000;
  }
  //Serial.println("CModeGenericRec::OnLoop C");
  if (millis() > uiTimeTestSD and paramsOperateur.iModeRecord != PTSTMICRO)
  {
    // On teste s'il reste de la place sur la carte SD (>50MO)
    float fSize = CSDModifier::GetSDFreeSpace();
    if (fSize <= 0.05)
    {
      // Warning, possible SD error (freeClusterCount = 0!)
      if (!isSDInit())
      {
        // Trace of the problem
        Serial.printf("SD loss on CModeGenericRec::OnLoop, new sd.begin");
      }
      else
      {
        iNbSizeSD++;
        LogFile::AddLog( LLOG, txtLogSDPbSize[commonParams.iLanguage], iNbSizeSD, fSize);
        if (iNbSizeSD > 3)
        {
          // Plus de place sur la carte SD, on passe en erreur carte pleine
          LogFile::AddLog( LLOG, txtLogSDErrSize[commonParams.iLanguage], fSize);
          CModeError::SetNewMode( MPARAMS, 0); // Pas de reset après 2mn, on reste en mode erreur avec une carte saturée
          CModeError::SetTxtError( (char *)txtErrFullSD[commonParams.iLanguage]);
          iNewMode = MERROR;
        }
      }
    }
    else if (iNbSizeSD > 0)
      iNbSizeSD--;
    if (iNbSizeSD == 0)
      // Prochain test dans 1mn
      uiTimeTestSD = millis() + 60000;
    else
      // Prochain test dans 15s
      uiTimeTestSD = millis() + 15000;
  }
  //Serial.println("CModeGenericRec::OnLoop D");
  // Test si erreur DMA
  if (pRecorder != NULL and pRecorder->IsDMAError())
  {
    // Passage en mode erreur
    CModeError::SetTxtError( pRecorder->GetDMAError());
    iNewMode = MERROR;
  }
  //Serial.println("CModeGenericRec::OnLoop E");
  // Test si erreur écriture SD
  if (pRecorder != NULL and pRecorder->getNbWavefileError() >= 5)
  {
    // Passage en mode erreur
    CModeError::SetTxtError( (char *)txtErrorWriteSD[commonParams.iLanguage]);
    iNewMode = MERROR;
  }
  //Serial.println("CModeGenericRec::OnLoop OK");
}

extern const char *txtDurTop192[MAXLANGUAGE][5];
extern const char *txtDurTop250[MAXLANGUAGE][5];
extern const char *txtDurTop384[MAXLANGUAGE][5];

//-------------------------------------------------------------------------
//! \brief Mémorisation des paramètres dans le log
void CModeGenericRec::ParamsToLog()
{
  char sTempA[200], sTempB[64];
  // Test batteries
  CheckBatteries(MAX_WAITMESURE);
  if (bADCBat)
    LogFile::AddLog( LLOG, txtBatteriesMCP[commonParams.iLanguage], fNivBatLiPo, iNivBatLiPo);
  else if (max17043.isOK())
    LogFile::AddLog( LLOG, txtBatteriesMAX[commonParams.iLanguage], fNivBatLiPo, iNivBatLiPo);
  else
    LogFile::AddLog( LLOG, txtBatteries[commonParams.iLanguage], fNivBatLiPo, iNivBatLiPo);
  strcpy( sTempA, txtParameters[commonParams.iLanguage]);
  if (paramsOperateur.iModeRecord == SYNCHRO)
  {
     strcpy ( sTempB, txtMasterSlave[commonParams.iLanguage][paramsOperateur.iMasterSlave]);
     strcat ( sTempA, sTempB);
     strcat ( sTempA, ", ");
  }
  if (paramsOperateur.iModeRecord == RECAUTO or paramsOperateur.iModeRecord == PROTFIXE or paramsOperateur.iModeRecord == SYNCHRO)
  {
     sprintf( sTempB, txtRecordAcq[commonParams.iLanguage], paramsOperateur.sHDebut, paramsOperateur.sHFin);
     strcat ( sTempA, sTempB);
     strcat ( sTempA, ", ");
  }
  int iFe = 384;
  int iFeOper = paramsOperateur.uiFe;
  if (paramsOperateur.iModeRecord == AUDIOREC)
    paramsOperateur.uiFe = paramsOperateur.uiFeA;
  switch (iFeOper)
  {
  case FE24KHZ : iFe =  24; break;
  case FE48KHZ : iFe =  48; break;
  case FE96KHZ : iFe =  96; break;
  case FE192KHZ: iFe = 192; break;
  case FE250KHZ: iFe = 250; break;
  case FE384KHZ: iFe = 384; break;
  case FE500KHZ: iFe = 500; break;
  }
  if (paramsOperateur.bFiltre)
  {
    if (paramsOperateur.iModeRecord == AUDIOREC or paramsOperateur.uiFe < FE192KHZ)
      sprintf( sTempB, txtRecordSFFLTA[commonParams.iLanguage], iFe, txtRecordYes[commonParams.iLanguage], paramsOperateur.fFreqFiltreHighPass);
    else
      sprintf( sTempB, txtRecordSFFLT[commonParams.iLanguage], iFe, txtRecordYes[commonParams.iLanguage], paramsOperateur.iFreqFiltreHighPass);
  }
  else
  {
    if (paramsOperateur.iModeRecord == AUDIOREC or paramsOperateur.uiFe < FE192KHZ)
      sprintf( sTempB, txtRecordSFFLTA[commonParams.iLanguage], iFe, txtRecordNo[commonParams.iLanguage], paramsOperateur.fFreqFiltreHighPass);
    else
      sprintf( sTempB, txtRecordSFFLT[commonParams.iLanguage], iFe, txtRecordNo[commonParams.iLanguage], paramsOperateur.iFreqFiltreHighPass);
  }
  strcat ( sTempA, sTempB);
  strcat ( sTempA, ", ");
  int iGainN = 0;
  switch (paramsOperateur.uiGainNum)
  {
  case GAIN0dB : iGainN =  0; break;
  case GAIN6dB : iGainN =  6; break;
  case GAIN12dB: iGainN = 12; break;
  case GAIN18dB: iGainN = 18; break;
  case GAIN24dB: iGainN = 24; break;
  };
  if (paramsOperateur.bTypeSeuil == false)
    sprintf( sTempB, txtRecordThrR[commonParams.iLanguage], paramsOperateur.iSeuilDet, paramsOperateur.iNbDetect, iGainN);
  else
    sprintf( sTempB, txtRecordThrA[commonParams.iLanguage], paramsOperateur.iSeuilAbs, paramsOperateur.iNbDetect, iGainN);
  strcat ( sTempA, sTempB);
  strcat ( sTempA, ", ");
  if (paramsOperateur.iModeRecord == AUDIOREC or iFeOper < FE192KHZ)
    sprintf( sTempB, txtRecordBFrA[commonParams.iLanguage], (float)paramsOperateur.uiFreqMinA/1000.0, (float)paramsOperateur.uiFreqMaxA/1000.0);
  else
    sprintf( sTempB, txtRecordBFr[commonParams.iLanguage], paramsOperateur.uiFreqMin/1000, paramsOperateur.uiFreqMax/1000);
  strcat ( sTempA, sTempB);
  strcat ( sTempA, ", ");
  sprintf( sTempB, txtRecordWSD[commonParams.iLanguage], paramsOperateur.uiDurMin, paramsOperateur.uiDurMax, CSDModifier::GetSDPourcentFreeSpace());
  strcat ( sTempA, sTempB);
  // Chaine de type "Wav 30-99s SD 3%", on ajoute un % pour qu'il soit compris par le vsnprintf de AddLog
  strcat ( sTempA, "%");
  if (paramsOperateur.iModeRecord == SYNCHRO)
  {
    char **pTxtDurTop = (char **)txtDurTop384;
    switch (paramsOperateur.uiFe)
    {
    case FE192KHZ: pTxtDurTop = (char **)txtDurTop192; break;
    case FE250KHZ: pTxtDurTop = (char **)txtDurTop250; break;
    case FE384KHZ: pTxtDurTop = (char **)txtDurTop384; break;
    }
    // Ajout des paramètres de synchro
    sprintf( sTempB, ", Top %dkHz%s %ds", paramsOperateur.iTopAudioFreq, pTxtDurTop[(commonParams.iLanguage*5)+paramsOperateur.iDurTop],  paramsOperateur.iTopPer);
    strcat ( sTempA, sTempB);
  }
  LogFile::AddLog( LLOG, sTempA);
}

/*-----------------------------------------------------------------------------
 Classe de gestion du mode enregistrement automatique
-----------------------------------------------------------------------------*/

//-------------------------------------------------------------------------
// Constructeur
CModeAutoRecorder::CModeAutoRecorder()
{
}

const char txtRamUsage[]          = "Ram after CModeAutoRecorder::BeginMode";
const char txtRamUsageTstMicro[]  = "Ram after  CModeTestMicro::BeginMode";
const char txtRamUsageProtocole[] = "Ram after  CModeProtocole::BeginMode";
//-------------------------------------------------------------------------
// Début du mode
void CModeAutoRecorder::BeginMode()
{
  //if (CModeGeneric::bDebug)
  //  Serial.println("CModeAutoRecorder::BeginMode");
  // On force le prochain affichage
  bRedraw = true;
  iCurrentSecond = 61;
  // Pour un affichage des caractéristiques d'acquisition pendant 15s
  bAffInfos = false;
  iDecompteurAffAcq = TIMEATTENTEAFF;
  if (commonParams.iUtilLed != LEDNOISE and commonParams.iUtilLed != NOLED and commonParams.iUtilLed != LEDRECORD)
  {
    for (int i = 0; i < 2; i++)
    {
        digitalWriteFast(LED_BUILTIN, HIGH);
        delay(200);
        digitalWriteFast(LED_BUILTIN, LOW);
        delay(200);
    }
  }
  //Serial.println("&&& Avant appel CModeGenericRec::BeginMode");
  //delay(4000);
  // Appel de la méthode de base
  CModeGenericRec::BeginMode();
  //Serial.println("&&& Après appel CModeGenericRec::BeginMode");
  //delay(4000);
  // Print Ram usage
  //PrintRamUsage( (char *)txtRamUsage);
  //bool bMemo = LogFile::GetbConsole();
  //logPR.SetbConsole( true);
  //LogFile::AddLog( LLOG, "Size of Record buffer %d bytes (int16_t * MAXSAMPLE %d * Nb Buffer %d)", pRecorder->GetSizeoffSamplesRecordBuffer(), MAXSAMPLE, pRecorder->GetiNbSampleBuffer());
  //logPR.SetbConsole( bMemo);
  if (pRecorder != NULL and paramsOperateur.iModeRecord != SYNCHRO)
    // On lance l'acquisition
    pRecorder->StartAcquisition();
#ifdef CHECKBAT
  uiTimeCheck = 0;
#endif
}

//-------------------------------------------------------------------------
// Fin du mode
void CModeAutoRecorder::EndMode()
{
  // Appel de la méthode de base qui stoppe l'acquisition
  CModeGenericRec::EndMode();
  //if (CModeGeneric::bDebug)
  //  Serial.println("CModeAutoRecorder::EndMode");
}

//-------------------------------------------------------------------------
// Affichage du mode sur l'écran
// Cette méthode est appelée régulièrement par le loop principal
// à charge des différents modes d'afficher les informations nécessaires
void CModeAutoRecorder::PrintMode()
{
  char Message[30];
  if (iDecompteurAffAcq > 0 and iCurrentSecond != second())
  {
    iCurrentSecond = second();
    if (iDecompteurAffAcq == 1)
    {
      // On suspend l'acquisition le temps d'afficher (sinon, perturbation de l'écran)
      if (pRecorder != NULL)
        pRecorder->SetAcquisition( false);
      // Effacement écran
      pDisplay->clearBuffer();
      pDisplay->sendBuffer();
      // On restaure l'acquisition
      if (pRecorder != NULL)
        pRecorder->SetAcquisition( true);
      iDecompteurAffAcq = 0;
    }
    else if (!bAffInfos and pRecorder != NULL and pRecorder->IsNoiseOK())
    {
      // On suspend l'acquisition le temps d'afficher (sinon, perturbation de l'écran)
      pRecorder->SetAcquisition( false);
      // Affichage des caractéristiques de l'acquisition
      bAffInfos = true;
      pDisplay->clearBuffer();
      pDisplay->setCursor(0,0);
      //                 12345678901234567890
      //                 Acquisi. 21:00-02:00
      sprintf( Message, txtRecordAcq[commonParams.iLanguage], paramsOperateur.sHDebut, paramsOperateur.sHFin);
      pDisplay->println(Message);
      pDisplay->setCursor(0,9);
      //                   12345678901234567890
      //                   Fe 384kHz Filtre N
      int iFe = 384;
      switch (paramsOperateur.uiFe)
      {
      case FE24KHZ : iFe =  24; break;
      case FE48KHZ : iFe =  48; break;
      case FE96KHZ : iFe =  96; break;
      case FE192KHZ: iFe = 192; break;
      case FE250KHZ: iFe = 250; break;
      case FE384KHZ: iFe = 384; break;
      case FE500KHZ: iFe = 500; break;
      }
      if (paramsOperateur.bFiltre)
      {
        if (paramsOperateur.uiFe >= FE192KHZ)
          sprintf( Message, txtRecordSFFLT[commonParams.iLanguage], iFe, txtRecordYes[commonParams.iLanguage], paramsOperateur.iFreqFiltreHighPass);
        else
          sprintf( Message, txtRecordSFFLTA[commonParams.iLanguage], iFe, txtRecordYes[commonParams.iLanguage], paramsOperateur.fFreqFiltreHighPass);
      }
      else
      {
        if (paramsOperateur.uiFe >= FE192KHZ)
          sprintf( Message, txtRecordSFFLT[commonParams.iLanguage], iFe, txtRecordNo[commonParams.iLanguage], paramsOperateur.iFreqFiltreHighPass);
        else
          sprintf( Message, txtRecordSFFLTA[commonParams.iLanguage], iFe, txtRecordNo[commonParams.iLanguage], paramsOperateur.fFreqFiltreHighPass);
      }
      pDisplay->println(Message);
      pDisplay->setCursor(0,18);
      int iGainN = 0;
      switch (paramsOperateur.uiGainNum)
      {
      case GAIN0dB : iGainN =  0; break;
      case GAIN6dB : iGainN =  6; break;
      case GAIN12dB: iGainN = 12; break;
      case GAIN18dB: iGainN = 18; break;
      case GAIN24dB: iGainN = 24; break;
      };
      if (paramsOperateur.bTypeSeuil == false)
        //                 12345678901234567890
        //                 S. R. 20dB 3dt. GN12
        //                 R. T. 20dB 3dt. NG12
        sprintf( Message, txtRecordThrR[commonParams.iLanguage], paramsOperateur.iSeuilDet, paramsOperateur.iNbDetect, iGainN);
      else
        //                 12345678901234567890
        //                 S.A.-100dB 3dt. GN12
        //                 A.T.-100dB 3dt. NG12
        sprintf( Message, txtRecordThrA[commonParams.iLanguage], paramsOperateur.iSeuilAbs, paramsOperateur.iNbDetect, iGainN);
      pDisplay->println(Message);
      pDisplay->setCursor(0,27);
      //                 12345678901234567890
      //                 Bd. Freq. 20-120kHz
      if (paramsOperateur.iModeRecord == AUDIOREC or paramsOperateur.uiFe < FE192KHZ)
        sprintf( Message, txtRecordBFrA[commonParams.iLanguage], (float)paramsOperateur.uiFreqMinA/1000.0, (float)paramsOperateur.uiFreqMaxA/1000.0);
      else
        sprintf( Message, txtRecordBFr[commonParams.iLanguage], paramsOperateur.uiFreqMin/1000, paramsOperateur.uiFreqMax/1000);
      pDisplay->println(Message);
      pDisplay->setCursor(0,36);
      //                 12345678901234567890
      //                 Wav 10-100s SD15.0GO
      CSDModifier::GetSDPourcentFreeSpace();
      sprintf( Message, txtRecordWSD[commonParams.iLanguage], paramsOperateur.uiDurMin, paramsOperateur.uiDurMax, CSDModifier::GetSDPourcentFreeSpace());
      pDisplay->println(Message);
      pDisplay->setCursor(0,45);
      //                 12345678901234567890
      //                 Bruit -100dB-98/-97
      int iNoise    = pRecorder->GetNoise();
      int iNoiseMin = pRecorder->GetMinNoise();
      int iNoiseMax = pRecorder->GetMaxNoise();
      sprintf( Message, txtRecordNoise[commonParams.iLanguage], iNoise, iNoiseMin, iNoiseMax);
      pDisplay->println(Message);
      pDisplay->setCursor(0,54);
      /*//                 12345678901234567890
      //                 0 pic max 125kHz
      int iNbPics = pRecorder->GetNbPics();
      int iFrMax  = pRecorder->GetFreqMaxNoise();
      if (iNbPics > 1)
        sprintf( Message, "%d pics max %dkHz", iNbPics, iFrMax);
      else
        sprintf( Message, "%d pic max %dkHz", iNbPics, iFrMax);*/
      //                 12345678901234567890
      //                 22/07/2018  -  23:58
      sprintf( Message, "%02d/%02d/%04d  -  %02d:%02d", day(), month(), year(), hour(), minute());
      pDisplay->println(Message);
      pDisplay->sendBuffer();
      // On mémorise le fichier des infos de bruit
      //pRecorder->MemoFileNoise();
      // On mémorise le bruit dans le Log
      LogFile::AddLog( LLOG, txtGANoise[commonParams.iLanguage], iNoise, iNoiseMin, iNoiseMax);
      // On restaure l'acquisition
      pRecorder->SetAcquisition( true);
    }
    if (iDecompteurAffAcq > 0)
      iDecompteurAffAcq--;
  }
#ifdef CHECKBAT
  if (millis() > uiTimeCheck)
  {
    if (max17043.isOK() and !bMAXPowerOn)
    {
      // Power On MAX and wait 125ms for mesure
      CheckBatteries(MAX_POWER);
      uiTimeCheck = millis() + 125;
    }
    else
    {
      if (max17043.isOK())
        // Test battery
        CheckBatteries(MAX_MESURE);
      else
        // Test battery
        CheckBatteries(MAX_WAITMESURE);
      LogFile::AddLog( LLOG, txtBatteries[commonParams.iLanguage], fNivBatLiPo, iNivBatLiPo);
      // Next time for check batteries
      uiTimeCheck = millis() + 5*60*1000;
    }
  }
#endif
}

//-------------------------------------------------------------------------
// Traitement des ordres claviers
// Si la touche est une touche de changement de mode, retourne le mode demandé
// Cette méthode est appelée régulièrement par le loop principal
// à charge des différents modes de traiter les actions opérateurs
int CModeAutoRecorder::KeyManager(
  unsigned short key  // Touche sollicitée par l'opérateur
  )
{
  // Gestion éventuelle des touches
  int newMode = NOMODE;
  if (key == K_PUSH or key == K_MODEA)
  {
    if (iDecompteurAffAcq <= 0)
    {
      // Demande d'affichage momentané des caractéristiques de l'enregistrement
      iDecompteurAffAcq = TIMEATTENTEAFF;
      bAffInfos = false;
    }
    else
    {
      if (GetRecorderType() == ACTIVE_RECORDER)
        // In Active Recorder K_MODEA change mode : Record mode -> Player -> Parameters -> Record mode
        iNewMode = MPLAYER;
      else
        // In other type go to parameters
        iNewMode = MPARAMS;
    }
  }
  // Test si temps de passer en veille
  if (IsTimeVeille())
    iNewMode = MVEILLE;
  if (iNewMode != NOMODE)
  {
    newMode  = iNewMode;
    iNewMode = NOMODE;
  }
  return newMode;
}

/*-----------------------------------------------------------------------------
 Classe générique de gestion des modes protocole
-----------------------------------------------------------------------------*/
//-------------------------------------------------------------------------
// Constructeur
CModeProtocole::CModeProtocole()
{
  bToStartRecord = false;
}

//-------------------------------------------------------------------------
// Début du mode
void CModeProtocole::BeginMode()
{
  // Appel de la méthode de base
  CModeGenericRec::BeginMode();
  // Print Ram usage
  PrintRamUsage( (char *)txtRamUsageProtocole);
  bool bMemo = LogFile::GetbConsole();
  logPR.SetbConsole( true);
  if (pRecorder != NULL)
    LogFile::AddLog( LLOG, "Size of Record buffer %d bytes (int16_t * MAXSAMPLE %d * Nb Buffer %d)", ((CRecorder *)pRecorder)->GetSizeoffSamplesRecordBuffer(), MAXSAMPLE, ((CRecorder *)pRecorder)->GetiNbSampleBuffer());
  logPR.SetbConsole( bMemo);
  // Point 1 pour commencer
  iPointProto = 1;
  bDoPrintProtocole = true;
  bToStartRecord = false;
  iEtatProto = PROT_STOP;
  uiTimeRec = 0;
  iCurMs = 0;
}

//-------------------------------------------------------------------------
// Fin du mode
void CModeProtocole::EndMode()
{
  // On restitue les paramètres opérateur et on passe hors protocole
  CModeGenericRec::ReadParams();
  // Appel de la méthode de base
  CModeGenericRec::EndMode();
}

//-------------------------------------------------------------------------
// Reading saved settings
// return true if OK
bool CModeProtocole::ReadParams()
{
  // Do nothing
  return true;
}
  
//-------------------------------------------------------------------------
// Affichage du mode sur l'écran
// Cette méthode est appelée régulièrement par le loop principal
// à charge des différents modes d'afficher les informations nécessaires
void CModeProtocole::PrintMode()
{
  if (bDoPrintProtocole)
  {
    bDoPrintProtocole = false;
    iOldEtatProto = iEtatProto;
    // Selon le mode 
    switch (iEtatProto)
    {
    case PROT_STOP : PrintPotocolStop();    break;
    case PROT_END  : PrintProtocolEnd();    break;
    case PROT_PAUSE: PrintProtocolPause();  break;
    case PROT_REC  :
      bDoPrintProtocole = true;
      if (millis() > iCurMs)
      {
        iCurMs = millis() + 500;
        PrintProtocolRecord();
      }
      break;
    };
    if (iEtatProto == PROT_REC and pRecorder != NULL and !pRecorder->IsStarting())
    {
      //LogFile::AddLog( LLOG, "CModeProtocole::PrintMode StartAcquisition");
      // On lance l'acquisition
      uiTimeRec = millis();
      pRecorder->StartAcquisition();
      // Puis on demande le lancement de l'enregistrement si ce n'est pas un protocole point fixe (enregistrement sur détection d'activité)
      if (paramsOperateur.iModeRecord != PROTFIXE)
        bToStartRecord = true;
    }
  }
  if (bToStartRecord and pRecorder != NULL and pRecorder->IsNoiseOK())
  {
    // Démarrage effectif de l'enregistrement après calcul du bruit OK
    delay(150);
    bToStartRecord = false;
    pRecorder->StartRecording();
  }
}

//-------------------------------------------------------------------------
// Affichage du protocole en état stop
void CModeProtocole::PrintPotocolStop()
{
  // De base, ne fait rien
}

//-------------------------------------------------------------------------
// Affichage du protocole en état enregistrement
void CModeProtocole::PrintProtocolRecord()
{
  // De base, ne fait rien
}

//-------------------------------------------------------------------------
// Affichage du protocole en état Fin
void CModeProtocole::PrintProtocolEnd()
{
  // De base, ne fait rien
}

//-------------------------------------------------------------------------
// Affichage du protocole en état pause
void CModeProtocole::PrintProtocolPause()
{
  // De base, ne fait rien
}

// Textes des actions sur les protocoles pour le Log
//                                    123456789012345678901
const char *txtLogStartPt[]       = {"Start point %d",
                                     "Départ point %d"};
const char *txtLogRestartPt[]     = {"Restart point %d",
                                     "Relance point %d"};
const char *txtLogStopPt[]        = {"Stop point %d",
                                     "Fin point %d"};
const char *txtLogStopOpPt[]      = {"Operator Stop point %d",
                                     "Fin point %d par opérateur"};
const char *txtLogPausePt[]       = {"Break point %d",
                                     "Pause point %d"};
const char *txtLogAbandonProt[]   = {"Protocol abort",
                                     "Abandon protocole"};

//-------------------------------------------------------------------------
// Traitement des ordres claviers
// Si la touche est une touche de changement de mode, retourne le mode demandé
// Cette méthode est appelée régulièrement par le loop principal
// à charge des différents modes de traiter les actions opérateurs
int CModeProtocole::KeyManager(
  unsigned short key  // Touche sollicitée par l'opérateur
  )
{
  // Gestion arret enregistrement sur protocole pédestre
  bool bTimeout = false;
  if (key == K_NO and idxCurrentMode == MPROPED and pRecorder != NULL and pRecorder->IsStarting() and millis() > uiTimeRec+TIMEPROTOPEDESTRE)
  {
    // Temps écoulé, on passe au point suivant en simulant une touche K_DOWN
    bTimeout = true;
    key = K_DOWN;
  }
  
  if (key != K_NO)
  {
    // Selon le mode 
    switch (iEtatProto)
    {
    case PROT_STOP :
      if (key == K_PUSH)
      {
        // On passe en enregistrement d'un point
        LogFile::AddLog( LLOG, txtLogStartPt[commonParams.iLanguage], iPointProto);
        // Le départ en enregistrement se faisant lors de l'affichage
        iEtatProto = PROT_REC;
        bDoPrintProtocole = true;
      }
      else if (key == K_DOWN)
      {
        // Abandon du protocole, passage en mode fin
        if (pRecorder != NULL and pRecorder->IsRecording())
          // On stoppe l'enregistrement
          pRecorder->StopRecording();
        if (pRecorder != NULL and pRecorder->IsStarting())
          // On stoppe l'acquisition
          pRecorder->StopAcquisition();
        iEtatProto = PROT_END;
        bDoPrintProtocole = true;
        LogFile::AddLog( LLOG, txtLogAbandonProt[commonParams.iLanguage]);
      }
      break;
    case PROT_REC  :
      if (   (key == K_PUSH and idxCurrentMode == MPROROU)
          or (key == K_DOWN and idxCurrentMode == MPROPED))
      {
        // Fin d'enregistrement d'un point
        if (pRecorder != NULL and pRecorder->IsRecording())
          // On stoppe l'enregistrement
          pRecorder->StopRecording();
        if (pRecorder != NULL and pRecorder->IsStarting())
          // On stoppe l'acquisition
          pRecorder->StopAcquisition();
        if (bTimeout or idxCurrentMode == MPROROU)
          LogFile::AddLog( LLOG, txtLogStopPt[commonParams.iLanguage], iPointProto);
        else
          LogFile::AddLog( LLOG, txtLogStopOpPt[commonParams.iLanguage], iPointProto);
        // On passe au point suivant
        iPointProto++;
        // On initialise le préfixe
        char sTempPrefixe[MAXPREFIXE*2];
        if (idxCurrentMode == MPROROU)
          sprintf( sTempPrefixe, "PR%02d_", iPointProto);
        else
          sprintf( sTempPrefixe, "PP%02d_", iPointProto);
        if (strlen(sTempPrefixe) < MAXPREFIXE)
          strcpy( paramsOperateur.sPrefixe, sTempPrefixe);
        else
        {
          strncpy( paramsOperateur.sPrefixe, sTempPrefixe, MAXPREFIXE);
          paramsOperateur.sPrefixe[MAXPREFIXE] = 0;
        }
        // On force l'affichage et l'attente du point suivant ou fin du protocole
        if (iPointProto > iMaxPoint)
          iEtatProto = PROT_END;
        else
          iEtatProto = PROT_STOP;
        bDoPrintProtocole = true;
      }
      else if (key == K_UP and idxCurrentMode == MPROROU)
      {
        // Pause du point du protocole
        if (pRecorder != NULL and pRecorder->IsRecording())
          // On stoppe l'enregistrement
          pRecorder->StopRecording();
        if (pRecorder != NULL and pRecorder->IsStarting())
          pRecorder->StopAcquisition();
        iEtatProto = PROT_PAUSE;
        bDoPrintProtocole = true;
        LogFile::AddLog( LLOG, txtLogPausePt[commonParams.iLanguage], iPointProto);
      }
      break;
    case PROT_END  :
      if (key == K_PUSH or key == K_MODEA)
      {
        if (GetRecorderType() == ACTIVE_RECORDER)
          // In Active Recorder K_MODEA change mode : Record mode -> Player -> Parameters -> Record mode
          iNewMode = MPLAYER;
        else
          // Always go to parameters
          iNewMode = MPARAMS;
      }
      break;
    case PROT_PAUSE:
      if (key == K_PUSH)
      {
        // On repasse en enregistrement d'un point
        LogFile::AddLog( LLOG, txtLogRestartPt[commonParams.iLanguage], iPointProto);
        // Le départ en enregistrement se faisant lors de l'affichage
        iEtatProto = PROT_REC;
        bDoPrintProtocole = true;
      }
      else if (key == K_DOWN)
      {
        // Fin du point
        if (pRecorder != NULL and pRecorder->IsRecording())
          // On stoppe l'enregistrement
          pRecorder->StopRecording();
        if (pRecorder != NULL and pRecorder->IsStarting())
          pRecorder->StopAcquisition();
        LogFile::AddLog( LLOG, txtLogStopPt[commonParams.iLanguage], iPointProto);
        // On passe au point suivant
        iPointProto++;
        // On force l'affichage et l'attente du point suivant ou fin du protocole
        if (iPointProto > iMaxPoint)
          iEtatProto = PROT_END;
        else
          iEtatProto = PROT_STOP;
        bDoPrintProtocole = true;
      }
      break;
    default:
      Serial.println("CModeProtocole::KeyManager default !!!");
    }
  }
  int newMode = NOMODE;
  if (iNewMode != NOMODE)
  {
    newMode  = iNewMode;
    iNewMode = NOMODE;
  }
  return newMode;
}

/*-----------------------------------------------------------------------------
 Classe de gestion du mode enregistrement protocole routier
-----------------------------------------------------------------------------*/

//-------------------------------------------------------------------------
// Constructeur
CModeProtocoleRoutier::CModeProtocoleRoutier()
{
}

//-------------------------------------------------------------------------
// Début du mode
void CModeProtocoleRoutier::BeginMode()
{
  if (CModeGeneric::bDebug)
    Serial.println("CModeProtocoleRoutier::BeginMode");
  // Lecture des paramètres
  CGenericMode::ReadParams();
  // Mode protocole, on impose certains paramètres
  paramsOperateur.uiDurMin   = 720;                       // 12 mn max pour le protocole routier (normalement stoppé avant par l'opérateur au environ de 6mn)
  paramsOperateur.uiDurMax   = paramsOperateur.uiDurMin;  // Durée min = durée max
  paramsOperateur.uiFreqMin  =   8000;                    // Fréquence minimale d'intérêt en kHz
  paramsOperateur.uiFreqMax  = 120000;                    // Frequence maximale d'intérêt en kHz
  paramsOperateur.uiFe       = FE384KHZ;                  // On impose 384kHz
  paramsOperateur.bTypeSeuil = false;                     // Seuil relatif
  paramsOperateur.iSeuilDet  = 90;                        // Seuil élevé pour ne pas enregistrer automatiquement
  paramsOperateur.uiGainNum  = GAIN0dB;                   // Pas de gain numérique
  paramsOperateur.bFiltre    = false;                     // Pas de filtre
  paramsOperateur.bExp10     = true;                      // Expension de temps X10
  paramsOperateur.iNbDetect  = 1;                         // Nb détection à 1 pour enregistrer en permanence
  paramsOperateur.iFreqFiltreHighPass  = 0;               // Pas de filtre passe haut
  iMaxPoint = 20;
  // Appel de la méthode de base sans lecture des paramètres
  bReadParams = false;
  CModeProtocole::BeginMode();
  char sTempPrefixe[MAXPREFIXE*2];
  sprintf( sTempPrefixe, "PR%02d_", iPointProto);  // Préfixe en mode protocole routier
  if (strlen(sTempPrefixe) < MAXPREFIXE)
    strcpy( paramsOperateur.sPrefixe, sTempPrefixe);
  else
  {
    strncpy( paramsOperateur.sPrefixe, sTempPrefixe, MAXPREFIXE);
    paramsOperateur.sPrefixe[MAXPREFIXE] = 0;
  }
}

//-------------------------------------------------------------------------
// Fin du mode
void CModeProtocoleRoutier::EndMode()
{
  // Appel de la méthode de base qui stoppe l'acquisition
  CModeProtocole::EndMode();
  if (CModeGeneric::bDebug)
    Serial.println("CModeProtocoleRoutier::EndMode");
}

//-------------------------------------------------------------------------
// Affichage du protocole en état attente départ d'un point
//       12345678901234567890
// 1(00) Protocole Routier
// 2(09) Parcours 1
// 3(19) 
// 4(27)
// 5(36) Clic pour lancer
// 6(45)
// 7(54) Bas abandon protoc.
void CModeProtocoleRoutier::PrintPotocolStop()
{
  char Message[25];
  pDisplay->clearBuffer();
  pDisplay->setCursor(0,0);
  pDisplay->println(    txtProtRoutier[commonParams.iLanguage]);
  pDisplay->setCursor(0,9);
  sprintf( Message,   txtProtParcours[commonParams.iLanguage], iPointProto);
  pDisplay->println( Message);
  pDisplay->setCursor(0,36);
  pDisplay->println(    txtProtStart[commonParams.iLanguage]);
  pDisplay->setCursor(0,54);
  pDisplay->println(    txtAbandon[commonParams.iLanguage]);
  pDisplay->sendBuffer();
}

//-------------------------------------------------------------------------
// Affichage du protocole en état enregistrement d'un point
//       12345678901234567890
// 1(00) Protocole Routier
// 2(09) Parcours 1...
// 3(19) en cours
// 4(27)
// 5(36) Clic pour stopper
// 6(45) Haut pour pause
// 7(54)
void CModeProtocoleRoutier::PrintProtocolRecord()
{
  char Message[25];
  // Calcul durée enregistrement
  uint32_t uiTime;
  int iMn = 0, iSec = 0;
  if (uiTimeRec != 0)
  {
    uiTime = (millis() - uiTimeRec) / 1000;
    iMn  = uiTime / 60;
    iSec = uiTime - (iMn * 60);
  }
  pDisplay->clearBuffer();
  pDisplay->setCursor(0,0);
  pDisplay->println(    txtProtRoutier[commonParams.iLanguage]);
  pDisplay->setCursor(0,9);
  sprintf( Message,   txtProtParcoursR[commonParams.iLanguage], iPointProto, iMn, iSec);
  pDisplay->println( Message);
  pDisplay->setCursor(0,19);
  pDisplay->println(    txtProtEnCoursR[commonParams.iLanguage]);
  pDisplay->setCursor(0,36);
  pDisplay->println(    txtProtStop[commonParams.iLanguage]);
  pDisplay->setCursor(0,45);
  pDisplay->println(    txtProtPause[commonParams.iLanguage]);
  pDisplay->sendBuffer();
}

//-------------------------------------------------------------------------
// Affichage du protocole en état Fin du Protocole
//       12345678901234567890
// 1(00) Protocole Routier
// 2(09) Fin du protocol
// 3(19) 
// 4(27)
// 5(36) Clic pour terminer
// 6(45) 
// 7(54) 
void CModeProtocoleRoutier::PrintProtocolEnd()
{
  pDisplay->clearBuffer();
  pDisplay->setCursor(0,0);
  pDisplay->println(    txtProtRoutier[commonParams.iLanguage]);
  pDisplay->setCursor(0,9);
  pDisplay->println(    txtProtEnd[commonParams.iLanguage]);
  pDisplay->setCursor(0,36);
  pDisplay->println(    txtProtClicEnd[commonParams.iLanguage]);
  pDisplay->sendBuffer();
}

//-------------------------------------------------------------------------
// Affichage du protocole en état pause
//       12345678901234567890
// 1(00) Protocole Routier
// 2(09) Pause parcours 01 -
// 3(19) 
// 4(27)
// 5(36) Clic pour relancer
// 6(45)
// 7(54) Bas stop point
void CModeProtocoleRoutier::PrintProtocolPause()
{
  char Message[25];
  pDisplay->clearBuffer();
  pDisplay->setCursor(0,0);
  pDisplay->println(    txtProtRoutier[commonParams.iLanguage]);
  pDisplay->setCursor(0,9);
  sprintf( Message,   txtProtParcoursP[commonParams.iLanguage], iPointProto);
  pDisplay->println( Message);
  pDisplay->setCursor(0,36);
  pDisplay->println(    txtProtRestart[commonParams.iLanguage]);
  pDisplay->setCursor(0,54);
  pDisplay->println(    txtProtStopP[commonParams.iLanguage]);
  pDisplay->sendBuffer();
}

/*-----------------------------------------------------------------------------
 Classe de gestion du mode enregistrement protocole pédestre
-----------------------------------------------------------------------------*/

//-------------------------------------------------------------------------
// Constructeur
CModeProtocolePedestre::CModeProtocolePedestre()
{
}

//-------------------------------------------------------------------------
// Début du mode
void CModeProtocolePedestre::BeginMode()
{
  if (CModeGeneric::bDebug)
    Serial.println("CModeProtocolePedestre::BeginMode");
  // Lecture des paramètres
  CGenericMode::ReadParams();
  // Mode protocole, on impose certains paramètres
  paramsOperateur.uiDurMin   = 420;                       // Normalement 6 mn pour le protocole pédestre, on met 7mn qui seront stoppées avant
  //paramsOperateur.uiDurMin = 10;                        // 10s pour le debug du protocole pédestre
  paramsOperateur.uiDurMax   = paramsOperateur.uiDurMin;  // Durée min = durée max
  paramsOperateur.uiFreqMin  =   8000;                    // Fréquence minimale d'intérêt en kHz
  paramsOperateur.uiFreqMax  = 120000;                    // Frequence maximale d'intérêt en kHz
  paramsOperateur.uiFe       = FE384KHZ;                  // On impose 384kHz
  paramsOperateur.bTypeSeuil = false;                     // Seuil relatif
  paramsOperateur.iSeuilDet  = 90;                        // Seuil élevé pour ne pas enregistrer automatiquement
  paramsOperateur.uiGainNum  = GAIN0dB;                   // Pas de gain numérique
  paramsOperateur.bFiltre    = false;                     // Pas de filtre
  paramsOperateur.bExp10     = true;                      // Expension de temps X10
  paramsOperateur.iNbDetect  = 1;                         // Nb détection à 1 pour enregistrer en permanence
  paramsOperateur.iFreqFiltreHighPass = 0;                // Pas de filtre passe haut
  iMaxPoint = 15;
  // Appel de la méthode de base sans lecture des paramètres
  bReadParams = false;
  CModeProtocole::BeginMode();
  char sTempPrefixe[MAXPREFIXE*2];
  sprintf( sTempPrefixe, "PP%02d_", iPointProto);  // Préfixe en mode protocole pédestre
  if (strlen(sTempPrefixe) < MAXPREFIXE)
    strcpy( paramsOperateur.sPrefixe, sTempPrefixe);
  else
  {
    strncpy( paramsOperateur.sPrefixe, sTempPrefixe, MAXPREFIXE);
    paramsOperateur.sPrefixe[MAXPREFIXE] = 0;
  }
}

//-------------------------------------------------------------------------
// Fin du mode
void CModeProtocolePedestre::EndMode()
{
  // Appel de la méthode de base qui stoppe l'acquisition
  CModeProtocole::EndMode();
  if (CModeGeneric::bDebug)
    Serial.println("CModeProtocolePedestre::EndMode");
}

//-------------------------------------------------------------------------
// Affichage du protocole en état attente départ d'un point
//       12345678901234567890
// 1(00) Protocole Pedestre
// 2(09) Parcours 1
// 3(19) 
// 4(27)
// 5(36) Clic pour lancer 6mn
// 6(45)
// 7(54) Bas abandon protoc.
void CModeProtocolePedestre::PrintPotocolStop()
{
  char Message[25];
  pDisplay->clearBuffer();
  pDisplay->setCursor(0,0);
  pDisplay->println(    txtProtPed[commonParams.iLanguage]);
  pDisplay->setCursor(0,9);
  sprintf( Message,   txtProtPoint[commonParams.iLanguage], iPointProto);
  pDisplay->println( Message);
  pDisplay->setCursor(0,36);
  pDisplay->println(    txtProtStart[commonParams.iLanguage]);
  pDisplay->setCursor(0,54);
  pDisplay->println(    txtAbandon[commonParams.iLanguage]);
  pDisplay->sendBuffer();
}

//-------------------------------------------------------------------------
// Affichage du protocole en état enregistrement
//       123456789012345678901
// 1(00) Protocole Pedestre
// 2(09) Parcours 1...
// 3(19) en cours (6mn)
// 4(27)
// 5(36)
// 6(45)
// 7(54) Bas abandon point
void CModeProtocolePedestre::PrintProtocolRecord()
{
  char Message[25];
  // Calcul durée enregistrement
  uint32_t uiTime;
  int iMn = 0, iSec = 0;
  if (uiTimeRec != 0)
  {
    uiTime = (millis() - uiTimeRec) / 1000;
    iMn  = uiTime / 60;
    iSec = uiTime - (iMn * 60);
  }
  //Serial.printf("uiTimeRec %d, current %d, uiTime %d, iMn %d, iSec %d\n", uiTimeRec, millis(), uiTime, iMn, iSec);
  pDisplay->clearBuffer();
  pDisplay->setCursor(0,0);
  pDisplay->println(    txtProtPed[commonParams.iLanguage]);
  pDisplay->setCursor(0,9);
  sprintf( Message,   txtProtPointR[commonParams.iLanguage], iPointProto, iMn, iSec);
  pDisplay->println( Message);
  pDisplay->setCursor(0,19);
  pDisplay->println(    txtProtEnCoursP[commonParams.iLanguage]);
  pDisplay->setCursor(0,54);
  pDisplay->println(    txtProtStopP[commonParams.iLanguage]);
  pDisplay->sendBuffer();
}

//-------------------------------------------------------------------------
// Affichage du protocole en état Fin du protocole
//       123456789012345678901
// 1(00) Protocole Pedestre
// 2(09) Fin du protocole
// 3(19) 
// 4(27)
// 5(36) Clic pour terminer
// 6(45) 
// 7(54) 
void CModeProtocolePedestre::PrintProtocolEnd()
{
  pDisplay->clearBuffer();
  pDisplay->setCursor(0,0);
  pDisplay->println(    txtProtPed[commonParams.iLanguage]);
  pDisplay->setCursor(0,9);
  pDisplay->println(    txtProtEnd[commonParams.iLanguage]);
  pDisplay->setCursor(0,36);
  pDisplay->println(    txtProtClicEnd[commonParams.iLanguage]);
  pDisplay->sendBuffer();
}

/*-----------------------------------------------------------------------------
 Classe de gestion du mode enregistrement protocole point fixe (mode enregistrement auto avec paramètres prédéfinies)
-----------------------------------------------------------------------------*/

//-------------------------------------------------------------------------
// Constructeur
CModeProtocolePFixe::CModeProtocolePFixe()
{
}

//-------------------------------------------------------------------------
// Début du mode
void CModeProtocolePFixe::BeginMode()
{
  //if (CModeGeneric::bDebug)
  //  Serial.println("CModeProtocolePFixe::BeginMode");
  // Lecture des paramètres
  CGenericMode::ReadParams();
  // Mode protocole, on impose certains paramètres
  paramsOperateur.uiDurMin   = 2;                         // 2s
  paramsOperateur.uiDurMax   = 30;                        // 30s
  paramsOperateur.uiFreqMin  =   8000;                    // Fréquence minimale d'intérêt en kHz
  paramsOperateur.uiFreqMax  = 120000;                    // Frequence maximale d'intérêt en kHz
  paramsOperateur.uiFe       = FE384KHZ;                  // On impose 384kHz
  paramsOperateur.bTypeSeuil = false;                     // Seuil relatif
  paramsOperateur.iSeuilDet  = 16;                        // Seuil à 16dB
  paramsOperateur.uiGainNum  = GAIN0dB;                   // Pas de gain numérique
  paramsOperateur.bFiltre    = false;                     // Pas de filtre
  paramsOperateur.bExp10     = true;                      // Expension de temps X10
  paramsOperateur.iNbDetect  = 1;                         // Nb détection à 1 pour enregistrer en permanence
  paramsOperateur.iFreqFiltreHighPass = 0;                // Pas de filtre passe haut
  // Appel de la méthode de base sans lecture des paramètres
  bReadParams = false;
  CModeAutoRecorder::BeginMode();
}

//-------------------------------------------------------------------------
// Fin du mode
void CModeProtocolePFixe::EndMode()
{
  // Appel de la méthode de base qui stoppe l'acquisition
  CModeAutoRecorder::EndMode();
  if (CModeGeneric::bDebug)
    Serial.println("CModeProtocolePFixe::EndMode");
}

//-------------------------------------------------------------------------
//! \class CModeSynchro
//! \brief Classe de gestion du mode enregistrement synchro

//-------------------------------------------------------------------------
//! \brief Gestionnaire d'interruption sur passage en enregistrement d'un esclave via le signal de son ESP32
void isrRecord()
{
  if (CModeSynchro::pInstance != NULL)
  {
#if defined(__IMXRT1062__) // Teensy 4.1
    __disable_irq();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
    unsigned char sreg_backup = SREG;     // Sauvegarde de l'état des interruptions
    cli();                                // On stoppe les interruptions
#endif
    CModeSynchro::pInstance->OnIsrRecord();
#if defined(__IMXRT1062__) // Teensy 4.1
    __enable_irq();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
    SREG = sreg_backup;                   // On retaure les interruptions
#endif
  }
}

//-------------------------------------------------------------------------
//! \brief Constructeur
CModeSynchro::CModeSynchro()
{
  pInstance = this;
  bConfMini = false;
  iTimeRecord = 0;
  // Init du mode d'affichage
  bAffInit = false;
  iTimeReset = 0;
}

//-------------------------------------------------------------------------
//! \brief Début du mode
void CModeSynchro::BeginMode()
{
  // On force l'affichage LED pendant les enregistrements
  iUsingLed = commonParams.iUtilLed;
  commonParams.iUtilLed = LEDRECORD;
  // Initialisation du type
  iPRType = paramsOperateur.iMasterSlave;
  // Création de la liaison avec l'ESP32 en fonction du type Master ou Esclave
  if (iPRType == MASTER)
  {
    // Mode Master
    pESP32Link = new CMasterLink();
#if defined(__MK66FX1M0__) // Teensy 3.6
    // Broche RECORD en sortie pour signaler à l'ESP32 le passage et l'arrêt en enregistrement
    pinMode( PIN00RECORD,    OUTPUT);
    digitalWriteFast( PIN00RECORD,  LOW); // No record
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
    bMasterRecording = false;
#endif
  }
  else
  {
    // Mode Esclave
    pESP32Link = new CSlaveLink();
#if defined(__MK66FX1M0__) // Teensy 3.6
    // Broche Record pour recevoir le passage et l'arrêt en enregistrement de l'ESP32
    pinMode( PIN00RECORD, INPUT_PULLUP);
#endif
  }
  // Initialisation de la liaison
  pESP32Link->SetLinkType( iPRType, &paramsOperateur);
  // Appel de la fonction de base
  CModeAutoRecorder::BeginMode();
  // Init du préfixe des fichiers
  if (iPRType == MASTER)
    strcpy( paramsOperateur.sPrefixe, "PRMst");
  else
    sprintf( paramsOperateur.sPrefixe, "PRS%02d", iPRType);
  //Serial.println("CModeSynchro::BeginMode OK");
}

//-------------------------------------------------------------------------
//! \brief Fin du mode
void CModeSynchro::EndMode()
{
#if defined(__MK66FX1M0__) // Teensy 3.6
  detachInterrupt(PIN00RECORD);
#endif
  pInstance = NULL;
  // Destruction du gestionnaire de liaison ESP32
  delete pESP32Link;
  // On restitue l'utilisation de la LED
  commonParams.iUtilLed = iUsingLed;
  // Appel de la fonction de base
  CModeAutoRecorder::EndMode();
  // RAZ LED
  digitalWriteFast(LED_BUILTIN, LOW);
}

extern const char *txtMastSlave[MAXLANGUAGE][10];
extern const char *txtStatusMS[MAXLANGUAGE][15];
extern const char *txtESP32Errors[MAXLANGUAGE][ES_MAXERR];
extern const char *txtOperatorCmdU[];
extern const char *txtOperatorCmdD[];

//-------------------------------------------------------------------------
//! \brief Affichage du mode sur l'écran
//! Cette méthode est appelée régulièrement par le loop principal
//! à charge des différents modes d'afficher les informations nécessaires
void CModeSynchro::PrintMode()
{
  bool bCriticalError = false;
  switch(pESP32Link->GetESP32Status())
  {
  default:
  case ES_OK       :  //!< Status OK
  case ES_ERSERMESS:  //<! Error decoding num message Serial Link
  case ES_ERLORMESS:  //<! Error decoding num message Lora Link
    bCriticalError = false;
    break;
  case ES_TIMEOUT  :  //<! Error time out
  case ES_MSSTATUS :  //<! Inconsistency on the master or slave state
  case ES_ERLORA   :  //!< Error on LORA
  case ES_ERTOP    :  //<! Error on DAC Top
  case ES_ERPARAM  :  //<! Error decoding params message
  case ES_ERSLAVE  :  //<! Error decoding Slave message
  case ES_ERCRC    :  //<! Error CRC
  case ES_UNKNOWER :  //<! Unknow error
    bCriticalError = true;
  }
  if (bCriticalError)
  {
    //Serial.println("CModeSynchro::PrintMode bCriticalError");
    // Affichage d'une erreur ESP32
    pDisplay->clearBuffer();
    pDisplay->setCursor(0,0);
    pDisplay->println( "ESP32 error !");
    pDisplay->setCursor(0,10);
    pDisplay->println( txtESP32Errors[commonParams.iLanguage][pESP32Link->GetESP32Status()]);
    // Affichage de la commande opérateur Down
    pDisplay->setCursor(0, 54);
    pDisplay->println( txtOperatorCmdD[commonParams.iLanguage]);
    UpdateScreen();
    iTimeReset = millis() + 3000;
  }
  else if (!bAffInit)
  {
    //Serial.println("CModeSynchro::PrintMode !bAffInit");
    int y = 0;
    int x = 0;
    pDisplay->clearBuffer();
    if (iPRType == MASTER)
    {
      //Serial.printf("Status Master %d [%s]\n", pESP32Link->GetStatus(MASTER), txtStatusMS[commonParams.iLanguage][pESP32Link->GetStatus(MASTER)]);
      // Affichage de l'état du maître et des esclaves
      for (int i=MASTER; i<MAXMS; i++)
      {
        // Affichage de l'état des différents PR
        pDisplay->setCursor(x,y);
        pDisplay->printf( txtMastSlave[commonParams.iLanguage][i], txtStatusMS[commonParams.iLanguage][pESP32Link->GetStatus(i)]);
        if (x == 0)
          x = 64;
        else
        {
          x = 0;
          y += HLINEPIX;
        }
        // Test si configuration minimale OK
        if (i > MASTER and pESP32Link->GetStatus(MASTER) == MSWAITINGSLAVES and pESP32Link->GetStatus(i) == MSSLAVEOK)
        //if (i > MASTER and pESP32Link->GetStatus(MASTER) == MSWAITINGESP32)
          // Il y a au moins un master et un esclave
          bConfMini = true;
      }
    }
    else
    {
      // Affichage de l'état de l'esclave
      pDisplay->setCursor(x,y);
      pDisplay->printf( txtMastSlave[commonParams.iLanguage][iPRType], txtStatusMS[commonParams.iLanguage][pESP32Link->GetStatus(iPRType)]);
      y = HLINEPIX * 5;
    }
    if (iPRType == MASTER and !pESP32Link->GetValidConfig() and bConfMini)
    {
      // Affichage de la commande opérateur Up
      pDisplay->setCursor(x,y);
      pDisplay->println( txtOperatorCmdU[commonParams.iLanguage]);
    }
    // Affichage de la commande opérateur Down
    y += HLINEPIX;
    pDisplay->setCursor(x,y);
    pDisplay->println( txtOperatorCmdD[commonParams.iLanguage]);
    UpdateScreen();
  }
  else
  {
    //Serial.println("CModeSynchro::PrintMode base");
    // Appel de la fonction de base
    CModeAutoRecorder::PrintMode();
  }
}

//-------------------------------------------------------------------------
//! \brief Traitement des ordres claviers
//! Si la touche est une touche de changement de mode, retourne le mode demandé
//! Cette méthode est appelée régulièrement par le loop principal
//! à charge des différents modes de traiter les actions opérateurs
//! \param key Touch to treat
int CModeSynchro::KeyManager(
  unsigned short key
  )
{
  if (!bAffInit)
  {
    // Gestion éventuelle des touches
    if (key == K_UP and iPRType == MASTER)
    {
      // Validation de la configuration, on passe en mode enregistrement
      bAffInit = true;
      // To calculate if it is time sleep
      iFinMinutes = -1;
      iDebMinutes = -1;
      ((CMasterLink *)pESP32Link)->SetValidConfig();
      // Wait a lot before record or sleep mode
      iTimeRecord = millis() + 300;
    }
    else if (key == K_DOWN)
    {
      // Demande de passage en mode paramètres
      iNewMode = MPARAMS;
      // On stoppe le mode synchro y compris chez les esclaves
      pESP32Link->StopMode();
      // Delais pour envoyer le message avant d'éteindre l'ESP32
      delay(300);
    }
    else
    {
      // Test si la configuration est complète
      if ((iPRType == MASTER and pESP32Link->GetStatus(MASTER) == MSMASTEROK) or (iPRType != MASTER and pESP32Link->GetStatus(iPRType) == MSSLAVEOK))
      {
        LogFile::AddLog( LLOG, "CModeSynchro config OK");
        // On passe en mode enregistrement
        bAffInit = true;
        // To calculate if it is time to sleep
        iFinMinutes = -1;
        iDebMinutes = -1;
        if (iPRType > MASTER)
        {
#if defined(__MK66FX1M0__) // Teensy 3.6
          // Init interruption record pour les esclaves
          attachInterrupt(PIN00RECORD, isrRecord, CHANGE);
          Serial.println("CModeSynchro::KeyManager attachInterrupt PIN00RECORD");
#endif
        }
        // Wait a lot before record or sleep mode
        iTimeRecord = millis() + 300;
      }
    }
  }
  else if (iTimeRecord > 0 and millis() > iTimeRecord)
  {
    // Copy synchro parameters on log file
    ParamsToLog();
    // Start acquisition
    if (pRecorder != NULL)
    {
      pRecorder->CalculPreAcquisition();
      pRecorder->StartAcquisition();
    }
    // OK for Record mode or sleep mode
    iTimeRecord = 0;
    if (iPRType == MASTER and pRecorder != NULL)
      // Passage en mode auto record
      pRecorder->SetAutoRecord( true);
    else if (pRecorder != NULL)
      // Attente des ordres d'enregistrement
      pRecorder->SetAutoRecord( false);
  }
  else
  {
    //Serial.println("CModeAutoRecorder::KeyManager");
    // Appel de la méthode de base. Si l'heure correspond à la veille, on passe en mode veille, sinon, on est en enregistrement auto synchro
    iNewMode = CModeAutoRecorder::KeyManager( key);
#if defined(__IMXRT1062__) // Teensy 4.1
    // Pour un slave, test si on doit passer en enregistrement
    if (iPRType > MASTER and pESP32Link->GetStatus(iPRType) == MSSLAVEWAITREC)
    {
      // Passage en enregistrement
      if (pRecorder != NULL)
        pRecorder->StartRecording();
      pESP32Link->SetStatus( MSSLAVERECORD);
    }
    // Pour un slave, test si on doit stopper l'enregistrement
    else if (iPRType > MASTER and pESP32Link->GetStatus(iPRType) == MSSLAVEWAITSREC)
    {
      // Arrêt enregistrement
      //Serial.println("CModeSynchro::KeyManager MSSLAVEWAITSREC StopRecording");
      if (pRecorder != NULL)
        pRecorder->StopRecording();
      pESP32Link->SetStatus( MSSLAVEOK);
      //Serial.println("CModeSynchro::KeyManager StopRecording OK");
    }
    // Pour un master, test si on doit lancer ou stopper les esclaves en enregistrement
    if (iPRType == MASTER and pRecorder != NULL)
    {
      if (pRecorder->IsRecording() and !bMasterRecording)
      {
        pESP32Link->StartRecord();
        bMasterRecording = true;
      }
      else if (!pRecorder->IsRecording() and bMasterRecording)
      {
        pESP32Link->StopRecord();
        bMasterRecording = false;
      }
    }
#endif
    // Pour un slave, test si le mode est stoppé par le master
    if (iPRType > MASTER and pESP32Link->GetStatus(iPRType) == MSSLAVESTOP)
    {
      // Le Master a stoppé le mode, on stope l'acquisition et on rebascule en mode init
      LogFile::AddLog( LLOG, "CModeSynchro Slave, Master stop mode");
      if (pRecorder != NULL)
        pRecorder->StopAcquisition();
      pESP32Link->SetStatus( MSWAITINGPARAMS);
      bAffInit = false;
      iDecompteurAffAcq = TIMEATTENTEAFF;
#if defined(__MK66FX1M0__) // Teensy 3.6
      // Arrêt momentané de l'ESP32 pour bien démarrer
      digitalWriteFast( PIN23ESPON,  LOW ); // EPS32 OFF
      delay(300);
      digitalWriteFast( PIN23ESPON,  HIGH); // EPS32 ON      
#endif
    }
    else if (iPRType == MASTER and iNewMode == MPARAMS)
    {
      // Stop du mode avec passage en mode paramètres (sur passage en mode veille, on ne fait rien)
      LogFile::AddLog( LLOG, "CModeSynchro Master stop mode");
      pESP32Link->StopMode();
      // Delais pour envoyer le message avant d'éteindre l'ESP32
      delay(300);
    }
    else if (iTimeReset != 0 and millis() > iTimeReset)
    {
      // Set status OK
      pESP32Link->ResetESP32Status();
      iTimeReset = 0;
      // Reset synchro mode
      //iNewMode = MSYNCHROR;
    }
  }
  return iNewMode;
}

//-------------------------------------------------------------------------
//! \brief Gestion des tâches de fonds
void CModeSynchro::OnLoop()
{
  //Serial.println("CModeSynchro::OnLoop");
  pESP32Link->OnLoop();
  //Serial.println("CModeSynchro::OnLoop B");
  CModeAutoRecorder::OnLoop();
  //Serial.println("CModeSynchro::OnLoop OK");
}

//-------------------------------------------------------------------------
//! \brief Traitement interruption Record
void CModeSynchro::OnIsrRecord()
{
  //Serial.println("CModeSynchro::OnIsrRecord");
#if defined(__MK66FX1M0__) // Teensy 3.6
  // Gestion recording...
  if (digitalReadFast( PIN00RECORD) == HIGH)
  {
    // Passage en enregistrement
    if (pRecorder != NULL)
      pRecorder->StartRecording();
    //Serial.println("PIN00RECORD HIGH StartRecording");
  }
  else
  {
    // Arrêt enregistrement
    if (pRecorder != NULL)
      pRecorder->StopRecording();
    //Serial.println("PIN00RECORD LOW StopRecording");
  }
#endif
}

//! Instance
CModeSynchro *CModeSynchro::pInstance = NULL;
