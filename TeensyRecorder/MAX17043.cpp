/*!
 * @file MAX17043.cpp
 *
 * @copyright   Copyright (c) 2010 DFRobot Co.Ltd (http://www.dfrobot.com)
 * @license     The MIT License (MIT)
 * @author [ouki.wang](ouki.wang@dfrobot.com)
 * @version  V1.1 (Modifier by Jean-Do for chinese clone of MAX17043)
 * @date  2018-4-14
 * @url https://github.com/DFRobot/MAX17043
 */

#include "MAX17043.h"

MAX17043::MAX17043() {bOK = false;}

int MAX17043::begin()
{
  // Test address
  address = MAX17043_ADDRESS_B;
  Wire.begin();
  Wire.beginTransmission( MAX17043_ADDRESS_B);
  delay(10);
  byte error = Wire.endTransmission();
  if (error == 0)
  {
    bOK = true;
    address = MAX17043_ADDRESS_B;
    //Serial.printf("MAX17043::begin Adr B OK\n");
  }
  else
  {
    Wire.beginTransmission( MAX17043_ADDRESS_A);
    delay(10);
    error = Wire.endTransmission();
    if (error == 0)
    {
      bOK = true;
      address = MAX17043_ADDRESS_A;
      //Serial.printf("MAX17043::begin Adr A OK\n");
    }
  }
  Wire.end();
  if (error != 0)
  {
    bOK = false;
    //Serial.printf("MAX17043::begin Adr A&B HS !\n");
  }
  if (bOK)
  {
    write16(MAX17043_COMMAND, 0x5400);        // power on reset
    delay(10);
    //Serial.printf("Config MAX %X\n", read16(MAX17043_CONFIG));
    if(read16(MAX17043_CONFIG) == 0x971c)     //default 0x971c
    {
      write16(MAX17043_MODE, 0x4000);         //quick start
      write16(MAX17043_CONFIG, 0x9700);
      delay(10);
    }
    else
    {
      Serial.println("MAX17043_CONFIG != 0x971c !!!");
      write16(MAX17043_MODE, 0x4000);         //quick start
      write16(MAX17043_CONFIG, 0x9700);
      delay(10);
    }
  }
  return 0;
}

float MAX17043::readVoltage()
{
  float fV = ((1.25f * (float)(read16(MAX17043_VCELL) >> 4)) / 1000) - 0.5;
  if (fV > 4.2)
    fV = 4.2;
  return fV;
}

float MAX17043::readPercentage()
{
  uint16_t per = read16(MAX17043_SOC);
  float fP = (float)(((per >> 8) + 0.003906f * (per & 0x00ff)) / 1.77);
  if (fP > 100.0)
    fP = 100.0;
  return fP;
}

void MAX17043::setPowerOn()
{
  write16(MAX17043_MODE, 0x4000);;
}

void MAX17043::setSleep()
{
  writeRegBits(MAX17043_CONFIG, 1, 0x01, 7);
}

void MAX17043::setWakeUp()
{
  writeRegBits(MAX17043_CONFIG, 0, 0x01, 7);
}

uint16_t MAX17043::getVersion()
{
  return read16(MAX17043_VERSION);
}
