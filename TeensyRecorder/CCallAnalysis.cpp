//-------------------------------------------------------------------------
//! \file CCallAnalysis.cpp
//! \brief Call analysis classes
//! \details class CCallAnalysis, CCallMem, CQFCCall
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2023 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 /*
  * Classes :
  * - CCallProcessing
  *   - CQFCProcessing
  * - CCallAnalysis
  */
#include "CCallAnalysis.h"
//#include "CRecorder.h"
#include "ModesModifiers.h"

//-------------------------------------------------------------------------
//! \class CCallProcessing
//! \brief Generic call processing class

//-------------------------------------------------------------------------
//! \brief Constructor (initialization of parameters to default values)
CCallProcessing::CCallProcessing()
{
  iMinChannel = 0;
  iMaxChannel = 0;
}

//-------------------------------------------------------------------------
//! \brief Destructor
CCallProcessing::~CCallProcessing()
{
}

//-------------------------------------------------------------------------
//! \brief Initialization of the characteristics of the analysis
//! \param iMinCh Minimum channel
//! \param iMaxCh Minimum channel
void CCallProcessing::InitializingAnalysis(
  int iMinCh,
  int iMaxCh
  )
{
  iMinChannel = iMinCh;
  iMaxChannel = iMaxCh;
}

//-------------------------------------------------------------------------
//! \brief Processing FFT results
//! \param piTbSeuilCnx Table of thresholds
//! \param pFFTResults Table of FFT levels
//! \return true if call detection
bool CCallProcessing::FFTProcessing(
  volatile uint32_t *piTbSeuilCnx,
  volatile  int32_t *pFFTResults
  )
{
  return false;
}

//-------------------------------------------------------------------------
//! \class CQFCProcessing
//! \brief QFC call processing class

//-------------------------------------------------------------------------
//! \brief Constructor (initialization of parameters to default values)
CQFCProcessing::CQFCProcessing()
{
  iMinBand     = 0;
  iMaxBand     = 0;
  iMinDuration = 0;
  iMaxDuration = 0;
  for (int i=0; i<MAXNBCALLS; i++)
  {
    tbQFC[i].iCurrentChannel = -1;
    tbQFC[i].iMaxChannel     = 0;
    tbQFC[i].iMinChannel     = 0;
    tbQFC[i].iNbDetections   = 0;
    tbQFC[i].bCurrentDet     = false;
  }
  bMaxQFC = false; 
}

//-------------------------------------------------------------------------
//! \brief Destructor
CQFCProcessing::~CQFCProcessing()
{
}

//-------------------------------------------------------------------------
//! \brief Initialization of the characteristics of the QFC
//! \param iMinCh Minimum frequency in channel
//! \param iMaxCh Maximum frequency in channel
//! \param iMinBd Number of channels of the minimum band
//! \param iMaxBd Number of channels of the maximum band
//! \param iMinDu Minimum duration in number of FFT
//! \param iMaxDu Maximum duration in number of FFT
void CQFCProcessing::InitializingAnalysis(
  int iMinCh,
  int iMaxCh,
  int iMinBd,
  int iMaxBd,
  int iMinDu,
  int iMaxDu
  )
{
  CCallProcessing::InitializingAnalysis(iMinCh, iMaxCh);
  iMinBand     = iMinBd;
  iMaxBand     = iMaxBd;
  iMinDuration = iMinDu;
  iMaxDuration = iMaxDu;
}

//-------------------------------------------------------------------------
//! \brief Processing FFT results
//! \param piTbSeuilCnx Table of thresholds
//! \param pFFTResults Table of FFT levels
//! \return true if call detection
bool CQFCProcessing::FFTProcessing(
  volatile uint32_t *piTbSeuilCnx,
  volatile  int32_t *pFFTResults
  )
{
  // For each channel of the band
  for (int ch=iMinChannel; ch<=iMaxChannel; ch++)
  {
    uint16_t iNiv = abs((int16_t)pFFTResults[ch]);
    if (iNiv > (uint16_t)piTbSeuilCnx[ch])
      // Detection processing on a channel
      ProcessingDetection( ch);
  }
  // Test if a QFC has just ended
  return TestFC();
}

//-------------------------------------------------------------------------
//! \brief Processing a detection
//! \param iCh Detection channel
void  CQFCProcessing::ProcessingDetection(
  int iCh
  )
{
  bool bNewQFC = true;
  // For each current QFC
  for (int i=0; i<MAXNBCALLS; i++)
  {
    tbQFC[i].iCurrentChannel = -1;
    // If a QFC exist and ch inside current QFC
    if (tbQFC[i].bCurrentDet and iCh >= (tbQFC[i].iMinChannel-1) and iCh <= (tbQFC[i].iMaxChannel+1))
    {
      // Add information
      tbQFC[i].iMinChannel     = min (tbQFC[i].iMinChannel, iCh);
      tbQFC[i].iMaxChannel     = max (tbQFC[i].iMaxChannel, iCh);
      tbQFC[i].iCurrentChannel = iCh;
      tbQFC[i].iNbDetections++;
      bNewQFC = false;
    }
  }
  if (bNewQFC)
  {
    // Create a new QFC
    for (int i=0; i<MAXNBCALLS; i++)
    {
      if (not tbQFC[i].bCurrentDet)
      {
        // Add information
        tbQFC[i].iMinChannel     = iCh;
        tbQFC[i].iMaxChannel     = iCh;
        tbQFC[i].iCurrentChannel = iCh;
        tbQFC[i].iNbDetections   = 1;
        tbQFC[i].bCurrentDet     = true;
        bNewQFC = false;
      }
    }
  }
  if (not bMaxQFC and bNewQFC)
  {
    // Indicates that the maximum number of QFCs has been exceeded
    bMaxQFC = true;
    LogFile::AddLog( LLOG, "***** CQFCProcessing maximum number of QFCs has been exceeded !");
  }
}

//-------------------------------------------------------------------------
//! \brief Test if a QFC is detected
//! \return true if call detection
bool  CQFCProcessing::TestFC()
{
  bool bQFC = false;
  for (int i=0; i<MAXNBCALLS; i++)
  {
    if (tbQFC[i].bCurrentDet and tbQFC[i].iCurrentChannel < 0)
    {
      // End of a QFC, test if it corresponds to the one sought
      int iBand = tbQFC[i].iMaxChannel - tbQFC[i].iMinChannel + 1;
      if (    tbQFC[i].iMinChannel >= iMinChannel
          and tbQFC[i].iMaxChannel <= iMaxChannel
          and iBand >= iMinBand
          and iBand <= iMaxBand
          and tbQFC[i].iNbDetections >= iMinDuration
          and tbQFC[i].iNbDetections <= iMaxDuration)
        bQFC = true;
      // QFC Reset        
      tbQFC[i].iCurrentChannel = -1;
      tbQFC[i].iMaxChannel     = 0;
      tbQFC[i].iMinChannel     = 0;
      tbQFC[i].iNbDetections   = 0;
      tbQFC[i].bCurrentDet     = false;
    }
  }
  return bQFC;
}

//-------------------------------------------------------------------------
//! \class CCallAnalysis
//! \brief Generic class for call analyzing

//-------------------------------------------------------------------------
//! \brief Constructor (initialization of parameters to default values)
CCallAnalysis::CCallAnalysis()
{
  for (int i=0; i<MAXNBCALLHANDLER; i++)
    tbpCallProcessing[i] = NULL;
  iCanalWidth    = 0;;
  iFFTDur        = 0;
  iNbFFTChannels = 0;
}

//-------------------------------------------------------------------------
//! \brief Destructor
CCallAnalysis::~CCallAnalysis()
{
  for (int i=0; i<MAXNBCALLHANDLER; i++)
    if (tbpCallProcessing[i] != NULL)
  	  delete tbpCallProcessing[i];
}

//-------------------------------------------------------------------------
//! \brief Initialization of the characteristics of the analysis
//! \param iFe Sampling frequency in Hz
//! \param iFFTLen FFT len in points
void CCallAnalysis::InitializingAnalysis(
  uint32_t iFe,
  uint16_t iFFTLen
  )
{
  iNbFFTChannels = iFFTLen / 2;
  iCanalWidth  = (iFe / 2) / iNbFFTChannels;
  float fDur = (1.0 / (float)iFe * (float)iFFTLen) * 1000;
  iFFTDur = (int)fDur;
}

//-------------------------------------------------------------------------
//! \brief Added QFC parser
//! \param iMinFr Minimum band frequency in Hz
//! \param iMaxFr Maximum band frequency in Hz
//! \param iMinBW Minimum bandwidth in Hz
//! \param iMaxBW Maximum bandwidth in Hz
//! \param iMinDu Minimum duration in µs
//! \param iMaxDu Maximum duration in µs
void CCallAnalysis::AddQFCCall(
  uint32_t iMinFr,
  uint32_t iMaxFr,
  uint32_t iMinBW,
  uint32_t iMaxBW,
  uint32_t iMinDu,
  uint32_t iMaxDu
  )
{
  for (int i=0; i<MAXNBCALLHANDLER; i++)
  {
    if (tbpCallProcessing[i] == NULL)
    {
      tbpCallProcessing[i] = new CQFCProcessing();
      ((CQFCProcessing *)tbpCallProcessing[i])->InitializingAnalysis( iMinFr/iCanalWidth, iMaxFr/iCanalWidth, iMinBW/iCanalWidth, iMaxBW/iCanalWidth, iMinDu*iFFTDur, iMaxDu*iFFTDur);
      break;
    }
  }
}

//-------------------------------------------------------------------------
//! \brief Processing FFT results
//! \param piTbSeuilCnx Table of thresholds
//! \param pFFTResults Table of FFT levels
//! \return true if call detection
bool CCallAnalysis::FFTProcessing(
  volatile uint32_t *piTbSeuilCnx,
  volatile  int32_t *pFFTResults
  )
{
  bool bReturn = false;
  for (int i=0; i<MAXNBCALLHANDLER; i++)
    if (tbpCallProcessing[i] != NULL and tbpCallProcessing[i]->FFTProcessing(piTbSeuilCnx, pFFTResults))
      bReturn = true;
  return bReturn;
}
