//-------------------------------------------------------------------------
//! \file CModifBandRL.h
//! \brief Classe de modification des bandes du mode RhinoLogger
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/* 
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "CModeGeneric.h"

#ifndef CMODIFBANDSRL_H
#define CMODIFBANDSRL_H

//! \enum IDXPARAMS
//! \brief Index of modifiers of a band
enum IDXMODIFBANDS {
  IDXRETURN  = 0, //!< Return to parameters mode
  IDXNUMBAND = 1, //!< Band Index
  IDXNAME    = 2, //!< Name of the band
  IDXTYPE    = 3, //!< Type of the band
  IDXNBDET   = 4, //!< Minimum detections to consider a positive second
  IDXBDFMIN  = 5, //!< Minimum frequency of the band
  IDXBDFMAX  = 6, //!< Maximum frequency of the band
  IDXMINDUR  = 7, //!< Minimum duration of the band
  IDXMAXDUR  = 8, //!< Maximum duration of the band
  IDXMINWIDTH= 9, //!< Minimum width of the band
};

//-------------------------------------------------------------------------
//! \class CModeBandsRL
//! \brief Management Class of Bands Edit Mode
//! Displays the different parameters and allows you to modify them
class CModeBandsRL : public CModeGeneric
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor
  CModeBandsRL();

  //-------------------------------------------------------------------------
  //! \brief Beginning of the mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  //! \brief Traitement des ordres claviers
  virtual int KeyManager(
    unsigned short key  // Touch to treat
    );
  
  //-------------------------------------------------------------------------
  //! \brief Called on end of change of a modifier
  //! If a mode change, returns the requested mode
  virtual int OnEndChange(
    int idxModifier // Index of affected modifier
    );

  //-------------------------------------------------------------------------
  //! \brief Consistency between parameters
  void ControleParams();
  
protected:
  //! Index of the band in modification
  int idxBand;
  
  //! Temporary band in modification
  MonitoringBand tmpBand;
};


#endif // CMODIFBANDSRL_H
