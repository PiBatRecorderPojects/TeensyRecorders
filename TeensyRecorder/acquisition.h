//! \file acquisition.h
//! \brief Classe de gestion des périphériques d'acquisition en mode mono
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/*
  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef ACQUISITION_H
#define ACQUISITION_H

#include <Arduino.h>

#if defined(__IMXRT1062__) // Teensy 4.1 *** ATTENTION sur T4.1 ADC2 = ADC1 T3.6 ***
extern "C" void xbar_connect(unsigned int input, unsigned int output);
extern void dma_T41_isr();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
extern void dma_ch2_isr(void);
#endif

//-------------------------------------------------------------------------
//! \class Acquisition
//! \brief Classe d'initialisation de l'acquistion via le PIT, ADC et DMA
//! Utilise l'ADC1, le PIT0 et le DMA2 sur un Teensy 3.6
//! Utilise l'ADC2, le PIT0 et le DMA2 sur un Teensy 4.1
//! Utilisation :
//! - Instantier un objet Acquisition,
//! - Initialiser la Fe et la résolution
//! - Faire un start pour lancer l'acquisition et un stop pour l'arrêter
//! - Traiter les interruption du DMA2 (void dma_ch2_isr())
class Acquisition
{

  public:
    //-------------------------------------------------------------------------
    //! \brief Constructeur
    //! \param pBuff Pointeur vers le double buffer d'échantillons utilisé par le DMA
    //! \param nbSamples Nombre d'échantillons d'un buffer
    //! \param Pin N° de la broche d'entrée (A0 à A9)
    //! \param Fe Fréquence d'échantillonnage en Hz
    Acquisition(
      uint16_t *pBuff,    
      uint16_t nbSamples, 
      unsigned int Pin,   
      unsigned int Fe     
      )
    {
      // Init des paramètres d'acquisition
      iFe  = Fe;
      iPin = Pin;
      pBuffEch = pBuff;
      uiNbSamplesBuffer = nbSamples;
      bAcquisition = false;
    }

    //-------------------------------------------------------------------------
    //! \brief Destructeur
    virtual ~Acquisition()
    {
    }
    
    //-------------------------------------------------------------------------
    //! \brief CInitialisation de la fréquence d'échantillonnage
    //! \param Fe Fréquence d'échantillonnage en Hz
    void setFe(
      unsigned int Fe // Fréquence d'échantillonnage en Hz
      )
    {
      iFe = Fe;
    }

    //-------------------------------------------------------------------------
    //! \brief Lance l'acquisition
    virtual void start()
    {
      // Lancement ADC, DMA et PIT
      adcInit();
      dmaInit();
      pitInit();
      bAcquisition = true;
    }
    
    //-------------------------------------------------------------------------
    //! \brief Stoppe l'acquisition
    virtual void stop()
    {
#if defined(__IMXRT1062__) // Teensy 4.1
      // 54.9.1.15 Timer Channel Enable Register
      TMR4_ENBL &= ~(1<<3);                           // Disable Timer
      // 6.5.5.8 Clear Enable Request
      DMA_CERQ = ch;
      // 14.7.23 CCM Clock Gating Register 2
      CCM_CCGR2 |= CCM_CCGR2_XBAR1(CCM_CCGR_OFF);     // xbar1 clock disable
      // 14.7.26 CCM Clock Gating Register 5
      CCM_CCGR5 |= CCM_CCGR5_DMA(CCM_CCGR_OFF);       // DMA clock disable
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
      // (46.4.6) Disable timer 0
      PIT_TCTRL0 &= ~PIT_TCTRL_TEN;     // Arrêt du PIT 0
      // (23.4.1) Channel Configuration Register
      DMAMUX0_CHCFG2 = DMAMUX_DISABLE;  // Arrêt du DMA 2
#endif
      bAcquisition = false;
    }

    //-------------------------------------------------------------------------
    //! \brief Indique si l'acquisition est active (true) ou non (false)
    bool IsAcquisition() {return bAcquisition;};

    //-------------------------------------------------------------------------
    //! \brief RAZ IT DMA
    virtual void ClearDMAIsr()
    {
#if defined(__IMXRT1062__) // Teensy 4.1
      DMA_CINT = ch;
      DMA_CDNE = ch; //????
      // Handle clear interrupt glitch in Teensy 4.x!
      asm("DSB");
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
      // ATTENTION, l'exécution de ce code ici casse le mécanisme des IT sur T3.6 !!!
      // Ce code est à faire uniquement dans la fonction de traitement de l'IT
      // RAZ bit d'interruption du canal 2
      //DMA_CINT = DMA_CINT_CINT(2); // (24.3.12) Clear interrupt request register
#endif
    }

    //-------------------------------------------------------------------------
    //! \brief Return DMA error (0 if no error)
    virtual uint32_t GetDMAError()
    {
      uint32_t DMAErr = 0;
      if (bAcquisition)
        // Même nom du registre sur T3.6 et T4.1 mais la lecture ne doit pas intervenir lorsque le DMA n'est pas lancé sur T4.1
        DMAErr = DMA_ES;
      return DMAErr;
    }

    //-------------------------------------------------------------------------
    //! \brief Retourne l'adresse courante d'écriture du DMA
    virtual uint32_t GetCurrentDMAaddr()
    {
#if defined(__IMXRT1062__) // Teensy 4.1
      // 6.5.5.26 TCD Destination Address
      return (uint32_t )IMXRT_DMA_TCD[ch].DADDR;
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
      return (uint32_t )(DMA_TCD2_DADDR);
#endif
    }

  protected:
    //-------------------------------------------------------------------------
    //! \brief Configuration de l'ADC 1 T3.6 ou ADC2 T4.1
    virtual void adcInit()
    {
      //iPin = 9; // Fixe A9 pour test (broche 4 coté USB et batteries pour test inutilisée sur PR)
      //iPin = 2; // Fixe A2 pour test (broche de mesure des batteries pour avoir un niveau constant)
      //iPin = 1; // Fixe A1 pour test (broche de mesure du 12V externe pour avoir un niveau 0 ou potentioméètre hétérodyne sur AR)
      //Serial.printf("adcInit iPin %d\n", iPin);
#if defined(__IMXRT1062__) // Teensy 4.1 *** ATTENTION sur T4.1 ADC2 = ADC1 T3.6 ***
      // Init de l'ADC2
      PROGMEM static const uint8_t adcT4_pin_to_channel[] = {
        7,      // 0/A0   AD_B1_02
        8,      // 1/A1   AD_B1_03
        12,     // 2/A2   AD_B1_07
        11,     // 3/A3   AD_B1_06
        6,      // 4/A4   AD_B1_01
        5,      // 5/A5   AD_B1_00
        15,     // 6/A6   AD_B1_10
        0,      // 7/A7   AD_B1_11
        13,     // 8/A8   AD_B1_08
        14,     // 9/A9   AD_B1_09
        255,    // 10/A10 AD_B0_12 - only on ADC1, 1 - can't use for audio
        255,    // 11/A11 AD_B0_13 - only on ADC1, 2 - can't use for audio
        3,      // 12/A12 AD_B1_14
        4,      // 13/A13 AD_B1_15
        7,      // 14/A0  AD_B1_02
        8,      // 15/A1  AD_B1_03
        12,     // 16/A2  AD_B1_07
        11,     // 17/A3  AD_B1_06
        6,      // 18/A4  AD_B1_01
        5,      // 19/A5  AD_B1_00
        15,     // 20/A6  AD_B1_10
        0,      // 21/A7  AD_B1_11
        13,     // 22/A8  AD_B1_08
        14,     // 23/A9  AD_B1_09
        255,    // 24/A10 AD_B0_12 - only on ADC1, 1 - can't use for audio
        255,    // 25/A11 AD_B0_13 - only on ADC1, 2 - can't use for audio
        255,    // 26/A12 AD_B1_14 - only on ADC2, do not use analogRead()
        255,    // 27/A13 AD_B1_15 - only on ADC2, do not use analogRead()
        255,    // 28
        255,    // 29
        255,    // 30
        255,    // 31
        255,    // 32
        255,    // 33
        255,    // 34
        255,    // 35
        255,    // 36
        255,    // 37
        255,    // 38/A14 AD_B1_12 - only on ADC2, do not use analogRead()
        255,    // 39/A15 AD_B1_13 - only on ADC2, do not use analogRead()
        9,      // 40/A16 AD_B1_04
        10,     // 41/A17 AD_B1_05
      };

      // 14.7.23 CCM Clock Gating Register 2
      const int trigger = 4; // 0-3 for ADC1, 4-7 for ADC2
      CCM_CCGR2 |= CCM_CCGR2_XBAR1(CCM_CCGR_ON);                                  // xbar1 clock enable

      // 61.5.1 Crossbar A Select Register 0 (XBARAx_SEL0)
      // Connect the timer output to the ADC_ETC input (intput = 39, output = 103 + 4 = 107 "/ 2" = 53 = Crossbar A Select Register 53 (XBARA1_SEL53))
      // Input (XBARA_INn = 39) to be muxed to XBARA_OUT106 (refer to Functional Description section for input/output assignment)
      xbar_connect(XBARA1_IN_QTIMER4_TIMER3, XBARA1_OUT_ADC_ETC_TRIG00 + trigger);
 
      // 67.6.1.2 ADC_ETC Global Control Register
      // turn on ADC_ETC and configure to receive trigger
      if (ADC_ETC_CTRL & (ADC_ETC_CTRL_SOFTRST | ADC_ETC_CTRL_TSC_BYPASS))
      {
        ADC_ETC_CTRL = 0; // SOFTRST ADC_ETC works normally
        ADC_ETC_CTRL = 0; // Clears TSC_BYPASS, To use ADC2, this bit should be cleared
      }
      ADC_ETC_CTRL |= ADC_ETC_CTRL_DMA_MODE_SEL;                                  // Trig DMA_REQ with pulsed signal, REQ will be cleared by ACK only
      // PRE_DIVIDER ? Pre-divider for trig delay and interval. The meaning of this field will be explained in the description of TRIGa_COUNTER
      // EXT1_TRIG_PRIORITY ? External TSC1 trigger priority, 7 is highest priority, while 0 is lowest
      // EXT1_TRIG_ENABLE ? TSC1 TRIG enable register
      // EXT0_TRIG_PRIORITY ? External TSC0 trigger priority, 7 is highest priority, while 0 is lowest
      // EXT0_TRIG_ENABLE ? TSC0 TRIG enable register
      ADC_ETC_CTRL |= ADC_ETC_CTRL_TRIG_ENABLE(1 << trigger);                     // TRIG enable register, 00010000b - enable external XBAR trigger 4
      
      // 67.6.1.5 ETC DMA control Register
      ADC_ETC_DMA_CTRL |= ADC_ETC_DMA_CTRL_TRIQ_ENABLE(trigger);                  // Enable DMA request when TRIG4 done, 1b - TRIG4 DMA request enabled    

      // 67.6.1.6 ETC_TRIG Control Register
      const int len = 1;
      IMXRT_ADC_ETC.TRIG[trigger].CTRL = ADC_ETC_TRIG_CTRL_TRIG_CHAIN(len - 1) |  // Trigger chain length is 1
                                         ADC_ETC_TRIG_CTRL_TRIG_PRIORITY(7);      // External trigger priority, 7 is highest priority
                                                                                  // SYNC_MODE 0b - Synchronization mode disabled, TRIGa and TRIG(a+4) are triggered independently
                                                                                  // TRIG_MODE 0b - Hardware trigger. The softerware trigger will be ignored
                                                                                  // SW_TRIG 0b - No software trigger event generated

      // 67.6.1.8 ETC_TRIG Chain 0/1 Register
      IMXRT_ADC_ETC.TRIG[trigger].CHAIN_1_0 = ADC_ETC_TRIG_CHAIN_HWTS0(1) |       // ADC TRIG0 selected
        ADC_ETC_TRIG_CHAIN_CSEL0(adcT4_pin_to_channel[iPin]) |                    // Select pin
        ADC_ETC_TRIG_CHAIN_B2B0;                                                  // Enable B2B0 When Segment 0 finished (ADC COCO) then automatically trigger next ADC
                                                                                  // conversion, no need to wait until interval delay reached
                                                                                  // IE1 00b - No interrupt when finished
                                                                                  // B2B1 Disable B2B. Wait until delay value defined by TRIG1_COUNTER[SAMPLE_INTERVAL] is reached
                                                                                  // HWTS1 00000000b - no trigger selected
                                                                                  // CSEL1 ADC channel selection
                                                                                  // IE0 Segment 0 done interrupt selection 00b - No interrupt when finished

      // 66.8.6 Configuration register (ADCx_CFG)
      ADC2_CFG = 0;                                                     // OVWREN disable overwriting, ADTRG Software trigger, REFSEL Selects VREFH/VREFL as reference voltage
                                                                        // ADHSC Normal conversion selected, ADLPC ADC hard block not in low power mode
      ADC2_CFG = ADC_CFG_MODE(2) | ADC_CFG_ADSTS(3) | ADC_CFG_ADLSMP    // 12 bits, 25 sample period, long sample time 
               | ADC_CFG_ADICLK(1) | ADC_CFG_ADIV(1) | ADC_CFG_AVGS(1); // IPG clock divided by 2, Input clock / 2, 8 samples averaged
//               | ADC_CFG_ADICLK(1) | ADC_CFG_ADIV(2) | ADC_CFG_AVGS(1); // IPG clock / 2, Input clock / 4, 8 samples averaged
      ADC2_CFG |= ADC_CFG_OVWREN;                                       // OVWREN enable overwriting
      ADC2_CFG |= ADC_CFG_ADLPC;                                        // ADC hard block in low power mode
      ADC2_CFG |= ADC_CFG_ADHSC;                                        // High speed conversion selected
      // 66.8.7 General control register (ADCx_GC)
      ADC2_GC &= ~ADC_GC_ACFE;                                          // Compare function disable
      ADC2_GC &= ~ADC_GC_ACFGT;                                         // Compare Function Greater Than disable
      ADC2_GC &= ~ADC_GC_ACREN;                                         // Range function disable
      ADC2_GC |= ADC_GC_AVGE;                                           // Averaging
      //ADC2_GC &= ~ADC_GC_AVGE;                                          // No Averaging
      //ADC2_GC |= ADC_GC_ADCO;                                           // Continuous Conversion Enable
      ADC2_GC &= ~ADC_GC_ADCO;                                          // Continuous Conversion disable
      ADC2_GC |= ADC_GC_DMAEN;                                          // DMA enable
      ADC2_GC &= ~ADC_GC_ADACKEN;                                       // ADACKEN Asynchronous clock output disable

      adcCalibrate();                                                   // Calibration de l'ADC 2

      // 66.8.1 Control register for hardware triggers (ADCx_HC0)
      ADC2_HC0 = ADC_HC_ADCH(16);                                       // 16 = controlled by ADC_ETC
      ADC2_CFG |= ADC_CFG_ADTRG;                                        // Hardware trigger selected (after calibration),
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
      // Teensy to ARM Pin Conversion Chart
      // ADC0, pas de restriction - ATTENTION, ADC1, seulement A2 et A3
      //                                A0  A1  A2  A3  A4  A5  A6  A7  A8  A9
      const uint8_t TsyADCtoARMpin[] = { 5, 14,  8,  9, 13, 12,  6,  7, 15,  4};
      const uint8_t TMuxSel[]        = { 1,  1,  0,  0,  1,  1,  1,  0,  0,  0};
      // (13.2.16) System clock gating control register 6
      SIM_SCGC3 |= SIM_SCGC3_ADC1;
      // (39.4.2) ADC Configuration Register 1
      ADC1_CFG1  = ADC_CFG1_ADIV(1);    // Input clock / 2
      ADC1_CFG1 |= ADC_CFG1_MODE(1);    // DIFF=0 Single-ended, 12-bit
      //ADC1_CFG1 |= ADC_CFG1_MODE(1);  // DIFF=1 Differential, 13-bit*/
      ADC1_CFG1 |= ADC_CFG1_ADLSMP;     // Long sample time
      // (39.4.3) ADC Configuration Register 2
      ADC1_CFG2  = ADC_CFG2_ADLSTS(3);  // 2 extra ADCK cycles; 6 ADCK cycles total sample time
      if (TMuxSel[iPin] == 1)
        ADC1_CFG2 |= ADC_CFG2_MUXSEL;   // ADxxB channels are selected (See table 39.1.3.1)
      // (39.4.6) Status and Control Register 2
      ADC1_SC2  = ADC_SC2_REFSEL(0);    // Voltage reference = Vcc
      //ADC1_SC2  = ADC_SC2_REFSEL(1);  // Voltage ref internal (1V2)
      ADC1_SC2 |= ADC_SC2_DMAEN;        // Enable DMA
      // (39.4.7) Status and Control Register 3
      ADC1_SC3  = ADC_SC3_AVGE;         // Enable hardware averaging
      ADC1_SC3 |= ADC_SC3_AVGS(1);      // Hardware average 8 samples
      // Calibration de l'ADC 1
      adcCalibrate();

      // (13.2.7) System options register 7
      SIM_SOPT7 |= SIM_SOPT7_ADC1ALTTRGEN;  // Alt trigger select
      SIM_SOPT7 |= SIM_SOPT7_ADC1TRGSEL(4); // PIT trigger 0 select

      // (39.4.1) ADC Status and Control Registers 1 (ADCx_SC1n)
      uint32_t sc1a_config = 0;
      sc1a_config |= ADC_SC1_ADCH(TsyADCtoARMpin[iPin]);  // Channel selection
      sc1a_config &= ~ADC_SC1_DIFF;                       // DIFF=0, Single-ended mode
      //sc1a_config |= ADC_SC1_DIFF;                      // DIFF=1, Differential mode
      sc1a_config &= ~ADC_SC1_AIEN;                       // Disable interrupts
      ADC1_SC1A = sc1a_config;

      // (39.4.6) Status and Control Register 2
      ADC1_SC2 |= ADC_SC2_ADTRG;    // Enable hardware trigger
#endif
    }
  
    //-------------------------------------------------------------------------
    //! \brief Calibration du DAC
    virtual void adcCalibrate()
    {
#if defined(__IMXRT1062__) // Teensy 4.1 *** ATTENTION sur T4.1 ADC2 = ADC1 T3.6 ***
      // Calibration ADC2
      ADC2_GC &= ~ADC_GC_CAL;                                           // Reset Cal bit
      ADC2_GS = ADC_GS_CALF;                                            // Reset calibration status
      ADC2_GC = ADC_GC_CAL;                                             // Start calibration
      while (ADC2_GC & ADC_GC_CAL);                                     // Wait for calibration
      if ((ADC2_GS & ADC_GS_CALF) > 0) Serial.println("Acquisition::adcCalibrate Teensy 4.1 Error calibration");
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
      uint16_t sum;
      // Begin calibration, initiate automatic calibration sequence
      ADC1_SC3 = ADC_SC3_CAL;
      // Wait for calibration
      while (ADC1_SC3 & ADC_SC3_CAL);
      if (ADC1_SC3 & ADC_SC3_CALF)
         Serial.println("Calibration Failed !!!");
      // Plus side gain
      sum = (ADC1_CLP0 + ADC1_CLP1 + ADC1_CLP2 + ADC1_CLP3 + ADC1_CLP4 + ADC1_CLPS) >> 1;
      sum |= (1 << 15);
      ADC1_PG = sum;
      // Minus side gain (not used in single-ended mode)
      sum = (ADC1_CLMS + ADC1_CLM4 + ADC1_CLM3 + ADC1_CLM2 + ADC1_CLM1 + ADC1_CLM0) >> 1;
      sum |= (1 << 15);
      ADC1_MG = sum;
#endif
    }
    
    //-------------------------------------------------------------------------
    //! \brief Init du PIT 0 (Periodic Interrupt Timer) pour générer la Fe
    virtual void pitInit()
    {
#if defined(__IMXRT1062__) // Teensy 4.1
      // Set TMR4 Timer 3
      int comp3 = ((float)F_BUS_ACTUAL) / (float)iFe / 2.0;
      //Serial.printf("Acquisition::pitInit F_CPU_ACTUAL %d, F_BUS_ACTUAL %d, iFe %d, comp1 %d\n", F_CPU_ACTUAL, F_BUS_ACTUAL, iFe, comp3);
      // 54.9.1.15 Timer Channel Enable Register
      TMR4_ENBL &= ~(1<<3);             // Disable Timer
      // 54.9.1.5 Timer Channel Load Register
      TMR4_LOAD3 = 0;                   // Reset load register
      // 54.9.1.2 Timer Channel Compare Register 1
      TMR4_COMP13  = comp3;             // Set up compare 1 register
      // 54.9.1.10 Timer Channel Comparator Load Register 1
      TMR4_CMPLD13 = comp3;             // Also set the compare preload register
      // 54.9.1.12 Timer Channel Comparator Status and Control Register
      TMR4_CSCTRL3 = //DBG_EN = 0       // Debug Actions Enable, Normal operation
                 // FAULT = 0           // Fault Enable, Fault function disabled
                 // ALT_LOAD = 0        // Alternative Load Enable, Counter can be re-initialized only with the LOAD register.
                 // ROC = 0             // Reload on Capture, Do not reload the counter on a capture event
                 // TCI = 0             // Triggered Count Initialization Control, Stop counter upon receiving a second trigger event while still counting from the first trigger event
                 // UP = 0              // read only
                 // TCF2EN = 0          // Timer Compare 2 Interrupt Enable, no interrupt
                   TMR_CSCTRL_TCF1EN    // Timer Compare 1 Interrupt Enable, Interrupt
                 // TCF2 = 0            // Timer Compare 2 Interrupt Flag
                 // TCF1 = 0            // Timer Compare 1 Interrupt Flag 
                 // CL2 = 0             // Compare Load Control 2, Never preload
                 | TMR_CSCTRL_CL1(1);   // Compare Load Control 1, Load upon successful compare with the value in COMP1
      // 54.9.1.8 Timer Channel Control Register
      TMR4_CTRL3 = TMR_CTRL_CM(1)       // Count Mode, Count rising edges of primary source
                 | TMR_CTRL_PCS(8)      // Primary Count Source, IP bus clock divide by 1 prescaler
                 // SCS = 0             // Secondary Count Source, Counter 0 input pin
                 // ONCE = 0            // Count Once, Count repeatedly
                 | TMR_CTRL_LENGTH      // Count Length, Count until compare, then re-initialize
                 // DIR = 0             // Count Direction, Count Up
                 // COINIT = 0          // Co-Channel Initialization, no Co-Channel
                 | TMR_CTRL_OUTMODE(3); // Output Mode, Toggle OFLAG output on successful compare
      // 54.9.1.14 Timer Channel DMA Enable Register
      TMR4_DMA3 = TMR_DMA_CMPLD1DE;     // Comparator Preload Register 1 DMA Enable
      // 54.9.1.7 Timer Channel Counter Register
      TMR4_CNTR3 = 0;
      // 54.9.1.15 Timer Channel Enable Register
      TMR4_ENBL |= (1<<3);              // Enable Timer
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
      // Calcul de la période en fonction de la Fe
      uint32_t PitPeriod = (F_BUS / iFe) - 1;
      //Serial.printf("pitInit F_BUS %d, iFe %d, PitPeriod %d, uiNbSamplesBuffer %d\n", F_BUS, iFe, PitPeriod, uiNbSamplesBuffer);
      // (13.2.16) Send system clock to PIT
      SIM_SCGC6 |= SIM_SCGC6_PIT;
      // (46.4.1) Turn on PIT
      PIT_MCR = 0x00;
      // (46.4.4) Set timer 0 period
      PIT_LDVAL0 = PitPeriod;
      // (46.4.6) Enable timer 0
      PIT_TCTRL0 |= PIT_TCTRL_TEN;
#endif
    }
    
    //-------------------------------------------------------------------------
    //! \brief Init du DMA2 pour remplir le double buffer avec interruption à la moitiée et sur la totalité
    //! Après remplissage, le DMA continue au début
    virtual void dmaInit()
    {
#if defined(__IMXRT1062__) // Teensy 4.1
      // Initialisation du DMA 5
      ch = 5;
      // 14.7.26 CCM Clock Gating Register 5
      CCM_CCGR5 |= CCM_CCGR5_DMA(CCM_CCGR_ON);                          // DMA clock enable

      // 6.5.5.2 Control
      DMA_CR = DMA_CR_EMLM;                                             // CX and ECX Normal operation, GRP1PRI and GRP1PRI 0, EMLM 4 fields in ATTR,
                                                                        // CLM Continuous link mode is off, HALT and HOE Normal operation,
                                                                        // ERGA and ERCA Fixed priority arbitration, EDBG When the chip is in Debug mode, the eDMA continues to operate
      DMA_CR = DMA_CR_GRP1PRI | DMA_CR_EMLM;// | DMA_CR_EDBG;

      // 6.5.5.6 Clear Enable Error Interrupt
      DMA_CEEI = ch;                                                    // Clear chanel 5
      // 6.5.5.8 Clear Enable Request
      DMA_CERQ = ch;                                                    // Clear chanel 5
      // 6.5.5.12 Clear Error
      DMA_CERR = ch;                                                    // Clear chanel 5
      // 6.5.5.13 Clear Interrupt Request
      DMA_CINT = ch;                                                    // Clear chanel 5
      // 6.5.5.19 TCD Source Address
      IMXRT_DMA_TCD[ch].SADDR = &(IMXRT_ADC_ETC.TRIG[4].RESULT_1_0);    // Source address is ADC2
      // 6.5.5.20 TCD Signed Source Address Offset
      IMXRT_DMA_TCD[ch].SOFF = 0;                                       // Don't advance source pointer after minor loop
      // 6.5.5.21 TCD Transfer Attributes
      IMXRT_DMA_TCD[ch].ATTR  = DMA_TCD_ATTR_SSIZE(1);                  // Source data transfer size (16-bit), source and destination address modulo feature is disabled
      IMXRT_DMA_TCD[ch].ATTR |= DMA_TCD_ATTR_DSIZE(1);                  // Destination data transfer size (16-bit)
      // 6.5.5.22 TCD Minor Byte Count
      IMXRT_DMA_TCD[ch].NBYTES_MLNO = sizeof(uint16_t);                 // Number of bytes to transfer (in each service request)
      // 6.5.5.25 TCD Last Source Address Adjustment
      IMXRT_DMA_TCD[ch].SLAST = 0;                                      // Don't advance source pointer after major loop
      // 6.5.5.26 TCD Destination Address
      IMXRT_DMA_TCD[ch].DADDR = pBuffEch;                               // Destination Address
      // 6.5.5.27 TCD Signed Destination Address Offset
      IMXRT_DMA_TCD[ch].DOFF = sizeof(uint16_t);                        // Advance destination pointer by 2 bytes per value
      // 6.5.5.28 TCD Current Minor Loop Link, Major Loop Count
      IMXRT_DMA_TCD[ch].CITER_ELINKNO = uiNbSamplesBuffer * 2;          // Current Major Iteration Count
      // 6.5.5.30 TCD Last Destination Address Adjustment/Scatter Gather Address
      IMXRT_DMA_TCD[ch].DLASTSGA = -(sizeof(uint16_t) * uiNbSamplesBuffer * 2); // Return to &pBuffEch[0] after major loop
      // 6.5.5.31 TCD Control and Status
      IMXRT_DMA_TCD[ch].CSR  = DMA_TCD_CSR_INTMAJOR;                    // Interrupt at major loop (CITER == 0)
      IMXRT_DMA_TCD[ch].CSR |= DMA_TCD_CSR_INTHALF;                     // Also interrupt at half major loop (CITER == BITER/2)
      // 6.5.5.32 TCD Beginning Minor Loop Link, Major Loop Count
      IMXRT_DMA_TCD[ch].BITER_ELINKNO = uiNbSamplesBuffer * 2;          // Starting Major Iteration Count

      // 5.6.1.2 Channel a Configuration Register
      volatile uint32_t *mux = &DMAMUX_CHCFG0 + ch;
      *mux = 0;
      *mux = (DMAMUX_SOURCE_ADC_ETC & 0x7F) | DMAMUX_CHCFG_ENBL;        // Route ADC_ETC to DMA_MUX
      
      // 6.5.5.9 Set Enable Request
      DMA_SERQ = ch;                                                    // Enable DMA

      // Set ISR vector with highest priority
      _VectorsRam[ch + IRQ_DMA_CH0 + 16] = dma_T41_isr;
      NVIC_ENABLE_IRQ(IRQ_DMA_CH0 + ch);
      //NVIC_SET_PRIORITY(IRQ_DMA_CH0 + ch, 0);
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
      // (13.2.16-17) System clock gating control registers 6 and 7
      SIM_SCGC7 |= SIM_SCGC7_DMA;     // Enable DMA clock
      SIM_SCGC6 |= SIM_SCGC6_DMAMUX;  // Enable DMAMUX clock

      // (24.3.11) Clear Error Register
      DMA_CERR = DMA_CERR_CAEI; // Clear all bits in ERR
      // (24.3.1) Control Register
      DMA_CR = 0;
      DMA_CR = DMA_CR_GRP0PRI;
      
      // ======= Source =======
      // (24.3.18) TCD Source Address
      DMA_TCD2_SADDR = &ADC1_RA;
      // (24.3.20) TCD Transfer Attributes
      DMA_TCD2_ATTR = 0x0000;
      DMA_TCD2_ATTR |= DMA_TCD_ATTR_SSIZE(1);             // Source data transfer size (16-bit)
      // (24.3.19) TCD Signed Source Address Offset
      DMA_TCD2_SOFF = 0;                                  // Don't advance source pointer after minor loop
      // (24.3.24) TCD Last Source Address Adjustment
      DMA_TCD2_SLAST = 0;                                 // Don't advance source pointer after major loop
      // Number of bytes to transfer (in each service request)
      DMA_TCD2_NBYTES_MLNO = sizeof(uint16_t);

      // ======= Destination =======
      // (21.3.24) TCD Destination Address
      DMA_TCD2_DADDR = pBuffEch;
      // (24.3.20) TCD Transfer Attributes
      DMA_TCD2_ATTR |= DMA_TCD_ATTR_DSIZE(1); // Destination data transfer size (16-bit)
      // (24.3.26) TCD Signed Destination Address Offset
      DMA_TCD2_DOFF = sizeof(uint16_t);       // Advance destination pointer by 2 bytes per value      
      // (24.3.29) TCD Last Destination Address Adjustment/Scatter Gather Address
      DMA_TCD2_DLASTSGA = -(sizeof(uint16_t) * uiNbSamplesBuffer * 2); // Return to &pBuffEch[0] after major loop

      // (24.3.28) TCD Current Minor Loop Link, Major Loop Count (Channel Linking Disabled)
      DMA_TCD2_CITER_ELINKNO = uiNbSamplesBuffer * 2;  // Current Major Iteration Count
      // (24.3.32) TCD Beginning Minor Loop Link, Major Loop Count (Channel Linking Disabled)
      DMA_TCD2_BITER_ELINKNO = uiNbSamplesBuffer * 2;  // Starting Major Iteration Count

      // ======= Interrupts =======
      // (24.3.30) TCD Control and Status
      DMA_TCD2_CSR  = DMA_TCD_CSR_INTMAJOR; // Interrupt at major loop (CITER == 0)
      DMA_TCD2_CSR |= DMA_TCD_CSR_INTHALF;  // Also interrupt at half major loop (CITER == BITER/2)
      // Enable interrupt request
      NVIC_ENABLE_IRQ(IRQ_DMA_CH2);
    
      // ======= Triggering =======
      // (23.4.1) Channel Configuration Register
      // Set ADC as source (CH 2), enable DMA MUX
      DMAMUX0_CHCFG2 = DMAMUX_DISABLE;
      DMAMUX0_CHCFG2 = DMAMUX_SOURCE_ADC1 | DMAMUX_ENABLE;
    
      // Enable request input signal for channel 2
      DMA_SERQ |= DMA_SERQ_SERQ(2); // Enable channel 2 DMA requests
#endif
    }

#if defined(__IMXRT1062__) // Teensy 4.1
    // DMA channel (right)
    uint32_t ch;
#endif
    
    //! Résolution, en nombre de bits, de l'ADC
    unsigned short iRes;

    //! Fréquence d'échantillonnage en Hz
    unsigned int iFe;

    //! N° de la broche d'entrée de l'ADC
    unsigned short iPin;

    //! Pointeur vers le double buffer d'échantillons utilisé par le DMA
    uint16_t *pBuffEch;

    //! Taille d'un buffer d'échantillons
    uint16_t uiNbSamplesBuffer;

    //! Indique si l'acquisition est active (true) ou non (false)
    bool bAcquisition;
};

#endif
