var struct_monitoring_band =
[
    [ "fMax", "struct_monitoring_band.html#a430844c782db4de8f3d8fb6c86e6cdcf", null ],
    [ "fMin", "struct_monitoring_band.html#adbed5ed7cf3753f7a221909df28fa528", null ],
    [ "fMinFWidth", "struct_monitoring_band.html#adde1a38fdba3d2c9c7856f809892ed90", null ],
    [ "iMaxDuration", "struct_monitoring_band.html#a89b592f1f5883feeca99211d278134ec", null ],
    [ "iMinDuration", "struct_monitoring_band.html#a3ad9b97df93f10fbc3fb8e459c0255c8", null ],
    [ "iNbDetections", "struct_monitoring_band.html#ab9c94f9332a807347f93ab8d56c3101a", null ],
    [ "iType", "struct_monitoring_band.html#a94476f0fe6a34522a208a6a022ab1936", null ],
    [ "sName", "struct_monitoring_band.html#a2173eaeac942e7fe927f7688fe0f4425", null ]
];