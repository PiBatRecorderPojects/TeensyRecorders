var class_c_mode_synchro =
[
    [ "CModeSynchro", "class_c_mode_synchro.html#ae38a3d882c9ce7673944bffb7a42685d", null ],
    [ "BeginMode", "class_c_mode_synchro.html#af8baa005091c39553163d8c3b5d729ab", null ],
    [ "EndMode", "class_c_mode_synchro.html#aaecbda213d972751f13e43a978361773", null ],
    [ "KeyManager", "class_c_mode_synchro.html#a28e89e252d9e7a01fdd970b57a347fe5", null ],
    [ "OnIsrRecord", "class_c_mode_synchro.html#a5c218d233678a0b35a5fd218e6329218", null ],
    [ "OnLoop", "class_c_mode_synchro.html#a056583d3bd97c76abf4d881814e57a0e", null ],
    [ "PrintMode", "class_c_mode_synchro.html#a871ae5539c09caa231135046388a64af", null ],
    [ "bAffInit", "class_c_mode_synchro.html#adc600bc4b3ea30cc4b3a98d3a37dc7a5", null ],
    [ "bConfMini", "class_c_mode_synchro.html#a110d39d1e4ad36cdbbc29780b8260661", null ],
    [ "iPRType", "class_c_mode_synchro.html#a49567e1e175f277dd35979e57362d18b", null ],
    [ "iTimeOut", "class_c_mode_synchro.html#ac412b7355001f7fa9709b6bcf07eda68", null ],
    [ "iTimeRecord", "class_c_mode_synchro.html#a0018d620f44d3a60cedd87bf5cf67371", null ],
    [ "iTimeReset", "class_c_mode_synchro.html#a04cafa429468fd70ceae49925892673e", null ],
    [ "pESP32Link", "class_c_mode_synchro.html#a90999eb2ca11aa0cb9246da7b912fcb7", null ]
];