/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Teensy Recorder", "index.html", [
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", "globals_vars" ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", "globals_eval" ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_c_analyse_r_l_8cpp.html",
"_c_mode_params_8cpp.html#aba278e2573f719997abf8b6924519517",
"_c_mode_recorder_8cpp.html#abb91207da375f664a0fb3339c80d28a2",
"_c_timed_recording_8h.html#a9eda79feae8755253b998ac1bc586cd3a6dc1b6f8d0a0f4d92f248e6c45120642",
"_const_char_8c.html#a2aeb72bdf896caa9969442ec931d9ea9",
"_write_b_m_p16_c_8h.html#a7508101be8133b5672dd1102301815cdaf9c699c0433e2991fe5934fcb7a33da2",
"class_c_graphe128k_hz.html#a71cf78f3aa8b9dfa3c4869c7b74834da",
"class_c_mode_protocole_routier.html#a72bff0bd0648e3bc74d61dbc85fd306f",
"class_c_recorder_h.html#a546937aa4708b940d112555cd4f73342",
"globals_eval.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';