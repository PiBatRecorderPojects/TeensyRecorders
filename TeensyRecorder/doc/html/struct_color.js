var struct_color =
[
    [ "Color", "struct_color.html#a789b6f26e5b6027ba99c442805c9f0cd", null ],
    [ "Color", "struct_color.html#a9a742cbe9f9f4037f5d9f4e81a9b2428", null ],
    [ "iAlpha", "struct_color.html#ad2e3f7a0b8a180a8a1ae2011b6268c83", null ],
    [ "iBlue", "struct_color.html#a6f50f0e50cc1be60a3b12ff26dbe76ca", null ],
    [ "iGreen", "struct_color.html#a1931e3e531377804ed4ddf2a55a1a66a", null ],
    [ "iRed", "struct_color.html#a640fb7a14896f16ac5c8911fb8d5ff51", null ]
];