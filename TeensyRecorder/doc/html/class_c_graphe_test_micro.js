var class_c_graphe_test_micro =
[
    [ "CGrapheTestMicro", "class_c_graphe_test_micro.html#a694f6762af366a3508e7bbe5abf1c2db", null ],
    [ "~CGrapheTestMicro", "class_c_graphe_test_micro.html#aaaeee9ea1a93003e595015c81dd9b270", null ],
    [ "PrintGraphe", "class_c_graphe_test_micro.html#aa46772f1dc92c9033a82781e13765e53", null ],
    [ "SetFrequence", "class_c_graphe_test_micro.html#ae6fcd2c51364410a41ce0179f477dd85", null ],
    [ "iFrMax", "class_c_graphe_test_micro.html#adfe95534c8b86b94b467897c42393531", null ],
    [ "iFrMin", "class_c_graphe_test_micro.html#ade09af341e299994433e80bc67739f49", null ],
    [ "iNbLines", "class_c_graphe_test_micro.html#a2914ff33067ce0fd55c74c5bb3f30681", null ],
    [ "iSizeGraph", "class_c_graphe_test_micro.html#a700dbcb97465936b835f813af4343444", null ],
    [ "iY", "class_c_graphe_test_micro.html#a4d31db680d55fe93ca55c0a1886f8dd6", null ],
    [ "pTbGraphe", "class_c_graphe_test_micro.html#ac053fb1748ee7b59c2996f5fbc300890", null ],
    [ "uiNextScroll", "class_c_graphe_test_micro.html#a15be117e6afbdbfa2d97882e6ba1a63b", null ],
    [ "uiTimeScroll", "class_c_graphe_test_micro.html#a491008c4d88a94d15e6e3c1701d81577", null ]
];