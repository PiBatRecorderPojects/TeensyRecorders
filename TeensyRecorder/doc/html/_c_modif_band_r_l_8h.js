var _c_modif_band_r_l_8h =
[
    [ "CModeBandsRL", "class_c_mode_bands_r_l.html", "class_c_mode_bands_r_l" ],
    [ "IDXMODIFBANDS", "_c_modif_band_r_l_8h.html#a35017c8b82469835a457c930b5e24cce", [
      [ "IDXRETURN", "_c_modif_band_r_l_8h.html#a35017c8b82469835a457c930b5e24ccea9428229872204d0af90b1522d948a94c", null ],
      [ "IDXNUMBAND", "_c_modif_band_r_l_8h.html#a35017c8b82469835a457c930b5e24ccea8f4be1b21d4a1f2c0f680243876ee142", null ],
      [ "IDXNAME", "_c_modif_band_r_l_8h.html#a35017c8b82469835a457c930b5e24ccea481e2209af8f9aadfa46505da6599399", null ],
      [ "IDXTYPE", "_c_modif_band_r_l_8h.html#a35017c8b82469835a457c930b5e24ccea9d7c596db15542c427e7e677e8252156", null ],
      [ "IDXNBDET", "_c_modif_band_r_l_8h.html#a35017c8b82469835a457c930b5e24cceaa1ef1c5b503f0fd44db09bf29f01679d", null ],
      [ "IDXBDFMIN", "_c_modif_band_r_l_8h.html#a35017c8b82469835a457c930b5e24ccea7a0675fa0e419373ca7261ef881a5a75", null ],
      [ "IDXBDFMAX", "_c_modif_band_r_l_8h.html#a35017c8b82469835a457c930b5e24ccea816d403be39dab92ef10b950f4c4a54f", null ],
      [ "IDXMINDUR", "_c_modif_band_r_l_8h.html#a35017c8b82469835a457c930b5e24cceab59679955876f9730280d74bf362beb2", null ],
      [ "IDXMAXDUR", "_c_modif_band_r_l_8h.html#a35017c8b82469835a457c930b5e24ccead39cdfcf2616a59b1c43a385aecfcc46", null ],
      [ "IDXMINWIDTH", "_c_modif_band_r_l_8h.html#a35017c8b82469835a457c930b5e24cceaf53dacde827e6ea686d9f42ecdbeae59", null ]
    ] ]
];