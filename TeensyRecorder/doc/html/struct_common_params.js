var struct_common_params =
[
    [ "bLumiereMin", "struct_common_params.html#a263f3440f4606e17de1e689a09f32ea5", null ],
    [ "iLanguage", "struct_common_params.html#a9fa575fc856f512384ed7247533ca7e6", null ],
    [ "iScreenType", "struct_common_params.html#ab4b38e7e081a567de78028399131debe", null ],
    [ "iSelProfile", "struct_common_params.html#a7a4f9f877730912960b7d4612611358b", null ],
    [ "iUserLevel", "struct_common_params.html#a21fc687129c13c5e638d3cffea5c0457", null ],
    [ "iUtilLed", "struct_common_params.html#a55bd9df53b8267b83a958ed2114372e3", null ],
    [ "ProfilesNames", "struct_common_params.html#a364330f4e6a61e81935d872426c1a161", null ]
];