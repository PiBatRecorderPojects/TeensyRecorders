var _c_e_s_p32_link_8h =
[
    [ "CESP32Link", "class_c_e_s_p32_link.html", "class_c_e_s_p32_link" ],
    [ "CMasterLink", "class_c_master_link.html", "class_c_master_link" ],
    [ "CSlaveLink", "class_c_slave_link.html", "class_c_slave_link" ],
    [ "MASTERSTATUS", "_c_e_s_p32_link_8h.html#acda48f1fd2e181874ed4c803dd5c305c", [
      [ "MSSINIT", "_c_e_s_p32_link_8h.html#acda48f1fd2e181874ed4c803dd5c305caa0e32cc2c4770bc2bf9d65a656c44113", null ],
      [ "MSWAITINGESP32", "_c_e_s_p32_link_8h.html#acda48f1fd2e181874ed4c803dd5c305ca441e94dfc911de181645e6ebba7baf30", null ],
      [ "MSWAITEESP32", "_c_e_s_p32_link_8h.html#acda48f1fd2e181874ed4c803dd5c305cae0da5949622b3330d59d7627d2829890", null ],
      [ "MSWAITINGSLAVES", "_c_e_s_p32_link_8h.html#acda48f1fd2e181874ed4c803dd5c305ca01dc0a93ba708c62c8d2c602ae98dbb9", null ],
      [ "MSMASTEROK", "_c_e_s_p32_link_8h.html#acda48f1fd2e181874ed4c803dd5c305ca8215ae07af5f364919c20612d37071bf", null ],
      [ "MSWAITINGVERS", "_c_e_s_p32_link_8h.html#acda48f1fd2e181874ed4c803dd5c305ca6bd50dbbc7775604defd7598ceabb5c9", null ],
      [ "MSTIMEOUTESP32", "_c_e_s_p32_link_8h.html#acda48f1fd2e181874ed4c803dd5c305ca9a3be7716e87d47013df88b8fac2b939", null ],
      [ "MSTIMEOUTSLAVES", "_c_e_s_p32_link_8h.html#acda48f1fd2e181874ed4c803dd5c305ca53164b5b62f7fe0bb8e2f9f61dfa07a2", null ],
      [ "MSWAITINGPARAMS", "_c_e_s_p32_link_8h.html#acda48f1fd2e181874ed4c803dd5c305ca261347ff0204540fb06dfad0ea070ddf", null ],
      [ "MSWAITINGSEND", "_c_e_s_p32_link_8h.html#acda48f1fd2e181874ed4c803dd5c305ca615b61bf8d7ad5300b12d36b8f08b0b8", null ],
      [ "MSSLAVEOK", "_c_e_s_p32_link_8h.html#acda48f1fd2e181874ed4c803dd5c305ca18a4f72dfcb4f3a9359232ca3ade2d21", null ],
      [ "MSSLAVESTOP", "_c_e_s_p32_link_8h.html#acda48f1fd2e181874ed4c803dd5c305caca1fce6c54486bfa60ab84ed012530c0", null ],
      [ "MSTIMEOUTMASTER", "_c_e_s_p32_link_8h.html#acda48f1fd2e181874ed4c803dd5c305ca37df543fc6a05b97d5bcd43496744202", null ],
      [ "MSERRORPARAMS", "_c_e_s_p32_link_8h.html#acda48f1fd2e181874ed4c803dd5c305cad4174565a98e19b333b68ce7e000fd4f", null ],
      [ "MSSLAVEERROR", "_c_e_s_p32_link_8h.html#acda48f1fd2e181874ed4c803dd5c305ca5b0ff4a363a20cef0f6c3771aaf808cb", null ],
      [ "MSSMAX", "_c_e_s_p32_link_8h.html#acda48f1fd2e181874ed4c803dd5c305ca14a9589198750eb9de2dd045f69d8272", null ]
    ] ]
];