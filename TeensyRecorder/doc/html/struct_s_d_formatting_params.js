var struct_s_d_formatting_params =
[
    [ "BU16", "struct_s_d_formatting_params.html#a4da38fb0cecd2fce9672ee1874e4ff9e", null ],
    [ "BU32", "struct_s_d_formatting_params.html#ae5c2fa29f581ebdc52b3f7d87a2f8577", null ],
    [ "cardCapacityMB", "struct_s_d_formatting_params.html#a8cc426301524a76765a4ab66ca26849a", null ],
    [ "cardSizeBlocks", "struct_s_d_formatting_params.html#a682ca154eeb8b91a96f20f40e7d7b55e", null ],
    [ "dataStart", "struct_s_d_formatting_params.html#a92f60260a2d3fc2ea4c0060157c9542e", null ],
    [ "fatSize", "struct_s_d_formatting_params.html#abde60e1116113ec2057484b8c4e26922", null ],
    [ "fatStart", "struct_s_d_formatting_params.html#abccb7d827af681c3393eda17be890cf6", null ],
    [ "numberOfHeads", "struct_s_d_formatting_params.html#a07f7e9239a872ea39b5a5c56ad696a66", null ],
    [ "partSize", "struct_s_d_formatting_params.html#af00eedcb3a5afc039e5fba4d8ad00992", null ],
    [ "partType", "struct_s_d_formatting_params.html#a48014b24ae1c59e0b6c12b51fbedf8e9", null ],
    [ "relSector", "struct_s_d_formatting_params.html#ae2b46ea7c5259776b6eab7638b48c8a5", null ],
    [ "reservedSectors", "struct_s_d_formatting_params.html#a4b11486eca8a0896ab98a50c4da6bdaf", null ],
    [ "sectorsPerCluster", "struct_s_d_formatting_params.html#a1d327a56be55f45c7284dcb5f7f88e8d", null ],
    [ "sectorsPerTrack", "struct_s_d_formatting_params.html#a5f28cb5982f6be9a72e0786d5ef5bef9", null ]
];