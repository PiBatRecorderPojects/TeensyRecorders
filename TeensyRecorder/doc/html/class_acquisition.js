var class_acquisition =
[
    [ "Acquisition", "class_acquisition.html#af559456d597f94ea2a35682697fb13e1", null ],
    [ "~Acquisition", "class_acquisition.html#af697b51bd72afcfe8f9f3f368743d20d", null ],
    [ "adcCalibrate", "class_acquisition.html#a22942be461f069bd3e5c0a0cfb24188f", null ],
    [ "adcInit", "class_acquisition.html#a6f05506c51e1d62319222e8747e77a4c", null ],
    [ "dmaInit", "class_acquisition.html#ac03c62d10032e564211a88fbb8c12660", null ],
    [ "IsAcquisition", "class_acquisition.html#a0f1909e9bb314975bf92ce094f73b52e", null ],
    [ "pitInit", "class_acquisition.html#a6e58fa9fea62007329a192a24ffb62bd", null ],
    [ "setFe", "class_acquisition.html#a3a515c081210475ca592cb74b480bbab", null ],
    [ "start", "class_acquisition.html#af97ea73f535cc8580dafcbd126f46f8e", null ],
    [ "stop", "class_acquisition.html#a78ce526632d7f65d7c660133a29e759e", null ],
    [ "bAcquisition", "class_acquisition.html#a93ceb3cfc9e1f6e2e0b5b75dc0692379", null ],
    [ "iFe", "class_acquisition.html#a5f64d293762c1915053e9d24aa25ed5c", null ],
    [ "iPin", "class_acquisition.html#a27fc368657e3f3deb594cbc5333cb24f", null ],
    [ "iRes", "class_acquisition.html#a72ab20685166bdc3f912481e8ae81c9c", null ],
    [ "pBuffEch", "class_acquisition.html#a68377dac4d65600c2ed900af478de061", null ],
    [ "uiNbSamplesBuffer", "class_acquisition.html#abf93863661ca4eb028900f5bdfdf1156", null ]
];