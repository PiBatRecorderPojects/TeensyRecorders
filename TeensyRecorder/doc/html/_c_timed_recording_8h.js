var _c_timed_recording_8h =
[
    [ "CTimedRecording", "class_c_timed_recording.html", "class_c_timed_recording" ],
    [ "IDXTRPARAMS", "_c_timed_recording_8h.html#a9eda79feae8755253b998ac1bc586cd3", [
      [ "IDXTRRECORD", "_c_timed_recording_8h.html#a9eda79feae8755253b998ac1bc586cd3ac41aed7152605a14d26f9926b413e01f", null ],
      [ "IDXTRDOWNC", "_c_timed_recording_8h.html#a9eda79feae8755253b998ac1bc586cd3a903c53451e62b8e3ac8ba41a2a802005", null ],
      [ "IDXTRTIMER", "_c_timed_recording_8h.html#a9eda79feae8755253b998ac1bc586cd3afd3ff3db65250c9279e8fb2f2e9c44d3", null ],
      [ "IDXTRWAIT", "_c_timed_recording_8h.html#a9eda79feae8755253b998ac1bc586cd3a55940675f59b12e3fa985858c795585b", null ],
      [ "IDTRBAT", "_c_timed_recording_8h.html#a9eda79feae8755253b998ac1bc586cd3a665a743b68b50f36d1c19e8396a344a8", null ],
      [ "IDXTRHOUR", "_c_timed_recording_8h.html#a9eda79feae8755253b998ac1bc586cd3a5b7590dae2c14dd70cc7c58272b16675", null ],
      [ "IDXTRDATE", "_c_timed_recording_8h.html#a9eda79feae8755253b998ac1bc586cd3aa4882dcf3d4a57846380fbb3a8b42d59", null ],
      [ "IDXTRFE", "_c_timed_recording_8h.html#a9eda79feae8755253b998ac1bc586cd3a40656ec627ffa9ffca7b47203b820b9a", null ],
      [ "IDXTRSD", "_c_timed_recording_8h.html#a9eda79feae8755253b998ac1bc586cd3a3e604ba04f60a9cfa40e3633426eee75", null ],
      [ "IDXTRLEAVE", "_c_timed_recording_8h.html#a9eda79feae8755253b998ac1bc586cd3a6dc1b6f8d0a0f4d92f248e6c45120642", null ],
      [ "IDXMAXTREC", "_c_timed_recording_8h.html#a9eda79feae8755253b998ac1bc586cd3a8dc044b9d38250c6c95c3e18efba88b1", null ]
    ] ],
    [ "TRSCREENSTATUS", "_c_timed_recording_8h.html#a8f403065e9514546fd720f3e1e7c8861", [
      [ "TRSCRENTOPRINT", "_c_timed_recording_8h.html#a8f403065e9514546fd720f3e1e7c8861ad00d69904622729a7035519fd0098df0", null ],
      [ "TRSCREENOK", "_c_timed_recording_8h.html#a8f403065e9514546fd720f3e1e7c8861a59464cfbf00bcceb19a2bf83ddbb170d", null ],
      [ "TRSCREENTOCLEAR", "_c_timed_recording_8h.html#a8f403065e9514546fd720f3e1e7c8861a25c86d6a7311c3f4e03d48c621cec7cb", null ],
      [ "TRSCREENCLEAR", "_c_timed_recording_8h.html#a8f403065e9514546fd720f3e1e7c8861a92e254fc67339a19629aa2d5abb93c6c", null ]
    ] ],
    [ "TRSTATUS", "_c_timed_recording_8h.html#a5d555213bcff4e3737f182c7998ccf01", [
      [ "TRSTATUSTORECORD", "_c_timed_recording_8h.html#a5d555213bcff4e3737f182c7998ccf01ac1278e0c851838433a338421aaa3db4e", null ],
      [ "TRSTATUSRECORDING", "_c_timed_recording_8h.html#a5d555213bcff4e3737f182c7998ccf01ab9168c51c015f853123fc1589fb2b24e", null ],
      [ "TRSTATUSENDRECORD", "_c_timed_recording_8h.html#a5d555213bcff4e3737f182c7998ccf01a1700e070b65c3f8b52eddfa29aa08bd3", null ],
      [ "TRSTATUSWAITING", "_c_timed_recording_8h.html#a5d555213bcff4e3737f182c7998ccf01a66c9b6aa0afe77df85c20c438c7ecdde", null ]
    ] ]
];