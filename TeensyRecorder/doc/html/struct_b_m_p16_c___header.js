var struct_b_m_p16_c___header =
[
    [ "BM", "struct_b_m_p16_c___header.html#af0a330193368c2110fb76e94991e9109", null ],
    [ "uiBitmapInfoSize", "struct_b_m_p16_c___header.html#aa33e7d416fb966491b4bf398c5887c1b", null ],
    [ "uiBitsByPixel", "struct_b_m_p16_c___header.html#ae7f226636b3bee03a36bfe9da48e5206", null ],
    [ "uiColorsNumber", "struct_b_m_p16_c___header.html#a53c72df113fcebcbd50c9f1294f31736", null ],
    [ "uiCompression", "struct_b_m_p16_c___header.html#a4dc6b8eee25c82c1617f92437e054bee", null ],
    [ "uiFileSize", "struct_b_m_p16_c___header.html#a1a6630b4f84bb270d7dd64a4d11c1882", null ],
    [ "uiHeight", "struct_b_m_p16_c___header.html#a03bd8f0a31490902a7352c77aada5213", null ],
    [ "uiHorizontalResolution", "struct_b_m_p16_c___header.html#afc2085b31c90d9b700e3dfa2bb58b31c", null ],
    [ "uiNbPlan", "struct_b_m_p16_c___header.html#a58eda5061a7f5ffcebbadbcd7b29e3a2", null ],
    [ "uiOffset", "struct_b_m_p16_c___header.html#a4e29aca98e6818c5932a26c65b43d068", null ],
    [ "uiReserved", "struct_b_m_p16_c___header.html#a6567ce3cc9e7fe563e2e50880b5c077e", null ],
    [ "uiSizeOfPicture", "struct_b_m_p16_c___header.html#a5142c26111db0bd55e1652ebafbe5a93", null ],
    [ "uiVerticalResolution", "struct_b_m_p16_c___header.html#a8821b66152e14517e813b4564396af6c", null ],
    [ "uiWidth", "struct_b_m_p16_c___header.html#ab51ef7122a940deb0b6c50a6d490d25d", null ]
];