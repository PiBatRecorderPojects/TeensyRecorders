var class_m_t_p_storage_interface =
[
    [ "close", "class_m_t_p_storage_interface.html#aed4532032b02c4890cea54786dac9ccc", null ],
    [ "Create", "class_m_t_p_storage_interface.html#a1e3e8e6a8647bbd34c4e842312b80ab2", null ],
    [ "DeleteObject", "class_m_t_p_storage_interface.html#a2135faf6a341a41e7bcba48e02022fc9", null ],
    [ "free", "class_m_t_p_storage_interface.html#ad668b28b65d0e6f3e2ce328af9cfb878", null ],
    [ "GetNextObjectHandle", "class_m_t_p_storage_interface.html#a500c749635b4fc8c4108812d2b073cee", null ],
    [ "GetObjectInfo", "class_m_t_p_storage_interface.html#a094658874018fec3b4ddc58b17378bac", null ],
    [ "GetSize", "class_m_t_p_storage_interface.html#abebadfb64f1a7751645115234d9a6757", null ],
    [ "has_directories", "class_m_t_p_storage_interface.html#ab69a93f72cf8f17c340070dde160b0a6", null ],
    [ "read", "class_m_t_p_storage_interface.html#a5b04a30cb560b9c0e5dfd54ca1292d84", null ],
    [ "readonly", "class_m_t_p_storage_interface.html#a5bb5d298af6c166f71d3b6526a54ec5d", null ],
    [ "size", "class_m_t_p_storage_interface.html#a08a2a5b84b529ae3aefb56338e8b5db2", null ],
    [ "StartGetObjectHandles", "class_m_t_p_storage_interface.html#a960019b4d783be3017fec6de8550b534", null ],
    [ "write", "class_m_t_p_storage_interface.html#a6bad735313fdd98f2fb8019179092745", null ]
];