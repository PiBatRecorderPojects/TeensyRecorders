var files_dup =
[
    [ "acquisition.h", "acquisition_8h.html", "acquisition_8h" ],
    [ "acquisitionS.h", "acquisition_s_8h.html", "acquisition_s_8h" ],
    [ "CAnalyseRL.cpp", "_c_analyse_r_l_8cpp.html", null ],
    [ "CAnalyseRL.h", "_c_analyse_r_l_8h.html", [
      [ "CAnalyzerRL", "class_c_analyzer_r_l.html", "class_c_analyzer_r_l" ],
      [ "CFCAnalyzerRL", "class_c_f_c_analyzer_r_l.html", "class_c_f_c_analyzer_r_l" ],
      [ "CQFCAnalyzerRL", "class_c_q_f_c_analyzer_r_l.html", "class_c_q_f_c_analyzer_r_l" ],
      [ "CFMAnalyzerRL", "class_c_f_m_analyzer_r_l.html", "class_c_f_m_analyzer_r_l" ]
    ] ],
    [ "CAudioRecorder.cpp", "_c_audio_recorder_8cpp.html", "_c_audio_recorder_8cpp" ],
    [ "CAudioRecorder.h", "_c_audio_recorder_8h.html", "_c_audio_recorder_8h" ],
    [ "CESP32Link.cpp", "_c_e_s_p32_link_8cpp.html", "_c_e_s_p32_link_8cpp" ],
    [ "CESP32Link.h", "_c_e_s_p32_link_8h.html", "_c_e_s_p32_link_8h" ],
    [ "CModeGeneric.cpp", "_c_mode_generic_8cpp.html", "_c_mode_generic_8cpp" ],
    [ "CModeGeneric.h", "_c_mode_generic_8h.html", "_c_mode_generic_8h" ],
    [ "CModeheterodyne.cpp", "_c_modeheterodyne_8cpp.html", "_c_modeheterodyne_8cpp" ],
    [ "CModeheterodyne.h", "_c_modeheterodyne_8h.html", "_c_modeheterodyne_8h" ],
    [ "CModeParams.cpp", "_c_mode_params_8cpp.html", "_c_mode_params_8cpp" ],
    [ "CModeParams.h", "_c_mode_params_8h.html", "_c_mode_params_8h" ],
    [ "CModePlayer.cpp", "_c_mode_player_8cpp.html", "_c_mode_player_8cpp" ],
    [ "CModePlayer.h", "_c_mode_player_8h.html", "_c_mode_player_8h" ],
    [ "CModeRecorder.cpp", "_c_mode_recorder_8cpp.html", "_c_mode_recorder_8cpp" ],
    [ "CModeRecorder.h", "_c_mode_recorder_8h.html", [
      [ "CModeGenericRec", "class_c_mode_generic_rec.html", "class_c_mode_generic_rec" ],
      [ "CModeAutoRecorder", "class_c_mode_auto_recorder.html", "class_c_mode_auto_recorder" ],
      [ "CModeProtocole", "class_c_mode_protocole.html", "class_c_mode_protocole" ],
      [ "CModeProtocoleRoutier", "class_c_mode_protocole_routier.html", "class_c_mode_protocole_routier" ],
      [ "CModeProtocolePedestre", "class_c_mode_protocole_pedestre.html", "class_c_mode_protocole_pedestre" ],
      [ "CModeProtocolePFixe", "class_c_mode_protocole_p_fixe.html", "class_c_mode_protocole_p_fixe" ],
      [ "CModeSynchro", "class_c_mode_synchro.html", "class_c_mode_synchro" ]
    ] ],
    [ "CModeRhinoLogger.cpp", "_c_mode_rhino_logger_8cpp.html", "_c_mode_rhino_logger_8cpp" ],
    [ "CModeRhinoLogger.h", "_c_mode_rhino_logger_8h.html", [
      [ "CModeRhinoLogger", "class_c_mode_rhino_logger.html", "class_c_mode_rhino_logger" ]
    ] ],
    [ "CModeTestMicro.cpp", "_c_mode_test_micro_8cpp.html", "_c_mode_test_micro_8cpp" ],
    [ "CModeTestMicro.h", "_c_mode_test_micro_8h.html", "_c_mode_test_micro_8h" ],
    [ "CModifBandRL.cpp", "_c_modif_band_r_l_8cpp.html", "_c_modif_band_r_l_8cpp" ],
    [ "CModifBandRL.h", "_c_modif_band_r_l_8h.html", "_c_modif_band_r_l_8h" ],
    [ "Const.c", "_const_8c.html", "_const_8c" ],
    [ "Const.h", "_const_8h.html", "_const_8h" ],
    [ "ConstChar.c", "_const_char_8c.html", "_const_char_8c" ],
    [ "CReader.cpp", "_c_reader_8cpp.html", "_c_reader_8cpp" ],
    [ "CReader.h", "_c_reader_8h.html", [
      [ "CReader", "class_c_reader.html", "class_c_reader" ]
    ] ],
    [ "CRecorder.cpp", "_c_recorder_8cpp.html", "_c_recorder_8cpp" ],
    [ "CRecorder.h", "_c_recorder_8h.html", "_c_recorder_8h" ],
    [ "CRecorderRL.cpp", "_c_recorder_r_l_8cpp.html", "_c_recorder_r_l_8cpp" ],
    [ "CRecorderRL.h", "_c_recorder_r_l_8h.html", [
      [ "CRecorderRL", "class_c_recorder_r_l.html", "class_c_recorder_r_l" ]
    ] ],
    [ "CTimedRecording.cpp", "_c_timed_recording_8cpp.html", "_c_timed_recording_8cpp" ],
    [ "CTimedRecording.h", "_c_timed_recording_8h.html", "_c_timed_recording_8h" ],
    [ "CWaveFile.cpp", "_c_wave_file_8cpp.html", null ],
    [ "CWaveFile.h", "_c_wave_file_8h.html", [
      [ "CWaveFile", "class_c_wave_file.html", "class_c_wave_file" ]
    ] ],
    [ "MTPsd.h", "_m_t_psd_8h.html", "_m_t_psd_8h" ],
    [ "RamMonitor.h", "_ram_monitor_8h.html", "_ram_monitor_8h" ],
    [ "Restitution.h", "_restitution_8h.html", [
      [ "Restitution", "class_restitution.html", "class_restitution" ]
    ] ],
    [ "WriteBMP16C.cpp", "_write_b_m_p16_c_8cpp.html", "_write_b_m_p16_c_8cpp" ],
    [ "WriteBMP16C.h", "_write_b_m_p16_c_8h.html", "_write_b_m_p16_c_8h" ]
];