var class_c_wave_file =
[
    [ "CWaveFile", "class_c_wave_file.html#a4b7eb059e75249788ca628533fea295f", null ],
    [ "~CWaveFile", "class_c_wave_file.html#aea59e99b66ec0c314cfcc239df26ca05", null ],
    [ "CloseWavfile", "class_c_wave_file.html#a129f5fc9851801b61eca54beccc5062e", null ],
    [ "GetDataLength", "class_c_wave_file.html#a367579e27fcba8336e920d8a767f4103", null ],
    [ "GetPlayDuration", "class_c_wave_file.html#adb7c6d80f7644593c6be64ef6937cd11", null ],
    [ "GetRecordDuration", "class_c_wave_file.html#a05b732fcd5e15a5311fdbe5d0c2567d9", null ],
    [ "GetSampleRate", "class_c_wave_file.html#ada12b5bbcda5e831bcc540291a29bb96", null ],
    [ "OpenWaveFileForRead", "class_c_wave_file.html#a149fb1cebfdd250220cbbe01ac102763", null ],
    [ "OpenWaveFileForWrite", "class_c_wave_file.html#affc551255f4d1e97c81a9e9e549bed21", null ],
    [ "ReadNext", "class_c_wave_file.html#a81148bf64f3fc5e01d850021cb5199cc", null ],
    [ "RestartWrite", "class_c_wave_file.html#a2fbca7aabdc5a60b8d289a8ee0cd2906", null ],
    [ "SetDecimation", "class_c_wave_file.html#ac747b9e18edba01d8ee9cf06b2cecd45", null ],
    [ "SetPosRead", "class_c_wave_file.html#ab92870b41dc657b66ce42ef400c09d7a", null ],
    [ "WavfileRead", "class_c_wave_file.html#a371a41b5d9920879fe5634cf20928604", null ],
    [ "WavfileWrite", "class_c_wave_file.html#a44c90e084d65f66a2510901f721b845e", null ]
];