//! \file CCodeLink.h
//! \brief Definition of the CCodeLink class for the serial Teensy Recorder to ESP32 dialogue and Lora message
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2019 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <TimeLib.h>
#include "Const.h"

#ifndef CCODELINK_H
#define CCODELINK_H

//! \enum LinkMessages
//! \brief Messages type
enum LinkMessages
{
  LMPARAMS     = 0,  //!< Send parameters
  LMSLAVE      = 1,  //!< Slave OK
  LMSTARTREC   = 2,  //!< Start recording (only on LORA link)
  LMSTOPREC    = 3,  //!< Stop recording  (only on LORA link)
  LMTOP        = 4,  //!< Top synchro     (only on LORA link)
  LMUNKNOW     = 5   //!< Unknow message
};


//-------------------------------------------------------------------------
//! \class CCodeLink
//! \brief Class for coding and decoding messages from ESP32/TEensy and Radio link
class CCodeLink
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor
  CCodeLink() {;};

  //-------------------------------------------------------------------------
  //! \brief Decode message type
  //! \param pMessage Message to decode
  //! \return Return message type
  int DecodeMessageType(
    char *pMessage
    )
  {
	  int iMessage = LMUNKNOW;
    // Vérification de la fin du message
    if (pMessage[strlen(pMessage)-1] == '!')
    {
      // Vérification de l'entête des messages
      if (strstr( pMessage, "$PAR") == pMessage)
        iMessage = LMPARAMS;
      else if (strstr( pMessage, "$SLAVE") == pMessage)
        iMessage = LMSLAVE;
      else if (strstr( pMessage, "$START") == pMessage)
        iMessage = LMSTARTREC;
      else if (strstr( pMessage, "$STOP") == pMessage)
        iMessage = LMSTOPREC;
      else if (strstr( pMessage, "$T") == pMessage)
        iMessage = LMTOP;
    }
	  return iMessage;
  };

  /*
	$PAR for parameters from master to slave
		Send by Link from Teensy Master to ESP32 Master
		Send by Radio from ESP32 Master to ESP32 Slave
		Send by Link from ESP32 Slave to Teensy Slave
	"$PAR,SlaveNum,DateHour,BeginHour,EndHour,AbsThres,Theshold,Fe,NumGain,TimeExp,HPFFreq,FreqTop,DurTop,TopPer!"
	- SlaveNum   Number of the slave (1 to 10)
	- DateHour   Date and Hour format DD/MM/YY-HH:mm:SS
	- BeginHour  Hour to beging recording format HH:mm
	- EndHour    Hour to stop recording format HH:mm
	- AbsThres   Threshold type, false (0) relative, true (1) absolute
	- Theshold   Threshold in dB, (5 to 99 in relative or -110 to -30 in absolute)
	- Fe         Sample rate (2 for 192kHz, 3 for 250kHz, 4 for 384kHz)
	- NumGain    Numeric gain (0 for 0dB, 1 for +6dB, 2 for +12dB, 3 for +18dB, 4 for +24dB)
	- TimeExp    Time expansion, true (1) for X10, false (0) for X1
	- HPFFreq    Hight pass filter frequency in kHz (0 to 25 kHz)
	- FreqTop    Top audio frequency in kHz (1 to 50 kHz)
	- DurTop     Top audio duration in samples (256, 512 or 1024)
	- TopPer     Top audio period (0 for one at the start of the recording, 1 to 10 for one every 1 to 10 seconds)
	*/
  //-------------------------------------------------------------------------
  //! \brief Code Parameters message
  //! \param pParams Parameters
  //! \return Return string of the message
  char *CodeParams(
    ParamsOperator *pParams
    )
  {
    char sTime[20];
    sprintf( sTime, "%02d/%02d/%02d-%02d:%02d:%02d", day(), month(), year()-2000, hour(), minute(), second());
    int iThreshold = pParams->iSeuilDet;
    if (pParams->bTypeSeuil)
      iThreshold = pParams->iSeuilAbs;
    sprintf( sMessageEmission, "$PAR,%s,%s,%s,%1d,%d,%d,%d,%d,%d,%d,%d,%d!", sTime, pParams->sHDebut, pParams->sHFin, pParams->bTypeSeuil, iThreshold,
      pParams->uiFe, pParams->uiGainNum, pParams->bExp10, pParams->iFreqFiltreHighPass, pParams->iTopAudioFreq, pParams->iDurTop, pParams->iTopPer);
    return sMessageEmission;
  };

  //-------------------------------------------------------------------------
  //! \brief Decode Parameters message
  //! \param pParams Parameters
  //! \param pMessage Message to decode
  //! \return Return true if decoding is OK
  bool DecodeParams(
    ParamsOperator *pParams,
    char *pMessage
    )
  {
	  bool bOK = true;
    char sHDebut[MAXH];
    char sHFin[MAXH];
    int iDay, iMonth, iYear, iHour, iMinute, iSecond, iTypeTH, iThreshold, iFe, iNumGain, iTimeExp, iFilt, iFreqTop, iDurTop, iTopPer;
    bool bThreshold, bTimeExp;
    sscanf( pMessage, "$PAR,%d/%d/%d-%d:%d:%d,%s,%s,%d,%d,%d,%d,%d,%d,%d,%d,%d!", &iDay, &iMonth, &iYear, &iHour, &iMinute, &iSecond,
      sHDebut, sHFin, &iTypeTH, &iThreshold, &iFe, &iNumGain, &iTimeExp, &iFilt, &iFreqTop, &iDurTop, &iTopPer);
    bThreshold = (bool)iTypeTH;
    bTimeExp   = (bool)iTimeExp;
    // Vérification date heure
    if (iDay<1 or iDay>31 or iMonth<1 or iMonth>12 or iYear<20 or iYear>99 or iHour<0 or iHour>23 or iMinute<0 or iMinute>59 or iSecond<0 or iSecond>59)
      bOK = false;
    else
    {
      // Update to the system date
      setTime( iHour, iMinute, iSecond, iDay, iMonth, iYear);
      // Set internal Teensy 3.6 clock
      Teensy3Clock.set(now());
    }
    if (bOK)
    {
      // Vérification heure de début
      sscanf( sHDebut, "%d:%d", &iHour, &iMinute);
      if (iHour<0 or iHour>23 or iMinute<0 or iMinute>59)
        bOK = false;
      else
        strcpy( pParams->sHDebut, sHDebut);
    }
    if (bOK)
    {
      // Vérification heure de fin
      sscanf( sHFin, "%d:%d", &iHour, &iMinute);
      if (iHour<0 or iHour>23 or iMinute<0 or iMinute>59)
        bOK = false;
      else
        strcpy( pParams->sHFin, sHFin);
    }
    if (bOK)
    {
      // Vérification du seuil
      if (bThreshold and (iThreshold < -110 or iThreshold > -30))
        bOK = false;
      else if (!bThreshold and (iThreshold < 5 or iThreshold > 99))
        bOK = false;
      else
      {
        pParams->bTypeSeuil = bThreshold;
        if (bThreshold)
          pParams->iSeuilAbs = iThreshold;
        else
          pParams->iSeuilDet = iThreshold;
      }
    }
    if (bOK)
    {
      // Vérification de la Fe
      if (iFe < FE192KHZ or iFe > FE384KHZ)
        bOK = false;
      else
        pParams->uiFe = iFe;
    }
    if (bOK)
    {
      // Vérification du gain numérique
      if (iNumGain < GAIN0dB or iNumGain >= MAXGAIN)
        bOK = false;
      else
        pParams->uiGainNum = iNumGain;
    }
    if (bOK)
    {
      // Vérification expansion de temps
      if (bTimeExp < 0 or bTimeExp > 1)
        bOK = false;
      else
        pParams->bExp10 = bTimeExp;
    }
    if (bOK)
    {
      // Vérification filtre passe haut
      if (iFilt < 0 or iFilt > 25)
        bOK = false;
      else
        pParams->iFreqFiltreHighPass = iFilt;
    }
    if (bOK)
    {
      // Vérification de la fréquence du top
      if (iFreqTop < 1 or iFreqTop > 50)
        bOK = false;
      else
        pParams->iTopAudioFreq = iFreqTop;
    }
    if (bOK)
    {
      // Vérification de la durée du top
      if (iDurTop < TS256 or iDurTop >= TSMAX)
        bOK = false;
      else
        pParams->iDurTop = iDurTop;
    }
    if (bOK)
    {
      // Vérification de la période du top
      if (iTopPer < 0 or iTopPer > 10)
        bOK = false;
      else
        pParams->iTopPer = iTopPer;
    }
	  return bOK;
  };

  /*
	&Slave to signal the good reception of the parameters from a slave to master
		Send by Link from Teensy Slave to ESP32 Slave
		Send by Radio from ESP32 Slave to ESP32 Master
		Send by Link from ESP32 Master to Teensy Master
	"$SLAVE,OK,SlaveNum!"
	- OK		     1 if OK else 0
	- SlaveNum   Number of the slave (1 to 10)
	*/
  //-------------------------------------------------------------------------
  //! \brief Code Slave message
  //! \param iSlave Number of the slave (1 to 10)
  //! \param bOK    Indicate if slave is OK
  //! \return Return string of the message
  char *CodeSlave(
    int  iSlave,
    bool bOK
    )
  {
    sprintf( sMessageEmission, "$SLAVE,%1d,%02d!", bOK, iSlave);
    return sMessageEmission;
  }

  //-------------------------------------------------------------------------
  //! \brief Decode Slave message
  //! \param piSlave Number of the slave (1 to 10)
  //! \param pbOK    Indicate if slave is OK
  //! \param pMessage Message to decode
  //! \return Return true if decoding is OK
  bool DecodeSlave(
    int  *piSlave,
    bool *pbOK,
    char *pMessage
    )
  {
	  bool bOK = true;
    int  iSlave, iOK;
    sscanf( pMessage, "$SLAVE,%d,%d!", &iOK, &iSlave);
    // Vérification OK
    if (iOK < 0 or iOK > 1)
      bOK = false;
    else
      *pbOK = (bool)iOK;
    if (bOK)
    {
      // Vérification numéro slave
      if (iSlave <= MASTER or iSlave >= MAXMS)
        bOK = false;
      else
        *piSlave = iSlave;
    }
	  return bOK;
  };

  /*
	$Start to start a record from master to all slaves
		Send by Link from Teensy Master to ESP32 Master
		Send by Radio from ESP32 Master to ESP32 Slave
		Send by Link from ESP32 Slave to Teensy Slave
	"$START!"
  */
  //-------------------------------------------------------------------------
  //! \brief Code Start recording message
  //! \return Return string of the message
  char *CodeStartRec()
  {
    strcpy( sMessageEmission, "$START!");
    return sMessageEmission;
  };

  //-------------------------------------------------------------------------
  //! \brief Decode start recording message
  //! \param pMessage Message to decode
  //! \return Return true if decoding is OK
  bool DecodeStartRec(
    char *pMessage
    )
  {
	  return true;
  };

  /*
	$Stop to stop a record from master to all slaves
		Send by Link from Teensy Master to ESP32 Master
		Send by Radio from ESP32 Master to ESP32 Slave
		Send by Link from ESP32 Slave to Teensy Slave
	"$STOP!"
  */
  //-------------------------------------------------------------------------
  //! \brief Code Stop recording message
  //! \return Return string of the message
  char *CodeStopRec()
  {
    strcpy( sMessageEmission, "$STOP!");
    return sMessageEmission;
  };

  //-------------------------------------------------------------------------
  //! \brief Decode Stop recording message
  //! \param pMessage Message to decode
  //! \return Return true if decoding is OK
  bool DecodeStopRec(
    char *pMessage
    )
  {
	  return true;
  };

  /*
	$Top for synch radio tops from master to all slaves
		Send by Link from Teensy Master to ESP32 Master
		Send by Radio from ESP32 Master to ESP32 Slave
		Send by Link from ESP32 Slave to Teensy Slave
	"$T!"
  */
  //-------------------------------------------------------------------------
  //! \brief Code Top synchro message
  //! \return Return string of the message
  char *CodeTopSynchro()
  {
    strcpy( sMessageEmission, "$T!");
    return sMessageEmission;
  };

  //-------------------------------------------------------------------------
  //! \brief Decode Top synchro message
  //! \param pMessage Message to decode
  //! \return Return true if decoding is OK
  bool DecodeTopSynchro(
    char *pMessage
    )
  {
	  return true;
  };

protected:
  //! Emission buffer
  char sMessageEmission[255];
};

#endif
