//-------------------------------------------------------------------------
//! \file CModeRhinoLogger.cpp
//! \brief Classes de gestion du mode RhinoLogger
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#include "SdFat.h"
#include <U8g2lib.h>
#include "CModeRhinoLogger.h"
#include "CRecorderRL.h"

// SD Card Manager
extern SdFs sd;

// Screen manager
extern U8G2 *pDisplay;

extern const char *txtFBdINVALID[];
extern const char *txtFBdFM[];
extern const char *txtFBdRH[];
extern const char *txtFBdCF[];
const char *txtGNum[]            = {" 0", " 6", "12", "18", "24"};
const char txtRamUsageRL[]       = "Ram after CModeRhinoLogger::BeginMode";

// Temps d'attente en secondes pour les affichages momentanés
#define TIMEATTENTEAFFP 10  // Temps d'affichage des paramètres
#define TIMEAFFBANDES   30  // Temps d'affichage des détections sur les bandes (écran noir après)

//-------------------------------------------------------------------------
// Constructeur
CModeRhinoLogger::CModeRhinoLogger()
{
  pRecorder = NULL;
}

//-------------------------------------------------------------------------
//! \brief Destructeur
CModeRhinoLogger::~CModeRhinoLogger()
{
  if (pRecorder != NULL)
  {
    delete pRecorder;
    pRecorder = NULL;
  }
}

//-------------------------------------------------------------------------
//! \brief Creation du gestionnaire d'enregistrement
void CModeRhinoLogger::CreateRecorder()
{
  pRecorder = new CRecorderRL( &paramsOperateur);
}
  
//-------------------------------------------------------------------------
// Début du mode
void CModeRhinoLogger::BeginMode()
{
  if (CModeGeneric::bDebug)
    Serial.println("CModeRhinoLogger::BeginMode");
  // Lecture des paramètres
  CGenericMode::ReadParams();
  // On impose certains paramètres
  paramsOperateur.uiFe        = FE250KHZ;                  // On impose 250kHz
  paramsOperateur.bTypeSeuil  = false;                     // Seuil relatif
  paramsOperateur.bFiltre     = false;                     // Pas de filtre
  paramsOperateur.iPeriodTemp = 60;                        // Période de lecture température de 60s
  // Mémorisation de la bande d'intéret générale et calcul de la bande d'intéret des bandes RhinoLogger
  iMemoFMin = paramsOperateur.uiFreqMin;
  iMemoFMax = paramsOperateur.uiFreqMax;
  paramsOperateur.uiFreqMin   = 500;
  paramsOperateur.uiFreqMax   = 0;
  for (int b=0; b<MAXBD; b++)
  {
    if (paramsOperateur.batBand[b].iType != BDINVALID)
    {
      paramsOperateur.uiFreqMin = min(paramsOperateur.uiFreqMin, paramsOperateur.batBand[b].fMin);
      paramsOperateur.uiFreqMax = max(paramsOperateur.uiFreqMax, paramsOperateur.batBand[b].fMax);
    }
  }
  // Appel de la méthode de base sans lecture des paramètres
  bReadParams = false;
  CModeGenericRec::BeginMode();
  // Lecture des tensions batteries
  CheckBatteries(MAX_WAITMESURE);
  // Lecture de la température et de l'humidité
  LectureTemperature();
  // Initialisation des tensions batteries, température et humidité
  if (pRecorder != NULL)
    ((CRecorderRL *)pRecorder)->SetTHVBat( cTemp, humidity, fNivBatExt, fNivBatLiPo);
  // Mesure des batteries toutes les minutes
  uiTimeBat = millis() + 60000;
  // Calcul de la place sur la carte SD
  long lFreeKB  = sd.vol()->freeClusterCount();
  long lTotalKB = sd.vol()->clusterCount();
  iFreeSizeSD = lFreeKB * 100 / lTotalKB;
  // On force le prochain affichage
  bRedraw = true;
  iCurrentSecond = 61;
  // Pour un affichage des caractéristiques d'acquisition pendant 15s
  iDecompteurAffPar = TIMEATTENTEAFFP;
  iDecompteurAffBd  = 0;
  // Mémo des paramètres d'enregistrement dans le Log
  char sTempA[256];
  if (commonParams.iLanguage == LFRENCH)
  {
    if (paramsOperateur.bTypeSeuil)
      sprintf( sTempA, "Paramètres: Seuil absolu %ddB, Gain numérique %sdB" , paramsOperateur.iSeuilAbs, txtGNum[paramsOperateur.uiGainNum]);
    else
      sprintf( sTempA, "Paramètres: Seuil relatif %ddB, Gain numérique %sdB", paramsOperateur.iSeuilDet, txtGNum[paramsOperateur.uiGainNum]);
  }
  else
  {
    if (paramsOperateur.bTypeSeuil)
      sprintf( sTempA, "Parameters: Absolute Threshold %ddB, Digital gain %sdB" , paramsOperateur.iSeuilAbs, txtGNum[paramsOperateur.uiGainNum]);
    else
      sprintf( sTempA, "Parameters: Relative Threshold %ddB, Digital gain %sdB" , paramsOperateur.iSeuilDet, txtGNum[paramsOperateur.uiGainNum]);
  }
  LogFile::AddLog( LLOG, sTempA);
  iNbBandesValides = 0;
  for (int b=0; b<MAXBD; b++)
  {
    switch (paramsOperateur.batBand[b].iType)
    {
    default:
    case BDINVALID:  // Invalid band
      sprintf( sTempA, txtFBdINVALID[iLanguage], paramsOperateur.batBand[b].sName);
      break;
    case BDFM     :  // Frequency modulation
      iNbBandesValides++;
      sprintf( sTempA, txtFBdFM[iLanguage]     , paramsOperateur.batBand[b].sName, paramsOperateur.batBand[b].iNbDetections, paramsOperateur.batBand[b].fMin,
        paramsOperateur.batBand[b].fMax, paramsOperateur.batBand[b].fMinFWidth);
      break;
    case BDFC     :  // Rhinolophus frequency
      iNbBandesValides++;
      sprintf( sTempA, txtFBdRH[iLanguage]     , paramsOperateur.batBand[b].sName, paramsOperateur.batBand[b].iNbDetections, paramsOperateur.batBand[b].fMin, paramsOperateur.batBand[b].fMax, paramsOperateur.batBand[b].iMinDuration);
      break;
    case BDQFC     :  // Constant frequency
      iNbBandesValides++;
      sprintf( sTempA, txtFBdCF[iLanguage]     , paramsOperateur.batBand[b].sName, paramsOperateur.batBand[b].iNbDetections, paramsOperateur.batBand[b].fMin, paramsOperateur.batBand[b].fMax, paramsOperateur.batBand[b].iMinDuration, paramsOperateur.batBand[b].iMaxDuration);
      break;
    }
    LogFile::AddLog( LLOG, sTempA);
  }
  // Print Ram usage
  PrintRamUsage( (char *)txtRamUsageRL);
  //Serial.println("CModeRhinoLogger::BeginMode avant StartAcquisition");
  if (iNbBandesValides > 0 and pRecorder != NULL)
    // On lance l'acquisition
    pRecorder->StartAcquisition();
  //Serial.println("CModeRhinoLogger::BeginMode après StartAcquisition");
}

//-------------------------------------------------------------------------
// Fin du mode
void CModeRhinoLogger::EndMode()
{
  // We restore the operator parameters
  ReadParams();
  paramsOperateur.uiFreqMin = iMemoFMin;
  paramsOperateur.uiFreqMax = iMemoFMax;
  // Appel de la méthode de base qui stoppe l'acquisition
  bWriteParams = false;
  CModeGenericRec::EndMode();
  if (CModeGeneric::bDebug)
    Serial.println("CModeAutoRecorder::EndMode");
}

//-------------------------------------------------------------------------
// Affichage du mode sur l'écran
// Cette méthode est appelée régulièrement par le loop principal
// à charge des différents modes d'afficher les informations nécessaires
void CModeRhinoLogger::PrintMode()
{
  //Serial.println("CModeRhinoLogger::PrintMode");
  char Message[30];
  if (iCurrentSecond != second())
  {
    //Serial.printf("CModeRhinoLogger::PrintMode IsNoiseOK %d, iDecompteurAffPar %d, iDecompteurAffBd %d \n", pRecorder->IsNoiseOK(), iDecompteurAffPar, iDecompteurAffBd);
    /*if (hour() == 12 and minute() == 30 and second() == 0)
      // A 12h30 on fait un Reset pour repartir propre tout les jours
      WRITE_RESTART(0x5FA0004);*/
    iCurrentSecond = second();
    if (commonParams.iUtilLed == NOLED)
    {
      // On fait clignotter la LED pour indiquer le bon fonctionnement
      digitalWriteFast(LED_BUILTIN, HIGH);
      delay(1);
      digitalWriteFast(LED_BUILTIN, LOW);
    }
    // Test s'il est temps de mesurer les batteries et la température
    if (millis() > uiTimeBat)
    {
      if (max17043.isOK() and !bMAXPowerOn)
      {
        // Power On MAX and wait 125ms for mesure
        CheckBatteries(MAX_POWER);
        uiTimeBat = millis() + 125;
      }
      else
      {
        if (max17043.isOK())
          // Test battery
          CheckBatteries(MAX_MESURE);
        else
          // Test battery
          CheckBatteries(MAX_WAITMESURE);
        // Lecture de la température et de l'humidité
        LectureTemperature();
        if (pRecorder != NULL)
          // Initialisation de la température, de l'humidité et des tensions batteries
          ((CRecorderRL *)pRecorder)->SetTHVBat( cTemp, humidity, fNivBatExt, fNivBatLiPo);
        // Mesure toutes les minutes
        uiTimeBat = millis() + 60000;
      }
    }
    //Serial.printf("NoiseOK %d, iDecompteurAffPar %d, iDecompteurAffBd %d\n", pRecorder->IsNoiseOK(), iDecompteurAffPar, iDecompteurAffBd);
    if (pRecorder != NULL and pRecorder->IsNoiseOK() and iDecompteurAffPar > 0)
    {
      // Affichage monentané des paramètres
      // RhinoLogger screen
      //   123456789012345678901  y
      // 1 Seuil relatif 15dB     0
      // 2 Gain numerique  0dB    9
      // 3 Noise-120dB- 120/-120 18
      // 4 02 noise peaks        27
      // 5 Max at 125.00kHz      36
      // 6 SD 95% libre          45
      // 7 12/02/19    14:57:12  54
      // X 000000000000000001111
      //   001123344566778990012
      //   062840628406284062840
      // Effacement écran
      pDisplay->clearBuffer();
      int y = 0;
      if (commonParams.iLanguage == LFRENCH)
      {
        if (paramsOperateur.bTypeSeuil)
          sprintf( Message, "Seuil absolu %ddB" , paramsOperateur.iSeuilAbs);
        else
          sprintf( Message, "Seuil relatif %ddB", paramsOperateur.iSeuilDet);
      }
      else
      {
        if (paramsOperateur.bTypeSeuil)
          sprintf( Message, "Absolute Thr. %ddB"  , paramsOperateur.iSeuilAbs);
        else
          sprintf( Message, "Relative Thres. %ddB", paramsOperateur.iSeuilDet);
      }
      pDisplay->setCursor(0, y);
      pDisplay->println(Message);
      y += 9;
      if (commonParams.iLanguage == LFRENCH)
        sprintf( Message, "Gain num. %sdB"   , txtGNum[paramsOperateur.uiGainNum]);
      else
        sprintf( Message, "Digital gain %sdB", txtGNum[paramsOperateur.uiGainNum]);
      pDisplay->setCursor(0, y);
      pDisplay->println(Message);
      y += 9;
      if (commonParams.iLanguage == LFRENCH)
        sprintf( Message, "Bruit%ddB %d/%d", pRecorder->GetNoise(), pRecorder->GetMinNoise(), pRecorder->GetMaxNoise());
      else
        sprintf( Message, "Noise%ddB %d/%d", pRecorder->GetNoise(), pRecorder->GetMinNoise(), pRecorder->GetMaxNoise());
      pDisplay->setCursor(0, y);
      pDisplay->println(Message);
      y += 9;
      int iNbPics = pRecorder->GetNbPics();
      if (commonParams.iLanguage == LFRENCH)
      {
        if (iNbPics == 0)
          sprintf( Message, "0 pic de bruit");
        else if (iNbPics == 1)
          sprintf( Message, "1 pic de bruit");
        else
          sprintf( Message, "%d pics de bruit", iNbPics);
      }
      else
      {
        if (iNbPics == 0)
          sprintf( Message, "0 noise peak");
        else if (iNbPics == 1)
          sprintf( Message, "1 noise peak");
        else
          sprintf( Message, "%d noise peaks", iNbPics);
      }
      pDisplay->setCursor(0, y);
      pDisplay->println(Message);
      y += 9;
      if (iNbPics > 0)
      {
        if (commonParams.iLanguage == LFRENCH)
          sprintf( Message, "Max a %6.2fkHz" , pRecorder->GetFreqMaxNoise());
        else
          sprintf( Message, "Max at %6.2fkHz", pRecorder->GetFreqMaxNoise());
        pDisplay->setCursor(0, y);
        pDisplay->println(Message);
        y += 9;
      }
      if (commonParams.iLanguage == LFRENCH)
        sprintf( Message, "SD %d%% libre", iFreeSizeSD);
      else
        sprintf( Message, "SD %d%% free" , iFreeSizeSD);
      pDisplay->setCursor(0, y);
      pDisplay->println(Message);
      y += 9;
      sprintf( Message, "%02d/%02d/%02d   %02d:%02d:%02d", day(), month(), year(), hour(), minute(), second());
      pDisplay->setCursor(0, 54);
      pDisplay->println(Message);
      UpdateScreen();
      // On décrémente le compteur d'affichage
      iDecompteurAffPar--;
      if (iDecompteurAffPar < 1)
      {
        // Demande d'affichage momentané des résultats des bandes
        iDecompteurAffPar = 0;
        iDecompteurAffBd = TIMEAFFBANDES;
      }
    }
    else if (pRecorder != NULL and pRecorder->IsNoiseOK() and iDecompteurAffBd > 1)
    {
      if (iNbBandesValides == 0)
      {
        pDisplay->clearBuffer();
        pDisplay->setCursor(0, 0);
        if (commonParams.iLanguage == LFRENCH)
          pDisplay->println("Mode RhinoLogger");
        else
          pDisplay->println("RhinoLogger mode");
        pDisplay->setCursor(0, 12);
        if (commonParams.iLanguage == LFRENCH)
          pDisplay->println("Pas de bande !");
        else
          pDisplay->println("No band !");
        pDisplay->setCursor(0, 36);
        if (commonParams.iLanguage == LFRENCH)
          //                 12345678901234567890
          pDisplay->println("Clic pour passer en");
        else
          pDisplay->println("Clic to switch in");
        pDisplay->setCursor(0, 48);
        if (commonParams.iLanguage == LFRENCH)
          //                 12345678901234567890
          pDisplay->println("mode parametres");
        else
          pDisplay->println("parameters mode");
        UpdateScreen();
      }
      else
      {
        // RhinoLogger screen
        //   12345678901234567890  y
        // 1 FM  00 -120 125.45     0
        // 2 GR  00 -120 125.45     9
        // 3 REb 00 -120 125.45    18
        // 4 REh 00 -120 125.45    27
        // 5 EPb 00 -120 125.45    36
        // 6 EPh 00 -120 125.45    45
        // 7 PR  00 -120 125.45    54
        // X 000000000000000001111
        //   001123344566778990012
        //   062840628406284062840
        // Affichage des caractéristiques de l'acquisition
        pDisplay->clearBuffer();
        // Récupération des infos de la minute courante
        ResultAnalyse ResMinute;
        // Affichage des 7 lignes
        float fMax;
        int   iNivMax, y=0;
        for (int b=0; b<MAXBD; b++)
        {
          if (paramsOperateur.batBand[b].iType != BDINVALID)
          {
            ((CRecorderRL *)pRecorder)->GetCurentMinute( b, &ResMinute);
            iNivMax = pRecorder->GetNivDB  ( ResMinute.iMagMax) / 10;
            fMax    = pRecorder->GetFreqIdx( ResMinute.iIdxMagMax);
            if (fMax < 1) fMax = 0.0;
            sprintf( Message, "%s %02d %+04d %06.2f", paramsOperateur.batBand[b].sName, ResMinute.iNbSec, iNivMax, fMax);
            pDisplay->setCursor(0, y);
            pDisplay->println(Message);
            y += 9;
          }
        }
        UpdateScreen();
        // On décompte si affichage non permanent
        if (!paramsOperateur.bRLAffPerm and iDecompteurAffBd > 1)
          iDecompteurAffBd--;
      }
    }
    else if (iDecompteurAffBd == 1)
    {
      // Effacement écran
      pDisplay->clearBuffer();
      UpdateScreen();
      iDecompteurAffBd = 0;
    }
  }
}

//-------------------------------------------------------------------------
// Traitement des ordres claviers
// Si la touche est une touche de changement de mode, retourne le mode demandé
// Cette méthode est appelée régulièrement par le loop principal
// à charge des différents modes de traiter les actions opérateurs
int CModeRhinoLogger::KeyManager(
  unsigned short key  // Touche sollicitée par l'opérateur
  )
{
  // Gestion éventuelle des touches
  int newMode = NOMODE;
  if (key == K_PUSH or key == K_MODEA)
  {
    if (iDecompteurAffPar > 0 or iNbBandesValides == 0)
    {
      if (GetRecorderType() == ACTIVE_RECORDER)
        // In Active Recorder K_MODEA change mode : Record mode -> Player -> Parameters -> Record mode
        iNewMode = MPLAYER;
      else
        // Passage en mode paramètres
        iNewMode = MPARAMS;
    }
    else if (iDecompteurAffBd <= 0)
      // Demande d'affichage momentané des résultats des bandes
      iDecompteurAffBd = TIMEAFFBANDES;
    else
    {
      if (GetRecorderType() == ACTIVE_RECORDER)
        // In Active Recorder K_MODEA change mode : Record mode -> Player -> Parameters -> Record mode
        iNewMode = MPLAYER;
      else
        // In other type go to parameters
       iNewMode = MPARAMS;
    }
  }
  // Test si temps de passer en veille
  if (IsTimeVeille())
    iNewMode = MVEILLE;
  if (pRecorder != NULL and ((CRecorderRL *)pRecorder)->IsFCtooLarge())
  {
    if (CModeGeneric::GetCommonsParams()->iLanguage == LFRENCH)
      CModeError::SetTxtError((char *)"RL Trop de canaux FC!");
    else
      CModeError::SetTxtError((char *)"RL too many FC chann!");
    iNewMode = MERROR;
  }
  if (iNewMode != NOMODE)
  {
    newMode  = iNewMode;
    iNewMode = NOMODE;
  }
  return newMode;
}
