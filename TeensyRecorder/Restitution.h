//-------------------------------------------------------------------------
//! \file Restitution.h
//! \brief Classe de gestion de la restitution des échantillons en lecture
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/*
  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef Restitution_H
#define Restitution_H

#if defined(__IMXRT1062__) // Teensy 4.1
extern void dma_I2S_isr(void);
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
extern void dma_ch3_isr();
#endif

//-------------------------------------------------------------------------
//! \class Restitution
//! \brief Classe d'initialisation de la restitution via le PIT, DAC et DMA
//! Utilise le DAC0 (A21), le PIT3 et le DMA3 sur un Teensy 3.6
//! Utilise la MQS (Medium Quality Sound), et le DMA4 sur Teensy 4.1
//! A charge du programme d'alimenter les buffers en échantillons via l'IT DMA
//! Utilisation :
//! - Instantier un objet Restitution,
//! - Initialiser la Fe
//! - Faire un start pour lancer la restitution et un stop pour l'arrêter
//! - Traiter les interruption du DMA3 (void dma_ch3_isr())
class Restitution
{

  public:
    //-------------------------------------------------------------------------
    //! \brief Constructeur
    //! \param pBuff Pointeur vers le double buffer d'échantillons utilisé par le DMA
    //! \param nbSamples Nombre d'échantillons d'un buffer
    //! \param Fe Fréquence d'échantillonnage en Hz
    Restitution(
      uint16_t *pBuff,
      uint16_t nbSamples,
      unsigned int Fe
      )
    {
      // Init des paramètres de Restitution
      iFe  = Fe;
      pBuffEch = pBuff;
      uiNbSamplesBuffer = nbSamples;
      bRestitution = false;
    }

    //-------------------------------------------------------------------------
    //! \brief Initialisation de la fréquence d'échantillonnage
    //! \param Fe Fréquence d'échantillonnage en Hz
    void setFe(
      unsigned int Fe
      )
    {
      iFe = Fe;
      //Serial.printf("Restitution::setFe iFe %d\n", iFe);
    }

    //-------------------------------------------------------------------------
    //! \brief Lance la Restitution
    void start()
    {
      //Serial.printf("Restitution::start iFe %d\n", iFe);
      // Lancement DAC, DMA et PIT
#if defined(__MK66FX1M0__) // Teensy 3.6
      dacInit();
      dmaInit();
      pitInit();
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
      I2sInit();
      dmaInit();
#endif
      bRestitution = true;
    }
    
    //-------------------------------------------------------------------------
    //! \brief Relance la Restitution
    void restart()
    {
#if defined(__MK66FX1M0__) // Teensy 3.6
      // (23.4.1) Channel Configuration Register
      // Set PIT3 as source of DMA3, enable DMA MUX
      DMAMUX0_CHCFG3 =
          DMAMUX_ENABLE           // DMA Channel 3 is enabled
        | DMAMUX_TRIG             // DMA Channel 3 is triggered by PIT 3
        | DMAMUX_SOURCE_ALWAYS0;  // Trigger is always active
      // (46.4.6) Enable timer 3
      PIT_TCTRL3 |= PIT_TCTRL_TEN;
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
      // 38.5.1.7 Transmit Configuration 3
      I2S3_TCR3 = I2S_TCR3_TCE;                                       // Transmit Channel Enable, 1b - Transmit data channel N is enabled
      // 6.5.5.9 Set Enable Request
      DMA_SERQ = ch;                                                  // Enable DMA
#endif
      bRestitution = true;
    }
    
    //-------------------------------------------------------------------------
    //! \brief Stoppe la Restitution
    void stop()
    {
#if defined(__MK66FX1M0__) // Teensy 3.6
      // (46.4.6) Disable timer 3
      PIT_TCTRL3 &= ~PIT_TCTRL_TEN;     // Arrêt du PIT 3
      // (23.4.1) Channel Configuration Register
      DMAMUX0_CHCFG3 = DMAMUX_DISABLE;  // Arrêt du DMA 3
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
      // 38.5.1.13 Receive Control
      I2S3_TCSR &= ~I2S_TCSR_TE                                       // Receiver Enable
                &  ~I2S_TCSR_BCE                                      // Bit Clock Enable
                &  ~I2S_TCSR_FRDE;                                    // FIFO Request DMA Enable
      // 38.5.1.7 Transmit Configuration 3
      I2S3_TCR3 &= ~I2S_TCR3_TCE;                                     // Transmit Channel Enable, 1b - Transmit data channel N is disabled
      if (bRestitution)
        // 6.5.5.8 Clear Enable Request
        DMA_CERQ = ch;                                                // top DMA request
#endif
     bRestitution = false;
    }

    //-------------------------------------------------------------------------
    //! \brief Indique si la Restitution est active (true) ou non (false)
    bool IsRestitution() {return bRestitution;};

    //-------------------------------------------------------------------------
    //! \brief RAZ IT DMA
    void ClearDMAIsr()
    {
#if defined(__IMXRT1062__) // Teensy 4.1
      DMA_CINT = ch;
      DMA_CDNE = ch; //????
      // Handle clear interrupt glitch in Teensy 4.x!
      asm("DSB");
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
      // RAZ bit d'interruption du canal 3
      DMA_CINT = DMA_CINT_CINT(3); // (24.3.12) Clear interrupt request register
#endif
    }

    //-------------------------------------------------------------------------
    //! \brief Retourne l'adresse courante d'écriture du DMA
    uint32_t *GetCurrentDMAaddr()
    {
#if defined(__IMXRT1062__) // Teensy 4.1
      // 6.5.5.19 TCD Source Address
      return (uint32_t *)IMXRT_DMA_TCD[ch].SADDR;
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
      return (uint32_t *)(DMA_TCD3_SADDR);
#endif
    }

  protected:

    //-------------------------------------------------------------------------
    //! \brief Configuration du DAC 0
    void dacInit()
    {
#if defined(__MK66FX1M0__) // Teensy 3.6
      // (13.2.16) System clock gating control register 6
      SIM_SCGC2 |= SIM_SCGC2_DAC0; // enable DAC clock
      // (41.5.4) DAC Control Register 0
      DAC0_C0 |= DAC_C0_DACEN;      // enable DAC
                                    // DACTRGSEL = 0 hardware trigger is selected
                                    // DACSWTRG  = 0 The DAC soft trigger is not valid
                                    // LPEN      = 0 High-Power mode
                                    // DACBBIEN  = 0 The DAC buffer read pointer bottom flag interrupt is disabled
      DAC0_C0 |= DAC_C0_DACRFS;     // use 3.3V VDDA as reference voltage (1.2V si masqué)
      DAC0_C0 |= DAC_C0_DACBWIEN;   // enable DMA trigger at watermark
      DAC0_C0 |= DAC_C0_DACBTIEN;   // enable DMA trigger at 0
      // (41.5.5) DAC Control Register 1
      DAC0_C1 = DAC_C1_DMAEN;       // enable DMA
                                    // DACBFWM   = 0 DAC Buffer Watermark Select 1 word
                                    // DACBFMD   = 0 DAC Buffer Work Mode Select 00 normal mode
                                    // DACBFEN   = 0 Buffer read pointer is disabled. The converted data is always the first word of the buffer
      // (41.5.6) DAC Control Register 2
      DAC0_C2 = DAC_C2_DACBFUP(0);  // set buffer size to 1
      // (41.5.3) DAC Status Register
      DAC0_SR &= ~(DAC_SR_DACBFWMF); // clear watermark flag
      DAC0_SR &= ~(DAC_SR_DACBFRTF); // clear top pos flag
      DAC0_SR &= ~(DAC_SR_DACBFRBF); // clear bottom pos flag
#endif
    }
  
#if defined(__IMXRT1062__) // Teensy 4.1
    //-------------------------------------------------------------------------
    //! \brief Configuration de la sortie I2S pour le DAC externe
    //! Pin 21 BCK (Serial Bit Clock Input), Pin 20 WS (Word Select Clock Input Pin), Pin 7 DIN (Data Input Pin)
    FLASHMEM void I2sInit(void)
    {
      // Output pin initialization
      CORE_PIN21_CONFIG = 3;  //1:RX_BCLK
      CORE_PIN20_CONFIG = 3;  //1:RX_SYNC
      CORE_PIN7_CONFIG  = 3;  //1:RX_DATA0

      // 14.7.26 CCM Clock Gating Register 5
      CCM_CCGR5 |= CCM_CCGR5_SAI1(CCM_CCGR_ON);       // sai1_clk_enable
    
      int fs = iFe;
      // PLL between 27*24 = 648MHz und 54*24=1296MHz
      int n1 = 4; // SAI prescaler 4 => (n1*n2) = multiple of 4
      int n2 = 1 + (24000000 * 27) / (fs * 256 * n1);
    
      double C = ((double)fs * 256 * n1 * n2) / 24000000;
      int nfact = C;
      int ndiv  = 10000;
      int nmult = C * ndiv - (nfact * ndiv);
      //Serial.printf("I2sInit Fe %d, n1 %d, n2 %d, C %f, nfact %d, ndiv %d, nmult %d\n", iFe, n1, n2, C, nfact, ndiv, nmult);
    
      // Init audio clock
      // 14.8.8 Analog Audio PLL control Register
      CCM_ANALOG_PLL_AUDIO = CCM_ANALOG_PLL_AUDIO_BYPASS                   // Bypass the PLL
                           | CCM_ANALOG_PLL_AUDIO_ENABLE                   // Enable PLL output
                           | CCM_ANALOG_PLL_AUDIO_POST_DIV_SELECT(2)       // These bits implement a divider after the PLL, but before the enable and bypass mux 2: 1/4; 1: 1/2; 0: 1/1
                           | CCM_ANALOG_PLL_AUDIO_DIV_SELECT(nfact);       // This field controls the PLL loop divider. Valid range for DIV_SELECT divider value: 27~54
      // 14.8.9 Numerator of Audio PLL Fractional Loop Divider Register
      CCM_ANALOG_PLL_AUDIO_NUM   = nmult & CCM_ANALOG_PLL_AUDIO_NUM_MASK;  // This register contains the numerator (A) of Audio PLL fractional loop divider (Signed number)
      // 14.8.10 Denominator of Audio PLL Fractional Loop Divider Register
      CCM_ANALOG_PLL_AUDIO_DENOM = ndiv & CCM_ANALOG_PLL_AUDIO_DENOM_MASK; // This register contains the denominator (B) of Audio PLL fractional loop divider (unsigned number)
      // 14.8.8 Analog Audio PLL control Register
      CCM_ANALOG_PLL_AUDIO &= ~CCM_ANALOG_PLL_AUDIO_POWERDOWN;             // Powers on the PLL
      while (!(CCM_ANALOG_PLL_AUDIO & CCM_ANALOG_PLL_AUDIO_LOCK)) {};      // Wait for pll-lock, 1 - PLL is currently locked
      // 14.8.19 Miscellaneous Register 2
      CCM_ANALOG_MISC2 &= ~(CCM_ANALOG_MISC2_DIV_MSB                       // MSB of Post-divider for Audio PLL, 0 divide by 1
                          | CCM_ANALOG_MISC2_DIV_LSB);                     // LSB of Post-divider for Audio PLL, 0 divide by 1
      // 14.8.8 Analog Audio PLL control Register
      CCM_ANALOG_PLL_AUDIO &= ~CCM_ANALOG_PLL_AUDIO_BYPASS;                // Disable PLL Bypass

      // 14.7.7 CCM Serial Clock Multiplexer Register 1
      CCM_CSCMR1 = (CCM_CSCMR1 & ~(CCM_CSCMR1_SAI1_CLK_SEL_MASK))
                 | CCM_CSCMR1_SAI1_CLK_SEL(2);                             // Selector for sai1 clock multiplexer, derive clock from PLL4
      // 14.7.10 CCM Clock Divider Register
      CCM_CS1CDR = (CCM_CS1CDR & ~(CCM_CS1CDR_SAI1_CLK_PRED_MASK | CCM_CS1CDR_SAI1_CLK_PODF_MASK))
                 | CCM_CS1CDR_SAI1_CLK_PRED(n1-1)                          // Divider for sai1 clock pred
                 | CCM_CS1CDR_SAI1_CLK_PODF(n2-1);                         // Divider for sai1 clock podf
      // 11.3.2 GPR1 General Purpose Register
      IOMUXC_GPR_GPR1 = (IOMUXC_GPR_GPR1 & ~(IOMUXC_GPR_GPR1_SAI1_MCLK1_SEL_MASK))
                      | (IOMUXC_GPR_GPR1_SAI1_MCLK_DIR                     // sai1.MCLK signal direction control, output signal
                      | IOMUXC_GPR_GPR1_SAI1_MCLK1_SEL(0));                // SAI1 MCLK3 source select, ccm.spdif0_clk_root

      // 38.5.1.12 Transmit Mask
      I2S1_TMR = 0;                                                        // Transmit Word Mask, Word N is enabled
      // 38.5.1.5 Transmit Configuration 1
      I2S1_TCR1 = I2S_TCR1_RFW(0);                                         // Transmit FIFO Watermark
      // 38.5.1.6 Transmit Configuration 2
      I2S1_TCR2 = I2S_TCR2_SYNC(1)                                         // Synchronous Mode, Synchronous with receiver
                | I2S_TCR2_BCP                                             // Bit Clock Polarity, Bit clock is active low with drive outputs on falling edge and sample inputs on rising edge
                | I2S_TCR2_MSEL(1)                                         // MCLK Select, Master Clock (MCLK) 1 option selected
                | I2S_TCR2_BCD                                             // Bit Clock Direction, Bit clock is generated internally in Master mode
                | I2S_TCR2_DIV(3);                                         // Bit Clock Divide, Divides down the audio master clock to generate the bit clock when configured for an internal bit clock. The division value is (DIV + 1) * 2.
      // 38.5.1.7 Transmit Configuration 3
      I2S1_TCR3 = I2S_TCR3_TCE;                                            // Transmit Channel Enable, Transmit data channel N is enabled
      // 38.5.1.8 Transmit Configuration 4
      I2S1_TCR4 = I2S_TCR4_FRSZ(1)                                         // Frame size, Configures the number of words in each frame
                | I2S_TCR4_SYWD(15)                                        // Sync Width, Configures the length of the frame sync in number of bit clocks. The value written must be one less than the number of bit clocks
                | I2S_TCR4_MF                                              // MSB First, MSB is transmitted first
                | I2S_TCR4_FSP                                             // Configures the polarity of the frame sync, Frame sync is active low
                | I2S_TCR4_FSD;                                            // Frame Sync Direction, Frame sync is generated internally in Master mode
      // 38.5.1.9 Transmit Configuration 5
      I2S1_TCR5 = I2S_TCR5_WNW(15)                                         // Word N Width, Configures the number of bits in each word, for each word except the first in the frame. The value written must be one less than the number of bits per word
                | I2S_TCR5_W0W(15)                                         // Word 0 Width, Configures the number of bits in the first word in each frame. The value written must be one less than the number of bits per word
                | I2S_TCR5_FBT(15);                                        // First Bit Shifted, Configures the bit index for the first bit transmitted for each word in the frame
      // 38.5.1.21 Receive Mask
      I2S1_RMR = 0;                                                        // Receive Word Mask, Word N is enabled
      // 38.5.1.14 Receive Configuration 1
      I2S1_RCR1 = I2S_RCR1_RFW(0);                                         // Receive FIFO Watermark
      // 38.5.1.15 Receive Configuration 2
      I2S1_RCR2 = I2S_RCR2_SYNC(0)                                         // Synchronous Mode, Asynchronous mode
                | I2S_RCR2_BCP                                             // Bit Clock Polarity, Bit Clock is active low with drive outputs on falling edge and sample inputs on rising edge
                | I2S_RCR2_MSEL(1)                                         // MCLK Select, Master Clock (MCLK) 1 option selected
                | I2S_TCR2_BCD                                             // Bit Clock Direction, Bit clock is generated internally in Master mode
                | I2S_TCR2_DIV(3);                                         // Bit Clock Divide, Divides down the audio master clock to generate the bit clock when configured for an internal bit clock. The division value is (DIV + 1) * 2
      // 38.5.1.16 Receive Configuration 3
      I2S1_RCR3 = I2S_RCR3_RCE;                                            // Receive Channel Enable, Receive data channel N is enabled
      // 38.5.1.17 Receive Configuration 4
      I2S1_RCR4 = I2S_RCR4_FRSZ(1)                                         // Frame Size, Configures the number of words in each frame. The value written must be one less than the number of words in the frame
                | I2S_RCR4_SYWD(15)                                        // Sync Width, Configures the length of the frame sync in number of bit clocks. The value written must be one less than the number of bit clocks
                | I2S_RCR4_MF                                              // MSB First, MSB is received first
                | I2S_RCR4_FSP                                             // Frame Sync Polarity, Frame sync is active low
                | I2S_RCR4_FSD;                                            // Frame Sync Direction, Frame Sync is generated internally in Master mode
      // 38.5.1.18 Receive Configuration 5
      I2S1_RCR5 = I2S_RCR5_WNW(15)                                         // Word N Width, Configures the number of bits in each word, for each word except the first in the frame. The value written must be one less than the number of bits per word
                | I2S_RCR5_W0W(15)                                         // Word 0 Width, Configures the number of bits in the first word in each frame. The value written must be one less than the number of bits in the first word
                | I2S_RCR5_FBT(15);                                        // First Bit Shifted, Configures the bit index for the first bit received for each word in the frame. If configured for MSB First, the index of the next bit received is one less than the current bit received
    }
#endif
    
#if defined(__MK66FX1M0__) // Teensy 3.6
    //-------------------------------------------------------------------------
    //! \brief Init du PIT 3 (Periodic Interrupt Timer) pour générer la Fe
    void pitInit()
    {
      // Calcul de la période en fonction de la Fe
      uint32_t PitPeriod = (F_BUS / iFe) - 1;
      // (13.2.16) Send system clock to PIT
      SIM_SCGC6 |= SIM_SCGC6_PIT;
      // (46.4.1) Turn on PIT
      PIT_MCR = 0x00;
      // (46.4.4) Set timer 3 period
      PIT_LDVAL3 = PitPeriod;
      // (46.4.6) Enable timer 3
      PIT_TCTRL3 |= PIT_TCTRL_TEN;
    }
#endif
     
    //-------------------------------------------------------------------------
    //! \brief Init du DMA3 pour envoyer le double buffer avec interruption à la moitiée et sur la totalité
    //! A charge du programme de remplir le buffer sur l'IT DMA
    void dmaInit()
    {
#if defined(__MK66FX1M0__) // Teensy 3.6
      // (13.2.16-17) System clock gating control registers 6 and 7
      SIM_SCGC7 |= SIM_SCGC7_DMA;     // Enable DMA clock
      SIM_SCGC6 |= SIM_SCGC6_DMAMUX;  // Enable DMAMUX clock

      // (24.3.11) Clear Error Register
      DMA_CERR = DMA_CERR_CAEI; // Clear all bits in ERR
      // (24.3.1) Control Register
      DMA_CR = 0;
      DMA_CR = DMA_CR_GRP0PRI;
      
      // ======= Source =======
      // (24.3.18) TCD Source Address
      DMA_TCD3_SADDR = pBuffEch;
      // (24.3.20) TCD Transfer Attributes
      DMA_TCD3_ATTR = 0x0000;
      DMA_TCD3_ATTR |= DMA_TCD_ATTR_SSIZE(1);             // Source data transfer size (16-bit)
      // (24.3.19) TCD Signed Source Address Offset
      DMA_TCD3_SOFF = sizeof(uint16_t);                   // Advance destination pointer by 2 bytes per value
      // (24.3.24) TCD Last Source Address Adjustment
      DMA_TCD3_SLAST = -(sizeof(uint16_t) * uiNbSamplesBuffer * 2); // Return to &pBuffEch[0] after major loop
      // Number of bytes to transfer (in each service request)
      DMA_TCD3_NBYTES_MLNO = sizeof(uint16_t);

      // ======= Destination =======
      // (21.3.24) TCD Destination Address
      DMA_TCD3_DADDR = &DAC0_DAT0L;
      // (24.3.20) TCD Transfer Attributes
      DMA_TCD3_ATTR |= DMA_TCD_ATTR_DSIZE(1); // Destination data transfer size (16-bit)
      // (24.3.26) TCD Signed Destination Address Offset
      DMA_TCD3_DOFF = 0;                      // La destination n'avance pas   
      // (24.3.29) TCD Last Destination Address Adjustment/Scatter Gather Address
      DMA_TCD3_DLASTSGA = 0;                  // La destination n'avance pas

      // (24.3.28) TCD Current Minor Loop Link, Major Loop Count (Channel Linking Disabled)
      DMA_TCD3_CITER_ELINKNO = uiNbSamplesBuffer * 2;  // Current Major Iteration Count
      // (24.3.32) TCD Beginning Minor Loop Link, Major Loop Count (Channel Linking Disabled)
      DMA_TCD3_BITER_ELINKNO = uiNbSamplesBuffer * 2;  // Starting Major Iteration Count

      // ======= Interrupts =======
      // (24.3.30) TCD Control and Status
      DMA_TCD3_CSR  = DMA_TCD_CSR_INTMAJOR; // Interrupt at major loop (CITER == 0)
      DMA_TCD3_CSR |= DMA_TCD_CSR_INTHALF;  // Also interrupt at half major loop (CITER == BITER/2)
      // Enable interrupt request
      //_VectorsRam[3 + IRQ_DMA_CH0 + 16] = dma_ch3_isr;
      NVIC_ENABLE_IRQ(IRQ_DMA_CH3);
      //attachInterruptVector(IRQ_DMA_CH3, dma_ch3_isr)
    
      // ======= Triggering =======
      // (23.4.1) Channel Configuration Register
      // Set PIT3 as source of DMA3, enable DMA MUX
      DMAMUX0_CHCFG3 = DMAMUX_DISABLE;
      DMAMUX0_CHCFG3 =
          DMAMUX_ENABLE           // DMA Channel 3 is enabled
        | DMAMUX_TRIG             // DMA Channel 3 is triggered by PIT 3
        | DMAMUX_SOURCE_ALWAYS0;  // Trigger is always active
      // Enable request input signal for channel 3
      DMA_SERQ |= DMA_SERQ_SERQ(3); // Enable channel 3 DMA requests
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
      int iSizeBuff = sizeof(uint16_t)*uiNbSamplesBuffer*4;
      arm_dcache_flush_delete( pBuffEch, iSizeBuff);
      // Initialisation du DMA 4
      ch = 4;
      // 14.7.26 CCM Clock Gating Register 5
      CCM_CCGR5 |= CCM_CCGR5_DMA(CCM_CCGR_ON);                          // DMA clock enable

      // 6.5.5.2 Control
      //DMA_CR = DMA_CR_EMLM;                                             // CX and ECX Normal operation, GRP1PRI and GRP1PRI 0, EMLM 4 fields in ATTR,
                                                                        // CLM Continuous link mode is off, HALT and HOE Normal operation,
                                                                        // ERGA and ERCA Fixed priority arbitration, EDBG When the chip is in Debug mode, the eDMA continues to operate
      DMA_CR = DMA_CR_GRP1PRI | DMA_CR_EMLM | DMA_CR_EDBG;

      // 6.5.5.6 Clear Enable Error Interrupt
      DMA_CEEI = ch;                                                    // Clear chanel
      // 6.5.5.8 Clear Enable Request
      DMA_CERQ = ch;                                                    // Clear chanel
      // 6.5.5.12 Clear Error
      DMA_CERR = ch;                                                    // Clear chanel
      // 6.5.5.13 Clear Interrupt Request
      DMA_CINT = ch;                                                    // Clear chanel
      // 6.5.5.19 TCD Source Address
      IMXRT_DMA_TCD[ch].SADDR = pBuffEch;                               // Source address is samples buffer
      // 6.5.5.20 TCD Signed Source Address Offset
      IMXRT_DMA_TCD[ch].SOFF = 2;                                       // Advance source pointer by 2 bytes per value
      // 6.5.5.21 TCD Transfer Attributes
      IMXRT_DMA_TCD[ch].ATTR = DMA_TCD_ATTR_SSIZE(1)                    // Source data transfer size (16-bit), source and destination address modulo feature is disabled
                             | DMA_TCD_ATTR_DSIZE(1);                  // Destination data transfer size (16-bit)
      // 6.5.5.22 TCD Minor Byte Count
      IMXRT_DMA_TCD[ch].NBYTES_MLNO = sizeof(uint16_t);                 // Number of bytes to transfer (in each service request)
      // 6.5.5.25 TCD Last Source Address Adjustment
      IMXRT_DMA_TCD[ch].SLAST = -iSizeBuff;                             // Return to &pBuffEch[0] after major loop
      // 6.5.5.26 TCD Destination Address
      IMXRT_DMA_TCD[ch].DADDR = (void *)((uint32_t)&I2S1_TDR0);         // Destination addess is I2S1_TDR0
      // 6.5.5.27 TCD Signed Destination Address Offset
      IMXRT_DMA_TCD[ch].DOFF = 0;                                       // Don't advance destination pointer after minor loop
      // 6.5.5.28 TCD Current Minor Loop Link, Major Loop Count
      IMXRT_DMA_TCD[ch].CITER_ELINKNO = iSizeBuff / 2;                  // Current Major Iteration Count
      // 6.5.5.30 TCD Last Destination Address Adjustment/Scatter Gather Address
      IMXRT_DMA_TCD[ch].DLASTSGA = 0;                                   // Don't advance destination pointer after major loop
      // 6.5.5.31 TCD Control and Status
      IMXRT_DMA_TCD[ch].CSR = DMA_TCD_CSR_INTMAJOR                      // Interrupt at major loop (CITER == 0)
                            | DMA_TCD_CSR_INTHALF;                      // Also interrupt at half major loop (CITER == BITER/2)
      // 6.5.5.32 TCD Beginning Minor Loop Link, Major Loop Count
      IMXRT_DMA_TCD[ch].BITER_ELINKNO = iSizeBuff / 2;                  // Starting Major Iteration Count

      // 5.6.1.2 Channel a Configuration Register
      volatile uint32_t *mux = &DMAMUX_CHCFG0 + ch;
      *mux = 0;
      *mux = (DMAMUX_SOURCE_SAI1_TX & 0x7F) | DMAMUX_CHCFG_ENBL;        // Route SAI1 to DMA_MUX

      // 38.5.1.13 Receive Control
      I2S1_RCSR |= I2S_RCSR_RE;                                         // Receiver Enable
      // 38.5.1.13 Receive Control
      I2S1_TCSR |= I2S_TCSR_TE                                          // Receiver Enable
                |  I2S_TCSR_BCE                                         // Bit Clock Enable
                |  I2S_TCSR_FRDE;                                       // FIFO Request DMA Enable
      
      // Set ISR vector with highest priority
      _VectorsRam[ch + IRQ_DMA_CH0 + 16] = dma_I2S_isr;
      NVIC_ENABLE_IRQ(IRQ_DMA_CH0 + ch);
      // 6.5.5.9 Set Enable Request
      DMA_SERQ = ch;                                                    // Enable DMA
#endif
    }
    
  private:
    //! Fréquence d'échantillonnage en Hz
    unsigned int iFe;

    //! Pointeur vers le double buffer d'échantillons utilisé par le DMA
    uint16_t *pBuffEch;

    //! Taille d'un buffer d'échantillons
    uint16_t uiNbSamplesBuffer;

    //! Indique si la Restitution est active (true) ou non (false)
    bool bRestitution;

#if defined(__IMXRT1062__) // Teensy 4.1
    // DMA channel
    uint32_t ch;
#endif
};

#endif
