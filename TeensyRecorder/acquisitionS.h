//! \file acquisitionS.h
//! \brief Classe de gestion des périphériques d'acquisition en mode stéréo
//! \details class CGenericMode, CModeError and LogFile
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/*
  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#include "Acquisition.h"

#ifndef ACQUISITIONS_H
#define ACQUISITIONS_H

#if defined(__IMXRT1062__) // Teensy 4.1
  // Max size for half DMA buffer for Teensy 4.1 stereo
  #define MAX_DMA_SETERO_T41 256
  // Teensy 4.1, right DMA channel
  #define DMACHR 4
  // Teensy 4.1, left DMA channel
  #define DMACHL 5
  // Teensy 4.1, nb copy for half big buffer
  #define HALFDMACPY MAXSAMPLE/MAX_DMA_SETERO_T41
  // Teensy 4.1, nb copy for complete big buffer
  #define MAXDMACPY HALFDMACPY*2
  // Teensy 4.1, small DMA buffer
  DMAMEM __attribute__((aligned(32))) uint16_t samplesDMAT41[MAX_DMA_SETERO_T41*2];
  // Teensy 4.1, current pointer to copy in big buffer
  uint16_t *pBuffCopy;
  // Teensy 4.1, pointer to the beginning of big buffer
  uint16_t *pMemBuffEch;
  // Teensy 4.1, current number of copy in big buffer
  unsigned int iNbCopy;
  // Indicateur buffer disponible
  int iBufferOK = -1;
  // Teensy 4.1, function for big buffer processing
  extern void dma_T41_isr();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
extern void dma_ch2_isr(void);
#endif

//-------------------------------------------------------------------------
//! \class AcquisitionStereo
//! \brief Classe d'initialisation de l'acquistion stéréo via le PIT, ADC et DMA
//! Utilise le PIT0, l'ADC1 (D), l'ADC0 (G), le DMA2 (D) et le DMA3 (G) pour un Teensy 3.6
//! Utilise le TMR4 Timer 3, l'ADC2 (D), l'ADC0 (G), le DMA5 (D) et le DMA4 (G) pour un Teensy 4.1
//! Utilisation :
//! - Instancier un objet AcquisitionStereo,
//! - Initialiser la Fe et la résolution
//! - Faire un start pour lancer l'acquisition et un stop pour l'arrêter
//! - Traiter les interruptions du DMA (void dma_ch2_isr())
class AcquisitionStereo: public Acquisition
{

  public:
    //-------------------------------------------------------------------------
    //! \brief Constructeur
    //! \param pBuff Pointeur vers le double buffer d'échantillons utilisé par le DMA
    //! \param nbSamples Nombre d'échantillons d'un buffer
    //! \param PinG N° de la broche d'entrée droite (A0 à A9)
    //! \param PinD N° de la broche d'entrée gauche (A0 à A9)
    //! \param Fe Fréquence d'échantillonnage en Hz
    AcquisitionStereo(
      uint16_t *pBuff,    
      uint16_t nbSamples, 
      unsigned int PinD,   
      unsigned int PinG,   
      unsigned int Fe     
      ): Acquisition( pBuff, nbSamples, PinD, Fe)
    {
      // Init des paramètres d'acquisition
      iPinG = PinG;
#if defined(__IMXRT1062__) // Teensy 4.1
      pMemBuffEch = pBuff;
      memset(samplesDMAT41, 0, MAX_DMA_SETERO_T41*2*sizeof(uint16_t));
      iBufferOK = -1;
      //Serial.printf("pBuf %d, +8192 %d, +2x8192 %d\n", pBuff, &(pBuff[nbSamples]), &(pBuff[(nbSamples*2)+1]));
#endif
    }

    //-------------------------------------------------------------------------
    //! \brief Stoppe l'acquisition
    virtual void stop()
    {
#if defined(__IMXRT1062__) // Teensy 4.1
      // 54.9.1.15 Timer Channel Enable Register
      TMR4_ENBL &= ~(1<<3);                           // Disable Timer
      // 6.5.5.8 Clear Enable Request
      DMA_CERQ = DMACHR;
      // 14.7.23 CCM Clock Gating Register 2
      CCM_CCGR2 |= CCM_CCGR2_XBAR1(CCM_CCGR_OFF);     // xbar1 clock disable
      // 14.7.26 CCM Clock Gating Register 5
      CCM_CCGR5 |= CCM_CCGR5_DMA(CCM_CCGR_OFF);       // DMA clock disable
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
      // (46.4.6) Disable timer 0
      PIT_TCTRL0 &= ~PIT_TCTRL_TEN;     // Arrêt du PIT 0
      // (23.4.1) Channel Configuration Register
      DMAMUX0_CHCFG2 = DMAMUX_DISABLE;  // Arrêt du DMA 2
      DMAMUX0_CHCFG3 = DMAMUX_DISABLE;  // Arrêt du DMA 3
#endif
      bAcquisition = false;
    }

    //-------------------------------------------------------------------------
    //! \brief RAZ IT DMA
    void ClearDMAIsr()
    {
#if defined(__IMXRT1062__) // Teensy 4.1
      // Nothing to do
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
      // RAZ bit d'interruption du canal 2
      DMA_CINT = DMA_CINT_CINT(2); // (24.3.12) Clear interrupt request register
#endif
    }

    //-------------------------------------------------------------------------
    //! \brief Retourne l'adresse courante d'écriture du DMA
    virtual uint32_t GetCurrentDMAaddr()
    {
#if defined(__IMXRT1062__) // Teensy 4.1
      uint32_t *pBuff = (uint32_t *)pMemBuffEch;
      if (iBufferOK == 64)
        pBuff = (uint32_t *)&(pMemBuffEch[MAXSAMPLE+10]);
      iBufferOK = -1;
      return (uint32_t) pBuff;
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
      return (uint32_t)(DMA_TCD2_DADDR);
#endif
    }

#if defined(__IMXRT1062__) // Teensy 4.1
    //-------------------------------------------------------------------------
    //! \brief Retourne true si un Buffer est disponible
    bool IsBufferOK()
    {
      return (iBufferOK > 0);
    }
#endif
    
    //-------------------------------------------------------------------------
    //! \brief Return DMA error (0 if no error)
    virtual uint32_t GetDMAError()
    {
      uint32_t DMAErr = 0;
      //if (bAcquisition)
      //  // Même nom du registre sur T3.6 et T4.1 mais la lecture ne doit pas intervenir lorsque le DMA n'est pas lancé sur T4.1
      //  DMAErr = DMA_ES;
      return DMAErr;
    }
  protected:
    //-------------------------------------------------------------------------
    //! \brief Configuration des ADC 0 et 1 (ADC 1 en mono)
    virtual void adcInit()
    {
      //Serial.println("adcInit");
#if defined(__IMXRT1062__) // Teensy 4.1 *** ATTENTION sur T4.1 ADC2 = ADC1 T3.6 ***
      // ADC2 for Right and ADC1 for Left
      PROGMEM static const uint8_t adcT4_pin_to_channel[] = {
        7,      // 0/A0   AD_B1_02
        8,      // 1/A1   AD_B1_03
        12,     // 2/A2   AD_B1_07  Right channel on PRS
        11,     // 3/A3   AD_B1_06  Left  channel on PRS
        6,      // 4/A4   AD_B1_01
        5,      // 5/A5   AD_B1_00
        15,     // 6/A6   AD_B1_10
        0,      // 7/A7   AD_B1_11
        13,     // 8/A8   AD_B1_08
        14,     // 9/A9   AD_B1_09
        255,    // 10/A10 AD_B0_12 - only on ADC1, 1 - can't use for audio
        255,    // 11/A11 AD_B0_13 - only on ADC1, 2 - can't use for audio
        3,      // 12/A12 AD_B1_14
        4,      // 13/A13 AD_B1_15
        7,      // 14/A0  AD_B1_02
        8,      // 15/A1  AD_B1_03
        12,     // 16/A2  AD_B1_07
        11,     // 17/A3  AD_B1_06
        6,      // 18/A4  AD_B1_01
        5,      // 19/A5  AD_B1_00
        15,     // 20/A6  AD_B1_10
        0,      // 21/A7  AD_B1_11
        13,     // 22/A8  AD_B1_08
        14,     // 23/A9  AD_B1_09
        255,    // 24/A10 AD_B0_12 - only on ADC1, 1 - can't use for audio
        255,    // 25/A11 AD_B0_13 - only on ADC1, 2 - can't use for audio
        255,    // 26/A12 AD_B1_14 - only on ADC2, do not use analogRead()
        255,    // 27/A13 AD_B1_15 - only on ADC2, do not use analogRead()
        255,    // 28
        255,    // 29
        255,    // 30
        255,    // 31
        255,    // 32
        255,    // 33
        255,    // 34
        255,    // 35
        255,    // 36
        255,    // 37
        255,    // 38/A14 AD_B1_12 - only on ADC2, do not use analogRead()
        255,    // 39/A15 AD_B1_13 - only on ADC2, do not use analogRead()
        9,      // 40/A16 AD_B1_04
        10,     // 41/A17 AD_B1_05
      };
      /*
       * Left  ADC1, DMA 4, ETC_TRIG0 Trig via QTimer4 Timer 3
       * Right ADC2, DMA 5, ETC_TRIG4 Sync_mode, Trig suite DMA4
       */
      //int triggerR = 4;
      //int triggerL = 0;
      // 14.7.23 CCM Clock Gating Register 2
      CCM_CCGR2 |= CCM_CCGR2_XBAR1(CCM_CCGR_ON);       // Utilisation XBAR2 ???                           // xbar1 clock enable
    
      // 61.5.1 Crossbar A Select Register 0 (XBARAx_SEL0)
      // Connect the timer output to the ADC_ETC input (intput = 39, output = 103 + 0 = 103 "/ 2" = 51 = Crossbar A Select Register 51 (XBARA1_SEL51))
      // Input (XBARA_INn = 39) to be muxed to XBARA_OUT103 (refer to Functional Description section for input/output assignment)
      xbar_connect(XBARA1_IN_QTIMER4_TIMER3, XBARA1_OUT_ADC_ETC_TRIG00);
      xbar_connect(XBARA1_IN_QTIMER4_TIMER3, XBARA1_OUT_ADC_ETC_TRIG00 + 4);
    
      // 67.6.1.2 ADC_ETC Global Control Register
      ADC_ETC_CTRL &= ~ADC_ETC_CTRL_SOFTRST;          // Soft reset
      ADC_ETC_CTRL = ADC_ETC_CTRL_TRIG_ENABLE(1) | ADC_ETC_CTRL_DMA_MODE_SEL; // enable XBAR trigger
      // 67.6.1.6 ETC_TRIG Control Register
      ADC_ETC_TRIG4_CTRL = ADC_ETC_TRIG_CTRL_SYNC_MODE | ADC_ETC_TRIG_CTRL_TRIG_CHAIN(0); // sync_mode, chainlength=1
      ADC_ETC_TRIG0_CTRL = 0x00000; // chainlength 1, sync_mode = 0
      // 67.6.1.8 ETC_TRIG Chain 0/1 Register
      ADC_ETC_TRIG4_CHAIN_1_0 = ADC_ETC_TRIG_CHAIN_IE0(2) | ADC_ETC_TRIG_CHAIN_HWTS0(1) | ADC_ETC_TRIG_CHAIN_CSEL0(adcT4_pin_to_channel[iPin ]) ;//0x4011; // irg on done1 16401
      ADC_ETC_TRIG0_CHAIN_1_0 = ADC_ETC_TRIG_CHAIN_IE0(1) | ADC_ETC_TRIG_CHAIN_HWTS0(1) | ADC_ETC_TRIG_CHAIN_CSEL0(adcT4_pin_to_channel[iPinG]); //0x3017;   // irq done0
      // 67.6.1.5 ETC DMA control Register
      ADC_ETC_DMA_CTRL = ADC_ETC_DMA_CTRL_TRIQ_ENABLE(0);
      ADC_ETC_DMA_CTRL = ADC_ETC_DMA_CTRL_TRIQ_ENABLE(4);

/*
      
      // turn on ADC_ETC and configure to receive trigger
      if (ADC_ETC_CTRL & (ADC_ETC_CTRL_SOFTRST | ADC_ETC_CTRL_TSC_BYPASS))
      {
        ADC_ETC_CTRL = 0; // SOFTRST ADC_ETC works normally
        ADC_ETC_CTRL = 0; // Clears TSC_BYPASS, To use ADC2, this bit should be cleared
      }
      ADC_ETC_CTRL |= ADC_ETC_CTRL_DMA_MODE_SEL;                                  // Trig DMA_REQ with pulsed signal, REQ will be cleared by ACK only
      // PRE_DIVIDER ? Pre-divider for trig delay and interval. The meaning of this field will be explained in the description of TRIGa_COUNTER
      // EXT1_TRIG_PRIORITY ? External TSC1 trigger priority, 7 is highest priority, while 0 is lowest
      // EXT1_TRIG_ENABLE ? TSC1 TRIG enable register
      // EXT0_TRIG_PRIORITY ? External TSC0 trigger priority, 7 is highest priority, while 0 is lowest
      // EXT0_TRIG_ENABLE ? TSC0 TRIG enable register
      ADC_ETC_CTRL |= ADC_ETC_CTRL_TRIG_ENABLE(4);                    // TRIG enable register, 00010000b - enable external XBAR trigger 4
      ADC_ETC_CTRL |= ADC_ETC_CTRL_TRIG_ENABLE(0);                                // TRIG enable register, 00000001b - enable external XBAR trigger 0
      
      // 67.6.1.5 ETC DMA control Register
      ADC_ETC_DMA_CTRL |= ADC_ETC_DMA_CTRL_TRIQ_ENABLE(4);                 // Enable DMA request when TRIG4 done, 1b - TRIG4 DMA request enabled    
      ADC_ETC_DMA_CTRL |= ADC_ETC_DMA_CTRL_TRIQ_ENABLE(0);                 // Enable DMA request when TRIG0 done, 1b - TRIG0 DMA request enabled    

      // 67.6.1.6 ETC_TRIG Control Register
      const int len = 1;
      IMXRT_ADC_ETC.TRIG[triggerR].CTRL = ADC_ETC_TRIG_CTRL_TRIG_CHAIN(len - 1) | // Trigger chain length is 1
                                          ADC_ETC_TRIG_CTRL_SYNC_MODE |           // Trigger synchronization mode selection, TRIGa and TRIG(a+4) are triggered by TRIGa source synchronously
                                          ADC_ETC_TRIG_CTRL_TRIG_PRIORITY(7);     // External trigger priority, 7 is highest priority
                                                                                  // TRIG_MODE 0b - Hardware trigger. The softerware trigger will be ignored
                                                                                  // SW_TRIG 0b - No software trigger event generated
      IMXRT_ADC_ETC.TRIG[triggerL].CTRL = ADC_ETC_TRIG_CTRL_TRIG_CHAIN(len - 1) | // Trigger chain length is 1
                                          ADC_ETC_TRIG_CTRL_SYNC_MODE |           // Trigger synchronization mode selection, TRIGa and TRIG(a+4) are triggered by TRIGa source synchronously
                                          ADC_ETC_TRIG_CTRL_TRIG_PRIORITY(7);     // External trigger priority, 7 is highest priority
                                                                                  // TRIG_MODE 0b - Hardware trigger. The softerware trigger will be ignored
                                                                                  // SW_TRIG 0b - No software trigger event generated

      // 67.6.1.8 ETC_TRIG Chain 0/1 Register
      IMXRT_ADC_ETC.TRIG[triggerR].CHAIN_1_0 = ADC_ETC_TRIG_CHAIN_HWTS0(1) |      // ADC TRIG0 selected
        ADC_ETC_TRIG_CHAIN_CSEL0(adcT4_pin_to_channel[iPin]) |                    // Select pin
        ADC_ETC_TRIG_CHAIN_B2B0;                                                  // Enable B2B0 When Segment 0 finished (ADC COCO) then automatically trigger next ADC
                                                                                  // conversion, no need to wait until interval delay reached
                                                                                  // IE1 00b - No interrupt when finished
                                                                                  // B2B1 Disable B2B. Wait until delay value defined by TRIG1_COUNTER[SAMPLE_INTERVAL] is reached
                                                                                  // HWTS1 00000000b - no trigger selected
                                                                                  // CSEL1 ADC channel selection
                                                                                  // IE0 Segment 0 done interrupt selection 00b - No interrupt when finished
      IMXRT_ADC_ETC.TRIG[triggerL].CHAIN_1_0 = ADC_ETC_TRIG_CHAIN_HWTS0(1) |      // ADC TRIG0 selected
        ADC_ETC_TRIG_CHAIN_CSEL0(adcT4_pin_to_channel[iPinG]) |                   // Select pin
        ADC_ETC_TRIG_CHAIN_B2B0;                                                  // Enable B2B0 When Segment 0 finished (ADC COCO) then automatically trigger next ADC
                                                                                  // conversion, no need to wait until interval delay reached
                                                                                  // IE1 00b - No interrupt when finished
                                                                                  // B2B1 Disable B2B. Wait until delay value defined by TRIG1_COUNTER[SAMPLE_INTERVAL] is reached
                                                                                  // HWTS1 00000000b - no trigger selected
                                                                                  // CSEL1 ADC channel selection
*/                                                                                  // IE0 Segment 0 done interrupt selection 00b - No interrupt when finished
      // 66.8.6 Configuration register (ADCx_CFG)
      ADC2_CFG = 0;                                                     // OVWREN disable overwriting, ADTRG Software trigger, REFSEL Selects VREFH/VREFL as reference voltage
                                                                        // ADHSC Normal conversion selected, ADLPC ADC hard block not in low power mode
      ADC2_CFG = ADC_CFG_MODE(2) | ADC_CFG_ADSTS(3) | ADC_CFG_ADLSMP    // 12 bits, 25 sample period, long sample time 
               | ADC_CFG_ADICLK(1) | ADC_CFG_ADIV(1) | ADC_CFG_AVGS(1); // IPG clock divided by 2, Input clock / 2, 8 samples averaged
      ADC2_CFG |= ADC_CFG_OVWREN;                                       // OVWREN enable overwriting
      ADC2_CFG |= ADC_CFG_ADLPC;                                        // ADC hard block in low power mode
      ADC2_CFG |= ADC_CFG_ADHSC;                                        // High speed conversion selected
      ADC1_CFG = 0;                                                     // OVWREN disable overwriting, ADTRG Software trigger, REFSEL Selects VREFH/VREFL as reference voltage
                                                                        // ADHSC Normal conversion selected, ADLPC ADC hard block not in low power mode
      ADC1_CFG = ADC_CFG_MODE(2) | ADC_CFG_ADSTS(3) | ADC_CFG_ADLSMP    // 12 bits, 25 sample period, long sample time 
               | ADC_CFG_ADICLK(1) | ADC_CFG_ADIV(1) | ADC_CFG_AVGS(1); // IPG clock divided by 2, Input clock / 2, 8 samples averaged
      ADC1_CFG |= ADC_CFG_OVWREN;                                       // OVWREN enable overwriting
      ADC1_CFG |= ADC_CFG_ADLPC;                                        // ADC hard block in low power mode
      ADC1_CFG |= ADC_CFG_ADHSC;                                        // High speed conversion selected
      // 66.8.7 General control register (ADCx_GC)
      ADC2_GC &= ~ADC_GC_ACFE;                                          // Compare function disable
      ADC2_GC &= ~ADC_GC_ACFGT;                                         // Compare Function Greater Than disable
      ADC2_GC &= ~ADC_GC_ACREN;                                         // Range function disable
      ADC2_GC |= ADC_GC_AVGE;                                           // Averaging
      ADC2_GC &= ~ADC_GC_ADCO;                                          // Continuous Conversion disable
      ADC2_GC |= ADC_GC_DMAEN;                                          // DMA enable
      ADC2_GC &= ~ADC_GC_ADACKEN;                                       // ADACKEN Asynchronous clock output disable
      ADC1_GC &= ~ADC_GC_ACFE;                                          // Compare function disable
      ADC1_GC &= ~ADC_GC_ACFGT;                                         // Compare Function Greater Than disable
      ADC1_GC &= ~ADC_GC_ACREN;                                         // Range function disable
      ADC1_GC |= ADC_GC_AVGE;                                           // Averaging
      ADC1_GC &= ~ADC_GC_ADCO;                                          // Continuous Conversion disable
      ADC1_GC |= ADC_GC_DMAEN;                                          // DMA enable
      ADC1_GC &= ~ADC_GC_ADACKEN;                                       // ADACKEN Asynchronous clock output disable

      adcCalibrate();                                                   // Calibration de l'ADC 2

      // 66.8.1 Control register for hardware triggers (ADCx_HC0)
      ADC2_HC0 = ADC_HC_ADCH(16);                                       // 16 = controlled by ADC_ETC
      ADC1_HC0 = ADC_HC_ADCH(16);                                       // 16 = controlled by ADC_ETC
      // 66.8.6 Configuration register (ADCx_CFG)
      ADC2_CFG |= ADC_CFG_ADTRG;                                        // Hardware trigger selected (after calibration)
      ADC1_CFG |= ADC_CFG_ADTRG;                                        // Hardware trigger selected (after calibration)
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
      // Tableau de conversion des broches Teensy vers ARM
      // ADC0 = Gauche, ADC1 = Droite
      // ADC0, pas de restriction - ATTENTION, ADC1, seulement A2 et A3
      //                                A0  A1  A2  A3  A4  A5  A6  A7  A8  A9
      const uint8_t TsyADCtoARMpin[] = { 5, 14,  8,  9, 13, 12,  6,  7, 15,  4};
      const uint8_t TMuxSel[]        = { 1,  1,  0,  0,  1,  1,  1,  0,  0,  0};
      // (13.2.16) System clock gating control register 6
      SIM_SCGC6 |= SIM_SCGC6_ADC0;
      SIM_SCGC3 |= SIM_SCGC3_ADC1;
      // (39.4.2) ADC Configuration Register 1
      ADC0_CFG1  = ADC_CFG1_ADIV(1);    // Input clock / 2
      ADC0_CFG1 |= ADC_CFG1_MODE(1);    // DIFF=0 Single-ended, 12-bit
      ADC0_CFG1 |= ADC_CFG1_ADLSMP;     // Long sample time
      ADC1_CFG1  = ADC_CFG1_ADIV(1);    // Input clock / 2
      ADC1_CFG1 |= ADC_CFG1_MODE(1);    // DIFF=0 Single-ended, 12-bit
      ADC1_CFG1 |= ADC_CFG1_ADLSMP;     // Long sample time
      // (39.4.3) ADC Configuration Register 2
      ADC0_CFG2  = ADC_CFG2_ADLSTS(3);  // 2 extra ADCK cycles; 6 ADCK cycles total sample time
      if (TMuxSel[iPinG] == 1)
        ADC0_CFG2 |= ADC_CFG2_MUXSEL;   // ADxxB channels are selected (See table 39.1.3.1)
      ADC0_CFG2 |= ADC_CFG2_MUXSEL;     // ADxxB channels are selected (See table 39.1.3.1)
      ADC1_CFG2  = ADC_CFG2_ADLSTS(3);  // 2 extra ADCK cycles; 6 ADCK cycles total sample time
      if (TMuxSel[iPin] == 1)
        ADC1_CFG2 |= ADC_CFG2_MUXSEL;   // ADxxB channels are selected (See table 39.1.3.1)
      // (39.4.6) Status and Control Register 2
      ADC0_SC2  = ADC_SC2_REFSEL(0);    // Voltage reference = Vcc
      ADC0_SC2 |= ADC_SC2_DMAEN;        // Enable DMA
      ADC1_SC2  = ADC_SC2_REFSEL(0);    // Voltage reference = Vcc
      ADC1_SC2 |= ADC_SC2_DMAEN;        // Enable DMA
      // (39.4.7) Status and Control Register 3
      ADC0_SC3  = ADC_SC3_AVGE;         // Enable hardware averaging
      ADC0_SC3 |= ADC_SC3_AVGS(1);      // Hardware average 8 samples
      ADC1_SC3  = ADC_SC3_AVGE;         // Enable hardware averaging
      ADC1_SC3 |= ADC_SC3_AVGS(1);      // Hardware average 8 samples
      // Calibration des ADC 0 et 1
      adcCalibrate();

      // (13.2.7) System options register 7
      SIM_SOPT7 = 0;
      SIM_SOPT7 |= SIM_SOPT7_ADC0ALTTRGEN;  // ADC0 Alt trigger select
      SIM_SOPT7 |= SIM_SOPT7_ADC0TRGSEL(4); // ADC0 PIT trigger 0 select
      SIM_SOPT7 |= SIM_SOPT7_ADC1ALTTRGEN;  // ADC1 Alt trigger select
      SIM_SOPT7 |= SIM_SOPT7_ADC1TRGSEL(4); // ADC1 PIT trigger 0 select

      // (39.4.1) ADC Status and Control Registers 1 (ADCx_SC1n)
      //Serial.printf("ADC0 pin A%d, ADC1 pin A%d\n", iPin, iPinG);
      uint32_t sc1a_config = 0;
      sc1a_config |= ADC_SC1_ADCH(TsyADCtoARMpin[iPinG]); // Left Channel selection
      sc1a_config &= ~ADC_SC1_DIFF;                       // DIFF=0, Single-ended mode
      sc1a_config &= ~ADC_SC1_AIEN;                       // Disable interrupts
      ADC0_SC1A = sc1a_config;
      sc1a_config = 0;
      sc1a_config |= ADC_SC1_ADCH(TsyADCtoARMpin[iPin]);  // Right Channel selection
      sc1a_config &= ~ADC_SC1_DIFF;                       // DIFF=0, Single-ended mode
      sc1a_config &= ~ADC_SC1_AIEN;                       // Disable interrupts
      ADC1_SC1A = sc1a_config;

      // (39.4.6) Status and Control Register 2
      ADC0_SC2 |= ADC_SC2_ADTRG;    // Enable hardware trigger
      ADC1_SC2 |= ADC_SC2_ADTRG;    // Enable hardware trigger
#endif
    }
  
    //-------------------------------------------------------------------------
    //! \brief Calibration du DAC
    virtual void adcCalibrate()
    {
#if defined(__IMXRT1062__) // Teensy 4.1 *** ATTENTION sur T4.1 ADC2 = ADC1 T3.6 ***
      // Calibration ADC2
      ADC2_GC &= ~ADC_GC_CAL;                                           // Reset Cal bit
      ADC2_GS = ADC_GS_CALF;                                            // Reset calibration status
      ADC2_GC = ADC_GC_CAL;                                             // Start calibration
      while (ADC2_GC & ADC_GC_CAL);                                     // Wait for calibration
      if ((ADC2_GS & ADC_GS_CALF) > 0) Serial.println("Acquisition::adcCalibrate Teensy 4.1 Error calibration ADC2");
      // Calibration ADC1
      ADC1_GC &= ~ADC_GC_CAL;                                           // Reset Cal bit
      ADC1_GS = ADC_GS_CALF;                                            // Reset calibration status
      ADC1_GC = ADC_GC_CAL;                                             // Start calibration
      while (ADC1_GC & ADC_GC_CAL);                                     // Wait for calibration
      if ((ADC1_GS & ADC_GS_CALF) > 0) Serial.println("Acquisition::adcCalibrate Teensy 4.1 Error calibration ADC1");
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
      uint16_t sum;
      // Begin calibration ADC0, initiate automatic calibration sequence
      ADC0_SC3 = ADC_SC3_CAL;
      // Wait for calibration
      while (ADC0_SC3 & ADC_SC3_CAL);
      if (ADC0_SC3 & ADC_SC3_CALF)
         Serial.println("Calibration ADC0 Failed !!!");
      // Plus side gain
      sum = (ADC0_CLP0 + ADC0_CLP1 + ADC0_CLP2 + ADC0_CLP3 + ADC0_CLP4 + ADC0_CLPS) >> 1;
      sum |= (1 << 15);
      ADC0_PG = sum;
      // Minus side gain (not used in single-ended mode)
      sum = (ADC0_CLMS + ADC0_CLM4 + ADC0_CLM3 + ADC0_CLM2 + ADC0_CLM1 + ADC0_CLM0) >> 1;
      sum |= (1 << 15);
      ADC0_MG = sum;
      
      // Begin calibration ADC1, initiate automatic calibration sequence
      ADC1_SC3 = ADC_SC3_CAL;
      // Wait for calibration
      while (ADC1_SC3 & ADC_SC3_CAL);
      if (ADC1_SC3 & ADC_SC3_CALF)
         Serial.println("Calibration ADC1 Failed !!!");
      // Plus side gain
      sum = (ADC1_CLP0 + ADC1_CLP1 + ADC1_CLP2 + ADC1_CLP3 + ADC1_CLP4 + ADC1_CLPS) >> 1;
      sum |= (1 << 15);
      ADC1_PG = sum;
      // Minus side gain (not used in single-ended mode)
      sum = (ADC1_CLMS + ADC1_CLM4 + ADC1_CLM3 + ADC1_CLM2 + ADC1_CLM1 + ADC1_CLM0) >> 1;
      sum |= (1 << 15);
      ADC1_MG = sum;
#endif
    }
    
    //-------------------------------------------------------------------------
    //! \brief Init du timer pour générer la Fe
    virtual void pitInit()
    {
#if defined(__IMXRT1062__) // Teensy 4.1
      // Set TMR4 Timer 3
      int comp3 = ((float)F_BUS_ACTUAL) / (float)iFe / 2.0;
      //Serial.printf("AcquisitionStereo::pitInit F_CPU_ACTUAL %d, F_BUS_ACTUAL %d, iFe %d, comp3 %d, uiNbSamplesBuffer %d\n", F_CPU_ACTUAL, F_BUS_ACTUAL, iFe, comp3, uiNbSamplesBuffer);
      //Serial.printf("HALFDMACPY %d, MAXDMACPY %d, Deb %d, Milieu %d, Fin %d\n", HALFDMACPY, MAXDMACPY, pBuffEch, &(pBuffEch[MAXSAMPLE]), &(pBuffEch[MAXSAMPLE*2]));
      // 54.9.1.15 Timer Channel Enable Register
      TMR4_ENBL &= ~(1<<3);             // Disable Timer
      // 54.9.1.5 Timer Channel Load Register
      TMR4_LOAD3 = 0;                   // Reset load register
      // 54.9.1.2 Timer Channel Compare Register 1
      TMR4_COMP13  = comp3;             // Set up compare 1 register
      // 54.9.1.10 Timer Channel Comparator Load Register 1
      TMR4_CMPLD13 = comp3;             // Also set the compare preload register
      // 54.9.1.12 Timer Channel Comparator Status and Control Register
      TMR4_CSCTRL3 = //DBG_EN = 0       // Debug Actions Enable, Normal operation
                 // FAULT = 0           // Fault Enable, Fault function disabled
                 // ALT_LOAD = 0        // Alternative Load Enable, Counter can be re-initialized only with the LOAD register.
                 // ROC = 0             // Reload on Capture, Do not reload the counter on a capture event
                 // TCI = 0             // Triggered Count Initialization Control, Stop counter upon receiving a second trigger event while still counting from the first trigger event
                 // UP = 0              // read only
                 // TCF2EN = 0          // Timer Compare 2 Interrupt Enable, no interrupt
                   TMR_CSCTRL_TCF1EN    // Timer Compare 1 Interrupt Enable, Interrupt
                 // TCF2 = 0            // Timer Compare 2 Interrupt Flag
                 // TCF1 = 0            // Timer Compare 1 Interrupt Flag 
                 // CL2 = 0             // Compare Load Control 2, Never preload
                 | TMR_CSCTRL_CL1(1);   // Compare Load Control 1, Load upon successful compare with the value in COMP1
      // 54.9.1.8 Timer Channel Control Register
      TMR4_CTRL3 = TMR_CTRL_CM(1)       // Count Mode, Count rising edges of primary source
                 | TMR_CTRL_PCS(8)      // Primary Count Source, IP bus clock divide by 1 prescaler
                 // SCS = 0             // Secondary Count Source, Counter 0 input pin
                 // ONCE = 0            // Count Once, Count repeatedly
                 | TMR_CTRL_LENGTH      // Count Length, Count until compare, then re-initialize
                 // DIR = 0             // Count Direction, Count Up
                 // COINIT = 0          // Co-Channel Initialization, no Co-Channel
                 | TMR_CTRL_OUTMODE(3); // Output Mode, Toggle OFLAG output on successful compare
      // 54.9.1.14 Timer Channel DMA Enable Register
      TMR4_DMA3 = TMR_DMA_CMPLD1DE;     // Comparator Preload Register 1 DMA Enable
      // 54.9.1.7 Timer Channel Counter Register
      TMR4_CNTR3 = 0;
      // 54.9.1.15 Timer Channel Enable Register
      TMR4_ENBL |= (1<<3);              // Enable Timer
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
      // Set PIT 0 (Periodic Interrupt Timer)
      // Calcul de la période en fonction de la Fe
      uint32_t PitPeriod = (F_BUS / iFe) - 1;
      // (13.2.16) Send system clock to PIT
      SIM_SCGC6 |= SIM_SCGC6_PIT;
      // (46.4.1) Turn on PIT
      PIT_MCR = 0x00;
      // (46.4.4) Set timer 0 period
      PIT_LDVAL0 = PitPeriod;
      // (46.4.6) Enable timer 0
      PIT_TCTRL0 |= PIT_TCTRL_TEN;
#endif
    }
    
    //-------------------------------------------------------------------------
    //! \brief Init des DMA pour remplir le double buffer avec interruption à la moitiée et sur la totalité
    //! Après remplissage, le DMA continue au début
    virtual void dmaInit()
    {
#if defined(__IMXRT1062__) // Teensy 4.1
      // 14.7.26 CCM Clock Gating Register 5
      CCM_CCGR5 |= CCM_CCGR5_DMA(CCM_CCGR_ON);                          // DMA clock enable
      // 6.5.5.2 Control
      DMA_CR = DMA_CR_EMLM;                                             // CX and ECX Normal operation, GRP1PRI and GRP1PRI 0, EMLM 4 fields in ATTR,
                                                                        // CLM Continuous link mode is off, HALT and HOE Normal operation,
                                                                        // ERGA and ERCA Fixed priority arbitration, EDBG When the chip is in Debug mode, the eDMA continues to operate
      DMA_CR = DMA_CR_GRP1PRI | DMA_CR_EMLM;// | DMA_CR_EDBG;
      // 6.5.5.6 Clear Enable Error Interrupt
      DMA_CEEI = DMACHR;                                                // Clear chanel R
      DMA_CEEI = DMACHL;                                                // Clear chanel L
      // 6.5.5.8 Clear Enable Request
      DMA_CERQ = DMACHR;                                                // Clear chanel R
      DMA_CERQ = DMACHL;                                                // Clear chanel L
      // 6.5.5.12 Clear Error
      DMA_CERR = DMACHR;                                                // Clear chanel R
      DMA_CERR = DMACHL;                                                // Clear chanel L
      // 6.5.5.13 Clear Interrupt Request
      DMA_CINT = DMACHR;                                                // Clear chanel R
      DMA_CINT = DMACHL;                                                // Clear chanel L
      delay(2);
      // 6.5.5.19 TCD Source Address
      IMXRT_DMA_TCD[DMACHR].SADDR = &(IMXRT_ADC_ETC.TRIG[4].RESULT_1_0);   // Source address is ADC_ETC TRIG4 ADC2 (right channel)
      IMXRT_DMA_TCD[DMACHL].SADDR = &(IMXRT_ADC_ETC.TRIG[0].RESULT_1_0);   // Source address is ADC_ETC TRIG0 ADC1 (left  channel)
      // 6.5.5.20 TCD Signed Source Address Offset
      IMXRT_DMA_TCD[DMACHR].SOFF = 0;                                   // Don't advance source pointer after minor loop
      IMXRT_DMA_TCD[DMACHL].SOFF = 0;                                   // Don't advance source pointer after minor loop
      // 6.5.5.21 TCD Transfer Attributes
      IMXRT_DMA_TCD[DMACHR].ATTR  = DMA_TCD_ATTR_SSIZE(1);              // Source data transfer size (16-bit), source and destination address modulo feature is disabled
      IMXRT_DMA_TCD[DMACHR].ATTR |= DMA_TCD_ATTR_DSIZE(1);              // Destination data transfer size (16-bit)
      IMXRT_DMA_TCD[DMACHL].ATTR  = DMA_TCD_ATTR_SSIZE(1);              // Source data transfer size (16-bit), source and destination address modulo feature is disabled
      IMXRT_DMA_TCD[DMACHL].ATTR |= DMA_TCD_ATTR_DSIZE(1);              // Destination data transfer size (16-bit)
      // 6.5.5.22 TCD Minor Byte Count
      IMXRT_DMA_TCD[DMACHR].NBYTES_MLNO = sizeof(uint16_t);             // Number of bytes to transfer (in each service request)
      IMXRT_DMA_TCD[DMACHL].NBYTES_MLNO = sizeof(uint16_t);             // Number of bytes to transfer (in each service request)
      // 6.5.5.25 TCD Last Source Address Adjustment
      IMXRT_DMA_TCD[DMACHR].SLAST = 0;                                  // Don't advance source pointer after major loop
      IMXRT_DMA_TCD[DMACHL].SLAST = 0;                                  // Don't advance source pointer after major loop
      // 6.5.5.26 TCD Destination Address
      IMXRT_DMA_TCD[DMACHR].DADDR = &(samplesDMAT41[1]);                // Destination Address, 1er mot: Gauche, 2ème mot: Droite et ainsi de suite
      IMXRT_DMA_TCD[DMACHL].DADDR = samplesDMAT41;                      // Destination Address, 1er mot: Gauche, 2ème mot: Droite et ainsi de suite
      // 6.5.5.27 TCD Signed Destination Address Offset
      IMXRT_DMA_TCD[DMACHR].DOFF = sizeof(uint16_t) * 2;                // Advance destination pointer by 4 bytes per value for stereo mode
      IMXRT_DMA_TCD[DMACHL].DOFF = sizeof(uint16_t) * 2;                // Advance destination pointer by 4 bytes per value for stereo mode
      // 6.5.5.28 TCD Current Minor Loop Link, Major Loop Count
      IMXRT_DMA_TCD[DMACHR].CITER_ELINKNO = MAX_DMA_SETERO_T41;         // Current Major Iteration Count
      IMXRT_DMA_TCD[DMACHL].CITER_ELINKNO = MAX_DMA_SETERO_T41;         // Current Major Iteration Count
      // 6.5.5.30 TCD Last Destination Address Adjustment/Scatter Gather Address
      IMXRT_DMA_TCD[DMACHR].DLASTSGA = -(sizeof(uint16_t) * MAX_DMA_SETERO_T41 * 2); // Return to &pBuffEch[1] after major loop
      IMXRT_DMA_TCD[DMACHL].DLASTSGA = -(sizeof(uint16_t) * MAX_DMA_SETERO_T41 * 2); // Return to &pBuffEch[0] after major loop
      // 6.5.5.31 TCD Control and Status
      IMXRT_DMA_TCD[DMACHR].CSR  = DMA_TCD_CSR_INTMAJOR;                // Interrupt at major loop (CITER == 0)
      IMXRT_DMA_TCD[DMACHR].CSR |= DMA_TCD_CSR_INTHALF;                 // Also interrupt at half major loop (CITER == BITER/2)
      IMXRT_DMA_TCD[DMACHL].CSR  = DMA_TCD_CSR_INTMAJOR;                // Interrupt at major loop (CITER == 0)
      IMXRT_DMA_TCD[DMACHL].CSR |= DMA_TCD_CSR_INTHALF;                 // Also interrupt at half major loop (CITER == BITER/2)
      // 6.5.5.32 TCD Beginning Minor Loop Link, Major Loop Count
      IMXRT_DMA_TCD[DMACHR].BITER_ELINKNO = MAX_DMA_SETERO_T41;         // Starting Major Iteration Count
      IMXRT_DMA_TCD[DMACHL].BITER_ELINKNO = MAX_DMA_SETERO_T41;         // Starting Major Iteration Count

      // 5.6.1.2 Channel a Configuration Register
      volatile uint32_t *mux = &DMAMUX_CHCFG0 + DMACHL;
      *mux  = 0;
      *mux  = (DMAMUX_SOURCE_ADC_ETC & 0x7F) | DMAMUX_CHCFG_ENBL;       // Route ADC_ETC to DMA_MUX for right channel
      // 6.5.5.33 TCD Beginning Minor Loop Link, Major Loop Count
      IMXRT_DMA_TCD[DMACHL].BITER = (IMXRT_DMA_TCD[DMACHL].BITER & ~DMA_TCD_BITER_ELINKYES_LINKCH_MASK)
        | DMA_TCD_BITER_ELINKYES_LINKCH(DMACHR) | DMA_TCD_BITER_ELINKYES_ELINK;
      IMXRT_DMA_TCD[DMACHL].CITER = IMXRT_DMA_TCD[DMACHL].BITER ;
      IMXRT_DMA_TCD[DMACHL].CSR |= (IMXRT_DMA_TCD[DMACHL].CSR & ~(DMA_TCD_CSR_MAJORLINKCH_MASK|DMA_TCD_CSR_DONE))
        | DMA_TCD_CSR_MAJORLINKCH(DMACHR) | DMA_TCD_CSR_MAJORELINK;
      // Préparation copie 256 échantillons dans le buffer 8192
      pBuffCopy = pMemBuffEch;
      iNbCopy   = 0;
      iBufferOK = -1;

      // 6.5.5.9 Set Enable Request
      DMA_SERQ = DMACHL;                                                // Enable DMA left
      DMA_SERQ = DMACHR;                                                // Enable DMA right

      // Set ISR vector with highest priority for DMA Left
      _VectorsRam[DMACHL + IRQ_DMA_CH0 + 16] = AcquisitionStereo::dma_T41_isrL;
      NVIC_ENABLE_IRQ(IRQ_DMA_CH0 + DMACHL);
      // No interrupt for DMA Right
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
      // (13.2.16-17) System clock gating control registers 6 and 7
      SIM_SCGC7 |= SIM_SCGC7_DMA;     // Enable DMA clock
      SIM_SCGC6 |= SIM_SCGC6_DMAMUX;  // Enable DMAMUX clock

      // (24.3.11) Clear Error Register
      DMA_CERR = DMA_CERR_CAEI; // Clear all bits in ERR
      // (24.3.1) Control Register
      DMA_CR = 0;
      DMA_CR = DMA_CR_GRP0PRI;
 
      // Init DMA 2 (ADC1 et DMA2 pour Droite, ADC0 et DMA3 pour Gauche)
      // ======= Source droite =======
      // (24.3.18) TCD Source Address
      DMA_TCD2_SADDR = &ADC1_RA;
      // (24.3.20) TCD Transfer Attributes
      DMA_TCD2_ATTR = 0x0000;
      DMA_TCD2_ATTR |= DMA_TCD_ATTR_SSIZE(1);             // Source data transfer size (16-bit)
      // (24.3.19) TCD Signed Source Address Offset
      DMA_TCD2_SOFF = 0;                                  // Don't advance source pointer after minor loop
      // (24.3.24) TCD Last Source Address Adjustment
      DMA_TCD2_SLAST = 0;                                 // Don't advance source pointer after major loop
      // (24.3.21) TCD Minor Byte Count (Minor Loop Mapping Disabled) Number of bytes to transfer (in each service request)
      DMA_TCD2_NBYTES_MLNO = sizeof(uint16_t);            // One sample of 2 bytes

      // ======= Destination droite =======
      // (21.3.24) TCD Destination Address
      DMA_TCD2_DADDR = &pBuffEch[1];          // 1er mot: Gauche, 2ème mot: Droite et ainsi de suite
      // (24.3.20) TCD Transfer Attributes
      DMA_TCD2_ATTR |= DMA_TCD_ATTR_DSIZE(1); // Destination data transfer size (16-bit)
      // (24.3.26) TCD Signed Destination Address Offset
      DMA_TCD2_DOFF = sizeof(uint16_t) * 2;   // Advance destination pointer by 4 bytes per value for stereo mode
      // (24.3.29) TCD Last Destination Address Adjustment/Scatter Gather Address
      DMA_TCD2_DLASTSGA = -(sizeof(uint16_t) * uiNbSamplesBuffer * 2); // Return to &pBuffEch[0] after major loop
      // (24.3.28) TCD Current Minor Loop Link, Major Loop Count (Channel Linking Disabled)
      DMA_TCD2_CITER_ELINKNO = uiNbSamplesBuffer;  // Current Major Iteration Count
      // (24.3.32) TCD Beginning Minor Loop Link, Major Loop Count (Channel Linking Disabled)
      DMA_TCD2_BITER_ELINKNO = uiNbSamplesBuffer;  // Starting Major Iteration Count

      // ======= Interrupts =======
      // (24.3.30) TCD Control and Status
      DMA_TCD2_CSR  = DMA_TCD_CSR_INTMAJOR; // Interrupt at major loop (CITER == 0)
      DMA_TCD2_CSR |= DMA_TCD_CSR_INTHALF;  // Also interrupt at half major loop (CITER == BITER/2)
      // Enable interrupt request
      //_VectorsRam[2 + IRQ_DMA_CH0 + 16] = dma_ch2_isr;
      NVIC_ENABLE_IRQ(IRQ_DMA_CH2);
      //attachInterruptVector(IRQ_DMA_CH2, dma_ch2_isr)

      // ======= Triggering =======
      // (23.4.1) Channel Configuration Register
      // Set ADC 1 as source DMA2, enable DMA MUX
      DMAMUX0_CHCFG2 = DMAMUX_DISABLE;
      DMAMUX0_CHCFG2 = DMAMUX_SOURCE_ADC1 | DMAMUX_ENABLE;
    
      // Init DMA 3 (ADC1 et DMA2 pour Droite, ADC0 et DMA3 pour Gauche)
      // ======= Source Gauche =======
      // (24.3.18) TCD Source Address
      DMA_TCD3_SADDR = &ADC0_RA;
      // (24.3.20) TCD Transfer Attributes
      DMA_TCD3_ATTR = 0x0000;
      DMA_TCD3_ATTR |= DMA_TCD_ATTR_SSIZE(1);             // Source data transfer size (16-bit)
      // (24.3.19) TCD Signed Source Address Offset
      DMA_TCD3_SOFF = 0;                                  // Don't advance source pointer after minor loop
      // (24.3.24) TCD Last Source Address Adjustment
      DMA_TCD3_SLAST = 0;                                 // Don't advance source pointer after major loop
      // Number of bytes to transfer (in each service request)
      DMA_TCD3_NBYTES_MLNO = sizeof(uint16_t);

      // ======= Destination Gauche =======
      // (21.3.24) TCD Destination Address
      DMA_TCD3_DADDR = pBuffEch;              // 1er mot: Gauche, 2ème mot: Droite et ainsi de suite
      // (24.3.20) TCD Transfer Attributes
      DMA_TCD3_ATTR |= DMA_TCD_ATTR_DSIZE(1); // Destination data transfer size (16-bit)
      // (24.3.26) TCD Signed Destination Address Offset
      DMA_TCD3_DOFF = sizeof(uint16_t) * 2;   // Advance destination pointer by 4 bytes per value for stereo mode
      // (24.3.29) TCD Last Destination Address Adjustment/Scatter Gather Address
      DMA_TCD3_DLASTSGA = -(sizeof(uint16_t) * uiNbSamplesBuffer * 2); // Return to &pBuffEch[1] after major loop
      // (24.3.28) TCD Current Minor Loop Link, Major Loop Count (Channel Linking Disabled)
      DMA_TCD3_CITER_ELINKNO = uiNbSamplesBuffer;  // Current Major Iteration Count
      // (24.3.32) TCD Beginning Minor Loop Link, Major Loop Count (Channel Linking Disabled)
      DMA_TCD3_BITER_ELINKNO = uiNbSamplesBuffer;  // Starting Major Iteration Count

      // ======= Interrupts =======
      // (24.3.30) TCD Control and Status
      DMA_TCD3_CSR  = DMA_TCD_CSR_INTMAJOR; // Interrupt at major loop (CITER == 0)
      DMA_TCD3_CSR |= DMA_TCD_CSR_INTHALF;  // Also interrupt at half major loop (CITER == BITER/2)
      // No interrupts for DMA3
    
      // ======= Triggering =======
      // (23.4.1) Channel Configuration Register
      // Set ADC 0 as source of DMA3, enable DMA MUX
      DMAMUX0_CHCFG3 = DMAMUX_DISABLE;
      DMAMUX0_CHCFG3 = DMAMUX_SOURCE_ADC0 | DMAMUX_ENABLE;

      // Enable request input signal for channel 2 and 3
      DMA_SERQ |= DMA_SERQ_SERQ(2); // Enable channel 2 DMA requests
      DMA_SERQ |= DMA_SERQ_SERQ(3); // Enable channel 3 DMA requests
#endif
    }

#if defined(__IMXRT1062__) // Teensy 4.1
    //-------------------------------------------------------------------------
    //! \brief gestion des interruptions DMA Teensy 4.1 suite à 256 échantillons
    //! et remplissage du buffer habituel
    static void dma_T41_isrL()
    {
      /*if (IMXRT_DMA_TCD[DMACHL].DADDR < &samplesDMAT41[MAX_DMA_SETERO_T41])
        Serial.printf("DMA R Flop NbCpy %d, pBuffCopy %d\n", iNbCopy, pBuffCopy);
      else
        Serial.printf("DMA R Flip NbCpy %d, pBuffCopy %d\n", iNbCopy, pBuffCopy);*/
      // Par défaut, début du buffer
      uint16_t *pBuff = samplesDMAT41;
      if (IMXRT_DMA_TCD[DMACHL].DADDR < &(samplesDMAT41[MAX_DMA_SETERO_T41]))
        // Le DMA est dans la partie basse, on copie la partie haute
        pBuff = &(samplesDMAT41[MAX_DMA_SETERO_T41]);
      // Récupération des données éventuellement en cache suite au transfert DMA
      arm_dcache_delete((void*)pBuff, sizeof(uint16_t)*MAX_DMA_SETERO_T41);
      // Copie du buffer 256 échantillons dans le grand buffer MAXSAMPLE
      memmove(pBuffCopy, pBuff, sizeof(int16_t)*MAX_DMA_SETERO_T41);
      // Incrément et test si on a copié MAXSAMPLE
      iNbCopy++;
      pBuffCopy += MAX_DMA_SETERO_T41;
      if (iNbCopy == HALFDMACPY)
      {
        /*Serial.printf("Flip iNbCopy %d pBuffCopy %d\n", iNbCopy, pBuffCopy);
        if (iBufferOK > -1)
          Serial.println("Buffer loupé");*/
        // Indique que les données sont disponible pour traitement
        iBufferOK = iNbCopy;
      }
      else if (iNbCopy >= MAXDMACPY)
      {
        /*Serial.printf("Flop iNbCopy %d pBuffCopy %d\n", iNbCopy, pBuffCopy);
        if (iBufferOK > -1)
          Serial.println("Buffer loupé");*/
        // Indique que les données sont disponible pour traitement
        iBufferOK = iNbCopy;
        // Prochaine copie au début du grand buffer
        pBuffCopy = pMemBuffEch;
        iNbCopy   = 0;
      }
      //if (iBufferOK)
      //  Serial.println("Buff dispo");
      DMA_CINT = DMACHL;
      DMA_CDNE = DMACHL;
      asm("dsb");
    }

#endif

    //! N° de la broche d'entrée de l'ADC gauche
    unsigned short iPinG;
};

#endif
