//-------------------------------------------------------------------------
//! \file CModeRecorder.h
//! \brief Classes de gestion des modes enregistrement
//! \author Jean-Do. Vrignault
/* 
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 /*
  * - CModeGenericRec
  * -   CModeAutoRecorder
  * -     CModeProtocolePFixe
  * -     CModeSynchro
  * -   CModeProtocole
  * -     CModeProtocoleRoutier
  * -     CModeProtocolePedestre
  */
#include "CModeGeneric.h"
#include "CRecorder.h"
#include "CSynchroLink.h"

#ifndef CMODERECORDER_H
#define CMODERECORDER_H

// To chek batteries every 5mn
//#define CHECKBAT

//-------------------------------------------------------------------------
//! \class CModeGenericRec
//! \brief Classe générique de gestion du mode enregistrement
class CModeGenericRec : public CModeGeneric
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructeur
  CModeGenericRec();

  //-------------------------------------------------------------------------
  //! \brief Destructeur
  ~CModeGenericRec();

  //-------------------------------------------------------------------------
  //! \brief Creation du gestionnaire d'enregistrement
  virtual void CreateRecorder();
  
  //-------------------------------------------------------------------------
  //! \brief Début du mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  //! \brief Fin du mode
  virtual void EndMode();

  //-------------------------------------------------------------------------
  //! \brief Gestion des tâches de fonds
  virtual void OnLoop();

  //-------------------------------------------------------------------------
  //! \brief Mémorisation des paramètres dans le log
  void ParamsToLog();

protected:
  //! Gestionnaire acquisition et enregistrement
  CGenericRecorder *pRecorder;

  //! Pour mesure de la taille restante sur la carte SD
  unsigned long uiTimeTestSD;

  // Nombre de test de taille SD erroné
  unsigned int iNbSizeSD;
};

//-------------------------------------------------------------------------
//! \class CModeAutoRecorder
//! \brief Classe de gestion du mode enregistrement automatique
class CModeAutoRecorder : public CModeGenericRec
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructeur
  CModeAutoRecorder();

  //-------------------------------------------------------------------------
  //! \brief Début du mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  //! \brief Fin du mode
  virtual void EndMode();
  
  //-------------------------------------------------------------------------
  //! \brief Affichage du mode sur l'écran
  //! Cette méthode est appelée régulièrement par le loop principal
  //! à charge des différents modes d'afficher les informations nécessaires
  virtual void PrintMode();
  
  //-------------------------------------------------------------------------
  //! \brief Traitement des ordres claviers
  //! Si la touche est une touche de changement de mode, retourne le mode demandé
  //! Cette méthode est appelée régulièrement par le loop principal
  //! à charge des différents modes de traiter les actions opérateurs
  //! \param key Touch to treat
  virtual int KeyManager(
    unsigned short key
    );

protected:
  //! Seconde courante pour une gestion de l'affichage toute les secondes
  uint8_t iCurrentSecond;
  
  //! Décompteur du mode affichage acquisition
  uint8_t iDecompteurAffAcq;

  //! Indique que l'affichage des infos de débuts d'enregistrement est effectué
  bool bAffInfos;

#ifdef CHECKBAT
  //! Time to check battery
  unsigned long uiTimeCheck;
#endif
};

//-------------------------------------------------------------------------
//! \class CModeProtocole
//! \brief Classe générique de gestion des modes protocole
class CModeProtocole : public CModeGenericRec
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructeur
  CModeProtocole();

  //-------------------------------------------------------------------------
  //! \brief Début du mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  //! \brief Fin du mode
  virtual void EndMode();
  
  //-------------------------------------------------------------------------
  //! \brief  Reading saved settings
  //! \return true if OK
  virtual bool ReadParams();
  
  //-------------------------------------------------------------------------
  //! \brief Affichage du mode sur l'écran
  //! Cette méthode est appelée régulièrement par le loop principal
  //! à charge des différents modes d'afficher les informations nécessaires
  virtual void PrintMode();

  //-------------------------------------------------------------------------
  //! \brief Affichage du protocole en état stop
  virtual void PrintPotocolStop();

  //-------------------------------------------------------------------------
  //! \brief Affichage du protocole en état enregistrement
  virtual void PrintProtocolRecord();

  //-------------------------------------------------------------------------
  //! \brief Affichage du protocole en état Fin
  virtual void PrintProtocolEnd();

  //-------------------------------------------------------------------------
  //! \brief Affichage du protocole en état Pause
  virtual void PrintProtocolPause();
  
  //-------------------------------------------------------------------------
  //! \brief Traitement des ordres claviers
  //! Si la touche est une touche de changement de mode, retourne le mode demandé
  //! Cette méthode est appelée régulièrement par le loop principal
  //! à charge des différents modes de traiter les actions opérateurs
  //! \param key Touch to treat
  virtual int KeyManager(
    unsigned short key
    );

protected:
  //! Indice du point de protocole courant
  uint8_t iPointProto;

  //! Nombre max de points du protocole
  uint8_t iMaxPoint;

  //! Indique si l'affichage doit être effectué
  bool bDoPrintProtocole;

  //! Etat du mode protocole
  int iEtatProto;
  
  //! Sauvegarde de l'état précédent
  int iOldEtatProto;

  //! Gestion du temps d'enregistrement
  unsigned long uiTimeRec;

  //! Milliseconde courante
  unsigned long iCurMs;

  //! Indique si on doit passer en mode enregistrement
  bool bToStartRecord;
};

//-------------------------------------------------------------------------
//! \class CModeProtocoleRoutier
//! \brief Classe de gestion du mode enregistrement protocole routier
class CModeProtocoleRoutier : public CModeProtocole
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructeur
  CModeProtocoleRoutier();

  //-------------------------------------------------------------------------
  //! \brief Début du mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  //! \brief Fin du mode
  virtual void EndMode();
  
  //-------------------------------------------------------------------------
  //! \brief Affichage du protocole en état stop
  virtual void PrintPotocolStop();

  //-------------------------------------------------------------------------
  //! \brief Affichage du protocole en état enregistrement
  virtual void PrintProtocolRecord();

  //-------------------------------------------------------------------------
  //! \brief Affichage du protocole en état Fin
  virtual void PrintProtocolEnd();

  //-------------------------------------------------------------------------
  //! \brief Affichage du protocole en état Pause
  virtual void PrintProtocolPause();
};

//-------------------------------------------------------------------------
//! \class CModeProtocolePedestre
//! \brief Classe de gestion du mode enregistrement protocole pédestre
class CModeProtocolePedestre : public CModeProtocole
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructeur
  CModeProtocolePedestre();

  //-------------------------------------------------------------------------
  //! \brief Début du mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  //! \brief Fin du mode
  virtual void EndMode();
  
  //-------------------------------------------------------------------------
  //! \brief Affichage du protocole en état stop
  virtual void PrintPotocolStop();

  //-------------------------------------------------------------------------
  //! \brief Affichage du protocole en état enregistrement
  virtual void PrintProtocolRecord();

  //-------------------------------------------------------------------------
  //! \brief Affichage du protocole en état Fin
  virtual void PrintProtocolEnd();
};

//-------------------------------------------------------------------------
//! \class CModeProtocolePFixe
//! \brief Classe de gestion du mode enregistrement protocole point fixe (mode enregistrement auto avec paramètres prédéfinies)
class CModeProtocolePFixe : public CModeAutoRecorder
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructeur
  CModeProtocolePFixe();

  //-------------------------------------------------------------------------
  //! \brief Début du mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  //! \brief Fin du mode
  virtual void EndMode();
};

//-------------------------------------------------------------------------
//! \class CModeSynchro
//! \brief Classe de gestion du mode enregistrement synchro
class CModeSynchro : public CModeAutoRecorder
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructeur
  CModeSynchro();

  //-------------------------------------------------------------------------
  //! \brief Début du mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  //! \brief Fin du mode
  virtual void EndMode();
  
  //-------------------------------------------------------------------------
  //! \brief Affichage du mode sur l'écran
  //! Cette méthode est appelée régulièrement par le loop principal
  //! à charge des différents modes d'afficher les informations nécessaires
  virtual void PrintMode();
  
  //-------------------------------------------------------------------------
  //! \brief Traitement des ordres claviers
  //! Si la touche est une touche de changement de mode, retourne le mode demandé
  //! Cette méthode est appelée régulièrement par le loop principal
  //! à charge des différents modes de traiter les actions opérateurs
  //! \param key Touch to treat
  virtual int KeyManager(
    unsigned short key
    );

  //-------------------------------------------------------------------------
  //! \brief Gestion des tâches de fonds
  virtual void OnLoop();

  //-------------------------------------------------------------------------
  //! \brief Traitement interruption Record
  void OnIsrRecord();
  
  //! Instance
  static CModeSynchro *pInstance;
  
protected:
  //! PR Type (Master or Slave n)
  int iPRType;

  //! Indique si l'affichage d'initialisation est terminé
  bool bAffInit;

  //! Indique si la configuration minimale est présente
  bool bConfMini;

  //! For timeout management
  unsigned long iTimeOut;

  //! For wainting reset mode
  unsigned long iTimeReset;

  //! For waiting record mode
  unsigned long iTimeRecord;

  //! ESP32 link manager
  CSynchroLink *pESP32Link;

  //! Save using LED
  int iUsingLed;

#if defined(__IMXRT1062__) // Teensy 4.1
  //! Master recording
  bool bMasterRecording;
#endif
};

#endif
