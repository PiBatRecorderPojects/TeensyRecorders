/* 
 * File:   CRecorder.cpp
   PassiveRecorder Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 /*
  * - CFIRFilter
  * - CGenericRecorder
  * -   CRecorder
  * -    CRecorderS
  * -    CRecorderH
  * -    CRecorderA
  * - CHeterodyne
  */
#include "arduino.h"
#include "Const.h"
#include "TimeLib.h"
#include "CModeGeneric.h"
#include "AcquisitionS.h"
#include "CRecorder.h"
#include "CModeParams.h"
#if defined(__MK66FX1M0__) // Teensy 3.6
  #include "RamMonitor.h"
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  #include <smalloc.h>
#endif
#include "FiltresFIR.h"

// Pour tester la détection des QFC
//#define TESTQFC

// Pour activer le test de la LED sur ITDMA
//#define TESTLEDDMA

// Taille du pré-trigger en fonction du type de processeur et de la Fe. Entre parantenthèses, nombre de buffers dans le cas ou le pré-trigger est limité volontairement à 1s
//            Teensy 3.6  Teensy 4.1  Tensy 4.1 + RAM 8GB
// nb buffers  10          26
// 500kHz      163ms       409ms (25)
// 384kHz      213ms       554ms
// 250kHz      327ms       851ms
// 192kHz      426ms      1109ms
//  96kHz      853ms      1194ms (14)
//  48kHz     1194ms (7)  1194ms (7)
//  24kHz     1024ms (3)  1024ms (3)

#if defined(__MK66FX1M0__) // Teensy 3.6
  // Teensy 3.6 Ram monitoring (https://github.com/veonik/Teensy-RAM-Monitor)
  extern RamMonitor ramMonitor;
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  /*
   * Détail de la RAM Teensy 4.1
   * ITCM (rwx):  ORIGIN = 0x00000000, LENGTH = 512K  FLASH (code + PROGMEM)
   * DTCM (rwx):  ORIGIN = 0x20000000, LENGTH = 512K  RAM1 classique
   * RAM (rwx):   ORIGIN = 0x20200000, LENGTH = 512K  RAM2 via new
   * FLASH (rwx): ORIGIN = 0x60000000, LENGTH = 1984K Emulated EEPROM
   * PSRAM (rwx): ORIGIN = ?, LENGTH = max 16384k QSPI Memory Expansion with extmem_malloc()
   * 
   * La mémoire est utilisée de la manière suivante (source https://www.electroseed.fr/shop/product_info.php?products_id=554) :
   * 
   * RAM1  La RAM1 est accessible par 2 bus 64 bits à très haute vitesse, ITCM pour exécuter le code et DTCM pour accéder aux données.
   * Pour des performances optimales, placez le code et les variables dans RAM1. La mise en cache n'est pas utilisée avec RAM1, car tous les emplacements
   * dans RAM1 sont accessibles à la même vitesse que les caches du processeur M7.
   * - Variables initialisées - Ce sont des variables globales et statiques qui ont des valeurs non nulles définies. 
   *   Leurs données sont copiées de la mémoire Flash vers la RAM1 au démarrage.
   * - Variables mises à zéro - Ce sont toutes les autres variables globales et statiques.
   * - Variables locales - Les variables locales, ainsi que les adresses de retour des appels de fonction et l'état enregistré des interruptions,
   *   sont placés sur une pile qui commence à partir du haut de RAM1 et croît vers le bas. La quantité d'espace pour les variables locales est la partie de RAM1 
   *   non utilisée par le code FASTRUN et les variables initialisées et mises à zéro.
   * - Code FASTRUN - Les fonctions définies avec "FASTRUN" sont attribuées au début de RAM1. Une copie est également stockée dans Flash et copiée sur RAM1
   *   au démarrage. Ces fonctions sont accessibles par le bus Cortex-M7 ITCM, pour des performances les plus rapides possibles. Par défaut, les fonctions
   *   sans type de mémoire défini sont traitées comme FASTRUN. Une petite quantité de mémoire est généralement inutilisée, car le bus ITCM doit accéder
   *   à une région de mémoire qui est un multiple de 32 Ko.
   * - Code PROGMEM - Fonctions définies avec "PROGMEM" exécutées directement à partir de Flash. Si le cache Cortex-M7 ne contient pas déjà une copie
   *   de la fonction, un retard se produit pendant que la mémoire Flash est lue dans le cache du M7. PROGMEM doit être utilisé sur le code de démarrage
   *   et d'autres fonctions où la vitesse n'est pas importante.
   *   
   * RAM2 La RAM2 est accessible par un bus AXI 64 bits qui fonctionne à 1/4 de la fréquence d'horloge du processeur. Bien que plus lent que ITCM et DTCM,
   * il est tout de même très rapide. Normalement, les tampons de données et les grands tableaux sont placés dans cette RAM2. Le cache de données ARM Cortex-M7
   * est utilisé avec cette mémoire. Lorsque des transferts DMA sont utilisés, les fonctions de gestion du cache de données doivent être utilisées pour vider ou 
   * supprimer les données mises en cache. Les DMA des périphériques peuvent accéder à la RAM2 sans rivaliser avec l'utilisation extrêmement large de la bande
   * passante de RAM1 par les bus ITCM et DTCM.
   * - Variables DMAMEM - Les variables définies avec "DMAMEM" ou avec "new" sont placées au début de RAM2. Normalement, des tampons et de grands tableaux
   *   sont placés ici. Ces variables ne peuvent pas être initialisées.
   * - new / malloc () - La mémoire allouée par C ++ "new" et C malloc () provient de RAM2, commençant immédiatement après les variables DMAMEM.
   * 
   * FLASH La mémoire Flash est accessible par FlexSPI, qui mappe l'intégralité du Flash dans la mémoire du M7. Tout emplacement dans la puce Flash
   * peut être lu comme s'il s'agissait d'une mémoire ordinaire. La mise en cache est utilisée, il y a donc souvent peu de perte de performances.
   * Mais un échec de cache nécessite de nombreux cycles d'horloge pour remplir une ligne de cache à partir de la puce Flash. Normalement,
   * seuls les grands tableaux, le code de démarrage et d'autres ressources qui ne sont pas critiques en termes de vitesse doivent être accessibles
   * à partir de la mémoire Flash. Les constantes de chaîne ne peuvent être placées que dans le flash en utilisant la syntaxe F ("chaîne").
   * - Variables PROGMEM - Les variables définies avec "PROGMEM" sont placées uniquement dans la mémoire flash. Elles sont accessibles normalement.
   * 
   * EEPROM
   * - Émulation EEPROM - 60 Ko de mémoire Flash sont réservés à l'émulation EEPROM. Normalement, la bibliothèque Arduino EEPROM est utilisée,
   *   mais les fonctions AVR eeprom sont également prises en charge pour la compatibilité avec le code hérité. Pendant le téléchargement,
   *   cette partie de la mémoire Flash n'est ni effacée ni modifiée, donc vos données enregistrées dans cette zone seront conservées.
   *
   * RAZ Teensy 4.1
   * Programme de restauration - Lorsque vous appuyez sur le bouton-poussoir Teensy 4 pendant 15 secondes (un flash rapide sur la LED rouge vous indiquera
   * le moment de relâcher), tout le flash, sauf 4K, est effacé, et un programme de clignotement LED est copié au début de la mémoire Flash.
   * Ces 4K sont une mémoire spéciale accessible uniquement en lecture seule, vous pouvez donc toujours utiliser cette pression de 15 secondes
   * pour effacer complètement votre Teensy 4 et le restaurer avec un programme de clignotement de référence.
   * 
   * -----------------------------------------------------------------------------------------------
   * Document sur la mémoire Teensy 4.1 : https://github.com/TeensyUser/doc/wiki/Memory-Mapping
   * 
   * RAM2 a un cache de 32 Ko. Au démarrage, le cache est configuré "Write Back". Cela signifie que si vous stockez une valeur dans la RAM2, 
   * elle peut ne pas être écrite immédiatement dans la RAM2 et peut être conservée dans le cache à la place. DMA ne peut accéder qu'à la RAM
   * et utilisera toujours les valeurs de la RAM. A cet effet, il existe trois fonctions :
   *
   * arm_dcache_flush(void *addr, uint32_t size) - Vide les données du cache vers la mémoire - Normalement, arm_dcache_flush() est utilisé lorsque
   * les métadonnées écrites en mémoire seront utilisées par un DMA ou un périphérique bus-master. Toutes les données du cache sont écrites dans la mémoire.
   * Une copie reste dans le cache, elle est donc généralement utilisée avec des champs spéciaux auxquels vous voudrez accéder rapidement à l'avenir.
   * Pour la transmission de données, utilisez arm_dcache_flush_delete().
   *
   * arm_dcache_delete(void *addr, uint32_t size) - Supprimer les données du cache, sans toucher à la mémoire - Normalement, arm_dcache_delete()
   * est utilisé avant de recevoir des données via DMA ou depuis des périphériques bus-master qui écrivent dans la mémoire. Vous souhaitez supprimer
   * tout ce que le cache peut avoir stocké, de sorte que votre prochaine lecture est certaine d'accéder à la mémoire physique.
   *
   * arm_dcache_flush_delete(void *addr, uint32_t size) - Vide les données du cache vers la mémoire et les supprime du cache. Normalement,
   * arm_dcache_flush_delete() est utilisé lors de la transmission de données via DMA ou des périphériques maîtres de bus qui lisent depuis
   * la mémoire. Vous souhaitez que toutes les données mises en cache soient écrites dans la mémoire, puis supprimées du cache, car vous n'avez
   * plus besoin d'accéder aux données après la transmission.
   * 
   */
  // Buffer pour copier le buffer DMA avant traitement
  DMAMEM __attribute__((aligned(32))) uint16_t samplesMem[MAXSAMPLE];

  // Pour le test de perte d'info DMA
  //#define TESTDMAT41
  #ifdef TESTDMAT41
    int iNbErrorDMAT41 = 0;
  #endif
  
  // Taille de l'éventuelle RAM supplémentaire
  extern "C" uint8_t external_psram_size;

  // Sur Teensy 4.1, définition des buffers DMA en RAM2, donc forcément static
  DMAMEM __attribute__((aligned(32))) uint16_t CGenericRecorder::samplesDMA[MAXSAMPLE*2];
  DMAMEM __attribute__((aligned(32))) uint16_t CRecorderH::samplesDAC[MAXSAMPLE_HET*4];
#endif

// Déclaration des tables de transformation des niveaux en dB
extern const short NivToDbA[];
extern const short NivToDbB[];

// Serial nuber string with recorder name
extern char sSerialName[MAXNAMERECORDER];
extern const char *txtSDFATTO[];
extern const char *txtSDFATBEGIN[];
extern const char *txtLogBufferLossR[];
extern const char *txtLogBufferLossA[];

// Gestionnaire de carte SD
extern SdFs sd;

// Pour le debug du calcul du bruit
//#define TEST_BRUIT

// Test de la période de chaque interruption
//#define TESTPERIODISR
#ifdef TESTPERIODISR
  // A 384kHz, 8192 ech = 21.33ms
  unsigned long uiTimeISRDMA = 0;
#endif

// Test du temps moyen de traitement des interruptions DMA (8192 échantillons)
// Il faut compter en plus au moins 2ms pour sauver les échantillons sur la carte SD et divers calculs en tâche de fond
// Et 1ms en plus sur un AR pour le son hétérodyne
// Il faut donc que le temps de traitement soit inférieur de 3ms à la période de récurrence des interruptions pour ne pas engorger le processus
//                      ----------------- Teensy 3.6 ----------------------    ------------------- Teensy 4.1 ------------------------
// FE     Temps buffer  PR144MHz  PR144MHz   PR180MHz  PR180MHz   AR180MHz     PR600MHz  PR600MHz PR396MHz  PR396MHz PR150MHz  PR150MHz
//        (période IT)  Sans Flt  Flt Lin    Sans Flt  Flt Lin    Hétérodyne   Sans Flt  Flt Lin  Sans Flt  Flt Lin  Sans Flt  Flt Lin
// 500kHz  16.38ms       9.051ms  Impossible  7.282ms  Impossible 13.350ms     1.242ms            1.869ms            4.865ms
// 384kHz  21.33ms       9.275ms  14.235ms    7.461ms  13.672ms   13.498ms     1.273ms   2.563ms  1.917ms   3.870ms  4.987ms   10.111ms
// 250kHz  32.17ms       9.754ms  17.483ms    7.845ms  14.052ms   13.878ms     1.344ms   2.635ms  2.024ms   3.978ms  5.270ms   10.394ms
// 48kHz   34.13ms      11.957ms  Fe réelle 240kHz avec filtrage et décimation 1.969ms            2.972ms            7.762ms
//#define TESTTIMEISR
#ifdef TESTTIMEISR
  unsigned long uiTimeISRDMA;
  unsigned int  uiNbTimeISRDMA;
#endif

// Pour le test de durée d'écriture de 8192 échantillons sur la carte SD
//#define TESTWRITEFILE

// Pour le test des échantillons min et max
//#define TESTMINMAX
#ifdef TESTMINMAX
  int16_t iEchMin, iEchMax;
  int iNbBuffEch;
#endif

// Pour des mesures de temps d'acquisition
//#define TESTTEMPSREEL
#ifdef TESTTEMPSREEL
unsigned long uiTimeA, uiNbTraite, uiTimeApp, uiTimeTot, uiTimeLoop, uiNbLoop, uiMaxLoop;
volatile uint16_t *pBA, *pBB;
#endif

float fAlphaLPFlt;
float fPrevLPFlt;
// Calcul du coefficient Alpha
// float Alpha = (2 * PI * (1/FeHz) * CutOffFrequencyHz) / (2 * PI * CutOffFrequencyHz * (1/FeHz) + 1)
void CaclulAlpha()
{
  fAlphaLPFlt = (DEUXPI * (1/240000.0) * 20000.0) / ((DEUXPI * 20000.0 * (1/240000.0)) + 1);
  fPrevLPFlt = 0;
  //Serial.printf("CaclulAlpha %f\n", fAlphaLPFlt);
}
// Algorithme
// FilterSample = PreviousFilteredSample  + Alpha x (Sample - PreviousFilteredSample)
int16_t LowPassFilter( int16_t Sample)
{
  float fFiltreSample = fPrevLPFlt + (fAlphaLPFlt * ((float)Sample - fPrevLPFlt));
  fPrevLPFlt = fFiltreSample;
  //Serial.printf("LowPassFilter s %d, sf %d\n", Sample, (int16_t)fFiltreSample);
  return (int16_t)fFiltreSample;
}

//-------------------------------------------------------------------------
//! \class CFIRFilter
//! \brief Classe qui implémente le calcul d'un filtre FIR via le DSP
//-------------------------------------------------------------------------
//! \brief Constructeur (initialisation de la classe)
CFIRFilter::CFIRFilter()
{
}

//-------------------------------------------------------------------------
//! \brief Initialisation des coefficients du filtre
//! \param iNTaps Nombre de coefficients du filtre (max FIR_MAX_COEFFS)
//! \param pfCoef Tableau des n coefficients du filtre
//! \return Retourne true si initialisation OK
bool CFIRFilter::InitFIRFilter(
  int iNTaps,
  const double *pfCoef
  )
{
  //Serial.printf("CFIRFilter::InitFIRFilter iNTaps %d\n", iNTaps);
  bool bOK = true;
  // Recherche du max en valeur absolue
  double fMax = -33000;
  for (int i=0; i<iNTaps; i++)
  {
    if (fabs(pfCoef[i]) > fMax)
      fMax = pfCoef[i];
  }
  // Calcul du coefficient de transformation en entier
  // pour ne pas dépasser le domaine 32767 et garder un max de précision
  double coef = 32700.0 / fMax;
  // Calcul des coefficients en format q15_t (int16_t)
  for (int i=0; i<iNTaps; i++)
  {
    double newCoef = pfCoef[i] * coef;
    lstCoeff[i] = (q15_t) newCoef;
    //Serial.printf("Tap %d, %f, %d\n", i, pfCoef[i], (int16_t)lstCoeff[i]);
  }
  // Initialise l'instance FIR (ARM DSP Math Library)
  if (arm_fir_init_q15(&fir_inst, iNTaps, (q15_t *)lstCoeff, (q15_t *)StateQ15, BLOCK_SAMPLES) != ARM_MATH_SUCCESS)
  {
    // Erreur d'init du filtre
    Serial.println("Erreur arm_fir_init_q15 !!!");
    bOK = false;
  }
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief Initialisation des coefficients du filtre
//! \param iNTaps Nombre de coefficients du filtre (max FIR_MAX_COEFFS)
//! \param pfCoef Tableau des 32 coefficients du filtre
//! \return Retourne true si initialisation OK
bool CFIRFilter::InitFIRFilter(
  int iNTaps,
  const int16_t *piCoef
  )
{
  bool bOK = true;
  for (int i=0; i<iNTaps; i++)
    lstCoeff[i] = (q15_t) piCoef[i];
  // Initialise l'instance FIR (ARM DSP Math Library)
  if (arm_fir_init_q15(&fir_inst, iNTaps, (q15_t *)lstCoeff, (q15_t *)StateQ15, BLOCK_SAMPLES) != ARM_MATH_SUCCESS)
  {
    // Erreur d'init du filtre
    //Serial.println("Erreur arm_fir_init_q15 !!!");
    bOK = false;
  }
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief Applique le filtre sur un tableau d'échantillons
//! \param pFirIn Tableau des échantillons en entrée du filtre (taille BLOCK_SAMPLES)
//! \param pFirOut Tableau des échantillons en sortie du filtre (taille BLOCK_SAMPLES)
void CFIRFilter::SetFIRFilter(
  q15_t *pFirIn,
  q15_t *pFirOut
  )
{
  // Application du filtre par le DSP
  arm_fir_fast_q15(&fir_inst, pFirIn, pFirOut, BLOCK_SAMPLES);
}

//-------------------------------------------------------------------------
//! \class CGenericRecorder
//! \brief Classe de base des différents enregistreurs
//! - Définie les méthodes de base virtuelles mais ne fait rien

//-------------------------------------------------------------------------
//! \brief Constructeur (initialisation de la classe)
//! \param pParams Pointeur sur les paramètres opérateur
//! \param fftLen Taille de la FFT
CGenericRecorder::CGenericRecorder(
  ParamsOperator *pParams,
  int fftLen
  )
{
  CaclulAlpha();
  // Initialisation des variables
  pInstance      = this;
  pParamsOp      = pParams;
  iFFTLen        = fftLen;
  bStarting      = false;
  bRecording     = false;
  bErrDMA        = false;
  sErrDMA[0]     = 0;
  iUtilLED       = NOLED;
  idxFMin        = 0;
  idxFMax        = iFFTLen / 2;
  iNbMinEch      = 0;
  iNbMaxEch      = 0;
  iSeuilMag      = 0;
  iTotalBruitMag = 0;
  iNbMagNoise    = 0;
  iMagMinNoise   = 0;
  idBMinNoise    = 0;
  iMagMaxNoise   = 0;
  idBMaxNoise    = 0;
  iNbPics        = 0;
  fFreqMaxPic    = 0.0;
  iNbFFTNoise    = 0;
  iNbFFTNoiseG   = 0;
  iBruitMag      = 0;
  iSeuildB       = 0;
  iBruitdB       = 0;
  bNoiseOK       = false;
  uiFeWav        = 384000;
  uiFeAcq        = 384000;
  fCanal         = 0.0;
  shift          = 0;
  iSample        = 0;
  pSaveEch       = NULL; 
  bTestSilence   = false;
  bTestSignal    = false;
  iNbWavefileError = 0;
  bStereo        = false;
  bRight         = false;
  bFFTSynchro    = true;
  bModeDebug     = false;
  if (CModeGeneric::GetCommonsParams()->iUserLevel > ULEXPERT)
    bModeDebug   = true;
  iBufferProcessMode = BPM_MONONOFLT;
  //uint32_t iTailleMax = sizeof(uint32_t) * iFFTLen/2 + sizeof(uint32_t) * iFFTLen/2 + sizeof(int32_t) * iFFTLen;
  //Serial.printf("CGenericRecorder::CGenericRecorder iFFTLen %d, taille uint32_t %d, taille mémoire %d\n", iFFTLen, sizeof(uint32_t), iTailleMax);
  // Création de buffers en fonction de la taille de la FFT
  iTbNoise     = new volatile short[iFFTLen/2];
  iTbMagCnx    = new volatile uint32_t[iFFTLen/2];
  iTbSeuilCnx  = new volatile uint32_t[iFFTLen/2];
  bufferCpxFFT = new volatile int32_t[iFFTLen];
  if (CModeGeneric::GetRecorderType() >= PASSIVE_STEREO and pParamsOp->iStereoRecMode == SMSTEREO and pParamsOp->iModeRecord == PTSTMICRO)
    iTbMagCnxG = new volatile uint32_t[iFFTLen/2];
  else
    iTbMagCnxG = NULL;

  memset( (void *)iTbMagCnx    , 0, sizeof(uint32_t) * iFFTLen/2);
  memset( (void *)iTbSeuilCnx  , 0, sizeof(uint32_t) * iFFTLen/2);
  memset( (void *)bufferCpxFFT , 0, sizeof( int32_t) * iFFTLen);
  memset( (void *)samplesDMA   , 0, sizeof(uint16_t) * MAXSAMPLE * 2);
  memset( (void *)&cpx_fft_inst, 0, sizeof(arm_cfft_radix4_instance_q15));
  // Init de l'utilisation de la LED
  iUtilLED = CModeGeneric::GetCommonsParams()->iUtilLed;
  if (iUtilLED == LEDNOISE)
    digitalWriteFast(LED_BUILTIN, HIGH);
  if (pParams->iModeRecord == SYNCHRO)
    iUtilLED = NOLED;
  if (pParamsOp->iModeRecord != SYNCHRO)
    CalculPreAcquisition();
}

//-------------------------------------------------------------------------
//! \brief Destructeur
CGenericRecorder::~CGenericRecorder()
{
  //Serial.printf("Destructeur CGenericRecorder\n");
  StopPreamPower();
  // Destruction des buffers FFT
  if (iTbMagCnxG != NULL)
    delete [] iTbMagCnxG;
  delete [] bufferCpxFFT;
  delete [] iTbSeuilCnx;
  delete [] iTbMagCnx;
  delete [] iTbNoise;
  pInstance = NULL;
  //Serial.printf("Destructeur CGenericRecorder OK\n");
}

//-------------------------------------------------------------------------
//! \brief Calcul pré-démarrage acquisition
void CGenericRecorder::CalculPreAcquisition()
{
  // Init du gain numérique d'enregistrement
  shift = (int8_t)pParamsOp->uiGainNum;
  // Init de la fréquence d'échantillonnage
  if (pParamsOp->iModeRecord == PHETER and pParamsOp->uiFe < FE250KHZ)
  {
    if (F_CPU < 179000000 and pParamsOp->uiFe >= FE384KHZ)
      pParamsOp->uiFe = FE250KHZ;
    else
      pParamsOp->uiFe = FE384KHZ;
  }
  if (pParamsOp->iModeRecord == AUDIOREC)
  {
    uiFeWav = 48000;
    switch (pParamsOp->uiFeA)
    {
    case FE24KHZ : uiFeWav =  24000; break;
    case FE48KHZ : uiFeWav =  48000; break;
    case FE96KHZ : uiFeWav =  96000; break;
    default:
    case FE192KHZ: uiFeWav = 192000; break;
    }
  }
  else
  {
    uiFeWav = 384000;
    switch (pParamsOp->uiFe)
    {
    case FE24KHZ : uiFeWav =  24000; break;
    case FE48KHZ : uiFeWav =  48000; break;
    case FE96KHZ : uiFeWav =  96000; break;
    case FE192KHZ: uiFeWav = 192000; break;
    case FE250KHZ: uiFeWav = 250000; break;
    case FE384KHZ: uiFeWav = 384000; break;
    case FE500KHZ: uiFeWav = 500000; break;
    }
    if (F_CPU < 144000000 and uiFeWav > 250000)
      uiFeWav = 250000;
  }
  if (uiFeWav < 192000)
  {
    // On force le filtre
    pParamsOp->bFiltre = true;
    // Acquisition à 240kHz et décimation
    uiFeAcq = 240000;
    memset( iDecimationTable, 0, MAXTBDECIM);
    // Index          0 1 2 3 4 5 6 7 8 9
    // Decimation  10 1 0 0 0 0 0 0 0 0 0 24kHz
    // Decimation   5 1 0 0 0 0 1 0 0 0 0 48kHz
    // Decimation 2.5 1 0 1 0 0 1 0 1 0 0 96kHz
    switch( pParamsOp->uiFe)
    {
    case FE24KHZ :
      iDecimationTable[0] = 1;
      break;
    case FE48KHZ :
      iDecimationTable[0] = 1;
      iDecimationTable[5] = 1;
      break;
    case FE96KHZ :
      iDecimationTable[0] = 1;
      iDecimationTable[2] = 1;
      iDecimationTable[5] = 1;
      iDecimationTable[7] = 1;
      break;
    }
  }
  else
    // Pas de décimation
    uiFeAcq = uiFeWav;
  // Largeur d'un canal en Hz
  fCanal = (float)(uiFeWav / 2) / (float)(iFFTLen / 2);
  // Calcul des indices des canaux des FreqMin et Max d'intéret
  if (pParamsOp->iModeRecord == AUDIOREC or pParamsOp->uiFe < FE192KHZ)
    idxFMin = pParamsOp->uiFreqMinA / (int)fCanal;
  else
    idxFMin = pParamsOp->uiFreqMin / (int)fCanal;
  if (idxFMin < 0)
    idxFMin = 0;
  if (pParamsOp->iModeRecord == AUDIOREC or pParamsOp->uiFe < FE192KHZ)
    idxFMax = pParamsOp->uiFreqMaxA / (int)fCanal;
  else
    idxFMax = pParamsOp->uiFreqMax / (int)fCanal;
  if (idxFMax > (iFFTLen / 2)-1)
    idxFMax = (iFFTLen / 2) - 1;
  if (idxFMin > idxFMax)
    idxFMin = idxFMax - 1;
  //Serial.printf("CGenericRecorder::CalculPreAcquisition FMin %dHz cnx %d, FMax %dHz cnx %d, fCanal %fHz, Fe %dHz\n", pParamsOp->uiFreqMin, idxFMin, pParamsOp->uiFreqMax, idxFMax, fCanal, uiFeWav);
  // Calcul du nombre d'échantillons pour les durées min et max
  if (pParamsOp->uiDurMin > pParamsOp->uiDurMax)
    // La durée max ne peut pas être inférieure à la durée min
    pParamsOp->uiDurMax = pParamsOp->uiDurMin;
  double fDurEch = 1.0 / (double)(uiFeWav / 1000);    // Calcul de la durée d'un échantillon en ms
  iNbEchPreTrig = 0;  // Si durée du Pré-trigger à 0, le nombre d'échantillons du pré-trigger est à 0
  if (pParamsOp->iPreTrigDurHeter > 0 and pParamsOp->iModeRecord == PHETER)
  {
    // Les durées min et max ne peuvent pas être inférieures à la durée du pré-trigger
    if (pParamsOp->uiDurMin < pParamsOp->iPreTrigDurHeter)
      pParamsOp->uiDurMin = pParamsOp->iPreTrigDurHeter;
    if (pParamsOp->uiDurMax < pParamsOp->iPreTrigDurHeter)
      pParamsOp->uiDurMax = pParamsOp->iPreTrigDurHeter;
    iNbEchPreTrig = (int)((double)(pParamsOp->iPreTrigDurHeter * 1000) / fDurEch);
  }
  else if (pParamsOp->iPreTrigDurAuto > 0 and (pParamsOp->iModeRecord == RECAUTO
      or pParamsOp->iModeRecord == AUDIOREC or pParamsOp->iModeRecord == SYNCHRO))
  {
    // Les durées min et max ne peuvent pas être inférieures à la durée du pré-trigger
    if (pParamsOp->uiDurMin < pParamsOp->iPreTrigDurAuto)
      pParamsOp->uiDurMin = pParamsOp->iPreTrigDurAuto;
    if (pParamsOp->uiDurMax < pParamsOp->iPreTrigDurAuto)
      pParamsOp->uiDurMax = pParamsOp->iPreTrigDurAuto;
    iNbEchPreTrig = (int)((double)(pParamsOp->iPreTrigDurAuto * 1000) / fDurEch);
  }
  double fDur = (double)(pParamsOp->uiDurMin * 1000) / fDurEch;
  iNbMinEch = (uint32_t)fDur;
  fDur = 1000.0 / fDurEch;
  uint32_t uiOneSec = (uint32_t)fDur;
  // Si, le temps du pré-trigger est supérieur à la durée min moins 1s
  if (iNbEchPreTrig > 0 and iNbEchPreTrig > (iNbMinEch - uiOneSec))
    // On ajoute une seconde à la durée min, sinon, on risque d'avoir des fichiers de la durée du pré-trigger seulement
    // Le signal déclenchant l'enregistrement arrive forcément après le pré-trigger et il faudrait un signal immédiatement après pour continuer le fichier
    iNbMinEch += uiOneSec;
  fDur = (double)(pParamsOp->uiDurMax * 1000) / fDurEch;
  if (pParamsOp->iModeRecord == SYNCHRO and pParamsOp->iMasterSlave != MASTER)
    // En mode synchro sur les esclaves, la durée max est seulement un garde fou au cas ou le message de fin d'enregistrement n'arrive pas
    // On ajoute 3% pour stopper en 1er sur réception du message
    fDur += fDur * 0.03;
  iNbMaxEch = (uint32_t)fDur;
  // Si, le nombre d'échantillons de la durée min est plus grand que la durée max
  if (iNbMinEch > iNbMaxEch)
    // La durée min est au maximum égale à la durée max moins 1 échantillon
    iNbMinEch = iNbMaxEch - 1;

  //Serial.printf("fDurEch %fms, uiDurMin %ds, iNbMinEch %d, uiDurMax %ds, iNbMaxEch %d\n", fDurEch, pParamsOp->uiDurMin, iNbMinEch, pParamsOp->uiDurMax, iNbMaxEch);
  // Préparation du calcul du bruit moyen
  iTotalBruitMag = 0;
  iBruitMag = 0;
  iBruitdB = 0;
  iNbMagNoise  = 0;
  iNbFFTNoise  = 0;
  iNbFFTNoiseG = 0;
  iMagMinNoise = 500000;
  iMagMaxNoise = 0;
  iNbPics = 0;
  fFreqMaxPic = 0.0;
  bNoiseOK = false;
  iFIR = 0;
  iFFT = 0;
  iSampleOut = 0;
  iDecim = 0;
  if (uiFeAcq != uiFeWav)
    iBufferProcessMode = BPM_NOISEFLTDECIM;   // Noise treatment with filter and decimation
  else if (pParamsOp->bFiltre)
    iBufferProcessMode = BPM_NOISEFLT;        // Noise treatment with filter
  else
    iBufferProcessMode = BPM_NOISENOFLT;      // Noise processing without filter
  /*switch(iBufferProcessMode)
  {
  case BPM_NOISEFLTDECIM: Serial.println("CGenericRecorder::CalculPreAcquisition goto BPM_NOISEFLTDECIM"); break;
  case BPM_NOISEFLT     : Serial.println("CGenericRecorder::CalculPreAcquisition goto BPM_NOISEFLT");      break;
  case BPM_NOISENOFLT   : Serial.println("CGenericRecorder::CalculPreAcquisition goto BPM_NOISENOFLT");    break;
  default               : Serial.println("CGenericRecorder::CalculPreAcquisition goto unknow process !");  break;
  }
  for (int i=0; i<MAXTBDECIM; i++)
    Serial.printf("iDecimationTable[%d]=%d, ", i, iDecimationTable[i]);
  Serial.println(" ");
  Serial.printf("iFIR %d, iFFT %d, iSampleOut %d\n", iFIR, iFFT, iSampleOut);*/
  // Switching on the power supply for the microphone preamp
  StartPreampPower();
}
  
//-------------------------------------------------------------------------
//! \brief Starts the power supply of the preamp
void CGenericRecorder::StartPreampPower()
{
  // Switching of the preamp power supply, loop for slowly loading the 470µF capacity on the preamp so as not to disturb the processor and especially access to the SD card
  // Démarrage avec 30µs ON et 2ms OFF, 30 boucles avec incrément 5µs ON et -50µs OFF. Fin, 175µs ON et 550µs OFF, temps de monté  91,328ms
  // Démarrage avec  2µs ON et 2ms OFF, 90 boucles avec incrément 2µs ON et -20µs OFF. Fin, 180µs ON et 220µs OFF, temps de monté 158,098ms
  //uint32_t uiTime = micros();
  //uint32_t iDelayOn  = 30;
  //uint32_t iDelayOff = 2000;
  //for (int i=0; i<30; i++)
  /*uint32_t iDelayOn  = 2;
  uint32_t iDelayOff = 2000;
  for (int i=0; i<180; i++)
  {
    //Serial.printf("iDelayOn %d, iDelayOff %d\n", iDelayOn, iDelayOff);
    digitalWriteFast( PIN_START_PREAMP, LOW);
    delayMicroseconds(iDelayOn);
    digitalWriteFast( PIN_START_PREAMP, HIGH);
    delayMicroseconds(iDelayOff);
    //iDelayOn+=5;
    //iDelayOff-=50;
    iDelayOn+=1;
    iDelayOff-=10;
  }
  digitalWriteFast( PIN_START_PREAMP, LOW);
  delay(50);*/
  uint32_t iDelayOn    = 1;
  uint32_t iDelayOff   = 2000;
  uint32_t iDelayPlus  = 1;
  uint32_t iDelayMinus = 5;
  int      iNb         = 200;  
  //uint32_t timeA = micros();
  //Serial.printf("CGenericRecorder::StartPreampPower Start %d, %d, Nb %d, Plus %d, Minus %d", iDelayOn, iDelayOff, iNb, iDelayPlus, iDelayMinus);
  for (int i=0; i<iNb; i++)
  {
    //Serial.printf("iDelayOn %d, iDelayOff %d\n", iDelayOn, iDelayOff);
    digitalWriteFast( PIN_START_PREAMP, LOW);
    delayMicroseconds(iDelayOn);
    digitalWriteFast( PIN_START_PREAMP, HIGH);
    delayMicroseconds(iDelayOff);
    iDelayOn  += iDelayPlus;
    iDelayOff -= iDelayMinus;
  }
  digitalWriteFast( PIN_START_PREAMP, LOW);
  //Serial.printf(", Stop %d, %d, Time %dµs\n", iDelayOn, iDelayOff, micros()-timeA);
  //Serial.printf("&&& B CGenericRecorder::StartPreampPower durée %dµs\n", micros()-uiTime);
  //delay(4000);
  // Warning, possible SD error following consumption of the pre-amp (freeClusterCount = 0!)
  if (sd.vol()->freeClusterCount() == 0)
  {
    // Trace of the problem
    Serial.printf("SD loss after preamp switch on, new sd.begin");
    if (!CModeGeneric::isSDInit())
      iNbWavefileError = 5;
  }
}

//-------------------------------------------------------------------------
//! \brief starts the power supply of the preamp
void CGenericRecorder::StopPreamPower()
{
  // Stoppe l'alimentation du pré-ampli micro
  digitalWriteFast( PIN_START_PREAMP, HIGH);
}

//-------------------------------------------------------------------------
//! \brief Démarre l'acquisition
//! Retourne true si démarrage OK
bool CGenericRecorder::StartAcquisition()
{
  // Basic, do nothing
  bErrDMA = false;
  return true;
}

//-------------------------------------------------------------------------
//! \brief Stoppe l'acquisition
//! Retourne true si arrêt OK
bool CGenericRecorder::StopAcquisition()
{
  // Basic, do nothing
  return false;
}

//-------------------------------------------------------------------------
//! \brief Démarre l'enregistrement en auto
//! \param bAuto true pour lancer l'enregistrement automatique sur détection d'énergie
void CGenericRecorder::SetAutoRecord(
  bool bAuto
  )
{
  // Basic, do nothing
}
  
//-------------------------------------------------------------------------
//! \brief Démarre le mode enregistrement
//! Retourne true si démarrage OK
bool CGenericRecorder::StartRecording()
{
  // Basic, do nothing
  return false;
}

//-------------------------------------------------------------------------
//! \brief Stoppe le mode enregistrement
//! Retourne true si arrêt OK
bool CGenericRecorder::StopRecording()
{
  // Basic, do nothing
  return false;
}
  
//-------------------------------------------------------------------------
//! \brief Stoppe ou relance l'acquisition
//! \param bAcq true pour lancer l'acquisition, false pour la suspendre
void CGenericRecorder::SetAcquisition(
  bool bAcq
  )
{
  // Basic, do nothing
}

//-------------------------------------------------------------------------
//! \brief Gestion de l'IT DMA lorsqu'un buffer d'acquisition est plein
void CGenericRecorder::OnDMA_ISR()
{
  // Basic, do nothing
  //Serial.println("CGenericRecorder::OnDMA_ISR");
}

//-------------------------------------------------------------------------
//! \brief Gestion de l'IT DMA de lecture
void CGenericRecorder::OnITDMA_Reader()
{
  // Basic, do nothing
  //Serial.println("CGenericRecorder::OnITDMA_Reader");
}

//-------------------------------------------------------------------------
//! \brief Processing under interruption of a full acquisition buffer
void CGenericRecorder::BufferProcessing()
{
  // Basic, do nothing
  //Serial.println("CGenericRecorder::BufferProcessing");
}

//-------------------------------------------------------------------------
//! \brief Traitement hétérodyne de chaque échantillon (de base, ne fait rien)
//! \param sample Echantillon à traiter
void CGenericRecorder::Traitementheterodyne(
  int16_t sample)
{
  // Basic, do nothing
}

//-------------------------------------------------------------------------
//! \brief Traitement détections
void CGenericRecorder::TraitementDetections()
{
  // Basic, do nothing
}

//#define TESTTIMETESTMICRO
#ifdef TESTTIMETESTMICRO
uint32_t uiTime;
int iGlobalSample;
#endif
//----------------------------------------------------
//! \brief Lance l'aquisition en mode mesure du silence (test micro)
void CGenericRecorder::StartSilence()
{
#ifdef TESTTIMETESTMICRO
  iGlobalSample = 0;
  uiTime = micros();
  Serial.printf("StartSilence %d\n", uiTime);
#endif
  // RAZ du tableau des magnitudes
  memset( (void *)iTbMagCnx, 0, sizeof(uint32_t) * iFFTLen/2);
  if (iTbMagCnxG != NULL)
    memset( (void *)iTbMagCnxG, 0, sizeof(uint32_t) * iFFTLen/2);
  // On passe en mode intégration des niveaux en mode test micro silence
  iNbFFTNoise  = 0;
  iNbFFTNoiseG = 0;
  bTestSilence = true;
  iBufferProcessMode = BPM_SILENCE;
}

//----------------------------------------------------
//! \brief Lance l'aquisition en mode mesure du signal (test micro)
void CGenericRecorder::StartSignal()
{
#ifdef TESTTIMETESTMICRO
  iGlobalSample = 0;
  uiTime = micros();
  Serial.printf("StartSignal %d\n", uiTime);
#endif
  // RAZ du tableau des magnitudes
  memset( (void *)iTbMagCnx, 0, sizeof(uint32_t) * iFFTLen/2);
  if (iTbMagCnxG != NULL)
    memset( (void *)iTbMagCnxG, 0, sizeof(uint32_t) * iFFTLen/2);
  // On passe en mode intégration des niveaux en mode test micro signal
  iNbFFTNoise  = 0;
  iNbFFTNoiseG = 0;
  bTestSignal = true;
  iBufferProcessMode = BPM_SIGNAL;
}

//-------------------------------------------------------------------------
//! \brief Traitement du test micro en mode silence
void CGenericRecorder::TraitementSilence()
{
  if (bStereo and !bRight)
  {
    if (iNbFFTNoiseG < MAXFFTSILENCE)
    {
      // Mode stéréo et canal gauche
      for( int j=idxFMin; j<=idxFMax; j++)
      {
        int16_t iNiv = (int16_t)bufferCpxFFT[j];
        iTbMagCnxG[j]+= abs(iNiv);
      }
      iNbFFTNoiseG++;
#ifdef TESTTIMETESTMICRO
      if (iNbFFTNoiseG >= MAXFFTSILENCE)
        Serial.printf("Fin de l'acquisition en mode test micro silence canal gauche en %d µs, iGlobalSample %d\n", micros()-uiTime, iGlobalSample);
#endif
    }
    bRight = true;
  }
  else
  {
    if (iNbFFTNoise < MAXFFTSILENCE)
    {
      // Mode mono ou traitement canal droit
      for( int j=idxFMin; j<=idxFMax; j++)
      {
        int16_t iNiv = (int16_t)bufferCpxFFT[j];
        iTbMagCnx[j]+= abs(iNiv);
      }
      iNbFFTNoise++;
#ifdef TESTTIMETESTMICRO
      if (iNbFFTNoise >= MAXFFTSILENCE)
        Serial.printf("Fin de l'acquisition en mode test micro silence canal droit en %d µs, iGlobalSample %d\n", micros()-uiTime, iGlobalSample);
#endif
    }
    bRight = false;
  }
  if ((!bStereo and iNbFFTNoise >= MAXFFTSILENCE) or (bStereo and iNbFFTNoise >= MAXFFTSILENCE and iNbFFTNoiseG >= MAXFFTSILENCE))
  {
    // Fin de l'acquisition en mode test micro silence
    bTestSilence = false;
    if (uiFeAcq != uiFeWav)
    {
      if (bStereo)
        iBufferProcessMode = BPM_STEREODECIM;  // Stereo processing with filter and decimation
      else
        iBufferProcessMode = BPM_MONOFLTDECIM; // Mono treatment with filter and decimation
    }
    else if (bStereo)
      iBufferProcessMode = BPM_STEREO;         // Stereo processing without filter
    else if (pParamsOp->bFiltre)
      iBufferProcessMode = BPM_MONOFLT;        // Mono treatment with filter
    else
      iBufferProcessMode = BPM_MONONOFLT;      // Mono treatment without filter
  }
}

//-------------------------------------------------------------------------
//! \brief Traitement du test micro en mode signal
void CGenericRecorder::TraitementSignal()
{
  if (bStereo and !bRight)
  {
    if (iNbFFTNoiseG < MAXFFTSIGNAL)
    {
      // Mode stéréo et canal gauche
      for( int j=idxFMin; j<=idxFMax; j++)
      {
        int16_t iNiv = (int16_t)bufferCpxFFT[j];
        iTbMagCnxG[j]+= abs(iNiv);
      }
      iNbFFTNoiseG++;
#ifdef TESTTIMETESTMICRO
      if (iNbFFTNoiseG >= MAXFFTSIGNAL)
        Serial.printf("Fin de l'acquisition en mode test micro signal canal gauche en %d %µs, iGlobalSample %d\n", micros()-uiTime, iGlobalSample);
#endif
    }
    bRight = true;
  }
  else
  {
    if (iNbFFTNoise < MAXFFTSIGNAL)
    {
      // Mode mono ou traitement canal droit
      for( int j=idxFMin; j<=idxFMax; j++)
      {
        int16_t iNiv = (int16_t)bufferCpxFFT[j];
        iTbMagCnx[j]+= abs(iNiv);
      }
      iNbFFTNoise++;
#ifdef TESTTIMETESTMICRO
      if (iNbFFTNoise >= MAXFFTSIGNAL)
        Serial.printf("Fin de l'acquisition en mode test micro signal canal droit en %d µs, iGlobalSample %d\n", micros()-uiTime, iGlobalSample);
#endif
    }
    bRight = false;
  }
  if ((!bStereo and iNbFFTNoise >= MAXFFTSIGNAL) or (bStereo and iNbFFTNoise >= MAXFFTSIGNAL and iNbFFTNoiseG >= MAXFFTSIGNAL))
  {
    // Fin de l'acquisition en mode test micro signal
    bTestSignal = false;
    if (uiFeAcq != uiFeWav)
    {
      if (bStereo)
        iBufferProcessMode = BPM_STEREODECIM;  // Stereo processing with filter and decimation
      else
        iBufferProcessMode = BPM_MONOFLTDECIM; // Mono treatment with filter and decimation
    }
    else if (bStereo)
      iBufferProcessMode = BPM_STEREO;         // Stereo processing without filter
    else if (pParamsOp->bFiltre)
      iBufferProcessMode = BPM_MONOFLT;        // Mono treatment with filter
    else
      iBufferProcessMode = BPM_MONONOFLT;      // Mono treatment without filter
  }
}
    
//-------------------------------------------------------------------------
//! \brief Traitement du bruit
void CGenericRecorder::NoiseTreatment()
{
  //Serial.printf("CGenericRecorder::NoiseTreatment FFT %d, iFFTLen %d, idxFMin %d, idxFMax %d\n", iNbFFTNoise, iFFTLen, idxFMin, idxFMax);
  //delay(1000);
  // Calcul du bruit moyen
  for( int j=idxFMin; j<=idxFMax; j++)
  {
    if (j >= iFFTLen/2)
      Serial.printf("CGenericRecorder::NoiseTreatment Dépassement iFFTLen %d, idxFMin %d, idxFMax %d\n", iFFTLen, idxFMin, idxFMax);
    int16_t iNiv = (int16_t)bufferCpxFFT[j];
    iTbMagCnx[j]   += abs(iNiv);
    iTotalBruitMag += abs(iNiv);
    iNbMagNoise++;
  }
  iNbFFTNoise++;
  // Test si suffisamment de FFT pour le calcul du bruit moyen
  if (iNbFFTNoise > MAXFFTNOISE and !bNoiseOK)
  {
#ifdef TEST_BRUIT
    Serial.printf("Fin bruit NbFFT %d, TSeuil %d, idxFMin %d (%fkHz), idxFMax %d (%fkHz), uiFe %d, FFTLen %d\n",
      iNbFFTNoise, pParamsOp->bTypeSeuil, idxFMin, GetFreqIdx(idxFMin), idxFMax, GetFreqIdx(idxFMax), uiFeWav, iFFTLen);
#endif
    // Calcul de la magnitude moyenne
    iBruitMag = iTotalBruitMag / iNbMagNoise;
    // Calcul du bruit moyen en dB
    iBruitdB = GetNivDB( iBruitMag);
#ifdef TEST_BRUIT
    Serial.printf("Bruit moyen %d dB (mag %d), NbMag %d\n", iBruitdB/10, iBruitMag, iNbMagNoise);
#endif
    // Calcul du seuil en dB
    if (pParamsOp->bTypeSeuil == false)
      iSeuildB = iBruitdB + (pParamsOp->iSeuilDet * 10);
    else
      iSeuildB = pParamsOp->iSeuilAbs * 10;
    // Calcul du seuil en magnitude
    iSeuilMag = GetNivMag(iSeuildB);
    // Calcul du bruit moyen global
    uint32_t iNoiseCnxMag;
    short iNoiseCnxdB;
    int iTotalNoisedB = 0;
    for( int j=idxFMin; j<=idxFMax; j++)
    {
      // Calcul de la magnitude moyenne
      iNoiseCnxMag = iTbMagCnx[j] / iNbFFTNoise;
      // Calcul du bruit moyen en dB
      iNoiseCnxdB = GetNivDB( (short)iNoiseCnxMag);
      iTotalNoisedB += iNoiseCnxdB;
    }
    // Calcul du bruit moyen des canaux
    iTotalNoisedB /= (idxFMax-idxFMin+1);
    if (iBruitdB < iTotalNoisedB)
      // On prend le bruit le plus fort
      iBruitdB = iTotalNoisedB;
    //Serial.printf("Fin NoiseTreatment idxFMin %d idxFMax %d\n", idxFMin, idxFMax);
    // Calcul du seuil de chaque canal
    ThresholdCalculation();
    // Test du nombre de pics (seuil +6dB par rapport au bruit moyen)
    iNbPics = 0;
    int iNoiseSeuil = iBruitdB + 60;
    if (pParamsOp->iModeRecord == PRHINOLOG)
      // En FFT 1024, +9dB par rapport au bruit moyen
      iNoiseSeuil += 30;
    for( int j=idxFMin; j<=idxFMax; j++)
    {
      if (iTbNoise[j] > iNoiseSeuil)
        iNbPics++;
#ifdef TEST_BRUIT
      if (iTbNoise[j] > iNoiseSeuil)
        Serial.printf("Pic de bruit sur %fkHz (%d) Bruit %ddB (seuil %ddB, bruit moyen %ddb)\n", GetFreqIdx(j), j, iTbNoise[j]/10, iNoiseSeuil/10, iBruitdB/10);
#endif
    }
    // Init de la fréquence max des pics en kHz
    if (iNbPics == 0)
      fFreqMaxPic = 0.0;
    else
      // Fréquence du pic le plus fort en kHz
      fFreqMaxPic = GetFreqIdx( (int)fFreqMaxPic);
    bNoiseOK = true;
    if (uiFeAcq != uiFeWav)
    {
      if (bStereo)
        iBufferProcessMode = BPM_STEREODECIM;  // Stereo processing with filter and decimation
      else
        iBufferProcessMode = BPM_MONOFLTDECIM; // Mono treatment with filter and decimation
    }
    else if (bStereo)
      iBufferProcessMode = BPM_STEREO;         // Stereo processing without filter
    else if (pParamsOp->bFiltre)
      iBufferProcessMode = BPM_MONOFLT;        // Mono treatment with filter
    else
      iBufferProcessMode = BPM_MONONOFLT;      // Mono treatment without filter
    /*switch(iBufferProcessMode)
    {
    case BPM_MONOFLTDECIM: Serial.println("CGenericRecorder::NoiseTreatment goto BPM_MONOFLTDECIM"); break;
    case BPM_STEREO      : Serial.println("CGenericRecorder::NoiseTreatment goto BPM_STEREO");       break;
    case BPM_MONOFLT     : Serial.println("CGenericRecorder::NoiseTreatment goto BPM_MONOFLT");      break;
    case BPM_MONONOFLT   : Serial.println("CGenericRecorder::NoiseTreatment goto BPM_MONONOFLT");    break;
    default              : Serial.println("CGenericRecorder::NoiseTreatment goto unknow process !"); break;
    }*/
    if (pParamsOp->bSaveNoise)
    {
      // Mémorisation du bruit dans un fichier
      MemoFileNoise();
      // On stoppe la mémorisation du bruit dans un fichier
      pParamsOp->bSaveNoise = false;
    }
    if (iUtilLED == LEDNOISE)
      digitalWriteFast(LED_BUILTIN, LOW);
    //Serial.printf("CGenericRecorder::NoiseTreatment OK bNoiseOK %d\n", bNoiseOK);
  }
}

//-------------------------------------------------------------------------
//! \brief Calcul du seuil de chaque canal
void CGenericRecorder::ThresholdCalculation()
{
  //Serial.printf("ThresholdCalculation idxFMin %d, idxFMax %d\n", idxFMin, idxFMax);
  // On calcule le seuil de chaque canaux, le bruit de chaque canaux ne pouvant pas être plus faible que le bruit moyen
  uint32_t iNoiseCnxMag;
  short iNoiseCnxdB, iSeuildBCnx;
  idBMaxNoise = -1200;
  idBMinNoise = -1200;
  for( int j=idxFMin; j<=idxFMax; j++)
  {
    // Calcul de la magnitude moyenne
    iNoiseCnxMag = iTbMagCnx[j] / iNbFFTNoise;
    // Calcul du bruit moyen en dB
    iNoiseCnxdB = GetNivDB( (short)iNoiseCnxMag);
    if (iNoiseCnxdB < iBruitdB)
      // On prend le bruit moyen
      iNoiseCnxdB = iBruitdB;
    iTbNoise[j] = iNoiseCnxdB;
    if (iNoiseCnxMag < iMagMinNoise)
    {
      iMagMinNoise = iNoiseCnxMag;
      idBMinNoise  = iNoiseCnxdB;
    }
    if (iNoiseCnxMag > iMagMaxNoise)
    {
      iMagMaxNoise = iNoiseCnxMag;
      idBMaxNoise  = iNoiseCnxdB;
      fFreqMaxPic  = j;
    }
    // Calcul du seuil en dB
    if (pParamsOp->bTypeSeuil == false)
      iSeuildBCnx = iNoiseCnxdB + (pParamsOp->iSeuilDet * 10);
    else
      iSeuildBCnx = pParamsOp->iSeuilAbs * 10;
    // Calcul du seuil en magnitude
    iTbSeuilCnx[j] = GetNivMag( iSeuildBCnx);
//#ifdef TEST_BRUIT
    //if ( (pParamsOp->iModeRecord == PRHINOLOG and j >= 311 and j<=331) or (pParamsOp->iModeRecord != PRHINOLOG and j >= 76 and j<=86))
    //  Serial.printf("Bruit c%d (%fkHz) Bruit %ddB (%dmag), Seuil %ddB %dmag\n", j, GetFreqIdx(j), iNoiseCnxdB/10, iNoiseCnxMag, iSeuildBCnx/10, iTbSeuilCnx[j]);
//#endif
  }
  //Serial.printf("ThresholdCalculation Max noise %d (%d), Min Noise %d (%d)\n", idBMaxNoise, iMagMaxNoise, idBMinNoise, iMagMinNoise);
}

//-------------------------------------------------------------------------
//! \brief Traitements dans la boucle principale du programme
void CGenericRecorder::OnLoop()
{
  // Basic, do nothing
}

//----------------------------------------------------
//! \brief Fonction de transformation des niveaux en dB
//! Retourne le niveau en dizièmes de dB
//! \param iNiv Niveau en magnitude
short CGenericRecorder::GetNivDB(
  short iNiv
  )
{
  // La table NivToDbA est valide de 1 à 1023 de -120.0dB à -59.7dB.
  // La table NivToDbB est valide de 1024 à 52224 de -59.7dB à -25.6dB.
  int iNivDB = 0;
  // Prise en compte de la valeur absolue pour prendre en compte les magnitude négative
  int iNiveau = abs(iNiv);
  if (iNiveau < MAXTBDBA)
    // de 1 à 1023, c'est le niveau direct en dizième de dB
    iNivDB = NivToDbA[iNiveau];
  else
  {
    iNiveau = (iNiveau-MAXTBDBA)/10;
    if (iNiveau < MAXTBDBB)
      // de 1024 à 52224, c'est le niveau direct en dizième de dB
      iNivDB = NivToDbB[iNiveau];
    else
      // Niveau trop élevé, saturation, on impose -25dB
      iNivDB = -250;
  }
  return iNivDB;
}

//----------------------------------------------------
//! \brief Fonction de transformation des niveaux en magnitude
//! Retourne le niveau en magnitude
//! \param iNiv Niveau en dixième de dB
short CGenericRecorder::GetNivMag(
  short iNiv
  )
{
    // Calcul du niveau en magnitude, on recherche dans la 1ère table
    short iMag = 0;
    for (int i=iBruitMag; i<MAXTBDBA; i++)
    {
      if (NivToDbA[i] >= iNiv)
      {
        iMag = i;
        break;
      }
    }
    if (iMag == 0)
    {
      // On recherche dans la seconde table
      for (int i=0; i<MAXTBDBB; i++)
      {
        if (NivToDbB[i] >= iNiv)
        {
          iMag = (MAXTBDBA + 1 + i) * 10;
          break;
        }
      }
    }
    if (iMag == 0)
    {
      // Niveau non trouvé, on met une magnitude élevée
      iMag = 32000;
    }
    return iMag;
}

//----------------------------------------------------
// Retourne une fréquence en kHz à partir de l'indice FFT
float CGenericRecorder::GetFreqIdx( short idx)
{
  float f = (fCanal + (float)idx * fCanal) / 1000.0;
  return f;
}
    
//----------------------------------------------------
//! \brief Mémorisation du fichier de bruit
void CGenericRecorder::MemoFileNoise()
{
  // Fichier csv des résultats de bruit
  SdFile dataFile;
  char fileName[60];
  char sTemp[80];
  char cPic;
  uint32_t iNoiseCnxMag;
  short iNoiseCnxdB;
  float fFreq;

  // Création du nom du fichier (nouveau fichier à chaque fois avec l'heure et la date)
  setTime(Teensy3Clock.get());
  sprintf( fileName, "%s%s_NoiseLog%04d%02d%02d_%02d%02d%02d.csv", pParamsOp->sPrefixe, sSerialName, year(), month(), day(), hour(), minute(), second());
  //Serial.printf("Fichier bruit %s (%d)\n", fileName, strlen(fileName));
  // Ouverture du fichier
  if (dataFile.open(fileName, O_CREAT | O_WRITE))
  {
    // Ecriture de la 1er ligne du fichier
    dataFile.println("Idx,F,BMoy,Pic");
    //Serial.printf("CGenericRecorder::MemoFileNoise idxFMin %d, idxFMax %d\n", idxFMin, idxFMax);
  
    // Calcul de la fréquence et du bruit pour chaque canaux de la bande d'intéret
    for( int j=idxFMin; j<=idxFMax; j++)
    {
      // Fréquence du canal en kHz
      fFreq = ((float)j * fCanal) / 1000.0;
      // Calcul de la magnitude moyenne
      iNoiseCnxMag = iTbMagCnx[j] / iNbFFTNoise;
      // Calcul du bruit moyen en dB
      iNoiseCnxdB = GetNivDB( (short)iNoiseCnxMag) / 10;
      if (iNoiseCnxdB < iBruitdB/10)
        // On prend le bruit moyen
        iNoiseCnxdB = iBruitdB/10;
      cPic = ' ';
      if (iNoiseCnxdB > (iBruitdB/10 + 5))
        cPic = '*';
      // Ecriture des infos de bruit
      sprintf(sTemp, "%d,%5.1f,%d,%c", j, fFreq, iNoiseCnxdB, cPic);
      dataFile.println( sTemp);
    }
    // Ecriture des infos globales
    if (iNbPics > 1)
      sprintf(sTemp, "%02d/%02d/%02d %02d:%02d:%02d Bruit moyen %ddB %d pics", day(), month(), year()-2000, hour(), minute(), second(), iBruitdB/10, iNbPics);
    else
      sprintf(sTemp, "%02d/%02d/%02d %02d:%02d:%02d Bruit moyen %ddB %d pic" , day(), month(), year()-2000, hour(), minute(), second(), iBruitdB/10, iNbPics);
    dataFile.println( sTemp);
    // Vide les buffers et ferme le fichier
    dataFile.sync();
    dataFile.close();
  }
  else
    Serial.printf("Erreur ouverture %s !!!\n", fileName);
}

//----------------------------------------------------
//! \brief Traitement erreur DMA
//! \param DMAErr Code d'erreur du DMA
void CGenericRecorder::OnErreurDMA(
  uint32_t DMAErr
  )
{
  // Arrêt éventuel de l'enregistreur
  StopRecording();
  // Mémorisation uniquement de la 1ère erreur
  if (!bErrDMA)
  {
    uint32_t uiChannel = (DMAErr & 0x00001F00) >> 8;
    if ((DMAErr & 0x00001F00) != 0)
      LogFile::AddLog( LLOG, "DMA error = Error Channel Number %d", (int)uiChannel);
    if ((DMAErr & 0x00000001) != 0)
      LogFile::AddLog( LLOG, "DMA error = Destination Bus Error");
    if ((DMAErr & 0x00000002) != 0)
      LogFile::AddLog( LLOG, "DMA error = Source Bus Error");
    if ((DMAErr & 0x00000004) != 0)
      LogFile::AddLog( LLOG, "DMA error = Scatter/Gather Configuration Error");
    if ((DMAErr & 0x00000008) != 0)
      LogFile::AddLog( LLOG, "DMA error = NBYTES/CITER Configuration Error");
    if ((DMAErr & 0x00000010) != 0)
      LogFile::AddLog( LLOG, "DMA error = Destination Offset Error");
    if ((DMAErr & 0x00000020) != 0)
      LogFile::AddLog( LLOG, "DMA error = Destination Address Error");
    if ((DMAErr & 0x00000040) != 0)
      LogFile::AddLog( LLOG, "DMA error = Source Offset Error");
    if ((DMAErr & 0x00000080) != 0)
      LogFile::AddLog( LLOG, "DMA error = Source Address Error");
    if ((DMAErr & 0x00004000) != 0)
      LogFile::AddLog( LLOG, "DMA error = Channel Priority Error");
    if ((DMAErr & 0x00008000) != 0)
      LogFile::AddLog( LLOG, "DMA error = Group Priority Error");
    if ((DMAErr & 0x00010000) != 0)
      LogFile::AddLog( LLOG, "DMA error = Transfer Canceled");
  }
  // Init de l'erreur DMA
  bErrDMA = true;
}

//! \brief Instance unique de la classe
CGenericRecorder *CGenericRecorder::pInstance = NULL;

// Erreur éventuelle sur le DMA
bool CGenericRecorder::bErrDMA;
char CGenericRecorder::sErrDMA[80];

// Largeur d'un canal en Hz
float CGenericRecorder::fCanal;

// Textes pour le log de la gestion mémoire
extern const char *txtLogAdjustNbBufferH[];
extern const char *txtLogAdjustNbBufferF[];
extern const char *txtLogAdjustNbBufferT41[];
extern const char *txtLogResetMemA[];
extern const char *txtLogResetMemB[];
extern const char *txtLogResetMemC[];

//-------------------------------------------------------------------------
// Classe rassemblant toutes les opérations nécessaire pour l'enregistrement
// - Acquisition (ADC via DMA cadencé par un PDB, traitement dans l'IT DMA)
// - Traitements (dans l'IT DMA)
// - FFT pour vérifier la présence d'un signal (dans l'IT DMA)
// - Enregistrement dans un fichier wav si activité (dans le loop du programme principal)
//-------------------------------------------------------------------------
// Constructeur (initialisation de la classe)
CRecorder::CRecorder(
  ParamsOperator *pParams, // Pointeur sur les paramètres opérateur
  int iNbSmpBuffer         // Nombre de buffers de taille MAXSAMPLE pour l'enregistrement
  )
  :CGenericRecorder( pParams, FFTLEN)
{
  //Serial.printf("CRecorder constructeur iFFTLen %d\n", iFFTLen);
  pCallAnalysis = NULL;
  bStereo = false;
  iMasterSlaveSynchro = -1;
  if (pParams->iModeRecord == SYNCHRO)
    iMasterSlaveSynchro = pParams->iMasterSlave;
  //Serial.printf("CRecorder::CRecorder, iMasterSlaveSynchro = %d\n", iMasterSlaveSynchro);
  iNbSampleBuffer = iNbSmpBuffer;
  pAcquisition = NULL;
  // Adaptation du nombre de buffers en fonction de la taille de la zone non allouée
  pSamplesRecord = NULL;
  int iPreTrigDuration = 0;
  double fDurEch = 1.0 / (double)uiFeWav;                     // Durée d'un échantillon en seconde
#if defined(__IMXRT1062__) // Teensy 4.1
  // ATTENTION, avant de lire une portion de la PSRAM, il faut utiliser arm_dcache_flush_delete(void *addr, uint32_t size)
  // Sur le T4.1, on tente de réserver une taille impossible qu'on diminue en boucle jusqu"à trouver la taille correcte
  if (external_psram_size > 0 and pParamsOp->iPreTrigDurAuto > 0 and (pParamsOp->iModeRecord == RECAUTO or pParamsOp->iModeRecord == PHETER
      or pParamsOp->iModeRecord == AUDIOREC or pParamsOp->iModeRecord == SYNCHRO))
  {
    iPreTrigDuration = pParamsOp->iPreTrigDurAuto;
    if (pParamsOp->iModeRecord == PHETER)
      iPreTrigDuration = pParamsOp->iPreTrigDurHeter;
    if (uiFeAcq == 384000 and iPreTrigDuration > 10)
      iPreTrigDuration = 10;
    else if (uiFeAcq == 500000 and iPreTrigDuration > 7)
      iPreTrigDuration = 7;
    // Calcul du nombre de buffer à réserver en mémoire étendue
    //Serial.printf("Teensy 4.1, CRecorder::CRecorder, RAM externe %dMO\n", external_psram_size);
    int iNbMax = (8 * 1024 * 1024) / MAXSAMPLE;
    double fDurBuff = MAXSAMPLE * fDurEch;
    double fNbBuff = (double)iPreTrigDuration / fDurBuff;
    iNbSampleBuffer = (int)fNbBuff + 1;
    if (iNbSampleBuffer > iNbMax)
      iNbSampleBuffer = iNbMax - 1;
    //Serial.printf("RAM étendue %dMO, Max buffer %d, durée buffer %fms, pré-trigger %ds, NbBuffer %d\n", 8, iNbMax, fDurBuff*1000.0, iPreTrigDuration, iNbSampleBuffer);
  }
  else
  {
    //Serial.println("Teensy 4.1, CRecorder::CRecorder, pas de RAM externe ou mode incompatible. On utilise la RAM2");
    /*size_t total, totalUser, totalFree;
    int nrBlocks;
    sm_malloc_stats(&total, &totalUser, &totalFree, &nrBlocks);
    Serial.printf("sm_malloc_stats total %u, totalUser %u, totalFree %u, nrBlocks %u\n",  total, totalUser, totalFree, nrBlocks);
    sm_malloc_stats_pool(&extmem_smalloc_pool, &total, &totalUser, &totalFree, &nrBlocks);
    Serial.printf("sm_malloc_stats(extmem_smalloc_pool) total %u, totalUser %u, totalFree %u, nrBlocks %u\n",  total, totalUser, totalFree, nrBlocks);*/
    int iMax = ((512 * 1024) / MAXSAMPLE) - 1;
    //Serial.printf("Max buffer %d\n", iMax);
    while(iMax > 0)
    {
      pSamplesRecord = new volatile int16_t[MAXSAMPLE * iMax];
      if ((int32_t)pSamplesRecord > 0x20200000 and (int32_t)pSamplesRecord < 0x20400000)
      {
        delete pSamplesRecord;
        break;
      }
      if (pSamplesRecord != NULL)
        delete pSamplesRecord;
      iMax--;
    }
    // On prend un buffer de moins pour garder de la place
    iNbSampleBuffer = iMax - 1;
    pSamplesRecord = NULL;
    pParamsOp->iPreTrigDurAuto  = 0;
    pParamsOp->iPreTrigDurHeter = 0;
  }
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  pParamsOp->iPreTrigDurAuto  = 0;
  pParamsOp->iPreTrigDurHeter = 0;
  bool bHeap = true;
  uint32_t uiFreeHeap = ramMonitor.unallocated();             // Récupération de la taille de la zone non allouée
  uint32_t uiFreeRam  = ramMonitor.free();                    // Récupération de la taille de la ram libre
  uint32_t uiSizeUnBuff = sizeof(int16_t) * MAXSAMPLE;        // Calcul de la taille d'un buffer
  if (uiFreeHeap < uiSizeUnBuff and uiFreeHeap < uiFreeRam)
  {
    uiFreeHeap = uiFreeRam;                                   // La taille de la ram libre est plus grande que la zone non allouée
    bHeap = false;
  }
  iNbSampleBuffer = (uiFreeHeap / uiSizeUnBuff) - 1;          // Calcul du nombre de buffer possible moins 1 par prudence
#endif
  if (pParams->iModeRecord == PTSTMICRO)
    iNbSampleBuffer = 3;                                      // 3 buffers seulement en mode test pour garder de la place pour l'image du test étendu
  else if (pParams->iModeRecord == PRHINOLOG or pParams->iModeRecord == TIMEDREC)
    iNbSampleBuffer = min(5, iNbSampleBuffer);                // 5 buffers seulement, pas de pré-trigger pour ces modes
  double fDurBuff = fDurEch * MAXSAMPLE * iNbSampleBuffer * 1000;     // Durée max du pré-trigger en ms
  if (iPreTrigDuration == 0)                                  // En absence de mémoire supplémentaire
  {
    while (fDurBuff > 1200.0)                                 // On limite le pré-trigger à 1.2s (cas des fréquences d'échantillonnage basses)
    {
      iNbSampleBuffer--;
      fDurBuff = fDurEch * MAXSAMPLE * iNbSampleBuffer * 1000;
    }
  }
  //if (pParams->iModeRecord == SYNCHRO)
  //  iNbSampleBuffer -= 2;                                     // 2 buffers de moins en mode synchro
  if (iNbSampleBuffer < 3)
  {
    // Log du problème
    LogFile::AddLog( LLOG, txtLogResetMemA[CModeGeneric::GetCommonsParams()->iLanguage], iNbSampleBuffer);
    delay(10000);
    // Passage en mode erreur
    CModeError::SetTxtError((char *)txtLogResetMemC[CModeGeneric::GetCommonsParams()->iLanguage]);
    CModeGeneric::ChangeMode( MERROR);
  }
  uint32_t uiSizeBuff = sizeof(int16_t) * MAXSAMPLE * iNbSampleBuffer; // Calcul de la taille du buffer alloué
  //Serial.printf("uiFreeHeap %ld, uiFreeRam %ld, uiSizeUnBuff %ld, uiSizeBuff %ld, iNbSampleBuffer %d, MAXSAMPLE %d, sizeof(int16_t) %d\n", uiFreeHeap, uiFreeRam, uiSizeUnBuff, uiSizeBuff, iNbSampleBuffer, MAXSAMPLE, sizeof(int16_t));
  if (CModeGeneric::GetRecorderType() >= PASSIVE_STEREO and pParamsOp->iStereoRecMode == SMSTEREO)
    fDurEch /= 2;                                             // Durée divisée par 2 en stéréo
#if defined(__MK66FX1M0__) // Teensy 3.6
  if (bHeap)
    LogFile::AddLog( LLOG, txtLogAdjustNbBufferH[CModeGeneric::GetCommonsParams()->iLanguage], uiFreeHeap, iNbSampleBuffer, uiSizeBuff, (int)fDurBuff);
  else
    LogFile::AddLog( LLOG, txtLogAdjustNbBufferF[CModeGeneric::GetCommonsParams()->iLanguage], uiFreeHeap, iNbSampleBuffer, uiSizeBuff, (int)fDurBuff);
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  LogFile::AddLog( LLOG, txtLogAdjustNbBufferT41[CModeGeneric::GetCommonsParams()->iLanguage], iNbSampleBuffer, uiSizeBuff, (int)fDurBuff);
#endif
  // Initialisation du pointeur de moitié du buffer DMA
  pHalfDMASamples = (uint32_t *)&(samplesDMA[MAXSAMPLE]);
  // Initialisation de la FFT
  arm_cfft_radix4_init_q15( &cpx_fft_inst, FFTLEN, 0, 1);
  // Création du buffer d'enregistrement
#if defined(__MK66FX1M0__) // Teensy 3.6
  pSamplesRecord = new int16_t[MAXSAMPLE*iNbSampleBuffer];
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  if (external_psram_size == 0)
    // Création en RAM normale sur T3.6 ou RAM2 sur T4.1
    pSamplesRecord = new volatile int16_t[MAXSAMPLE*iNbSampleBuffer];
  else
    // Création en RAM optionnelle sur T4.1
    pSamplesRecord = (int16_t *)extmem_malloc( sizeof(int16_t)*MAXSAMPLE*iNbSampleBuffer);
#endif
  //CModeGeneric::PrintRamUsage( "Ram après création buffer");
  // Vérification que le buffer est bien alloué
  if (pSamplesRecord == NULL)
  {
    // Log du problème
    LogFile::AddLog( LLOG, txtLogResetMemB[CModeGeneric::GetCommonsParams()->iLanguage]);
    delay(10000);
    // Passage en mode erreur
    CModeError::SetTxtError((char *)txtLogResetMemC[CModeGeneric::GetCommonsParams()->iLanguage]);
    CModeGeneric::ChangeMode( MERROR);
  }
  // RAZ du buffer
  memset( (void *)pSamplesRecord, 0, sizeof(int16_t)*MAXSAMPLE*iNbSampleBuffer);
  // Initialisation du pointeur max et moitié du buffer d'enregistrement
  if (iNbSampleBuffer > 1)
    pSampMax = &(pSamplesRecord[MAXSAMPLE*(iNbSampleBuffer-1)]);
  else
    pSampMax = pSamplesRecord;
  bStarting = false;
  // Init des variables
  iNiveauMax  = -1;
  iNiveauMaxL = -1;
  fDCBlockingFactor = 0.0;
  strcpy(sPrefixe, pParamsOp->sPrefixe);
  if (strlen(sPrefixe) > 4)
    sPrefixe[4] = 0;
  else if (strlen(sPrefixe) < 4)
  {
    while (strlen(sPrefixe) < 4)
      strcat(sPrefixe, "_");
  }
  iNbSynchRec = 0;  
/*#ifdef TESTQFC
    // Test provisoire des QFC
    pCallAnalysis = new CCallAnalysis();
    pCallAnalysis->InitializingAnalysis( uiFeAcq, FFTLEN);
    // Test QFC Nathusius
    //pCallAnalysis->AddQFCCall( 32400, 42700, 400, 5000, 6000, 107000);
    // Test QFC Leisler
    pCallAnalysis->AddQFCCall( 20500, 27400, 500, 4600, 5100, 186000);
    // Test QFC Grande Noctule
    //pCallAnalysis->AddQFCCall( 11400, 17800, 200, 5300, 15300, 345000);
#endif*/
  //Serial.println("CRecorder constructeur OK");
}
  
//-------------------------------------------------------------------------
// Destructeur
CRecorder::~CRecorder()
{
  //Serial.println("CRecorder delete pSamplesRecord");
  if (pCallAnalysis != NULL)
    delete pCallAnalysis;
  if (pAcquisition != NULL)
    delete pAcquisition;
#if defined(__MK66FX1M0__) // Teensy 3.6
    delete [] pSamplesRecord;
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  if (external_psram_size == 0)
    delete [] pSamplesRecord;
  else
    extmem_free( (void *)pSamplesRecord);
#endif
}

//-------------------------------------------------------------------------
//! \brief Création du gestionnaire d'acquisition
void CRecorder::CreateAcquisition()
{
  bStereo = false;
  pAcquisition = new Acquisition( (uint16_t *)samplesDMA, MAXSAMPLE, PINA3_AUDIO, 384000);
}
  
//-------------------------------------------------------------------------
// Démarre l'acquisition
// Retourne true si démarrage OK
bool CRecorder::StartAcquisition()
{
#ifdef TESTTIMEISR
  uiTimeISRDMA   = 0;
  uiNbTimeISRDMA = 0;
#endif
  //if (CModeGeneric::bDebug)
  //  Serial.println("CRecorder::StartAcquisition");
  if (pAcquisition == NULL)
    CreateAcquisition();
  CGenericRecorder::StartAcquisition();
#ifdef TESTMINMAX
  // RAZ des échantillons min et max
  iEchMin = 65000;
  iEchMax = -65000;
  iNbBuffEch = 0;
#endif
  filteredOutput = 0;
  filteredOutputL = 0;
  scaledPreviousFilterOutput = 0;
  scaledPreviousFilterOutputL = 0;
  previousSample = 0;
  previousSampleL = 0;
  previousFilterOutput = 0;
  previousFilterOutputL = 0;
  if (pParamsOp->bBatcorderLog)
  {
    CreateBatcorderLogFile();
    AddNewPeriodBatcorderLogFile(true);
  }
  // Calcul du coefficient du filtre passe haut
  if (iFreqHighPass < 100)
    fDCBlockingFactor = 0.999;
  else
  {
    // 1 / (2 * PI * cut_off_frequency * (1 / sampling_frequency) + 1)  (Frank DD4WH)
    fDCBlockingFactor = 1.0 / (2.0 * PI * (float)iFreqHighPass * (1.0 / (float)uiFeWav) + 1.0);
    //Serial.printf("iFreqHighPass %d, uiFe %d, fDCBlockingFactor %f\n", iFreqHighPass, uiFeWav, fDCBlockingFactor);
  }
  //if (CModeGeneric::bDebug)
  //  Serial.printf("iFreqHighPass = %d, coeff = %f\n", iFreqHighPass, fDCBlockingFactor);
  // Calcul des informations
  bAutorecord = false;
  // Auto record in automatic record mode, fixed point protocol or Heterodyne mode with automatic recording
  if (pParamsOp->iModeRecord == RECAUTO or pParamsOp->iModeRecord == PROTFIXE or (pParamsOp->iModeRecord == PHETER and pParamsOp->bAutoRecH))
    bAutorecord = true;
  /*if (CModeGeneric::bDebug)
  {
    LogFile::SetbConsole( true);
    if (!bAutorecord)
      LogFile::AddLog( LLOG, "bAutorecord = false, iModeRecord=%d, bAutoRecH %d", pParamsOp->iModeRecord, pParamsOp->bAutoRecH);
    else
      LogFile::AddLog( LLOG, "bAutorecord = true, iModeRecord=%d, bAutoRecH %d", pParamsOp->iModeRecord, pParamsOp->bAutoRecH);
    LogFile::SetbConsole( CModeGeneric::bDebug);
  }*/
  bTestMicro = false;
  if (pParamsOp->iModeRecord == PTSTMICRO)
    bTestMicro = true;
  // RAZ des détections
  memset( (void *)TbBitsDetect,   0, sizeof(uint8_t)*FFTLEN/2);
  memset( (void *)TbNbDetect,     0, sizeof(uint8_t)*FFTLEN/2);
  memset( (void *)TbMemoNbDetect, 0, sizeof(uint8_t)*FFTLEN/2);
  if (CModeGeneric::GetRecorderType() >= PASSIVE_STEREO)
  {
    memset( (void *)TbBitsDetectL,   0, sizeof(uint8_t)*FFTLEN/2);
    memset( (void *)TbNbDetectL,     0, sizeof(uint8_t)*FFTLEN/2);
    memset( (void *)TbMemoNbDetectL, 0, sizeof(uint8_t)*FFTLEN/2);
  }
  bActivite = false;
  bActivGauche = false;
  bRecording = false;
  iTotalEchRec = 0;
  iNbEchLastA = 0;
  //iFileNumber = 0;
  // Init du filtre
  InitFiltre();
  /*
   * Position de départ des pointeurs
   *  |>pPrevSampIn réception précédente d'échantillons et dernier bloc à enregistrer
   *  |>pCurrentSampIn réception courante d'échantillons
   *  |     |>pNextSampIn prochaine réception d'échantillon
   *  |     |     |>pSampRecord 1er bloc à enregistrer en absence d'activités
   *  |     |     |     |     |      |>pSampMax dernier bloc du buffer
   *  |-----|-----|-----|-----|-----|-----|
   */
  pPrevSampIn    = pSamplesRecord;
  pCurrentSampIn = pSamplesRecord;
  pNextSampIn    = pCurrentSampIn;
  pNextSampIn   += MAXSAMPLE;
  if (pNextSampIn > pSampMax)
    pNextSampIn  = pSamplesRecord;
  pSampRecord    = NULL;
  bBufferLoss    = false;
  // Lancement de l'acquisition
  memset( (void *)samplesDMA, 0, sizeof(uint16_t)*MAXSAMPLE*2);
  pBufferATraiter = NULL;
#ifdef TESTTEMPSREEL
  uiTimeTot  = 0;
  uiTimeApp  = 0;
  uiNbTraite = 0;
  uiTimeLoop = 0;
  uiNbLoop   = 0;
  uiMaxLoop   = 0;
  pBA = NULL;
  pBB = NULL;
#endif
  strcpy( sWaveFileTemp, "temp.wav");
  if (sd.exists( sWaveFileTemp))
      sd.remove( sWaveFileTemp);
  if (!wavefile.OpenWaveFileForWrite( uiFeWav, sWaveFileTemp, pParamsOp->bExp10, bStereo))
  {
    iNbWavefileError++;
    LogFile::AddLog( LLOG, "CRecorder::StartAcquisition error on wavefile.OpenWaveFileForWrite(%s)", sWaveFileTemp);
  }
  if (pAcquisition != NULL)
  {
    pAcquisition->setFe( uiFeAcq);
#ifdef TESTPERIODISR
    double dTime = 1.0 / (double)uiFeAcq * 1000000.0 * 8192.0;
    if (CModeGeneric::GetRecorderType() >= PASSIVE_STEREO and pParamsOp->iStereoRecMode == SMSTEREO)
    {
      dTime /= 2;
      Serial.printf("Temps 8192 ISR attendu %dµs (Fe %dkHz stéréo)\n", (int)dTime, uiFeAcq);
    }
    else
      Serial.printf("Temps 8192 ISR attendu %dµs (Fe %dkHz mono)\n", (int)dTime, uiFeAcq);
#endif
    pAcquisition->start();
    bStarting = true;
  }
  iNbSynchRec = 0;
  bFFTSynchro = true;
  if (iMasterSlaveSynchro > MASTER)
    bFFTSynchro = false;
  if (iMasterSlaveSynchro >= MASTER)
  {
    switch (pParamsOp->iLEDSynchro)
    {
    case NOLEDSYNCH : iNbSynchRec =  0; break;  // LED non utilisée
    case LEDRECSYNCH: iNbSynchRec = 10; break;  // LED allumée sur enregistrement d'un fichier
    case LED3RECS   : iNbSynchRec =  3; break;  // LED allumée sur les 3 1er enregistrement seulement
    }
  }
  //Serial.printf("StartAcquisition iLEDSynchro %d, iNbSynchRec %d\n", pParamsOp->iLEDSynchro, iNbSynchRec);
  //Serial.println("CRecorder::StartAcquisition OK");
  return true;
}
  
//-------------------------------------------------------------------------
// Stoppe l'acquisition
// Retourne true si arrêt OK
bool CRecorder::StopAcquisition()
{
  //if (CModeGeneric::bDebug)
  //  Serial.println("CRecorder::StopAcquisition");
  //digitalWriteFast(LED_BUILTIN, LOW);
  // Fermeture éventuelle d'un fichier wav
  if (bRecording)
    StopRecording();
  if (bStarting and pAcquisition != NULL)
  {
    //Serial.println("CRecorder::StopAcquisition pAcquisition->stop");
    // Arret de l'acquisition
    pAcquisition->stop();
    bStarting = false;
  }
  // Fermeture du fichier wave temporaire
  wavefile.CloseWavfile();
  // Et on l'efface
  sd.remove( sWaveFileTemp);
  if (pParamsOp->bBatcorderLog)
    AddNewPeriodBatcorderLogFile(false);
#ifdef TESTTEMPSREEL
  Serial.printf("%d Traitements, Temps moyen traitement %04.1fms, max loop %dms\n", uiNbTraite, (float)uiTimeTot/(float)uiNbTraite, uiMaxLoop);
#endif
  return true;
}

//-------------------------------------------------------------------------
//! \brief Démarre l'enregistrement en auto
//! \param bAuto true pour lancer l'enregistrement automatique sur détection d'énergie
void CRecorder::SetAutoRecord(
  bool bAuto
  )
{
  bAutorecord = bAuto;
}

//-------------------------------------------------------------------------
// Démarre le mode enregistrement
// Retourne true si démarrage OK
bool CRecorder::StartRecording()
{
  //unsigned long uiTime = micros();
  if (!bRecording)
    // Ouverture du fichier wav
    OpenWave();
  //Serial.printf("StartRecording %d µs\n", micros() - uiTime);
  return true;
}

//-------------------------------------------------------------------------
// Stoppe le mode enregistrement
// Retourne true si arrêt OK
bool CRecorder::StopRecording()
{
  if (iUtilLED == LEDRECORD)
    digitalWriteFast(LED_BUILTIN, LOW);
  if (bRecording)
    CloseWave();
  // On positionne le prochain pointeur d'enregistrement au min du buffer de pré-enregistrement pour éviter les recouvrements
  SetSampRecordMin();
  return true;
}

const char *sMasterSlave[] = {"Ms", "S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9"};

//-------------------------------------------------------------------------
// Ouverture du fichier wav courant
void CRecorder::OpenWave()
{
  /*if (CModeGeneric::bDebug)
    Serial.println("CRecorder::OpenWave");*/
#if defined(__MK66FX1M0__) // Teensy 3.6
  if (iMasterSlaveSynchro == MASTER)
    // Set Recording pin for ESP32
    digitalWriteFast( PIN00RECORD, HIGH); // Record
#endif
  if (iNbSynchRec > 0)
    digitalWriteFast( 13, HIGH);
  iTotalEchRec = 0;
  iNbEchLastA  = 0;
  setTime(Teensy3Clock.get());
  if (pParamsOp->bBatcorderLog == false)
  {
    if (bStereo and bAutorecord)
    {
      if (bActivGauche)
        // Création d'un nouveau fichier wav au standard PaRecPR4146560_0_AAAAMMJJ_HHMMSSL.wav et enregistrement
        sprintf( sWaveFileName, "%s%s_0_%04d%02d%02d_%02d%02d%02d.wav", pParamsOp->sPrefixe, sSerialName, year(), month(), day(), hour(), minute(), second());
      else
        // Création d'un nouveau fichier wav au standard PaRecPR4146560_1_AAAAMMJJ_HHMMSSR.wav et enregistrement
        sprintf( sWaveFileName, "%s%s_1_%04d%02d%02d_%02d%02d%02d.wav", pParamsOp->sPrefixe, sSerialName, year(), month(), day(), hour(), minute(), second());      
    }
    else
      // Création d'un nouveau fichier wav au standard PaRecPR4146560_AAAAMMJJ_HHMMSS.wav et enregistrement
      sprintf( sWaveFileName, "%s%s_%04d%02d%02d_%02d%02d%02d.wav", pParamsOp->sPrefixe, sSerialName, year(), month(), day(), hour(), minute(), second());
    //Serial.printf("OpenWave %s\n", sWaveFileName);
  }
  else
  {
    iFileNumber++;
    // Création d'un nouveau fichier wav au standard Batcorder DDMMYY-HHMMSS-NAMENUMBER-00001.wav
    char sTemp[60];
    sprintf( sTemp        , "%02d%02d%02d-%02d%02d%02d-%s%s-%05d.wav", day(), month(), year()-2000, hour(), minute(), second(), sPrefixe, &sSerialName[2], iFileNumber);
    sprintf( sWaveFileName, "%02d%02d%02d-%02d%02d%02d-%s%s-%05d.wav", day(), month(), year()-2000, hour(), minute(), second(), sPrefixe, &sSerialName[2], iFileNumber);
    //                       24.02.20  11:03:24
    sprintf( sTimeWave    , "%02d.%02d.%02d\t%02d:%02d:%02d", day(), month(), year()-2000, hour(), minute(), second());
  }
  // Set temp wavefile empty
  if (!wavefile.IsOpen())
  {
    if (!wavefile.OpenWaveFileForWrite( uiFeWav, sWaveFileTemp, pParamsOp->bExp10, bStereo))
    {
      iNbWavefileError++;
      LogFile::AddLog( LLOG, "CRecorder::StartAcquisition error on wavefile.OpenWaveFileForWrite(%s)", sWaveFileTemp);
    }
  }
  else if (!wavefile.RestartWrite())
  {
    iNbWavefileError++;
    LogFile::AddLog( LLOG, "CRecorder::OpenWave error on wavefile.RestartWrite");
  }
  /*if (CModeGeneric::bDebug)
    Serial.printf("CRecorder::OpenWave %s\n", sWaveFileName);*/
  bRecording = true;
  if (iUtilLED == LEDRECORD)
    digitalWriteFast(LED_BUILTIN, HIGH);
}

//-------------------------------------------------------------------------
// Fermeture du fichier wav courant
void CRecorder::CloseWave()
{
  /*if (CModeGeneric::bDebug)
    Serial.println("CRecorder::CloseWave");*/
#if defined(__MK66FX1M0__) // Teensy 3.6
  if (iMasterSlaveSynchro == MASTER)
    // Reset Recording pin for ESP32
    digitalWriteFast( PIN00RECORD,  LOW); // No record
#endif
  //Serial.println("CRecorder::CloseWave");
  if (iNbSynchRec > 0)
  {
    digitalWriteFast( 13, LOW);
    if (iNbSynchRec < 9)
      iNbSynchRec--;
  }
  //Serial.printf("CloseWave iNbSynchRec %d\n", iNbSynchRec);
  // Stop recording
  bRecording = false;
  if (pParamsOp->bBatcorderLog)
    AddNewWavBatcorderLogFile();
  // Fermeture du fichier
  wavefile.CloseWavfile();
  // Vérification de la taille du fichier (uniquement hors protocole)
  if (pParamsOp->iModeRecord == RECAUTO and iTotalEchRec < iNbMinEch/2)
    // Fichier trop petit, on l'efface
    sd.remove( sWaveFileTemp);
  else
  {
    // On renome le fichier avec le bon nom
    sd.rename( sWaveFileTemp, sWaveFileName);
  }
  // Init des variables pour un nouvel enregistrement
  iTotalEchRec = 0;
  iNbEchLastA  = 0;
  // On ouvre un nouveau fichier temporaire
  if (!wavefile.OpenWaveFileForWrite( uiFeWav, sWaveFileTemp, pParamsOp->bExp10, bStereo))
  {
    iNbWavefileError++;
    LogFile::AddLog( LLOG, "CRecorder::CloseWave error on wavefile.OpenWaveFileForWrite(%s)", sWaveFileTemp);
  }
}

//----------------------------------------------------
//! \brief Creation of the log file in Batcorder mode
void CRecorder::CreateBatcorderLogFile()
{
  //Serial.println("CreateBatcorderLogFile");
  // Test if LOGFILE.txt exist
  if (!sd.exists("LOGFILE.txt"))
  {
    // Creation of the log file
    SdFile dataFile;
    if (dataFile.open( "LOGFILE.txt", O_CREAT | O_WRITE | O_APPEND))
    {
      // Write log
      dataFile.println( "Batcorder SW3.05 / logfile 3.0");
      dataFile.printf(  "created on %02d.%02d.%04d %02d:%02d:%02d\n", day(), month(), year(), hour(), minute(), second());
      dataFile.println( "");
      dataFile.println( "                                                                 ");
      dataFile.println( "");
      dataFile.println( "");
      // Close file
      dataFile.sync();
      dataFile.close();
    }
  }
}

//----------------------------------------------------
//! \brief Addition of a recording period in the Batcorder log
//! \param bOn true for begin period
void CRecorder::AddNewPeriodBatcorderLogFile(
  bool bOn)
{
  //Serial.println("AddNewPeriodBatcorderLogFile");
  SdFile dataFile;
  if (dataFile.open( "LOGFILE.txt", O_CREAT | O_WRITE | O_APPEND))
  {
    if (bOn)
      // Write log "Timer on  24.02.20  11:00:00  WK01539739  "20;42;800;16""
      dataFile.printf( "Timer on\t%02d.%02d.%02d\t%02d:%02d:%02d\t%s%s\t\"20;42;800;16\"\n", day(), month(), year()-2000, hour(), minute(), second(), sPrefixe, &sSerialName[2]);
    else
      // Write log "Timer off  25.02.20  06:00:00"
      dataFile.printf( "Timer off\t%02d.%02d.%02d\t%02d:%02d:%02d\n", day(), month(), year()-2000, hour(), minute(), second());
    // Close file
    dataFile.sync();
    dataFile.close();
  }
}

//----------------------------------------------------
//! \brief Adding a wav file in the Batcorder log
void CRecorder::AddNewWavBatcorderLogFile()
{
  //Serial.println("AddNewWavBatcorderLogFile");
  SdFile dataFile;
  if (dataFile.open( "LOGFILE.txt", O_CREAT | O_WRITE | O_APPEND))
  {
    unsigned long iDuration = (unsigned long)(wavefile.GetRecordDuration() * 1000.0);
    // Write log "T 24.02.20  11:03:24  240220-110324-WK01539739-00001.wav   3523ms"
    dataFile.printf( "T\t%s\t%s\t%dms\n", sTimeWave, sWaveFileName, iDuration);
    // Close file
    dataFile.sync();
    dataFile.close();
  }
}

//----------------------------------------------------
//! \brief Adding a temperature in the Batcorder log
//! \param fTemp Temperature
void CRecorder::AddNewTemperatureBatcorderLogFile(
  float fTemp)
{
  //Serial.printf("AddNewTemperatureBatcorderLogFile %02d:%02d:%02d\n", hour(), minute(), second());
  SdFile dataFile;
  if (dataFile.open( "LOGFILE.txt", O_CREAT | O_WRITE | O_APPEND))
  {
    // Write log "C  24.02.20  11:00:00  22.4"
    dataFile.printf( "C\t%02d.%02d.%02d\t%02d:%02d:%02d\t %.1f\n", day(), month(), year()-2000, hour(), minute(), second(), fTemp);
    // Close file
    dataFile.sync();
    dataFile.close();
  }
}

//-------------------------------------------------------------------------
// Stoppe ou lance l'acquisition
void CRecorder::SetAcquisition(
  bool bAcq   // true pour lancer l'acquisition, false pour la suspendre
  )
{
  if (bAcq and pAcquisition != NULL and !pAcquisition->IsAcquisition())
  {
    //Serial.println("CRecorder::SetAcquisition pAcquisition->start");
    pAcquisition->start();
  }
  else if (!bAcq and pAcquisition != NULL and pAcquisition->IsAcquisition())
  {
    //Serial.println("CRecorder::SetAcquisition pAcquisition->stop");
    pAcquisition->stop();
    delay(50);
  }
}

//-------------------------------------------------------------------------
// Retourne le pointeur sur la zone de réception et prépare le pointeur pour la prochaine réception
// Permet le partage des pointeurs entre le loop et les IT
//
// Gestion du buffer d'enregistrement
//        |>pPrevSampIn réception précédente d'échantillons et dernier bloc à enregistrer
//        |     |>pCurrentSampIn réception courante d'échantillons
//        |     |     |>pNextSampIn prochaine réception d'échantillon
//        |     |     |     |>pSampRecord 1er bloc à enregistrer en absence d'activités
//        |     |     |     |      |>pSampMax dernier bloc du buffer
//  |-----|-----|-----|-----|-----|-----|
//
int16_t *CRecorder::GetpSaveSample()
{
  int16_t *pSave;                       // Préparation valeur de retour
#if defined(__IMXRT1062__) // Teensy 4.1
  __disable_irq();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  unsigned char sreg_backup = SREG;     // Sauvegarde de l'état des interruptions
  cli();                                // On stoppe les interruptions
#endif
  pSave  = (int16_t *)pNextSampIn;      // Préparation du retour
  pPrevSampIn = pCurrentSampIn;         // Initialisation du dernier pointeur reçu
  pCurrentSampIn = pNextSampIn;         // Initialisation du pointeur de réception
  pNextSampIn += MAXSAMPLE;             // Préparation du prochain pointeur de réception
  if (pNextSampIn > pSampMax)
    pNextSampIn = pSamplesRecord;
  if (pSampRecord == NULL)              // S'il n'y a plus d'échantillons à enregistrer
    pSampRecord = pPrevSampIn;          // Il y a un nouveau bloc disponible
  else if (pSampRecord == pCurrentSampIn)
    pSampRecord = pNextSampIn;          // On avance le pointeur d'enregistrement
  else if (bRecording and pSampRecord == pCurrentSampIn) // En enregistrement et la mémorisation a rattrapé l'enregistrement
    bBufferLoss = true;                 // Perte de buffer
#if defined(__IMXRT1062__) // Teensy 4.1
  __enable_irq();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  SREG = sreg_backup;                   // On retaure les interruptions
#endif
  return pSave; 
}

//-------------------------------------------------------------------------
// Retourne le pointeur sur la zone d'enregistrement wav et prépare le pointeur pour le prochain enregistrement
// Permet le partage des pointeurs entre le loop et les IT
// NULL si aucun échantillons à enregistrer
int16_t *CRecorder::GetpRecordSample()
{
  int16_t *pRecord = NULL;              // Préparation du retour
#if defined(__IMXRT1062__) // Teensy 4.1
  __disable_irq();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  unsigned char sreg_backup = SREG;     // Sauvegarde de l'état des interruptions
  cli();                                // On stoppe les interruptions
#endif
  if (pSampRecord != NULL)              // Test s'il y a des échantillons à enregistrer
  {
    pRecord = (int16_t *)pSampRecord;   // Oui, préparation du retour                    
    if (pSampRecord == pPrevSampIn)     // Test s'il y a encore des échantillons
      pSampRecord = NULL;               // Plus d'échantillons disponibles
    else
    {
      pSampRecord += MAXSAMPLE;         // Il y a encore des échantillons à enregistrer, on pousse d'un bloc
      if (pSampRecord > pSampMax)
        pSampRecord = (int16_t *)pSamplesRecord;
    }
  }
#if defined(__IMXRT1062__) // Teensy 4.1
  __enable_irq();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  SREG = sreg_backup;                   // On retaure les interruptions
#endif
  return pRecord;
}

//----------------------------------------------------
// A la fin d'un enregistrement, initialise le pointeur du dernier élément à enregistrer
// au minimum du buffer de pré-enregistrement pour ne pas avoir de recouvrement
void CRecorder::SetSampRecordMin()
{
#if defined(__IMXRT1062__) // Teensy 4.1
  __disable_irq();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  unsigned char sreg_backup = SREG;     // Sauvegarde de l'état des interruptions
  cli();                                // On stoppe les interruptions
#endif
  pSampRecord = NULL;          // Le pointeur du futur enregistrement sera positionné au prochain bloc reçu
#if defined(__IMXRT1062__) // Teensy 4.1
  __enable_irq();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  SREG = sreg_backup;                   // On retaure les interruptions
#endif
}

//----------------------------------------------------
// Initialise la fréquence de coupure du filtre passe-haut
void CRecorder::SetFreqHighPass(
  int iHighPassFreq) // Fréquence du coupure en Hz
{
  // Mémorisation de la fréquence du filtre passe haut en Hz
  iFreqHighPass = iHighPassFreq;
}

//-------------------------------------------------------------------------
// Gestion de l'IT DMA lorsqu'un buffer d'acquisition est plein
// Test temps réel :
// Rappel du temps d'un buffer de 8192 : 250kHz=33.77ms, 384kHz=21.33ms, 500kHz=16.38ms
// PassiveRecorder : 7.4ms
// Hétérodyne      : 384/250=9.7ms, 500kHz=8.8ms
void CRecorder::OnDMA_ISR()
{
  //Serial.println("CRecorder::OnDMA_ISR");
#ifdef TESTTIMEISR
  unsigned long uiTime = micros();
#endif
#ifdef TESTLEDDMA
  if (iUtilLED == LEDITDMA)
    digitalWriteFast(LED_BUILTIN, HIGH);
  else if (iUtilLED == LEDDMA)
    digitalWriteFast(LED_BUILTIN, LOW);
#endif
  // Lecture de l'adresse courante d'écriture du DMA
  uint32_t daddr = pAcquisition->GetCurrentDMAaddr();
  // Par défaut, 1er buffer
  pBufferATraiter = (uint16_t *)samplesDMA;
  //Serial.printf("DMA ISR %d (%d)\n", daddr, pHalfDMASamples);
  if (daddr < (uint32_t)pHalfDMASamples)
  {
#ifdef TESTLEDDMA
    if (iUtilLED == LEDDMA)
      digitalWriteFast(LED_BUILTIN, HIGH);
#endif
    // Le DMA est dans le 1er buffer, on s'occupe du second
    pBufferATraiter = (uint16_t *)&(samplesDMA[MAXSAMPLE]);
    //Serial.println("Flip");
  }
  //else
  //  Serial.println("Flop");
#if defined(__IMXRT1062__) // Teensy 4.1
  // Sur Teensy 4.1, il est nécessaire de récupérer le buffer qui est éventuellement dans le cache
  // Delete data from the cache, without touching memory
  //
  // WARNING: This function is DANGEROUS!!  The address must be
  // 32 byte aligned and the size must be a multiple of 32 bytes.
  //
  // DO NOT USE this function with arbitrarily aligned data,
  // especially pointers from malloc() or C++ new.  The ARM cache
  // can only delete with granularity of 32 byte cache rows.  If
  // you attempt to delete improperly aligned data, any other
  // cached variables shared within the same 32 byte cache row(s)
  // will become collateral damage!
  //
  // If you wish to assure some variable or array or other data
  // is not cached, use arm_dcache_flush_delete().  This
  // arm_dcache_delete() should only be used for very special
  // cases like DMA buffers or hardware testing & benchmarks.
  //
  // See this forum thread for more detail:
  // https://forum.pjrc.com/threads/68100-BUG-in-arm_dcache_delete
  //
  // Normally arm_dcache_delete() is used before receiving data via
  // DMA or from bus-master peripherals which write to memory.  You
  // want to delete anything the cache may have stored, so your next
  // read is certain to access the physical memory.
  arm_dcache_delete((void*)pBufferATraiter, sizeof(uint16_t)*MAXSAMPLE);
  // Copie rapide dans un buffer
  memmove( samplesMem, (int16_t *)pBufferATraiter, sizeof(uint16_t)*MAXSAMPLE);
  pBufferATraiter = samplesMem;
#endif
  BufferProcessing();
#ifdef TESTLEDDMA
  if (iUtilLED == LEDITDMA)
    digitalWriteFast(LED_BUILTIN, LOW);
  else if (iUtilLED == LEDACTIV)
  {
    if (bActivite)
      digitalWriteFast(LED_BUILTIN, HIGH);
    else
      digitalWriteFast(LED_BUILTIN, LOW);
  }
#endif
#ifdef TESTTIMEISR
  uiTimeISRDMA += (micros()-uiTime);
  uiNbTimeISRDMA++;
  if (uiNbTimeISRDMA >= 100)
  {
    Serial.printf("ISR %dµs\n", uiTimeISRDMA/100);
    uiTimeISRDMA   = 0;
    uiNbTimeISRDMA = 0;
  }
#endif
#ifdef TESTMINMAX
  // Toutes les secondes environ, affichage des échantillons min et max et RAZ
  iNbBuffEch++;
  if (iNbBuffEch > 50)
  {
    Serial.printf("Ech min %d, max %d\n", iEchMin, iEchMax);
    iEchMin = 65000;
    iEchMax = -65000;
    iNbBuffEch = 0;
  }
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  // RAZ bit d'interruption du DMA
  pAcquisition->ClearDMAIsr();
#endif
  //Serial.println("CRecorder::OnDMA_ISR OK");
}

const uint16_t sqrt_integer_guess_table[33] = {
55109,
38968,
27555,
19484,
13778,
 9742,
 6889,
 4871,
 3445,
 2436,
 1723,
 1218,
  862,
  609,
  431,
  305,
  216,
  153,
  108,
   77,
   54,
   39,
   27,
   20,
   14,
   10,
    7,
    5,
    4,
    3,
    2,
    1,
    0
};

// computes ((a[15:0] * b[15:0]) + (a[31:16] * b[31:16]))
static inline int32_t multiply_16tx16t_add_16bx16b(uint32_t a, uint32_t b) __attribute__((always_inline, unused));
static inline int32_t multiply_16tx16t_add_16bx16b(uint32_t a, uint32_t b)
{
  int32_t out;
  asm volatile("smuad %0, %1, %2" : "=r" (out) : "r" (a), "r" (b));
  return out;
}

inline uint32_t sqrt_uint32_approx(uint32_t in) __attribute__((always_inline,unused));
inline uint32_t sqrt_uint32_approx(uint32_t in)
{
  uint32_t n = sqrt_integer_guess_table[__builtin_clz(in)];
  n = ((in / n) + n) / 2;
  n = ((in / n) + n) / 2;
  return n;
}

//-------------------------------------------------------------------------
// Processing under interruption of a full acquisition buffer
void CRecorder::BufferProcessing()
{
#ifdef TESTTEMPSREEL
  unsigned long uiTime = millis();
#endif

  // Traitement du buffer
  bActivite = false;
  bActivGauche = false;

  // Récupération du pointeur de sauvegarde des échantillons
  if (pSaveEch == NULL)
  {
    pSaveEch = GetpSaveSample();
    iSampleOut = 0;
  }

  // Buffer processing mode
  switch (iBufferProcessMode)
  {
  case BPM_SILENCE:       MicrophoneTestSilenceProcessing();        break;  // Extended microphone test, silence processing
  case BPM_SIGNAL:        MicrophoneTestSignalProcessing();         break;  // Extended microphone test, signal processing
  case BPM_NOISENOFLT:    NoiseSampleProcessWithoutFilter();        break;  // Noise processing without filter
  case BPM_NOISEFLT:      NoiseSampleProcessWithFilter();           break;  // Noise treatment with filter
  case BPM_NOISEFLTDECIM: NoiseSampleProcessWithFilterDecimation(); break;  // Noise treatment with filter with decimation
  case BPM_STEREO:        SampleProcessingStereo();                 break;  // Stereo processing without filter and decimation
  case BPM_STEREODECIM:   SampleProcessingStereoDecim();            break;  // Stereo processing with filter and decimation
  case BPM_MONOFLT:       SampleProcessingMonoFilter();             break;  // Mono treatment with filter
  case BPM_MONOFLTDECIM:  SampleProcessingMonoFilterDecim();        break;  // Mono treatment with filter and decimation
  default:
  case BPM_MONONOFLT:     SampleProcessingMonoWithoutFilter();      break;  // Mono treatment without filter
  }
  //Serial.printf("BufferProcessing %d µs\n", micros()-uiMicros);
#ifdef TESTTEMPSREEL
  if (bNoiseOK)
  {
    uiTimeTot += (millis() - uiTime);
    uiNbTraite++;
  }
#endif
}

//-------------------------------------------------------------------------
//! \brief Extended microphone test, silence processing
void CRecorder::MicrophoneTestSilenceProcessing()
{
  //Serial.println("CRecorder::MicrophoneTestSilenceProcessing");
  int16_t signedEchR, signedEchL;
  for (iSample=0; iSample<MAXSAMPLE;)                                     // For each sample (increment performed in the FFT preparation)
  {
    for (iFFT=0; iFFT<iFFTLen; iFFT++, iSample++)                         // FFT preparation without filter
    {
      if (bStereo)
      {
        signedEchL = pBufferATraiter[iSample] - 2048;                     // Signed left sample
        iSample++;
        signedEchR = pBufferATraiter[iSample] - 2048;                     // Signed right sample
        if (bRight)                                                       // In stereo, we do an FFT on the right samples then those on the left
          bufferCpxFFT[iFFT] = signedEchR;                                // Right Sample signed for the FFT (imaginary at 0 and real at the sample value)
        else
          bufferCpxFFT[iFFT] = signedEchL;                                // Left Sample signed for the FFT (imaginary at 0 and real at the sample value)
      }
      else
      {
        signedEchR = pBufferATraiter[iSample] - 2048;                     // Signed sample
        bufferCpxFFT[iFFT] = signedEchR;                                  // Sample signed for the FFT (imaginary at 0 and real at the sample value)
      }
    }
    arm_cfft_radix4_q15(&cpx_fft_inst, (q15_t *)bufferCpxFFT);            // FFT calculation
    for (iFFT=0; iFFT < FFTLEN/2; iFFT++)                                 // Exact calculation of magnitude
    {
      uint32_t tmp       = bufferCpxFFT[iFFT];                            // real & imag
      uint32_t magsq     = multiply_16tx16t_add_16bx16b(tmp, tmp);
      bufferCpxFFT[iFFT] = sqrt_uint32_approx(magsq);
    }
    TraitementSilence();                                                  // Silence treatment
  }
  pSaveEch = NULL;
}

//-------------------------------------------------------------------------
//! \brief Extended microphone test, signal processing
void CRecorder::MicrophoneTestSignalProcessing()
{
  //Serial.println("CRecorder::MicrophoneTestSignalProcessing");
  int16_t signedEchR, signedEchL;
  for (iSample=0; iSample<MAXSAMPLE;)                                     // For each sample (increment performed in the FFT preparation)
  {
    for (iFFT=0; iFFT<iFFTLen; iFFT++, iSample++)                         // FFT preparation without filter
    {
      if (bStereo)
      {
        signedEchL = pBufferATraiter[iSample] - 2048;                     // Signed left sample
        iSample++;
        signedEchR = pBufferATraiter[iSample] - 2048;                     // Signed right sample
        if (bRight)                                                       // In stereo, we do an FFT on the right samples then those on the left
          bufferCpxFFT[iFFT] = signedEchR;                                // Right Sample signed for the FFT (imaginary at 0 and real at the sample value)
        else
          bufferCpxFFT[iFFT] = signedEchL;                                // Left Sample signed for the FFT (imaginary at 0 and real at the sample value)
      }
      else
      {
        signedEchR = pBufferATraiter[iSample] - 2048;                     // Signed sample
        bufferCpxFFT[iFFT] = signedEchR;                                  // Sample signed for the FFT (imaginary at 0 and real at the sample value)
      }
    }
    arm_cfft_radix4_q15(&cpx_fft_inst, (q15_t *)bufferCpxFFT);            // FFT calculation
    for (iFFT=0; iFFT < FFTLEN/2; iFFT++)                                 // Exact calculation of magnitude
    {
      uint32_t tmp       = bufferCpxFFT[iFFT];                            // real & imag
      uint32_t magsq     = multiply_16tx16t_add_16bx16b(tmp, tmp);
      bufferCpxFFT[iFFT] = sqrt_uint32_approx(magsq);
    }
    TraitementSignal();                                                   // Signal treatment
  }
  pSaveEch = NULL;  
}
    
//-------------------------------------------------------------------------
//! \brief Sample processing for noise calculation without filter 
void CRecorder::NoiseSampleProcessWithoutFilter()
{
  //Serial.println("CRecorder::NoiseSampleProcessWithoutFilter");
  int16_t signedEch;
  for (iSample=0; iSample<MAXSAMPLE;)                                  // For each sample (increment performed in the FFT preparation)
  {
    for (iFFT=0; iFFT<iFFTLen; iFFT++, iSample++)                      // FFT preparation without filter
    {
      signedEch = HighPassDCRemovalR(pBufferATraiter[iSample] - 2048); // High pass filter and DC removal with signed sample
      pSaveEch[iSample]  = signedEch << shift;                         // Digital gain and save sample
      bufferCpxFFT[iFFT] = signedEch;                                  // Sample signed for the FFT (imaginary at 0 and real at the sample value)
      Traitementheterodyne(signedEch);                                 // Possible heterodyne treatment
    }
    arm_cfft_radix4_q15(&cpx_fft_inst, (q15_t *)bufferCpxFFT);         // FFT calculation
    for (iFFT=0; iFFT < FFTLEN/2; iFFT++)                              // Exact calculation of magnitude
    {
      uint32_t tmp       = bufferCpxFFT[iFFT];                         // real & imag
      uint32_t magsq     = multiply_16tx16t_add_16bx16b(tmp, tmp);
      bufferCpxFFT[iFFT] = sqrt_uint32_approx(magsq);
    }
    NoiseTreatment();                                                  // Noise treatment
  }
  pSaveEch = NULL;
}

//-------------------------------------------------------------------------
//! \brief Sample processing for noise calculation with filter without decimation
void CRecorder::NoiseSampleProcessWithFilter()
{
  //Serial.println("CRecorder::NoiseSampleProcessWithFilter");
  int16_t signedEch;
  for (iSample=0; iSample<MAXSAMPLE;)                                   // For each sample (increment made in the preparation of the filter)
  {
    for (iFFT=0; iFFT<iFFTLen;)                                         // FFT preparation with filter
    {
      int iInitialSample = iSample;                                     // Storing of the initial sample index
      for (iFIR=0; iFIR<BLOCK_SAMPLES; iFIR++, iSample++)               // Filter preparation with signed sample
        samplesFirIn[iFIR] = (q15_t)(pBufferATraiter[iSample] - 2048);
      firFilter.SetFIRFilter( (q15_t *)samplesFirIn, (q15_t *)samplesFirOut); // Application of the filter by the DSP
      iSample = iInitialSample;                                         // Restitution of the initial sample index
      for (iFIR=0; iFIR<BLOCK_SAMPLES; iFIR++, iFFT++, iSample++)       // FFT Preparation
      {
        signedEch = HighPassDCRemovalR((int16_t)samplesFirOut[iFIR]);   // High pass filter and DC removal with filtered signed sample
        pSaveEch[iSample] = signedEch << shift;                         // Digital gain and save sample
        bufferCpxFFT[iFFT] = signedEch;                                 // Sample signed for the FFT (imaginary at 0 and real at the sample value)
        Traitementheterodyne(signedEch);                                // Possible heterodyne treatment
      }
    }
    arm_cfft_radix4_q15(&cpx_fft_inst, (q15_t *)bufferCpxFFT);          // FFT calculation
    for (iFFT=0; iFFT < FFTLEN/2; iFFT++)                               // Exact calculation of magnitude
    {
      uint32_t tmp    = bufferCpxFFT[iFFT];                             // real & imag
      uint32_t magsq  = multiply_16tx16t_add_16bx16b(tmp, tmp);
      bufferCpxFFT[iFFT] = sqrt_uint32_approx(magsq);
    }
    NoiseTreatment();                                                   // Noise treatment
  }
  pSaveEch = NULL;
}

//-------------------------------------------------------------------------
//! \brief Sample processing for noise calculation with filter and decimation
void CRecorder::NoiseSampleProcessWithFilterDecimation()
{
  //Serial.println("CRecorder::NoiseSampleProcessWithFilterDecimation");
  int16_t signedEch;
  for (iSample=0; iSample<MAXSAMPLE; iSample++)                         // For each sample (iFIR, iFFT and iSampleOut are initialized at the start of acquisition)
  {
    samplesFirIn[iFIR++] = (q15_t)(pBufferATraiter[iSample] - 2048);    // Preparing the filter with a signed sample
    if (iFIR >= BLOCK_SAMPLES)                                          // There are enough samples for the filter
    {
      iFIR = 0;                                                         // For next filter preparation
      firFilter.SetFIRFilter( (q15_t *)samplesFirIn, (q15_t *)samplesFirOut); // Application of the filter by the DSP (Sample frequency 240kHz)
      for (int i=0; i<BLOCK_SAMPLES; i++)                               // For each filtered sample
      {
        if (iDecimationTable[iDecim++] != 0)                            // Decimation
        {
         signedEch = HighPassDCRemovalR((int16_t)samplesFirOut[i]);    // High pass filter and DC removal with filtered signed sample
          pSaveEch[iSampleOut++] = signedEch << shift;                  // Digital gain and save sample
          if (iSampleOut >= MAXSAMPLE)                                  // Test if the backup buffer is full
          {
            iSampleOut = 0;                                             // Next backup buffer
            pSaveEch = GetpSaveSample();
          }
          Traitementheterodyne(signedEch);                              // Possible heterodyne treatment
          bufferCpxFFT[iFFT++] = signedEch;                             // Sample signed for the FFT (imaginary at 0 and real at the sample value)
          if (iFFT >= iFFTLen)                                          // If FFT buffer full
          {
            iFFT = 0;                                                   // For next FFT preparation
            arm_cfft_radix4_q15(&cpx_fft_inst, (q15_t *)bufferCpxFFT);  // FFT calculation
            for (int i=0; i < FFTLEN/2; i++)                            // Exact calculation of magnitude
            {
              uint32_t tmp    = bufferCpxFFT[i];                        // real & imag
              uint32_t magsq  = multiply_16tx16t_add_16bx16b(tmp, tmp);
              bufferCpxFFT[i] = sqrt_uint32_approx(magsq);
            }
            NoiseTreatment();                                           // Noise treatment
          }
        }
        if (iDecim >= MAXTBDECIM)                                       // Decimation management
          iDecim = 0;
      }
    }
  }
}
    
//-------------------------------------------------------------------------
//! \brief Sample processing for stereo mode without filter
void CRecorder::SampleProcessingStereo()
{
  //Serial.println("CRecorder::SampleProcessingStereo");
  int16_t signedEchR, signedEchL;
  for (iSample=0; iSample<MAXSAMPLE;)                                     // For each sample (increment performed in the FFT preparation)
  {
    for (iFFT=0; iFFT<iFFTLen; iFFT++, iSample++)                         // FFT preparation without filter
    {
      signedEchL = HighPassDCRemovalL(pBufferATraiter[iSample] - 2048);   // High pass filter and DC removal with signed left sample
      pSaveEch[iSample]  = signedEchL << shift;                           // Digital gain and save first left sample
      iSample++;
      signedEchR = HighPassDCRemovalR(pBufferATraiter[iSample] - 2048);   // High pass filter and DC removal with signed right sample
      pSaveEch[iSample]  = signedEchR << shift;                           // Digital gain and save second right sample
      if (bRight)                                                         // In stereo, we do an FFT on the right samples then those on the left
        bufferCpxFFT[iFFT] = signedEchR;                                  // Right Sample signed for the FFT (imaginary at 0 and real at the sample value)
      else
        bufferCpxFFT[iFFT] = signedEchL;                                  // Left Sample signed for the FFT (imaginary at 0 and real at the sample value)
    }
    arm_cfft_radix4_q15(&cpx_fft_inst, (q15_t *)bufferCpxFFT);            // FFT calculation (use of the approximate magnitude to save computing time)
    if (bRight)
    {
      TraitementDetections();                                             // Testing whether right channel detections
      bRight = false;                                                     // Next FFT on the left channel samples
    }
    else
    {
      TraitementDetectionsGauche();                                       // Testing whether right channel detections
      bRight = true;                                                      // Next FFT on the left channel samples
    }
  }
  pSaveEch = NULL;  
}
    
//-------------------------------------------------------------------------
//! \brief SStereo processing with filter and decimation
void CRecorder::SampleProcessingStereoDecim()
{
  //Serial.println("CRecorder::SampleProcessingStereoDecim");
  int16_t signedEchR, signedEchL;
  for (iSample=0; iSample<MAXSAMPLE; iSample++)                         // For each sample (iFIR, iFFT and iSampleOut are initialized at the start of acquisition)
  {
    samplesFirInL[iFIR] = (q15_t)(pBufferATraiter[iSample++] - 2048);   // Preparing the filter with a signed left sample
    samplesFirIn[iFIR++]= (q15_t)(pBufferATraiter[iSample  ] - 2048);   // Preparing the filter with a signed right sample
    if (iFIR >= BLOCK_SAMPLES)                                          // There are enough samples for the filter
    {
      iFIR = 0;                                                         // For next filter preparation
      firFilterL.SetFIRFilter( (q15_t *)samplesFirInL, (q15_t *)samplesFirOutL); // Application of the filter by the DSP (Sample frequency 240kHz) on channel left
      firFilter .SetFIRFilter( (q15_t *)samplesFirIn,  (q15_t *)samplesFirOut);  // Application of the filter by the DSP (Sample frequency 240kHz) on channel right
      for (int i=0; i<BLOCK_SAMPLES; i++)                               // For each filtered sample
      {
        if (iDecimationTable[iDecim++] != 0)                            // Decimation
        {
          signedEchL = HighPassDCRemovalL((int16_t)samplesFirOutL[i]);  // High pass filter and DC removal with filtered signed left sample
          signedEchR = HighPassDCRemovalR((int16_t)samplesFirOut [i]);  // High pass filter and DC removal with filtered signed right sample
          pSaveEch[iSampleOut++] = signedEchR << shift;                 // Digital gain and save second right sample
          if (iSampleOut >= MAXSAMPLE)                                  // Test if the backup buffer is full
          {
            iSampleOut = 0;                                             // Next backup buffer
            pSaveEch = GetpSaveSample();
          }
          pSaveEch[iSampleOut++] = signedEchL << shift;                 // Digital gain and save first left sample
          if (iSampleOut >= MAXSAMPLE)                                  // Test if the backup buffer is full
          {
            iSampleOut = 0;                                             // Next backup buffer
            pSaveEch = GetpSaveSample();
          }
          if (bRight)                                                   // In stereo, we do an FFT on the right samples then those on the left
            bufferCpxFFT[iFFT++] = signedEchR;                          // Right sample signed for the FFT (imaginary at 0 and real at the sample value)
          else
            bufferCpxFFT[iFFT++] = signedEchL;                          // Left sample signed for the FFT (imaginary at 0 and real at the sample value)
          if (iFFT >= iFFTLen)                                          // If FFT buffer full
          {
            iFFT = 0;                                                   // For next FFT preparation
            arm_cfft_radix4_q15(&cpx_fft_inst, (q15_t *)bufferCpxFFT);  // FFT calculation (use of the approximate magnitude to save computing time)
            if (bRight)
            {
              TraitementDetections();                                   // Testing whether right channel detections
              bRight = false;                                           // Next FFT on the left channel samples
            }
            else
            {
              TraitementDetectionsGauche();                             // Testing whether right channel detections
              bRight = true;                                            // Next FFT on the left channel samples
            }
          }
        }
        if (iDecim >= MAXTBDECIM)                                       // Decimation management
          iDecim = 0;
      }
    }
  }
}
    
//-------------------------------------------------------------------------
//! \brief Sample processing for mono mode with filter without decimation
void CRecorder::SampleProcessingMonoFilter()
{
  //Serial.println("CRecorder::SampleProcessingMonoFilter");
  int16_t signedEch;
  for (iSample=0; iSample<MAXSAMPLE;)                                   // For each sample (increment made in the preparation of the filter)
  {
    for (iFFT=0; iFFT<iFFTLen;)                                         // FFT preparation with filter
    {
      int iInitialSample = iSample;                                     // Storing of the initial sample index
      for (iFIR=0; iFIR<BLOCK_SAMPLES; iFIR++, iSample++)               // Filter preparation with signed sample
        samplesFirIn[iFIR] = (q15_t)(pBufferATraiter[iSample] - 2048);
      firFilter.SetFIRFilter( (q15_t *)samplesFirIn, (q15_t *)samplesFirOut); // Application of the filter by the DSP
      iSample = iInitialSample;                                         // Restitution of the initial sample index
      for (iFIR=0; iFIR<BLOCK_SAMPLES; iFIR++, iFFT++, iSample++)       // FFT Preparation
      {
        signedEch = HighPassDCRemovalR((int16_t)samplesFirOut[iFIR]);   // High pass filter and DC removal with filtered signed sample
        pSaveEch[iSample] = signedEch << shift;                         // Digital gain and save sample
        bufferCpxFFT[iFFT] = signedEch;                                 // Sample signed for the FFT (imaginary at 0 and real at the sample value)
        Traitementheterodyne( signedEch);                               // Possible heterodyne treatment
      }
    }
    arm_cfft_radix4_q15(&cpx_fft_inst, (q15_t *)bufferCpxFFT);          // FFT calculation (use of the approximate magnitude to save computing time)
    TraitementDetections();                                             // Testing whether detections
  }
  pSaveEch = NULL;
}
    
//-------------------------------------------------------------------------
// Sample processing for mono mode with filter and decimation
// Acquisition sampling frequency = 240kHz 240kHz 240kHz
// Recording sample rate          =  96kHz  48kHz  24kHz
// Decimation                     =  2.5     5     10
/*
 * Decimation table
 * Index          0 1 2 3 4 5 6 7 8 9
 * Decimation   1 1 1 1 1 1 1 1 1 1 1
 * Decimation 2.5 1 0 1 0 0 1 0 1 0 0
 * Decimation   5 1 0 0 0 0 1 0 0 0 0
 * Decimation  10 1 0 0 0 0 0 0 0 0 0
 *
 * With decimation, we do not have a fair count of FFT and filter, we must treat differently
 * MAX_SAMPLE 8192 BLOCK_SAMPLES 128 FFTLEN 256
 * Decimation   1 32   FFT, 64   FIRFILTER
 * Decimation 2.5 12.8 FFT, 25.6 FIRFILTER
 * Decimation   5  6.4 FFT, 12.8 FIRFILTER
 * Decimation  10  3.2 FFT,  6.4 FIRFILTER
 */
void CRecorder::SampleProcessingMonoFilterDecim()
{
  //Serial.println("CRecorder::SampleProcessingMonoFilterDecim");
  int16_t signedEch;
  for (iSample=0; iSample<MAXSAMPLE; iSample++)                         // For each sample (iFIR, iFFT and iSampleOut are initialized at the start of acquisition)
  {
    samplesFirIn[iFIR++] = (q15_t)(pBufferATraiter[iSample] - 2048);    // Preparing the filter with a signed sample
    if (iFIR >= BLOCK_SAMPLES)                                          // There are enough samples for the filter
    {
      iFIR = 0;                                                         // For next filter preparation
      firFilter.SetFIRFilter( (q15_t *)samplesFirIn, (q15_t *)samplesFirOut); // Application of the filter by the DSP (Sample frequency 240kHz)
      for (int i=0; i<BLOCK_SAMPLES; i++)                               // For each filtered sample
      {
        if (iDecimationTable[iDecim++] != 0)                            // Decimation
        {
          signedEch = HighPassDCRemovalR((int16_t)samplesFirOut[i]);    // High pass filter and DC removal with filtered signed sample
          pSaveEch[iSampleOut++] = signedEch << shift;                  // Digital gain and save sample
          if (iSampleOut >= MAXSAMPLE)                                  // Test if the backup buffer is full
          {
            iSampleOut = 0;                                             // Next backup buffer
            pSaveEch = GetpSaveSample();
          }
          bufferCpxFFT[iFFT++] = signedEch;                             // Sample signed for the FFT (imaginary at 0 and real at the sample value)
          if (iFFT >= iFFTLen)                                          // If FFT buffer full
          {
            iFFT = 0;                                                   // For next FFT preparation
            arm_cfft_radix4_q15(&cpx_fft_inst, (q15_t *)bufferCpxFFT);  // FFT calculation (use of the approximate magnitude to save computing time)
            TraitementDetections();                                     // Testing whether detections
          }
          Traitementheterodyne( signedEch);                             // Possible heterodyne treatment
        }
        if (iDecim >= MAXTBDECIM)                                       // Decimation management
          iDecim = 0;
      }
    }
  }
}

//-------------------------------------------------------------------------
//! \brief Sample processing for mono mode without filter without decimation
void CRecorder::SampleProcessingMonoWithoutFilter()
{
  //Serial.println("CRecorder::SampleProcessingMonoWithoutFilter");
  int16_t signedEch;
  for (iSample=0; iSample<MAXSAMPLE;)                                  // For each sample (increment performed in the FFT preparation)
  {
    for (iFFT=0; iFFT<iFFTLen; iFFT++, iSample++)                      // FFT preparation without filter
    {
      signedEch = HighPassDCRemovalR(pBufferATraiter[iSample] - 2048); // High pass filter and DC removal with signed sample
      //signedEch = pBufferATraiter[iSample] - 2048; // High pass filter and DC removal with signed sample
#ifdef TESTDMAT41
      if (pBufferATraiter[iSample] == 5000)
        iNbErrorDMAT41++;
      pBufferATraiter[iSample] = 5000;
#endif
//Serial.printf("Sample[%d]=%d %d\n", iSample, pBufferATraiter[iSample], signedEch);
      pSaveEch[iSample]  = signedEch << shift;                         // Digital gain and save sample
      bufferCpxFFT[iFFT] = signedEch;                                  // Sample signed for the FFT (imaginary at 0 and real at the sample value)
      Traitementheterodyne(signedEch);                                 // Possible heterodyne treatment
    }
    arm_cfft_radix4_q15(&cpx_fft_inst, (q15_t *)bufferCpxFFT);         // FFT calculation (use of the approximate magnitude to save computing time)
    TraitementDetections();                                            // Testing whether detections
  }
  pSaveEch = NULL;
}
    
//-------------------------------------------------------------------------
// Processing of mono or right channel detections
void CRecorder::TraitementDetections()
{
/*#ifdef TESTQFC
    // Test provisoire des QFC
    bActivite = pCallAnalysis->FFTProcessing( iTbSeuilCnx, bufferCpxFFT);
#else*/
  int iNivMax = -1;
  int iCnx    = 0;
  for( int j=idxFMin; j<=idxFMax; j++)
  {
    // Sur ce canal, on enlève la dernière détection sur les 8 mémorisées, si bit de poids faible à 1, on décrémente la somme
    if ((TbBitsDetect[j] & 0x01) > 0 and TbNbDetect[j] > 0)
      TbNbDetect[j]--;
    // On décale les 8 dernières détections du canal pour perdre la dernière
    TbBitsDetect[j] = TbBitsDetect[j] >> 1;
    uint16_t iNiv = abs((int16_t)bufferCpxFFT[j]);
    if (iNiv > (uint16_t)iTbSeuilCnx[j])
    {
      // On incrémente la somme des 8 dernières détections du canal
      if (TbNbDetect[j] < 8)
        TbNbDetect[j]++;
      // Et on injecte un 1 sur le bit 7 pour mémoriser cette détection
      // 76543210
      // 10000000
      TbBitsDetect[j] |= 0x80;
      // Test si activité
      if (TbNbDetect[j] >= pParamsOp->iNbDetect)
        bActivite = true;
      if (iNiv > iNivMax)
      {
        //Serial.printf("Det Right on %d = %d\n", j, iNiv);
        // Mémorisation du niveau max et de sa fréquence
        iNivMax = iNiv;
        iCnx    = j;
      }
    }
    // Mémorisation du max du nombre de détections
    TbMemoNbDetect[j] = max( TbMemoNbDetect[j], TbNbDetect[j]);
  }
  // Mémorisation du niveau max du canal droit
  if (iNivMax > iNiveauMax)
  {
    // Mémorisation du niveau max et de sa fréquence du canal droit
    iNiveauMax = iNivMax;
    iCnxDetect = iCnx;
  }
//#endif
}

//-------------------------------------------------------------------------
// Processing of left channel detections
void CRecorder::TraitementDetectionsGauche()
{
  int iNivMax = -1;
  int iCnx    = 0;
  for( int j=idxFMin; j<=idxFMax; j++)
  {
    // Sur ce canal, on enlève la dernière détection sur les 8 mémorisées, si bit de poids faible à 1, on décrémente la somme
    if ((TbBitsDetectL[j] & 0x01) > 0 and TbNbDetectL[j] > 0)
      TbNbDetectL[j]--;
    // On décale les 8 dernières détections du canal pour perdre la dernière
    TbBitsDetectL[j] = TbBitsDetectL[j] >> 1;
    uint16_t iNiv = abs((int16_t)bufferCpxFFT[j]);
    if (iNiv > (uint16_t)iTbSeuilCnx[j])
    {
      // On incrémente la somme des 8 dernières détections du canal
      if (TbNbDetectL[j] < 8)
        TbNbDetectL[j]++;
      // Et on injecte un 1 sur le bit 7 pour mémoriser cette détection
      // 76543210
      // 10000000
      TbBitsDetectL[j] |= 0x80;
      // Test si activité
      if (TbNbDetectL[j] >= pParamsOp->iNbDetect)
      {
        bActivite = true;
        bActivGauche = true;
      }
      if (iNiv > iNivMax)
      {
        //Serial.printf("Det Left on %d = %d\n", j, iNiv);
        // Mémorisation du niveau max et de sa fréquence
        iNivMax = iNiv;
        iCnx    = j;
      }
    }
    // Mémorisation du max du nombre de détections
    TbMemoNbDetectL[j] = max( TbMemoNbDetectL[j], TbNbDetectL[j]);
  }
  // Mémorisation du niveau max en fonction du canal
  if (iNivMax > iNiveauMaxL)
  {
    // On vient de traiter le canal gauche, mémorisation du niveau max et de sa fréquence du canal gauche
    iNiveauMaxL = iNivMax;
    iCnxDetectL = iCnx;
  }
}

//-------------------------------------------------------------------------
// Traitements dans la boucle principale du programme
void CRecorder::OnLoop()
{
  //Serial.println("CRecorder::OnLoop");
  bool bWavefileError = false;
#ifdef TESTTEMPSREEL
  unsigned long uiTimeL = millis();
#endif
  // On teste une éventuelle perte de buffer
  if (bModeDebug and bBufferLoss)            
  {
    // Perte du temps réel !
    if (bRecording)
    {
      if (iNbEchLastA >= iNbMinEch)
      {
    		// On stoppe l'enregistrement pour partir sur de bonnes bases
    		StopRecording();
    		LogFile::AddLog( LLOG, txtLogBufferLossR[CModeGeneric::GetCommonsParams()->iLanguage]);
      }
      // else, At the beginning of recording, the losses of buffers are frequent, especially on AR. We stop only when the minimum duration is reached.	  
    }
    else
      LogFile::AddLog( LLOG, txtLogBufferLossA[CModeGeneric::GetCommonsParams()->iLanguage]);
  }
  bBufferLoss = false;
#ifdef TESTDMAT41
  if (iNbErrorDMAT41 >= 1000)
  {
#if defined(__IMXRT1062__) // Teensy 4.1
    __disable_irq();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
    unsigned char sreg_backup = SREG;     // Sauvegarde de l'état des interruptions
    cli();                                // On stoppe les interruptions
#endif
    iNbErrorDMAT41 = 0;
#if defined(__IMXRT1062__) // Teensy 4.1
    __enable_irq();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
    SREG = sreg_backup;                   // On retaure les interruptions
#endif
    LogFile::AddLog( LLOG, "**** 100 erreurs buffer DMA T4.1");
  }
 #endif
  if (pAcquisition != NULL)
  {
    // On teste une éventuelle erreur DMA
    uint32_t DMAErr = pAcquisition->GetDMAError();
    if (DMAErr > 0)
      OnErreurDMA( DMAErr);
  }
  if (bStarting)
  {
    // On mémorise l'activité éventuelle
    bool bNewActivite = bActivite;
    bActivite = false;
    // S'il y a de l'activité, en enregistrement auto et pas d'enregistrement en cours
    if (bNewActivite and bAutorecord and !bRecording)
      // Création d'un nouveau fichier wav et enregistrement
      StartRecording();
    // Si enregistrement en cours
    if (bRecording)
    {
      // Récupération éventuelle du buffer d'enregistrement
      int16_t *pRec = GetpRecordSample();
      if (pRec != NULL)
      {
#ifdef TESTWRITEFILE
        unsigned long uiTA = micros();
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
        //__disable_irq();
#endif
        // Mémorisation des échantillons
        if (wavefile.WavfileWrite( pRec, MAXSAMPLE) == 0)
        {
          bWavefileError = true;
          iNbWavefileError++;
          LogFile::AddLog( LLOG, "CRecorder::OnLoop error %d on wavefile.WavfileWrite", sd.card()->errorCode());
        }
#if defined(__IMXRT1062__) // Teensy 4.1
        //__enable_irq();
#endif
#ifdef TESTWRITEFILE
        unsigned long uiTB = micros();
        Serial.printf("WavfileWrite %d µs\n", uiTB-uiTA);
#endif
        iTotalEchRec += MAXSAMPLE;
        if (bNewActivite)
          iNbEchLastA  = 0;             // RAZ de la durée min en présence d'activité
        else
          iNbEchLastA  += MAXSAMPLE;    // Incrément de la durée min en absence d'activité
        if (bAutorecord)
        {
          //Serial.printf("bNewActivite %d, iTotalEchRec %d, iNbEchLastA %d\n", bNewActivite, iTotalEchRec, iNbEchLastA);
          // Test si durée max atteinte
          if (iTotalEchRec >= iNbMaxEch)
          {
            // Fin de l'enregistrement
            //Serial.printf("Stop sur iNbMaxEch, durée %04.1fs, iTotalEchRec %d\n", wavefile.GetRecordDuration(), iTotalEchRec);
            StopRecording();
          }
          // Test si durée min sans activité atteinte
          else if (!bNewActivite and iNbEchLastA >= iNbMinEch)
          {
            // Fin de l'enregistrement
            //Serial.printf("Stop sur iNbMinEch, bNewActivite %d, durée %04.1fs, iTotalEchRec %d, iNbEchLastA %d\n", bNewActivite, wavefile.GetRecordDuration(), iTotalEchRec, iNbEchLastA);
            StopRecording();
          }
        }
        else if (iTotalEchRec >= iNbMaxEch)
          // Fin de l'enregistrement
          StopRecording();
        if (bWavefileError)
        {
          // Fin de l'enregistrement sur erreur écriture
          StopRecording();
          Serial.printf("Erreur Write mode %d\n", pParamsOp->iModeRecord);
          if (pParamsOp->iModeRecord != MSYNCHRO) // En synchro la LED reste utilisée pour les moments d'enregistrement
            // Allume la LED de la carte processeur pour information
            digitalWriteFast(LED_BUILTIN, HIGH);
        }
      }
    }
  }
#ifdef TESTTEMPSREEL
  if (bStarting)
  {
    unsigned long uiTLoop = millis() - uiTimeL;
    if (uiTLoop > uiMaxLoop)
      uiMaxLoop = uiTLoop;
    uiTimeLoop += uiTLoop;
    uiNbLoop++;
  }
#endif
  //Serial.println("CRecorder::OnLoop OK");
}

//----------------------------------------------------
// Retourne la durée de l'enregistrement courant en secondes
uint32_t CRecorder::GetRecordDuration()
{
  if (bRecording)
    return (uint32_t)wavefile.GetRecordDuration();
  else
    return 0;
}

#if defined(__IMXRT1062__) // Teensy 4.1
/*------------------------------------------------------------------------------------------------------
 * Routine d'interruption du DMA d'acquisition, un buffer d'échantillons est disponible
 * Ne pas passer trop de temps dans le traitement d'une IT
 *------------------------------------------------------------------------------------------------------*/
void dma_T41_isr()
{
  // Appel de la fonction de traitement
  CGenericRecorder::pInstance->OnDMA_ISR();
}
#endif

unsigned long uiTimeISRZ = 0;

#if defined(__MK66FX1M0__) // Teensy 3.6
/*------------------------------------------------------------------------------------------------------
 * Routine d'interruption du DMA d'acquisition, un buffer d'échantillons est disponible
 * Ne pas passer trop de temps dans le traitement d'une IT
 *------------------------------------------------------------------------------------------------------*/
void dma_ch2_isr()
{
  /*if ((DMA_INT & 4) == 0)
  {
    if (uiTimeISRZ != 0)
      Serial.printf("dma_ch2_isr ! DMA_INT %d, %d\n", micros()-uiTimeISRZ);
    uiTimeISRZ = micros();
  }
  //Serial.println("dma_ch2_isr");
  if ((DMA_INT & 4) != 0)  // Only channel 2
  {*/
#ifdef TESTPERIODISR
    unsigned long uiTime = micros();
    if (uiTimeISRDMA != 0)
      Serial.printf("OnDMA_ISR %d µs, DMA_INT %d\n", uiTime - uiTimeISRDMA, DMA_INT);
    else
      Serial.printf("OnDMA_ISR 1er IT, DMA_INT %d\n", DMA_INT);
    uiTimeISRDMA = uiTime;
#endif
    //Serial.println("dma_ch2_isr");
    // Appel de la fonction de traitement
    CGenericRecorder::pInstance->OnDMA_ISR();
    // RAZ bit d'interruption du canal 2
    DMA_CINT = DMA_CINT_CINT(2); // (24.3.12) Clear interrupt request register
  //}
}
#endif

//----------------------------------------------------
// Initialisation du filtre éventuel
// Retourne false en cas d'erreur
bool CRecorder::InitFiltre()
{
  //Serial.printf("CRecorder::InitFiltre uiFeWav %d uiFeAcq %d\n", uiFeWav, uiFeAcq);
  bool bOK = true;
  if (pParamsOp->bFiltre)
  {
    // Init des coefficients de type q15_t
    int iNTaps = 32;
    const int16_t *newCoefs = NULL;
    if (pParamsOp->iModeRecord == AUDIOREC)
    {
      //Serial.println("AUDIOREC");
      if (F_CPU < 144000000)
      {
        iNTaps = 32;
        switch (pParamsOp->uiFeA)
        {
        case FE96KHZ : newCoefs = F240kHzMur48kHz32T;  break;
        case FE48KHZ : newCoefs = F240kHzMur24kHz32T;  break;
        case FE24KHZ : newCoefs = F240kHzMur12kHz32T;  break;
        }
        
      }
      else
      {
        iNTaps = 64;
        switch (pParamsOp->uiFeA)
        {
        case FE96KHZ : newCoefs = F240kHzMur48kHz64T;  break;
        case FE48KHZ : newCoefs = F240kHzMur24kHz64T;  break;
        case FE24KHZ : newCoefs = F240kHzMur12kHz64T;  break;
        }
      }
    }
    else
    {
      switch (pParamsOp->uiFe)
      {
      case FE384KHZ:
        //Serial.println("FE384KHZ");
        if (pParamsOp->iMicrophoneType == MTSPU0410 and F_CPU > 144000000)
        {
          newCoefs = F1_384k64T;
          iNTaps = 64;
        }
        else if (pParamsOp->iMicrophoneType == MTSPU0410 and F_CPU < 180000000)
        {
          newCoefs = F1_384k32T;
          iNTaps = 32;
        }
        else if (pParamsOp->iMicrophoneType == MTICS40730 and F_CPU > 144000000)
        {
          newCoefs = ICS_384k_64Taps;
          iNTaps = 64;
        }
        else if (pParamsOp->iMicrophoneType == MTICS40730 and F_CPU < 180000000)
        {
          newCoefs = ICS_384k_32Taps;
          iNTaps = 32;
        }
        break;
      case FE250KHZ:
        //Serial.println("FE250KHZ");
       iNTaps = 64;
       switch (pParamsOp->iMicrophoneType)
        {
        case MTSPU0410 : newCoefs = F1_250k32T;      iNTaps = 64; break;
        case MTICS40730: newCoefs = ICS_250k_64Taps; iNTaps = 64; break;
        }
        break;
      case FE96KHZ :
        if (F_CPU < 144000000)
         { newCoefs = F240kHzMur48kHz32T; iNTaps = 32; }
        else
         { newCoefs = F240kHzMur48kHz64T; iNTaps = 64; }
        break;
      case FE48KHZ :
        if (F_CPU < 144000000)
          { newCoefs = F240kHzMur24kHz32T; iNTaps = 32; }
        else
          { newCoefs = F240kHzMur24kHz64T; iNTaps = 64; }
        break;
      case FE24KHZ :
        if (F_CPU < 144000000)
          { newCoefs = F240kHzMur12kHz32T; iNTaps = 32; }
        else
          { newCoefs = F240kHzMur12kHz64T; iNTaps = 64; }
        break;
      }
    }
    if (newCoefs != NULL)
    {
      // Initialisation du filtre FIR
      bOK = firFilter.InitFIRFilter( iNTaps, newCoefs);
      // Si stéréo et filtre, initialisation du filtre FIR gauche
      if (bStereo)
        bOK = firFilterL.InitFIRFilter( iNTaps, newCoefs);
    }
    else
    {
      //Serial.println("newCoefs = NULL !");
      pParamsOp->bFiltre = false;
      bOK = false;
    }
  }
  return bOK;
}

//----------------------------------------------------
//! \brief Retourne les informations de détection max pour le mode test micro
//! \param *pNivMaxR Niveau max du canal droit
//! \param *pFrMaxR  Fréquence en kHz du maximum du canal droit
//! \param *pNivMaxL Niveau max du canal gauche
//! \param *pFrMaxL  Fréquence en kHz du maximum du canal gauche
void CRecorder::GetNiveauMax(
  int *pNivMaxR,
  int *pFrMaxR,
  int *pNivMaxL,
  int *pFrMaxL
  )
{
  //Serial.printf("CRecorder::GetNiveauMax D %d, G %d\n", iNiveauMax, iNiveauMaxL);
  // Par défaut, pas de détection
  *pNivMaxR = -120;
  *pNivMaxL = -120;
  *pFrMaxR  = 0;
  *pFrMaxL  = 0;
  
  // On stoppe les interruptions
#if defined(__IMXRT1062__) // Teensy 4.1
  __disable_irq();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  unsigned char sreg_backup = SREG;     // Sauvegarde de l'état des interruptions
  cli();                                // On stoppe les interruptions
#endif

  // Canal droit éventuel
  if (iNiveauMax > -1)
  {
    // Calcul du niveau max en dB
    *pNivMaxR   = GetNivDB(iNiveauMax) / 10;
    iNiveauMax  = -1;
    // Calcul de la fréquence en kHz
    float fFreq = (float)iCnxDetect * fCanal / 1000;
    *pFrMaxR    = (int)fFreq;
    //Serial.printf("CRecorder::GetNiveauMax Droit %ddB, %f\n", *pNivMaxR, fFreq);
  }
  // Canal gauche éventuel
  if (iNiveauMaxL > -1)
  {
    // Calcul du niveau max en dB
    *pNivMaxL   = GetNivDB(iNiveauMaxL) / 10;
    iNiveauMaxL = -1;
    // Calcul de la fréquence en kHz
    float fFreq = (float)iCnxDetectL * fCanal / 1000;
    *pFrMaxL    = (int)fFreq;
    //Serial.printf("CRecorder::GetNiveauMax Droit %ddB, %f\n", *pNivMaxL, fFreq);
  }
  // On retaure les interruptions
#if defined(__IMXRT1062__) // Teensy 4.1
  __enable_irq();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  SREG = sreg_backup;                   // On retaure les interruptions
#endif
}

//! For file number in Batcoder file name mode (set to 0 when switch on only)
int     CRecorder::iFileNumber = 0;

//-------------------------------------------------------------------------
//! \class CRecorderS
//! \brief Classe dérivée qui implémente la fonction d'enregistrement auto stéréo

//-------------------------------------------------------------------------
//! \brief Constructeur (initialisation de la classe)
//! \param pParams Pointeur sur les paramètres opérateur
//! \param iNbSmpBuffer Nombre de buffers de taille MAXSAMPLE pour l'enregistrement
CRecorderS::CRecorderS(
  ParamsOperator *pParams,
  int iNbSmpBuffer
  ): CRecorder( pParams, iNbSmpBuffer)
{
}

//-------------------------------------------------------------------------
//! \brief Création du gestionnaire d'acquisition
void CRecorderS::CreateAcquisition()
{
  switch( pParamsOp->iStereoRecMode)
  {
  case SMSTEREO:
    bStereo = true;
    // On double le nombre d'échantillons pour les durées min et max
    iNbMinEch *= 2;
    iNbMaxEch *= 2;
    // Droite pin A3, Gauche pin A2
    pAcquisition = new AcquisitionStereo( (uint16_t *)samplesDMA, MAXSAMPLE, PINA3_AUDIOR, PINA2_AUDIOL, 384000);
    Serial.println("CreateAcquisition AcquisitionStereo OK");
    break;
  case SMRIGHT:
    bStereo = false;
    // Droite pin A03
    pAcquisition = new Acquisition( (uint16_t *)samplesDMA, MAXSAMPLE, PINA3_AUDIOR, 384000);
    break;
  case SMLEFT:
    bStereo = false;
    // Gauche pin A2
    pAcquisition = new Acquisition( (uint16_t *)samplesDMA, MAXSAMPLE, PINA2_AUDIOL, 384000);
    break;
  }
}

#if defined(__IMXRT1062__) // Teensy 4.1
//-------------------------------------------------------------------------
//! \brief Traitements dans la boucle principale du programme
void CRecorderS::OnLoop()
{
  //Serial.println("CRecorderS::OnLoop A");
  if (pParamsOp->iStereoRecMode == SMSTEREO and pAcquisition != NULL and pAcquisition->IsAcquisition() and ((AcquisitionStereo *)pAcquisition)->IsBufferOK())
  {
    //Serial.println("CRecorderS::OnLoop B");
    // En stéréo sur Teensy 4.1, traitement du buffer hors IT
    // Lecture de l'adresse courante d'écriture du DMA
    uint32_t daddr = (uint32_t)pAcquisition->GetCurrentDMAaddr();
    // Par défaut, 1er buffer
    pBufferATraiter = (uint16_t *)samplesDMA;
    if (daddr < (uint32_t)pHalfDMASamples)
      // Le DMA est dans le 1er buffer, on s'occupe du second
      pBufferATraiter = (uint16_t *)&(samplesDMA[MAXSAMPLE]);
    BufferProcessing();
  }
  //Serial.println("CRecorderS::OnLoop C");
  // Traitement standard
  CRecorder::OnLoop();
  //Serial.println("CRecorderS::OnLoop D");
}
#endif

//-------------------------------------------------------------------------
// Classe rassemblant toutes les opérations nécessaire pour l'enregistrement
// - Acquisition (ADC via DMA cadencé par un PDB, traitement dans l'IT DMA)
// - Traitements (dans l'IT DMA)
// - FFT pour vérifier la présence d'un signal (dans l'IT DMA)
// - Enregistrement dans un fichier wav si activité (dans le loop du programme principal)
//-------------------------------------------------------------------------

#if defined(__IMXRT1062__) // Teensy 4.1, filtre hétérodyne sélectif
#ifdef SELFILTERH20KHZ
/*
 * Table des filtres sélectifs bande 20kHz
 */
const int16_t* TbSelectiveFilter20kHz[] = {
  FS000_020kHz, FS005_025kHz, FS010_030kHz, FS015_035kHz, FS020_040kHz, FS025_045kHz, FS030_050kHz, FS035_055kHz, FS040_060kHz, FS045_065kHz, FS050_070kHz, FS055_075kHz,
  FS060_080kHz, FS065_085kHz, FS070_090kHz, FS075_095kHz, FS080_100kHz, FS085_105kHz, FS090_110kHz, FS095_115kHz, FS100_120kHz, FS105_125kHz, FS110_130kHz
  };
#endif
#ifdef SELFILTERH10KHZ
/*
 * Table des filtres sélectifs bande 10kHz
 */
const int16_t* TbSelectiveFilter10kHz[] = {
  FS5000_15000Hz,    FS7500_17500Hz,    FS10000_20000Hz,   FS12500_22500Hz,   FS15000_25000Hz,  FS17500_27500Hz,  FS20000_30000Hz,  FS22500_32500Hz,  FS25000_35000Hz,   FS27500_37500Hz,
  FS30000_40000Hz,   FS32500_42500Hz,   FS35000_45000Hz,   FS37500_47500Hz,   FS40000_50000Hz,  FS42500_52500Hz,  FS45000_55000Hz,  FS47500_57500Hz,  FS50000_60000Hz,   FS52500_62500Hz,
  FS55000_65000Hz,   FS57500_67500Hz,   FS60000_70000Hz,   FS62500_72500Hz,   FS65000_75000Hz,  FS67500_77500Hz,  FS70000_80000Hz,  FS72500_82500Hz,  FS75000_85000Hz,   FS77500_87500Hz,
  FS80000_90000Hz,   FS82500_92500Hz,   FS85000_95000Hz,   FS87500_97500Hz,   FS90000_100000Hz, FS92500_102500Hz, FS95000_105000Hz, FS97500_107500Hz, FS100000_110000Hz, FS102500_112500Hz,
  FS105000_115000Hz, FS107500_117500Hz, FS110000_120000Hz, FS112500_122500Hz, FS115000_125000Hz
  };
#endif
#endif

// Constructeur (initialisation de la classe)
CRecorderH::CRecorderH(
  ParamsOperator *pParams, // Pointeur sur les paramètres opérateur
  int iNbSmpBuffer         // Nombre de buffers de taille MAXSAMPLE pour l'enregistrement
  )
  :CRecorder( pParams, iNbSmpBuffer),
  restitution( (uint16_t *)samplesDAC, MAXSAMPLE_HET, 384000)
{
  bHeterodyneActif = false;
  bInitRestitution = false;
#if defined(__MK66FX1M0__) // Teensy 3.6
  iMaxDMA  = MAXSAMPLE_HET * 2;
  iHalfDMA = MAXSAMPLE_HET;
#endif
#if defined(__IMXRT1062__) // Teensy 4.1, buffer stéréo
  iMaxDMA  = MAXSAMPLE_HET * 4;
  iHalfDMA = MAXSAMPLE_HET * 2;
#endif
  //Serial.printf("CRecorderH::CRecorderH Taille buffer filtre %d\n", BLOCK_SAMPLES);
}
  
//-------------------------------------------------------------------------
// Destructeur
CRecorderH::~CRecorderH()
{
}

//-------------------------------------------------------------------------
// Démarre l'acquisition
// Retourne true si démarrage OK
bool CRecorderH::StartAcquisition()
{
  /*if (CModeGeneric::bDebug)
    Serial.println("CRecorderH::StartAcquisition");*/
  // En hétérodyne, Fe au minimum de 250kHz
  if (uiFeWav < 250000)
  {
    uiFeWav = 250000;
    uiFeAcq = 250000;
  }
  // Appel de la méthode de base
  CRecorder::StartAcquisition();
  // Init de la fréquence d'échantillonnage hétérodyne
  switch (pParamsOp->uiFe)
  {
  case FE24KHZ : uiFeH = 250000; iModHet = 1; break;
  case FE48KHZ : uiFeH = 250000; iModHet = 1; break;
  case FE96KHZ : uiFeH = 250000; iModHet = 1; break;
  case FE192KHZ: uiFeH = 250000; iModHet = 1; break;
  case FE250KHZ: uiFeH = 250000; iModHet = 1; break;
  case FE500KHZ:
  case FE384KHZ: uiFeH = 384000; iModHet = 1; break;
  }
  Heterodyne.StartHeterodyne( uiFeH);
  // Fréquence hétérodyne à 0 (pas d'hétérodyne)
  fFreqH = 0.0;
  bHeterodyneActif = false;
  SetFrHeterodyne( fFreqH);
  iSampleDAC  = 0;
  idxDecim = 0;
  uiFiltFir   = 0;
  // Initialisation de la Fe de restitution
  if (uiFeH == 384000)
  {
    //iDecimation = 12;
    //iPlusDecim  = 4;
    iDecimation = 16; // 384000 / 16 = 24000 Hz
    iPlusDecim  = 0;
  }
  else
  {
    iDecimation = 10;
    iPlusDecim  = 2;
  }
  restitution.setFe( uiFeH/iDecimation);
  // Initialisation du filtre passe bas hétérodyne
  InitFiltreH();
  // RAZ du buffer du DAC
  memset( (void *)samplesDAC, 0, sizeof(samplesDAC));
  //Serial.printf("CRecorderH::StartAcquisition FeH %d, FeDAC %d, decimation %d\n", uiFeH, uiFeH/iDecimation, iDecimation);
#if defined(__IMXRT1062__) // Teensy 4.1, filtre hétérodyne sélectif
  // Préparation filtre sélectif
  bFiltreSel = false; //true;
  idxSelFilter = -1;
  idxSampleSelFilter = 0;
  InitFiltreSel();
#endif
  // On ne lance pas la restitution en attente des 1er échantillons disponibles
  bRestitution = false;
  return true;
}
  
//-------------------------------------------------------------------------
// Stoppe l'acquisition
// Retourne true si arrêt OK
bool CRecorderH::StopAcquisition()
{
  if (CModeGeneric::bDebug)
    Serial.println("CRecorderH::StopAcquisition");
  // Appel de la méthode de base
  CRecorder::StopAcquisition();
  // Fermeture éventuelle de l'hétérodyne
  // Stoppe la restitution
  restitution.stop();
  bRestitution = false;
  return true;
}

//-------------------------------------------------------------------------
// Gestion de l'IT DMA de lecture
void CRecorderH::OnITDMA_Reader()
{
  /*uint32_t daddr = (uint32_t)restitution.GetCurrentDMAaddr();
  if ((daddr < (uint32_t)&samplesDAC[iHalfDMA] and iSampleDAC < iHalfDMA) or (daddr > (uint32_t)&samplesDAC[iHalfDMA] and iSampleDAC >= iHalfDMA))
  {
    int iRestitution = daddr-(int)&samplesDAC[0];
    if (iRestitution <= iSampleDAC)
      Serial.println("La préparation est en retard");
    else
      Serial.println("La restitution est en retard");
  }*/
  //Serial.printf("OnITDMA_Reader, %d\n", micros());
  // Lecture de l'adresse courante de lecture du DMA
  uint32_t daddr = (uint32_t)restitution.GetCurrentDMAaddr();
  if ((daddr < (uint32_t)&samplesDAC[iHalfDMA] and iSampleDAC < iHalfDMA) or (daddr > (uint32_t)&samplesDAC[iHalfDMA] and iSampleDAC >= iHalfDMA))
  {
    // La restitution et la préparation sont dans le même buffer
    // |------------------|------------------|
    //    R       P          R       P        La préparation est en retard
    //    P       R          P       R        La restitution est en retard
    int iRestitution = daddr-(int)&samplesDAC[0];
    if (iRestitution <= iSampleDAC)
    {
      // La préparation est en retard, on ajoute des échantillons à 0
      int iNbSample = iMaxDMA - iSampleDAC;
      if (iNbSample > iHalfDMA)
        iNbSample -= iHalfDMA;
      iNbSample += 64;
      //Serial.printf("Plus %d\n", iNbSample);
      for (int j=0; j<iNbSample; j++)
      {
#if defined(__MK66FX1M0__) // Teensy 3.6
        samplesDAC[iSampleDAC] = 2048;
#endif
#if defined(__IMXRT1062__) // Teensy 4.1, buffer stéréo
        samplesDAC[iSampleDAC++] = 0;
        samplesDAC[iSampleDAC] = 0;
#endif
        // Indice du prochain échantillon
        iSampleDAC++;
        if (iSampleDAC >= iMaxDMA)
          iSampleDAC = 0;
      }
    }
    else
    {
      // La restitution est en retard, on retire des échantillons
      int iNbSample = iMaxDMA - iSampleDAC;
      if (iNbSample > iHalfDMA)
        iNbSample -= iHalfDMA;
      iNbSample -= 64;
      if (iNbSample < 0)
        iNbSample += iHalfDMA;
      //Serial.printf("Moins %d\n", iNbSample);
#if defined(__MK66FX1M0__) // Teensy 3.6
      iSampleDAC = iNbSample;
#endif
#if defined(__IMXRT1062__) // Teensy 4.1, buffer stéréo
      iSampleDAC = iNbSample;
#endif
    }
  }
#if defined(__IMXRT1062__) // Teensy 4.1, buffer stéréo
  // RAZ IT DMA
  restitution.ClearDMAIsr();
#endif
}

//-------------------------------------------------------------------------
//! \brief Traitement détections
void CRecorderH::TraitementDetections()
{
  // Appel de la fonction de base
  CRecorder::TraitementDetections();
  // Test si la restitution est stoppée
  if (!bRestitution and iSampleDAC >= iHalfDMA)
  {
    // On lance la restitution
    bRestitution = true;
    if (!bInitRestitution)
      restitution.start();
    else
      restitution.restart();
    bInitRestitution = true;
    if (iUtilLED == LEDBUFFER)
      digitalWriteFast( 13, LOW);
  }
}

//-------------------------------------------------------------------------
// Traitement hétérodyne de chaque échantillon (de base, ne fait rien)
// sample Echantillon à traiter (signé)
void CRecorderH::Traitementheterodyne(
	int16_t sample)
{
  bool bDecim = false;
#if defined(__IMXRT1062__) && (defined(SELFILTERH10KHZ) || defined(SELFILTERH20KHZ)) // Teensy 4.1, filtre hétérodyne sélectif
  if (bFiltreSel and idxSelFilter >= 0)
  {
    samplesFirSelIn[idxSampleSelFilter] = (q15_t)sample;
    idxSampleSelFilter++;
    if (idxSampleSelFilter >= BLOCK_SAMPLES)
    {
      //q15_t tmpSamples[BLOCK_SAMPLES];
      idxSampleSelFilter = 0;
      // Application du filtre sélectif
      SelectiveFilter.SetFIRFilter( (q15_t *)samplesFirSelIn, (q15_t *)samplesFirSelOut);
      for (int i=0; i<BLOCK_SAMPLES; i++)
        // Application du calcul hétérodyne aux échantillons filtrés
        samplesFirIn[i] = (q15_t)Heterodyne.CalculHeterodyne( (int16_t)samplesFirSelOut[i]);
      // Application du filtre passe bas par le DSP sur les échantillons du résultat hétérodyne avant décimation
      firFilter.SetFIRFilter( (q15_t *)samplesFirIn, (q15_t *)samplesFirOut);
      bDecim = true;
    }
  }
  else
  {
#endif
    // Calcul de l'échantillon et mémorisation pour le filtre avant décimation
    samplesFirIn[uiFiltFir] = (q15_t)Heterodyne.CalculHeterodyne( sample);
    uiFiltFir++;
    if (uiFiltFir >= BLOCK_SAMPLES)
    {
      uiFiltFir = 0;
      // 54µs filtre + décimation à 180MHz (40µs à 240MHz)
      // BLOCK_SAMPLES = 128
      // Application du filtre passe bas par le DSP sur les échantillons du résultat hétérodyne avant décimation
      firFilter.SetFIRFilter( (q15_t *)samplesFirIn, (q15_t *)samplesFirOut);
      bDecim = true;
    }
#if defined(__IMXRT1062__) && (defined(SELFILTERH10KHZ) || defined(SELFILTERH20KHZ)) // Teensy 4.1, filtre hétérodyne sélectif
  }
#endif
  if (bDecim)
  {
    // Mémo du réultat dans le buffer de lecture avec décimation par 10 ou 12 et passage dans le domaine non signé de -2048/+2047 à 0/4095
    for (int j=idxDecim; j<BLOCK_SAMPLES; j+=iDecimation)
    {
#if defined(__MK66FX1M0__) // Teensy 3.6, sortie mono sur DAC 12 bits (0 to 4096)
      samplesDAC[iSampleDAC] = samplesFirOut[j] + 2048;
#endif
#if defined(__IMXRT1062__) // Teensy 4.1, sortie stéréo sur I2S 16 bits (+/-32767)
      samplesDAC[iSampleDAC++] = samplesFirOut[j];
      samplesDAC[iSampleDAC  ] = samplesFirOut[j];
#endif
      // Indice du prochain échantillon
      iSampleDAC++;
#if defined(__MK66FX1M0__) // Teensy 3.6
      if (iSampleDAC >= iMaxDMA)
        iSampleDAC = 0;
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
      if (iSampleDAC >= iMaxDMA)
      {
        // Vide les données du cache vers la mémoire pour l'accès DMA
        arm_dcache_flush( &samplesDAC[iHalfDMA], sizeof(uint16_t)*MAXSAMPLE_HET*2);
        iSampleDAC = 0;
      }
      else if (iSampleDAC == iHalfDMA)
        // Vide les données du cache vers la mémoire pour l'accès DMA
        arm_dcache_flush( samplesDAC, sizeof(uint16_t)*MAXSAMPLE_HET*2);
#endif
    }
    // Le buffer du filtre FIR fait 128 et donc ne tombe pas juste pour une décimation par 10 ou 12
    idxDecim += iPlusDecim;
    if (idxDecim % iDecimation == 0)
      idxDecim = 0;
    //Serial.printf("idxDecim %d\n", idxDecim);
    //Serial.printf(" %d µs, BLOCK_SAMPLES %d\n", micros() - uiTime, BLOCK_SAMPLES);
  }
}

//----------------------------------------------------
// Initialise la fréquence hétérodyne
void CRecorderH::SetFrHeterodyne( float fH)
{
  // Initialise la fréquence hétérodyne
  fFreqH = fH;
  Heterodyne.SetFreqHeterodyne( fFreqH);
  // Test de la fréquence hétérodyne
  if (fFreqH <= 0.1)
    bHeterodyneActif = false;
  else
    bHeterodyneActif = true;
#if defined(__IMXRT1062__) // Teensy 4.1, filtre hétérodyne sélectif
  if (bFiltreSel)
    // Init du filtre sélectif
    InitFiltreSel();
#endif
}

#if defined(__IMXRT1062__) // Teensy 4.1, filtre hétérodyne sélectif
//----------------------------------------------------
//! \brief Initialisation du filtre sélectif
//! \return Retourne false en cas d'erreur
bool CRecorderH::InitFiltreSel()
{
  //Serial.println("CRecorderH::InitFiltreSel");
  bool bOK = true;
#if defined(SELFILTERH10KHZ) || defined(SELFILTERH20KHZ)
  // Calcul de l'indice du filtre utilisé en fonction de la fréquence hétérodyne
  if (fFreqH >= 10.0)
  {
#ifdef SELFILTERH20KHZ
    // Filtres de 20kHz à partir de 10kHz et par pas de 5kHz
    float fIdx = ((fFreqH - 10.0) / 5.0) - 0.5;
#endif
#ifdef SELFILTERH10KHZ
    // Filtres de 10kHz à partir de 10kHz et par pas de 2.5kHz
    float fIdx = ((fFreqH - 10.0) / 2.5);
#endif
    int iNewIdxSelFilter = (int)fIdx;
    if (iNewIdxSelFilter < 0)
      iNewIdxSelFilter = 0;
#ifdef SELFILTERH20KHZ
    else if (iNewIdxSelFilter > 22)
      iNewIdxSelFilter = 22;
#endif
#ifdef SELFILTERH10KHZ
    else if (iNewIdxSelFilter > 44)
      iNewIdxSelFilter = 44;
#endif    
    if (iNewIdxSelFilter != idxSelFilter)
    {
      idxSelFilter = iNewIdxSelFilter;
#ifdef SELFILTERH20KHZ
      const int16_t *newCoefs = TbSelectiveFilter20kHz[idxSelFilter];
#endif
#ifdef SELFILTERH10KHZ
      const int16_t *newCoefs = TbSelectiveFilter10kHz[idxSelFilter];
#endif
      // Initialisation du filtre FIR sélectif
      bOK = SelectiveFilter.InitFIRFilter( 64, newCoefs);
      //Serial.printf("CRecorderH::InitFiltreSel, fFreqH %fkHz, idxSelFilter %d\n", fFreqH, idxSelFilter);
    }
  }
#endif
  return bOK;
}

//----------------------------------------------------
//! \brief Initialisation de l'état sélectif ou non
//! \param bSel true si avec filtre sélectif
void CRecorderH::SetFiltreSel( bool bSel)
{
#if defined(__IMXRT1062__) && (defined(SELFILTERH10KHZ) || defined(SELFILTERH20KHZ)) // Teensy 4.1, filtre hétérodyne sélectif
  bFiltreSel = bSel;
  if (bFiltreSel)
    InitFiltreSel();
#endif
}
#endif

//----------------------------------------------------
// Initialisation du filtre passe bas du signal hétérodyne avant décimation
// Retourne false en cas d'erreur
bool CRecorderH::InitFiltreH()
{
  bool bOK = true;
#if defined(__MK66FX1M0__) // Teensy 3.6
  // Init des coefficients de type q15_t
  const double *newCoefs = FLowH384kHz;
  switch (pParamsOp->uiFe)
  {
  case FE250KHZ: newCoefs = FLowH250kHz; break;
  case FE500KHZ:
  case FE384KHZ: newCoefs = FLowH384kHz; break;
  }
  // Initialisation du filtre FIR
  bOK = firFilter.InitFIRFilter( 32, newCoefs);
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  // Init des coefficients de type q15_t
  const int16_t *newCoefs = FLowH384kHz64T;
  switch (pParamsOp->uiFe)
  {
  case FE250KHZ: newCoefs = FLowH250kHz64T; break;
  case FE500KHZ: newCoefs = FLowH500kHz64T; break;
  case FE384KHZ: newCoefs = FLowH384kHz64T; break;
  }
  // Initialisation du filtre FIR
  bOK = firFilter.InitFIRFilter( 64, newCoefs);
#endif
  return bOK;
}

//-------------------------------------------------------------------------
//! \class CHeterodyne
//! \brief Classe qui implémente le calcul hétérodyne

//-------------------------------------------------------------------------
//! \brief Constructeur (initialisation de la classe)
CHeterodyne::CHeterodyne()
{
  fFreqH = 40.0;
  fLevel = 0.9;
  uiFeH  = 384000;
  uiTimeMax = 0;
  iNbSampleMax = 0;
  iNivMax      = 0;
  fHCoef       = HAGC_CMIN;
  iSeuilNiv    = HAGC_THR;
  bAGCAuto     = true;
}

//-------------------------------------------------------------------------
//! \brief Démarre l'hétérodyne
//! \param uiFe Fréquence d'échantillonnage en Hz
void CHeterodyne::StartHeterodyne(
  uint32_t uiFe
  )
{
  //Serial.printf("CHeterodyne::StartHeterodyne uiFe %d\n", uiFe);
  uiFeH = uiFe;
  // En hétérodyne, Fe au minimum de 250kHz
  if (uiFeH < 250000)
    uiFeH = 250000;
  // Fréquence hétérodyne à 0 (pas d'hétérodyne)
  fFreqH = 0.0;
  SetFreqHeterodyne( fFreqH);
}

//-------------------------------------------------------------------------
//! \brief Initialisation de la fréquence hétérodyne
//! \param fH Fréquence hétérodyne en kHz
void CHeterodyne::SetFreqHeterodyne(
  float fH
  )
{
  //Serial.printf("CHeterodyne::SetFreqHeterodyne %f\n", fH);
  if (abs(fH-fFreqH) > 0.09 or millis() > uiTimeMax)
  {
    // Initialise la fréquence hétérodyne
    fFreqH = fH;
    // Test de la fréquence hétérodyne
    if (fFreqH > 0.1)
    {
      // Préparation des infos de génération de la porteuse
      float fAmplitude = MAXDAC * fLevel;
      float fPorteuse  = (fFreqH * 1000) / (float)uiFeH;
      ej2PiFoTe.Re  = cosf( DEUXPI * fPorteuse );
      ej2PiFoTe.Im  = sinf( DEUXPI * fPorteuse );
      ej2PiFoNTe.Re = cosf( 0.0 ) * fAmplitude ;
      ej2PiFoNTe.Im = sinf( 0.0 ) * fAmplitude ;
    }
    else
    {
      ej2PiFoTe.Re  = 0.0;
      ej2PiFoTe.Im  = 0.0;
      ej2PiFoNTe.Re = 0.0;
      ej2PiFoNTe.Im = 0.0;
    }
    uiTimeMax = millis() + 15000;
  }
}

//-------------------------------------------------------------------------
//! \brief Initialise l'AGC auto ou non
//! \param level Niveau du signal de 0.0 à 1.0
void CHeterodyne::SetAGC(
  bool bAuto) // true si AGC auto
{
  bAGCAuto = bAuto;
  if (bAGCAuto)
  {
    // Préparation de l'AGC auto
    iNivMax   = 0;
    fHCoef    = HAGC_CMIN;
    iSeuilNiv = HAGC_THR;
  }
}

//-------------------------------------------------------------------------
//! \class CRecorderA
//! \brief Classe dérivée qui implémente la fonction enregistrement audio (mode hétérodyne si Fe < 192kHz)

//-------------------------------------------------------------------------
//! \brief Constructeur (initialisation de la classe)
//! \param pParams Pointeur sur les paramètres opérateur
//! \param iNbSmpBuffer Nombre de buffers de taille MAXSAMPLE pour l'enregistrement
CRecorderA::CRecorderA(
  ParamsOperator *pParams,
  int iNbSmpBuffer
  )
  :CRecorder( pParams, iNbSmpBuffer),
  restitution( (uint16_t *)samplesAudio, MAXSAMPLE_AUDIOR, 384000)
{
#if defined(__MK66FX1M0__) // Teensy 3.6
  memset( (void *)samplesAudio, 0, sizeof(uint16_t)*MAXSAMPLE_AUDIOR*2);  // Buffer mono
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  memset( (void *)samplesAudio, 0, sizeof(uint16_t)*MAXSAMPLE_AUDIOR*4);  // Buffer stéréo
#endif
}

//-------------------------------------------------------------------------
//! \brief Destructeur
CRecorderA::~CRecorderA()
{
}

//-------------------------------------------------------------------------
//! \brief Démarre l'acquisition
//! Retourne true si démarrage OK
bool CRecorderA::StartAcquisition()
{
  if (CModeGeneric::bDebug)
    Serial.println("CRecorderA::StartAcquisition");
  // En audio, Fe au maximum de 192kHz
  if (uiFeWav > 192000)
  {
    uiFeWav = 192000;
    uiFeAcq = 192000;
  }
  // Init de la fréquence d'échantillonnage de restitution audio
  switch (pParamsOp->uiFeA)
  {
  case FE24KHZ : uiFeA =  24000; uiFeWav =  24000; break;
  case FE48KHZ : uiFeA =  48000; uiFeWav =  48000; break;
  case FE96KHZ : uiFeA =  96000; uiFeWav =  96000; break;
  case FE192KHZ: uiFeA = 192000; uiFeWav = 192000; break;
  default      : uiFeA = 192000; uiFeWav = 192000; break;
  }
  // Appel de la méthode de base
  CRecorder::StartAcquisition();
  iSampleA = 0;
  // Initialisation de la Fe de restitution
  restitution.setFe( uiFeA);
  // Lancement de la restitution
  restitution.start();
  //Serial.printf("CRecorderA::StartAcquisition uiFeA %d, uiFeAcq %d, uiFeWav %d\n", uiFeA, uiFeAcq, uiFeWav);
  return true;
}

//-------------------------------------------------------------------------
//! \brief Stoppe l'acquisition
//! Retourne true si arrêt OK
bool CRecorderA::StopAcquisition()
{
  if (CModeGeneric::bDebug)
    Serial.println("CRecorderA::StopAcquisition");
  // Appel de la méthode de base
  CRecorder::StopAcquisition();
  // Stoppe la restitution
  restitution.stop();
  return true;
}

//-------------------------------------------------------------------------
//! \brief Gestion de l'IT DMA de lecture
void CRecorderA::OnITDMA_Reader()
{
  // On ne fait rien, au pire, un buffer vide est joué en boucle
  //Serial.println("CRecorderA::OnITDMA_Reader");
  // RAZ IT DMA
  restitution.ClearDMAIsr();
}

//-------------------------------------------------------------------------
//! \brief Traitement hétérodyne de chaque échantillon (de base, ne fait rien)
//! \param sample Echantillon à traiter
void CRecorderA::Traitementheterodyne(
  int16_t sample)
{
  // Les échantillons sont directement restitués sur le DAC
  // Mémo réultat dans le buffer de lecture et passage dans le domaine non signé de -2048/+2047 à 0/4095
#if defined(__MK66FX1M0__) // Teensy 3.6, sortie mono sur DAC 12 bits (0 to 4096)
  samplesAudio[iSampleA++] = sample + 2048;
  if (iSampleA > MAXSAMPLE_AUDIOR*2)
    iSampleA = 0;
#endif
#if defined(__IMXRT1062__) // Teensy 4.1, sortie stéréo sur I2S 16 bits (+/-32767)
  samplesAudio[iSampleA++] = (sample << 4);
  samplesAudio[iSampleA++] = (sample << 4);
  if (iSampleA > MAXSAMPLE_AUDIOR*4)
    iSampleA = 0;
#endif
}

#if defined(__IMXRT1062__) // Teensy 4.1
  // Sur Teensy 4.1, définition des buffers DMA en RAM2, donc forcément static
  DMAMEM __attribute__((aligned(32))) int16_t CRecorderA::samplesAudio[MAXSAMPLE_READ*4];
#endif
