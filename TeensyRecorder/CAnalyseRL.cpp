/* 
 * File:   CAnalyseRL.cpp
   PassiveRecorder Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 /*
  * Classes :
  * - CAnalyzerRL
  *   - CFCAnalyzerRL
  *   - CQFCAnalyzerRL
  *   - CFMAnalyzerRL
  */
#include "CAnalyseRL.h"
#include "CRecorder.h"
#include "ModesModifiers.h"

//-------------------------------------------------------------------------
// Generic class for analyzing cries

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CAnalyzerRL::CAnalyzerRL()
{
  // Duration of FFT samples (4ms if FFT 1024 without zeropadding, 2ms with zeropadding 512 and 1ms if zeropadding 256)
#ifdef TESTRL256
  // Sans mode Zero padding et FFT 256 points
  iTimeFFTRL = 1;
#else
  #if defined(__MK66FX1M0__) // Teensy 3.6
    if (F_CPU < 50000000)
      // Sans mode Zero padding
      iTimeFFTRL = 4;
    else
      // Avec zeropadding 256
      iTimeFFTRL = 1;
  #endif
  #if defined(__IMXRT1062__) // Teensy 4.1
    // Avec zeropadding 256
    iTimeFFTRL = 1;
  #endif
#endif
}

//-------------------------------------------------------------------------
// Destructor
CAnalyzerRL::~CAnalyzerRL()
{
  
}

//-------------------------------------------------------------------------
// Initialization of the characteristics of the analysis
void CAnalyzerRL::InitializingAnalysis(
  uint32_t iFe,          // Sampling frequency 
  MonitoringBand *pBand, // Band characteristics
  bool bDebugDet         // For debugging bands
  )
{
  bDebugAll  = bDebugDet;
  //bDebugAll  = true;
  //bDebug     = true;
  // Save sampling frequency
  uiFe       = iFe;
  // Width of a channel in Hz
  iCanal     = (uiFe / 2) / (FFTLENRL / 2);
  // Calculation of the min and max indices of the band GR and min RE-PR to avoid hramonics
  float fIdx = 76.0 * 1000.0 / (float)iCanal;
  idxMinGR   = (short)fIdx;
  fIdx       = 86.0 * 1000.0 / (float)iCanal;
  idxMaxGR   = (short)fIdx;
  fIdx       = 99.0 * 1000.0 / (float)iCanal;
  idxMinREPR = (short)fIdx;
  // Save Band characteristics
  Band   = *pBand;
  // Calculation of the min and max indices of the band
  fIdx   = Band.fMin * 1000.0 / (float)iCanal;
  idxMin = (short)(fIdx + 0.5);
  fIdx   = Band.fMax * 1000.0 / (float)iCanal;
  idxMax = (short)(fIdx - 0.5);
  idxMin = max( idxMin, 0);
  idxMax = min( idxMax, (FFTLENRL/2)-1);
  // Reset Results of the current timer period
  RAZBeforeTimer();
  // Reset the current second
  memset( &CurrentSecond, 0, sizeof(CurrentSecond));
  CurrentSecond.iIdxMin = FFTLENRL;
  // Reset of the current minute
  memset( &CurrentMinute, 0, sizeof(CurrentMinute));
  CurrentMinute.iIdxMin = FFTLENRL;
  // Reset of the current hour
  memset( &CurrentHour, 0, sizeof(CurrentHour));
  CurrentHour.iIdxMin = FFTLENRL;
  // Reset of the current day
  memset( &CurrentDay, 0, sizeof(CurrentDay));
  CurrentDay.iIdxMin = FFTLENRL;
  //Serial.printf("CAnalyzerRL::InitializingAnalysis Bd %s, FMin %f %d, FMax %f %d, idxMinGR %d, idxMaxGR %d, idxMinREPR %d, MFFTLENRL %d\n", pBand->sName, pBand->fMin, idxMin, pBand->fMax, idxMax, idxMinGR, idxMaxGR, idxMinREPR, MFFTLENRL);
  iCurentSec = 0;
}

//-------------------------------------------------------------------------
// Initialization before the start of the analysis time of the timer period
void CAnalyzerRL::RAZBeforeTimer()
{
  // Reset FC Rhino Detection Charts
  memset( TbCurrentRhino, 0, sizeof(TbCurrentRhino));
  memset( TbSaveRhino   , 0, sizeof(TbSaveRhino));
  // Reset Results of the current timer period
  memset( &CurrentTimer , 0, sizeof(CurrentTimer));
  CurrentTimer.iIdxMin = FFTLENRL;
}

//-------------------------------------------------------------------------
// Harmonic processing for Rhinolophus cries
void CAnalyzerRL::HarmonicProcessing(
  volatile uint32_t *piTbSeuilCnx,  // Table of thresholds
  volatile  int32_t *pFFTResults    // Table of FFT levels
  )
{
  // For each Rhino band channel
  for (int c=idxMinGR; c<MFFTLENRL; c++)
  {
    if (c == idxMaxGR+1)
      // End of the band RE, jump directly to the channels of the RE band
      c = idxMinREPR;
    // Reading the magnitude of the 3 adjacent channels
    uint16_t iNivPrev = abs((int16_t)pFFTResults[c-1]);
    uint16_t iNiv     = abs((int16_t)pFFTResults[c]);
    uint16_t iNivNext = abs((int16_t)pFFTResults[c+1]);
    // Test if the magnitude exceeds the threshold of the 3 channels
    if (iNivPrev > (uint16_t)piTbSeuilCnx[c-1] or iNiv > (uint16_t)piTbSeuilCnx[c] or iNivNext > (uint16_t)piTbSeuilCnx[c+1])
    {
      //Serial.printf("HarmonicProcessing A Det Rhino iNiv %ddB, Seuil %ddB\n", CGenericRecorder::GetNivDB(iNiv)/10, CGenericRecorder::GetNivDB((uint16_t)piTbSeuilCnx[c])/10);
      // Detection on this channel (even if it was already the case before)
      TbCurrentRhino[c].bDetect = true;
      // Memo of the current max magnitude on this channel
      TbCurrentRhino[c].iMaxMag = max(TbCurrentRhino[c].iMaxMag, iNiv);
      // We cumulate the time of the FC
      TbCurrentRhino[c].iDuration += iTimeFFTRL;
    }
    else if (TbCurrentRhino[c].bDetect)
    {
      // No detection and previous detection, end of a CF
      // If it has a greater duration than one FFT and the previous one
      if (TbCurrentRhino[c].iDuration > iTimeFFTRL and TbSaveRhino[c].iDuration < TbCurrentRhino[c].iDuration)
      {
        // We memorize it
        TbSaveRhino[c].bDetect     = true;
        TbSaveRhino[c].iDuration   = TbCurrentRhino[c].iDuration;
        TbSaveRhino[c].iMaxMag     = TbCurrentRhino[c].iMaxMag;
      }
      //Serial.printf("HarmonicProcessing B Det Rhino %6.2fkHz, %ddB, %dms\n", CGenericRecorder::GetFreqIdx(c), CGenericRecorder::GetNivDB(TbSaveRhino[c].iMaxMag)/10, TbSaveRhino[c].iDuration);
      // Reset current FC
      TbCurrentRhino[c].bDetect  = false;
      TbCurrentRhino[c].iMaxMag  = 0;
      TbCurrentRhino[c].iDuration= 0;
    }
  }
}

//-------------------------------------------------------------------------
// Harmonic analysis on the timer period
void CAnalyzerRL::HarmonicAnalysis()
{
  // By default, no Rhino activity
  bRhino = false;
  // In the presence of an FC GR, we mask the channels likely to have harmonics GR
  // By default no mask
  memset( TbMsq, 0, sizeof(TbMsq));
  // For all GR channels
  int fMin, fMax;
  //Serial.printf("CAnalyzerRL::HarmonicAnalysis idxMinGR %d, idxMaxGR %d\n", idxMinGR, idxMaxGR);
  for (int c=idxMinGR; c<idxMaxGR; c++)
  {
    // If presence of an FC (more than 10ms, 3 FFT)
    if (TbSaveRhino[c].iDuration >= 10)
    {
      bRhino = true;
      // We mask 2kHz around f- (f / 2), f + (f / 2) 2f is +/- 2 channels
      // Be careful not to go below 0 and not to exceed FFTLENRL / 2
      // If the frequency exceeds FFTLENRL / 2, the folding is calculated in this way: MFFTLENRL - (2f - MFFTLENRL)
      fMin = c - (c/2);
      for (int cm=max(0, fMin-2); cm<min(fMin+3, MFFTLENRL-1); cm++)
        TbMsq[cm] = true;
      fMax = c + (c/2);
      for (int cm=max(0, fMax-2); cm<min(fMax+3, MFFTLENRL-1); cm++)
        TbMsq[cm] = true;
      fMax = 2 * c;
      if (fMax > MFFTLENRL)
        fMax = MFFTLENRL - (fMax- MFFTLENRL);
      for (int cm=max(0, fMax-2); cm<min(fMax+3, MFFTLENRL-1); cm++)
        TbMsq[cm] = true;
    }
  }
  // In the presence of an FC RE or PR, we mask the channels succeptible to have harmonics
  for (int c=idxMinREPR; c<MFFTLENRL; c++)
  {
    // If presence of an FC (more than 10ms) and unmasked channel
    if (TbSaveRhino[c].iDuration >= 10 and !TbMsq[c])
    {
      bRhino = true;
      // We mask 2kHz around f- (f / 2), f + (f / 2) 2f is +/- 2 channels
      // Be careful not to go below 0 and not to exceed (FFTLENRL / 2)-1
      // If the frequency exceeds FFTLENRL / 2, the folding is calculated in this way: MFFTLENRL - (2f - MFFTLENRL)
      fMin = c - (c/2);
      for (int cm=max(0, fMin-2); cm<min(fMin+3, MFFTLENRL-1); cm++)
        TbMsq[cm] = true;
      fMax = c + (c/2);
      if (fMax >= MFFTLENRL)
        fMax = min(MFFTLENRL-1, MFFTLENRL - (fMax- MFFTLENRL));
      for (int cm=max(0, fMax-2); cm<min(fMax+3, MFFTLENRL-1); cm++)
        TbMsq[cm] = true;
      fMax = 2 * c;
      if (fMax >= MFFTLENRL)
        fMax = min(MFFTLENRL-1, MFFTLENRL - (fMax- MFFTLENRL));
      for (int cm=max(0, fMax-2); cm<min(fMax+3, MFFTLENRL-1); cm++)
        TbMsq[cm] = true;
    }
  }
}

//-------------------------------------------------------------------------
// Processing FFT results
void CAnalyzerRL::FFTProcessing(
  volatile uint32_t *piTbSeuilCnx,  // Table of thresholds
  volatile  int32_t *pFFTResults    // Table of FFT levels
  )
{
  // Basically, do nothing 
}

//-------------------------------------------------------------------------
// Cries analysis in the timer period
void CAnalyzerRL::OnTimerProcessing()
{
  // Basically, do nothing
}

//-------------------------------------------------------------------------
// Update the current second every timer period
void CAnalyzerRL::UpdateCurrentSecond()
{
  // We accumulate the number of detections
  CurrentSecond.iNbDetAct += CurrentTimer.iNbDetAct;
  // We save the min and max indexes of activity
  CurrentSecond.iIdxMin = min(CurrentSecond.iIdxMin, CurrentTimer.iIdxMin);
  CurrentSecond.iIdxMax = max(CurrentSecond.iIdxMax, CurrentTimer.iIdxMax);
  // And we save the FME with the highest level
  if (CurrentSecond.iMagMax < CurrentTimer.iMagMax)
  {
    CurrentSecond.iMagMax    = CurrentTimer.iMagMax;
    CurrentSecond.iIdxMagMax = CurrentTimer.iIdxMagMax;
  }
}

//-------------------------------------------------------------------------
// Update the current minute every second
void CAnalyzerRL::UpdateCurrentMinute()
{
  // Update current second
  iCurentSec++;
  // If the second is active
  if (CurrentSecond.iNbDetAct >= Band.iNbDetections)
  {
    //ResultAnalyse memo = CurrentMinute;
    // We accumulate the number of positive seconds in one minute
    CurrentMinute.iNbSec++;
    CurrentMinute.iNbMin = 1;
    // If more than 5s after prévious contact, new contact
    if (iCurentSec > 5)
    {
      CurrentMinute.iNbCtx++;
      // Reset current second for a possible new contact after 5s
      iCurentSec = 0;
    }
    //Serial.printf("UpdateCurrentMinute Before/After iNbSec %d/%d, iNbCtx %d/%d, iNbMin %d/%d\n", memo.iNbSec, CurrentMinute.iNbSec, memo.iNbCtx, CurrentMinute.iNbCtx, memo.iNbMin, CurrentMinute.iNbMin);
    //if (bDebug)
    //  Serial.printf("UpdateCurrentMinute %s iNbDetAct %d, iNbDetections %d, iNbSec %d\n", Band.sName, CurrentSecond.iNbDetAct, Band.iNbDetections, CurrentMinute.iNbSec);
    // We save the min and max indexes of activity
    CurrentMinute.iIdxMin = min(CurrentMinute.iIdxMin, CurrentSecond.iIdxMin);
    CurrentMinute.iIdxMax = max(CurrentMinute.iIdxMax, CurrentSecond.iIdxMax);
    // And we save the FME with the highest level
    if (CurrentMinute.iMagMax < CurrentSecond.iMagMax)
    {
      CurrentMinute.iMagMax    = CurrentSecond.iMagMax;
      CurrentMinute.iIdxMagMax = CurrentSecond.iIdxMagMax;
    }
  }
  /*else if (bDebug and CurrentSecond.iNbDetAct > 0)
    Serial.printf("UpdateCurrentMinute %s iNbDetAct %d pas assez (%d)\n", Band.sName, CurrentSecond.iNbDetAct, Band.iNbDetections);*/
  // Reset the current second
  memset( &CurrentSecond, 0, sizeof(CurrentSecond));
  CurrentSecond.iIdxMin = FFTLENRL;
}

//-------------------------------------------------------------------------
// Storing the current minute
void CAnalyzerRL::StoreCurrentMinute()
{
  // Saving the results of the current minute
  SaveMinute = CurrentMinute;
  if (bDebug and CurrentMinute.iNbSec > 0)
    Serial.printf("StoreCurrentMinute Current/Save iNbSec %d/%d, iNbCtx %d/%d, iNbMin %d/%d\n", CurrentMinute.iNbSec, SaveMinute.iNbSec, CurrentMinute.iNbCtx, SaveMinute.iNbCtx, CurrentMinute.iNbMin, SaveMinute.iNbMin);
  // Reset of the current minute
  memset( &CurrentMinute, 0, sizeof(CurrentMinute));
  CurrentMinute.iIdxMin = FFTLENRL;
}

//-------------------------------------------------------------------------
// Update the current hour every minute
void CAnalyzerRL::UpdateCurrentHour()
{
  // If the minute is active
  if (SaveMinute.iNbSec > 0)
  {
    // We accumulate the number of positive seconds, contacts and minutes
    CurrentHour.iNbSec += SaveMinute.iNbSec;
    if (bDebug)
      Serial.printf("UpdateCurrentHour CurrentHour.iNbSec %d\n", CurrentHour.iNbSec);
    CurrentHour.iNbCtx += SaveMinute.iNbCtx;
    CurrentHour.iNbMin += SaveMinute.iNbMin;
    // We save the min and max indexes of activity
    CurrentHour.iIdxMin = min(CurrentHour.iIdxMin, SaveMinute.iIdxMin);
    CurrentHour.iIdxMax = max(CurrentHour.iIdxMax, SaveMinute.iIdxMax);
    // And we save the FME with the highest level
    if (CurrentHour.iMagMax < SaveMinute.iMagMax)
    {
      CurrentHour.iMagMax    = SaveMinute.iMagMax;
      CurrentHour.iIdxMagMax = SaveMinute.iIdxMagMax;
    }
  }
}

//-------------------------------------------------------------------------
// Storing the current hour
void CAnalyzerRL::StoreCurrentHour()
{
  // Saving the results of the current hour
  SaveHour = CurrentHour;
  //Serial.printf("StoreCurrentHour SaveHour.iNbSec %d\n", SaveHour.iNbSec);
  // Reset of the current hour
  memset( &CurrentHour, 0, sizeof(CurrentHour));
  CurrentHour.iIdxMin = FFTLENRL;
}

//-------------------------------------------------------------------------
// Update the current day
void CAnalyzerRL::UpdateCurrentDay()
{
  // If the minute is active
  if (SaveHour.iNbSec > 0)
  {
    // We accumulate the number of positive seconds, contacts and minutes
    CurrentDay.iNbSec += SaveHour.iNbSec;
    CurrentDay.iNbCtx += SaveHour.iNbCtx;
    CurrentDay.iNbMin += SaveHour.iNbMin;
    // We save the min and max indexes of activity
    CurrentDay.iIdxMin = min(CurrentDay.iIdxMin, SaveHour.iIdxMin);
    CurrentDay.iIdxMax = max(CurrentDay.iIdxMax, SaveHour.iIdxMax);
    // And we save the FME with the highest level
    if (CurrentDay.iMagMax < SaveHour.iMagMax)
    {
      CurrentDay.iMagMax    = SaveHour.iMagMax;
      CurrentDay.iIdxMagMax = SaveHour.iIdxMagMax;
    }
  }
}

//-------------------------------------------------------------------------
// Storing the current day
void CAnalyzerRL::StoreCurrentDay()
{
  // Saving the results of the current day
  SaveDay = CurrentDay;
  // Reset of the current day
  memset( &CurrentDay, 0, sizeof(CurrentDay));
  CurrentDay.iIdxMin = FFTLENRL;
}

//! Duration of FFT samples (4ms if FFT 1024 without zeropadding, 2ms with zeropadding 512 and 1ms if zeropadding 256)
int CAnalyzerRL::iTimeFFTRL = 0;

//-------------------------------------------------------------------------
// Class for FC Rhinolophus type cries analysis

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CFCAnalyzerRL::CFCAnalyzerRL() :CAnalyzerRL()
{
  // Nothing to do, all the initializations are done in CAnalyzerRL::InitializingAnalysis
  pTimerDetections  = NULL;
}

//-------------------------------------------------------------------------
// Destructor
CFCAnalyzerRL::~CFCAnalyzerRL()
{
  if (pTimerDetections != NULL)
    delete [] pTimerDetections;
}

//-------------------------------------------------------------------------
// Initialization of the characteristics of the analysis
void CFCAnalyzerRL::InitializingAnalysis(
  uint32_t iFe,          // Sampling frequency 
  MonitoringBand *pBand, // Band characteristics
  bool bDebugDet         // For debugging bands
  )
{
  // Calling the base class method
  CAnalyzerRL::InitializingAnalysis( iFe, pBand, bDebugDet);
  // Number of channels in the band
  iNbCnx = idxMax - idxMin + 1;
  pTimerDetections  = new DetectionFC[iNbCnx];
  // Reset FC Detection Charts
  memset( pTimerDetections, 0, sizeof(DetectionFC)*iNbCnx);
}

//-------------------------------------------------------------------------
// Initialization before the start of the analysis time of the timer period
void CFCAnalyzerRL::RAZBeforeTimer()
{
  // Calling the base class method
  CAnalyzerRL::RAZBeforeTimer();
  if (pTimerDetections != NULL)
    // Reset FC Detection Charts
    memset( pTimerDetections, 0, sizeof(DetectionFC)*iNbCnx);
}

//-------------------------------------------------------------------------
// Processing FFT results : Principle
// An FC is necessarily included on a single channel, its frequency is known with the precision of 244Hz
// On each channel, if the level exceeds the threshold, we increment the detections duration on this channel,
// the maximum level detected during the timer period
//
// Over the period of the analysis timer, we have the duration of detections and the maximum level
//
void CFCAnalyzerRL::FFTProcessing(
  volatile uint32_t *piTbSeuilCnx,  // Table of thresholds
  volatile  int32_t *pFFTResults    // Table of FFT levels
  )
{
  // For each channel of the band
  for (int c=idxMin+1, i=0; c<idxMax; c++, i++)
  {
    // Reading the magnitude of the 3 adjacent channels
    uint16_t iNivPrev = abs((int16_t)pFFTResults[c-1]);
    uint16_t iNiv     = abs((int16_t)pFFTResults[c]);
    uint16_t iNivNext = abs((int16_t)pFFTResults[c+1]);
    // Test if the magnitude exceeds the threshold of the 3 channels
    if (iNivPrev > (uint16_t)piTbSeuilCnx[c-1] or iNiv > (uint16_t)piTbSeuilCnx[c] or iNivNext > (uint16_t)piTbSeuilCnx[c+1])
    {
      // Detection on this channel
      // Memo of the current max magnitude on this channel
      pTimerDetections[i].iMaxMag = max(pTimerDetections[i].iMaxMag, iNiv);
      // We cumulate the time of the FC
      pTimerDetections[i].iDuration += iTimeFFTRL;
    }
  }
}

//-------------------------------------------------------------------------
// Cries analysis in the timer period : Principle
// Over a timer period, the longest FCs in each channel are kept.
// The analysis memorizes the min and max channels of detection of a compatible FC as well as the highest level and the corresponding frequency (FME).
void CFCAnalyzerRL::OnTimerProcessing()
{
  // Max current detection
  bool bDetect  = false;
  int iMaxMag   = 0;
  int iDuration = 0;
  int cDet      = 0;
  // For each channel of the band
  for (int c=idxMin, i=0; c<=idxMax; c++, i++)
  {
    // Test if unmasked channel, signal presence and time compatible
    if (TbMsq[c] == false and pTimerDetections[i].iDuration >= Band.iMinDuration)
    {
      // The band is active test if 
      bDetect  = true;
      // We memorize the detection with the highest level and the longest duration
      if (pTimerDetections[i].iMaxMag > iMaxMag)
      {
        cDet     = c;
        iMaxMag  = max( iMaxMag  , pTimerDetections[i].iMaxMag);
        iDuration= max( iDuration, pTimerDetections[i].iDuration);
        //if (bDebug)
        //  LogFile::AddLog( LLOG, "OnTimerProcessing A Good detection FCR %6.2fkHz, %dms, %ddB", CGenericRecorder::GetFreqIdx(c), pTbSaveFC[i].iDuration, CGenericRecorder::GetNivDB(pTbSaveFC[i].iMaxMag)/10);
      }
    }
    /*else if (bDebug and TbMsq[c] == false and pTbSaveFC[i].iDuration != 0)
      LogFile::AddLog( LLOG, "Duration FCR %6.2fkHz %dms not compatible (%d-%d ms)", CGenericRecorder::GetFreqIdx(c), pTbSaveFC[i].iDuration, Band.iMinDuration, Band.iMaxDuration); */
  }
  if (bDetect)
  {
    // New detection on current second
    CurrentSecond.iNbDetAct++;
    // We save the min and max indexes of activity
    CurrentSecond.iIdxMin = min(CurrentSecond.iIdxMin, cDet);
    CurrentSecond.iIdxMax = max(CurrentSecond.iIdxMax, cDet);
    // And we save the FME with the highest level
    if (CurrentSecond.iMagMax < iMaxMag)
    {
      CurrentSecond.iMagMax    = iMaxMag;
      CurrentSecond.iIdxMagMax = cDet;
    }
    //LogFile::AddLog( LLOG, "OnTimerProcessing B New detection FCR %d det, %6.2fkHz, %dms, %ddB", CurrentSecond.iNbDetAct, CGenericRecorder::GetFreqIdx(cDet), iDuration, CGenericRecorder::GetNivDB(iMaxMag)/10); 
  }
}


//-------------------------------------------------------------------------
// Class for FC type cries analysis

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CQFCAnalyzerRL::CQFCAnalyzerRL() :CAnalyzerRL()
{
  // Nothing to do, all the initializations are done in CAnalyzerRL::InitializingAnalysis
  pTbCurrentFC  = NULL;
  pTbSaveFC     = NULL;
}

//-------------------------------------------------------------------------
// Destructor
CQFCAnalyzerRL::~CQFCAnalyzerRL()
{
  if (pTbSaveFC != NULL)
    delete [] pTbSaveFC;
  if (pTbCurrentFC != NULL)
    delete [] pTbCurrentFC;
}

//-------------------------------------------------------------------------
// Initialization of the characteristics of the analysis
void CQFCAnalyzerRL::InitializingAnalysis(
  uint32_t iFe,          // Sampling frequency 
  MonitoringBand *pBand, // Band characteristics
  bool bDebugDet         // For debugging bands
  )
{
  // Calling the base class method
  CAnalyzerRL::InitializingAnalysis( iFe, pBand, bDebugDet);
  // Number of channels in the band
  iNbCnx = idxMax - idxMin + 1;
  pTbCurrentFC  = new DetectionFC[iNbCnx];
  pTbSaveFC     = new DetectionFC[iNbCnx];
  // Reset FC Detection Charts
  memset( pTbCurrentFC    , 0, sizeof(DetectionFC)*iNbCnx);
  memset( pTbSaveFC       , 0, sizeof(DetectionFC)*iNbCnx);
}

//-------------------------------------------------------------------------
// Initialization before the start of the analysis time of the timer period
void CQFCAnalyzerRL::RAZBeforeTimer()
{
  // Calling the base class method
  CAnalyzerRL::RAZBeforeTimer();
  if (pTbCurrentFC != NULL)
  {
    // Reset FC Detection Charts
    memset( pTbCurrentFC    , 0, sizeof(DetectionFC)*iNbCnx);
    memset( pTbSaveFC       , 0, sizeof(DetectionFC)*iNbCnx);
  }
}

//-------------------------------------------------------------------------
// Processing FFT results : Principle
// An FC is necessarily included on a single channel, its frequency is known with the precision of 244Hz
// On each channel, if the level exceeds the threshold, we start an FC on this channel, with the frequency (the index of the channel),
// the detected level and a duration of 4ms (duration of the samples of an FFT, normally 4,096ms but rounded to 4ms)
// On the next FFT, if the channel is still active, add 4ms to the duration and keep the loudest level
// If the channel is no longer active, the longest FC is memorized for this channel if one was already detected
//
// Over the period of the analysis timer, we have for all the channels, the longest CF
//
void CQFCAnalyzerRL::FFTProcessing(
  volatile uint32_t *piTbSeuilCnx,  // Table of thresholds
  volatile  int32_t *pFFTResults    // Table of FFT levels
  )
{
  // For each channel of the band
  for (int c=idxMin+1, i=0; c<idxMax; c++, i++)
  {
    // Reading the magnitude of the 3 adjacent channels
    uint16_t iNivPrev = abs((int16_t)pFFTResults[c-1]);
    uint16_t iNiv     = abs((int16_t)pFFTResults[c]);
    uint16_t iNivNext = abs((int16_t)pFFTResults[c+1]);
    // Test if the magnitude exceeds the threshold of the 3 channels
    if (iNivPrev > (uint16_t)piTbSeuilCnx[c-1] or iNiv > (uint16_t)piTbSeuilCnx[c] or iNivNext > (uint16_t)piTbSeuilCnx[c+1])
    {
      // Detection on this channel (even if it was already the case before)
      pTbCurrentFC[i].bDetect = true;
      // Memo of the current max magnitude on this channel
      pTbCurrentFC[i].iMaxMag = max(pTbCurrentFC[i].iMaxMag, iNiv);
      // We cumulate the time of the FC
      pTbCurrentFC[i].iDuration += iTimeFFTRL;
    }
    else if (pTbCurrentFC[i].bDetect)
    {
      // No detection, end of a CF
      // If the duration is compatible
      if (pTbCurrentFC[i].iDuration >= Band.iMinDuration and pTbCurrentFC[i].iDuration <= Band.iMaxDuration)
      {
        // We memorize it
        pTbSaveFC[i].bDetect     = true;
        pTbSaveFC[i].iDuration   = max(pTbSaveFC[i].iDuration, pTbCurrentFC[i].iDuration);
        pTbSaveFC[i].iMaxMag     = max(pTbSaveFC[i].iMaxMag  , pTbCurrentFC[i].iMaxMag);
        // We count the number of valid FC on this channel
        pTbSaveFC[i].iNbDetect++;
      }
      // Reset current FC
      pTbCurrentFC[i].bDetect  = false;
      pTbCurrentFC[i].iNbDetect= 0;
      pTbCurrentFC[i].iMaxMag  = 0;
      pTbCurrentFC[i].iDuration= 0;
    }
  }
}

//-------------------------------------------------------------------------
// Cries analysis in the timer period : Principle
// Over a timer period, the longest FCs in each channel are kept.
// The analysis memorizes the min and max channels of detection of a compatible FC as well as the highest level and the corresponding frequency (FME).
void CQFCAnalyzerRL::OnTimerProcessing()
{
  // Max current detection
  bool bDetect  = false;
  int iMaxMag   = 0;
  int iDuration = 0;
  int cDet      = 0;
  // For each channel of the band
  for (int c=idxMin, i=0; c<=idxMax; c++, i++)
  {
    // Test if unmasked channel and signal presence
    if (TbMsq[c] == false and pTbSaveFC[i].bDetect)
    {
      // The band is active test if 
      bDetect  = true;
      // We memorize the detection with the highest level
      if (pTbSaveFC[i].iMaxMag > iMaxMag)
      {
        cDet     = c;
        iMaxMag  = max( iMaxMag  , pTbSaveFC[i].iMaxMag);
        iDuration= max( iDuration, pTbSaveFC[i].iDuration);
        /*if (bDebug)
          LogFile::AddLog( LLOG, "Good detection FC %6.2fkHz, %dms, %ddB", CGenericRecorder::GetFreqIdx(c), pTbSaveFC[i].iDuration, CGenericRecorder::GetNivDB(pTbSaveFC[i].iMaxMag)/10);*/
      }
    }
    /*else if (bDebug and TbMsq[c] == false and pTbSaveFC[i].iDuration != 0)
      LogFile::AddLog( LLOG, "Duration FC %6.2fkHz %dms not compatible (%d-%d ms)", CGenericRecorder::GetFreqIdx(c), pTbSaveFC[i].iDuration, Band.iMinDuration, Band.iMaxDuration); */
  }
  if (bDetect)
  {
    // New detection on current second
    CurrentSecond.iNbDetAct++;
    // We save the min and max indexes of activity
    CurrentSecond.iIdxMin = min(CurrentSecond.iIdxMin, cDet);
    CurrentSecond.iIdxMax = max(CurrentSecond.iIdxMax, cDet);
    // And we save the FME with the highest level
    if (CurrentSecond.iMagMax < iMaxMag)
    {
      CurrentSecond.iMagMax    = iMaxMag;
      CurrentSecond.iIdxMagMax = cDet;
    }
    //LogFile::AddLog( LLOG, "New detection FC %d det, %6.2fkHz, %dms, %ddB", CurrentSecond.iNbDetAct, CGenericRecorder::GetFreqIdx(cDet), iDuration, CGenericRecorder::GetNivDB(iMaxMag)/10); 
  }
}

//-------------------------------------------------------------------------
// Class for FM type cries analysis

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CFMAnalyzerRL::CFMAnalyzerRL()
{
  // Nothing to do, all the initializations are done in CAnalyzerRL::InitializingAnalysis
}

//-------------------------------------------------------------------------
// Initialization before the start of the analysis time of the timer period
void CFMAnalyzerRL::RAZBeforeTimer()
{
  // Calling the base class method
  CAnalyzerRL::RAZBeforeTimer();
  // Reset the FM memory
  memset( &currentFM     , 0, sizeof(currentFM));
  memset( &maxFM         , 0, sizeof(maxFM));
}

int iPrint = 0;
//-------------------------------------------------------------------------
// Processing FFT results : Principle
// An FFT representing 4.096ms of samples, it is considered that a cry is included entirely in an FFT result
// If a cry straddles 2 FFTs, it will probably not be detected but, over 1s, it is highly likely that a cry will be included in an FFT
// So we start from the highest channel of the band to the weakest channel.
// If we have a channel that exceeds the threshold, we start an FM signal with FI (Initial Frequency)
// For the following channels, if they exceed the threshold, the FM signal is continued by memorizing the lowest channel FT (Terminal Frequency).
// We also memorize the higher level channel that will correspond to the FME (Frequency of Maximum Energy)
// Given that the entire signal of the cry is included in an FFT, it struggles to find energy on each channel, the signal of a channel being too short,
// To overcome this problem, we tolerate holes of 3 channels (732Hz). If a hole is greater than 3 channels, the FM signal is stopped
// So we have FI, FT, the width (FI-FT + 1), the max level and the FME
//
// During the timer analysis period, the largest FM signal is stored
//
void CFMAnalyzerRL::FFTProcessing(
  volatile uint32_t *piTbSeuilCnx,  // Table of thresholds
  volatile  int32_t *pFFTResults    // Table of FFT levels
  )
{
  bool bDetect = false;
  // Reset current FM info
  memset( &currentFM, 0, sizeof(DetectionFM));
  // For each channel of the band
  for (int c=idxMax; c>=idxMin; c--)
  {
    // Reading the magnitude of the channel
    uint16_t iNiv = abs((int16_t)pFFTResults[c]);
    // Test if detection on the channel
    if (iNiv > (uint16_t)piTbSeuilCnx[c])
    {
      if (iPrint < 30)
      {
        iPrint++;
        //Serial.printf("CFMAnalyzerRL::FFTProcessing Det canal %d, Niv %d (%d), Seuil %d (%d)\n", c, iNiv, CGenericRecorder::GetNivDB(iNiv)/10, piTbSeuilCnx[c], CGenericRecorder::GetNivDB(piTbSeuilCnx[c])/10);
      }
      // Detection on this channel
      if (!bDetect)
      {
        // Detection of a 1st channel, memo of the beginning of the FM
        currentFM.idxFI      = c;    // FM start channel index
        currentFM.idxFT      = c;    // End channel index of the FM
        currentFM.iNivMax    = iNiv; // Max level of the FM
        currentFM.idxNivMax  = c;    // Max level channel index
        bDetect  = true;
      }
      else
      {
        // Detection on a lower channel, memo of the end of the FM
        currentFM.idxFT      = c;    // End channel index of the FM
        if (iNiv > currentFM.iNivMax)
        {
          currentFM.iNivMax    = iNiv; // Max level of the FM
          currentFM.idxNivMax  = c;    // ax level channel index
        }
      }
    }
    else if (bDetect)
    {
      // No detection,
      // We tolerate a hole of 4 channels (976Hz), the detections are not necessarily consecutive
      if ((currentFM.idxFT - c) > 4)
      {
        // No more detection and previous detection, end of an FM
        // The largest width, the terminal frequency channel and the maximum level are stored
        int iWidth = currentFM.idxFI - currentFM.idxFT + 1;
        if (iWidth > maxFM.iWidth)
        {
          maxFM.idxFI      = currentFM.idxFI;     // FM start channel index
          maxFM.idxFT      = currentFM.idxFT;     // End channel index of the FM
          maxFM.iWidth     = iWidth;              // Width of the FM in number of channels
          maxFM.iNivMax    = currentFM.iNivMax;   // Max level of the FM
          maxFM.idxNivMax  = currentFM.idxNivMax; // Max level channel index
          /*if (bDebugAll)
          {
            float fWidth = (float)(maxFM.iWidth * iCanal) / 1000.0;
            LogFile::AddLog( LLOG, "maxFM A Width %6.2fkHz (%d), FI %6.2fkHz (%d), FT %6.2fkHz (%d), FME %6.2fkHz, %ddB\n", fWidth, maxFM.iWidth, CGenericRecorder::GetFreqIdx(maxFM.idxFI), maxFM.idxFI,
              CGenericRecorder::GetFreqIdx(maxFM.idxFT), maxFM.idxFT, CGenericRecorder::GetFreqIdx(maxFM.idxNivMax), CGenericRecorder::GetNivDB(maxFM.iNivMax)/10);
          }*/
        }
        // Reset of current detection
        bDetect = false;
        memset( &currentFM, 0, sizeof(DetectionFM));
      }
    }
  }
  // End of the channels of the band, if an active FM, it stops
  if (bDetect)
  {
    // No more detection and previous detection, end of an FM
    // The largest width, the terminal frequency channel and the maximum level are stored
    int iWidth = currentFM.idxFI - currentFM.idxFT + 1;
    if (iWidth > maxFM.iWidth)
    {
      maxFM.idxFI      = currentFM.idxFI;     // FM start channel index
      maxFM.idxFT      = currentFM.idxFT;     // End channel index of the FM
      maxFM.iWidth     = iWidth;              // Width of the FM in number of channels
      maxFM.iNivMax    = currentFM.iNivMax;   // Max level of the FM
      maxFM.idxNivMax  = currentFM.idxNivMax; // Max level channel index
      /*if (bDebugAll)
      {
        float fWidth = (float)(maxFM.iWidth * iCanal) / 1000.0;
        LogFile::AddLog( LLOG, "maxFM B Width %6.2fkHz (%d), FI %6.2fkHz (%d), FT %6.2fkHz (%d), FME %6.2fkHz, %ddB\n", fWidth, maxFM.iWidth, CGenericRecorder::GetFreqIdx(maxFM.idxFI), maxFM.idxFI,
          CGenericRecorder::GetFreqIdx(maxFM.idxFT), maxFM.idxFT, CGenericRecorder::GetFreqIdx(maxFM.idxNivMax), CGenericRecorder::GetNivDB(maxFM.iNivMax)/10);
      }*/
    }
  }
}

//-------------------------------------------------------------------------
// Cries analysis in the timer period : Principle
// If the width is compatible with the parameters of the analysis band,
// the max level, the FME channel and the IF and FT channels are memorized
void CFMAnalyzerRL::OnTimerProcessing()
{
  // Calculation of FM width in kHz
  float fWidth = (float)(maxFM.iWidth * iCanal) / 1000.0;
  // If there is Rhino activity, you do not memorize FM activity
  if (bRhino == false and fWidth >=  Band.fMinFWidth)
  {
    // The band is active
    // We cumulate the number of activities
    CurrentTimer.iNbDetAct++;
    // Active min and max channel memo
    CurrentTimer.iIdxMin    = maxFM.idxFT;
    CurrentTimer.iIdxMax    = maxFM.idxFI;
    //Memo of the max level
    CurrentTimer.iMagMax    = maxFM.iNivMax;
    CurrentTimer.iIdxMagMax = maxFM.idxNivMax;
    /*if (bDebug)
      LogFile::AddLog( LLOG, "Memo FM Width %6.2fkHz, FI %6.2fkHz, FT %6.2fkHz, FME %6.2fkHz, %ddB", fWidth, CGenericRecorder::GetFreqIdx(CurrentTimer.iIdxMax),
        CGenericRecorder::GetFreqIdx(CurrentTimer.iIdxMin), CGenericRecorder::GetFreqIdx(CurrentTimer.iIdxMagMax), CGenericRecorder::GetNivDB(CurrentTimer.iMagMax)/10);*/
  }
  /*else if (fWidth >= 1.0 and bDebug)
    Serial.printf("FM Width %6.2fkHz, FI %6.2fkHz, FT %6.2fkHz, FME %6.2fkHz, %ddB\n", fWidth, CGenericRecorder::GetFreqIdx(maxFM.idxFI),
      CGenericRecorder::GetFreqIdx(maxFM.idxFT), CGenericRecorder::GetFreqIdx(maxFM.idxNivMax), CGenericRecorder::GetNivDB(maxFM.iNivMax)/10);*/
}

// Static menbers of CAnalyzerRL
// Debug mode
bool        CAnalyzerRL::bDebug = false;
// Sampling frequency
uint32_t    CAnalyzerRL::uiFe;
// Width of a channel in Hz
int         CAnalyzerRL::iCanal;
// Min and max index of the GR band to avoid harmonics
short       CAnalyzerRL::idxMinGR, CAnalyzerRL::idxMaxGR;
// Min index of the RE-PR band, the max being MFFTLENRL
short       CAnalyzerRL::idxMinREPR;
// Detection tables of Rhinolophus for the treatment of harmonics
// Table of current FC detections
DetectionFC CAnalyzerRL::TbCurrentRhino[MFFTLENRL];
// Backup table for the longest CF on each channel
DetectionFC CAnalyzerRL::TbSaveRhino[MFFTLENRL];
// Activity presence indicator of Rhinolophus to block FM
bool        CAnalyzerRL::bRhino;
// Hidden channels to avoid GR, RE and PR harmonics
bool        CAnalyzerRL::TbMsq[MFFTLENRL];
