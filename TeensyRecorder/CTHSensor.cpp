//-------------------------------------------------------------------------
//! \file CTHSensor.cpp
//! \brief Management class of temperature / humidity sensors type BMP280, BME280 or AHT10
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
//! BPM   from https://github.com/ControlEverythingCommunity/BME280
//! AHT10 from https://github.com/Thinary/AHT10
/* 
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdint.h>
#include <math.h>
#include <Arduino.h>
#include <Wire.h>
#include "CTHSensor.h"

// BME280 I2C address 0x76 (108)
#define AddrBME280 0x76

// AHT10 I2C address
#define AddrAHT10  0x38

// AHT10 constant
typedef unsigned char Sensor_CMD;
Sensor_CMD eSensorCalibrateCmd[3] = {0xE1, 0x08, 0x00};
Sensor_CMD eSensorMeasureCmd[3]   = {0xAC, 0x33, 0x00};
boolean    GetRHumidityCmd        = true;
boolean    GetTempCmd             = false;

//-------------------------------------------------------------------------
//! \class CTHSensor
//! \brief Management class of temperature / humidity sensors type BMP280, BME280 or AHT10

//-------------------------------------------------------------------------
//! \brief Constructor (initialization of parameters to default values)
CTHSensor::CTHSensor()
{
  bInitOK = false;
  bBMPType = false;
  fHumidity = -1;
  fTemperature = -100;
}

//-------------------------------------------------------------------------
//! \brief Test if the sensor is present
//! \return True if a sensor is present
bool CTHSensor::Begin()
{
  // Test si présence sonde type BMP280 ou BME280
  Wire.beginTransmission( AddrBME280);
  byte error = Wire.endTransmission();
  if (error == 0)
  {
    bInitOK = true;
    bBMPType = true;
    fHumidity = -1;
    fTemperature = -100;
    //Serial.println("Sonde type BMP");
  }
  else
  {
    Wire.beginTransmission( AddrAHT10);
    byte error = Wire.endTransmission();
    if (error == 0)
    {
      bInitOK = true;
      bBMPType = false;
      //Serial.println("Sonde type AHT10");
    }
  }
  return bInitOK;
}

//-------------------------------------------------------------------------
//! \brief Read humidity
//! \return Humidity in percentage
float CTHSensor::ReadHumidity()
{
  float fH = -1;

  if (bInitOK and bBMPType)
  {
    if (fTemperature < -99)
      fH = readBMPSensor(GetRHumidityCmd);
    else
    {
      fH = fHumidity;
      fTemperature = -100;
    }
    //Serial.printf("ReadHumidity BMP %f %%\n", fH);
  }
  else if (bInitOK)
  {
    Wire.beginTransmission(AddrAHT10);
    Wire.write(eSensorCalibrateCmd, 3);
    Wire.endTransmission();
    delay(10);
    float value = readAHT10Sensor(GetRHumidityCmd);
    if (value != 0)
      fH = value * 100 / 1048576;
    //Serial.printf("ReadHumidity AHT10 %f %%\n", fH);
  }
  return fH;
}

//-------------------------------------------------------------------------
//! \brief Read temperature
//! \return Temperature in degrees celsius
float CTHSensor::ReadTemperature()
{
  float fT = -100;

  if (bInitOK and bBMPType)
  {
    if (fTemperature < -99)
      fT = readBMPSensor(GetTempCmd);
    else
    {
      fT = fTemperature;
      fTemperature = -100;
    }
    //Serial.printf("ReadTemperature BMP %f % C\n", fT);
  }
  else if (bInitOK)
  {
    Wire.beginTransmission(AddrAHT10);
    Wire.write(eSensorCalibrateCmd, 3);
    Wire.endTransmission();
    delay(10);
    float value = readAHT10Sensor(GetTempCmd);
    fT = ((200 * value) / 1048576) - 50;
    //Serial.printf("ReadTemperature AHT10 %f % C\n", fT);
  }
  return fT;
}

//-------------------------------------------------------------------------
//! \brief Read AHT10 sensor
//! \param GetDataCmd true for humidity, false for temperature
//! \return result
unsigned long CTHSensor::readAHT10Sensor(bool GetDataCmd)
{
  unsigned long result, temp[6];
  
  Wire.beginTransmission(AddrAHT10);
  Wire.write(eSensorMeasureCmd, 3);
  Wire.endTransmission();
  delay(100);
  
  Wire.requestFrom(AddrAHT10, 6);
  
  for(unsigned char i = 0; Wire.available() > 0; i++)
    temp[i] = Wire.read();
  
  if (GetDataCmd)
    result = ((temp[1] << 16) | (temp[2] << 8) | temp[3]) >> 4;
  else
    result = ((temp[3] & 0x0F) << 16) | (temp[4] << 8) | temp[5];
  
  return result;
}

//-------------------------------------------------------------------------
//! \brief Read BMP sensor
//! \param GetDataCmd true for humidity, false for temperature
//! \return result
float CTHSensor::readBMPSensor(bool GetDataCmd)
{
  float cTemp, humidity, pressure, fReturn;
  unsigned int b1[24];
  unsigned int data[8];
  unsigned int dig_H1 = 0;
  for(int i = 0; i < 24; i++)
  {
    // Start I2C Transmission
    Wire.beginTransmission(AddrBME280);
    // Select data register
    Wire.write((136+i));
    // Stop I2C Transmission
    Wire.endTransmission();
     
    // Request 1 byte of data
    Wire.requestFrom(AddrBME280, 1);
     
    // Read 24 bytes of data
    if(Wire.available() == 1)
      b1[i] = Wire.read();
  }
   
  // Convert the data
  // temp coefficients
  unsigned int dig_T1 = (b1[0] & 0xff) + ((b1[1] & 0xff) * 256);
  int dig_T2 = b1[2] + (b1[3] * 256);
  int dig_T3 = b1[4] + (b1[5] * 256);
   
  // pressure coefficients
  unsigned int dig_P1 = (b1[6] & 0xff) + ((b1[7] & 0xff ) * 256);
  int dig_P2 = b1[8] + (b1[9] * 256);
  int dig_P3 = b1[10] + (b1[11] * 256);
  int dig_P4 = b1[12] + (b1[13] * 256);
  int dig_P5 = b1[14] + (b1[15] * 256);
  int dig_P6 = b1[16] + (b1[17] * 256);
  int dig_P7 = b1[18] + (b1[19] * 256);
  int dig_P8 = b1[20] + (b1[21] * 256);
  int dig_P9 = b1[22] + (b1[23] * 256);

  // Start I2C Transmission
  Wire.beginTransmission(AddrBME280);
  // Select data register
  Wire.write(161);
  // Stop I2C Transmission
  Wire.endTransmission();
   
  // Request 1 byte of data
  Wire.requestFrom(AddrBME280, 1);
   
  // Read 1 byte of data
  if(Wire.available() == 1)
    dig_H1 = Wire.read();
   
  for(int i = 0; i < 7; i++)
  {
    // Start I2C Transmission
    Wire.beginTransmission(AddrBME280);
    // Select data register
    Wire.write((225+i));
    // Stop I2C Transmission
    Wire.endTransmission();
     
    // Request 1 byte of data
    Wire.requestFrom(AddrBME280, 1);
     
    // Read 7 bytes of data
    if(Wire.available() == 1)
      b1[i] = Wire.read();
  }
   
  // Convert the data
  // humidity coefficients
  int dig_H2 = b1[0] + (b1[1] * 256);
  unsigned int dig_H3 = b1[2] & 0xFF ;
  int dig_H4 = (b1[3] * 16) + (b1[4] & 0xF);
  int dig_H5 = (b1[4] / 16) + (b1[5] * 16);
  int dig_H6 = b1[6];
   
  // Start I2C Transmission
  Wire.beginTransmission(AddrBME280);
  // Select control humidity register
  Wire.write(0xF2);
  // Humidity over sampling rate = 1
  Wire.write(0x01);
  // Stop I2C Transmission
  Wire.endTransmission();
   
  // Start I2C Transmission
  Wire.beginTransmission(AddrBME280);
  // Select control measurement register
  Wire.write(0xF4);
  // Normal mode, temp and pressure over sampling rate = 1
  Wire.write(0x27);
  // Stop I2C Transmission
  Wire.endTransmission();
   
  // Start I2C Transmission
  Wire.beginTransmission(AddrBME280);
  // Select config register
  Wire.write(0xF5);
  // Stand_by time = 1000ms
  Wire.write(0xA0);
  // Stop I2C Transmission
  Wire.endTransmission();
   
  for(int i = 0; i < 8; i++)
  {
    // Start I2C Transmission
    Wire.beginTransmission(AddrBME280);
    // Select data register
    Wire.write((247+i));
    // Stop I2C Transmission
    Wire.endTransmission();
     
    // Request 1 byte of data
    Wire.requestFrom(AddrBME280, 1);
     
    // Read 8 bytes of data
    if(Wire.available() == 1)
      data[i] = Wire.read();
  }
   
  // Convert pressure and temperature data to 19-bits
  long adc_p = (((long)(data[0] & 0xFF) * 65536) + ((long)(data[1] & 0xFF) * 256) + (long)(data[2] & 0xF0)) / 16;
  long adc_t = (((long)(data[3] & 0xFF) * 65536) + ((long)(data[4] & 0xFF) * 256) + (long)(data[5] & 0xF0)) / 16;
  // Convert the humidity data
  long adc_h = ((long)(data[6] & 0xFF) * 256 + (long)(data[7] & 0xFF));
   
  // Temperature offset calculations
  double var1 = (((double)adc_t) / 16384.0 - ((double)dig_T1) / 1024.0) * ((double)dig_T2);
  double var2 = ((((double)adc_t) / 131072.0 - ((double)dig_T1) / 8192.0) *
    (((double)adc_t)/131072.0 - ((double)dig_T1)/8192.0)) * ((double)dig_T3);
  double t_fine = (long)(var1 + var2);
  cTemp = (var1 + var2) / 5120.0;

  // Pressure offset calculations
  var1 = ((double)t_fine / 2.0) - 64000.0;
  var2 = var1 * var1 * ((double)dig_P6) / 32768.0;
  var2 = var2 + var1 * ((double)dig_P5) * 2.0;
  var2 = (var2 / 4.0) + (((double)dig_P4) * 65536.0);
  var1 = (((double) dig_P3) * var1 * var1 / 524288.0 + ((double) dig_P2) * var1) / 524288.0;
  var1 = (1.0 + var1 / 32768.0) * ((double)dig_P1);
  double p = 1048576.0 - (double)adc_p;
  p = (p - (var2 / 4096.0)) * 6250.0 / var1;
  var1 = ((double) dig_P9) * p * p / 2147483648.0;
  var2 = p * ((double) dig_P8) / 32768.0;
  pressure = (p + (var1 + var2 + ((double)dig_P7)) / 16.0) / 100;
   
  // Humidity offset calculations
  double var_H = (((double)t_fine) - 76800.0);
  var_H = (adc_h - (dig_H4 * 64.0 + dig_H5 / 16384.0 * var_H)) * (dig_H2 / 65536.0 * (1.0 + dig_H6 / 67108864.0 * var_H * (1.0 + dig_H3 / 67108864.0 * var_H)));
  humidity = var_H * (1.0 - dig_H1 * var_H / 524288.0);
  //Serial.printf("readBMPSensor cTemp %f C, humidity %d %%\n", cTemp, humidity);
  fHumidity = humidity;
  fTemperature = cTemp;

  // Only to avoid warning
  fReturn = pressure;
  // Set good value
  if (GetDataCmd)
    fReturn = humidity;
  else
    fReturn = cTemp;
  return fReturn;
}

//-------------------------------------------------------------------------
//! \class CADCVoltage
//! \brief Management class of an MCP3221 for reading battery voltages

#define MCP3X21_DEFAULT_ADDRESS 0x4D

#define MCP3221_CONVERSE 0x9B //10011011 NOTE IT ENDS IN 1, this is the READ ADDRESS. This is all this device does.
                                 //It opens a conversation via this specific READ address

//-------------------------------------------------------------------------
//! \brief Constructor (initialization of parameters to default values)
CADCVoltage::CADCVoltage()
{
  bInitOK = false;
}

//-------------------------------------------------------------------------
//! \brief Test if the MCP3221 is present
//! \return True if a MCP3221 is present
bool CADCVoltage::begin()
{
  bInitOK = false;
  // Test si présence MCP3221
  Wire.begin();
  Wire.beginTransmission( MCP3X21_DEFAULT_ADDRESS);
  byte error = Wire.endTransmission();
  //Serial.printf("CADCVoltage::begin error %d\n", error);
  if (error == 0)
    bInitOK = true;
  return bInitOK;  
}

//-------------------------------------------------------------------------
//! \brief Read humidity
//! \return Batterie voltage in volt
float CADCVoltage::getVoltage()
{
  //uint32_t uiT = micros();
  uint32_t data = 0;
  int iMax = 2;
  // n lectures puis moyenne
  for (int i=0; i<iMax; i++)
    data += read();
  data  = data / iMax;
  // VRef = 2.8V, Max ADC = 4095, resistor divisor is 2
  // float V = (2.8 * (float)data / 4095.0) * 2;
  float V = (float)data * 2.0 / 1000.0;
  //Serial.printf("getVoltage data %d, %4.1fV, %dµs\n", data, V, micros()-uiT);
  return V;
}

//-------------------------------------------------------------------------
//! \brief Read ADC MCP3221
//! \return ADC value
uint16_t CADCVoltage::read()
{
  char data[2];
  // Start I2C Transmission
  Wire.begin();
  Wire.beginTransmission(MCP3X21_DEFAULT_ADDRESS);
  // Send a byte to start the conversation. It should be acknowledged
  Wire.write(MCP3221_CONVERSE);
  // Stop I2C Transmission
  Wire.endTransmission();
    
  // Request 2 bytes of data
  Wire.requestFrom(MCP3X21_DEFAULT_ADDRESS, 2);
    
  // Read 2 bytes of data
  data[0] = Wire.read();
  data[1] = Wire.read();
 
  //convert to 12 bit.
  uint16_t res;
  int _12_bit_var; // 2 bytes
  char _4_bit_MSnibble = data[0]; // 1 byte, example 0000 1000
  char _8_bit_LSByte   = data[1]; // 1 byte, example 1111 0000

  _12_bit_var = ((0x0F & _4_bit_MSnibble) << 8) | _8_bit_LSByte;   //example 100011110000
  res = _12_bit_var;

  return  res;
}
