//-------------------------------------------------------------------------
//! \file CModePlayer.h
//! \brief Classe de gestion du mode lecture
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/* 
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "ModesModifiers.h"
#include "CModeGeneric.h"
#include "CReader.h"

#ifndef MODEPLAYER
#define MODEPLAYER

#define MAXLINESWAVE  5  //!< Number of files displayed on the screen
#define MIDDLELINEWAV 3  //!< Middle line to display files

//! \enum PLAYTYPES
//! \brief Index of Player type
enum PLAYTYPES {
  TYPEX1  , //!< Normal player
  TYPEX10 , //!< Time expansion X10
  TYPEHET , //!< Heterodyne
  TYPEDEL , //!< Delete current file
  MAXPTYPE
};

//! \enum IDXPLPARAMS
//! \brief Index of Player mode parameters
enum IDXPLPARAMS {
  IDXPLBAT  , //!< Internal battery charge level in %
  IDXPLPLAY , //!< Play / Break Modifier
  IDXPLPOS  , //!< Bargraph of the reading position
  IDXPLDUR  , //!< Duration of the file in reading
  IDXPLRPOS , //!< Reading position in seconds
  IDXPLTYPE , //!< X1, X10, Heterodyne or delete file
  IDXPLFRHET, //!< Heterodyne frequency
  IDXDELFILE, //!< Deleting file
  IDXPLTYPEL, //!< X1, X10, Heterodyne or delete file, mode in line
  IDXPLWAVA , //!< Wav file line A
  IDXPLWAVB , //!< Wav file line B
  IDXPLWAVC , //!< Wav file line C
  IDXPLWAVD , //!< Wav file line D
  IDXPLWAVE , //!< Wav file line E
  IDXMAXPLAY
};

//-------------------------------------------------------------------------
//! \class CModePlayer
//! \brief Player mode management class
class CModePlayer: public CModeGeneric
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  CModePlayer();

  //-------------------------------------------------------------------------
  //! \brief Beginning of the mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  //! \brief End of the mode
  virtual void EndMode();

  //-------------------------------------------------------------------------
  //! \brief To display information outside of modifiers
  virtual void AddPrint();
  
  //-------------------------------------------------------------------------
  //! \brief Keyboard order processing
  //! If the key is a mode change key, returns the requested mode
  //! This method is called regularly by the main loop
  //! By default, handles modifiers
  //! \param key Touch to treat
  virtual int KeyManager(
    unsigned short key
    );
    
  //-------------------------------------------------------------------------
  //! \brief Initializing the selected wav file
  //! Return true if file OK
  bool InitWavFile();

  //-------------------------------------------------------------------------
  //! \brief Set read mode
  void SetReadMode();

  //-------------------------------------------------------------------------
  //! \brief Start reading the wav file
  void StartRead();

  //-------------------------------------------------------------------------
  //! \brief Stop reading the wav file
  void StopRead();

  //-------------------------------------------------------------------------
  //! \brief Returns the string to display, taking only the n most significant characters
  //! 123456789012345678901
  //!  PR001_190126_221812.wav
  //!  PR001_190126_221812
  //! \param str string to verify
  String PrepareStrFile(
    String str
    );

  //-------------------------------------------------------------------------
  //! \brief Initializes the list of displayed files according to the 1st selected
  void InitLstAffFiles();
    
  //-------------------------------------------------------------------------
  //! \brief List the wav files in the root directory
  void ListFiles();
    
  //-------------------------------------------------------------------------
  //! \brief To set true Automatic playback of the last recorded file
  static void SetAutomaticPlay();
    
protected:
  //! Play / Break status
  int iPlayBreak;

  //! File duration in seconds
  int iFileDuration;

  //! File position in seconds
  int iFilePosition;

  //! File position in percent
  int iFilePercent;

  //! Player type
  int iPlayerType;

  //! Heterodyne frequency
  float fHeterodyne;

  //! Step Value of the converter for calculating the heterodyne frequency
  float fStepADC;

  //! list of wav files displayed on the screen
  char LstFilesScreen[MAXLINESWAVE][MAX_LINEPARAM+1];

  //! List of wav files in the root directory
  std::vector<String> lstWaveFiles;

  //! Index of selectd file
  int iSelectedFile;

  //! Time to check battery
  unsigned long uiTimeCheck;

  //! Gestionnaire de lecture
  CReader Reader;

  // Chaine de la Fe du fichier en lecture
  char sFe[12];

  // Pour l'affichage du paramètre du type de lecture mode en ligne
  bool bTypeModeInLine;

  // Pour la suppression du fichier wav courant
  bool bDelCurrentFile, bYesDelFile;

  // Index de la ligne du fichier sélectionné
  int idxSelFile;

  // Automatic playback of the last recorded file
  static bool bAutoplay;
};
#endif // MODEPLAYER
