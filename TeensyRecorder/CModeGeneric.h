//-------------------------------------------------------------------------
//! \file CModeGeneric.h
//! \brief Classe générique de gestion de smode de fonctionnement et mode veille
//! \details class CModeGeneric, CModeVeille
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 /*
  * Classes :
  * - CModeGeneric
  * - CModeVeille
  * - CModeTestSD
  */
#include "SdFat.h"
#include "Const.h"
#include "ModesModifiers.h"
#include "CTHSensor.h"
#include "MAX17043.h"

#ifndef CMODEGENERIC_H
#define CMODEGENERIC_H

//-------------------------------------------------------------------------
//! \class CModeGeneric
//! \brief Classe générique de gestion d'un mode de fonctionnement des Teensy Recorder
//! Définie les paramètres communs des différents modes et gère leur lecture et sauvegarde
 //!Gère l'état des alimentations
class CModeGeneric: public CGenericMode
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  CModeGeneric();

  //-------------------------------------------------------------------------
  //! \brief Beginning of the mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  //! \brief End of the mode
  virtual void EndMode();
  
  //-------------------------------------------------------------------------
  //! \brief Initialising static parameters
  static void StaticBeginMode();
  
  //-------------------------------------------------------------------------
  //! \brief Default setting of the parameters
  static void SetDefault();
  
  //-------------------------------------------------------------------------
  //! \brief Default setting for RhinoLogger bands
  static void SetDefaultRLBands();
  
  //-------------------------------------------------------------------------
  //! \brief Reading saved settings
  virtual bool ReadParams();
  
  //-------------------------------------------------------------------------
  //! \brief Reading saved settings in static function
  static bool StaticReadParams();
  
  //-------------------------------------------------------------------------
  //! \brief Control Common Params
  static bool StaticCtrlCommonParams();
  
  //-------------------------------------------------------------------------
  //! \brief Control Profil Params
  static bool StaticControlParams();
  
  //-------------------------------------------------------------------------
  //! \brief Writing parameters to save them
  virtual void WriteParams();
  
  //-------------------------------------------------------------------------
  //! \brief Writing parameters to save them in static function
  static void StaticWriteParams();

  //-------------------------------------------------------------------------
  //! \brief Preparation of EEPROM with default profiles
  //! \param pPrefixe Previous or default prefix
  //! \param iLangue  Previous language or default language
  //! \param iLangue  Previous screen type or default type
  static void PreparationEEPROM(
    char *pPrefixe,
    int iLangue,
    int iScreenType);
    
  //-------------------------------------------------------------------------
  //! \brief Update screen (standard behavior = pDisplay->sendBuffer())
  virtual void UpdateScreen();

  //-------------------------------------------------------------------------
  //! \brief Calcul des minutes de début et fin d'enregistrement
  static void CalcTimeDebFin();
  
  //-------------------------------------------------------------------------
  //! \brief Retourne true si on doit passer en veille
  static bool IsTimeVeille();
  
  //-------------------------------------------------------------------------
  //! \brief Retourne un pointeur sur les paramètres communs
  static CommonParams *GetCommonsParams( ) { return &commonParams;};
  
  //-------------------------------------------------------------------------
  //! \brief Retourne un pointeur sur les paramètres opérateurs
  static ParamsOperator *GetParamsOperator( ) { return &paramsOperateur;};
  
  //-------------------------------------------------------------------------
  //! \brief Mesure des batteries
  //! \param iMode Mode de mesure si MAX17043
  //! Retourne false si les batteries sont trops faibles
  static bool CheckBatteries(
    int iMode = MAX_WAITMESURE
    );

  //-------------------------------------------------------------------------
  //! \brief Read options bits to decode the type of the recorder
  //! Return recorder type
  static int ReadRecorderType();

  //-------------------------------------------------------------------------
  //! \brief Return recorder type
  static int GetRecorderType() {return iRecorderType;};

#ifdef WITHBLUETOOTH
  //! Indique si le Bluetooth est actif ou non
  static bool bBluetooth;
#endif

  //! Indique un démarrage en mode debug (touche Up pressée)
  static bool bDebug;
  
  //-------------------------------------------------------------------------
  //! \brief Initialization function and calibration of the ADC0 to read the heterodyne frequency
  //! \param bRefVCC true for VCC Ref, false for internal 1V2 ref
  static void ADC0Init(
    bool bRefVCC
    );
    
  //-------------------------------------------------------------------------
  //! \brief Single-shot reading function of an analog input with ADC 0 to read the heterodyne frequency
  //! Returns the value read (0-4095)
  //! \param iPin Pin to read
  static uint16_t ADC0Read(
    int iPin
    );
    
  //-------------------------------------------------------------------------
  //! \brief Retourne true si la sonde de température est présente
  static bool IsTempSensor();

  //-------------------------------------------------------------------------
  //! \brief Print Ram usage
  //! \param pTxt Sting to print
  //! \return true if OK, false if open log file problem
  static bool PrintRamUsage(
    const char *pTxt
    );

  //-------------------------------------------------------------------------
  //! \brief Lecture du capteur de température
  void LectureTemperature();

  //-------------------------------------------------------------------------
  //! \brief Retourne la température en degré
  float GetTemperature() { return cTemp;};

  //-------------------------------------------------------------------------
  //! \brief Retourne l'humidité en %
  int GetHumidity() {return (int)humidity;};

  //-------------------------------------------------------------------------
  //! \brief Mémorisation de la température en début d'enregistrement d'un fichier wav
  void MemoTemperature();
  
  //-------------------------------------------------------------------------
  //! \brief Function test of the SD card. Resets the card if necessary and goes into error if the card stops responding
  //! \return true if OK, false if SD error
  static bool isSDInit();
  
  //-------------------------------------------------------------------------
  //! \brief Turns headphone audio circuits ON
  static void TurnAudioON();
  
  //-------------------------------------------------------------------------
  //! \brief Turns headphone audio circuits OFF
  static void TurnAudioOFF();
  
  //-------------------------------------------------------------------------
  //! \brief Export profiles on SD in Profiles.ini file
  void ExportProfiles();

  //-------------------------------------------------------------------------
  //! \brief Import profiles from SD in Profiles.ini file
  //! \param *pFile  .ini file name
  //! \return true if import is OK
  static bool ImportProfiles( char *pFile);

  //-------------------------------------------------------------------------
  //! \brief Read AutoProfiles.ini if exist
  //! \return true if time is set
  static bool AutoProfiles();

  //-------------------------------------------------------------------------
  //! \brief Read AutoTime.ini if exist
  //! \return true if time is set
  static bool AutoTime();

  //-------------------------------------------------------------------------
  //! \brief Seek to a section of a .ini file
  //! \param *f         .ini file
  //! \param *pFileName Ini file name
  //! \param *pSection  Section name
  //! \return true if seek is OK
  static bool SeekToSection(
    SdFile *f, char *pFileName, char *pSection
    );
    
  //-------------------------------------------------------------------------
  //! \brief Decode a int in a .ini line
  //! \param *f       .ini file
  //! \param *pFileName Ini file name
  //! \param *pSection  Section name
  //! \param *pCode   String of the code
  //! \param *pInt    integer to initialize
  //! \param iMin     minimum value
  //! \param iMax     maximum value
  //! \param iDefault Default value
  //! \return true if decode is OK
  static bool DecodeInt(
    SdFile *f, char *pFileName, char *pSection, char *sCode, int *pInt, int iMin, int iMax, int iDefault
    );

  //-------------------------------------------------------------------------
  //! \brief Decode a bool in a .ini line
  //! \param *f       .ini file
  //! \param *pFileName Ini file name
  //! \param *pSection  Section name
  //! \param *pCode   String of the code
  //! \param *pBool   bool to initialize
  //! \param bDefault Default value
  //! \return true if decode is OK
  static bool DecodeBool(
    SdFile *f, char *pFileName, char *pSection, char *sCode, bool *pBool, bool bDefault
    );

  //-------------------------------------------------------------------------
  //! \brief Decode a float in a .ini line
  //! \param *f       .ini file
  //! \param *pFileName Ini file name
  //! \param *pSection  Section name
  //! \param *pCode   String of the code
  //! \param *pFloat  float to initialize
  //! \param fMin     minimum value
  //! \param fMax     maximum value
  //! \param fDefault Default value
  //! \return true if decode is OK
  static bool DecodeFloat(
    SdFile *f, char *pFileName, char *pSection, char *sCode, float *pFloat, float fMin, float fMax, float fDefault
    );

  //-------------------------------------------------------------------------
  //! \brief Decode a string in a .ini line
  //! \param *f         .ini file
  //! \param *pFileName Ini file name
  //! \param *pSection  Section name
  //! \param *pCode     String of the code
  //! \param *pChar     String to initialize
  //! \param iSize      maximum char
  //! \param *pDefault  Default value
  //! \return true if decode is OK
  static bool DecodeString(
    SdFile *f, char *pFileName, char *pSection, char *sCode, char *pChar, int iMax, const char *pDefault
    );

  //-------------------------------------------------------------------------
  //! \brief Decode enum value in a .ini line
  //! \param *f         .ini file
  //! \param *pFileName Ini file name
  //! \param *pSection  Section name
  //! \param *pCode     String of the code
  //! \param **pChar    Table of the enum values
  //! \param iMax       maximum enum
  //! \param *pInt      integer to initialize
  //! \param iDef       Default value
  //! \return true if decode is OK
  static bool DecodeEnum(
    SdFile *f, char *pFileName, char *pSection, char *sCode, const char **pChar, int iMax, int *pInt, int iDef
    );

  // Mot de controle des versions de paramètres
  static uint16_t uiCtrlVersion;

protected:
  //! Pour mesure la température toute les 10mn
  unsigned long uiTimeTemp;
  //! Humidité
  float humidity;
  //! Pression atmosphérique
  float pressure;
  //! Température
  float cTemp;

  //! Paramètres communs aux différents profils
  static CommonParams commonParams;
  
  //! Paramètres opérateurs
  static ParamsOperator paramsOperateur;

  //! Tension de la batterie LiPo interne en V
  static double fNivBatLiPo;
  //! Tension de la batterie externe en V
  static double fNivBatExt;
  //! Niveau de charge de la batterie LiPo interne en %
  static int    iNivBatLiPo;
  //! Niveau de charge de la batterie externe en %
  static int    iNivBatExt;

  //! Nombre de Minutes de début et de fin d'acquisition/enregistrement par rapport à minuit
  static int iDebMinutes, iFinMinutes;

  //! Recorder type
  static int iRecorderType;
  
  //! Indique que le teste de présence de la sonde a été effectué
  static bool bTestSonde;
  //! Indique la présence de la sonde de température
  static bool bSonde;
  //! Gestionnaire de lecture de la sonde
  static CTHSensor GestSensor;
  //! Pour lire ou non les paramètres communs
  static bool bReadCommon;
  //! Indique que le module MAX est power on
  static bool bMAXPowerOn;
  //! Indique que le module MAX est en erreur
  static bool bMAXError;
  //! Indique qu'un MCP3221 est présent pour la meure de la tension batterie
  static bool bADCBat;
  //! Gestionnaire d'un éventuel MCP3221 pour la mesure de la tension batterie
  static CADCVoltage ADCBat;
public:
  //! Gestionnaire d'un éventuel MAX17043 pour la mesure de la tension batterie
  static MAX17043 max17043;
};

//-------------------------------------------------------------------------
//! \class CModeVeille
//! \brief Classe de gestion du mode veille
//! Passe en veille après 15s et, sur fin de veille, repasse en mode enregistrement
class CModeVeille : public CModeGeneric
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructeur
  CModeVeille();

  //-------------------------------------------------------------------------
  //! \brief Début du mode
  virtual void BeginMode();

  //-------------------------------------------------------------------------
  //! \brief Affichage du mode sur l'écran
  //! Cette méthode est appelée régulièrement par le loop principal
  //! à charge des différents modes d'afficher les informations nécessaires
  virtual void PrintMode();
  
  //-------------------------------------------------------------------------
  //! \brief Traitement des ordres claviers
  //! Si la touche est une touche de changement de mode, retourne le mode demandé
  //! Cette méthode est appelée régulièrement par le loop principal
  //! à charge des différents modes de traiter les actions opérateurs
  //! \param key Touche sollicitée par l'opérateur
  virtual int KeyManager(
    unsigned short key
    );

  //-------------------------------------------------------------------------
  //! \brief Mise en veille du processeur. Réveil à l'heure demandée ou sur clic
  void MiseEnVeille();  

  //-------------------------------------------------------------------------
  //! For Teensy 4.1, indicate who wake up
  static int16_t iWhoWakeUp;

protected:
  //! Seconde courante pour une gestion de l'affichage toute les secondes
  uint8_t iCurrentSecond;
  
  //! Décompteur du mode attente du mode veille
  uint8_t iDecompteurAttVeille;
};

//! \struct SDFormattingParams
struct SDFormattingParams
{
  //
  uint32_t cardSizeBlocks;
  uint32_t cardCapacityMB;
  // MBR information
  uint8_t  partType;
  uint32_t relSector;
  uint32_t partSize;
  
  // Fake disk geometry
  uint8_t numberOfHeads;
  uint8_t sectorsPerTrack;
  
  // FAT parameters
  uint16_t reservedSectors;
  uint8_t  sectorsPerCluster;
  uint32_t fatStart;
  uint32_t fatSize;
  uint32_t dataStart;
  
  // constants for file system structure
  uint16_t const BU16 = 128;
  uint16_t const BU32 = 8192;
};

//-------------------------------------------------------------------------
//! \class CModeTestSD
//! \brief Classe de test de la carte SD
class CModeTestSD : public CModeGeneric
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructeur
  CModeTestSD();

  //-------------------------------------------------------------------------
  //! \brief Début du mode
  virtual void BeginMode();

  //-------------------------------------------------------------------------
  //! \brief To display information outside of modifiers
  virtual void AddPrint();
  
  //-------------------------------------------------------------------------
  //! \brief Called on end of change of a modifier
  //! If a mode change, returns the requested mode
  //! \param idxModifier Index of affected modifier
  virtual int OnEndChange(
    int idxModifier
    );
  
  //-------------------------------------------------------------------------
  //! \brief Test des performances de la carte SD
  void TestSD();
  
  //-------------------------------------------------------------------------
  //! \brief Affichage pendnat le formatage
  static void AffFormatting();
  
  //-------------------------------------------------------------------------
  //! \brief Formatage de la carte SD
  static bool FormattingSD();
  
protected:
  //! Pointeur sur l'instance
  static CModeGeneric *pInstance;
  
  //! Chaine de taille SD
  char sSizeSD[25];
  
  //! Booléen de RAZ des fichiers de la carte SD
  bool bRAZ;
  
  //! Indicateur de formatage de la carte SD
  static int iFormat;
  
  //! Size of the SD card in GO
  long iSizeSD;
  
  //! Temps d'ouverture fichier wave en ms
  unsigned long uiOpen;

  //! Temps d'écriture fichier wave en µs
  unsigned long uiWrite;

  //! Temps de fermeture fichier wave en ms
  unsigned long uiClose;

  //! Timer pour affichage
  IntervalTimer timerAff;
};

#endif
