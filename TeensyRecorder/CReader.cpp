/* 
 * File:   CReader.cpp
   TeensyRecorders Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Const.h"
#include "CReader.h"
#include "CRecorder.h"
#include "CModeGeneric.h"
#include "CModeParams.h"
#include "FiltresFIR.h"

// Gestionnaire de carte SD
extern SdFs sd;

//#define TESTMINMAXECH
#ifdef TESTMINMAXECH
int16_t uiMaxEch = -32000;
int16_t uiMinEch = 32000;
#endif

#if defined(__IMXRT1062__) // Teensy 4.1
  // Sur Teensy 4.1, définition des buffers DMA en RAM2, donc forcément static
  DMAMEM __attribute__((aligned(32))) int16_t CReader::samplesDMA[MAXSAMPLE_READ*4];
#endif

//-------------------------------------------------------------------------
// Classe pour la gestion de la lecture d'un fichier wav
// Gère la lecture du fichier wav et l'envoi des échantillons vers le DAC0

//-------------------------------------------------------------------------
// Constructeur (initialisation de la classe)
CReader::CReader(
  ParamsOperator *pParams
  ) : restitution( (uint16_t *)samplesDMA, MAXSAMPLE_READ, 384000)
{
  // Init des variables
  pParamsOp     = pParams;
  pInstance     = this;
  bReading      = false;
  bExp10        = false;
  bHet          = false;
  iFe           = 0;
  lWaveLength   = 0;
  lWaveEchRead  = 0;
  iWaveDuration = 0;
  iWavePos      = 0;
  iPosLecture   = 0;
  iShift        = 0;
  sizeBuffer    = MAXSAMPLE_READ * sizeof(uint16_t);
  sPathWaveFile[0] = 0;
  P1 = (uint16_t *)samplesDMA;
#if defined(__MK66FX1M0__) // Teensy 3.6
  P2 = &samplesDMA[MAXSAMPLE_READ];
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  //! Sur Teensy 4.1, buffer stéréo
  P2 = (uint16_t *)&samplesDMA[MAXSAMPLE_READ*2];
#endif
}

//-------------------------------------------------------------------------
// Destructeur
CReader::~CReader()
{
  pInstance = NULL;
}

//-------------------------------------------------------------------------
// Initialisation du fichier à lire (ouverture du fichier)
// Retourne true si ouverture OK
bool CReader::SetWaveFile(
  char *pPathWavFile // Pointeur vers le nom du fichier
  )
{
  bool bOK = false;
  // Mémo du nom du fichier
  strcpy( sPathWaveFile, pPathWavFile);
  // Ouverture du fichier wave sans expansion de temps
  if (waveFile.OpenWaveFileForRead( sPathWaveFile, false))
  {
    // Init de la fréquence d'échantillonnage
    iFe           = waveFile.GetSampleRate();
    lWaveLength   = waveFile.GetDataLength() / sizeof(int16_t);
    float fDur    = (float)lWaveLength / (float)iFe;
    iWaveDuration = (int)fDur;
    bStereo       = waveFile.IsStereo();
    lWaveEchRead  = 0;
    iWavePos      = 0;
    iPosLecture   = 0;
    iShift        = 0;
    waveFile.SetPosRead(0);
    bOK = true;
    if (bStereo)
    {
      sizeBuffer  = MAXSAMPLE_READ * sizeof(uint16_t) * 2;
      fDur = fDur / 2.0;
    }
    else
      sizeBuffer  = MAXSAMPLE_READ * sizeof(uint16_t);
    //Serial.printf("CReader::SetWaveFile lWaveLength %d, MAXSAMPLE_READ %d, nbITs %d, iShift %d\n", lWaveLength, MAXSAMPLE_READ, lWaveLength/MAXSAMPLE_READ, iShift);
  }
  else
  {
    iWaveDuration = 0;
    lWaveEchRead  = 0;
    iWavePos      = 0;
    iPosLecture   = 0;
    bStereo       = false;
  }
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief Initialisation du mode de lecture
void CReader::SetReadMode(
  int iFeRead,      // Fréquence d'échantillonnage de lecture
  bool BHeterodyne  // Indique si lecture en hétérodyne (true)
  )
{
  // Mémo de la Fe de lecture et hétérodyne
  iFe    = iFeRead;
  bHet   = BHeterodyne;
  // Calcul de la durée
  lWaveLength   = waveFile.GetDataLength() / sizeof(int16_t);
  float fDur    = (float)lWaveLength / (float)iFe;
  if (bStereo)
    fDur = fDur / 2.0;
  iWaveDuration = (int)fDur;
  if (bHet)
  {
    // Initialisation de l'hétérodyne
    heterodyne.StartHeterodyne( iFe);
    // Initialisation du filtre de sortie hétérodyne
#if defined(__MK66FX1M0__) // Teensy 3.6
    const double *newCoefs = FLowH384kHz;
    switch (iFe)
    {
    case 250000: newCoefs = FLowH250kHz; break;
    case 384000: newCoefs = FLowH384kHz; break;
    case 500000: newCoefs = FLowH500kHz; break;
    }
    // Initialisation du filtre FIR
    firFilter.InitFIRFilter( 32, newCoefs);
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
    const int16_t *newCoefs = FLowH384kHz64T;
    switch (iFe)
    {
    case FE250KHZ: newCoefs = FLowH250kHz64T; break;
    case FE500KHZ: newCoefs = FLowH500kHz64T; break;
    case FE384KHZ: newCoefs = FLowH384kHz64T; break;
    }
    // Initialisation du filtre FIR
    firFilter.InitFIRFilter( 64, newCoefs);
#endif
  }
  //if (CModeGeneric::bDebug)
  //  Serial.printf("CReader::SetReadMode [%s], Fe %d Hz, durée %ld s, nb Ech %ld, Stereo %d\n", sPathWaveFile, iFe, iWaveDuration, lWaveLength, bStereo);
}

//-------------------------------------------------------------------------
// Lance la lecture du fichier sélectionné
// Retourne true si lecture OK
bool CReader::StartRead()
{
  if (CModeGeneric::bDebug)
    Serial.printf("CReader::StartRead [%s], Fe %d Hz\n", sPathWaveFile, iFe);
  /*#if IMXRT_CACHE_ENABLED >= 2    
    Serial.println("IMXRT_CACHE_ENABLED");
  #else
    Serial.println("IMXRT_CACHE_ENABLED");
  #endif*/
#if defined(__IMXRT1062__) // Teensy 4.1, filtre hétérodyne sélectif
  InitFiltreSel(40.0);
#endif
  // Autorisation audio
  CModeGeneric::TurnAudioON();
#ifdef WITHBLUETOOTH
  // Autorisation éventuelle du Bluetooth
  if (CModeGeneric::GetCommonsParams()->bBluetooth)
    digitalWriteFast( OPT_ALIM_BLE, LOW);
#endif
  if (bHet)
    // Initialisation de la Fe de restitution (décimation par 10 en hétérodyne)
    restitution.setFe( iFe / 10);
  else
    // Initialisation de la Fe de restitution
    restitution.setFe( iFe);
  // Remplissage des buffers avant lancement
  if (SetBuffer( (uint16_t *)P1) and SetBuffer( (uint16_t *)P2))
  {
    // Lancement de la restitution
    bReading       = true;
    restitution.start();
  }
  else
    Serial.println("CReader::StartRead erreur sur SetBuffer !!");
#ifdef TESTMINMAXECH
  uiMinEch = 32000;
  uiMaxEch = -32000;
#endif
#if defined(__IMXRT1062__) // Teensy 4.1, filtre hétérodyne sélectif
  idxSelFilter = -1;
#endif
  return bReading;
}

//-------------------------------------------------------------------------
// Stoppe la lecture du fichier en lecture
// Retourne true si arret OK
bool CReader::StopRead()
{
#ifdef TESTMINMAXECH
  Serial.printf("Min ECH %D, Max ECH %d, iShift %d\n", uiMinEch, uiMaxEch, iShift);
#endif
  // Stoppe la restitution
  restitution.stop();
  bReading = false;
  lWaveEchRead  = 0;
  iWavePos      = 0;
  iPosLecture   = 0;
  waveFile.SetPosRead(0);
  // Stop alimentation audio et Bluetooth
  CModeGeneric::TurnAudioOFF();
#ifdef WITHBLUETOOTH
  digitalWriteFast( OPT_ALIM_BLE, HIGH);
#endif
  return bReading;
}

//-------------------------------------------------------------------------
// Positionne le fichier en lecture à +/- Coef de sa durée totale
// Retourne false si la durée du fichier est dépassée ou inférieure à 0
bool CReader::ReadNext(
  int iNext,    // +1 pour avancer, -1 pour reculer
  int iCoef     // Coefficient d'avancement
  )
{
  bool bOK = waveFile.ReadNext( iNext, iCoef);
  if (bOK)
  {
    if (iNext > 0)
      lWaveEchRead += (lWaveLength / iCoef);
    else
      lWaveEchRead -= (lWaveLength / iCoef);
    // Calcul de la position de lecture
    float fDur    = (float)lWaveEchRead / (float)iFe;
    iWavePos      = (unsigned int)fDur;
    if (iWavePos > iWaveDuration)
      iWavePos = iWaveDuration;
    fDur = (float)iWavePos * 100.0 / (float)iWaveDuration;
    iPosLecture   = (unsigned short)fDur;
    if (iPosLecture > 100)
      iPosLecture = 100;
  }
  return bOK;
}
  
//-------------------------------------------------------------------------
//! \brief Initialisation de la fréquence hétérodyne
//! \param fH Fréquence hétérodyne en kHz
void CReader::SetFreqHeterodyne(
  float fH
  )
{
  heterodyne.SetFreqHeterodyne( fH);
#if defined(__IMXRT1062__) // Teensy 4.1, filtre hétérodyne sélectif
  InitFiltreSel(fH);
#endif
}

#if defined(__IMXRT1062__) // Teensy 4.1, filtre hétérodyne sélectif
#ifdef SELFILTERH20KHZ
/*
 * Table des filtres sélectifs bande 20kHz
 */
extern const int16_t* TbSelectiveFilter20kHz[];
#endif
#ifdef SELFILTERH10KHZ
/*
 * Table des filtres sélectifs bande 10kHz
 */
extern const int16_t* TbSelectiveFilter10kHz[];
#endif

//----------------------------------------------------
//! \brief Initialisation du filtre sélectif
//! \param fH Fréquence hétérodyne en kHz
//! \return Retourne false en cas d'erreur
bool CReader::InitFiltreSel(
  float fFreqH
  )
{
  bool bOK = true;
#if defined(SELFILTERH10KHZ) || defined(SELFILTERH20KHZ)
  // Calcul de l'indice du filtre utilisé en fonction de la fréquence hétérodyne
  if (fFreqH >= 10.0)
  {
    float fIdx = 0.0;    
#ifdef SELFILTERH20KHZ
    // Filtres de 20kHz à partir de 10kHz et par pas de 5kHz
    fIdx = ((fFreqH - 10.0) / 5.0) - 0.5;
#endif
#ifdef SELFILTERH10KHZ
    // Filtres de 10kHz à partir de 10kHz et par pas de 2.5kHz
    fIdx = ((fFreqH - 10.0) / 2.5);
#endif
    int iNewIdxSelFilter = (int)fIdx;
    if (iNewIdxSelFilter < 0)
      iNewIdxSelFilter = 0;
#ifdef SELFILTERH20KHZ
    else if (iNewIdxSelFilter > 22)
      iNewIdxSelFilter = 22;
#endif
#ifdef SELFILTERH10KHZ
    else if (iNewIdxSelFilter > 44)
      iNewIdxSelFilter = 44;
#endif
    if (iNewIdxSelFilter != idxSelFilter)
    {
      idxSelFilter = iNewIdxSelFilter;
#ifdef SELFILTERH20KHZ
      const int16_t *newCoefs = TbSelectiveFilter20kHz[idxSelFilter];
#endif
#ifdef SELFILTERH10KHZ
      const int16_t *newCoefs = TbSelectiveFilter10kHz[idxSelFilter];
#endif
      // Initialisation du filtre FIR sélectif
      bOK = SelectiveFilter.InitFIRFilter( 64, newCoefs);
    }
    //Serial.printf("CReader::InitFiltreSel, fFreqH %fkHz, idxSelFilter %d\n", fFreqH, idxSelFilter);
  }
#endif
  return bOK;
}
#endif

//-------------------------------------------------------------------------
// Fonction d'initialisation d'un buffer avec les échantillons du fichier wave
// Sur Teensy 3.6, DAC interne 12 bits mono, sur Teensy 4.1, DAC externe 16 bits stéréo (seule la voie Droite est utilisée)
// Retourne true si OK
bool CReader::SetBuffer(
  uint16_t *pBuffer   // Pointeur sur le buffer à initialiser
  )
{
  //Serial.println("CReader::SetBuffer");
#if defined(__IMXRT1062__) // Teensy 4.1
  // Mémo du pointeur de début du buffer DMA
  uint16_t *pMemBuffer = pBuffer;
#endif
  //unsigned long uiT1 = micros();
  bool bOK = false;
  int32_t echR, echL;
  unsigned long lRead = 0;
  int           lMax  = MAXSAMPLE_READ;
  int iPlus = 1;
  if (bStereo)
  {
    // Mode stéréo, lecture d'un double buffer
    lMax *= 2;
    iPlus = 2;
  }
  if (!bHet)
  {
    //Serial.println("SetBufferX");
    // Mode expansion de temps X10 ou lecture normale X1
    // Lecture d'un bloc d'échantillons depuis le fichier wave
    lRead = waveFile.WavfileRead( samplesWave, lMax);
    if (lRead == sizeBuffer)
    {
      bOK = true;
      // Pour chaque échantillon du buffer
      for (int i=0; i<lMax; i+=iPlus)
      {
        echR = samplesWave[i];
        if (bStereo)
        {
          // Gestion stéréo, la restitution étant mono, on mélange les deux canaux sur le canal droit
          echL = samplesWave[i+1];
          // Mélange des canaux
          echR = (echR + echL) / 2;
          echL = echR;
        }
#if defined(__MK66FX1M0__) // Teensy 3.6, sortie mono sur DAC 12 bits
        // Test that we do not leave the domain of the 12 bits ADC (-2048/+2047)
        if (abs(echR) > 16383 and iShift < 4)     // signed 16 bits (+/-32767) to signed 12 bits (+/-2047)
          iShift = 4;
        else if (abs(echR) > 8191 and iShift < 3) // signed 15 bits (+/-16383) to signed 12 bits (+/-2047)
          iShift = 3;
        else if (abs(echR) > 4095 and iShift < 2) // signed 14 bits (+/-8191)  to signed 12 bits (+/-2047)
          iShift = 2;
        else if (abs(echR) > 2047 and iShift < 1) // signed 13 bits (+/-4095)  to signed 12 bits (+/-2047)
          iShift = 1;
        // Passage des échantillons signés (-2048/+2047) en échantillons non signés (0/4095) avec atténuation pour rester dans le domaine du DAC
        echR = (echR >> iShift) + 2048;
        echR = max( echR, 0);
        echR = min( echR, 4095);
        // Init du buffer
        *pBuffer++ = (uint16_t)echR;
#endif
#if defined(__IMXRT1062__) // Teensy 4.1, sortie stéréo sur I2S 16 bits
        // By default, we consider that the samples are on 12 bits (+/-2047) and we pass them on 16 bits (+/-32767)
        echR = (echR << 4);
        // Test that we do not leave the domain of the 16 bits ADC
        if (abs(echR) > 262143 and iShift < 4)      // signed 20 bits (+/-524287) to signed 16 bits (+/-32767)
          iShift = 4;
        else if (abs(echR) > 131071 and iShift < 3) // signed 19 bits (+/-262143) to signed 16 bits (+/-32767)
          iShift = 3;
        else if (abs(echR) > 65535 and iShift < 2)  // signed 18 bits (+/-131071) to signed 16 bits (+/-32767)
          iShift = 2;
        else if (abs(echR) > 32767 and iShift < 1)  // signed 17 (+/-65535) bits to signed 16 bits (+/-32767)
          iShift = 1;
        echR = (echR >> iShift);
        echR = max( echR, -32767);
        echR = min( echR, 32767);
        // Init du buffer stéréo
        *pBuffer++ = (int16_t)echR;
        *pBuffer++ = (int16_t)echR;
        /*Serial.printf("%d, ", echR);
        if (i % 10 == 0)
          Serial.println(" ");*/
#endif
#ifdef TESTMINMAXECH
        uiMinEch = min(uiMinEch, samplesWave[i]);
        uiMaxEch = max(uiMaxEch, samplesWave[i]);
#endif
      }
    }
    // Calcul de la position de lecture
    lWaveEchRead += MAXSAMPLE_READ;
    float fDur    = (float)lWaveEchRead / (float)iFe;
    iWavePos      = (unsigned int)fDur;
    if (iWavePos > iWaveDuration)
      iWavePos = iWaveDuration;
    fDur = (float)iWavePos * 100.0 / (float)iWaveDuration;
    iPosLecture   = (unsigned short)fDur;
    if (iPosLecture > 100)
      iPosLecture = 100;
  }
  else
  {
    Serial.printf("SetBufferH BLOCK_SAMPLES %d, MAXSAMPLE_READ %d\n", BLOCK_SAMPLES, MAXSAMPLE_READ);
    int iEch = 0;
    // **** Mode hétérodyne
    // Lecture de 10 blocs d'échantillons depuis le fichier wave pour une décimation par 10 en mode hétérodyne
    // 10 * 4096 (32 * 128)
    for (int i=0; i<10; i++)
    {
      unsigned long lRead = waveFile.WavfileRead( samplesWave, lMax);
      //Serial.printf("%d read %d\n", i, lRead);
      if (lRead == sizeBuffer)
      {
        bOK = true;
        // Pour chaque échantillon du buffer
        for (int j=0; j<MAXSAMPLE_READ;)
        {
          //Serial.printf("j %d\n", j);
          // Pour un buffer de filtre FIR
          for (int k=0; k<BLOCK_SAMPLES; j+=iPlus, k++)
          {
            //Serial.printf("k %d\n", k);
            echR = samplesWave[j];
            if (bStereo)
            {
              // Gestion stéréo, la restitution étant mono, on mélange les deux canaux sur le canal droit
              echL = samplesWave[j+1];
              // Mélange des canaux
              echR = (echR + echL) / 2;
              echL = echR;
            }
#if defined(__MK66FX1M0__) // Teensy 3.6, sortie mono sur DAC 12 bits
            // Test that we do not leave the domain of the 12 bits ADC (-2048/+2047)
            if (abs(echR) > 16383 and iShift < 4)     // signed 16 bits (+/-32767) to signed 12 bits (+/-2047)
              iShift = 4;
            else if (abs(echR) > 8191 and iShift < 3) // signed 15 bits (+/-16383) to signed 12 bits (+/-2047)
              iShift = 3;
            else if (abs(echR) > 4095 and iShift < 2) // signed 14 bits (+/-8191)  to signed 12 bits (+/-2047)
              iShift = 2;
            else if (abs(echR) > 2047 and iShift < 1) // signed 13 bits (+/-4095)  to signed 12 bits (+/-2047)
              iShift = 1;
            // Passage des échantillons signés (-2048/+2047) en échantillons non signés (0/4095) avec atténuation pour rester dans le domaine du DAC
            echR = echR >> iShift;
            echR = max( echR, -2048);
            echR = min( echR, 2047);
            // Calcul hétérodyne
            echR = heterodyne.CalculHeterodyne( echR);
            samplesFirIn[k] = (q15_t)echR;
#endif
#if defined(__IMXRT1062__) // Teensy 4.1, sortie stéréo sur I2S 16 bits
            // By default, we consider that the samples are on 12 bits (+/-2047) and we pass them on 16 bits (+/-32767)
            echR = (echR << 4);
            // Test that we do not leave the domain of the 16 bits ADC
            if (abs(echR) > 262143 and iShift < 4)      // signed 20 bits (+/-524287) to signed 16 bits (+/-32767)
              iShift = 4;
            else if (abs(echR) > 131071 and iShift < 3) // signed 19 bits (+/-262143) to signed 16 bits (+/-32767)
              iShift = 3;
            else if (abs(echR) > 65535 and iShift < 2)  // signed 18 bits (+/-131071) to signed 16 bits (+/-32767)
              iShift = 2;
            else if (abs(echR) > 32767 and iShift < 1)  // signed 17 (+/-65535) bits to signed 16 bits (+/-32767)
              iShift = 1;
            echR = (echR >> iShift);
            echR = max( echR, -32767);
            echR = min( echR, 32767);
            // Préparation du filtre sélectif
            samplesFirSelIn[k] = (q15_t)echR;
#endif
          }
#if defined(__IMXRT1062__) // Teensy 4.1, sortie stéréo sur I2S 16 bits
#if defined(SELFILTERH10KHZ) || defined(SELFILTERH20KHZ)
          // Application du filtre sélectif
          SelectiveFilter.SetFIRFilter( (q15_t *)samplesFirSelIn, (q15_t *)samplesFirSelOut);
          for (int k=0; k<BLOCK_SAMPLES; k++)
          {
            // Application du calcul hétérodyne aux échantillons filtrés et préparation des échantillons du filtre passe bas
            samplesFirIn[k] = (q15_t)heterodyne.CalculHeterodyne( (int16_t)samplesFirSelOut[k]);
          }
#else
          for (int k=0; k<BLOCK_SAMPLES; k++)
          {
            // Application du calcul hétérodyne aux échantillons filtrés et préparation des échantillons du filtre passe bas
            samplesFirIn[k] = (q15_t)heterodyne.CalculHeterodyne( (int16_t)samplesFirSelIn[k]);
          }
#endif
#endif
          // Application du filtre passe bas par le DSP
          firFilter.SetFIRFilter( (q15_t *)samplesFirIn, (q15_t *)samplesFirOut);
          // Mémo réultat dans le buffer de lecture avec décimation par 10 et passage dans le domaine non signé de -2048/+2047 à 0/4095
          // Décimation par 10 en sortie de filtre hétérodyne
          int m = 0;
          for (int l=0; l<BLOCK_SAMPLES; l+=10, m++)
          {
            if (iEch < MAXSAMPLE_READ)
            {
#if defined(__MK66FX1M0__) // Teensy 3.6
              *pBuffer++ = (uint16_t)(samplesFirOut[l] + 2048);
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
              *pBuffer++ = (uint16_t)(samplesFirOut[l]);
              *pBuffer++ = (uint16_t)(samplesFirOut[l]);
#endif
            }
            iEch++;
            //Serial.printf("l %d, m %d\n", l, m);
          }
          //Serial.printf("i %d, j %d, k %d, iEch %d\n", i, j, mk, iEch);
        }
        bOK = true;
      }
      else
      {
        //Serial.println("Exit for");
        break;
      }
      //Serial.printf("iEch %d\n", iEch);
    }
    // Calcul de la position de lecture
    lWaveEchRead += (MAXSAMPLE_READ*10);
    float fDur    = (float)lWaveEchRead / (float)(iFe);
    iWavePos      = (unsigned int)fDur;
    if (iWavePos > iWaveDuration)
      iWavePos = iWaveDuration;
    fDur = (float)iWavePos * 100.0 / (float)iWaveDuration;
    iPosLecture   = (unsigned short)fDur;
    if (iPosLecture > 100)
      iPosLecture = 100;
    //Serial.println("SetBufferH OK");
  }
#if defined(__IMXRT1062__) // Teensy 4.1
  // Vide les données du cache vers la mémoire pour l'accès DMA
  //arm_dcache_delete( pMemBuffer, MAXSAMPLE_READ*2*sizeof(int16_t));
  arm_dcache_flush( pMemBuffer, MAXSAMPLE_READ*2*sizeof(int16_t));
  //arm_dcache_flush_delete( pMemBuffer, MAXSAMPLE_READ*2*sizeof(int16_t));
#endif
  /*unsigned long uiT2 = micros();
  Serial.printf("SetBuffer %dµs\n", uiT2 - uiT1);*/
  return bOK;
}

unsigned long uiTimePerIT = 0;
unsigned long uiNbITs = 0;
//-------------------------------------------------------------------------
// Fonction de traitement des interruption DMA pour remplir les buffers
void CReader::OnITDMA()
{
  /*if (uiTimePerIT == 0)
    uiTimePerIT = micros();
  else
  {
    uiNbITs++;
    Serial.printf("OnITDMA %d Per %dµs, 85320µs à 48kHz\n", uiNbITs, micros() - uiTimePerIT);
    uiTimePerIT = micros();
  }*/
  //Serial.println("CReader::OnITDMA");
  // Lecture de l'adresse courante de lecture du DMA
  uint32_t daddr = (uint32_t)restitution.GetCurrentDMAaddr();
  // Par défaut, 1er buffer
  uint16_t *P = (uint16_t *)P1;
  if (daddr < (uint32_t)P2)
    // Le DMA est dans le 1er buffer, on s'occupe du second
    P = (uint16_t *)P2; 
  // Initialisation du buffer
  if (!SetBuffer( P))
    // Fin du fichier, on stoppe la restitution
    StopRead();
  // RAZ IT DMA
  restitution.ClearDMAIsr();
}

/*------------------------------------------------------------------------------------------------------
 * Routine d'interruption du DMA de restitution, un buffer d'échantillons est disponible
 * Ne pas passer trop de temps dans le traitement d'une IT
 *------------------------------------------------------------------------------------------------------*/
#if defined(__MK66FX1M0__) // Teensy 3.6
void dma_ch3_isr()
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
void dma_I2S_isr()
#endif
{
/*#if defined(__MK66FX1M0__) // Teensy 3.6
  if ((DMA_INT & 8) == 0)
    Serial.println("dma_ch3_isr !!!");
  if ((DMA_INT & 8) != 0) // Only ISR channel 3
  {
#endif*/
    // Appel de la fonction de traitement
    if (CReader::pInstance != NULL)
      // Lecture d'un fichier wav
      CReader::pInstance->OnITDMA();
    else if (CGenericRecorder::pInstance != NULL)
      // Génération hétérodyne
      CGenericRecorder::pInstance->OnITDMA_Reader();
#if defined(__MK66FX1M0__) // Teensy 3.6
      // RAZ bit d'interruption du canal 3
      DMA_CINT = DMA_CINT_CINT(3); // (24.3.12) Clear interrupt request register
  //}
#endif
}


// Pointeur sur l'instance unique de la classe
CReader *CReader::pInstance = NULL;
