/* 
 * File:   WriteBMP16C.cpp
   PassiveRecorder Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "WriteBMP16C.h"

extern const unsigned char ib8x8u [];

#define SIZETXTLINE 11

//-------------------------------------------------------------------------
//! \class Graphe16C
//! \brief Classe pour la création d'un graphe dans une BMP 16 couleurs
//! \brief Coordonnées base 0 (0,0 = coin haut gauche)

//-------------------------------------------------------------------------
//! \brief Constructeur (initialisation de la classe)
//! \param width Largeur en pixel
//! \param height Hauteur en pixel
//! \param color Couleur de fond
//! \param dbMin Valeur min de l'échelle des dB
//! \param dbMax Valeur max de l'échelle des dB
//! \param dbStep Intervalle de l'échelle des dB
//! \param khzMin Valeur min de l'échelle des kHz
//! \param khzMax Valeur max de l'échelle des kHz
//! \param khzStep Intervelle de l'échelle des kHz
//! \param sTitle Titre du graphe
//! \param sName Nom de fichier bmp
Graphe16C::Graphe16C(
  int width,
  int height,
  int color,
  int dbMin,
  int dbMax,
  int dbStep,
  int khzMin,
  int khzMax,
  int khzStep,
  char *sTitle,
  char *sName
  )
{
  // Mémo des valeurs
  idBMin   = dbMin;
  idBMax   = dbMax;
  idBStep  = dbStep;
  ikHzMin  = khzMin;
  ikHzMax  = khzMax;
  ikHzStep = khzStep;
  psTitle  = sTitle;
  psName   = sName;
  BackgroundColor = color;
  yLegende = SIZETXTLINE;
  // Création de l'image
  pImage = new BMP16C(width, height, color);
  // Création de l'axe des dB
  AxedB();
  // Création de l'axe des kHz
  AxekHz();
  // Affichage du titre au milieux en haut
  int iSize = strlen(psTitle) * 8;
  int x = (width / 2) - (iSize / 2);
  pImage->fillRectangle( x - 10, 0, iSize + 10, 15, color);
  pImage->printString( x, 1, COLORBLACK, psTitle);
}

//-------------------------------------------------------------------------
//! \brief Destructeur
Graphe16C::~Graphe16C()
{
  // Destruction de l'image
  delete pImage;
}

//-------------------------------------------------------------------------
//! \brief Construction de l'axe des dB
void Graphe16C::AxedB()
{
  char temp[10];
  // Calcul du nombre de pixels de l'axe
  int iNbPixels = pImage->getHeight() - (MARGEBASSE + MARGEHAUTE);
  // Calcul de l'échelle de l'axe
  fdBPix = (idBMax - idBMin + 1) / (float)iNbPixels;
  // Ligne de l'axe
  pImage->drawLine( MARGEGAUCHE-1, MARGEHAUTE-1, MARGEGAUCHE-1, pImage->getHeight()-MARGEBASSE, COLORBLACK);
  // Echelle
  for (int idB=idBMin; idB<=idBMax; idB+=idBStep)
  {
    int y = getYdB( idB);
    // Texte
    sprintf( temp, "% 4d", idB);
    pImage->printString( 1, y-4, COLORBLACK, temp);
    // Tiret    
    pImage->drawLine( MARGEGAUCHE-TIRET, y, MARGEGAUCHE-1, y, COLORBLACK);
    if (idB > idBMin)
      // Ligne grise
      pImage->drawLine( MARGEGAUCHE+1, y, pImage->getWidth()-MARGEDROITE, y, COLORGREY2);
  }
}

//-------------------------------------------------------------------------
//! \brief Construction de l'axe des kHz
void Graphe16C::AxekHz()
{
  char temp[10];
  // Calcul du nombre de pixels de l'axe
  int iNbPixels = pImage->getWidth() - (MARGEGAUCHE + MARGEDROITE);
  // Calcul de l'échelle de l'axe
  fkHzPix  = (float)(ikHzMax - ikHzMin + 1) / (float)iNbPixels;
  // Ligne de l'axe
  pImage->drawLine( MARGEGAUCHE-1, pImage->getHeight()-MARGEBASSE, pImage->getWidth()-MARGEDROITE, pImage->getHeight()-MARGEBASSE, COLORBLACK);
  // Echelle
  for (int ikHz=ikHzMin; ikHz<=ikHzMax; ikHz+=ikHzStep)
  {
    int x = getXkHz( (float)ikHz);
    // Texte
    sprintf( temp, "%d", ikHz);
    int iLargeur = strlen( temp) * 8 / 2;
    pImage->printString( x-iLargeur, pImage->getHeight()-MARGEBASSE+TIRET+1, COLORBLACK, temp);
    // Tiret    
    pImage->drawLine( x, pImage->getHeight()-MARGEBASSE, x, pImage->getHeight()-MARGEBASSE+TIRET, COLORBLACK);
    if (ikHz > ikHzMin)
      // Ligne grise
      pImage->drawLine( x, pImage->getHeight()-MARGEBASSE-1, x, MARGEHAUTE, COLORGREY2);
  }
}

//-------------------------------------------------------------------------
//! \brief Construction d'une courbe
//! \param sLegende Légende de la courbe
//! \param color Couleur de la courbe
//! \param iType Type de la courbe
//! \param NbPoints Nombre de points
//! \param tpidB Tableau des valeurs du point en dB
//! \param tpfkHz Tableau des valeurs du point en kHz
void Graphe16C::SetGraphe(
  char *sLegende,
  int color,
  int iType,
  int NbPoints,
  int   *tbidB,
  float *tbfkHz
  )
{
  //Serial.printf("SetGraphe %d points\n", NbPoints);
  // Tracé de la courbe
  int x1, x2, y1, y2;
  // Recherche du 1er point dans le graphe
  for (int p=1; p<NbPoints; p++)
  {
    if (tbfkHz[p] >= ikHzMin and tbfkHz[p] <= ikHzMax and tbidB[p] >= idBMin and tbidB[p] <= idBMax)
    {
      // Calcul du 1er point
      x1 = getXkHz(tbfkHz[p]);
      y1 = getYdB(tbidB[p]);
      // Pour chaque autre point
      for (int i=p; i<NbPoints; i++)
      {
        if (tbfkHz[i] >= ikHzMin and tbfkHz[i] <= ikHzMax and tbidB[i] >= idBMin and tbidB[i] <= idBMax)
        {
          // Calcul du point
          x2 = getXkHz(tbfkHz[i]);
          y2 = getYdB(tbidB[i]);
          //if (i<5) Serial.printf("%fkHz, %ddB, x1 %d, y1 %d, x2 %d, y2 %d\n", tbfkHz[i], tbidB[i], x1, y1, x2, y2);
          // Traçage de la ligne
          pImage->drawLine( x1, y1, x2, y2, color);
          if (iType == TR_MAX)
            pImage->drawLine( x1, y1-3, x1+10, y1-10, color);
          else if (iType == TR_MIN and x1-10 > MARGEGAUCHE)
            pImage->drawLine( x1, y1+3, x1-10, y1+10, color);
          x1 = x2;
          y1 = y2;
        }
      }
      // Fin des points
      break;
    }
  }
  // Tracé de la légende
  //Serial.printf("Légende [%s]\n", sLegende);
  x1 = pImage->getWidth() / 3 * 2;
  x2 = x1 + 10;
  pImage->fillRectangle( x1 - 5, yLegende-1, pImage->getWidth()-x1-2, 10, BackgroundColor);
  pImage->drawLine( x1, yLegende+4, x2, yLegende+4, color);
  pImage->printString( x2+3, yLegende, color, sLegende);
  yLegende += SIZETXTLINE;
}

//-------------------------------------------------------------------------
//! \brief Retourne la valeur de y à partir d'un nombre de dB
//! \param dB Nombre de dB
int Graphe16C::getYdB( int dB)
{
  // Calcul du nombre de dB à partir du min
  int idB = dB;
  if (idB > idBMax)
    idB = idBMax;
  idB -= idBMin;
  // Calcul du nombre de pixels
  int y = idB / fdBPix;
  // Calcul de y à partir de la base du graphe
  y = pImage->getHeight()-MARGEBASSE - y;
  return y;
}

//-------------------------------------------------------------------------
//! \brief Retourne la valeur de x à partir d'un nombre de kHz
//! \param kHz Nombre de kHz
int Graphe16C::getXkHz( int kHz)
{
  // Calcul du nombre de kHz à partir du min
  int ikHz = kHz;
  if (ikHz > ikHzMax)
    ikHz = ikHzMax;
  ikHz -= ikHzMin;
  // Calcul du nombre de pixels
  int x = ikHz / fkHzPix;
  // Calcul de x à partir de la gauche du graphe
  x = MARGEGAUCHE + x;
  return x;
}

//-------------------------------------------------------------------------
//! \brief Ecriture du fichier sur la carte SD
void Graphe16C::WriteFile()
{
  char sFilename[50];
  sprintf( sFilename, "%s.bmp", psName);
  pImage->write( sFilename);
}

//-------------------------------------------------------------------------
//! \class BMP16C
//! \brief Classe pour la création d'un fichier image type BMP 16 couleurs

//-------------------------------------------------------------------------
//! \brief Constructeur (initialisation de la classe)
//! \param width Largeur en pixel
//! \param height Hauteur en pixel
//! \param color Couleur de fond
BMP16C::BMP16C(
  int width,
  int height,
  int color
  )
{
  iWidth  = width;
  iHeight = height;
  // Calcul de la taille des lignes en octets (modulo 4) et de la taille de la bmp
  iSizeLine = (iWidth / 2) + (iWidth / 2) % 4;
  iSizeBMP = iSizeLine * height;
  //Serial.printf("iWidth %d, iHeight %d, iSizeLine %d, iSizeBMP %d\n", iWidth, iHeight, iSizeLine, iSizeBMP);
  // Création du tableau de pixels
  pBMP = new unsigned char[iSizeBMP];
  memset( pBMP, 0, iSizeBMP);
  // Initialisation de la bitmap à la couleur de fond
  //fillRectangle( 0, 0, iWidth-1, iHeight-1, color);
}

//-------------------------------------------------------------------------
//! \brief Destructeur
BMP16C::~BMP16C()
{
  delete [] pBMP;
}

//-------------------------------------------------------------------------
//! \brief Initialise un pixel
//! \param x Position x du pixel
//! \param y Position y pixel
//! \param c Couleur du pixel
void BMP16C::setPixel(
  int x,
  int y,
  unsigned char c
)
{
  bool bOK = true;
  if( x < 0 || x >= (int)iWidth)
  {
    //Serial.printf("setPixel valeur invalide pour x %d\n", x);
    bOK = false;
  }
  if (y < 0 || y >= (int)iHeight)
  {
    //Serial.printf("setPixel valeur invalide pour y %d\n", y);
    bOK = false;
  }
  
  if(bOK)
  {
    // Calcul de l'indice de la ligne en octets (attention, ligne du haut tout en bas du tableau)
    uint32_t iLine = abs(iHeight - y - 1) * iSizeLine;
    // Calcul de l'indice du pixel dans la ligne avec indication si poids fort ou faible
    uint32_t iX = x / 2;
    bool bPFaible = false;
    if (x % 2 != 0)
      bPFaible = true;
    // Calcul de l'indice du pixel dans le tableau
    uint32_t idx = iLine + iX;
    // Init du pixel
    if (idx < iSizeBMP)
    {
      if (!bPFaible)
        pBMP[idx] = (pBMP[idx] & 0x0f) | (c << 4);
      else
        pBMP[idx] = (pBMP[idx] & 0xf0) | c;
    }
    /*else
      Serial.printf("setPixel indice invalide x %d, y %d, iLine %d, iX %d, idx %d, iSizeBMP %d, bPFaible %d\n", x, y, iLine, iX, idx, iSizeBMP, bPFaible);*/
  }
}

//-------------------------------------------------------------------------
//! \brief Ecriture d'un caractère
//! \param x Position x du coin haut gauche
//! \param y Position y du coin haut gauche
//! \param color Couleur de remplissage
//! \param c Caractère à afficher
bool BMP16C::printChar(
  int x,
  int y,
  unsigned char color,
  char c
  )
{
  unsigned char tbBits[] = {0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01};
  unsigned char *pFont = (unsigned char *)ib8x8u;
  int iSizeChar = 8;
  int iMax = 8;
  int idx;
  idx = iSizeChar * c;
  //Serial.printf("writechar iSizeChar %d, idx %d\n", iSizeChar, idx);
  for (int iy=0; iy<iMax; iy++)
  {
    //Serial.printf(" for y iy %d, 0x%02X\n", iy, pFont[idx+iy]);
    for (int ix=0; ix<iMax; ix++)
    {
      //Serial.printf("  iy %d, ix %d, idxBits %d, octet 0X%02X, tbBits 0X%02X, bit %d, x %d, y %d\n", iy, ix, idxBits, pFont[idx+iy], tbBits[ix] & pFont[idx+iy], x+ix, y+iy);
      if ((tbBits[ix] & pFont[idx+iy]) != 0)
        setPixel( x+ix, y+iy, color);
    }
  }
  return true;
}
  
//-------------------------------------------------------------------------
//! \brief Ecriture d'une chaine de caractères
//! \param x Position x du coin haut gauche
//! \param y Position y du coin haut gauche
//! \param color Couleur de remplissage
//! \param pStr Chaine de caractères à afficher
bool BMP16C::printString(
  int x,
  int y,
  unsigned char color,
  char *pStr
)
{
  int iSizeChar = 8;
  int ix = x;
  for (int i=0; i<(int)strlen(pStr); i++)
  {
    printChar( ix, y, color, pStr[i]);
    ix += iSizeChar;
  }
  return true;
}

//-------------------------------------------------------------------------
//! \brief Dessine une ligne d'un pixel d'épaisseur
//! \param x0, y0 Position x, y de départ
//! \param y1, y1 Position x, y d'arrivée
//! \param c Couleur de remplissage
void BMP16C::drawLine(
  int x0, int y0,
  int x1, int y1, 
  unsigned char c
)
{
  int xDelta = (x1 - x0);
  int yDelta = (y1 - y0);


  if(xDelta == 0)
  {
    // A vertical line
    
    if(y0 > y1)
    {
      int yn = y1;
      y1 = y0;
      y0 = yn;
    }
    
    for(int y = y0; y <= y1; y ++)
      setPixel(x0, y, c);
  }
  else if(yDelta == 0)
  {
    // A horizontal line
    
    if(x0 > x1)
    {
      int xn = x1;
      x1 = x0;
      x0 = xn;
    }
    
    for(int x = x0; x <= x1; x ++)
      setPixel(x, y0, c);
  }
  else
  {
    setPixel(x0, y0, c);
    
    int xStep = (xDelta < 0 ? -1 : 1);
    int yStep = (yDelta < 0 ? -1 : 1);
    
    xDelta = abs(xDelta) / 2;
    yDelta = abs(yDelta) / 2;

    if(xDelta >= yDelta)
    {
      int error = yDelta - 2 * xDelta;
      
      while(x0 != x1)
      {
        if(error >= 0 && (error || xStep > 0))
        {
          error -= xDelta;
          y0    += yStep;
        }
        error += yDelta;
        x0    += xStep;
        setPixel(x0, y0, c);
      }
    }
    else
    {
      int error = xDelta - 2 * yDelta;
      while(y0 != y1)
      {
        if(error >= 0 && (error || yStep > 0))
        {
          error -= yDelta;
          x0    += xStep;
        }
        error += xDelta;
        y0    += yStep;
        setPixel(x0, y0, c);
      }
    }
  }

}

//-------------------------------------------------------------------------
//! \brief Dessine un rectangle avec des lignes d'un pixel
//! \param x Position x du coin haut gauche
//! \param y Position y du coin haut gauche
//! \param width Largeur en pixel
//! \param height Hauteur en pixel
//! \param c Couleur des lignes
void BMP16C::drawRectangle(
  int x,
  int y,
  int width,
  int height,
  unsigned char c
)
{
  drawLine(x,         y,          x + width, y,          c);
  drawLine(x + width, y,          x + width, y + height, c);
  drawLine(x + width, y + height, x,         y + height, c);
  drawLine(x,         y + height, x,         y,          c);
}

//-------------------------------------------------------------------------
//! \brief Dessine un rectangle rempli
//! \param x Position x du coin haut gauche
//! \param y Position y du coin haut gauche
//! \param width Largeur en pixel
//! \param height Hauteur en pixel
//! \param c Couleur de remplissage
void BMP16C::fillRectangle(
  int x,
  int y,
  int width,
  int height,
  unsigned char c
)
{
  for(int i = 0; i < width; i ++)
    drawLine( x + i, y, x + i, y + height, c);
}

//-------------------------------------------------------------------------
//! \brief Retourne la largeur de l'image en pixel
int BMP16C::getWidth()
{
  return iWidth;
}

//-------------------------------------------------------------------------
//! \brief Retourne la hauteur de l'image en pixel
int BMP16C::getHeight()
{
  return iHeight;
}

//-------------------------------------------------------------------------
//! \brief Ecriture du fichier bmp sur la carte SD
//! \param fileName BMP file name
bool BMP16C::write(char *fileName)
{
  // Création de l'entête
  int iSizePalette = 16 * 4 + 4;
  BMP16C_Header header;
  memset( &header, 0, sizeof(BMP16C_Header));
  strcpy( header.BM, "BM");
  header.uiFileSize       = BMP16CHEADERSIZE + iSizePalette + iSizeBMP;
  header.uiOffset         = BMP16CHEADERSIZE + iSizePalette;
  header.uiBitmapInfoSize = 40;
  header.uiWidth          = iWidth;
  header.uiHeight         = iHeight;
  header.uiNbPlan         = 1;
  header.uiBitsByPixel    = 4;
  header.uiSizeOfPicture  = iSizeBMP;

  // Création de la palette de couleurs
  Color palette[NB_COLORS];
  palette[COLORWHITE   ] = Color::white();
  palette[COLORRED     ] = Color::red();
  palette[COLORGREEN   ] = Color::green();
  palette[COLORBLUE    ] = Color::blue();
  palette[COLORCYAN    ] = Color::cyan();
  palette[COLORMAGENTA ] = Color::magenta();
  palette[COLORYELLOW  ] = Color::yellow();
  palette[COLORBLACK   ] = Color::black();
  palette[COLORORANGE  ] = Color::orange();
  palette[COLORREDB    ] = Color::red();
  palette[COLORGREENB  ] = Color::green();
  palette[COLORBLUEB   ] = Color::blue();
  palette[COLORCYANB   ] = Color::cyan();
  palette[COLORMAGENTAB] = Color::magenta();
  palette[COLORGREY1   ] = Color(220, 220, 220);
  palette[COLORGREY2   ] = Color::grey();

  // Ouverture du fichier
#ifdef SDFAT_FILE_TYPE
  FsFile BMPfile;
#else
  File BMPfile;
#endif
  if (!BMPfile.open(fileName, O_RDWR | O_CREAT | O_TRUNC))
  {
    Serial.printf( "Ouverture [%s] impossible !", fileName);
    return false;
  }
  // Ecriture de l'entête
  BMPfile.write( header.BM, 2);
  BMPfile.write( &header.uiFileSize, 4);
  BMPfile.write( &header.uiReserved, 4);
  BMPfile.write( &header.uiOffset, 4);
  BMPfile.write( &header.uiBitmapInfoSize, 4);
  BMPfile.write( &header.uiWidth, 4);
  BMPfile.write( &header.uiHeight, 4);
  BMPfile.write( &header.uiNbPlan, 2);
  BMPfile.write( &header.uiBitsByPixel, 2);
  BMPfile.write( &header.uiCompression, 4);
  BMPfile.write( &header.uiSizeOfPicture, 4);
  BMPfile.write( &header.uiHorizontalResolution, 4);
  BMPfile.write( &header.uiVerticalResolution, 4);
  BMPfile.write( &header.uiColorsNumber, 4);

  // Number of important colors (0 = all)
  uint32_t uiZero = 0;
  BMPfile.write( &uiZero, 4);
  // Ecriture palette
  for (int i=0; i<NB_COLORS; i++)
  {
    BMPfile.write( &(palette[i].iBlue), 1);
    BMPfile.write( &(palette[i].iGreen), 1);
    BMPfile.write( &(palette[i].iRed), 1);
    BMPfile.write( &(palette[i].iAlpha), 1);
  }
  
  // Boucle d'écriture de l'image par block de 36384 octest
  uint32_t iReste = iSizeBMP;
  uint32_t iBlock = 36384;
  if (iSizeBMP < iBlock)
    iBlock = iSizeBMP;
  int i = 0;
  while( true)
  {
    // Ecriture d'un bloc
    BMPfile.write( &pBMP[i], iBlock);
    iReste -= iBlock;
    i += iBlock;
    if (iReste <= 0)
      break;
    else if (iReste < iBlock)
      iBlock = iReste;
  }
  // Fermeture du fichier
  BMPfile.flush();
  BMPfile.close();
  return true;
}

const unsigned char ib8x8u [] = {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7E, 0x81, 0xA5, 0x81, 0xBD, 0x99, 0x81, 0x7E, 0x7E, 0xFF, 0xDB, 0xFF, 0xC3, 0xE7, 0xFF, 0x7E, 0x00, 0x6C, 0xFE, 0xFE, 0xFE, 0x7C, 0x38, 0x10, 
  0x00, 0x10, 0x38, 0x7C, 0xFE, 0x7C, 0x38, 0x10, 0x38, 0x7C, 0x38, 0xFE, 0xFE, 0x7C, 0x38, 0x7C, 0x10, 0x10, 0x38, 0x7C, 0xFE, 0x7C, 0x38, 0x7C, 0x00, 0x00, 0x00, 0x00, 0x60, 0xF0, 0xF0, 0x60, 
  0xFF, 0xFF, 0xE7, 0xC3, 0xC3, 0xE7, 0xFF, 0xFF, 0x00, 0x00, 0x78, 0xCC, 0x84, 0x84, 0xCC, 0x78, 0xFF, 0xC3, 0x99, 0xBD, 0xBD, 0x99, 0xC3, 0xFF, 0x0F, 0x07, 0x0F, 0x7D, 0xCC, 0xCC, 0xCC, 0x78, 
  0x78, 0xCC, 0xCC, 0xCC, 0x78, 0x30, 0xFC, 0x30, 0x3F, 0x33, 0x3F, 0x30, 0x30, 0x70, 0xF0, 0xE0, 0x7F, 0x63, 0x7F, 0x63, 0x63, 0x67, 0xE6, 0xC0, 0x99, 0x5A, 0x3C, 0xE7, 0xE7, 0x3C, 0x5A, 0x99, 
  0x00, 0x80, 0xE0, 0xF8, 0xFE, 0xF8, 0xE0, 0x80, 0x00, 0x02, 0x0E, 0x3E, 0xFE, 0x3E, 0x0E, 0x02, 0x30, 0x78, 0xFC, 0x30, 0x30, 0xFC, 0x78, 0x30, 0x00, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0x00, 0xCC, 
  0x00, 0x7F, 0xDB, 0xDB, 0x7B, 0x1B, 0x1B, 0x1B, 0x3E, 0x63, 0x38, 0x6C, 0x6C, 0x38, 0xCC, 0x78, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFC, 0xFC, 0xFC, 0x18, 0x3C, 0x7E, 0x18, 0x7E, 0x3C, 0x18, 0xFF, 
  0x00, 0x30, 0x78, 0xFC, 0x30, 0x30, 0x30, 0x30, 0x00, 0x30, 0x30, 0x30, 0x30, 0xFC, 0x78, 0x30, 0x00, 0x00, 0x00, 0x18, 0x0C, 0xFE, 0x0C, 0x18, 0x00, 0x00, 0x00, 0x30, 0x60, 0xFE, 0x60, 0x30, 
  0x00, 0x00, 0x00, 0x00, 0xC0, 0xC0, 0xC0, 0xFE, 0x00, 0x00, 0x00, 0x24, 0x66, 0xFF, 0x66, 0x24, 0x00, 0x00, 0x00, 0x18, 0x3C, 0x7E, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x7E, 0x3C, 0x18, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 0xF0, 0xF0, 0x60, 0x60, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0xD8, 0xD8, 0xD8, 0x00, 0x6C, 0x6C, 0xFE, 0x6C, 0xFE, 0x6C, 0x6C, 
  0x00, 0x30, 0x7C, 0xC0, 0x78, 0x0C, 0xF8, 0x30, 0x00, 0x00, 0xC6, 0xCC, 0x18, 0x30, 0x66, 0xC6, 0x00, 0x38, 0x6C, 0x38, 0x76, 0xDC, 0xCC, 0x76, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 0x60, 0xC0, 
  0x00, 0x30, 0x60, 0xC0, 0xC0, 0xC0, 0x60, 0x30, 0x00, 0xC0, 0x60, 0x30, 0x30, 0x30, 0x60, 0xC0, 0x00, 0x00, 0x00, 0x66, 0x3C, 0xFF, 0x3C, 0x66, 0x00, 0x00, 0x00, 0x30, 0x30, 0xFC, 0x30, 0x30, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 0x60, 0xC0, 0x00, 0x00, 0x00, 0x00, 0xFC, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xC0, 0x00, 0x06, 0x0C, 0x18, 0x30, 0x60, 0xC0, 0x80, 
  0x00, 0x7C, 0xC6, 0xCE, 0xDE, 0xF6, 0xE6, 0x7C, 0x00, 0x30, 0x70, 0x30, 0x30, 0x30, 0x30, 0xFC, 0x00, 0x78, 0xCC, 0x0C, 0x38, 0x60, 0xC4, 0xFC, 0x00, 0x78, 0xCC, 0x0C, 0x38, 0x0C, 0xCC, 0x78, 
  0x00, 0x1C, 0x3C, 0x6C, 0xCC, 0xFE, 0x0C, 0x1E, 0x00, 0xFC, 0xC0, 0xF8, 0x0C, 0x0C, 0xCC, 0x78, 0x00, 0x38, 0x60, 0xC0, 0xF8, 0xCC, 0xCC, 0x78, 0x00, 0xFC, 0xCC, 0x0C, 0x18, 0x30, 0x30, 0x30, 
  0x00, 0x78, 0xCC, 0xCC, 0x78, 0xCC, 0xCC, 0x78, 0x00, 0x78, 0xCC, 0xCC, 0x7C, 0x0C, 0x18, 0x70, 0x00, 0x00, 0xC0, 0xC0, 0x00, 0x00, 0xC0, 0xC0, 0x00, 0x00, 0x60, 0x60, 0x00, 0x60, 0x60, 0xC0, 
  0x00, 0x18, 0x30, 0x60, 0xC0, 0x60, 0x30, 0x18, 0x00, 0x00, 0xFC, 0x00, 0x00, 0xFC, 0x00, 0x00, 0x00, 0xC0, 0x60, 0x30, 0x18, 0x30, 0x60, 0xC0, 0x00, 0x78, 0xCC, 0x0C, 0x18, 0x30, 0x00, 0x30, 
  0x00, 0x7C, 0xC6, 0xDE, 0xDE, 0xDE, 0xC0, 0x78, 0x00, 0x30, 0x78, 0xCC, 0xCC, 0xFC, 0xCC, 0xCC, 0x00, 0xFC, 0x66, 0x66, 0x7C, 0x66, 0x66, 0xFC, 0x00, 0x3C, 0x66, 0xC0, 0xC0, 0xC0, 0x66, 0x3C, 
  0x00, 0xF8, 0x6C, 0x66, 0x66, 0x66, 0x6C, 0xF8, 0x00, 0xFE, 0x62, 0x68, 0x78, 0x68, 0x62, 0xFE, 0x00, 0xFE, 0x62, 0x68, 0x78, 0x68, 0x60, 0xF0, 0x00, 0x3C, 0x66, 0xC0, 0xC0, 0xCE, 0x66, 0x3E, 
  0x00, 0xCC, 0xCC, 0xCC, 0xFC, 0xCC, 0xCC, 0xCC, 0x00, 0xF0, 0x60, 0x60, 0x60, 0x60, 0x60, 0xF0, 0x00, 0x1E, 0x0C, 0x0C, 0x0C, 0xCC, 0xCC, 0x78, 0x00, 0xE6, 0x66, 0x6C, 0x78, 0x6C, 0x66, 0xE6, 
  0x00, 0xF0, 0x60, 0x60, 0x60, 0x62, 0x66, 0xFE, 0x00, 0xC6, 0xEE, 0xFE, 0xFE, 0xD6, 0xC6, 0xC6, 0x00, 0xC6, 0xE6, 0xF6, 0xDE, 0xCE, 0xC6, 0xC6, 0x00, 0x38, 0x6C, 0xC6, 0xC6, 0xC6, 0x6C, 0x38, 
  0x00, 0xFC, 0x66, 0x66, 0x7C, 0x60, 0x60, 0xF0, 0x00, 0x78, 0xCC, 0xCC, 0xCC, 0xDC, 0x78, 0x1C, 0x00, 0xFC, 0x66, 0x66, 0x7C, 0x6C, 0x66, 0xE6, 0x00, 0x78, 0xCC, 0xE0, 0x70, 0x1C, 0xCC, 0x78, 
  0x00, 0xFC, 0xB4, 0x30, 0x30, 0x30, 0x30, 0x78, 0x00, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xFC, 0x00, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0x78, 0x30, 0x00, 0xC6, 0xC6, 0xC6, 0xD6, 0xFE, 0xEE, 0xC6, 
  0x00, 0xC6, 0xC6, 0x6C, 0x38, 0x38, 0x6C, 0xC6, 0x00, 0xCC, 0xCC, 0xCC, 0x78, 0x30, 0x30, 0x78, 0x00, 0xFE, 0xC6, 0x8C, 0x18, 0x32, 0x66, 0xFE, 0x00, 0xF0, 0xC0, 0xC0, 0xC0, 0xC0, 0xC0, 0xF0, 
  0x00, 0xC0, 0x60, 0x30, 0x18, 0x0C, 0x06, 0x02, 0x00, 0xF0, 0x30, 0x30, 0x30, 0x30, 0x30, 0xF0, 0x00, 0x10, 0x38, 0x6C, 0xC6, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 
  0xC0, 0xC0, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x78, 0x0C, 0x7C, 0xCC, 0x76, 0x00, 0xE0, 0x60, 0x60, 0x7C, 0x66, 0x66, 0xDC, 0x00, 0x00, 0x00, 0x78, 0xCC, 0xC0, 0xCC, 0x78, 
  0x00, 0x1C, 0x0C, 0x0C, 0x7C, 0xCC, 0xCC, 0x76, 0x00, 0x00, 0x00, 0x78, 0xCC, 0xFC, 0xC0, 0x78, 0x00, 0x38, 0x6C, 0x60, 0xF0, 0x60, 0x60, 0xF0, 0x00, 0x00, 0x76, 0xCC, 0xCC, 0x7C, 0x0C, 0xF8, 
  0x00, 0xE0, 0x60, 0x6C, 0x76, 0x66, 0x66, 0xE6, 0x00, 0x60, 0x00, 0xE0, 0x60, 0x60, 0x60, 0xF0, 0x0C, 0x00, 0x0C, 0x0C, 0x0C, 0xCC, 0xCC, 0x78, 0x00, 0xE0, 0x60, 0x66, 0x6C, 0x78, 0x6C, 0xE6, 
  0x00, 0xE0, 0x60, 0x60, 0x60, 0x60, 0x60, 0xF0, 0x00, 0x00, 0x00, 0xCC, 0xFE, 0xFE, 0xD6, 0xC6, 0x00, 0x00, 0x00, 0xF8, 0xCC, 0xCC, 0xCC, 0xCC, 0x00, 0x00, 0x00, 0x78, 0xCC, 0xCC, 0xCC, 0x78, 
  0x00, 0x00, 0xDC, 0x66, 0x66, 0x7C, 0x60, 0xF0, 0x00, 0x00, 0x76, 0xCC, 0xCC, 0x7C, 0x0C, 0x1E, 0x00, 0x00, 0x00, 0xDC, 0x76, 0x66, 0x60, 0xF0, 0x00, 0x00, 0x00, 0x7C, 0xC0, 0x78, 0x0C, 0xF8, 
  0x00, 0x20, 0x60, 0xF8, 0x60, 0x60, 0x68, 0x30, 0x00, 0x00, 0x00, 0xCC, 0xCC, 0xCC, 0xCC, 0x76, 0x00, 0x00, 0x00, 0xCC, 0xCC, 0xCC, 0x78, 0x30, 0x00, 0x00, 0x00, 0xC6, 0xD6, 0xFE, 0xFE, 0x6C, 
  0x00, 0x00, 0x00, 0xC6, 0x6C, 0x38, 0x6C, 0xC6, 0x00, 0x00, 0xCC, 0xCC, 0xCC, 0x7C, 0x0C, 0xF8, 0x00, 0x00, 0x00, 0xFC, 0x98, 0x30, 0x64, 0xFC, 0x00, 0x1C, 0x30, 0x30, 0xE0, 0x30, 0x30, 0x1C, 
  0x00, 0xC0, 0xC0, 0xC0, 0x00, 0xC0, 0xC0, 0xC0, 0x00, 0xE0, 0x30, 0x30, 0x1C, 0x30, 0x30, 0xE0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x76, 0xDC, 0x00, 0x10, 
 };
