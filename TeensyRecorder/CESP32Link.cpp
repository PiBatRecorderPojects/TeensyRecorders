//-------------------------------------------------------------------------
//! \file CESP32Link.cpp
//! \brief Classe de gestion de la liaison avec l'ESP32 pour les enregistrements stéréo synchro
//! \details class ESP32Link
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "CESP32Link.h"
#include "ModesModifiers.h"

/*
 * CESP32Link
 *   CMasterLink
 *   CSlaveLink
 */

// Time (ms) between salves to send Slave OK message
/*
 * Temps de transmission (Time On Air) d'un message LORA sur https://www.loratools.nl/#/airtime
 * Paramètres SPF 7 à 12, Band 125kHz, Coding Rate 5, Preambule 8, No CRC, No Explicit header
 * Taille des messages LoRa (octets) et temps de transmission avec les paramètres LoRa (ms) en fonction du Spreading Factor
 *        octets  SF 7   SF 8    SF 9
 * LMSLAVE     7, 30,98  51,71  103,42
 */
#define TIMEBETWEENSLAVES 150
 
//-------------------------------------------------------------------------
//! \class CESP32Link
//! \brief Classe de gestion de la liaison avec l'ESP32 pour les enregistrements stéréo synchro
//! \brief Gère le cas Master comme les cas Slave n. Gère la liaison série et les liaisons par fils

//-------------------------------------------------------------------------
//! \brief Constructor (initialization of parameters to default values)
CESP32Link::CESP32Link()
{
  bDebug = false;
  //bDebug = true;
  iPRType = 0;
  iESP32Status = ES_OK;
  for (int i=0; i<MAXMS; i++)
    iStatus[i] = MSSINIT;
  iPos = 0;
  // No test link
  iTimeTest = 0;
  // To test link
  //iTimeTest = millis() + 1000;
  bFirst = false;
  bEnd = false;
  iTimeLoop = millis();
  iTimePrintLoop = millis()+5000;
  iTimeOut = 0;
  iTimeSecondmessage = 0;
}

//-------------------------------------------------------------------------
//! \brief Destructor
CESP32Link::~CESP32Link()
{
  Serial2.end();
  digitalWriteFast( PIN23ESPON,  LOW); // EPS32 OFF
}

//-------------------------------------------------------------------------
//! \brief Set type of the recorder and begin link
//! \param iType Master or Slave n (enum MASTERSLAVES)
//! \param pPar  Parameter to send if Master or to modify if Slave
void CESP32Link::SetLinkType(
  int iType,
  ParamsOperator *pPar
  )
{
  iPRType = iType;
  pParams = pPar;
  // Set serial link
  Serial2.begin(19200);
  Serial2.clear();
  iPos = 0;
  bFirst = false;
  bEnd = false;
  digitalWriteFast( PIN23ESPON,  HIGH); // EPS32 ON
  iTimeStatus = millis() + TIMESTATUSESP + TIMESTATUSESP / 2;
}

//-------------------------------------------------------------------------
//! \brief Reset Slaves status
void CESP32Link::ResetStatus()
{
  iStatus[MASTER] = MSWAITINGESP32;
  for (int i=SLAVE01; i<MAXMS; i++)
    iStatus[i] = MSSINIT;
}

//-------------------------------------------------------------------------
//! \brief Set status
//! \param iNewStatus New status of the PR Type
void CESP32Link::SetStatus(
  int iNewStatus
  )
{
  iStatus[iPRType] = iNewStatus;
}

//-------------------------------------------------------------------------
//! \brief Test link on loop
void CESP32Link::OnLoop()
{
  if (iTimeStatus != 0 and millis() > iTimeStatus)
  {
    if (millis()-iTimeLoop > TIMESTATUSESP)
      Serial.printf("Appel loop pas assez rapide ! %dms\n", millis()-iTimeLoop);
    else
    {
      // Timeout on ESP32 status message
      if (iESP32Status != ES_TIMEOUT)
        LogFile::AddLog( LLOG, "ESP32 Error time out !");
      iESP32Status = ES_TIMEOUT;
    }
  }
  if (iTimeTest != 0 and millis() > iTimeTest)
  {
    char *pMessParams = link.CodeTest();
    Serial.printf("CSlaveLink::OnLoop send Test [%s]\n", pMessParams);
    Serial2.println( pMessParams);
    iTimeTest = millis() + 1000;
  }
  if (iTimeSecondmessage != 0 and millis() > iTimeSecondmessage)
  {
    char *pMessStopMode = link.CodeStopMode();
    if (bDebug) Serial.printf("CESP32Link::StopMode second message [%s]\n", pMessStopMode);
    Serial2.println( pMessStopMode);
    iTimeSecondmessage = 0;
  }
  iTimeLoop = millis();
  if (bDebug and millis() > iTimePrintLoop)
  {
    Serial.println("CESP32Link::OnLoop");
    iTimePrintLoop = millis()+5000;
  }
}

//-------------------------------------------------------------------------
//! \brief Send Stop Mode
void CESP32Link::StopMode()
{
  char *pMessStopMode = link.CodeStopMode();
  if (bDebug) Serial.printf("CESP32Link::StopMode [%s]\n", pMessStopMode);
  Serial2.println( pMessStopMode);
  // To send a second message
  iTimeSecondmessage = millis() + 200;
  LogFile::AddLog( LLOG, "Synchro mode, Send Stop mode");
}

//-------------------------------------------------------------------------
//! \brief Requesting the software version of the ESP32
void CESP32Link::VersionRequest()
{
  iStatus[iPRType] = MSWAITINGVERS;
  char *pMessParams = link.CodeVersionRequest();
  if (bDebug) Serial.printf("CMasterLink::VersionRequest [%s]\n", pMessParams);
  Serial2.println( pMessParams);
}
  
//-------------------------------------------------------------------------
//! \brief Processing message receptions
//! \return Returns the type of the received message (LMUNKNOW if not)
int CESP32Link::OnReceive()
{
  int iReturn = LMUNKNOW;
  //Serial.println("CESP32Link::OnReceive");
  while (Serial2.available())
  {
    //Serial.println("CESP32Link::OnReceive read");
    char cCar = (char)Serial2.read();
    if (cCar == '!' and bFirst)
    {
      // End of a message
      bEnd = true;
      sMessageReception[iPos]   = cCar;
      sMessageReception[iPos+1] = 0;
      break;
    }
    else if (cCar == '$')
    {
      bFirst = true;
      iPos   = 0;
    }
    //Serial.printf("Serial %c, iPos %d, bFirst %d, bEnd %d\n", cCar, iPos, bFirst, bEnd);
    if (bFirst)
    {
      sMessageReception[iPos] = cCar;
      iPos++;
      if (iPos > 120)
      {
        // RAZ message en cours
        Serial2.clear();
        iPos   = 0;
        bFirst = false;
        bEnd   = false;
        break;
      }
    }
  }
  if (bEnd)
  {
    //Serial.println(" ");
    if (bDebug) Serial.printf("CESP32Link::OnReceive [%s], iPos %d\n", sMessageReception, iPos);
    // Decoding the message
    iReturn = link.DecodeMessageType( sMessageReception);
    // Préparation prochain message
    iPos   = 0;
    bFirst = false;
    bEnd   = false;
  }
  return iReturn;
}

//-------------------------------------------------------------------------
//! \brief Processing message receptions
//! \param iMessage the type of the received message (LinkMessages)
void CESP32Link::MessageProcessing(
  int iMessage
  )
{
  int iNewESP32Status;
  char *pVersion;
  bool bMaster = false;
  switch (iMessage)
  {
  case LMTEST:      // Test
    Serial.println("CESP32Link::MessageProcessing Test message");
    break;
  case LMSVERSION: // ESP32 Software version
    pVersion = link.DecodeVersion( sMessageReception);
    if (strlen(pVersion) > 10) pVersion[9] = 0;
    strcpy(sESP32Version, pVersion);
    Serial.printf("CESP32Link::MessageProcessing ESP32 Software version: %s\n", sESP32Version);
    break;
  case LMSTATUS:
    if (iPRType == MASTER)
      bMaster = true;
    iNewESP32Status = link.DecodeESP32Status( bMaster, sMessageReception);
    if (iNewESP32Status > ES_OK and iNewESP32Status != iESP32Status)
    {
      switch(iNewESP32Status)
      {
      case ES_ERLORA   : LogFile::AddLog( LLOG, "ESP32 Error on LORA !"); break;
      case ES_ERTOP    : LogFile::AddLog( LLOG, "ESP32 Error on DAC Top !"); break;
      case ES_ERPARAM  : LogFile::AddLog( LLOG, "ESP32 Error decoding params message !"); break;
      case ES_ERSLAVE  : LogFile::AddLog( LLOG, "ESP32 Error decoding Slave message !"); break;
      case ES_ERCRC    : LogFile::AddLog( LLOG, "ESP32 Error CRC !"); break;
      case ES_ERSERMESS: LogFile::AddLog( LLOG, "ESP32 Error decoding num message Serial Link !"); break;
      case ES_ERLORMESS: LogFile::AddLog( LLOG, "ESP32 Error decoding num message Lora Link !"); break;
      case ES_TIMEOUT  : LogFile::AddLog( LLOG, "ESP32 Error time out !"); break;
      case ES_MSSTATUS : LogFile::AddLog( LLOG, "ESP32 Inconsistency on the master or slave state !"); break;
      default          :
      case ES_UNKNOWER : LogFile::AddLog( LLOG, "ESP32 Unknow error %d! [%s]", iNewESP32Status, sMessageReception); break;
      }
    }
    iESP32Status = iNewESP32Status;
    iTimeStatus = millis() + TIMESTATUSESP + TIMESTATUSESP / 2;
    break;
  }
}

//! false after switch on, indicate if config is validate by operator
bool CESP32Link::bValidConfig = false;

// ESP32 software version
char CESP32Link::sESP32Version[10];

//-------------------------------------------------------------------------
//! \class CMasterLink
//! \brief Gère le cas Master

//-------------------------------------------------------------------------
//! \brief Constructor (initialization of parameters to default values)
CMasterLink::CMasterLink():CESP32Link()
{
}

//-------------------------------------------------------------------------
//! \brief Destructor
CMasterLink::~CMasterLink()
{
  
}

//-------------------------------------------------------------------------
//! \brief Set type of the recorder and begin link
//! \param iType Master or Slave n (enum MASTERSLAVES)
//! \param pPar  Parameter to send if Master or to modify if Slave
void CMasterLink::SetLinkType(
  int iType,
  ParamsOperator *pPar
  )
{
  //digitalWriteFast( 13, HIGH);
  // Set ESP32 Master
  digitalWriteFast( PIN04MASTER, HIGH); // Master
  // Call basic function (ESP32 On)
  CESP32Link::SetLinkType( iType, pPar);
  // Wait for ESP32 ready and prepare timeout 3s
  iStatus[iPRType] = MSWAITINGESP32;
  iTimeOut = millis() + 3000;
}

//-------------------------------------------------------------------------
//! \brief Set valid configuration by operator
void CMasterLink::SetValidConfig()
{
  for (int i=SLAVE01; i<MAXMS; i++)
    if (iStatus[i] ==  MSSLAVEOK)
      bConfig[i] = true;
  bValidConfig = true;
}

unsigned long iDiff;
//-------------------------------------------------------------------------
//! \brief Test link on loop
void CMasterLink::OnLoop()
{
  //Serial.printf("CMasterLink::OnLoop, iPRType %d, iStatus %d\n", iPRType, iStatus[iPRType]);
  if (iStatus[iPRType] == MSWAITINGESP32)
  {
    //Serial.println("CMasterLink::OnLoop");
    if (digitalReadFast(PIN22ESPREADY) == LOW)
    {
      Serial.println("Détection ESP32 ON OK");
      iTimeStatus = 0;
      // Wait 1s
      iStatus[iPRType] = MSWAITEESP32;
      iTimeOut = millis() + 1000;
      iDiff = millis();
      if (bDebug) Serial.printf("Attente 1s %d\n", iDiff);
    }
    else if (iTimeOut != 0 and millis() > iTimeOut)
    {
      // Timeout waiting ESP32 on
      iStatus[iPRType] = MSTIMEOUTESP32;
      iTimeOut = 0;
    }
  }
  else if (iStatus[iPRType] == MSWAITEESP32 and iTimeOut != 0 and millis() > iTimeOut)
  {
    iDiff = millis() - iDiff;
    if (bDebug)
      Serial.printf("Attente 300ms OK %d, diff %dms\n", millis(), iDiff);
    //Serial.printf("Emission params iLEDSynchro %d\n", pParams->iLEDSynchro);
    // Send parameters for all Slaves and prepare timeout for 3 minutes
    char *pMessParams = link.CodeParams(pParams);
    if (bDebug) Serial.printf("CMasterLink::OnLoop ESP32 Ready, send params [%s]\n", pMessParams);
    Serial2.println( pMessParams);
    iStatus[iPRType] = MSWAITINGSLAVES;
    // Time out waiting slaves and operator (3mn)
    iTimeOut = millis() + 60*3*1000;
    // Time to send a second message
    iTimeSecondmessage = millis() + 10 * TIMEBETWEENSLAVES;
    LogFile::AddLog( LLOG, "Synchro mode, Send parameters [%s]", pMessParams);
  }
  else if (iStatus[iPRType] == MSWAITINGSLAVES)
  {
    // Waiting slaves messages
    int iMessage = OnReceive();
    if (iMessage != LMUNKNOW)
      MessageProcessing( iMessage);
    else if (iTimeOut != 0 and millis() > iTimeOut)
    {
      // Timeout waiting slaves
      iStatus[iPRType] = MSTIMEOUTSLAVES;
      iTimeOut = 0;
    }
    else if (iTimeSecondmessage != 0 and millis() > iTimeSecondmessage)
    {
      // Send second params message
      iTimeSecondmessage = 0;
      char *pMessParams = link.CodeParams(pParams);
      if (bDebug) Serial.printf("CMasterLink::OnLoop ESP32 Ready, send second params [%s]\n", pMessParams);
      Serial2.println( pMessParams);
    }
    // Test si configuration complète
    if (bValidConfig)
    {
      bool bOK = true;
      for (int i=SLAVE01; i<MAXMS; i++)
        // Si l'esclave fait parti de la config et qu'il n'est pas OK, alors config non complète
        if (bConfig[i] and iStatus[i] != MSSLAVEOK)
          bOK = false;
      if (bOK)
        // Master OK
        iStatus[iPRType] = MSMASTEROK;
    }
  }
  else
  {
    int iMessage = OnReceive();
    if (iMessage != LMUNKNOW)
      MessageProcessing( iMessage);
  }
  CESP32Link::OnLoop();
}

//-------------------------------------------------------------------------
//! \brief Processing message receptions
//! \param iMessage the type of the received message (LinkMessages)
void CMasterLink::MessageProcessing(
  int iMessage
  )
{
  int iSlave;
  bool bOK;
  switch (iMessage)
  {
  case LMSLAVE :  // Slave OK
    iSlave = -1;
    if (link.DecodeSlave( &iSlave, &bOK, sMessageReception))
    {
      if (bOK and iSlave > -1)
        iStatus[iSlave] = MSSLAVEOK;
      else if (iStatus[iSlave] != MSSLAVEOK)
        iStatus[iSlave] = MSSLAVEERROR;
    }
    else if (iSlave > -1 and iStatus[iSlave] != MSSLAVEOK)
      iStatus[iSlave] = MSSLAVEERROR;
    LogFile::AddLog( LLOG, "Synchro mode, Receiving the status of a slave [%s]", sMessageReception);
    break;
  default      :
    CESP32Link::MessageProcessing( iMessage);
    break;
  }
}

//! Esclaves présents en fonctions de la configuration validée
bool CMasterLink::bConfig[MAXMS] = {false, false, false, false, false, false, false, false, false, false};

//-------------------------------------------------------------------------
//! \class CSlaveLink
//! \brief Gère le cas Slave

//-------------------------------------------------------------------------
//! \brief Constructor (initialization of parameters to default values)
CSlaveLink::CSlaveLink():CESP32Link()
{
  bParamsOK = false;
}

//-------------------------------------------------------------------------
//! \brief Destructor
CSlaveLink::~CSlaveLink()
{
}

//-------------------------------------------------------------------------
//! \brief Set type of the recorder and begin link
//! \param iType Master or Slave n (enum MASTERSLAVES)
//! \param pPar  Parameter to send if Master or to modify if Slave
void CSlaveLink::SetLinkType(
  int iType,
  ParamsOperator *pPar
  )
{
  // Set ESP32 Salve
  digitalWriteFast( PIN04MASTER, LOW); // Slave
  // Call basic function (ESP32 On)
  CESP32Link::SetLinkType( iType, pPar);
  // Wait for ESP32 ready and prepare timeout 2s
  iStatus[iPRType] = MSWAITINGESP32;
  iTimeOut = millis() + 2000;
}

//-------------------------------------------------------------------------
//! \brief Test link on loop
void CSlaveLink::OnLoop()
{
  if (iStatus[iPRType] == MSWAITINGESP32)
  {
    if (digitalReadFast(PIN22ESPREADY) == LOW)
    {
      if (bDebug) Serial.println("CSlaveLink::OnLoop ESP32 ready");
      iTimeStatus = 0;
      // Waiting parameters, no timeout
      iStatus[iPRType] = MSWAITINGPARAMS;
      iTimeOut = 0;
    }
    else if (iTimeOut != 0 and millis() > iTimeOut)
    {
      if (bDebug) Serial.println("CSlaveLink::OnLoop Timeout waiting ESP32");
      // Timeout waiting ESP32
      iStatus[iPRType] = MSTIMEOUTESP32;
      iTimeOut = 0;
    }
  }
  else if (iStatus[iPRType] == MSSLAVESTOP and iTimeOut != 0 and millis() > iTimeOut)
  {
    // Return to init waiting parameters from master
    iStatus[iPRType] = MSWAITINGPARAMS;
    iTimeOut = 0;
    bParamsOK = false;
  }
  else if (iStatus[iPRType] == MSWAITINGSEND and iTimeOut != 0 and millis() > iTimeOut)
  {
    char *pMessSlaveOK = link.CodeSlave( iPRType, bParamsOK);
    if (bDebug) Serial.printf("CSlaveLink::OnLoop Send Slave OK [%s]\n", pMessSlaveOK);
    // Send Slave OK
    Serial2.println( pMessSlaveOK);
    if (bParamsOK)
      iStatus[iPRType] = MSSLAVEOK;
    else
      iStatus[iPRType] = MSSLAVEERROR;
    iTimeOut = 0;
    LogFile::AddLog( LLOG, "Synchro mode, Sending the status of a slave [%s]", pMessSlaveOK);
  }
  else
  {
    // Slave waiting message
    int iMessage = OnReceive();
    if (iMessage != LMUNKNOW)
      MessageProcessing( iMessage);
  }
  CESP32Link::OnLoop();
}

//-------------------------------------------------------------------------
//! \brief Processing message receptions
//! \param iMessage the type of the received message (LinkMessages)
void CSlaveLink::MessageProcessing(
  int iMessage
  )
{
  switch (iMessage)
  {
  case LMPARAMS:  // Receive parameters
    if (bDebug) Serial.printf("CSlaveLink::MessageProcessing LMPARAMS {%s]\n", sMessageReception);
    if (link.DecodeParams( pParams, sMessageReception))
      bParamsOK = true;
    else
      Serial.print(link.GetErrorString());
    Serial.printf("Réception params iLEDSynchro %d\n", pParams->iLEDSynchro);
    // Timeout to send SLAVEOK (250ms * nSlave, one message every 250ms for 9 slaves maximum = 2.5s)
    iStatus[iPRType] = MSWAITINGSEND;
    iTimeOut = millis() + TIMEBETWEENSLAVES*iPRType;
    LogFile::AddLog( LLOG, "Synchro mode, Receiving parameters [%s]", sMessageReception);
    break;
  case LMSTOPMODE:  // Stop mode
    if (bDebug) Serial.println("CSlaveLink::MessageProcessing MSSLAVESTOP");
    iStatus[iPRType] = MSSLAVESTOP;
    iTimeOut = millis() + 1000;
    LogFile::AddLog( LLOG, "Synchro mode, Receiving Stop mode");
    break;
  default      :
    if (bDebug)
      Serial.printf("CSlaveLink::MessageProcessing = %d\n", iMessage);
    CESP32Link::MessageProcessing( iMessage);
    break;
  }
}
 
