/* 
 * File:   ModePlayer.cpp
   Teensy Recorder Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "SdFat.h"
#include <U8g2lib.h>
#include <algorithm>
#include "CModePlayer.h"
#include "CModeParams.h"

// Screen manager
extern U8G2 *pDisplay;

// SD Card Manager
extern SdFs sd;

//-------------------------------------------------------------------------
// Player mode management class

const char *txtStr[]       = {"%s",
                              "%s",
                              "%s",
                              "%s"};
const char *txtInt[]       = {"%04ds",
                              "%04ds",
                              "%04ds",
                              "%04ds"};
extern const char *txtNoFile[];
const char formIDXTPLAY[]  =  "[&$$$$]";
const char formIDXTPLAYL[] =  "[&]";
const char *txtIDXTPLAY[]  = {"[%s]",
                              "[%s]",
                              "[%s]",
                              "[%s]"};
extern const char *txtTPLAY[MAXLANGUAGE][4];
extern const char *txtTPLAYL[MAXLANGUAGE][4];
const char formIDXPFHET[]   =  "[&$$$$$]";
const char *txtIDXPFHET[]   = {"[%06.2fk]",
                               "[%06.2fk]",
                               "[%06.2fk]",
                               "[%06.2fk]"};
const char formIDXDELFILE[]       =  "         &";
extern const char *txtIDXDELFILE[];
extern const char *txtYesNo[MAXLANGUAGE][2];

// Player mode screen
//   12345678901234567890  y
// 1 [>][Player Bar][Bat]  0
// 2 0000s   9999s[ X10 ]  9
// 2 0000s   9999s[120.5]  9
// 3 >Axxxxxxxxxxxxxx.wav  18
// 4  Bxxxxxxxxxxxxxx.wav  27
// 5  Cxxxxxxxxxxxxxx.wav  36
// 6  Dxxxxxxxxxxxxxx.wav  45
// 7  Exxxxxxxxxxxxxx.wav  54
// X 000000000000000001111
//   001123344566778990012
//   062840628406284062840
// Keys : K_UP K_DOWN If break Select file Else Fast forward / Fast back
//        K_PUSH      If Play Go to begining file and play
//        K_MODEB     Stop/Start Play
//        K_MODEA     Change mode

// Informations for test
extern int iBat;

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CModePlayer::CModePlayer():Reader( &paramsOperateur)
{
  iSelectedFile = -1;
  iPlayerType = TYPEX10;
  fHeterodyne = 40.0;
  strcpy(sFe, "0.0");
  bTypeModeInLine = false;
  bDelCurrentFile = false;
  bYesDelFile = false;
  // IDXHBAT                 Internal battery charge level in %
  LstModifParams.push_back(  new CBatteryModifier( &iNivBatLiPo, 18, 8, 106, 0));
  // IDXPLPLAY               Play / Break Modifier
  LstModifParams.push_back(  new CPlayRecModifier( &iPlayBreak, true, false, 24, 9, 0, 0));
  // IDXPLPOS                Bargraph of the reading position
  LstModifParams.push_back(  new CDrawBarModifier( &iFilePercent, 52, 7, 25, 1));
  // IDXPLDUR                Duration of the file in reading
  LstModifParams.push_back(  new CModificatorInt( "&$$  ", false, txtInt, &iFileDuration, 0, 9999, 1, 40, 9));
  // IDXPLRPOS               Reading position in seconds
  LstModifParams.push_back(  new CModificatorInt( "&$$  ", false, txtInt, &iFilePosition, 0, 9999, 1, 0, 9));
  // IDXPLTYPE               Reading type, X1, X10, heterodyne or Del
  LstModifParams.push_back(  new CEnumModifier ( formIDXTPLAY, false, true, txtIDXTPLAY, &iPlayerType, MAXPTYPE, (const char **)txtTPLAY, 71, 9));
  // IDXPLFRHET              Heterodyne frequency
  LstModifParams.push_back(  new CFloatModifier( formIDXPFHET,  false, txtIDXPFHET, &fHeterodyne,  8.0, 125.0, 0.25, 71, 9));
  // IDXDELFILE              Deleting current file
  LstModifParams.push_back(  new CBoolModifier ( formIDXDELFILE, false, false, txtIDXDELFILE, &bYesDelFile, (const char **)txtYesNo, 0, 9));
  // IDXPLTYPEL              Reading type, X1, X10, heterodyne or Del, mode in line
  LstModifParams.push_back(  new CLineEnumModifier ( formIDXTPLAYL, false, false, txtIDXTPLAY, &iPlayerType, MAXPTYPE, (const char **)txtTPLAYL, 0, 9));
  // IDXPLWAVA to IDXPLWAVE  Wav file A to E
  char temp[MAX_LINEPARAM+1];
  for (int i=0; i<MAXLINESWAVE; i++)
  {
    LstModifParams.push_back(new CCharModifier( " &$$$$$$$$$$$$$$$$$$$", true, txtStr, (char *)LstFilesScreen[i], MAX_LINEPARAM, false, false, false, false, false, 0, 0));
    sprintf( temp, " File%dxxxx.wav", i+1);
    strcpy( LstFilesScreen[i], temp);
  }
  // Initialization
  LstModifParams[IDXDELFILE]->SetbValid( false);
  LstModifParams[IDXPLTYPEL]->SetbValid( false);
  for (int i=0; i<IDXPLWAVA; i++)
    LstModifParams[i]->SetEditable( false);
  iPlayBreak    = PBREAK;
  iFileDuration = 0;
  iFilePosition = 0;
  iFilePercent  = 0;
}

//-------------------------------------------------------------------------
// Beginning of the mode
void CModePlayer::BeginMode()
{
  // Call of the parent method
  CModeGeneric::BeginMode();
  // List the wav files in the root directory
  ListFiles();
  // First line position
  iYFirstLine = 18;
  // Set lines files
  InitLstAffFiles();
  InitWavFile();
  // Test battery
  CheckBatteries(MAX_WAITMESURE);
  uiTimeCheck = millis() + TIMECHECKBATTERY;
  // Heterodyne wheel ADC Initialization
  ADC0Init( true);
  fStepADC  = 110.0 / 4096.0;
  if (bAutoplay and lstWaveFiles.size() != 0 and iSelectedFile > -1)
    // Automatic playback of the last recorded file
    StartRead();
}

//-------------------------------------------------------------------------
// End of the mode
void CModePlayer::EndMode()
{
  Reader.StopRead();
  lstWaveFiles.clear();
  bAutoplay = false;
  CModeGeneric::EndMode();
}

//-------------------------------------------------------------------------
// To display information outside of modifiers
void CModePlayer::AddPrint()
{
  // Read heterodyne wheel (<1ms)
  uint16_t uiVal = ADC0Read( ANALOGPINHETERODYNE);
  fHeterodyne = 10.0 + (float)uiVal * fStepADC;
  //Serial.printf("fStepADC %f, uiVal %d, fHeterodyne %f\n", fStepADC, uiVal, fHeterodyne);
  if (iPlayerType == TYPEHET and iPlayBreak == PPLAY)
    // Mode hétérodyne
    Reader.SetFreqHeterodyne( fHeterodyne);
  if (iPlayBreak == PPLAY)
  {
    // Test if still playing
    if (Reader.IsReading())
    {
      iFilePosition = Reader.GetWavePos();         // Position en secondes de la lecture du fichier courant
      iFilePercent  = Reader.GetPosLecture();      // Position en pourcent de la lecture du fichier courant
    }
    else
    {
      iPlayBreak    = PBREAK;
      iFilePosition = 0;
      iFilePercent  = 0;
    }
  }
  if (iSelectedFile != -1)
  {
    // Sample frequency display
    pDisplay->setFont(u8g2_font_5x7_mf);
    pDisplay->setCursor( 79, 0);
    pDisplay->print( sFe);
    // Restitution de la fonte normale
    pDisplay->setFont(u8g2_font_6x10_mf);
  }
  if (millis() > uiTimeCheck)
  {
    if (!max17043.isOK())
    {
      // En absence du module max, mesure classique de la batterie
      CheckBatteries();
      // Next time for check batteries
      uiTimeCheck = millis() + TIMECHECKBATTERY;
      // Mesure temperature
      LectureTemperature();
      // Init ADC for hétérodyne
      ADC0Init( true);
    }
    else if (max17043.isOK() and !bMAXError)
    {
      // Module Max présent et non en erreur
      if (!bMAXPowerOn)
      {
        // Power On MAX and wait 125ms for mesure
        CheckBatteries(MAX_POWER);
        uiTimeCheck = millis() + 125;
      }
      else
      {
        bool bOK = false;
        if (max17043.isOK())
          // Test battery
          bOK = CheckBatteries(MAX_MESURE);
        else
          // Test battery
          bOK = CheckBatteries(MAX_WAITMESURE);
        if (!bOK)
        {
          LogFile::AddLog( LLOG, "Error CheckBatteries !");
          bMAXError = true;
        }
        else
          // Next time for check batteries
          uiTimeCheck = millis() + TIMECHECKBATTERY;
      }
      // Mesure temperature
      LectureTemperature();
      // Init ADC for hétérodyne
      ADC0Init( true);
    }
  }
#if defined(__IMXRT1062__) // Teensy 4.1
#ifdef WITHBLUETOOTH
  if (commonParams.bBluetooth)
  {
    // Dessin Blutooth
    int x=99, y=0;  // Coin haut gauche, dimensions 4x8 pixels
    pDisplay->drawLine( x+2, y  , x+2, y+8);
    pDisplay->drawLine( x+2, y  , x+4, y+2);
    pDisplay->drawLine( x+4, y+2, x  , y+6);
    pDisplay->drawLine( x+2, y+8, x+4, y+6);
    pDisplay->drawLine( x+4, y+6, x  , y+2);
  }
#endif
#endif
}

//-------------------------------------------------------------------------
// Keyboard order processing
// If the key is a mode change key, returns the requested mode
// This method is called regularly by the main loop
// By default, handles modifiers
int CModePlayer::KeyManager(
  unsigned short key  // Touch to treat
  )
{
  int iNewMode = NOMODE;
  switch( key)
  {
  case K_UP  :
    // If Del Current File Else If break Select file Else Fast forward / Fast back
    if (bDelCurrentFile or bTypeModeInLine)
      CModeGeneric::KeyManager(key);
    else if (iPlayBreak == PBREAK)
    {
      // Select files
      iSelectedFile++;
      if (iSelectedFile >= (int)lstWaveFiles.size())
        iSelectedFile = (int)lstWaveFiles.size() - 1;
      InitLstAffFiles();
      InitWavFile();
    }
    else
    {
      if (Reader.ReadNext( 1) == false)
      {
        StopRead();
        iFilePosition = 0;
        iFilePercent  = 0;
      }
      else
      {
        iFilePosition = Reader.GetWavePos();         // Position en secondes de la lecture du fichier courant
        iFilePercent  = Reader.GetPosLecture();      // Position en pourcent de la lecture du fichier courant
      }
    }
    break;
  case K_DOWN:
    // If Del Current File Else If break Select file Else Fast forward / Fast back
    if (bDelCurrentFile or bTypeModeInLine)
      CModeGeneric::KeyManager(key);
    else if (iPlayBreak == PBREAK)
    {
      // Select files
      iSelectedFile--;
      if (iSelectedFile < 0)
        iSelectedFile = 0;
      InitLstAffFiles();
      InitWavFile();
    }
    else
    {
      if (Reader.ReadNext( -1) == false)
      {
        StopRead();
        iFilePosition = 0;
        iFilePercent  = 0;
      }
      else
      {
        iFilePosition = Reader.GetWavePos();         // Position en secondes de la lecture du fichier courant
        iFilePercent  = Reader.GetPosLecture();      // Position en pourcent de la lecture du fichier courant
      }
    }
    break;
  case K_PUSH:
    if (bDelCurrentFile)
    {
      // Parameter Del current file is show
      LstModifParams[IDXDELFILE]->SetParam();
      LstModifParams[IDXDELFILE]->StopModif();
      bModifParam = false;
      idxSelParam = idxSelFile;
      bDelCurrentFile = false;
      LstModifParams[IDXPLRPOS ]->SetbValid( true );
      LstModifParams[IDXPLDUR  ]->SetbValid( true );
      LstModifParams[IDXDELFILE]->SetbValid( false);
      if (iPlayerType != TYPEHET)
      {
        // Mode expansion de temps X10 ou X1
        LstModifParams[IDXPLTYPE ]->SetbValid( true);
        LstModifParams[IDXPLFRHET]->SetbValid(false);
      }
      else
      {
        // Mode hétérodyne
        LstModifParams[IDXPLTYPE ]->SetbValid(false);
        LstModifParams[IDXPLFRHET]->SetbValid( true);
      }
      if (bYesDelFile)
      {
        // Delete current file
        sd.remove( (char *)lstWaveFiles[iSelectedFile].c_str());
        bYesDelFile = false;
        // Initialize file list
        ListFiles();
        InitLstAffFiles();
        InitWavFile();
      }
      else
      {
        // No delete file, return in first mode (X1 or X10)
        int iMemo = iPlayerType;
        do
        {
          iPlayerType++;
          if (iMemo == iPlayerType)
            break;
          if (iPlayerType >= MAXPTYPE)
            iPlayerType = 0;
        } while (!((CEnumModifier *)LstModifParams[IDXPLTYPE])->GetElemValidity(iPlayerType));
      }
    }
    else if (bTypeModeInLine)
    {
      // Parameter Current Type is show
      LstModifParams[IDXPLTYPEL]->SetParam();
      LstModifParams[IDXPLTYPEL]->StopModif();
      bModifParam = false;
      idxSelParam = idxSelFile;
      bTypeModeInLine = false;
      LstModifParams[IDXPLRPOS ]->SetbValid( true );
      LstModifParams[IDXPLDUR  ]->SetbValid( true );
      LstModifParams[IDXPLTYPEL]->SetbValid( false);
      if (iPlayerType != TYPEHET)
      {
        // Mode expansion de temps X10 ou X1
        LstModifParams[IDXPLTYPE ]->SetbValid( true);
        LstModifParams[IDXPLFRHET]->SetbValid(false);
      }
      else
      {
        // Mode hétérodyne
        LstModifParams[IDXPLTYPE ]->SetbValid(false);
        LstModifParams[IDXPLFRHET]->SetbValid( true);
      }
      if (iPlayerType == TYPEDEL)
      {
        // Set mode del current file
        bDelCurrentFile = true;
        bModifParam = true;
        idxSelFile = idxSelParam;
        LstModifParams[IDXDELFILE]->StartModif();
        idxSelParam = IDXDELFILE;
        LstModifParams[IDXPLRPOS ]->SetbValid( false);
        LstModifParams[IDXPLDUR  ]->SetbValid( false);
        LstModifParams[IDXPLTYPE ]->SetbValid( false);
        LstModifParams[IDXPLFRHET]->SetbValid( false);
        LstModifParams[IDXDELFILE]->SetbValid( true );
      }
      else
        SetReadMode();
    }
    else if (CModeGeneric::GetRecorderType() == PASSIVE_RECORDER and lstWaveFiles.size() != 0 and iSelectedFile > -1)
    {
      // Stop/Start Play
      if (iPlayBreak == PBREAK)
        // Start play
        StartRead();
      else
        // Stop play
        StopRead();
    }
    // If Play Go to begining file and play
    else if (iPlayBreak == PPLAY)
    {
      // Go to begining file and play
      iFilePosition = 0;
      iFilePercent  = 0;
      StopRead();
      StartRead();
    }
    else if (!bTypeModeInLine and lstWaveFiles.size() != 0 and iSelectedFile > -1)
    {
      // Show parameter for player type
      LstModifParams[IDXPLRPOS ]->SetbValid( false);
      LstModifParams[IDXPLDUR  ]->SetbValid( false);
      LstModifParams[IDXPLTYPE ]->SetbValid( false);
      LstModifParams[IDXPLFRHET]->SetbValid( false);
      LstModifParams[IDXPLTYPEL]->SetbValid( true );
      bTypeModeInLine = true;
      bModifParam = true;
      idxSelFile = idxSelParam;
      LstModifParams[IDXPLTYPEL]->StartModif();
      idxSelParam = IDXPLTYPEL;
    }
    break;
  case K_MODEB:
    if (iPlayerType == TYPEDEL and !bDelCurrentFile)
    {
      // Stop mode del current file
      bDelCurrentFile = false;
      LstModifParams[IDXDELFILE]->StopModif();
      idxSelParam = idxSelFile;
    }
    else if (lstWaveFiles.size() != 0 and iSelectedFile > -1)
    {
      // Stop/Start Play
      if (iPlayBreak == PBREAK)
        // Start play
        StartRead();
      else
        // Stop play
        StopRead();
    }
    break;
  case K_MODEA:
    // Player is only in Active Recorder
    if (bAutoplay)
      // Return to Heterodyne mode
      iNewMode = MHETER;
    else
      // Swap to parameters mode : Record mode -> Player -> Parameters -> Record Mode
      iNewMode = MPARAMS;
    delay(1000);
    break;
  default:
    // To manage selected file
    CGenericMode::KeyManager( key);
  }
  return iNewMode;
}

//-------------------------------------------------------------------------
// Initializing the selected wav file
// Return true if file OK
bool CModePlayer::InitWavFile()
{
  bool bOK = false;
  if (lstWaveFiles.size() > 0 and iSelectedFile < (int)lstWaveFiles.size())
  {
    if (CModeGeneric::bDebug)
      Serial.printf("CModePlayer::InitWavFile select %d [%s]\n", iSelectedFile, lstWaveFiles[iSelectedFile].c_str());
    bOK = Reader.SetWaveFile( (char *)lstWaveFiles[iSelectedFile].c_str());
    if (!bOK)
    {
      // Seconde tentative
      delay(500);
      bOK = Reader.SetWaveFile( (char *)lstWaveFiles[iSelectedFile].c_str());
      if (!bOK)
      {
        if (commonParams.iLanguage == LFRENCH)
          LogFile::AddLog( LLOG, "Reader.SetWaveFile erreur lecture fichier [%s]", lstWaveFiles[iSelectedFile].c_str());
        else
          LogFile::AddLog( LLOG, "Reader.SetWaveFile error reading file [%s]", lstWaveFiles[iSelectedFile].c_str());
        bOK = false;
      }
    }
    if (bOK)
      SetReadMode();
  }
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief Set read mode
void CModePlayer::SetReadMode()
{
  /* Playback modes depending on the sampling frequency
   * Samp Fr  Het X10 X1
   *  500000   Y   Y  N
   *  384000   Y   Y  N
   *  250000   Y   Y  N
   *  192000   Y   Y  Y
   *   96000   N   N  Y
   *   48000   N   N  Y
   *   44100   N   N  Y
   *   38400   Y   Y  N
   *   25000   Y   Y  N
   *   24000   N   N  Y
   *   19200   Y   Y  N
   */
  // Management of the sampling frequency
  bool bHet=false, bX10=false, bX1=false;
  int iFe = Reader.GetFeFile();
  switch (iFe)
  {
  case 500000: bHet=true ; bX10=true ; bX1=false; break;
  case 384000: bHet=true ; bX10=true ; bX1=false; break;
  case 250000: bHet=true ; bX10=true ; bX1=false; break;
  case 192000: bHet=true ; bX10=true ; bX1=true ; break;
  case  96000: bHet=false; bX10=false; bX1=true ; break;
  case  48000: bHet=false; bX10=false; bX1=true ; break;
  case  44100: bHet=false; bX10=false; bX1=true ; break;
  case  50000: bHet=true ; bX10=true ; bX1=false; break;
  case  38400: bHet=true ; bX10=true ; bX1=false; break;
  case  25000: bHet=true ; bX10=true ; bX1=false; break;
  case  24000: bHet=false; bX10=false; bX1=true ; break;
  case  19200: bHet=true ; bX10=true ; bX1=false; break;
  }
  if (iPlayerType == TYPEDEL)
    iPlayerType = TYPEX1;
  if (iPlayerType == TYPEX10 and !bX10)
    iPlayerType = TYPEX1;
  else if (iPlayerType == TYPEHET and !bHet)
    iPlayerType = TYPEX1;
  else if (iPlayerType == TYPEX1 and !bX1)
    iPlayerType = TYPEX10;
  if (iPlayerType == TYPEX10 and iFe > 190000)
    iFe = iFe / 10;
  else if (iPlayerType != TYPEX10 and iFe < 48000)
    iFe = iFe * 10;
  bool bSetH = false;
  if (iPlayerType == TYPEHET)
    bSetH = true;
  ((CEnumModifier *)LstModifParams[IDXPLTYPE] )->SetElemValidity(TYPEX1,  bX1);
  ((CEnumModifier *)LstModifParams[IDXPLTYPE] )->SetElemValidity(TYPEX10, bX10);
  ((CEnumModifier *)LstModifParams[IDXPLTYPE] )->SetElemValidity(TYPEHET, bHet);
  ((CEnumModifier *)LstModifParams[IDXPLTYPEL])->SetElemValidity(TYPEX1,  bX1);
  ((CEnumModifier *)LstModifParams[IDXPLTYPEL])->SetElemValidity(TYPEX10, bX10);
  ((CEnumModifier *)LstModifParams[IDXPLTYPEL])->SetElemValidity(TYPEHET, bHet);
  //Serial.printf("CModePlayer::SetReadMode, Fe wav %d, Fe %d, iPlayerType %d, bX1 %d, bX10 %d, bHet %d, bSetH %d\n", Reader.GetFeFile(), iFe, iPlayerType, bX1, bX10, bHet, bSetH);
  Reader.SetReadMode( iFe, bSetH);
  // Init file Duration
  iFileDuration = Reader.GetWaveDuration();    // Durée max en secondes du fichier courant
  // Set sample frequency string
  float fFe = (float)Reader.GetFe() / 1000;
  if (bX10 and !bSetH)
    // X10, on restitue la bonne Fe
    sprintf(sFe, "%.0fk", fFe * 10);
  else
    sprintf(sFe, "%.0fk", fFe);
  if (!bTypeModeInLine and !bDelCurrentFile)
  {
    if (bSetH)
    {
      LstModifParams[IDXPLTYPE ]->SetbValid(false);
      LstModifParams[IDXPLFRHET]->SetbValid(true );
    }
    else
    {
      LstModifParams[IDXPLTYPE ]->SetbValid( true);
      LstModifParams[IDXPLFRHET]->SetbValid(false);
    }
  }
}

//-------------------------------------------------------------------------
// Start reading the wav file
void CModePlayer::StartRead()
{
  if (commonParams.iLanguage == LFRENCH)
    LogFile::AddLog( LLOG, "Lancement lecture [%s]", lstWaveFiles[iSelectedFile].c_str());
  else
    LogFile::AddLog( LLOG, "Start reading [%s]", lstWaveFiles[iSelectedFile].c_str());
  Reader.StartRead();
  iPlayBreak    = PPLAY;
}

//-------------------------------------------------------------------------
// Stop reading the wav file
void CModePlayer::StopRead()
{
  Reader.StopRead();
  if (commonParams.iLanguage == LFRENCH)
    LogFile::AddLog( LLOG, "Arrêt lecture opérateur");
  else
    LogFile::AddLog( LLOG, "Operator stop reading");
  iPlayBreak    = PBREAK;
}

//-------------------------------------------------------------------------
// Returns the string to display, taking only the n most significant characters
// 123456789012345678901
//  PR001_190126_221812.wav
//  PR001_190126_221812
String CModePlayer::PrepareStrFile(
  String str // Chaine à vérifier
  )
{
  String newStr = str;
  // Copie des 20 derniers caractères (sauf .wav)
  int max  = MAX_LINEPARAM - 1;
  if ((int)newStr.length() > max and (int)newStr.length() <= max+4)
    // On enlève .wav
    newStr = newStr.substring( 0, max);
  else if ((int)newStr.length() > max)
  {
    // On enlève .wav
    newStr = newStr.substring( 0, newStr.length()-4);
    // On enlève les 1er caractères
    newStr = newStr.substring( newStr.length()-max);
  }
  else if ((int)newStr.length() < max)
  {
    // On complète a 15 caractéres éventuellement
    while ((int)newStr.length() < max)
      newStr += " ";
  }
  return newStr;
}

//-------------------------------------------------------------------------
// Initializes the list of displayed files according to the 1st selected
void CModePlayer::InitLstAffFiles()
/*
 * 7   0
 * 6 < 1
 * 5   2         2
 * 4   3    3    3
 * 3   4    4  < 4
 * 2        5    5
 * 1      < 6    6
 * 0        7
 */
{
  int iOldSelect = idxSelParam;
  if (lstWaveFiles.size() == 0)
  {
    // No files !
    //Serial.println("CModePlayer::InitLstAffFiles No files");
    strcpy( LstFilesScreen[0], txtNoFile[GetCommonsParams()->iLanguage]);
    LstModifParams[IDXPLWAVA]->SetbValid( true);
    for (int i=1; i<MAXLINESWAVE; i++)
      LstModifParams[IDXPLWAVA+i]->SetbValid( false);
    if (iOldSelect >= 0)
      LstModifParams[iOldSelect ]->SetbSelect(false);
    idxSelParam = -1;
    iSelectedFile = -1;
  }
  else
  {
    // Files, Attention, the first file is the last of the list (sorted in ascending alphabetical order)
    int iFirst, iLast, iNb;
    iNb = (int)lstWaveFiles.size();
    if ((iNb - iSelectedFile - 1) < MIDDLELINEWAV)
    {
      iFirst = iNb - 1;
      iLast  = iFirst - MAXLINESWAVE + 1;
      if (iLast < 0)
        iLast = 0;
      idxSelParam = IDXPLWAVA + iNb - iSelectedFile - 1;
      //Serial.printf("A iNb %d, iFirst %d, iLast %d, iSelectedFile %d, idxSelParam %d, iOldSelect %d, iFirstIdxLine %d, iLastIdxLine %d\n", iNb, iFirst, iLast, iSelectedFile, idxSelParam, iOldSelect, iFirstIdxLine, iLastIdxLine);
    }
    else if (iSelectedFile < MIDDLELINEWAV)
    {
      iFirst = MAXLINESWAVE - 1;
      iLast  = 0;
      if (iFirst > (iNb-1))
        iFirst = iNb - 1;
      idxSelParam = IDXPLWAVA + (iFirst - iSelectedFile);
      //Serial.printf("B iNb %d, iFirst %d, iLast %d, iSelectedFile %d, idxSelParam %d, iOldSelect %d\n", iNb, iFirst, iLast, iSelectedFile, idxSelParam, iOldSelect);
    }
    else
    {
      iFirst = iSelectedFile + MIDDLELINEWAV - 1;
      iLast  = iSelectedFile - MIDDLELINEWAV + 1;
      idxSelParam = IDXPLWAVA + MIDDLELINEWAV - 1;
      //Serial.printf("C iNb %d, iFirst %d, iLast %d, iSelectedFile %d, idxSelParam %d, iOldSelect %d\n", iNb, iFirst, iLast, iSelectedFile, idxSelParam, iOldSelect);
    }
    //Serial.printf("iFirstVisibleIdxLine %d, iLastVisibleIdxLine %d\n", iFirstVisibleIdxLine, iLastVisibleIdxLine);
    for (int i=0, j=iFirst; i<MAXLINESWAVE; i++, j--)
    {
      if (j >= iLast)
      {
        // Prepare first char
        strcpy( LstFilesScreen[i], " ");
        String str = PrepareStrFile( lstWaveFiles[j]);
        char sTemp[MAX_LINEPARAM+1];
        str.toCharArray( sTemp, MAX_LINEPARAM);
        strcat( LstFilesScreen[i], sTemp);
        // File, line is valid
        LstModifParams[IDXPLWAVA+i]->SetbValid( true);
        iLastVisibleIdxLine = IDXPLWAVA + i;
        //Serial.printf("ligne %d wav %d [%s]\n", i, j, LstFilesScreen[i]);
      }
      else
      {
        //                          12345678901234567890
        strcpy( LstFilesScreen[i], "                   ");
        // No file, no valid line
        LstModifParams[IDXPLWAVA+i]->SetbValid( false);
        //Serial.printf("ligne %d vide\n", i);
      }
    }
  }
  if (iOldSelect >= IDXPLWAVA and iOldSelect <= IDXPLWAVE)
    LstModifParams[iOldSelect ]->SetbSelect(false);
  if (idxSelParam >= IDXPLWAVA and idxSelParam <= IDXPLWAVE)
    LstModifParams[idxSelParam]->SetbSelect( true);
}

//-------------------------------------------------------------------------
// List the wav files in the root directory
void CModePlayer::ListFiles()
{
  lstWaveFiles.clear();
  char name[80];
  char namelwr[80];
  FsFile file;
  FsFile root;
  // Select root directory
  if (!root.open("/"))
    Serial.printf("CModeGeneric::ListFilesErreur sd.chdir(/)\n");
  int iNbWav = 0;
  // Directory files are searched
  while (file.openNext(&root, O_RDONLY))
  {
    // Recovering the file name
    file.getName( name, 80);
    strcpy( namelwr, name);
    strlwr( namelwr);
    // We do not take into account the "System Volume Information" directory nor directories and files other than wav
    if (strcmp( name, "System Volume Information") != 0 and file.isDir() == false and strstr(namelwr, ".wav") != NULL)
    {
      //Serial.printf("%d [%s]\n", iNbWav, name);
      lstWaveFiles.push_back( name);
      iNbWav++;
    }
    file.close();
    if (iNbWav >= 2000)
      break;
  }
  if (lstWaveFiles.size() > 0)
  {
    // Sort ascending alphabetically
    std::sort (lstWaveFiles.begin(), lstWaveFiles.end());
    // Select last file
    iSelectedFile = (int)lstWaveFiles.size() - 1;
    if (CModeGeneric::bDebug)
    {
      Serial.println("CModePlayer::ListFiles");
      for (unsigned int i=0; i<lstWaveFiles.size(); i++)
        Serial.printf("%d [%s]\n", i, lstWaveFiles[i].c_str());
    }
  }
}

//-------------------------------------------------------------------------
//! \brief To set true Automatic playback of the last recorded file
void CModePlayer::SetAutomaticPlay()
{
  bAutoplay = true;
}

// Automatic playback of the last recorded file
bool CModePlayer::bAutoplay = false;
