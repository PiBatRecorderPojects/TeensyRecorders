/* 
 * File:   CKeyManager.cpp
   Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <Arduino.h>
#include "CKeyManager.h"
 
//-------------------------------------------------------------------------
// Management class of 4 or 2 direction button, a central push and two others buttons.
// Use interrupts

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CKeyManager::CKeyManager()
{
  if (pInstance != NULL)
    Serial.println("Only one instance of CKeyManager !");
  else
    pInstance = this;
}

//-------------------------------------------------------------------------
// Manager launch with initialisation of the buttons pins
void CKeyManager::Begin(
    unsigned char pinP,   // Pin for Push        Key
    unsigned char pinU,   // Pin for Up          Key
    unsigned char pinB,   // Pin for Bottom      Key
    unsigned char pinR,   // Pin for Right       Key (Not use if 0)
    unsigned char pinL,   // Pin for Left        Key (Not use if 0)
    unsigned char pinM,   // Pin for Change mode Key (Not use if 0)
    unsigned char pinPR   // Pin for Play/Rec    Key (Not use if 0)
    )
{
  // Memo Pin buttons
  PinPush       = pinP;
  PinUp         = pinU;
  PinDown       = pinB;
  PinLeft       = pinL;
  PinRight      = pinR;
  PinChangeModeA = pinM;
  PinChangeModeB    = pinPR;
  // Booleans of Change nitialization
  bPushChange       = false;
  bRightChange      = false;
  bLeftChange       = false;
  bUpChange         = false;
  bDownChange       = false;
  bChangeModeAChange = false;
  bChangeModeBChange    = false;
  // Time of last change initialization
  uiTPush       = millis();
  uiTRight      = uiTPush;
  uiTLeft       = uiTPush;
  uiTUp         = uiTPush;
  uiTDown       = uiTPush;
  uiTChangeModeA = uiTPush;
  uiTChangeModeB    = uiTPush;
#if defined(__IMXRT1062__) // Teensy 4.1
  // Input pins initialization
  pinMode(PinPush,    INPUT_PULLUP);
  pinMode(PinUp,      INPUT_PULLUP);
  pinMode(PinDown,    INPUT_PULLUP);
  if (PinRight > 0)
    pinMode(PinRight, INPUT_PULLUP);
  if (PinLeft > 0)
    pinMode(PinLeft,  INPUT_PULLUP);
  if (PinChangeModeA > 0)
    pinMode(PinChangeModeA,  INPUT_PULLUP);
  if (PinChangeModeB > 0)
    pinMode(PinChangeModeB,  INPUT_PULLUP);
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  // Input pins initialization
  pinMode(PinPush,    INPUT_PULLUP);
  pinMode(PinUp,      INPUT_PULLUP);
  pinMode(PinDown,    INPUT_PULLUP);
  if (PinRight > 0)
    pinMode(PinRight, INPUT_PULLUP);
  if (PinLeft > 0)
    pinMode(PinLeft,  INPUT_PULLUP);
  if (PinChangeModeA > 0)
    pinMode(PinChangeModeA,  INPUT_PULLUP);
  if (PinChangeModeB > 0)
    pinMode(PinChangeModeB,  INPUT_PULLUP);
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
  // Input pins initialization
  gpio_set_direction((gpio_num_t)PinPush         , GPIO_MODE_INPUT);
  gpio_set_pull_mode((gpio_num_t)PinPush         , GPIO_PULLUP_ONLY);
  gpio_set_direction((gpio_num_t)PinUp           , GPIO_MODE_INPUT);
  gpio_set_pull_mode((gpio_num_t)PinUp           , GPIO_PULLUP_ONLY);
  gpio_set_direction((gpio_num_t)PinDown         , GPIO_MODE_INPUT);
  gpio_set_pull_mode((gpio_num_t)PinDown         , GPIO_PULLUP_ONLY);
  if (PinLeft != 0)
  {
    gpio_set_direction((gpio_num_t)PinLeft       , GPIO_MODE_INPUT);
    gpio_set_pull_mode((gpio_num_t)PinLeft       , GPIO_PULLUP_ONLY);
  }
  if (PinRight != 0)
  {
    gpio_set_direction((gpio_num_t)PinRight      , GPIO_MODE_INPUT);
    gpio_set_pull_mode((gpio_num_t)PinRight      , GPIO_PULLUP_ONLY);
  }
  if (PinChangeModeA > 0)
  {
    gpio_set_direction((gpio_num_t)PinChangeModeA, GPIO_MODE_INPUT);
    gpio_set_pull_mode((gpio_num_t)PinChangeModeA, GPIO_PULLUP_ONLY);
  }
  if (PinChangeModeB > 0)
  {
    gpio_set_direction((gpio_num_t)PinChangeModeB, GPIO_MODE_INPUT);
    gpio_set_pull_mode((gpio_num_t)PinChangeModeB, GPIO_PULLUP_ONLY);
  }
#endif
  // Interrupt functions initialization
  SetInterrupt();
  uiTimeOutUp   = KEYHOLDTIME;
  uiTimeOutDown = KEYHOLDTIME;
  uiMaintUp     = 0;
  uiMaintDown   = 0;
}


//-------------------------------------------------------------------------
// Initializing interrupt functions
void CKeyManager::SetInterrupt()
{
#if defined(__IMXRT1062__) // Teensy 4.1
  // Interrupt functions initialization
  attachInterrupt(digitalPinToInterrupt(PinPush)         , OnPushChange       , CHANGE);
  attachInterrupt(digitalPinToInterrupt(PinUp)           , OnUpChange         , CHANGE);
  attachInterrupt(digitalPinToInterrupt(PinDown)         , OnDownChange       , CHANGE);
  if (PinRight > 0)
    attachInterrupt(digitalPinToInterrupt(PinRight)      , OnRightChange      , CHANGE);
  if (PinLeft > 0)
    attachInterrupt(digitalPinToInterrupt(PinLeft)       , OnLeftChange       , CHANGE);
  if (PinChangeModeA > 0)
    attachInterrupt(digitalPinToInterrupt(PinChangeModeA), OnChangeModeA, CHANGE);
  if (PinChangeModeB > 0)
    attachInterrupt(digitalPinToInterrupt(PinChangeModeB), OnChangeModeB, CHANGE);
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  // Interrupt functions initialization
  attachInterrupt(digitalPinToInterrupt(PinPush)         , OnPushChange       , CHANGE);
  attachInterrupt(digitalPinToInterrupt(PinUp)           , OnUpChange         , CHANGE);
  attachInterrupt(digitalPinToInterrupt(PinDown)         , OnDownChange       , CHANGE);
  if (PinRight > 0)
    attachInterrupt(digitalPinToInterrupt(PinRight)      , OnRightChange      , CHANGE);
  if (PinLeft > 0)
    attachInterrupt(digitalPinToInterrupt(PinLeft)       , OnLeftChange       , CHANGE);
  if (PinChangeModeA > 0)
    attachInterrupt(digitalPinToInterrupt(PinChangeModeA), OnChangeModeA, CHANGE);
  if (PinChangeModeB > 0)
    attachInterrupt(digitalPinToInterrupt(PinChangeModeB), OnChangeModeB, CHANGE);
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
  // Install GPIO isr service
  gpio_install_isr_service(ESP_INTR_FLAG_LEVEL2);
  // Interrupt functions initialization
  gpio_set_intr_type( (gpio_num_t)PinPush, GPIO_INTR_NEGEDGE);
  gpio_intr_enable( (gpio_num_t)PinPush);
  gpio_isr_handler_add( (gpio_num_t)PinPush, CKeyManager::OnPushChange, NULL);
  gpio_set_intr_type( (gpio_num_t)PinUp, GPIO_INTR_NEGEDGE);
  gpio_intr_enable( (gpio_num_t)PinUp);
  gpio_isr_handler_add( (gpio_num_t)PinUp, CKeyManager::OnUpChange, NULL);
  gpio_set_intr_type( (gpio_num_t)PinDown, GPIO_INTR_NEGEDGE);
  gpio_intr_enable( (gpio_num_t)PinDown);
  gpio_isr_handler_add( (gpio_num_t)PinDown, CKeyManager::OnDownChange, NULL);
  if (PinRight != 0)
  {
    gpio_set_intr_type( (gpio_num_t)PinRight, GPIO_INTR_NEGEDGE);
    gpio_intr_enable( (gpio_num_t)PinRight);
    gpio_isr_handler_add( (gpio_num_t)PinRight, CKeyManager::OnRightChange, NULL);
  }
  if (PinLeft != 0)
  {
    gpio_set_intr_type( (gpio_num_t)PinLeft, GPIO_INTR_NEGEDGE);
    gpio_intr_enable( (gpio_num_t)PinLeft);
    gpio_isr_handler_add( (gpio_num_t)PinLeft, CKeyManager::OnLeftChange, NULL);
  }
  if (PinChangeModeA > 0)
  {
    gpio_set_intr_type( (gpio_num_t)PinChangeModeA, GPIO_INTR_NEGEDGE);
    gpio_intr_enable( (gpio_num_t)PinChangeModeA);
    gpio_isr_handler_add( (gpio_num_t)PinChangeModeA, CKeyManager::OnChangeModeA, NULL);
  }
  if (PinChangeModeA > 0)
  {
    gpio_set_intr_type( (gpio_num_t)PinChangeModeB, GPIO_INTR_NEGEDGE);
    gpio_intr_enable( (gpio_num_t)PinChangeModeB);
    gpio_isr_handler_add( (gpio_num_t)PinChangeModeB, CKeyManager::OnChangeModeA, NULL);
  }
#endif
}
  
//-------------------------------------------------------------------------
// Reading buttons, returns the code of the action
// To call regularly (more than REBOUNDTIME)
unsigned short CKeyManager::GetKey()
{
  unsigned short uiKey = K_NO;
  uiTimeGetKey = millis();
  if (IsKPush())
    uiKey = K_PUSH;
  else if (IsKUp())
    uiKey = K_UP;
  else if (IsKDown())
    uiKey = K_DOWN;
  else if (IsKLeft())
    uiKey = K_LEFT;
  else if (IsKRight())
    uiKey = K_RIGHT;
  else if (IsKChangeMode())
    uiKey = K_MODEA;
  else if (IsKPlayRec())
    uiKey = K_MODEB;
  return uiKey;
}

//-------------------------------------------------------------------------
// Returns true if the Push button was pressed
bool CKeyManager::IsKPush()
{
  bool bKey = false;
  if (bPushChange and (uiTimeGetKey-uiTPush) > REBOUNDTIME and digitalRead(PinPush) == LOW)
  { // Stable LOW status
    uiTPush = uiTimeGetKey;
    bPushChange = false;
    bKey = true;
  }
  return bKey;
}

//-------------------------------------------------------------------------
//  Returns true if the Up button was pressed
bool CKeyManager::IsKUp()
{
  bool bKey = false;
  if (bUpChange and (uiTimeGetKey-uiTUp) > REBOUNDTIME and digitalRead(PinUp) == LOW)
  { // Stable LOW status
    uiTUp = uiTimeGetKey;
    bUpChange = false;
    bKey = true;
  }
  else if (digitalRead(PinUp) == LOW and uiTimeGetKey-uiTUp > uiTimeOutUp)
  {
    uiTUp = uiTimeGetKey;
    bUpChange = false;
    bKey = true;
    // We may change the timeout to accelerate the keying on maintained key
    uiMaintUp++;
    if (uiMaintUp == HOLDFORINTSPEED)
      uiTimeOutUp = KEYHOLDTIME / 3;
    else if (uiMaintUp == HOLDFORMAXSPEED)
      uiTimeOutUp = KEYHOLDTIME / 6;
  }
  return bKey;
}

//-------------------------------------------------------------------------
//  Returns true if the Down button was pressed
bool CKeyManager::IsKDown()
{
  bool bKey = false;
  if (bDownChange and (uiTimeGetKey-uiTDown) > REBOUNDTIME and digitalRead(PinDown) == LOW)
  { // Stable LOW status
    uiTDown = uiTimeGetKey;
    bDownChange = false;
    bKey = true;
  }
  else if (digitalRead(PinDown) == LOW and uiTimeGetKey-uiTDown > uiTimeOutDown)
  {
    uiTDown = uiTimeGetKey;
    bDownChange = false;
    bKey = true;
    // We may change the timeout to accelerate the keying on maintained key
    uiMaintDown++;
    if (uiMaintDown == HOLDFORINTSPEED)
      uiTimeOutDown = KEYHOLDTIME / 3;
    else if (uiMaintDown == HOLDFORMAXSPEED)
      uiTimeOutDown = KEYHOLDTIME / 6;
  }
  return bKey;
}

//-------------------------------------------------------------------------
//  Returns true if the Left button was pressed
bool CKeyManager::IsKLeft()
{
  bool bKey = false;
  if (bLeftChange and (uiTimeGetKey-uiTLeft) > REBOUNDTIME and digitalRead(PinLeft) == LOW)
  { // Stable LOW status
    uiTLeft = uiTimeGetKey;
    bLeftChange = false;
    bKey = true;
  }
  return bKey;
}

//-------------------------------------------------------------------------
//  Returns true if the Right button was pressed
bool CKeyManager::IsKRight()
{
  bool bKey = false;
  if (bRightChange and (uiTimeGetKey-uiTRight) > REBOUNDTIME and digitalRead(PinRight) == LOW)
  { // Stable LOW status
    uiTRight = uiTimeGetKey;
    bRightChange = false;
    bKey = true;
  }
  return bKey;
}
  
//-------------------------------------------------------------------------
//  Returns true if the Change mode button was pressed
bool CKeyManager::IsKChangeMode()
{
  bool bKey = false;
  if (bChangeModeAChange and (uiTimeGetKey-uiTChangeModeA) > REBOUNDTIME and digitalRead(PinChangeModeA) == LOW)
  { // Stable LOW status
    uiTChangeModeA = uiTimeGetKey;
    bChangeModeAChange = false;
    bKey = true;
  }
  return bKey;
}
  
//-------------------------------------------------------------------------
//  Returns true if the Play/Rec button was pressed
bool CKeyManager::IsKPlayRec()
{
  bool bKey = false;
  if (bChangeModeBChange and (uiTimeGetKey-uiTChangeModeB) > REBOUNDTIME and digitalRead(PinChangeModeB) == LOW)
  { // Stable LOW status
    uiTChangeModeB = uiTimeGetKey;
    bChangeModeBChange = false;
    bKey = true;
  }
  return bKey;
}
  
//-------------------------------------------------------------------------
// Returns true if the Left / Right buttons are used
bool CKeyManager::IsLeftRight()
{
  if (pInstance->PinLeft == 0 or pInstance->PinRight == 0)
    return false;
  return true;
}
  
//-------------------------------------------------------------------------
// Static function for managing interruption of Push pin
#if defined(__IMXRT1062__) // Teensy 4.1
  void CKeyManager::OnPushChange()
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  void CKeyManager::OnPushChange()
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
  void IRAM_ATTR CKeyManager::OnPushChange( void* arg)
#endif
{
  pInstance->bPushChange = true;
  pInstance->uiTPush     = millis();
}
    
//-------------------------------------------------------------------------
// Static function for managing interruption of Up pin
#if defined(__IMXRT1062__) // Teensy 4.1
  void CKeyManager::OnUpChange()
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  void CKeyManager::OnUpChange()
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
  void IRAM_ATTR CKeyManager::OnUpChange( void* arg)
#endif
{
  pInstance->bUpChange     = true;
  pInstance->uiTUp         = millis();
  pInstance->uiTimeOutUp   = KEYHOLDTIME;
  pInstance->uiMaintUp     = 0;
}

//-------------------------------------------------------------------------
// Static function for managing interruption of Down pin
#if defined(__IMXRT1062__) // Teensy 4.1
  void CKeyManager::OnDownChange()
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  void CKeyManager::OnDownChange()
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
  void IRAM_ATTR CKeyManager::OnDownChange( void* arg)
#endif
{
  pInstance->bDownChange   = true;
  pInstance->uiTDown       = millis();
  pInstance->uiTimeOutDown = KEYHOLDTIME;
  pInstance->uiMaintDown   = 0;
}

//-------------------------------------------------------------------------
// Static function for managing interruption of Left pin
#if defined(__IMXRT1062__) // Teensy 4.1
  void CKeyManager::OnLeftChange()
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  void CKeyManager::OnLeftChange()
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
  void IRAM_ATTR CKeyManager::OnLeftChange( void* arg)
#endif
{
  pInstance->bLeftChange = true;
  pInstance->uiTLeft     = millis();
}
    
//-------------------------------------------------------------------------
// Static function for managing interruption of Right pin
#if defined(__IMXRT1062__) // Teensy 4.1
  void CKeyManager::OnRightChange()
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  void CKeyManager::OnRightChange()
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
  void IRAM_ATTR CKeyManager::OnRightChange( void* arg)
#endif
{
  pInstance->bRightChange = true;
  pInstance->uiTRight     = millis();
}
    
//-------------------------------------------------------------------------
// Static function for managing interruption of Change mode A pin
#if defined(__IMXRT1062__) // Teensy 4.1
  void CKeyManager::OnChangeModeA()
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  void CKeyManager::OnChangeModeA()
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
  void IRAM_ATTR CKeyManager::OnChangeModeA( void* arg)
#endif
{
  pInstance->bChangeModeAChange = true;
  pInstance->uiTChangeModeA     = millis();
}
    
//-------------------------------------------------------------------------
// Static function for managing interruption of Change mode B pin
#if defined(__IMXRT1062__) // Teensy 4.1
  void CKeyManager::OnChangeModeB()
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  void CKeyManager::OnChangeModeB()
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
  void IRAM_ATTR CKeyManager::OnChangeModeB( void* arg)
#endif
{
  pInstance->bChangeModeBChange = true;
  pInstance->uiTChangeModeB     = millis();
}
    
// Pointer on the instance of the class
CKeyManager *CKeyManager::pInstance = NULL;
  

