/* 
 * File:   ModesModifiers.h
   Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 /*
  *
  * V0.1 - Initial version december 2018
  * V0.2 - Version for Teensy 3.6 and ESP32 january 2019
  *		   - Added the ability to change the font
  *      - Invalid parameters are not displayed in line mode
  * V0.3 - Add ModesModifiersConfig.h to choose SDFat or SDFs library
  * V0.4 - Added the possibility to manage operating modes in static or dynamic
  * V0.5 - Added GetKeyManager and SetInterrupt on CKeyManager to reset interrupts after a sleep on a Teensy
  *      - Improved operating mode change with the ability to use static or dynamic instances
  *      - Added virtual function OnLoop in CGenericMode
  *      - CDrawBarModifier can visualize data with a min / max other than 0/100
  *      - CGenericMode if presence of modifying in line initializes by default the indexes for the max number of lines
  *      - Improved cursor management for the selected line to prioritize the selected line in the middle of the screen
  *      - Add the CGenericMode::AddPrint function to display information outside the edits
  *      - Add CGenericMode::UpdateScreen function to improve the display speed
  * V0.6 - Fix CGenericMode::ChangeMode
  *      - Fix CSDModifier display
  * V0.7 - Add Doxygen documentation
  *      - Add CGenericModifier::SetFormat function
  * V0.8 - Correct bugs when bLeftRight is false
  *      - Added management of the validity of each element of an enum 
  * V0.9 - Add parameter to read or not parameters in CGenericMode::BeginMode
  * V1.0 - Fix bug impossible to del wav in English language
  *      - Fix bad initialisation in line parameters
  *      - Put all LogFile functions in static
  *      - Add parameter to write or not parameters in CGenericMode::EndMode
  *      - Fix bug in LogFile::AddLog (buffer overflow for long string)
  * V1.1 - Add static LogFile::GetbConsole()
  * V1.2 - If no parameter of type line, selection of the valid 1st
  *      - Added ability to exit error mode
  *      - Fixed a bug when the screen is composed only of non-online modifiers
  * V1.3 - Fixed a bug when the screen is composed with lin and non-online modifiers. Il the numbre of lines in the screen is less than MAXLINES, iNbLines must be set befor call of CGenericMode::BeginMode
  *      - Fixed a bug on CBatteryModifier when charge is 100%
  * V1.4 - Correction of a bug on the display of the lines at the end of modification of a parameters
  * V1.4 - Add CGenericMode::PrePrint to print something before modifiers
  * V1.5 - Use SdFatSdioEX for fast write
  * V1.6 - Add CEnumModifier::SetStringArray To change array of strings for different values of the enum
  * V1.7 - Optimizing the memory used by LogFile::AddLog
  * V1.8 - LogFile::AddLog return false on write acces problem on SD card
  *      - CModeError, Possible automatic end of error mode with a reset
  * V1.9 - Addition of "ConstChar.c" for multilingual management
  *      - Addition of German and Dutch languages
  *      - Set SDFAT V2.0.4 for working with SDXC in FAT64 (exFAT). Formatting to FAT32 is no longer necessary
  * V1.10 - Addition CSecondModifier, management class of a seconds modifier in time mode (hh:mm:ss)
  * V1.11 - Time display, correction of unwanted jumps of 2 or 3s
  * V1.12 - Fixed CSecondModifier behavior
  * V1.13 - Add Teensy 4
  * V1.14 - Add CLineEnumModifier
  * 
  * CKeyManager.h
  * - CKeyManager Management class of buttons, a central push, up and bottom, left and right, modeA and modeB
  *
  * CModifiers.h
  * Management of parameters modifiers. For each parameter, there are 2 display modes:
  * - Online, for example "> Min Freq 120kHz" with "120" editable.
  *     The selected line displays an arrow at the beginning of the line
  *     In modification, the modifiable zone appears in inverse
  * - Anywhere on the screen, for example "[Player]" with "Player" editable.
  *     If the parameter is not modifiable, it is never selected
  *     If the parameter is selected, the 1st and last characters are in reverse video
  *     In modification, the modifiable zone appears in inverse
  * - CGenericModifier       Base class of a field modifier, manages all common behaviors to modifiers, two modes of modification: on line, on site
  *   - CCharModifier        Management class of a string type modifier
  *   - CModificatorInt      Management class of a integer type modifier
  *     - CDrawBarModifier   Management class of a bargraph type modifier from 0 to 100%
  *   - CFloatModifier       Management class of a float type modifier
  *   - CEnumModifier        Management class of a enum type modifier
  *   - CBoolModifier        Management class of a boolean type modifier
  *     - CLigthModifier     Management class of a boolean type modifier for screen brightness
  *   - CPushButtonModifier  Management class of a push button type modifier
  *   - CDateModifier        Management class of a date type modifier (current date or date parameter)
  *   - CHourModifier        Management class of a time type modifier (current time or time parameter)
  *   - CSDModifier          Management class of an SD card type modifier (size, free place, number of files and erasing files)
  *   - CPlayRecModifier     Management class of an Break/Play/Rec button type modifier
  *   - CBatteryModifier     Management class of a battery type modifier (0 to 100%)
  *
  * CGenericMode.h
  * - CGenericMode Generic class for managing a mode of operation, defines the minimal interface, manages a list of modifiers
  * - CModeError   Error mode management class, displays the error and waits for a shutdown
  * - LogFile      Management class of software flow log file
  *
  * WARNING To choose between SDFat and SDFs, do not forget to uncomment the used library and comment on the library not used in ModesModifiersConfig.h
  *
  * WARNING Do not forget to create the CreateNewMode function to create the different modes of operation
  * You have the choice to create them dynamically or statically. Look at the examples.
  */
#define VERSION_MODIFIERS "V1.14"

#ifndef MODESMODIFIERS_H
#define MODESMODIFIERS_H

#include "ModesModifiersConfig.h"
#include "CKeyManager.h"
#include "CModifiers.h"
#include "CGenericMode.h"

#endif  /* MODESMODIFIERS_H */


