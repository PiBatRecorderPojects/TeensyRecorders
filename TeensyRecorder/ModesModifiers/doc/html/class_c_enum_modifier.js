var class_c_enum_modifier =
[
    [ "CEnumModifier", "class_c_enum_modifier.html#acf1880a5a50145a6eb1374abf9bdf70d", null ],
    [ "~CEnumModifier", "class_c_enum_modifier.html#ad319b7f306e9bcdad203ff26c882500b", null ],
    [ "GetElemValidity", "class_c_enum_modifier.html#a4d0bae0e8db701f3a19d60f2b31ce379", null ],
    [ "GetString", "class_c_enum_modifier.html#ac09c8da522268e33e9aca3cf20806c2c", null ],
    [ "NextValue", "class_c_enum_modifier.html#aee52e4f03232d48fc9a83025ba75504b", null ],
    [ "PrevValue", "class_c_enum_modifier.html#a4b220c2bb2234617626969b7849e3fc2", null ],
    [ "ReceiveKey", "class_c_enum_modifier.html#ac093bb5b53d1b9542f6d9a7202e0e0bd", null ],
    [ "SetElemValidity", "class_c_enum_modifier.html#a7efef0a9b5708e1a8844029cacb3fe9e", null ],
    [ "SetParam", "class_c_enum_modifier.html#a3dec8f4dc195a44b57bfa65fb3f4f10a", null ],
    [ "SetStringArray", "class_c_enum_modifier.html#a1cc90ef35f1322573e16a94e1586a71b", null ],
    [ "StartModif", "class_c_enum_modifier.html#a05e27d464cb1038db1303c412a4d0bb4", null ],
    [ "iTemp", "class_c_enum_modifier.html#a430d0b7e8355ebc7b9451fcff825c21e", null ],
    [ "iValMax", "class_c_enum_modifier.html#a1d2d39094be7251d440b7eea075d9bfa", null ],
    [ "pParam", "class_c_enum_modifier.html#a4f6cd5d9f877dc6314db7446d10e026e", null ],
    [ "pTbStrings", "class_c_enum_modifier.html#ab8ddeeac40b28019dc3ff74951bfc655", null ],
    [ "pTbVal", "class_c_enum_modifier.html#aaba9e144d9c19318b65ca3d3a3b387a0", null ]
];