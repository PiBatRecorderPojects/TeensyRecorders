var annotated_dup =
[
    [ "CBatteryModifier", "class_c_battery_modifier.html", "class_c_battery_modifier" ],
    [ "CBoolModifier", "class_c_bool_modifier.html", "class_c_bool_modifier" ],
    [ "CCharModifier", "class_c_char_modifier.html", "class_c_char_modifier" ],
    [ "CDateModifier", "class_c_date_modifier.html", "class_c_date_modifier" ],
    [ "CDrawBarModifier", "class_c_draw_bar_modifier.html", "class_c_draw_bar_modifier" ],
    [ "CEnumModifier", "class_c_enum_modifier.html", "class_c_enum_modifier" ],
    [ "CFloatModifier", "class_c_float_modifier.html", "class_c_float_modifier" ],
    [ "CGenericModifier", "class_c_generic_modifier.html", "class_c_generic_modifier" ],
    [ "CHourModifier", "class_c_hour_modifier.html", "class_c_hour_modifier" ],
    [ "CKeyManager", "class_c_key_manager.html", "class_c_key_manager" ],
    [ "CModificatorInt", "class_c_modificator_int.html", "class_c_modificator_int" ],
    [ "CModificatorLight", "class_c_modificator_light.html", "class_c_modificator_light" ],
    [ "CPlayRecModifier", "class_c_play_rec_modifier.html", "class_c_play_rec_modifier" ],
    [ "CPushButtonModifier", "class_c_push_button_modifier.html", "class_c_push_button_modifier" ],
    [ "CSDModifier", "class_c_s_d_modifier.html", "class_c_s_d_modifier" ]
];