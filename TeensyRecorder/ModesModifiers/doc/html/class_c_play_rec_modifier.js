var class_c_play_rec_modifier =
[
    [ "CPlayRecModifier", "class_c_play_rec_modifier.html#a109155d89e801af3829f333d47630085", null ],
    [ "GetString", "class_c_play_rec_modifier.html#a3a78843e47421ad2f935e61d04069866", null ],
    [ "PrintParam", "class_c_play_rec_modifier.html#a279dbd149c9ffc2a03fe5cd7779398b5", null ],
    [ "StartModif", "class_c_play_rec_modifier.html#aa09d59b644dd2d55ad859d32f7ca4b47", null ],
    [ "bPlayer", "class_c_play_rec_modifier.html#a7d24f72334206170ff2bce04b82578cb", null ],
    [ "bRecord", "class_c_play_rec_modifier.html#ad776936192017bbbf4cf83763b93fc8a", null ],
    [ "iHeigth", "class_c_play_rec_modifier.html#a52bb459fe64c7c0517ca40bcaa85c5ab", null ],
    [ "iWidth", "class_c_play_rec_modifier.html#a6e66025fcdba9241995034c1ec8dae79", null ],
    [ "pParam", "class_c_play_rec_modifier.html#a8f0fc4c8e5df7bbf816f962219ebea47", null ]
];