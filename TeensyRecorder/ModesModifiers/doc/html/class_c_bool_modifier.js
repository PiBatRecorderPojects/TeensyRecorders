var class_c_bool_modifier =
[
    [ "CBoolModifier", "class_c_bool_modifier.html#a139f7c76346c50220b91dc0fa2aec9b7", null ],
    [ "GetString", "class_c_bool_modifier.html#a27c7e3a213267926bce7a2aed502e49a", null ],
    [ "ReceiveKey", "class_c_bool_modifier.html#a92d785fccd6ba795e7d665e6da3e0b14", null ],
    [ "SetParam", "class_c_bool_modifier.html#a63fff866a2de1a47142c71825db449d3", null ],
    [ "StartModif", "class_c_bool_modifier.html#a6576387be4057ef4b47419510dcd2e89", null ],
    [ "iTemp", "class_c_bool_modifier.html#a23f74ff8367faedd9b8ed8affe821c8f", null ],
    [ "pParam", "class_c_bool_modifier.html#a1411c9334026fe78496df6a5a518ac18", null ],
    [ "pTbStrings", "class_c_bool_modifier.html#a7f6e1b2bb3e8fec0331b0a62763cda8b", null ]
];