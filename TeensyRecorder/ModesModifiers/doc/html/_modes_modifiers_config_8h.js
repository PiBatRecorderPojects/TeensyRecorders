var _modes_modifiers_config_8h =
[
    [ "SD_FAT_TYPE", "_modes_modifiers_config_8h.html#a488e49b67d5862e96d70104cdcedcf4b", null ],
    [ "LANGUAGE", "_modes_modifiers_config_8h.html#a3729d01cbf3eaaaba26dd0239041d35e", [
      [ "LENGLISH", "_modes_modifiers_config_8h.html#a3729d01cbf3eaaaba26dd0239041d35ea16e68814fa74838cca25d7f17d6f8110", null ],
      [ "LFRENCH", "_modes_modifiers_config_8h.html#a3729d01cbf3eaaaba26dd0239041d35ea1025990b1f9391e30926bfca2f03080b", null ],
      [ "LGERMAN", "_modes_modifiers_config_8h.html#a3729d01cbf3eaaaba26dd0239041d35ea9bba41bf75606dc24c1709a6435cf120", null ],
      [ "LDUTCH", "_modes_modifiers_config_8h.html#a3729d01cbf3eaaaba26dd0239041d35ead99a8b92e41171b6e98d17ba6d17988e", null ],
      [ "MAXLANGUAGE", "_modes_modifiers_config_8h.html#a3729d01cbf3eaaaba26dd0239041d35ea6f6195399e24ef92e4d57de246ac9d18", null ]
    ] ]
];