/* 
 * File:   TestModifier.ino
   Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/*
 * Test program for Modifiers, Generic mode and Log
 */
 /*
 * WARNING - WARNING - WARNING - WARNING - WARNING
 * For a short call to the function sd.vol()->freeClusterCount() (except the first call made in the init) 
 * MAINTAIN_FREE_CLUSTER_COUNT must be set to 1 in SdFatConfig.h (SdFat library to download on https://github.com/greiman/SdFat)
 * 
*/

#include "ModesModifiersConfig.h"
#ifdef MMC_SDFAT
  #include "SdFat.h"
#endif
#ifdef MMC_SDFAT_EXT
  #include "SdFat.h"
#endif
#ifdef MMC_SDFS
  #include "SdFs.h"
#endif
#include "TimeLib.h"
#include "U8g2lib.h"
#include "CGenericMode.h"
#include "CTestModes.h"
#include "RamMonitor.h"

// N° des pins du bouton 5 positions
#define PINPUSH  7
#define PINRIGHT 3
#define PINLEFT  8
#define PINUP    2
#define PINDOWN  6

// Screen type
#define ST_SSD1306 1
#define ST_SH1106  0

// Screen manager
U8G2 *pDisplay = NULL;
// Screen type
//int iScreenType = ST_SSD1306;
int iScreenType = ST_SH1106;

// SD Manager
#ifdef MMC_SDFAT
  SdFatSdio sd;
#endif
#ifdef MMC_SDFAT_EXT
  SdFatSdioEX sd;
#endif
#ifdef MMC_SDFS
  SdFs sd;
#endif

// Ram monitoring
RamMonitor ramMonitor;

// Key manager
CKeyManager KeyManag;

// Log file manager
LogFile logTest("TstMod");

// Storing the loop time for button and screen treatments
unsigned long ulTimeKey;
unsigned long ulTimePrint;
// Time in ms between two buttons controls
#define TIMEKEY 10
// Time in ms between two screen display
#define TIMEPRINT 200

// GR Logo bitmap
extern const unsigned char LogoGR [];

// Comment to test the dynamic creation of the modes of operation (at a given moment a single mode of operation exists in memory)
// Otherwise, the operating modes are static (the operating modes all exist in memory for the duration of the program)
#define TEST_STATIC

#ifdef TEST_STATIC
  // Static modes of operation
  CModeTest         modeTest;
  CModeParametersA  modeParamsA;
  CModeParametersB  modeParamsB;
  CModeError        modeError;
#endif

//----------------------------------------------------
// Function to create or activate a new mode
CGenericMode* CreateNewMode(
  CGenericMode* pOldMode,   // Pointer on the previous mode of operation
  int idxMode               // New mode of operation index
  )
{
  CGenericMode* pNewMode = NULL;
#ifdef TEST_STATIC
  // Static creation of operating modes
  // We just swap the pointer on the current operating mode
  switch (idxMode)
  {
  case MODETEST    : pNewMode = &modeTest;     Serial.print("Swap to CModeTest");          break;
  case MODEPARAMSA : pNewMode = &modeParamsA;  Serial.print("Swap to CModeParametersA");   break;
  case MODEPARAMSB : pNewMode = &modeParamsB;  Serial.print("Swap to CModeParametersB");   break;
  case MODEERROR   : pNewMode = &modeError;    Serial.print("Swap to CModeError");         break;
  }
#else
  // Dynamic creation of operating modes
  // First, destruction of the previous mode to free memory
  delete pOldMode;
  // Second, creating the new mode of operation in memory
  switch (idxMode)
  {
  case MODETEST    : pNewMode = new CModeTest();         Serial.print("new CModeTest");          break;
  case MODEPARAMSA : pNewMode = new CModeParametersA();  Serial.print("new CModeParametersA");   break;
  case MODEPARAMSB : pNewMode = new CModeParametersB();  Serial.print("new CModeParametersB");   break;
  case MODEERROR   : pNewMode = new CModeError();        Serial.print("new CModeError");         break;
  }
#endif
  // Print ram usage after new (for Teensy 3.6), if used heap is not identical after a same mode, delete is missing....
  uint32_t TotalRam = 262144;
  uint32_t FreeRam  = ramMonitor.free();
  uint32_t HeapUsed = ramMonitor.heap_used();
  uint32_t FreeRamP = FreeRam * 100 / TotalRam;
  Serial.printf(" Total Ram %d, free Ram %d (%d%%), heap used %d\n", TotalRam, FreeRam, FreeRamP, HeapUsed);
  return pNewMode;
}

//----------------------------------------------------
// Call back for file timestamps.  Only called for file create and sync().
void dateTime(uint16_t* date, uint16_t* time)
{
  // return date using FAT_DATE macro to format fields
  *date = FAT_DATE(year(), month(), day());

  // return time using FAT_TIME macro to format fields
  *time = FAT_TIME(hour(), minute(), second());
}

//----------------------------------------------------
// Print Logo
void AffLogo()
{
  pDisplay->clearBuffer();
  pDisplay->drawXBM(0, 0, 128, 64, LogoGR);
  pDisplay->setFont(u8g2_font_6x13B_mf);  // Hauteur 9
  pDisplay->setFontPosTop();
  pDisplay->setCursor( 2, 27);
  pDisplay->print( "Modifiers");
  pDisplay->setFont(u8g2_font_micro_mr);  // Hauteur 5
  pDisplay->setCursor( 2, 2);
  switch (iScreenType)
  {
  case ST_SSD1306: pDisplay->print( "SSD1306"); break;
  default        : pDisplay->print( "SH1106") ; break;
  }
  pDisplay->setFont(u8g2_font_6x10_mf);  // Hauteur 7
  pDisplay->setCursor( 2, 52);
  pDisplay->print( VERSION_MODIFIERS);
  pDisplay->drawRFrame( 0, 0, 128, 64, 0);
  pDisplay->sendBuffer();
  delay(2000);
}

//-------------------------------------------------------------------------
// Create screen manager
void CreateGestScreen()
{
  switch (iScreenType)
  {
  case ST_SSD1306:
    pDisplay = new U8G2_SSD1306_128X64_NONAME_F_HW_I2C(U8G2_R0, U8X8_PIN_NONE);
    break;
  default:
    pDisplay = new U8G2_SH1106_128X64_NONAME_F_HW_I2C(U8G2_R0, U8X8_PIN_NONE);
  }
  // Initialisation
  pDisplay->begin();
}

//-------------------------------------------------------------------------
// SD card init
bool InitSD()
{
  bool bReturn = false;
#ifdef MMC_SDFAT
  if (!sd.begin())
#endif
#ifdef MMC_SDFAT_EXT
  if (!sd.begin())
#endif
#ifdef MMC_SDFS
  if (!sd.begin(SdioConfig(FIFO_SDIO)))
#endif
    Serial.println("SdFatSdio begin() failed !!!");
  else
    bReturn = true;
  return bReturn;
}

//-------------------------------------------------------------------------
// Time Recovery from Integrated RTC in Teensy 3.0
time_t getTeensy3Time()
{
  return Teensy3Clock.get();
}

//----------------------------------------------------------------------------
// Program setup
void setup()
{
  ramMonitor.initialize();
  logTest.SetbConsole( true);
  // Initialize language
  CGenericMode::SetLanguage( LENGLISH);
  // Initialize the time of the libary Time with the RTC Teensy 3.6
  setSyncProvider(getTeensy3Time);
  // Initializing the console output
  Serial.begin(115200);
  delay(100);
  Serial.println("===== TEST =====");

  // Build in LED
  pinMode( 13, OUTPUT);
  digitalWrite( 13, LOW);
  
  // Initializing the Button Manager
  // With right and left buttons
  KeyManag.Begin( PINPUSH, PINUP, PINDOWN, PINRIGHT, PINLEFT);
  CGenericModifier::SetbLeftRight( true);
  // Without right and left buttons and test Change mode and Play/Rec buttons
  //KeyManag.Begin( PINPUSH, PINUP, PINDOWN, 0, 0, PINRIGHT, PINLEFT);
  //CGenericModifier::SetbLeftRight( false);

  // Create screen manager
  CreateGestScreen();

  // Print logo and version
  AffLogo();

  // SD card init (remove SD card to test Error mode)
  if (!InitSD())
  {
    CModeError::SetTxtError((char *)"SD card error !");
    CGenericMode::ChangeMode( MODEERROR);
  }
  else
  {
    // Set callback to date the files
#ifdef MMC_SDFAT
    SdFile::dateTimeCallback(dateTime);
#endif
#ifdef MMC_SDFAT_EXT
    SdFile::dateTimeCallback(dateTime);
#endif
#ifdef MMC_SDFS
    FsDateTime::callback = dateTime;
#endif

    // Start log and software
    LogFile::StartLog();
    if (CGenericMode::GetLanguage() == LFRENCH)
      LogFile::AddLog( LLOG, "Démarrage Test Modifiers %s (Fréquence processeur %d MHz)", VERSION_MODIFIERS, F_CPU/1000000);
    else
      LogFile::AddLog( LLOG, "Starting Modifiers Test %s (processor frequency %d MHz)", VERSION_MODIFIERS, F_CPU/1000000);
    // Go to Test mode
    CGenericMode::ChangeMode( MODETEST);
  }
  // Initialise le temps des traitements
  ulTimeKey   = millis() + TIMEKEY;
  ulTimePrint = millis() + TIMEPRINT;
}

//----------------------------------------------------------------------------
// Main loop
void loop()
{
  // Test Ram
  bool bRamOK = true;
  if (ramMonitor.warning_lowmem())
  {
    Serial.println("Warning low memory !!!");
    bRamOK = false;
  }
  if (ramMonitor.warning_crash())
  {
    Serial.println("Warning crash memory !!!");
    bRamOK = false;
  }
  // Test of the time since the last control of the buttons
  if (bRamOK and millis() > ulTimeKey and CGenericMode::GetpCurrentMode() != NULL)
  {
    // We treat a possible button
    int iOldMode = CGenericMode::GetCurrentMode();
    unsigned short key = KeyManag.GetKey();
    switch (key)
    {
    case K_UP      : Serial.println("Key Up")           ; break;
    case K_DOWN    : Serial.println("Key Down")         ; break;
    case K_PUSH    : Serial.println("Key Push")         ; break;
    case K_LEFT    : Serial.println("Key Left")         ; break;
    case K_RIGHT   : Serial.println("Key Right")        ; break;
    case K_MODEA   : Serial.println("Key change mode A"); break;
    case K_MODEB   : Serial.println("Key change mode B"); break;
    }
    if (key != K_NO)
    {
      int iNewMode = CGenericMode::GetpCurrentMode()->KeyManager( key);
      if (iNewMode != NOMODE and iNewMode != iOldMode)
        // Changing the operating mode
        CGenericMode::ChangeMode( iNewMode);
    }
    ulTimeKey = millis() + TIMEKEY;
  }
  // Test if it's time to display
  if (bRamOK and millis() > ulTimePrint and CGenericMode::GetpCurrentMode() != NULL)
  {
    // Print current mode
    CGenericMode::GetpCurrentMode()->PrintMode();
    ulTimePrint = millis() + TIMEPRINT;
    ramMonitor.run();
  }
  // Attente 10ms
  delay( 100);
}