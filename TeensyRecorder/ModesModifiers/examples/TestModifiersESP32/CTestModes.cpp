/* 
 * File:   CTestModes.cpp
   Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <arduino.h>
#include <EEPROM.h>
#include <U8g2lib.h>
#include "CTestModes.h"

// Gestionnaire écran
extern U8G2 *pDisplay;

// Classs to test CGenericMode and Modifiers

//-------------------------------------------------------------------------
// Generic class for testing

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CGenericTest::CGenericTest()
{
  strcpy( params.sName, "Def Name");
  params.iDuration = 10;
  params.fFrequency = 100.0;
  params.iRecordMode = MODETEST;
  params.bAutoHeterodyne = false;
  strcpy( params.sTime, "22:00");
  params.iPlayMode = PBREAK;
  params.iRecMode = PBREAK;
  params.iBattery = 20;
  params.iDrawBar = 50;
  params.bLight = false;
  params.iLanguage = LENGLISH;
  params.iModeThreshold = RELATIVE_THRESHOLD;
  params.iRelativeThreshold = 15;
  params.iAbsoluteThreshold = -90;
}

//-------------------------------------------------------------------------
// Beginning of the mode
void CGenericTest::BeginMode()
{
  // Calling the basic function
  CGenericMode::BeginMode();
  // Init language
  CGenericMode::SetLanguage( params.iLanguage);
  // Change light
  if (params.bLight)
    pDisplay->setContrast(1);
  else
    pDisplay->setContrast(255);
  // Set battery value for test
  params.iBattery = 20;
}

//-------------------------------------------------------------------------
// Reading saved settings
bool CGenericTest::ReadParams()
{
  bool bOK = false;
  uint16_t uiControle = 0;
  char *p = (char *)&uiControle;
  int address = 0;
  for (int i = 0; i<(int)sizeof(uint16_t); i++)
    *p++ = EEPROM.read(address++);
  if (uiControle == 0x55AA)
  {
    // Reading parameters at the following addresses
    p = (char *)&params;
    for (int i = 0; i<(int)sizeof(Parameters); i++)
      *p++ = EEPROM.read(address++);
    // Test parameters
    //if (strlen(params.sName) > MAXCHARNAME)
      strcpy( params.sName, "Def Name");
    if (params.iDuration < 5 or params.iDuration > 100)
      params.iDuration = 10;
    if (params.fFrequency < 15.0 or params.fFrequency > 130.0)
      params.fFrequency = 100.0;
    if (params.iRecordMode < AUTOMATIC_REC or params.iRecordMode >= MAX_REC)
      params.iRecordMode = AUTOMATIC_REC;
    if (params.bAutoHeterodyne != true and params.bAutoHeterodyne != false)
      params.bAutoHeterodyne = false;
    if (strlen(params.sTime) != 5)
      strcpy( params.sTime, "22:00");
    if (params.iPlayMode != PBREAK)
      params.iPlayMode = PBREAK;
    if (params.iRecMode != PBREAK)
      params.iRecMode = PBREAK;
    if (params.iModeThreshold < RELATIVE_THRESHOLD or params.iModeThreshold >= MAXMODE_THRESHOLD)
      params.iModeThreshold = RELATIVE_THRESHOLD;
    if (params.iRelativeThreshold < 5 or params.iRelativeThreshold >= 50)
      params.iRelativeThreshold = 15;
    if (params.iAbsoluteThreshold < -110 or params.iAbsoluteThreshold >= -30)
      params.iAbsoluteThreshold = -90;
    params.iBattery = 20;
    params.iDrawBar = 50;
    params.bLight   = false;
    if (params.iLanguage < LENGLISH or params.iLanguage >= MAXLANGUAGE)
      params.iLanguage = LENGLISH;
    bOK = true;
  }
  return bOK;
}

//-------------------------------------------------------------------------
// Writing parameters to save them
void CGenericTest::WriteParams()
{ 
  // Mechanism to minimize number of write on EEPROM
  // If the parameters in EEPROM are identical, no writing
  // Saving the current settings
  Parameters saveParams = params;
  // Reading parameters in EEPROM
  if (!ReadParams() or memcmp(&params, &saveParams, sizeof(Parameters)) != 0)
  {
    // They are different, restitution of the backup and writing in EEPROM
    params = saveParams;
    int address = 0;
    uint16_t uiControle = 0x55AA;
    char *p = (char *)&uiControle;
    for (int i = 0; i<(int)sizeof(uint16_t); i++)
      EEPROM.write(address++, *p++);
    p = (char *)&params;
    for (int i = 0; i<(int)sizeof(Parameters); i++)
      EEPROM.write(address++, *p++);
  }
}

//-------------------------------------------------------------------------
// Class for testing Modifiers not in line

const char *txtTModes[2][3]     = {{"Tst Mode", "Param. B", "Param. A"},
                                   {"Mode Tst", "Param. B", "Param. A"}};
const char *txtIDXMT_MODE[]     = {"[>%s]",
                                   "[>%s]"};
const char *txtIDXTDATE[]       = {"%s  -",
                                   "%s  -"};
const char *txtIDXTHEURE[]      = {"%s",
                                   "%s"};
const char *txtTime[]           = {"%02d s",
                                   "%02d s"};

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CModeTest::CModeTest()
{
  // Normal modifiers first and then lines modifiers
  iPlayrecPos = 0;
  iNewMode    = MODETEST;
  // Create all modifiers
  // IDXMT_MODE      Button to change mode
  LstModifParams.push_back(new CEnumModifier       ( ">&$$$$$$$", false, true, txtIDXMT_MODE, &idxCurrentMode, MODEPARAMSB, (const char **)txtTModes, 0, 0));
  // IDXMT_LIGHT     Light modifier
  LstModifParams.push_back(new CModificatorLight   ( &params.bLight, 106, 0));
  // IDXMT_BAT       Battery status
  LstModifParams.push_back(new CBatteryModifier    ( &params.iBattery, 36, 10, 69, 0));
  // IDXMT_CURDATE   Current date
  LstModifParams.push_back(new CDateModifier       ( "&$/&$/&$  -", false, txtIDXTDATE, 0, 55));
  // IDXMT_CURTIME   Current time
  LstModifParams.push_back(new CHourModifier       ( "&$:&$:&$ ", false, txtIDXTHEURE, NULL, 78, 55));
  // IDXMT_PLAY      Play Start/Stop button
  LstModifParams.push_back(new CPlayRecModifier    ( &params.iPlayMode, true, false, 25, 10, 0, 10));
  // IDXMT_REC       Record Start/Stop button
  LstModifParams.push_back(new CPlayRecModifier    ( &params.iRecMode , false, true, 25, 10, 35, 10));
  // IDXMT_POS       Play/Rec position
  LstModifParams.push_back(new CDrawBarModifier    ( &iPlayrecPos, 120, 10, 0, 22));
  // IDXMT_TIME      Time position
  LstModifParams.push_back(new CModificatorInt     ( "&$ s", false, txtTime, &iTime, 0, 999, 1, 0, 34));
  // IDXMT_TMAX      Time max
  LstModifParams.push_back(new CModificatorInt     ( "&$ s", false, txtTime, &iTimeMax, 0, 999, 1, 100, 34));
}

//-------------------------------------------------------------------------
// Beginning of the mode
void CModeTest::BeginMode()
{
  // Calling the basic function
  CGenericTest::BeginMode();
  // Initialize unchangeable modifiers
  LstModifParams[IDXMT_BAT    ]->SetEditable( false);
  LstModifParams[IDXMT_CURDATE]->SetEditable( false);
  LstModifParams[IDXMT_CURTIME]->SetEditable( false);
  LstModifParams[IDXMT_POS    ]->SetEditable( false);
  LstModifParams[IDXMT_TIME   ]->SetEditable( false);
  LstModifParams[IDXMT_TMAX   ]->SetEditable( false);
  // No modifier in line so no index to initialize
  // Initialize time
  iTime    = 0;
  iTimeMax = 0;
}

//-------------------------------------------------------------------------
// Called on end of change of a modifier
// If a mode change, returns the requested mode
int CModeTest::OnEndChange(
  int idxModifier // Index of affected modifier
  )
{
  int iReturn = NOMODE;
  switch(idxModifier)
  {
  case IDXMT_PLAY:
  case IDXMT_REC :
    // For time simulation on Play/Rec
    iTimeMax = 30;
    uiTime = millis();
    break;
  case IDXMT_MODE:
    // Change mode to Parameters B
    iReturn = MODEPARAMSB;
    break;
  }
  return iReturn;
}

//-------------------------------------------------------------------------
// Mode display on the screen
// This method is called regularly by the main loop
// By default, it displays the list of modifiers
void CModeTest::PrintMode()
{
  if (params.iPlayMode == PPLAY or params.iRecMode == PREC)
  {
    // Time in second
    iTime = (millis() - uiTime) / 1000;
    if (iTime > iTimeMax)
    {
      iTime = 0;
      uiTime = millis();
    }
    // Set position in percent
    iPlayrecPos = iTime * 100 / iTimeMax;
  }
  // Calling the basic function
  CGenericTest::PrintMode();  
}
  
//-------------------------------------------------------------------------
// Class for testing Modifiers in line
const char *txtRecModes[2][3]   = {{"Auto      ", "Heterodyne", "Mic test  "},
                                   {"Auto      ", "Heterodyne", "Mic test  "}};
const char *txtAutoManu[2][2]   = {{"Auto ", "Manual"},
                                   {"Auto ", "Manuel"}};
const char *txtOuiNon[2][2]     = {{" No", "Yes"},
                                   {"Non", "Oui"}};
const char *txtLanguage[2][2]   = {{"English", "Francais"},
                                   {"English", "Francais"}};
const char *txtIDXRETURN[]      = {" Go back to Test...",
                                   " Retour test..."};
const char *txtIDXNAME[]        = {" Name [%s]",
                                   " Nom  [%s]"};
const char *txtIDXDURSTEP[]     = {" Duration step  %03ds",
                                   " Duree pas      %03ds"};
const char *txtIDXDURDIGIT[]    = {" Duration digit %03ds",
                                   " Duree digit    %03ds"};
const char *txtIDXFREQUSTEP[]   = {" Freq step  %05.1f kHz",
                                   " Freq pas   %05.1f kHz"};
const char *txtIDXFREQUDIGIT[]  = {" Freq digit %05.1f kHz",
                                   " Freq digit %05.1f kHz"};
const char *txtIDXRECMODE[]     = {" Mode %s",
                                   " Mode %s"};
const char *txtIDXHETERODYNE[]  = {" Heterodyne %s",
                                   " Heterodyne %s"};
const char *txtIDXLIGHT[]       = {" Min light    %s ",
                                   " Lumiere min. %s "};
const char *txtIDXTIME[]        = {" Start Hour %s",
                                   " Heure deb. %s"};
const char *txtIDXBAT[]         = {" Int. Batt. %d%%",
                                   " Batt. Int. %d%%"};
const char *txtIDXSD[]         =  {" SD % 3.0fGO %d%% %04dwav",
                                   " SD % 3.0fGO %d%% %04dwav"};
const char *txtIDXLUM[]         = {" Min light    %s ",
                                   " Lumiere min. %s "};
const char *txtIDXLANG[]        = {" Language: %s",
                                   " Langue :  %s"};
const char *txtIDXDATE[]        = {" Date  %s ",
                                   " Date  %s "};
const char *txtIDXHEURE[]       = {" Hour  %s",
                                   " Heure %s"};

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CModeParametersA::CModeParametersA()
{
  // Normal modifiers first and then lines modifiers
  // Create lines modifiers
  // IDXPA_RETURN      First line to return in CModeTest
  LstModifParams.push_back(new CPushButtonModifier ( " &$$ ", true, txtIDXRETURN  , 0, 0));
  // IDXPA_CURTIME     Current hour modifier
  LstModifParams.push_back(new CHourModifier       ( " Hour  &$:&$:&$ ", true, txtIDXHEURE, NULL, 0, 0));
  // IDXPA_CURDATE     Current date modifier
  LstModifParams.push_back(new CDateModifier       ( " Date  &$/&$/&$ ", true, txtIDXDATE, 0, 0));
  // IDXPA_NAME        Name modifier
  LstModifParams.push_back(new CCharModifier       ( " Name [&&&&&&&&]", true, txtIDXNAME, params.sName, MAXCHARNAME, false, false, false, true, true, 0, 0));
  // IDXPA_DURSTEP      Duration modifier step by step
  LstModifParams.push_back(new CModificatorInt     ( " Duration step  &$$ ", true, txtIDXDURSTEP, &params.iDuration, 5, 100, 1, 0, 0));
  // IDXPA_DURDIGIT     Duration modifier digit by digit
  LstModifParams.push_back(new CModificatorInt     ( " Duration digit &&& ", true, txtIDXDURDIGIT, &params.iDuration, 5, 100, 1, 0, 0));
  // IDXPA_FREQUSTEP   Frequency modifier step by step
  LstModifParams.push_back(new CFloatModifier      ( " Freq step  &$$$$ ", true, txtIDXFREQUSTEP, &params.fFrequency, 15.0, 130.0, 0.1, 0, 0));
  // IDXPA_FREQUDIGIT  Frequency modifier digit by digit
  LstModifParams.push_back(new CFloatModifier      ( " Freq digit &&&.& ", true, txtIDXFREQUDIGIT, &params.fFrequency, 15.0, 130.0, 0.1, 0, 0));
  // IDXPA_RECMODE     Mode modifier,
  LstModifParams.push_back(new CEnumModifier       ( " Mode &$$$$$$$$$ ", true, false, txtIDXRECMODE, &params.iRecordMode, MAX_REC, (const char **)txtRecModes, 0, 0));
  // IDXPA_HETERODYNE  Heterodyne modifier
  LstModifParams.push_back(new CBoolModifier       ( " Heterodyne &$$$$$ ", true, false, txtIDXHETERODYNE, &params.bAutoHeterodyne, (const char **)txtAutoManu, 0, 0));
  // IDXPA_TIME        Time modifier
  LstModifParams.push_back(new CHourModifier       ( " Start Hour &$:&$ ", true, txtIDXTIME, params.sTime, 0, 0));
  // IDXPA_BAT         Battery modifier
  LstModifParams.push_back(new CPushButtonModifier ( " &$$ ", true, txtIDXBAT, 0, 0));
  // IDXPA_LIGHT       Brightness modifier
  LstModifParams.push_back(new CBoolModifier       ( "              &$$ ", true, false, txtIDXLUM, &params.bLight, (const char **)txtOuiNon, 0, 0));
  // IDXPA_LANGUAGE    Language modifier
  LstModifParams.push_back(new CEnumModifier       ( "           &$$$$$$$$ ", true, false, txtIDXLANG, (int *)&params.iLanguage, MAXLANGUAGE, (const char **)txtLanguage, 0, 0));
  // IDXPA_SD          SD modifier
  LstModifParams.push_back(new CSDModifier         ( " Del      0000wav &", true, txtIDXSD, (const char *)"wav", 0, 0));
}

//-------------------------------------------------------------------------
// Beginning of the mode
void CModeParametersA::BeginMode()
{
  // Calling the basic function
  CGenericTest::BeginMode();
  // Initialize index, all modifier in lines
  iYFirstLine          = 0;
  iNbLines             = MAX_LINES;
  iFirstVisibleIdxLine = IDXPA_RETURN;
  iLastVisibleIdxLine  = IDXPA_RETURN + iNbLines - 1;
  // Control of the SD card
  LstModifParams[IDXPA_SD]->SetbCalcul( true);
  // Initialize battery text
  sprintf(sTxtBat, txtIDXBAT[params.iLanguage], params.iBattery);
  ((CPushButtonModifier *)LstModifParams[IDXPA_BAT])->SetString(sTxtBat);
}

//-------------------------------------------------------------------------
// Called on end of change of a modifier
// If a mode change, returns the requested mode
int CModeParametersA::OnEndChange(
  int idxModifier // Index of affected modifier
  )
{
  int iReturn = NOMODE;
  switch(idxModifier)
  {
  case IDXPA_BAT:
    // Change battery value for test
    if (params.iBattery > 0)
      params.iBattery--;
    else
      params.iBattery = 100;
    // Initialize battery text with the new value
    sprintf(sTxtBat, txtIDXBAT[params.iLanguage], params.iBattery);
    break;
  case IDXPA_LIGHT:
    // Change light
    if (params.bLight)
      pDisplay->setContrast(1);
    else
      pDisplay->setContrast(255);
    break;
  case IDXPA_LANGUAGE:
    // Init new language
    CGenericMode::SetLanguage( params.iLanguage);
    break;
  case IDXPA_RETURN:
    // Change mode to Test Mode
    iReturn = MODETEST;
    break;
  }
  return iReturn;
}

const char *txtModeThreshold[2][2]= {{"relative", "absolute"},
                                     {"relatif ", "absolu  "}};
const char *txtIDXMThreshold[]     = {" Threshold: %s",
                                      " Seuil    : %s"};
const char *txtIDXRELTHRESHOLD[]   = {" Rel. Threshold %02ddB",
                                      " Seuil rel.     %02ddB"};
const char *txtIDXABSTHRESHOLD[]   = {" Abs. Threshold%04ddB",
                                      " Seuil abs.    %04ddB"};

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CModeParametersB::CModeParametersB()
{
  // Normal modifiers first and then lines modifiers
  // Create normal modifiers
  // IDXPB_MODE      Button to change mode
  LstModifParams.push_back(new CEnumModifier       ( ">&$$$$$$$", false, true, txtIDXMT_MODE, &idxCurrentMode, MODEPARAMSB, (const char **)txtTModes, 0, 0));
  // IDXPB_LIGHT     Light modifier
  LstModifParams.push_back(new CModificatorLight   ( &params.bLight, 106, 0));
  // IDXPB_BAT       Battery status
  LstModifParams.push_back(new CBatteryModifier    ( &params.iBattery, 36, 10, 69, 0));
  // Create lines modifiers
  // IDXPB_CURTIME     Current hour modifier
  LstModifParams.push_back(new CHourModifier       ( " Hour  &$:&$:&$ ", true, txtIDXHEURE, NULL, 0, 0));
  // IDXPB_CURDATE     Current date modifier
  LstModifParams.push_back(new CDateModifier       ( " Date  &$/&$/&$ ", true, txtIDXDATE, 0, 0));
  // IDXPB_NAME        Name modifier
  LstModifParams.push_back(new CCharModifier       ( " Name [&&&&&&&&]", true, txtIDXNAME, params.sName, MAXCHARNAME, false, false, false, true, true, 0, 0));
  // IDXPB_MTHRESHOLD  Mode Threshold
  LstModifParams.push_back(new CEnumModifier       ( " Threshold: &$$$$$$$", true, true, txtIDXMThreshold, &params.iModeThreshold, MAXMODE_THRESHOLD, (const char **)txtModeThreshold, 0, 0));
  // IDXPB_RTHRESHOLD  Relative Threshold
  LstModifParams.push_back(new CModificatorInt     ( " Rel. Threshold &$ ", true, txtIDXRELTHRESHOLD, &params.iRelativeThreshold, 5, 50, 1, 0, 0));
  // IDXPB_ATHRESHOLD  Absolute Threshold
  LstModifParams.push_back(new CModificatorInt     ( " Abs. Threshold&$$$ ", true, txtIDXABSTHRESHOLD, &params.iAbsoluteThreshold, -110, -30, 1, 0, 0));
  // IDXPB_DURSTEP      Duration modifier step by step
  LstModifParams.push_back(new CModificatorInt     ( " Duration step  &$$ ", true, txtIDXDURSTEP, &params.iDuration, 5, 100, 1, 0, 0));
  // IDXPB_DURDIGIT     Duration modifier digit by digit
  LstModifParams.push_back(new CModificatorInt     ( " Duration digit &&& ", true, txtIDXDURDIGIT, &params.iDuration, 5, 100, 1, 0, 0));
  // IDXPB_FREQUSTEP   Frequency modifier step by step
  LstModifParams.push_back(new CFloatModifier      ( " Freq step  &$$$$ ", true, txtIDXFREQUSTEP, &params.fFrequency, 15.0, 130.0, 0.1, 0, 0));
  // IDXPB_FREQUDIGIT  Frequency modifier digit by digit
  LstModifParams.push_back(new CFloatModifier      ( " Freq digit &&&.& ", true, txtIDXFREQUDIGIT, &params.fFrequency, 15.0, 130.0, 0.1, 0, 0));
  // IDXPB_RECMODE     Mode modifier,
  LstModifParams.push_back(new CEnumModifier       ( " Mode &$$$$$$$$$ ", true, false, txtIDXRECMODE, &params.iRecordMode, MAX_REC, (const char **)txtRecModes, 0, 0));
  // IDXPB_HETERODYNE  Heterodyne modifier
  LstModifParams.push_back(new CBoolModifier       ( " Heterodyne &$$$$$ ", true, false, txtIDXHETERODYNE, &params.bAutoHeterodyne, (const char **)txtAutoManu, 0, 0));
  // IDXPB_TIME        Time modifier
  LstModifParams.push_back(new CHourModifier       ( " Start Hour &$:&$ ", true, txtIDXTIME, params.sTime, 0, 0));
  // IDXPB_LANGUAGE    Language modifier
  LstModifParams.push_back(new CEnumModifier       ( "           &$$$$$$$$ ", true, false, txtIDXLANG, (int *)&params.iLanguage, MAXLANGUAGE, (const char **)txtLanguage, 0, 0));
  // IDXPB_SD          SD modifier
  LstModifParams.push_back(new CSDModifier         ( " Del      0000wav &", true, txtIDXSD, (const char *)"wav", 0, 0));
}

//-------------------------------------------------------------------------
// Beginning of the mode
void CModeParametersB::BeginMode()
{
  // Calling the basic function
  CGenericTest::BeginMode();
  LstModifParams[IDXMT_BAT    ]->SetEditable( false);
  // Initialize index, no line modifer on the firt line
  iYFirstLine    = 9;
  iNbLines       = MAX_LINES - 1;
  iFirstVisibleIdxLine = IDXPB_CURTIME;
  iLastVisibleIdxLine  = IDXPB_CURTIME + iNbLines;	  // Warning, one parameter not valid (absolute/relative threshold)
  // Control of the SD card
  LstModifParams[IDXPB_SD]->SetbCalcul( true);
  // Control Threshold mode
  if (params.iModeThreshold == RELATIVE_THRESHOLD)
  {
    LstModifParams[IDXPB_RTHRESHOLD]->SetbValid( true);
    LstModifParams[IDXPB_ATHRESHOLD]->SetbValid(false);
  }
  else
  {
    LstModifParams[IDXPB_RTHRESHOLD]->SetbValid(false);
    LstModifParams[IDXPB_ATHRESHOLD]->SetbValid( true);
  }
}

//-------------------------------------------------------------------------
// Called on end of change of a modifier
// If a mode change, returns the requested mode
int CModeParametersB::OnEndChange(
  int idxModifier // Index of affected modifier
  )
{
  int iReturn = NOMODE;
  switch(idxModifier)
  {
  case IDXPB_LANGUAGE:
    // Init new language
    CGenericMode::SetLanguage( params.iLanguage);
    break;
  case IDXPB_MODE:
    // Change mode to Parameters A
    iReturn = MODEPARAMSA;
    break;
  case IDXPB_MTHRESHOLD:
    if (params.iModeThreshold == RELATIVE_THRESHOLD)
    {
      LstModifParams[IDXPB_RTHRESHOLD]->SetbValid( true);
      LstModifParams[IDXPB_ATHRESHOLD]->SetbValid(false);
    }
    else
    {
      LstModifParams[IDXPB_RTHRESHOLD]->SetbValid(false);
      LstModifParams[IDXPB_ATHRESHOLD]->SetbValid( true);
    }
    break;
  }
  return iReturn;
}

