ModesModifier library
=
Library for managing operating modes, parameter modifiers and command buttons on a small 128x64 pixel monochrome display.

**It mainly uses the following libraries:**

- **SDFat** <https://github.com/greiman/SdFat>
- **U8G2Lib** <https://github.com/olikraus/u8g2>

**It is compatible with Teensy 3.6, Teensy 4.1 and ESP32 cards** 

Version V1.14
-
- Add CLineEnumModifier

Version 1.13
-
- Add Teensy 4.1

Version 1.12
-
- Fixed CSecondModifier behavior

Version 1.11
-
- Time display, correction of unwanted jumps of 2 or 3s

Version 1.10
-
- Addition CSecondModifier, management class of a seconds modifier in time mode (hh:mm:ss)

Version 1.9
-
- Addition of "ConstChar.c" for multilingual management
- Addition of German and Dutch languages
- Set SDFAT V2.0.4 for working with SDXC in FAT64 (exFAT). Formatting to FAT32 is no longer necessary

Version 1.8
-
- Add CEnumModifier::SetStringArray To change array of strings for different values of the enum
- Optimizing the memory used by LogFile::AddLog
- LogFile::AddLog return false on write acces problem on SD card
- CModeError, Possible automatic end of error mode with a reset

Version 1.5
-
- Use SdFatSdioEX for fast write

Version 1.4
-
- Correction of a bug on the display of the lines at the end of modification of a parameters

Version 1.3
-
- Fixed a bug when the screen is composed with lin and non-online modifiers. Il the numbre of lines in the screen is less than MAXLINES, iNbLines must be set befor call of CGenericMode::BeginMode
- Fixed a bug on CBatteryModifier when charge is 100%

Version 1.2
-
- If no parameter of type line, selection of the valid 1st
- Added ability to exit error mode
- Fixed a bug when the screen is composed only of non-online modifiers

Version 1.1
-
- Add static LogFile::GetbConsole()

Version 1.0
-
- Fix bug impossible to del wav in English language
- Fix bad initialisation in line parameters
- Put all LogFile functions in static
- Add parameter to write or not parameters in CGenericMode::EndMode
- Fix bug in LogFile::AddLog (buffer overflow for long string)

Version 0.9
-
- Add parameter to read or not parameters in CGenericMode::BeginMode

Version 0.8
-
- Correct bugs when bLeftRight is false
- Added management of the validity of each element of an enum 

Version 0.6
-
- Fix CGenericMode::ChangeMode
- Fix CSDModifier display

Version 0.5
-
- Added GetKeyManager and SetInterrupt on CKeyManager to reset interrupts after a sleep on a Teensy
- Improved operating mode change with the ability to use static or dynamic instances
- Added virtual function OnLoop in CGenericMode
- CDrawBarModifier can visualize data with a min / max other than 0/100
- CGenericMode if presence of modifying in line initializes by default the indexes for the max number of lines
- Improved cursor management for the selected line to prioritize the selected line in the middle of the screen
- Add the CGenericMode::AddPrint function to display information outside the edits
- Add CGenericMode::UpdateScreen function to improve the display speed

Version 0.4
-
- Added the possibility to manage operating modes in static or dynamic

Version 0.3
-
- Add ModesModifiersConfig.h to choose SDFat or SDFs library

Version 0.2
-
- Version for Teensy 3.6 and ESP32 january 2019
- Added the ability to change the font
- Invalid parameters are not displayed in line mode

Version 0.1
-
- Initial version december 2018
