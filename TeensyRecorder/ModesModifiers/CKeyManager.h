//-------------------------------------------------------------------------
//! \file CKeyManager.h
//! \brief Generic class for managing buttons
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/* 
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CKEYMANAGER_H
#define CKEYMANAGER_H

// Key definitions
#define K_NO      0   //!< No action key
#define K_UP      1   //!< Key Up
#define K_DOWN    2   //!< Key Down
#define K_PUSH    3   //!< Key Push
#define K_LEFT    4   //!< Key Left
#define K_RIGHT   5   //!< Key Right
#define K_MODEA   6   //!< Key change mode A
#define K_MODEB   7   //!< Key change mode B

#define REBOUNDTIME 50		//!< Min time in ms of button rebounds
#define KEYHOLDTIME 400		//!< Hold time to take into account a button that remains in a pressed position
#define HOLDFORINTSPEED  8	//!< Number of hold to switch to intermediate speed
#define HOLDFORMAXSPEED  15	//!< Number of hold to switch to maximum speed

//-------------------------------------------------------------------------
//! \class CKeyManager
//! \brief Management class of 4 or 2 direction button, a central push and two others buttons
//! Use interrupts
//! How to use :
//! On setup create an instance and init keys pins
//! CKeyManager keyManager;
//! keyManager.begin( 1, 2, 3, 4, 5);
//! On loop, evrey 60ms
//! unsigned short key = keyManager.GetKey();
class CKeyManager
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  CKeyManager();

  //-------------------------------------------------------------------------
  //! \brief Manager launch with initialisation of the buttons pins
  //! \param pinP  Pin for Push        Key
  //! \param pinU  Pin for Up          Key
  //! \param pinB  Pin for Bottom      Key
  //! \param pinR  Pin for Right       Key (Not use if 0)
  //! \param pinL  Pin for Left        Key (Not use if 0)
  //! \param pinM  Pin for Change mode Key (Not use if 0)
  //! \param pinPR Pin for Play/Rec    Key (Not use if 0)
  void Begin(
      unsigned char pinP,
      unsigned char pinU,
      unsigned char pinB,
      unsigned char pinR=0,
      unsigned char pinL=0,
      unsigned char pinM=0,
      unsigned char pinPR=0
      );

  //-------------------------------------------------------------------------
  //! \brief Initializing interrupt functions
  void SetInterrupt();
  
  //-------------------------------------------------------------------------
  //! \brief Reading buttons, returns the code of the action
  //! To call regularly (more than TIMEREBOUND)
  unsigned short GetKey();
  
  //-------------------------------------------------------------------------
  //! \brief Returns true if the Left / Right buttons are used
  static bool IsLeftRight();
  
  //-------------------------------------------------------------------------
  //! \brief Returns the CKeyManager instance
  static CKeyManager *GetKeyManager() {return pInstance;};
  
protected:
  //-------------------------------------------------------------------------
  //! \brief Returns true if the Push button was pressed
  bool IsKPush();
  
  //-------------------------------------------------------------------------
  //! \brief Returns true if the Up button was pressed
  bool IsKUp();
  
  //-------------------------------------------------------------------------
  // ! \briefReturns true if the Down button was pressed
  bool IsKDown();
  
  //-------------------------------------------------------------------------
  //! \brief Returns true if the Right button was pressed
  bool IsKRight();
  
  //-------------------------------------------------------------------------
  //! \brief Returns true if the Left button was pressed
  bool IsKLeft();
  
  //-------------------------------------------------------------------------
  //! \brief Returns true if the Change mode button was pressed
  bool IsKChangeMode();
  
  //-------------------------------------------------------------------------
  //! \brief Returns true if the Play/Rec button was pressed
  bool IsKPlayRec();
  
  //-------------------------------------------------------------------------
  //! \brief Static function for managing interruption of Push pin
#if defined(__IMXRT1062__) // Teensy 4.1
  static void OnPushChange();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  static void OnPushChange();
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
  static void IRAM_ATTR OnPushChange( void* arg);
#endif
    
  //-------------------------------------------------------------------------
  //! \brief Static function for managing interruption of Up pin
#if defined(__IMXRT1062__) // Teensy 4.1
  static void OnUpChange();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  static void OnUpChange();
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
  static void IRAM_ATTR OnUpChange( void* arg);
#endif
  
  //-------------------------------------------------------------------------
  //! \brief Static function for managing interruption of Down pin
#if defined(__IMXRT1062__) // Teensy 4.1
  static void OnDownChange();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  static void OnDownChange();
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
  static void IRAM_ATTR OnDownChange( void* arg);
#endif
  
  //-------------------------------------------------------------------------
  //! \brief Static function for managing interruption of Left pin
#if defined(__IMXRT1062__) // Teensy 4.1
  static void OnLeftChange();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  static void OnLeftChange();
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
  static void IRAM_ATTR OnLeftChange( void* arg);
#endif
    
  //-------------------------------------------------------------------------
  //! \brief Static function for managing interruption of Right pin
#if defined(__IMXRT1062__) // Teensy 4.1
  static void OnRightChange();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  static void OnRightChange();
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
  static void IRAM_ATTR OnRightChange( void* arg);
#endif
    
  //-------------------------------------------------------------------------
  //! \brief Static function for managing interruption of Change mode A pin
#if defined(__IMXRT1062__) // Teensy 4.1
  static void OnChangeModeA();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  static void OnChangeModeA();
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
  static void IRAM_ATTR OnChangeModeA( void* arg);
#endif
    
  //-------------------------------------------------------------------------
  //! \brief Static function for managing interruption of Change mode B pin
#if defined(__IMXRT1062__) // Teensy 4.1
  static void OnChangeModeB();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  static void OnChangeModeB();
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
  static void IRAM_ATTR OnChangeModeB( void* arg);
#endif
    
private:
  //! Pointer on the instance of the class
  static CKeyManager *pInstance;
  
  //! Pin for Push   Key
  unsigned char PinPush;
  
  //! Pin for Up     Key
  unsigned char PinUp;
  
  //! Pin for Down   Key
  unsigned char PinDown;
  
  //! Pin for Left   Key
  unsigned char PinLeft;
  
  //! Pin for Right  Key
  unsigned char PinRight;

  //! Pin for Change mode A Key
  unsigned char PinChangeModeA;

  //! Pin for Change mode B Key
  unsigned char PinChangeModeB;

  //! Boolean indicating a change on the buttons
  bool bPushChange, bUpChange, bDownChange, bRightChange, bLeftChange, bChangeModeAChange, bChangeModeBChange;

  //! Time of last button change
  unsigned long uiTPush, uiTUp, uiTDown, uiTRight, uiTLeft, uiTChangeModeA, uiTChangeModeB;

  //! Timeout on holds up and down keys
  unsigned long uiTimeOutUp, uiTimeOutDown;

  //! Number of Up and Down keys to go faster
  unsigned long uiMaintUp, uiMaintDown;

  //! Time on GetKey call
  unsigned long uiTimeGetKey;
};

#endif  /* CKEYMANAGER_H */


