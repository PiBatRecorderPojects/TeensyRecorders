/* 
 * File:   ConstChar.c
   Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.
   Many thanks to Edwin Houwertjes for the translations in German and Dutch.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 #include "ModesModifiersConfig.h"

//------------------------------------------------------
// Strings used in the library to easily add a language
//------------------------------------------------------

/*
  To add a language, you must:
  - Add the language to the LANGUAGE enum at the start of the ModesModifiersConfig.h file
  - Add to each string of the ConstChar.c file the translation of the message in the new language
  Order of languages :
  LENGLISH    = 0
  LFRENCH     = 1
  LGERMAN     = 2
  LDUTCH      = 3
*/

// Error mode title without error code
const char *txtERROR[]       = {" ERROR ! ",
                                " ERREUR !",
				                " FEHLER !",
    							" FOUT !  "};
// Error mode title with error code
const char *txtERRORNUM[]    = {" ERROR %d ! ",
                                " ERREUR %d !",
						        " FEHLER %d !",
				                " FOUT %d !  "};
// Error mode, 1st message to restart the device
const char *txtStop[]        = {"Stop the device"     ,
                                "Stopper l'appareil"  ,
						        "Ger\xe4t auschalten.",
							    "Toestel uitschakelen."};
// Error mode, second message to restart the device on possibly 2 lines
const char *txtCorrectA[]    = {"Correct and restart"   ,
                                "Corriger et remettre"  ,
								"Corrigieren und wieder",
								"Herstellen en opnieuw"};
const char *txtCorrectB[]    = {" "                 ,
                                "en marche"         ,
						        "einschalten bitte!",
							    "inschakelen a.u.b!"};
// Text of the little gadget to change the screen brightness (x = low, * = high)
const char *txtLum[MAXLANGUAGE][2]     = {{"*", "x"},
                   					      {"*", "x"},
							              {"*", "x"},
                                          {"*", "x"}};
// Test for log in error mode
const char *txtLOGERRORMODE[]   = {"Switching to Error mode (%s)",
                                   "Passage en mode Erreur (%s)" ,
                                   "Switching to Error mode (%s)",
                                   "Switching to Error mode (%s)"};
