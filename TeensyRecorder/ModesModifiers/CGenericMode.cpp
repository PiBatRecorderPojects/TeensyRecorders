/* 
 * File:   CGenericMode.cpp
   Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 /*
  * - CGenericMode
  * -   CModeError
  * - LogFile
  */
#include "ModesModifiersConfig.h"
#include <U8g2lib.h>
#include <time.h>
#include <TimeLib.h>
#include "CGenericMode.h"

#ifdef MMC_SDFAT
  #include "SdFat.h"
#endif
#ifdef MMC_SDFAT_EXT
  #include "SdFat.h"
#endif
#ifdef SD_FAT_TYPE
  #include "SdFat.h"
#endif
#ifdef MMC_SDFS
  #include "SdFs.h"
#endif

// Macro pour Reset du Teensy 3.6
#define RESTART_ADDR_T36       0xE000ED0C
#define READ_RESTART_T36()     (*(volatile uint32_t *)RESTART_ADDR_T36)
#define WRITE_RESTART_T36(val) ((*(volatile uint32_t *)RESTART_ADDR_T36) = (val))

// send reboot command -----
#define RESTART_T4  SCB_AIRCR = 0x05FA0004

extern const char *txtLOGERRORMODE[];

//-------------------------------------------------------------------------
// Function to create or activate a new mode
// In dynamic mode, need to first delete the previous mode and then create the new mode
// In static mode, need just to sawp the pointer of the current mode
// User need to define this function. Look at the examples
extern CGenericMode* CreateNewMode(
	CGenericMode* pOldMode,		// Pointer on the previous mode of operation
	int idxMode					      // New mode of operation index
	);

// Screen manager
extern U8G2 *pDisplay;

#if defined(__IMXRT1062__) // Teensy 4.1
  // Teensy 3.6 Specific SD Card Manager
  #ifdef MMC_SDFAT
    extern SdFatSdio sd;
  #endif
  #ifdef MMC_SDFAT_EXT
	extern SdFatSdioEX sd;
  #endif
  #ifdef MMC_SDFS
    extern SdFs sd;
  #endif
  #ifdef SD_FAT_TYPE
    #if SD_FAT_TYPE == 0
      extern SdFat sd;
    #endif
  #endif
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  // Teensy 3.6 Specific SD Card Manager
  #ifdef MMC_SDFAT
    extern SdFatSdio sd;
  #endif
  #ifdef MMC_SDFAT_EXT
	extern SdFatSdioEX sd;
  #endif
  #ifdef MMC_SDFS
    extern SdFs sd;
  #endif
  #ifdef SD_FAT_TYPE
    #if SD_FAT_TYPE == 0
      extern SdFat sd;
    #endif
  #endif
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
  // ESP32 SD Card Manager
  #ifdef MMC_SDFAT
    extern SdFat sd;
  #endif
  #ifdef MMC_SDFS
    extern SdFs sd;
  #endif
  #ifdef SD_FAT_TYPE
    #if SD_FAT_TYPE == 0
      extern SdFat sd;
    #endif
  #endif
#endif

//-------------------------------------------------------------------------
// Generic class for managing a mode of operation
// Defines the minimal interface for managing a mode of operation
// Manages a list of modifiers

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CGenericMode::CGenericMode()
{
  // Normal modifiers first and then lines modifiers
  idxSelParam    = 0;
  iFirstIdxLine  = -1;
  iLastIdxLine   = -1;
  idxCurrentMode = -1;
  iYFirstLine    = 0;
  iNewMode       = NOMODE;
  iNbLines       = MAX_LINES;
  bModifParam    = false;
  bRedraw        = false;
  pCurrentMode   = NULL;
  bReadParams    = true;
  bWriteParams   = true;
}

//-------------------------------------------------------------------------
// Destructor, delete the list of modifiers
CGenericMode::~CGenericMode()
{
  for (int i=0; i<(int)LstModifParams.size(); i++)
    delete LstModifParams[i];
  LstModifParams.clear();
}

//-------------------------------------------------------------------------
// Beginning of the mode
void CGenericMode::BeginMode()
{
  //Serial.println("CGenericMode::BeginMode");
  if (bReadParams)
    // Read settings
    ReadParams();
  // Prepare modifiers
  if (LstModifParams.size() > 0)
  {
    // Initialize index for lines modifiers
    idxSelParam    = -1;
    iFirstIdxLine  = -1;
    iLastIdxLine   = -1;
    iFirstVisibleIdxLine = -1;
    iLastVisibleIdxLine  = -1;
    for (int i=0; i<(int)LstModifParams.size(); i++)
    {
	  if (idxSelParam < 0 and LstModifParams[i]->GetbValid())
	    idxSelParam = i;
      if (LstModifParams[i]->IsInLine() == true and iFirstIdxLine < 0)
        // First line index
        iFirstIdxLine = i;
      else if (LstModifParams[i]->IsInLine() == true)
        // Last line index
        iLastIdxLine = i;
    }
	/*if (iLastIdxLine > (int)LstModifParams.size())
	  iLastIdxLine = (int)LstModifParams.size() - 1;*/
	//Serial.printf("CGenericMode::BeginMode A iFirstIdxLine %d, iLastIdxLine %d, size %d\n", iFirstIdxLine, iLastIdxLine, (int)LstModifParams.size());
	if (iFirstIdxLine >= 0)
	{
	  // By default, first valid line selected
	  int idx = iFirstIdxLine;
      while (LstModifParams[idx]->GetbValid() == false or LstModifParams[idx]->IsEditable() == false)
	  {
        idx++;
        if (idx > iLastIdxLine)
          // We went around without finding
          break;
      };
	  if (idx >= 0 and idx <= iLastIdxLine)
	  {
        // Select first line modifier
		idxSelParam = idx;
        LstModifParams[idxSelParam]->SetbSelect( true);
	  }
	  // First line visible is first valid line
      iFirstVisibleIdxLine = idxSelParam;
	  //Serial.printf("CGenericMode::BeginMode B iFirstVisibleIdxLine %d, iLastVisibleIdxLine %d, iNbLines %d\n", iFirstVisibleIdxLine, iLastVisibleIdxLine, iNbLines);
	  // Find last line visible
	  int iNbValid = 0;
	  for (int i=iFirstIdxLine; i<=iLastIdxLine; i++)
	  {
	    if (LstModifParams[i]->GetbValid() and LstModifParams[i]->IsEditable())
		  iNbValid++;
	    if (iNbValid == MAX_LINES)
	    {
		  iLastVisibleIdxLine = i;
		  break;
	    }
	  }
	  if (iLastVisibleIdxLine < 0 and iNbValid > 1)
		iLastVisibleIdxLine = iFirstVisibleIdxLine + iNbValid - 1;
	  iMiddle  = iNbLines / 2;
	  if (2 * iMiddle < iNbLines)
	    iMiddle++;
	  //Serial.printf("CGenericMode::BeginMode C iFirstVisibleIdxLine %d, iLastVisibleIdxLine %d, iNbLines %d\n", iFirstVisibleIdxLine, iLastVisibleIdxLine, iNbLines);
	}
	else
	{
	  // By default, first valid param selected
	  for (int idx=0; idx<=(int)LstModifParams.size(); idx++)
	  {
	    if (LstModifParams[idx]->GetbValid() and LstModifParams[idx]->IsEditable())
	    {
		  // Select first modifier
		  idxSelParam = idx;
		  LstModifParams[idxSelParam]->SetbSelect( true);
		  break;
	    }
      };
	}
  }
  else
    idxSelParam = -1;
  bRedraw = true;
  //Serial.printf("CGenericMode::BeginMode Fin iFirstVisibleIdxLine %d, iLastVisibleIdxLine %d, iNbLines %d\n", iFirstVisibleIdxLine, iLastVisibleIdxLine, iNbLines);
}

//-------------------------------------------------------------------------
// End of the mode
void CGenericMode::EndMode()
{
  if (bWriteParams)
    // Save settings
    WriteParams();
}

//-------------------------------------------------------------------------
// Called on end of change of a modifier
// If a mode change, returns the requested mode
int CGenericMode::OnEndChange(
  int idxModifier // Index of affected modifier
  )
{
  // Basic, do nothing
  return NOMODE;
}
  
//-------------------------------------------------------------------------
// Reading saved settings
bool CGenericMode::ReadParams()
{
  // Basic, do nothing
  return false;
}

//-------------------------------------------------------------------------
// Writing parameters to save them
void CGenericMode::WriteParams()
{
  // Basic, do nothing
}
  
//-------------------------------------------------------------------------
// Mode display on the screen
// This method is called regularly by the main loop
// By default, it displays the list of modifiers
void CGenericMode::PrintMode()
{
  //Serial.println("CGenericMode::PrintMode");
  // Clear screen
  pDisplay->clearBuffer();
  // Display information before modifiers
  PrePrint();
  // Print modifiers list not in line
  for (int i=0; i<(int)LstModifParams.size(); i++)
    if (LstModifParams[i]->GetbValid() == true and LstModifParams[i]->IsInLine() == false)
      LstModifParams[i]->PrintParam( LstModifParams[i]->GetX(), LstModifParams[i]->GetY());
  if (iFirstVisibleIdxLine >= 0)
  {
    // Print modifiers list in line and visible
    int y = iYFirstLine;
    for (int i=iFirstVisibleIdxLine; i<=iLastVisibleIdxLine; i++)
    {
      if (i >= 0 and i < (int)LstModifParams.size()  and LstModifParams[i]->IsInLine() and LstModifParams[i]->GetbValid() )
      {
        LstModifParams[i]->PrintParam( 0, y);
        y += HLINEPIX;
      }
	}
  }
  // Display information outside of modifiers
  AddPrint();
  // Update screen
  UpdateScreen();
  bRedraw = false;
  //Serial.println("CGenericMode::PrintMode OK");
}

//-------------------------------------------------------------------------
//! \brief  To display information outside of modifiers at the begin of PrintMode
void CGenericMode::PrePrint()
{
  // nothing to do
}

//-------------------------------------------------------------------------
// To display information outside of modifiers
void CGenericMode::AddPrint()
{
  // nothing to do
}
  
//-------------------------------------------------------------------------
// Update screen (standard behavior = pDisplay->sendBuffer())
void CGenericMode::UpdateScreen()
{
  pDisplay->sendBuffer();
}

//-------------------------------------------------------------------------
// Keyboard order processing
// If the key is a mode change key, returns the requested mode
// This method is called regularly by the main loop
// By default, handles modifiers
int CGenericMode::KeyManager(
  unsigned short key  // Touch to treat
  )
{
  int newMode = NOMODE;
  int oldMode = idxCurrentMode;
  int iOldIdx = idxSelParam;
  if (key != K_NO)
  {
    if (bModifParam)
    {
      // A modifier is modifying, we pass the pressed key
      LstModifParams[idxSelParam]->ReceiveKey( key);
      // We check if the parameter is still modifying
      bModifParam = LstModifParams[idxSelParam]->IsModif();
      if (!bModifParam and oldMode != idxCurrentMode)
        // New mode
        newMode = idxCurrentMode;
      if (!bModifParam)
        // Call function on end of change of a modifier
        newMode = OnEndChange( idxSelParam);
    }
    else
    {
      // No modifiers is modifying, we navigate in the modifiers
      switch (key)
      {
      case K_DOWN:
        // Selection of the following valid and editable parameter down
        do {
          idxSelParam++;
          if (idxSelParam >= (int)LstModifParams.size())
            idxSelParam = 0;
          if (idxSelParam == iOldIdx)
            // We went around without finding
            break;
        } while (LstModifParams[idxSelParam]->GetbValid() == false or LstModifParams[idxSelParam]->IsEditable() == false);
        break;
      case K_UP:
        // Selection of the previous parameter valid and modifiable upwards
        do {
          idxSelParam--;
          if (idxSelParam < 0)
            idxSelParam = LstModifParams.size() - 1;
          if (idxSelParam == iOldIdx)
            // We went around without finding
            break;
        } while (LstModifParams[idxSelParam]->GetbValid() == false or LstModifParams[idxSelParam]->IsEditable() == false);
        break;
      case K_PUSH:
		//Serial.println("CGenericMode::KeyManager K_PUSH");
        // Start modifying a parameter
        LstModifParams[idxSelParam]->StartModif();
        bModifParam = true;
        // We check if the parameter is still modifying
        bModifParam = LstModifParams[idxSelParam]->IsModif();
        if (!bModifParam and oldMode != idxCurrentMode)
          // New mode
          newMode = idxCurrentMode;
        if (!bModifParam)
          // Call function on end of change of a modifier
          newMode = OnEndChange( idxSelParam);
      }
    }
    if (iOldIdx != idxSelParam)
    {
      // Deselect the old one and select the new one
      LstModifParams[iOldIdx    ]->SetbSelect( false);
      LstModifParams[idxSelParam]->SetbSelect( true);
    }
	SetIdxLines();
  }
  else if (iNewMode != NOMODE)
  {
    // Switch to a new operating mode
    newMode  = iNewMode;
    iNewMode = NOMODE;
  }
  return newMode;
}

//-------------------------------------------------------------------------
//! \brief  Initializes the indexes of visible lines
void CGenericMode::SetIdxLines()
{
  //Serial.printf("iFirstVisibleIdxLine %d, iLastVisibleIdxLine %d, iMiddle %d, idxSelParam %d, iLastIdxLine %d, iNbLines %d\n", iFirstVisibleIdxLine, iLastVisibleIdxLine, iMiddle, idxSelParam, iLastIdxLine, iNbLines);
  int i, iNbValid = 0;
  // Test how many valid lines before the selected parameter
  int iNbBefore = 0;
  for (i=idxSelParam; i>=iFirstIdxLine; i--)
  {
    if (LstModifParams[i]->GetbValid())
	  iNbBefore++;
  }
  // Test how many valid lines after the selected parameter
  int iNbAfter = 0;
  for (i=idxSelParam; i<iLastIdxLine; i++)
  {
    if (LstModifParams[i]->GetbValid())
	  iNbAfter++;
  }
  if (iNbBefore < iMiddle)
  {
    // We start from the beginning
	//Serial.println("T");
    iFirstVisibleIdxLine = iFirstIdxLine;
	iNbValid = 0;
    for (i=iFirstVisibleIdxLine; i<=iLastIdxLine; i++)
    {
      if (LstModifParams[i]->GetbValid())
 	    iNbValid++;
      if (iNbValid == iNbLines)
	  {
	    iLastVisibleIdxLine = i;
	    break;
	  }
    }
  }
  else if (iNbAfter < iMiddle)
  {
	// We start from the end
	//Serial.println("E");
    iLastVisibleIdxLine = iLastIdxLine;
	iNbValid = 0;
    for (i=iLastVisibleIdxLine; i>=iFirstIdxLine; i--)
    {
      if (LstModifParams[i]->GetbValid())
 	    iNbValid++;
      if (iNbValid == iNbLines)
	  {
	    iFirstVisibleIdxLine = i;
	    break;
	  }
    }
  }
  else
  {
	// We start from the middle (selected parameter)
	//Serial.println("M");
	iNbValid = 0;
    for (i=idxSelParam; i>=iFirstIdxLine; i--)
    {
      if (LstModifParams[i]->GetbValid())
 	    iNbValid++;
      if (iNbValid == iMiddle)
	  {
	    iFirstVisibleIdxLine = i;
	    break;
	  }
    }
    for (i=idxSelParam+1; i<iLastIdxLine; i++)
    {
      if (LstModifParams[i]->GetbValid())
 	    iNbValid++;
      if (iNbValid == iNbLines)
	  {
	    iLastVisibleIdxLine = i;
	    break;
	  }
    }
  }
  //Serial.printf("iFirstVisibleIdxLine %d, iLastVisibleIdxLine %d, iMiddle %d, idxSelParam %d, iLastIdxLine %d, iNbValid %d, iNbBefore %d, iNbAfter %d\n", iFirstVisibleIdxLine, iLastVisibleIdxLine, iMiddle, idxSelParam, iLastIdxLine, iNbValid, iNbBefore, iNbAfter);
}

//-------------------------------------------------------------------------
// Possible management of fund tasks
void CGenericMode::OnLoop()
{
	// Nothing to do
}

//-------------------------------------------------------------------------
// Change curent mode, never call this function in a mode
// Always called by loop
FLASHMEM void CGenericMode::ChangeMode(
  int iNewM  // New mode
  )
{
  if (pCurrentMode != NULL)
    // End curent mode
    pCurrentMode->EndMode();
  idxCurrentMode = -1;
  iNewMode = NOMODE;
  // Create or swap to the new mode
  // In dynamic mode, need to first delete the previous mode and then create the new mode
  // In static mode, need just to sawp the pointer of the current mode
  pCurrentMode = CreateNewMode( pCurrentMode, iNewM);
  if (pCurrentMode != NULL)
  {
    // Begin the new mode
    idxCurrentMode = iNewM;
    //LogFile::AddLog( LLOG, "CGenericMode::ChangeMode new and BeginMode for a new mode (%d)", idxCurrentMode); 
    pCurrentMode->BeginMode();
  }
  /*else
    LogFile::AddLog( LLOG, "CGenericMode::ChangeMode for iNewMode=%d, pCurrentMode is NULL !", idxCurrentMode);*/
}

//-------------------------------------------------------------------------
// Initialize the language used
void CGenericMode::SetLanguage( int iL)
{
  iLanguage = iL;
  CGenericModifier::SetLanguage( iL);
};

// Current mode
CGenericMode *CGenericMode::pCurrentMode = NULL;

// Index of active mode
int CGenericMode::idxCurrentMode = -1;

// New possible mode of operation (default NOMODE)
// the change of operating mode is mandatory at the output of ManageKey
int CGenericMode::iNewMode = NOMODE;

// Current language
int CGenericMode::iLanguage = LENGLISH;

// SD card error code
uint8_t CGenericMode::uiSDError = 0;

//-------------------------------------------------------------------------
// Error mode management class
// Displays the error and waits for a shutdown

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CModeError::CModeError()
{
  uiEndTime = 0;
}

//                               012345678901234567890
extern const char *txtERROR[];
extern const char *txtERRORNUM[];
extern const char *txtStop[];
extern const char *txtCorrectA[];
extern const char *txtCorrectB[];

//-------------------------------------------------------------------------
// Beginning of the mode
void CModeError::BeginMode()
{
  LogFile::AddLog( LLOG, txtLOGERRORMODE[CGenericModifier::GetLanguage()], ErrorText);
  // Clear screen and print error
  pDisplay->clearBuffer();
  pDisplay->setFont(u8g2_font_6x13B_mf);  // Hauteur 9
  pDisplay->setCursor(0,0);
  if (uiSDError == 0)
    pDisplay->print( txtERROR[CGenericModifier::GetLanguage()]);
  else
    pDisplay->printf( txtERRORNUM[CGenericModifier::GetLanguage()], uiSDError);
  uiSDError = 0;
  pDisplay->setFont(u8g2_font_6x10_mf);  // Hauteur 7
  pDisplay->setCursor(0,20);
  pDisplay->setCursor(0,12);
  if (strlen(ErrorText) <= 21)
	pDisplay->print( ErrorText);
  else
  {
    char txt[MAXCHAR_ERROR];
	strncpy( txt, ErrorText, 21);
	txt[21] = 0;
	pDisplay->print( txt);
    pDisplay->setCursor(0,20);
	pDisplay->print( &ErrorText[21]);
  }
  pDisplay->setCursor(0,34);
  pDisplay->print(txtStop[CGenericModifier::GetLanguage()]);
  pDisplay->setCursor(0,44);
  pDisplay->print(txtCorrectA[CGenericModifier::GetLanguage()]);
  pDisplay->setCursor(0,53);
  pDisplay->print(txtCorrectB[CGenericModifier::GetLanguage()]);
  pDisplay->sendBuffer();
  if (uiTimeMax != 0)
    // Time to automatically end error mode
    uiEndTime = millis() + uiTimeMax;
  else
    uiEndTime = 0;
}

//-------------------------------------------------------------------------
// Set error text
void CModeError::SetTxtError(
  char *pStr  // Error text
  )
{
  strcpy( ErrorText, pStr);
}

//-------------------------------------------------------------------------
// Set new mode on any key
// iMode new mode
void CModeError::SetNewMode(
  int iMode,
  uint32_t uiTime
  )
{
  iNewMode = iMode;
  uiTimeMax = uiTime;
}

//-------------------------------------------------------------------------
// Mode display on the screen
// This method is called regularly by the main loop
// By default, it displays the list of modifiers
void CModeError::PrintMode()
{
  // Do nothing
}

//-------------------------------------------------------------------------
// Keyboard order processing
// If the key is a mode change key, returns the requested mode
// This method is called regularly by the main loop
// By default, handles modifiers
int CModeError::KeyManager(
  unsigned short key  // Touch to treat
  )
{
  if (key != K_NO and iNewMode != 0)
    return iNewMode;
  if (uiEndTime != 0 and millis() > uiEndTime)
  {
	Serial.println("CModeError::KeyManager Restart software !");
	delay(1000);
#if defined(__IMXRT1062__) // Teensy 4.1
	RESTART_T4;
#elif defined(__MK66FX1M0__) // Teensy 3.6
    // Reset
    WRITE_RESTART_T36(0x5FA0004);
#else
    return iNewMode;
#endif
  }
  // Do nothing
  return NOMODE;
}

// Error text
char CModeError::ErrorText[MAXCHAR_ERROR];
// New mode on key
int  CModeError::iNewMode = 0;
// Max time (ms) before stop error mode if different of 0 (default)
uint32_t CModeError::uiTimeMax = 0;

//-------------------------------------------------------------------------
// Management class of software flow log file

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
LogFile::LogFile(
  const char *pSoftName,  // Software name
  LEVEL_LOG level,        // Log level
  bool bCons              // Info to double the Log with console outputs
  )
{
  // Verification of a single instance
  if (LogFile::pInstance == NULL)
  {
    LogFile::pInstance = this;
    // Save parameters
    if (strlen(pSoftName) < MAXSOFTNAME)
      strcpy(softName, pSoftName);
    else
    {
      strncpy(softName, pSoftName, MAXSOFTNAME-1);
      softName[MAXSOFTNAME] = 0;
    }
    sprintf( logFileName, "/Log%s.txt", softName);
    sprintf( oldFileName, "/Log%s.old", softName);
    LogLevel = level;
    bConsole = bCons;
  }
  bCheckLines = false;
  bStarting   = false;
}

//-------------------------------------------------------------------------
//! \brief Set Log file name
void LogFile::SetLogFileName(
  char *pSoftName
  )
{
  if (strlen(pSoftName) < MAXSOFTNAME)
    strcpy(softName, pSoftName);
  /*sprintf( logFileName, "/Log%s.txt", softName);
  sprintf( oldFileName, "/Log%s.old", softName);*/
  sprintf( logFileName, "Log%s.txt", softName);
  sprintf( oldFileName, "Log%s.old", softName);
}
  
//-------------------------------------------------------------------------
// Static function of adding a line in the log file
bool LogFile::AddLog(
  LEVEL_LOG Level,        // Level of the log line, if the level is higher than the active level, the log is not memorized
  const char *pLogString, // Pointer on the string (\ n at the end unnecessary)
  ...                     // List of composition parameters of sprintf style string
  )
{
  bool bOK = true;
  // If the log level is compatible
  if (bStarting and Level <= LogFile::LogLevel and LogFile::pInstance != NULL)
  {
    if (!bCheckLines)
      CheckLines();
    // String prepare
    char sLine[256];
    // Dreate date and time
    char sTime[25];
#if defined(__IMXRT1062__) // Teensy 4.1
    sprintf( sTime, "%02d/%02d/%02d - %02d:%02d:%02d", day(), month(), year()-2000, hour(), minute(), second());
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
    sprintf( sTime, "%02d/%02d/%02d - %02d:%02d:%02d", day(), month(), year()-2000, hour(), minute(), second());
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
    time_t curtime;
    struct tm * timeinfo;
    time(&curtime);
    timeinfo = localtime (&curtime);
    strftime(sTime, 24, "%d/%m/%y - %H:%M:%S", timeinfo);
#endif
    // Line string with the date, time and software name at the beginning of the line
    sprintf (sLine, "%s %s ", sTime, softName);
    int iLen = strlen(sLine);
    va_list args;
    va_start (args, pLogString);
    vsnprintf (&sLine[iLen], 255-iLen, pLogString, args);
    va_end (args);
    // Add parameters in the line
#if defined(__IMXRT1062__) // Teensy 4.1
    #ifdef MMC_SDFAT
      // Select root SD
      sd.chdir( "/", true);
      // Open log file
      SdFile dataFile;
    #endif
    #ifdef MMC_SDFAT_EXT
      // Select root SD
      sd.chdir( "/", true);
      // Open log file
      SdFile dataFile;
    #endif
    #ifdef SD_FAT_TYPE
      // Select root SD
      sd.chdir( "/");
      // Open log file
      FsFile dataFile;
    #endif
    #ifdef MMC_SDFS
      // Select root SD
      sd.chdir();
      // Open log file
      FsFile dataFile;
    #endif
    if (dataFile.open( logFileName, O_CREAT | O_WRITE | O_APPEND))
    {
      // Write log
      dataFile.println( sLine);
      // Close file
      dataFile.sync();
      dataFile.close();
    }
    else
    {
#ifdef SD_FAT_TYPE
      CGenericMode::uiSDError = sd.card()->errorCode();
#else
      CGenericMode::uiSDError = sd.cardErrorCode();
#endif
      Serial.printf("LogFile::AddLog Impossible to open %s ! cardErrorCode %d\n", logFileName, CGenericMode::uiSDError);
      bOK = false;
    }
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
    #ifdef MMC_SDFAT
      // Select root SD
      sd.chdir( "/", true);
      // Open log file
      SdFile dataFile;
    #endif
    #ifdef MMC_SDFAT_EXT
      // Select root SD
      sd.chdir( "/", true);
      // Open log file
      SdFile dataFile;
    #endif
    #ifdef SD_FAT_TYPE
      // Select root SD
      sd.chdir( "/");
      // Open log file
      FsFile dataFile;
    #endif
    #ifdef MMC_SDFS
      // Select root SD
      sd.chdir();
      // Open log file
      FsFile dataFile;
    #endif
    if (dataFile.open( logFileName, O_CREAT | O_WRITE | O_APPEND))
    {
      // Write log
      dataFile.println( sLine);
      // Close file
      dataFile.sync();
      dataFile.close();
    }
    else
    {
#ifdef SD_FAT_TYPE
      CGenericMode::uiSDError = sd.card()->errorCode();
#else
      CGenericMode::uiSDError = sd.cardErrorCode();
#endif
      Serial.printf("LogFile::AddLog Impossible to open %s ! cardErrorCode %d\n", logFileName, CGenericMode::uiSDError);
      bOK = false;
    }
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
    #ifdef MMC_SDFAT
      // Select root SD
      sd.chdir( "/", true);
      // Open log file
      SdFile dataFile;
    #endif
    #ifdef MMC_SDFAT_EXT
      // Select root SD
      sd.chdir( "/", true);
      // Open log file
      SdFile dataFile;
    #endif
    #ifdef SD_FAT_TYPE
      // Select root SD
      sd.chdir( "/");
      // Open log file
      SdFile dataFile;
    #endif
    #ifdef MMC_SDFS
      // Select root SD
      sd.chdir();
      // Open log file
      FsFile dataFile;
    #endif
    if (dataFile.open( logFileName, O_CREAT | O_WRITE | O_APPEND))
    {
      // Write log
      dataFile.println( sLine);
      // Close file
      dataFile.sync();
      dataFile.close();
    }
    else
    {
      Serial.printf("LogFile::AddLog Impossible to open %s !\n", logFileName);
      bOK = false;
    }
#endif
    //Serial.printf("AddLog sLine %d\n", strlen(sLine));
    if (LogFile::bConsole)
      // Write to console
      Serial.println( sLine);
  }
  return bOK;
}

//-------------------------------------------------------------------------
// Check the number of lines in the log file
void LogFile::CheckLines()
{
  //Serial.println("LogFile::CheckLines");
  bCheckLines = true;
#if defined(__IMXRT1062__) // Teensy 4.1
  #ifdef MMC_SDFAT
    // Select root SD
    sd.chdir( "/", true);
    SdFile f;
  #endif
  #ifdef MMC_SDFAT_EXT
    // Select root SD
    sd.chdir( "/", true);
    SdFile f;
  #endif
  #ifdef SD_FAT_TYPE
    // Select root SD
    sd.chdir( "/");
    SdFile f;
  #endif
  #ifdef MMC_SDFS
    // Select root SD
    sd.chdir();
    // File for checking
    FsFile f;
  #endif
  //Serial.println("LogFile::CheckLines before f.open");
  // Checking the number of lines in the file
  int iNbLines = 0;
  char sLine[350];
  if (f.open( logFileName, O_READ))
  {
    //Serial.println("LogFile::CheckLines before while");
    // Reading the lines
    while (f.fgets(sLine, 255) > 0)
    {
      iNbLines++;
      if (iNbLines > 512)
        break;
    }
    // Close the file
    f.close();
  }
  //Serial.printf("LogFile::CheckLines iNbLines %d\n", iNbLines);
  // If more than 512 lines
  if (iNbLines > 512)
  {
    // Rename the current file to .old
    sd.rename( logFileName, oldFileName);
    // The last 256 lines are copied into a new .txt file
    #ifdef MMC_SDFAT
      SdFile fold, fnew;
    #endif
    #ifdef MMC_SDFAT_EXT
      SdFile fold, fnew;
    #endif
    #ifdef SD_FAT_TYPE
      SdFile fold, fnew;
    #endif
    #ifdef MMC_SDFS
      FsFile fold, fnew;
    #endif
    if (!fold.open( oldFileName, O_READ))
        Serial.printf("LogFile::LogFile Impossible to open %s !\n", oldFileName);
    else if (!fnew.open( logFileName, O_CREAT | O_WRITE | O_APPEND))
        Serial.printf("LogFile::LogFile Impossible to open %s !\n", logFileName);
    else
    {
      // Reading the 256 1st lines that are forgotten
      for (int i=0; i<256; i++)
        fold.fgets(sLine, 255);
      // Write the last lines in the new file
      while (fold.fgets(sLine, 255) > 0)
        fnew.print(sLine);
      // Close files
      fnew.sync();
      fnew.close();
      fold.close();
    }
    // Delete .old file
    sd.remove( oldFileName);
  }
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  #ifdef MMC_SDFAT
    // Select root SD
    sd.chdir( "/", true);
    SdFile f;
  #endif
  #ifdef MMC_SDFAT_EXT
    // Select root SD
    sd.chdir( "/", true);
    SdFile f;
  #endif
  #ifdef SD_FAT_TYPE
    // Select root SD
    sd.chdir( "/");
    SdFile f;
  #endif
  #ifdef MMC_SDFS
    // Select root SD
    sd.chdir();
    // File for checking
    FsFile f;
  #endif
  //Serial.println("LogFile::CheckLines before f.open");
  // Checking the number of lines in the file
  int iNbLines = 0;
  char sLine[350];
  if (f.open( logFileName, O_READ))
  {
    //Serial.println("LogFile::CheckLines before while");
    // Reading the lines
    while (f.fgets(sLine, 255) > 0)
    {
      iNbLines++;
      if (iNbLines > 512)
        break;
    }
    // Close the file
    f.close();
  }
  //Serial.printf("LogFile::CheckLines iNbLines %d\n", iNbLines);
  // If more than 512 lines
  if (iNbLines > 512)
  {
    // Rename the current file to .old
    sd.rename( logFileName, oldFileName);
    // The last 256 lines are copied into a new .txt file
    #ifdef MMC_SDFAT
      SdFile fold, fnew;
    #endif
    #ifdef MMC_SDFAT_EXT
      SdFile fold, fnew;
    #endif
    #ifdef SD_FAT_TYPE
      SdFile fold, fnew;
    #endif
    #ifdef MMC_SDFS
      FsFile fold, fnew;
    #endif
    if (!fold.open( oldFileName, O_READ))
        Serial.printf("LogFile::LogFile Impossible to open %s !\n", oldFileName);
    else if (!fnew.open( logFileName, O_CREAT | O_WRITE | O_APPEND))
        Serial.printf("LogFile::LogFile Impossible to open %s !\n", logFileName);
    else
    {
      // Reading the 256 1st lines that are forgotten
      for (int i=0; i<256; i++)
        fold.fgets(sLine, 255);
      // Write the last lines in the new file
      while (fold.fgets(sLine, 255) > 0)
        fnew.print(sLine);
      // Close files
      fnew.sync();
      fnew.close();
      fold.close();
    }
    // Delete .old file
    sd.remove( oldFileName);
  }
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
  // Select root SD
  sd.chdir( "/", true);
  // Checking the number of lines in the file
  int iNbLines = 0;
  char sLine[255];
  File f;
  if (f.open( logFileName, FILE_READ))
  {
    // Reading the lines
    while (f.fgets(sLine, 255) > 0)
    {
      iNbLines++;
      if (iNbLines > 512)
        break;
    }
    // Close the file
    f.close();
  }
  // If more than 512 lines
  if (iNbLines > 512)
  {
    // Rename the current file to .old
    sd.rename( logFileName, oldFileName);
    // The last 256 lines are copied into a new .txt file
    SdFile fold, fnew;
    if (!fold.open( oldFileName, O_READ))
        Serial.printf("LogFile::LogFile Impossible to open %s !\n", oldFileName);
    else if (!fnew.open( logFileName, O_CREAT | O_WRITE | O_APPEND))
        Serial.printf("LogFile::LogFile Impossible to open %s !\n", logFileName);
    else
    {
      // Reading the 256 first lines that are forgotten
      for (int i=0; i<256; i++)
        fold.fgets(sLine, 255);
      // Write the last lines in the new file
      while (fold.fgets(sLine, 255) > 0)
        fnew.print(sLine);
      // Close files
      fnew.sync();
      fnew.close();
      fold.close();
    }
    // Delete .old file
    sd.remove( oldFileName);
  }
#endif
}

// Static info that indicate if log is starting
bool LogFile::bStarting = false;
  
// Static info of checking the number of lines in the file
bool LogFile::bCheckLines = false;

// Pointer on the instance of the class
LogFile *LogFile::pInstance = NULL;

// Static Info of the active level of the log
LEVEL_LOG LogFile::LogLevel = LINFO;

// Static Info to double the Log with console outputs
bool LogFile::bConsole = true;

// Static info of the software name displayed in each trace
char LogFile::softName[MAXSOFTNAME+1];

// Log file name
char LogFile::logFileName[MAXLOGFILENAME+1];

// Old log file name
char LogFile::oldFileName[MAXLOGFILENAME+1];


