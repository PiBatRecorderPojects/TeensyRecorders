/* 
 * File:   CModifiers.cpp
   Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "ModesModifiersConfig.h"
#include <algorithm>
#include <string.h>
#include <stdio.h>
#ifdef MMC_SDFAT
  #include "SdFat.h"
#endif
#ifdef MMC_SDFAT_EXT
  #include "SdFat.h"
#endif
#ifdef SD_FAT_TYPE
  #include "SdFat.h"
#endif
#ifdef MMC_SDFS
  #include "SdFs.h"
#endif
#include <sys/time.h>                   // struct timeval
#include <U8g2lib.h>
#include <TimeLib.h>
#include "CKeyManager.h"
#include "CModifiers.h"

 /*
  * Management of parameter modifiers. For each parameter, there are 2 display modes:
  * - Online, for example "> Min Freq 120kHz" with "120" editable.
  *     The selected line displays an arrow at the beginning of the line
  *     In modification, the modifiable zone appears in inverse
  * - Anywhere on the screen, for example "[Player]" with "Player" editable.
  *     If the parameter is not modifiable, it is never selected
  *     If the parameter is selected, the 1st and last characters are in reverse video
  *     In modification, the modifiable zone appears in inverse
  * CGenericModifier
  *   CCharModifier
  *   CModificatorInt
  *     CDrawBarModifier
  *   CFloatModifier
  *   CEnumModifier
  *     CLineEnumModifier
  *   CBoolModifier
  *     CModificatorLight
  *   CPushButtonModifier
  *   CDateModifier
  *   CHourModifier
  *   CSDModifier
  *   CPlayRecModifier
  *   CBatteryModifier
  */

// Gestionnaire écran
extern U8G2 *pDisplay;

#if defined(__IMXRT1062__) // Teensy 4.1
  // Teensy 3.6 Specific SD Card Manager
  #ifdef MMC_SDFAT
    extern SdFatSdio sd;
  #endif
  #ifdef MMC_SDFAT_EXT
	extern SdFatSdioEX sd;
  #endif
  #ifdef MMC_SDFS
    extern SdFs sd;
  #endif
  #ifdef SD_FAT_TYPE
    #if SD_FAT_TYPE == 0
      extern SdFat sd;
    #endif
  #endif
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  // Teensy 3.6 Specific SD Card Manager
  #ifdef MMC_SDFAT
    extern SdFatSdio sd;
  #endif
  #ifdef MMC_SDFAT_EXT
	extern SdFatSdioEX sd;
  #endif
  #ifdef MMC_SDFS
    extern SdFs sd;
  #endif
  #ifdef SD_FAT_TYPE
    #if SD_FAT_TYPE == 0
      extern SdFat sd;
    #endif
  #endif
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
  // ESP32 SD Card Manager
  #ifdef MMC_SDFAT
    extern SdFat sd;
  #endif
  #ifdef MMC_SDFS
    extern SdFs sd;
  #endif
  #ifdef SD_FAT_TYPE
    #if SD_FAT_TYPE == 0
      extern SdFat sd;
    #endif
  #endif
#endif


//-------------------------------------------------------------------------
// Base class of a field modifier
// Manages all behaviors common to modifiers
// Manages two modes of modification:
// - Online for Settings Mode
// - On site for other modes

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CGenericModifier::CGenericModifier(
  const char *pIndicModif,  // Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
  bool bLine,               // true to indicate a change on a line, false otherwise
  bool bPush,               // true to indicate a push to change the value
  const char **pForm,       // Parameter format like " Min Freq %04dkHz"
  int ix,                   // X coordinate of the top left point of the modifier
  int iy                    // Y coordinate of the top left point of the modifier
  )
{
  // Default values
  bOnLine = bLine;
  bModifPush = bPush;
  bSelect = false;
  bModif = false;
  bRedraw = true;
  bEditable = true;
  bCalcul = false;
  iCurpos = -1;
  iIdxModif = 0;
  lineParam[0] = 0;
  bValid = true;
  iX = ix;
  iY = iy;
  if (pIndicModif != NULL)
    strcpy( indicModif, pIndicModif);
  else
    strcpy( indicModif, "0");
  pFormat = pForm;
  if (pFormat != NULL and pIndicModif != NULL and strlen(pIndicModif) > MAX_LINEPARAM)
    Serial.printf("***** CGenericModifier pIndicModif [%s] (pForm [%s]) too long ! *****\n", pIndicModif, pForm[0]);
  FindBeginEndModif();
}

//-------------------------------------------------------------------------
// Destructor
CGenericModifier::~CGenericModifier()
{
  // Nothing to do
}

//-------------------------------------------------------------------------
// Initializes the validity of the parameter
void CGenericModifier::SetbValid( bool bVal)
{
  bool bOldValid = bValid;
  bValid = bVal;
  if (bValid != bOldValid)
    bRedraw = true;
}

//-------------------------------------------------------------------------
// Key Event Processing
void CGenericModifier::ReceiveKey(
  int iKey  // Touch to treat
  )
{
  // If Push and selected parameter
  if (iKey == K_PUSH and bSelect)
  {
    // If set to change
    if (bModif)
    {
      if (bZones and !bLeftRight and ilstSIdxEditable[iIdxModif+1] != -1)
      {
        // We move to the next editable box (only solution if Left / Right buttons are absent)
        iIdxModif++;
        iCurpos = ilstSIdxEditable[iIdxModif];
      }
      else
      {
        // Validation of the parameter
        SetParam();
        // End change
        StopModif();
      }
    }
    else
      // We go into modification
      StartModif();
    bRedraw = true;
  }
  else if (bModif and bZones)
  {
    switch (iKey)
    {
    case K_RIGHT:
      // We move on to the next field
      iIdxModif++;
      if (iIdxModif >= iNbZones)
        iIdxModif = 0;
      bRedraw = true;
      break;
    case K_LEFT:
      // We go to the previous field
      iIdxModif--;
      if (iIdxModif < 0)
        iIdxModif = iNbZones - 1;
      bRedraw = true;
      break;
    }
  }
}

//-------------------------------------------------------------------------
// Display of the parameter
void CGenericModifier::PrintParam(
  int X,    // X position of the display
  int Y     // Y position of the display
  )
{
  if (!bModif)
  {
    // Initializing the display string
    GetString();
    if (bOnLine)
    {
      // We complete with blanks to erase the entire width of the line
      while (strlen(lineParam) < MAX_LINEPARAM)
        strcat(lineParam, " ");
    }
  }
  if (bModif and iIdxModif >= 0 and ilstSIdxEditable[iIdxModif] >= 0)
  {
    // Display with the modification part in inverted
    char tmp[MAX_LINEPARAM+5];
    int iD, iF;
    iD = ilstSIdxEditable[iIdxModif];
    iF = ilstEIdxEditable[iIdxModif];
    // Display of the beginning in normal
    strncpy(tmp, lineParam, iD);
    tmp[iD] = 0;
    DrawString( tmp, X, Y);
    X += (iWidthCar * strlen(tmp));
    // Display of the part in modification in inverted
    strncpy(tmp, &lineParam[iD], iF-iD+1);
    tmp[iF-iD+1] = 0;
    DrawString( tmp, X, Y, true);
    X += (iWidthCar * strlen(tmp));
    // Display of the end in normal
    if ((int)strlen(lineParam) > iF+1)
    {
      strncpy(tmp, &lineParam[iF+1], strlen(lineParam)-iF-1);
      tmp[strlen(lineParam)-iF-1] = 0;
      DrawString( tmp, X, Y);
    }
  }
  else if (bSelect and !bOnLine)
  {
    // Selected parameter and offline display, reverse 1st and last characters
    char tmp[MAX_LINEPARAM+5];
    tmp[0] = lineParam[0];
    tmp[1] = 0;
    DrawString( tmp, X, Y, true);
    X += (iWidthCar * strlen(tmp));
    strcpy( tmp, &(lineParam[1]));
    tmp[strlen(tmp)-1] = 0;
    DrawString( tmp, X, Y, false);
    X += (iWidthCar * strlen(tmp));
    DrawString( &(lineParam[strlen(lineParam)-1]), X, Y, true);
  }
  else
    // Display of the line without inverted part
    DrawString( lineParam, X, Y, false);
  // If selected line, we put an arrow at the beginning of the line
  if (bOnLine and bSelect)
  {
	pDisplay->setFont(pArrowFont);
    pDisplay->drawGlyph( 0, Y, cArrow);
    pDisplay->setFont(pCharFont);
  }
}

//-------------------------------------------------------------------------
// Returns a parameter string
// The return chain looks like " Min Freq 120kHz"
char *CGenericModifier::GetString()
{
  // No action in base class
  return NULL;
}

//-------------------------------------------------------------------------
// Updating the parameter with the displayed string
void CGenericModifier::SetParam()
{
  // No action in base class
}

//-------------------------------------------------------------------------
// Start changing the parameter
void CGenericModifier::StartModif()
{
  GetString();
  bModif = true;
  iIdxModif = 0;
  bRedraw = true;
}

//-------------------------------------------------------------------------
// Stop changing the parameter
void CGenericModifier::StopModif()
{
  GetString();
  iIdxModif = 0;
  bRedraw = true;
  bModif = false;
}

//-------------------------------------------------------------------------
// Calculation of the beginnings and ends of the fields to modify
void CGenericModifier::FindBeginEndModif()
{
  for (int i=0; i<MAX_MODIF; i++)
  {
    ilstSIdxEditable[i] = -1;
    ilstEIdxEditable[i] = -1;
  }
  bool bZone = false;
  bool bUn   = false;
  iIdxMin = MAX_MODIF;
  iIdxMax = 0;
  iNbZones = 0;
  // "Max frequency &$$$$ Hz"
  // "Date &$/&$/&$"
  for (int i=0, j=-1; i<(int)strlen(indicModif); i++)
  {
    if (indicModif[i] == '$' or indicModif[i] == '&')
    {
      if (!bZone)
      {
        // Nouvelle zone de modif
        j++;
        iNbZones++;
        ilstSIdxEditable[j] = i;
        ilstEIdxEditable[j] = i;
        bZone = true;
      }
      if (indicModif[i] == '&')
      {
        if (!bUn)
          // & trouvé
          bUn = true;
        else
        {
          // Nouvelle zone de modif
          j++;
          ilstSIdxEditable[j] = i;
          ilstEIdxEditable[j] = i;
          bZone = true;
          iNbZones++;
        }
      }
      ilstEIdxEditable[j] = i;
      iIdxMax = std::max( iIdxMax, i);
    }
    else
    {
      // Possible end of a modification zone
      bZone = false;
      bUn   = false;
    }
  }
  if (ilstSIdxEditable[1] == -1)
    bZones = false;
  else
    bZones = true;
  iIdxMin = ilstSIdxEditable[0];
  //Serial.printf("FindBeginEndModif indicModif [%s], bZones %d, iNbZones %d\n", indicModif, bZones, iNbZones);
}

//-------------------------------------------------------------------------
// Initializes the parameter selection
void CGenericModifier::SetbSelect( bool bSel)
{
  bool bOldSelect = bSelect;
  bSelect = bSel;
  if (bSelect != bOldSelect)
    bRedraw = true;
}

//-------------------------------------------------------------------------
// Returns true if we are on the last fiel of the parameter
bool CGenericModifier::IsLastZone()
{
  bool bOK = false;
  if (ilstSIdxEditable[iIdxModif+1] == -1)
    bOK = true;
  return bOK;
}

//-------------------------------------------------------------------------
// Displaying a string on the screen
void CGenericModifier::DrawString(
  char *pString,      // String to display
  int x,              // Position x of the beginning of the string
  int y,              // Position y of the beginning of the string
  bool bInverse       // Normal or inverted display
  )
{
  if (bInverse)
    pDisplay->setDrawColor(0);
  pDisplay->setCursor( x, y);
  pDisplay->print( pString);
  if (bInverse)
    pDisplay->setDrawColor(1);
}

//-------------------------------------------------------------------------
// Go to the next right field if present
// Returns false if there is no more field
bool CGenericModifier::NextZone()
{
  bool bOK = false;
  if (ilstSIdxEditable[iIdxModif+1] != -1)
  {
    iIdxModif++;
    iCurpos = ilstSIdxEditable[iIdxModif];
    bOK = true;
  }
  return bOK;
}

// Width of a character in pixel
int CGenericModifier::iWidthCar = LCHARPIX;

// Height of a character in pixel
int CGenericModifier::iHeightCar = HLINEPIX;

// Indicates whether the Left / Right buttons are used
bool CGenericModifier::bLeftRight = true;

// Language index
int CGenericModifier::iLanguage = LENGLISH;

// Font for string print
uint8_t *CGenericModifier::pCharFont = (uint8_t *)u8g2_font_6x10_mf;

// Font for arrow on selected line
uint8_t *CGenericModifier::pArrowFont = (uint8_t *)u8g2_font_open_iconic_arrow_1x_t;

// Char for arrow on selected line
char CGenericModifier::cArrow = 82;

//-------------------------------------------------------------------------
// Management class for a string modifier

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CCharModifier::CCharModifier(
  const char *pIndicModif,  // Pointer on the string indicating editable fields like " Name &$$$$$$$". & indicates first digit of a field, $ other digit of a field
  bool bLine,               // true to indicate a change on a line, false otherwise
  const char **pForm,       // Parameter format like " Name %s"
  char *pPar,               // Pointer on the string to modify
  int iLen,                 // Number of characters
  bool bOnlyNumber,         // Indicates whether only numbers 0 t0 9 are allowed
  bool bWhit,               // Indicates whether whites are allowed
  bool bSymb,               // Indicates whether symbols are allowed
  bool bTiret,              // Indicates whether the - and _ symbols are allowed
  bool bNumb,               // Indicates whether digits are allowed
  int ix,                   // X coordinate of the top left point of the modifier
  int iy                    // Y coordinate of the top left point of the modifier
  ):CGenericModifier( pIndicModif, bLine, false, pForm, ix, iy)
{
  // Initialization of the variables
  pParam = pPar;
  iNbChar = iLen;
  bNumbersOnly = bOnlyNumber;
  bWhite = bWhit;
  bSymboles = bSymb;
  bTirets = bTiret;
  bNumbers = bNumb;
}

//-------------------------------------------------------------------------
// Key Event Processing
void CCharModifier::ReceiveKey(
  int iKey  // Touch to treat
  )
{
  bool bTraite = false;
  // If parameter in modification
  if (bModif)
  {
    int iCur = ilstSIdxEditable[iIdxModif];
    // Init of min and max of the digit according to the context (A <-> z)
    switch (iKey)
    {
    case K_UP:
      if (lineParam[iCur] > '9' and bNumbersOnly)
        lineParam[iCur] = '0';
      else if (lineParam[iCur] == '~')
      {// ' ' ! " # $ % & ' ( ) * + ' - . / 0>9 : ; < = > ? @ A>Z [ \ ] ^ _ ' a>z { | ] ~
        if (bWhite)         lineParam[iCur] = ' ';
        else if (bSymboles) lineParam[iCur] = '!';
        else if (bTirets)   lineParam[iCur] = '-';
        else if (bNumbers)  lineParam[iCur] = '0';
        else                lineParam[iCur] = 'A';
      }
      else if (lineParam[iCur] == 'z' and !bSymboles)
      {// ' ' ! " # $ % & ' ( ) * + ' - . / 0>9 : ; < = > ? @ A>Z [ \ ] ^ _ ' a>z { | ] ~
        if (bWhite)         lineParam[iCur] = ' ';
        else if (bTirets)   lineParam[iCur] = '-';
        else if (bNumbers) lineParam[iCur] = '0';
        else                lineParam[iCur] = 'A';
      }
      else if (lineParam[iCur] == 'Z' and !bSymboles)
      {// ' ' ! " # $ % & ' ( ) * + ' - . / 0>9 : ; < = > ? @ A>Z [ \ ] ^ _ ' a>z { | ] ~
        if (bTirets)        lineParam[iCur] = '_';
        else                lineParam[iCur] = 'a';
      }
      else if (lineParam[iCur] == '_' and !bSymboles)
      {// ' ' ! " # $ % & ' ( ) * + ' - . / 0>9 : ; < = > ? @ A>Z [ \ ] ^ _ ' a>z { | ] ~
        lineParam[iCur] = 'a';
      }
      else if (lineParam[iCur] == '9' and !bSymboles)
      {// ' ' ! " # $ % & ' ( ) * + ' - . / 0>9 : ; < = > ? @ A>Z [ \ ] ^ _ ' a>z { | ] ~
        lineParam[iCur] = 'A';
      }
      else if (lineParam[iCur] == '-' and !bSymboles)
      {// ' ' ! " # $ % & ' ( ) * + ' - . / 0>9 : ; < = > ? @ A>Z [ \ ] ^ _ ' a>z { | ] ~
        lineParam[iCur] = '0';
      }
      else if (lineParam[iCur] == ' ' and !bSymboles)
      {// ' ' ! " # $ % & ' ( ) * + ' - . / 0>9 : ; < = > ? @ A>Z [ \ ] ^ _ ' a>z { | ] ~
        if (bTirets)        lineParam[iCur] = '-';
        else                lineParam[iCur] = '0';
      }
      else
        lineParam[iCur]++;
      bTraite = true;
      break;
    case K_DOWN:
      if (lineParam[iCur] < '0' and bNumbersOnly)
        lineParam[iCur] = '9';
      else if (lineParam[iCur] == ' ')
      {// ' ' ! " # $ % & ' ( ) * + ' - . / 0>9 : ; < = > ? @ A>Z [ \ ] ^ _ ' a>z { | ] ~
        if (bSymboles)      lineParam[iCur] = '~';
        else                lineParam[iCur] = 'z';
      }
      else if (lineParam[iCur] == '!')
      {// ' ' ! " # $ % & ' ( ) * + ' - . / 0>9 : ; < = > ? @ A>Z [ \ ] ^ _ ' a>z { | ] ~
        if (bWhite)         lineParam[iCur] = ' ';
        else if (bSymboles) lineParam[iCur] = '~';
        else                lineParam[iCur] = 'z';
      }
      else if (lineParam[iCur] == '-' and !bSymboles)
      {// ' ' ! " # $ % & ' ( ) * + ' - . / 0>9 : ; < = > ? @ A>Z [ \ ] ^ _ ' a>z { | ] ~
        if (bWhite)         lineParam[iCur] = ' ';
        else                lineParam[iCur] = 'z';
      }
      else if (lineParam[iCur] == '0' and !bSymboles)
      {// ' ' ! " # $ % & ' ( ) * + ' - . / 0>9 : ; < = > ? @ A>Z [ \ ] ^ _ ' a>z { | ] ~
        if (bTirets)        lineParam[iCur] = '-';
        else if (bWhite)    lineParam[iCur] = ' ';
        else                lineParam[iCur] = 'z';
      }
      else if (lineParam[iCur] == 'A' and !bSymboles)
      {// ' ' ! " # $ % & ' ( ) * + ' - . / 0>9 : ; < = > ? @ A>Z [ \ ] ^ _ ' a>z { | ] ~
        lineParam[iCur] = '9';
      }
      else if (lineParam[iCur] == '_' and !bSymboles)
      {// ' ' ! " # $ % & ' ( ) * + ' - . / 0>9 : ; < = > ? @ A>Z [ \ ] ^ _ ' a>z { | ] ~
        lineParam[iCur] = 'Z';
      }
      else if (lineParam[iCur] == 'a' and !bSymboles)
      {// ' ' ! " # $ % & ' ( ) * + ' - . / 0>9 : ; < = > ? @ A>Z [ \ ] ^ _ ' a>z { | ] ~
        if (bTirets)        lineParam[iCur] = '_';
        else                lineParam[iCur] = 'Z';
      }
      else
        lineParam[iCur]--;
      bTraite = true;
      break;
    }
  }
  if (!bTraite)
    // Calling the base class method
    CGenericModifier::ReceiveKey( iKey);
  else
  {
    GetString();
    bRedraw = true;
  }
}

//-------------------------------------------------------------------------
// Returns a parameter string
// The return chain looks like " Name Toto"
char *CCharModifier::GetString()
{
  if (!bModif and pFormat != NULL)
    sprintf( lineParam, pFormat[iLanguage], pParam);
  return lineParam;
}

//-------------------------------------------------------------------------
// Updating the parameter with the displayed string
void CCharModifier::SetParam()
{
  if (pFormat != NULL)
    strncpy( pParam, &(lineParam[iIdxMin]), iIdxMax-iIdxMin+1);
  pParam[iIdxMax-iIdxMin+1] = 0;
}

//-------------------------------------------------------------------------
// Management class of a integer type modifier
//-------------------------------------------------------------------------

  // Constructeur (initialisation des paramètres aux valeurs par défaut)
CModificatorInt::CModificatorInt(
  const char *pIndicModif,  // Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
  bool bLine,               // true to indicate a change on a line, false otherwise
  const char **pForm,       // Parameter format like " Min Freq %04dkHz"
  int *pPar,                // Pointer on the integer to modify
  int iMin,                 // Min value for integer to modify
  int iMax,                 // Max value for integer to modify
  int istep,                // Step to increase integer
  int ix,                   // X coordinate of the top left point of the modifier
  int iy                    // Y coordinate of the top left point of the modifier
  ):CGenericModifier( pIndicModif, bLine, false, pForm, ix, iy)
{
  // Initialization of the variables
  pParam = pPar;
  iTemp = *pParam;
  iMinimum = iMin;
  iMaximum = iMax;
  iStep = istep;
  // Test if sign presence
  bSigne = false;
  if (pFormat != NULL and strstr( pFormat[iLanguage], "%-") != NULL)
    bSigne = true;
}

//-------------------------------------------------------------------------
// Key Event Processing
void CModificatorInt::ReceiveKey(
  int iKey  // Touch to treat
  )
{
  bool bTraite = false;
  if (bModif)
  {
    int iCur = ilstSIdxEditable[iIdxModif];
    switch (iKey)
    {
    case K_UP:
      if (!bZones)
      {
        // Single field, Parameter increment
        iTemp += iStep;
        if (iTemp > iMaximum)
          iTemp = iMaximum;
      }
      else
      {
        // Several fields, test if sign field
        if (bSigne and iIdxModif == 0)
        {
          // Sign change
          if (lineParam[iCur] == '+')
            lineParam[iCur] = '-';
          else
            lineParam[iCur] = '+';
        }
        else
        {
          // Digit increment
          lineParam[iCur]+=1;
          if (lineParam[iCur] > '9')
            lineParam[iCur] = '0';
          else if (lineParam[iCur] < '0')
            lineParam[iCur] = '9';
        }
        sscanf( &lineParam[ilstSIdxEditable[0]], "%d", &iTemp);
        if (iTemp > iMaximum)
          iTemp = iMaximum;
        else if (iTemp < iMinimum)
          iTemp = iMinimum;
      }
      bTraite = true;
      break;
    case K_DOWN:
      if (!bZones)
      {
        // Single field, Decrement parameter
        iTemp -= iStep;
        if (iTemp < iMinimum)
          iTemp = iMinimum;
      }
      else
      {
        // Several fields, test if sign field
        if (bSigne and iIdxModif == 0)
        {
          // Sign change
          if (lineParam[iCur] == '+')
            lineParam[iCur] = '-';
          else
            lineParam[iCur] = '+';
        }
        else
        {
          // Decrement digit
          lineParam[iCur]-=1;
          if (lineParam[iCur] > '9')
            lineParam[iCur] = '0';
          else if (lineParam[iCur] < '0')
            lineParam[iCur] = '9';
        }
        sscanf( &lineParam[ilstSIdxEditable[0]], "%d", &iTemp);
        if (iTemp > iMaximum)
          iTemp = iMaximum;
        else if (iTemp < iMinimum)
          iTemp = iMinimum;
      }
      bTraite = true;
      break;
    }
  }
  if (!bTraite)
    // Calling the base class method
    CGenericModifier::ReceiveKey( iKey);
  else
  {
    GetString();
    bRedraw = true;
  }
}

//-------------------------------------------------------------------------
// Returns a parameter string
// The return chain looks like " Min Freq 120kHz"
char *CModificatorInt::GetString()
{
  if (!bModif)
  {
    iTemp = *pParam;
    if (pFormat != NULL)
      sprintf( lineParam, pFormat[iLanguage], iTemp);
  }
  else if (!bZones)
  {
    if (pFormat != NULL)
      sprintf( lineParam, pFormat[iLanguage], iTemp);
  }
  return lineParam;
}

//-------------------------------------------------------------------------
// Updating the parameter with the displayed string
void CModificatorInt::SetParam()
{
  if (iTemp > iMaximum)
    iTemp = iMaximum;
  else if (iTemp < iMinimum)
    iTemp = iMinimum;
  *pParam = iTemp;
}

//-------------------------------------------------------------------------
// Start changing the parameter
void CModificatorInt::StartModif()
{
  iTemp = *pParam;
  CGenericModifier::StartModif();
}

//-------------------------------------------------------------------------
// Initialize Min value for integer to modify
void CModificatorInt::SetMin( int iMin)
{
  iMinimum = iMin;
  if (iTemp < iMin)
    iTemp = iMin;
  bRedraw = true;
  GetString();
}

//-------------------------------------------------------------------------
// Initialize Max value for integer to modify
void CModificatorInt::SetMax( int iMax)
{
  iMaximum = iMax;
  if (iTemp < iMax)
    iTemp = iMax;
  bRedraw = true;
  GetString();
}

//-------------------------------------------------------------------------
// Management class of a float type modifier

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CFloatModifier::CFloatModifier(
  const char *pIndicModif,  // Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
  bool bLine,               // true to indicate a change on a line, false otherwise
  const char **pForm,       // Parameter format like " Min Freq %05.1fkHz"
  float *pPar,              // Pointer on the float to modify
  float fMin,               // Min value for float to modify
  float fMax,               // Max value for float to modify
  float fstep,              // Step to increase float
  int ix,                   // X coordinate of the top left point of the modifier
  int iy                    // Y coordinate of the top left point of the modifier
  ):CGenericModifier( pIndicModif, bLine, false, pForm, ix, iy)
{
  // Initialization of the variables
  pParam = pPar;
  fTemp = *pParam;
  fMinimum = fMin;
  fMaximum = fMax;
  fStep = fstep;
  // Test if sign presence
  bSigne = false;
  if (pFormat != NULL and strstr( pFormat[iLanguage], "%+") != NULL)
    bSigne = true;
}

//-------------------------------------------------------------------------
// Key Event Processing
void CFloatModifier::ReceiveKey(
  int iKey  // Touch to treat
  )
{
  bool bTraite = false;
  if (bModif)
  {
    int iCur = ilstSIdxEditable[iIdxModif];
    switch (iKey)
    {
    case K_UP:
      if (!bZones)
      {
        // Single field, Parameter increment
        fTemp += fStep;
        if (fTemp > fMaximum)
          fTemp = fMaximum;
      }
      else
      {
        // Several fields, test if sign field
        if (bSigne and iIdxModif == 0)
        {
          // Change of sign
          if (lineParam[iCur] == '+')
            lineParam[iCur] = '-';
          else
            lineParam[iCur] = '+';
        }
        else
        {
          // Digit increment
          lineParam[iCur]+=1;
          if (lineParam[iCur] > '9')
            lineParam[iCur] = '0';
          else if (lineParam[iCur] < '0')
            lineParam[iCur] = '9';
        }
        sscanf( &lineParam[ilstSIdxEditable[0]], "%f", &fTemp);
        if (fTemp > fMaximum)
          fTemp = fMaximum;
        else if (fTemp < fMinimum)
          fTemp = fMinimum;
        GetString();
      }
      bTraite = true;
      break;
    case K_DOWN:
      if (!bZones)
      {
        // Single field, Parameter decrement
        fTemp -= fStep;
        if (fTemp < fMinimum)
          fTemp = fMinimum;
      }
      else
      {
        // Several fields, test if sign field
        if (bSigne and iIdxModif == 0)
        {
          // Changement du signe
          if (lineParam[iCur] == '+')
            lineParam[iCur] = '-';
          else
            lineParam[iCur] = '+';
        }
        else
        {
          // Digit decrement
          lineParam[iCur]-=1;
          if (lineParam[iCur] > '9')
            lineParam[iCur] = '0';
          else if (lineParam[iCur] < '0')
            lineParam[iCur] = '9';
        }
        sscanf( &lineParam[ilstSIdxEditable[0]], "%f", &fTemp);
        if (fTemp > fMaximum)
          fTemp = fMaximum;
        else if (fTemp < fMinimum)
          fTemp = fMinimum;
        GetString();
      }
      bTraite = true;
      break;
    }
  }
  if (!bTraite)
    // Calling the base class method
    CGenericModifier::ReceiveKey( iKey);
  else
  {
    GetString();
    bRedraw = true;
  }
}

//-------------------------------------------------------------------------
// Returns a parameter string
// The return chain looks like " Min Freq 120.0kHz"
char *CFloatModifier::GetString()
{
  if (!bModif)
  {
    fTemp = *pParam;
    if (pFormat != NULL)
      sprintf( lineParam, pFormat[iLanguage], fTemp);
  }
  else if (!bZones)
  {
    if (pFormat != NULL)
      sprintf( lineParam, pFormat[iLanguage], fTemp);
  }
  return lineParam;
}

//-------------------------------------------------------------------------
// Updating the parameter with the displayed string
void CFloatModifier::SetParam()
{
  if (fTemp > fMaximum)
    fTemp = fMaximum;
  else if (fTemp < fMinimum)
    fTemp = fMinimum;
  *pParam = fTemp;
}

//-------------------------------------------------------------------------
// Start changing the parameter
void CFloatModifier::StartModif()
{
  fTemp = *pParam;
  CGenericModifier::StartModif();
}

//-------------------------------------------------------------------------
// Initialize Min value for float to modify
void CFloatModifier::SetMin( float fMin)
{
  fMinimum = fMin;
  if (fTemp < fMin)
    fTemp = fMin;
  bRedraw = true;
  GetString();
}

//-------------------------------------------------------------------------
// Initialize Min value for float to modify
void CFloatModifier::SetMax( float fMax)
{
  fMaximum = fMax;
  if (fTemp < fMax)
    fTemp = fMax;
  bRedraw = true;
  GetString();
}

//-------------------------------------------------------------------------
// Management class of a enum type modifier

//-------------------------------------------------------------------------
//  Constructor (initialization of parameters to default values)
CEnumModifier::CEnumModifier(
  const char *pIndicModif,  // Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
  bool bLine,               // true to indicate a change on a line, false otherwise
  bool bPush,               // true to indicate a push to change the value
  const char **pForm,       // Parameter format like " Min Freq %04dkHz"
  int *pPar,                // Pointer on the integer to modify
  int iMax,                 // Max value of the enum (always starts at 0)
  const char **pStrings,    // Pointer on an array of strings for different values of the enum
  int ix,                   // X coordinate of the top left point of the modifier
  int iy                    // Y coordinate of the top left point of the modifier
  ):CGenericModifier( pIndicModif, bLine, bPush, pForm, ix, iy)
{
  // Initialization of the variables
  pParam = pPar;
  iTemp = *pParam;
  iValMax = iMax;
  pTbStrings = pStrings;
  pTbVal = new bool[iValMax];
  for (int i=0; i<iValMax; i++)
	pTbVal[i] = true;
}

//-------------------------------------------------------------------------
//! \brief Destructor
CEnumModifier::~CEnumModifier()
{
  delete [] pTbVal;
}

//-------------------------------------------------------------------------
//! \brief To change array of strings for different values of the enum
//! \param pStrings Pointer on an array of strings for different values of the enum
void CEnumModifier::SetStringArray(
  const char **pStrings
  )
{
  pTbStrings = pStrings;
}

//-------------------------------------------------------------------------
// Start changing the parameter
void CEnumModifier::StartModif()
{
  if (bModifPush)
  {
    // We automatically go to the next valid value of the enumerated
	NextValue();
    SetParam();
    bRedraw = true;
    // End of the modification
    StopModif();
  }
  else
    // Call of the basic method
    CGenericModifier::StartModif();
}

//-------------------------------------------------------------------------
//Key Event Processing
void CEnumModifier::ReceiveKey(
  int iKey  // Touch to treat
  )
{
  bool bTraite = false;
  if (bModif)
  {
    switch (iKey)
    {
    case K_UP:
      // Parameter increment
	  NextValue();
      bTraite = true;
      break;
    case K_DOWN:
      // Parameter decrement
	  PrevValue();
      bTraite = true;
      break;
    }
  }
  if (!bTraite)
    // Call of the basic method
    CGenericModifier::ReceiveKey( iKey);
  else
  {
    GetString();
    bRedraw = true;
  }
}

//-------------------------------------------------------------------------
// Choose the next valid value
void CEnumModifier::NextValue()
{
  int iMemo = iTemp;
  do
  {
    iTemp++;
	if (iMemo == iTemp)
	  break;
    if (iTemp >= iValMax)
      iTemp = 0;
  } while (!pTbVal[iTemp]);
}

//-------------------------------------------------------------------------
// Choose the prev valid value
void CEnumModifier::PrevValue()
{
  int iMemo = iTemp;
  do
  {
    iTemp--;
	if (iMemo == iTemp)
	  break;
    if (iTemp < 0)
      iTemp = iValMax - 1;
  } while (!pTbVal[iTemp]);
}

//-------------------------------------------------------------------------
// Returns a parameter string
char *CEnumModifier::GetString()
{
  if (!bModif)
    iTemp = *pParam;
  if (iTemp < 0 or iTemp > iValMax)
    iTemp = 0;
  if (pFormat !=NULL)
    sprintf( lineParam, pFormat[iLanguage], pTbStrings[(iLanguage*iValMax)+iTemp]);
  return lineParam;
}

//-------------------------------------------------------------------------
// Updating the parameter with the displayed string
void CEnumModifier::SetParam()
{
  if (iTemp < 0 or iTemp > iValMax or !pTbVal[iTemp])
  {
    iTemp = 0;
	if (!pTbVal[iTemp])
	  NextValue();
  }
  *pParam = iTemp;
}

//-------------------------------------------------------------------------
// Initialize validity of an element (by default, all are valid)
// idx Index of the element
// bVal validity 
void CEnumModifier::SetElemValidity( int idx, bool bVal)
{
  if (idx >= 0 and idx < iValMax)
	pTbVal[idx] = bVal;
  if (iTemp == idx and !bVal)
    PrevValue();
}

//-------------------------------------------------------------------------
// Return the validity of an element
// idx Index of the element
// Return validity of the element
bool CEnumModifier::GetElemValidity( int idx)
{
  bool bReturn = false;
  if (idx >= 0 and idx < iValMax)
	bReturn = pTbVal[idx];
  return bReturn;
}

//-------------------------------------------------------------------------
// Management class of a enum type modifier mode in line

//-------------------------------------------------------------------------
//  Constructor (initialization of parameters to default values)
CLineEnumModifier::CLineEnumModifier(
  const char *pIndicModif,  // Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
  bool bLine,               // true to indicate a change on a line, false otherwise
  bool bPush,               // true to indicate a push to change the value
  const char **pForm,       // Parameter format like " Min Freq %04dkHz"
  int *pPar,                // Pointer on the integer to modify
  int iMax,                 // Max value of the enum (always starts at 0)
  const char **pStrings,    // Pointer on an array of strings for different values of the enum
  int ix,                   // X coordinate of the top left point of the modifier
  int iy                    // Y coordinate of the top left point of the modifier
  ):CEnumModifier( pIndicModif, bLine, bPush, pForm, pPar, iMax, pStrings, ix, iy)
{
}

//-------------------------------------------------------------------------
//! \brief Display of the parameter
//! \param x X position of the display
//! \param y Y position of the display
void CLineEnumModifier::PrintParam(
  int x,
  int y 
  )
{
  int cx = x;
  int iNbFirst=0;
  char temp[MAX_LINEPARAM+5];
  // First print left of format string "[&]" or "&"
  char *p = strchr(indicModif, '&');
  if (p != NULL and p>indicModif)
  {
    iNbFirst = p-indicModif;
    strncpy(temp, indicModif, iNbFirst);
	temp[iNbFirst] = 0;
    DrawString( temp, cx, y);
    //Serial.printf("PFirst (%s) cx %d, iNbFirst %d\n", temp, cx, iNbFirst);
    cx += iWidthCar * (strlen(temp) + 1);
  }
  // Print all valid values
  bool bSelValue = false;
  for (int i=0; i<iValMax; i++)
  {
    if (pTbVal[i])
    {
      if (i == iTemp)
        bSelValue = true;
      else
        bSelValue = false;
      DrawString( (char *)pTbStrings[(iLanguage*iValMax)+i], cx, y, bSelValue);
      //Serial.printf("Val %d (%s) cx %d\n", i, pTbStrings[(iLanguage*iValMax)+i], cx);
      cx += iWidthCar * (strlen(pTbStrings[i]) + 1);
    }
  }
  // End print right of format string
  if (p != NULL and p>indicModif)
  {
    int iNbLast = strlen(indicModif)-(iNbFirst+1);
    if (iNbLast > 0)
    {
      strncpy(temp, p+1, iNbLast);
	  temp[iNbLast] = 0;
      DrawString( temp, cx, y);
      //Serial.printf("PLast (%s) cx %d, iNbLast %d\n", temp, cx, iNbLast);
    }
  }
}

//-------------------------------------------------------------------------
// Management class of a boolean type modifier

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CBoolModifier::CBoolModifier(
  const char *pIndicModif,  // Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
  bool bLine,               // true to indicate a change on a line, false otherwise
  bool bPush,               // true to indicate a push to change the value
  const char **pForm,       // Parameter format like " Min Freq %04dkHz"
  bool *pPar,               // Pointer on the boolean to modify
  const char **pStrings,    // Pointer on an array of strings for the two values of the boolean
  int ix,                   // X coordinate of the top left point of the modifier
  int iy                    // Y coordinate of the top left point of the modifier
  ):CGenericModifier( pIndicModif, bLine, bPush, pForm, ix, iy)
{
  // Initialization of the variables
  pParam = pPar;
  iTemp = *pParam;
  pTbStrings = pStrings;
}

//-------------------------------------------------------------------------
// Start changing the parameter
void CBoolModifier::StartModif()
{
  if (bModifPush)
  {
    // We automatically reverse the value of the Boolean
    if (iTemp)
      iTemp = false;
    else
      iTemp = true;
    SetParam();
    bRedraw = true;
    // End of the modification
    StopModif();
  }
  else
    // Call of the basic method
    CGenericModifier::StartModif();
}

//-------------------------------------------------------------------------
// Key Event Processing
void CBoolModifier::ReceiveKey(
  int iKey  // Touch to treat
  )
{
  bool bTraite = false;
  // Si paramétre en modif
  if (bModif)
  {
    switch (iKey)
    {
    case K_UP:
    case K_DOWN:
      // Reverse the value of the Boolean
      if (iTemp)
        iTemp = false;
      else
        iTemp = true;
      bTraite = true;
      break;
    }
  }
  if (!bTraite)
    // Call of the basic method
    CGenericModifier::ReceiveKey( iKey);
  else
  {
    GetString();
    bRedraw = true;
  }
}

//-------------------------------------------------------------------------
// Returns a parameter string
// The return chain looks like " Filter Yes"
char *CBoolModifier::GetString()
{
  if (!bModif)
    iTemp = *pParam;
  char *pBool = (char *)&iTemp;
  if (*pBool < 0 or *pBool > 1)
    iTemp = false;
  if (pFormat != NULL)
  {
    if (iTemp)
      sprintf( lineParam, pFormat[iLanguage], pTbStrings[(iLanguage*2)+1]);
    else
      sprintf( lineParam, pFormat[iLanguage], pTbStrings[(iLanguage*2)]);
  }
  return lineParam;
}

//-------------------------------------------------------------------------
// Updating the parameter with the displayed string
void CBoolModifier::SetParam()
{
  char *pBool = (char *)&iTemp;
  if (*pBool < 0 or *pBool > 1)
    iTemp = false;
  *pParam = iTemp;
}

//-------------------------------------------------------------------------
// Management class of a push button type modifier

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CPushButtonModifier::CPushButtonModifier(
  const char *pIndicModif,  // Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
  bool bLine,               // true to indicate a change on a line, false otherwise
  const char **pForm,       // Parameter format like " Min Freq %04dkHz"
  int ix,                   // X coordinate of the top left point of the modifier
  int iy                    // Y coordinate of the top left point of the modifier
  ) : CGenericModifier( pIndicModif, bLine, true, pForm, ix, iy)
{
  bPush = false;
  strcpy( lineParam, " ");
  pString = NULL;
}

//-------------------------------------------------------------------------
// Start changing the parameter
void CPushButtonModifier::StartModif()
{
  //Serial.println("CPushButtonModifier::StartModif");
  // Save action
  bPush = true;
  // End change
  StopModif();
}

//-------------------------------------------------------------------------
// Returns a parameter string
// The return chain looks like " Filter Yes"
char *CPushButtonModifier::GetString()
{
  if (!bModif and pString != NULL)
    strcpy( lineParam, pString);
  else if (!bModif and pFormat != NULL)
    strcpy( lineParam, pFormat[iLanguage]);
  else
    strcpy( lineParam, " ");
  return lineParam;
}

//-------------------------------------------------------------------------
// Returns true if the button has been enabled and returns the flag to false
bool CPushButtonModifier::GetbPush()
{
  bool bReturn = bPush;
  bPush = false;
  return bReturn;
}

//-------------------------------------------------------------------------
// Management class of a date type modifier

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CDateModifier::CDateModifier(
  const char *pIndicModif,  // Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
  bool bLine,               // true to indicate a change on a line, false otherwise
  const char **pForm,       // Parameter format like " Min Freq %04dkHz"
  int ix,                   // X coordinate of the top left point of the modifier
  int iy                    // Y coordinate of the top left point of the modifier
  ):CGenericModifier( pIndicModif, bLine, false, pForm, ix, iy)
{
}

//-------------------------------------------------------------------------
// Key Event Processing
void CDateModifier::ReceiveKey(
  int iKey  // Touch to treat
  )
{
  if (bModif)
  {
    // Init of the min and max of the digit and the pointer on the value to modify
    int iMin, iMax;
    int *pValue;
    switch (iIdxModif)
    {
    case 0:
      iMin = 1;
      iMax = GetMaxMonth(iNewM, iNewA);
      pValue = &iNewJ;
      break;
    case 1:
      iMin = 1;
      iMax = 12;
      pValue = &iNewM;
      break;
    case 2:
    default:
      iMin = 18;
      iMax = 99;
      pValue = &iNewA;
      break;
    }
    switch (iKey)
    {
    case K_UP:
      // We increment the digit
      (*pValue)++;
      if (*pValue > iMax)
        *pValue = iMin;
      break;
    case K_DOWN:
      // We decrement the digit
      (*pValue)--;
      if (*pValue < iMin)
        *pValue = iMax;
      break;
    default:
      CGenericModifier::ReceiveKey(iKey);
    }
    GetString();
    bRedraw = true;
  }
  else
    CGenericModifier::ReceiveKey(iKey);
}

//-------------------------------------------------------------------------
// Returns a parameter string
char *CDateModifier::GetString()
{
  char temp[MAX_LINEPARAM];
  if (!bModif)
  {
#if defined(__IMXRT1062__) // Teensy 4.1
    sprintf( temp, "%02d/%02d/%02d", day(), month(), year()-2000);
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
    sprintf( temp, "%02d/%02d/%02d", day(), month(), year()-2000);
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
    time_t curtime;
    struct tm * timeinfo;
    time(&curtime);
    timeinfo = localtime (&curtime);
    strftime(temp, 20, "%d/%m/%y", timeinfo);
#endif
    sprintf( lineParam, pFormat[iLanguage], temp);
  }
  else
  {
    sprintf(temp, "%02d/%02d/%02d", iNewJ, iNewM, iNewA);
    sprintf( lineParam, pFormat[iLanguage], temp);
  }
  return lineParam;
}

//-------------------------------------------------------------------------
// Start changing the parameter
void CDateModifier::StartModif()
{
  // Call of the basic method
  CGenericModifier::StartModif();
  // Save current value
#if defined(__IMXRT1062__) // Teensy 4.1
  iNewJ = day();
  iNewM = month();
  iNewA = year() - 2000;
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  iNewJ = day();
  iNewM = month();
  iNewA = year() - 2000;
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
  char temp[MAX_LINEPARAM];
  time_t curtime;
  struct tm * timeinfo;
  time(&curtime);
  timeinfo = localtime (&curtime);
  strftime(temp, MAX_LINEPARAM, "%d/%m/%y", timeinfo);
  sscanf(temp, "%d/%d/%d", &iNewJ, &iNewM, &iNewA);
#endif
}

//-------------------------------------------------------------------------
// Updating the parameter with the displayed string
void CDateModifier::SetParam()
{
  if (bModif)
  {
    // Verification of the parameter
    bool bOK = true;
    if (iNewJ < 1 or iNewJ > GetMaxMonth(iNewM, iNewA))
      bOK = false;
    else if (iNewM < 1 or iNewM > 12)
      bOK = false;
    else if (iNewA < 18)
      bOK = false;
    if (bOK)
    {
#if defined(__IMXRT1062__) // Teensy 4.1
      // Update to the system date
      setTime( hour(), minute(), second(), iNewJ, iNewM, iNewA);
      // Set internal Teensy 3.6 clock
      Teensy3Clock.set(now());
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
      // Update to the system date
      setTime( hour(), minute(), second(), iNewJ, iNewM, iNewA);
      // Set internal Teensy 3.6 clock
      Teensy3Clock.set(now());
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
      // Update to the system date
      struct tm start, *curtm;
      time_t curtime;
      time(&curtime);
      curtm = localtime (&curtime);
      start.tm_year = (iNewA + 2000) - 1900;
      start.tm_mon  = iNewM-1;
      start.tm_mday = iNewJ;
      start.tm_hour = curtm->tm_hour;
      start.tm_min  = curtm->tm_min;
      start.tm_sec  = curtm->tm_sec;
      time_t t = mktime(&start);
      timeval now = { t, 0 };
      settimeofday(&now, NULL);
#endif
    }
    bModif = false;
  }
}

//-------------------------------------------------------------------------
// Gives the max of one month depending on the year
int CDateModifier::GetMaxMonth( int iMois, int Annee)
{
  int iMax = 31;
  if (iMois == 2)
  {
    if (IsBissextile(Annee))
      iMax = 29;
    else
      iMax = 28;
  }
  else if (iMois == 4 or iMois == 6 or iMois == 9 or iMois == 11)
    iMax = 30;
  else
    iMax = 31;
  return iMax;
}

//-------------------------------------------------------------------------
// Indicates if a year is a bissextile
bool CDateModifier::IsBissextile( int Annee)
{
    // 1 - If the year is divisible by 4, go to step 2, otherwise go to step 5.
    // 2 - If the year is divisible by 100, go to step 3. Otherwise, go to step 4.
    // 3 - If the year is divisible by 400, proceed to step 4. Otherwise, go to step 5.
    // 4 - The year is a leap year (it has 366 days).
    // 5 - The year is not a leap year (it has 365 days).
  bool bissextile = false;
  if ((Annee % 4) == 0)
  {
    if ((Annee % 100) == 0)
    {
      if ((Annee % 400) == 0)
        bissextile = true;
      else
        bissextile = false;
    }
    else
      bissextile = true;
  }
  else
    bissextile = false;
  return bissextile;
}

//-------------------------------------------------------------------------
// Management class of a hour type modifier

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CHourModifier::CHourModifier(
  const char *pIndicModif,  // Pointer on the string indicating editable fields like " Min Freq &$$$". & indicates first digit of a field, $ other digit of a field
  bool bLine,               // true to indicate a change on a line, false otherwise
  const char **pForm,       // Parameter format like " Min Freq %04dkHz"
  char *pStrH,              // Possible time string (hh: mm or hh: mm: ss) to modify (current time if NULL)
  int ix,                   // X coordinate of the top left point of the modifier
  int iy                    // Y coordinate of the top left point of the modifier
  ):CGenericModifier( pIndicModif, bLine, false, pForm, ix, iy)
{
  pStrHeure = pStrH;
}

//-------------------------------------------------------------------------
// Key Event Processing
void CHourModifier::ReceiveKey(
  int iKey  // Touch to treat
  )
{
  if (bModif)
  {
    // Init of the min and max of the digit and the pointer on the value to modify
    int iMin, iMax;
    int *pValue;
    switch (iIdxModif)
    {
    case 0:
      iMin = 0;
      iMax = 23;
      pValue = &iNewH;
      break;
    case 1:
      iMin = 0;
      iMax = 59;
      pValue = &iNewM;
      break;
    case 2:
    default:
      iMin = 0;
      iMax = 59;
      pValue = &iNewS;
      break;
    }
    switch (iKey)
    {
    case K_UP:
      // We increment the digit
      (*pValue)++;
      if (*pValue > iMax)
        *pValue = iMin;
      break;
    case K_DOWN:
      // We decrement the digit
      (*pValue)--;
      if (*pValue < iMin)
        *pValue = iMax;
      break;
    default:
      CGenericModifier::ReceiveKey(iKey);
    }
    GetString();
    bRedraw = true;
  }
  else
  {
    if (pStrHeure == NULL)
      // Current time always to redisplay
      bRedraw = true;
    CGenericModifier::ReceiveKey(iKey);
  }
}

//-------------------------------------------------------------------------
// Start changing the parameter
void CHourModifier::StartModif()
{
  // Call of the basic method
  CGenericModifier::StartModif();
  // Save current value
  if (pStrHeure == NULL)
  {
#if defined(__IMXRT1062__) // Teensy 4.1
    iNewH = hour();
    iNewM = minute();
    iNewS = second();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
    iNewH = hour();
    iNewM = minute();
    iNewS = second();
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
    char temp[22];
    time_t curtime;
    struct tm * timeinfo;
    time(&curtime);
    timeinfo = localtime (&curtime);
    strftime(temp, 20, "%H:%M:%S", timeinfo);
    sscanf(temp, "%d:%d:%d", &iNewH, &iNewM, &iNewS);
#endif
  }
  else
  {
    iNewS = 0;
    if (strlen(pStrHeure) > 5)
      sscanf(pStrHeure, "%d:%d:%d", &iNewH, &iNewM, &iNewS);
    else
      sscanf(pStrHeure, "%d:%d", &iNewH, &iNewM);
  }
}

//-------------------------------------------------------------------------
// Returns a parameter string
char *CHourModifier::GetString()
{
  char temp[MAX_LINEPARAM];
  if (!bModif)
  {
    if (pStrHeure == NULL)
    {
#if defined(__IMXRT1062__) // Teensy 4.1
	  setTime(Teensy3Clock.get());
      sprintf( temp, "%02d:%02d:%02d", hour(), minute(), second());
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
	  setTime(Teensy3Clock.get());
      sprintf( temp, "%02d:%02d:%02d", hour(), minute(), second());
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
      time_t curtime;
      struct tm * timeinfo;
      time(&curtime);
      timeinfo = localtime (&curtime);
      strftime(temp, 20, "%H:%M:%S", timeinfo);
#endif
      sprintf( lineParam, pFormat[iLanguage], temp);
    }
    else
      sprintf( lineParam, pFormat[iLanguage], pStrHeure);
  }
  else
  {
    if (pStrHeure == NULL or strlen(pStrHeure) > 5)
      sprintf( temp, "%02d:%02d:%02d", iNewH, iNewM, iNewS);
    else
      sprintf( temp, "%02d:%02d", iNewH, iNewM);
    sprintf( lineParam, pFormat[iLanguage], temp);
  }
  return lineParam;
}

//-------------------------------------------------------------------------
// Updating the parameter with the displayed string
void CHourModifier::SetParam()
{
  // Vérification du paramètre
  bool bOK = true;
  if (iNewH < 0 or iNewH > 23)
    bOK = false;
  else if (iNewM < 0 or iNewM > 59)
    bOK = false;
  else if (iNewS < 0 or iNewS > 59)
    bOK = false;
  if (bOK)
  {
    if (pStrHeure == NULL)
    {
#if defined(__IMXRT1062__) // Teensy 4.1
      // Update to the system date
      setTime( iNewH, iNewM, iNewS, day(), month(), year());
      // Set internal Teensy 3.6 clock
      Teensy3Clock.set(now());
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
      // Update to the system date
      setTime( iNewH, iNewM, iNewS, day(), month(), year());
      // Set internal Teensy 3.6 clock
      Teensy3Clock.set(now());
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
      // Update to the system date
      struct tm start, *curtm;
      time_t curtime;
      time(&curtime);
      curtm = localtime (&curtime);
      start.tm_year = curtm->tm_year;
      start.tm_mon  = curtm->tm_mon;
      start.tm_mday = curtm->tm_mday;
      start.tm_hour = iNewH;
      start.tm_min  = iNewM;
      start.tm_sec  = iNewS;
      time_t t = mktime(&start);
      //printf("Setting time: %s", asctime(&tm));
      timeval now = { t, 0 };
      settimeofday(&now, NULL);
#endif
    }
    else
    {
      if (strlen(pStrHeure) > 5)
        sprintf( pStrHeure, "%02d:%02d:%02d", iNewH, iNewM, iNewS);
      else
        sprintf( pStrHeure, "%02d:%02d", iNewH, iNewM);
    }
  }
  bModif = false;
}

//-------------------------------------------------------------------------
// Management class of an SD card type modifier
// Displays the remaining size on the SD card
// On modification, displays information on the SD card and proposes to erase wav files present on the card

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CSDModifier::CSDModifier(
  const char *pIndicModif,  // Pointer on the string indicating editable fields like " Min Freq &$$$". If no field, no delete files
  bool bLine,               // true to indicate a change on a line, false otherwise
  const char **pForm,       // Parameter format like " Min Freq %04dkHz"
  const char *ext,          // Extension files to delete ("wav" for exemple, "*" for all)
  int ix,                   // X coordinate of the top left point of the modifier
  int iy                    // Y coordinate of the top left point of the modifier
  ):CGenericModifier( pIndicModif, bLine, false, pForm, ix, iy)
{
  strcpy( sExtFile, ext);
  // No default for erasing wav files
  cEff = 'N';
  // By default, SD info is not filled
  iNbFiles = -1;
  bCalcul = true;
}

//-------------------------------------------------------------------------
// Start changing the parameter
void CSDModifier::StartModif()
{
  // Test if it is possible to delete files
  if (iNbZones > 0)
    CGenericModifier::StartModif();
  GetString();
}

//-------------------------------------------------------------------------
// Key Event Processing
void CSDModifier::ReceiveKey(
  int iKey  // Touch to treat
  )
{
  if (bModif)
  {
    switch (iKey)
    {
    case K_UP:
    case K_DOWN:
      // Change char Y/N
      if (cEff == 'N')
      {
        if (iLanguage == LENGLISH)
          cEff = 'Y';
        else
          cEff = 'O';
      }
      else
        cEff = 'N';
      break;
    default:
      CGenericModifier::ReceiveKey(iKey);
    }
    GetString();
    bRedraw = true;
  }
  else
    CGenericModifier::ReceiveKey(iKey);
}

//-------------------------------------------------------------------------
// Returns a parameter string
char *CSDModifier::GetString()
{
  if (bCalcul)
  {
    fFreeSizeSD = GetSDFreeSpace();
    iNbFiles = GetSDNbFiles();
    bCalcul = false;
  }
  if (!bModif)
    sprintf( lineParam, pFormat[iLanguage], fFreeSizeSD, iFreeSizeSD, iNbFiles);
  else
  {
    if (iLanguage == LENGLISH)
      sprintf( lineParam, " Del      %04d%s %c ", iNbFiles, sExtFile, cEff);
    else
      sprintf( lineParam, " Effacem. %04d%s %c ", iNbFiles, sExtFile, cEff);
  }
  return lineParam;
}

//-------------------------------------------------------------------------
// Updating the parameter with the displayed string
void CSDModifier::SetParam()
{
  if (cEff == 'O' or cEff == 'Y')
  {
    // Del wav files
    char name[80];
#ifdef MMC_SDFAT
    SdFile file;
    sd.vwd()->rewind(); 
    while (file.openNext(sd.vwd(), O_READ))
#endif
#ifdef MMC_SDFAT_EXT
    SdFile file;
    sd.vwd()->rewind(); 
    while (file.openNext(sd.vwd(), O_READ))
#endif
#ifdef SD_FAT_TYPE
    SdFile file;
    SdFile root;
    root.open("/");
    while (file.openNext(&root, O_RDONLY))
#endif
#ifdef MMC_SDFS
    FsFile file;
    FsFile dir;
    dir.open("/");
    while (file.openNext(&dir, O_READ))
#endif
    {
      if (!file.isDir())
      {
        // Test if wav file
        file.getName( name, 80);
        char *p = strrchr(name, '.');
        if (p != NULL and (strstr(p, sExtFile) != NULL or strstr(sExtFile, "*") != NULL))
          sd.remove( name);
      }
      file.close();
    }
    // Recalculate the size of the SD card
    fFreeSizeSD = GetSDFreeSpace();
    iNbFiles = GetSDNbFiles();
  }
  bModif = false;
}

//-------------------------------------------------------------------------
// Returns the free size of the SD card in GO
float CSDModifier::GetSDFreeSpace()
{
  // Calculate free space (volume free clusters * blocks per clusters / 2)
#ifdef MMC_SDFAT
  long lFreeKB  = sd.vol()->freeClusterCount();
  long lTotalKB = sd.vol()->clusterCount();
  iFreeSizeSD = lFreeKB * 100 / lTotalKB;
  lFreeKB *= sd.vol()->blocksPerCluster()/2;
#endif
#ifdef MMC_SDFAT_EXT
  long lFreeKB  = sd.vol()->freeClusterCount();
  long lTotalKB = sd.vol()->clusterCount();
  iFreeSizeSD = lFreeKB * 100 / lTotalKB;
  lFreeKB *= sd.vol()->blocksPerCluster()/2;
#endif
#ifdef SD_FAT_TYPE
  long lFreeKB  = sd.vol()->freeClusterCount();
  long lTotalKB = sd.vol()->clusterCount();
  iFreeSizeSD = lFreeKB * 100 / lTotalKB;
  lFreeKB *= sd.vol()->sectorsPerCluster()/2;
#endif
#ifdef MMC_SDFS
  long lFreeKB  = sd.freeClusterCount();
  long lTotalKB = sd.clusterCount();
  iFreeSizeSD = lFreeKB * 100 / lTotalKB;
  lFreeKB *= sd.sectorsPerCluster()/2;
#endif
  float fFree = lFreeKB / 1024.0;
  fFree = fFree / 1024.0;
  return fFree;
}

//-------------------------------------------------------------------------
//  Returns the number of files matching with sExtFiles on the SD card
// CAUTION only read the root, not the subdirectories
uint16_t CSDModifier::GetSDNbFiles()
{
  char name[80];
  uint16_t uiNbFiles = 0;
#ifdef MMC_SDFAT
  SdFile file;
  sd.vwd()->rewind(); 
  while (file.openNext(sd.vwd(), O_READ))
#endif
#ifdef MMC_SDFAT_EXT
  SdFile file;
  sd.vwd()->rewind(); 
  while (file.openNext(sd.vwd(), O_READ))
#endif
#ifdef SD_FAT_TYPE
  SdFile file;
  SdFile root;
  root.open("/");
  while (file.openNext(&root, O_RDONLY))
#endif
#ifdef MMC_SDFS
  FsFile file;
  FsFile dir;
  dir.open("/");
  while (file.openNext(&dir, O_READ))
#endif
  {
    if (!file.isDir())
    {
      // Test if good file
      file.getName( name, 80);
      char *p = strrchr(name, '.');
      if (p != NULL and (strstr(p, sExtFile) != NULL or strstr(sExtFile, "*") != NULL))
        uiNbFiles++;
    }
    file.close();
  }
  return uiNbFiles;
}

// Free size of the SD card in GO
float CSDModifier::fFreeSizeSD = 0.0;

// Free size of the SD card as a percentage
int CSDModifier::iFreeSizeSD = 0;

//-------------------------------------------------------------------------
// Management class of an Break/Play/Rec button type modifier

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
// Never in line
CPlayRecModifier::CPlayRecModifier(
  int *pPar,                // Pointer on the parameter to modify
  bool bPlay,               // Indicates if the button can take the PLAY aspect
  bool bRec,                // Indicates if the button can take the REC  aspect
  int iW,                   // Button width in pixel
  int iH,                   // Button height in pixel
  int ix,                   // X coordinate of the top left point of the modifier
  int iy                    // Y coordinate of the top left point of the modifier
  ) : CGenericModifier( NULL, false, false, NULL, ix, iy)
{
  pParam   = pPar;
  bPlayer  = bPlay;
  bRecord  = bRec;
  iWidth   = iW;
  iHeigth  = iH;
}

//-------------------------------------------------------------------------
// Returns a parameter string
char *CPlayRecModifier::GetString()
{
  if (!bModif and pFormat != NULL)
    strcpy( lineParam, pFormat[iLanguage]);
  return lineParam;
}

//-------------------------------------------------------------------------
// Start changing the parameter
void CPlayRecModifier::StartModif()
{
  // change button status
  if (*pParam == PBREAK)
  {
    if (bPlayer)
      *pParam = PPLAY;
    else if (bRecord)
      *pParam = PREC;
  }
  else
    *pParam = PBREAK;
  // End change
  StopModif();
}

//-------------------------------------------------------------------------
// Display of the parameter
void CPlayRecModifier::PrintParam(
  int x,    // X position of the display
  int y     // Y position of the display
  )
{
  // Display of [] delimitation of the button
  DrawString( (char *)"[", x, y, bSelect);
  DrawString( (char *)"]", x + iWidth - iWidthCar, y, bSelect);
  if (*pParam == PPLAY)
  {
    // Display Play graphic (triangle)
    int cx1 = x + iWidthCar + 2, cx2 = cx1 + iWidth - 2*iWidthCar - 4;
    int cy1 = y, cy2 = cy1 + (iHeigth/2), cy3 = cy1 + iHeigth;
    pDisplay->drawTriangle( cx1, cy1, cx2, cy2, cx1, cy3);
  }
  else if (*pParam == PREC)
  {
    // Display Record graphic (circle)
    // Calculate radus
    int iR = (iWidth / 2) - 1;
    if (iHeigth < iWidth)
      iR = (iHeigth / 2) - 1;
    // Calculate center
    int cx1 = x + (iWidth  / 2) - 1;
    int cy1 = y + (iHeigth / 2) + 1;
    pDisplay->drawDisc( cx1, cy1, iR);
  }
  else
  {
    // Display Break graphic (two vertical bars)
    int iW = ((iWidth - 2*iWidthCar) / 2) - 3;
    if (iW < 1)
      iW = 1;
    int cx1 = x + iWidthCar + iW + 4;
    pDisplay->drawRBox(  x+iWidthCar + 2 , y+1, iW, iHeigth, 0);
    pDisplay->drawRBox( cx1, y+1, iW, iHeigth, 0);
  }
}

//-------------------------------------------------------------------------
// Management class of a battery type modifier (0 to 100%)

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
// Never in line
CBatteryModifier::CBatteryModifier(
  int *pPar,                // Pointer on the parameter to modify
  int iW,                   // Button width in pixel
  int iH,                   // Button height in pixel
  int ix,                   // X coordinate of the top left point of the modifier
  int iy                    // Y coordinate of the top left point of the modifier
  ) : CGenericModifier( NULL, false, false, NULL, ix, iy)
{
  pParam = pPar;
  iWidth = iW;
  iHeigth = iH;
}

//-------------------------------------------------------------------------
//  Display of the parameter
void CBatteryModifier::PrintParam(
  int x,    // X position of the display
  int y     // Y position of the display
  )
{
  // Print the battery box
  pDisplay->drawRFrame( x+2, y, iWidth-2, iHeigth, 0);
  pDisplay->drawLine( x, y+2, x, y+iHeigth-4);
  pDisplay->drawLine( x+1, y+2, x+1, y+iHeigth-4);
  // Calculation of the position of the load
  int iVal = *pParam;
  if (iVal > 100)
    iVal = 100;
  int w = iVal * (iWidth-5) / 100;
  //Serial.printf("CBatteryModifier::PrintParam *pParam %d, iVal %d, iWidth %d, w %d, x %d\n", *pParam, iVal, iWidth, w, x);
  // Showing the significant area
  pDisplay->drawRBox( x+iWidth-1-w, y+1, w, iHeigth-2, 0);
  // Showing the non significant area
  pDisplay->setDrawColor( 0);
  pDisplay->drawRBox( x+3, y+1, iWidth-w-4, iHeigth-2, 0);
  pDisplay->setDrawColor( 1);
}

//-------------------------------------------------------------------------
// Management class of a bargraph type modifier from 0 to 100%

const char *txtIntB[]       = {"%d",
                               "%d"};

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
// Never in line
CDrawBarModifier::CDrawBarModifier(
  int *pPar,                // Pointer on the parameter to modify
  int iW,                   // Box width in pixel
  int iH,                   // Box height in pixel
  int ix,                   // X coordinate of the top left point of the modifier
  int iy,                   // Y coordinate of the top left point of the modifier
  int iMin,                 // Min value (0 by default)
  int iMax                  // Max value (100 by default)
  ) : CModificatorInt( "&$", false, txtIntB, pPar, iMin, iMax, 1, ix, iy)
{
  iWidth = iW;
  iHeigth = iH;
}

//-------------------------------------------------------------------------
// Display of the parameter
void CDrawBarModifier::PrintParam(
  int x,    // X position of the display
  int y     // Y position of the display
  )
{
  float fStep = (float)iWidth / (float)(iMaximum - iMinimum);
  float fV = fStep * (float)(*pParam - iMinimum);
  if (fV > iWidth)
    fV = iWidth;
  // Print outline
  pDisplay->drawRFrame( x, y, iWidth, iHeigth, 0);
  if (fV > 0.0)
    // Showing the significant area
    pDisplay->drawRBox( x, y, (int)fV, iHeigth, 0);
}

//-------------------------------------------------------------------------
// Management class of a Boolean type modifier for brightness
// (normal-false / weak-true) Can not be displayed in line mode

// Chains of the 2 values
extern const char *txtLum[MAXLANGUAGE][2];
const char *txtStrLum[]  = {"[%s]",
                            "[%s]",
                            "[%s]",
                            "[%s]"};
//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
// Never in line
CModificatorLight::CModificatorLight(
  bool *pPar,               // Pointer on the parameter to modify
  int ix,                   // X coordinate of the top left point of the modifier
  int iy                    // Y coordinate of the top left point of the modifier
  ):CBoolModifier( "[&]", false, true, txtStrLum, pPar, (const char **)txtLum, ix, iy)
{
}

//-------------------------------------------------------------------------
// SStart changing the parameter
void CModificatorLight::StartModif()
{
  // Change status button
  if (*pParam)
    *pParam = false;
  else
    *pParam = true;
  if (*pParam)
    pDisplay->setContrast(1);
  else
    pDisplay->setContrast(255);
  // End changing
  StopModif();
}

//-------------------------------------------------------------------------
//! \class CSecondModifier
//! \brief Management class of a seconds modifier in time mode (hh:mm:ss)

//-------------------------------------------------------------------------
//! \brief Constructor (initialization of parameters to default values)
//! \param TypeSecModif type of seconds modification (enum TYPES_SECONDMODIFIER)
//! \param bLine true to indicate a change on a line, false otherwise
//! \param pForm Parameter format like " Duration %s"
//! \param pPar Pointer on the integer to modify
//! \param iMin Min value
//! \param iMax Max value for integer to modify, maximum allowed value 86400 (23:59:59)
//! \param ix X coordinate of the top left point of the modifier
//! \param iy Y coordinate of the top left point of the modifier
CSecondModifier::CSecondModifier(
  int TypeSecModif,
  bool bLine,
  const char **pForm,
  int *pPar,
  int iMin,
  int iMax,               
  int ix,                 
  int iy                  
  ) : CModificatorInt( NULL, bLine, pForm, pPar, iMin, iMax, 1, ix, iy)
{
  // Initialization of the variables
  iTypeSecModif = TypeSecModif;
  switch (iTypeSecModif)
  {
  case SECOND_HMS: strcpy( strTime, "&$:&$:&$"); break;
  case SECOND_HM : strcpy( strTime, "&$:&$");    break;
  case SECOND_MS : strcpy( strTime, "&$:&$");    break;
  }
  sprintf( indicModif, pFormat[iLanguage], strTime);
  FindBeginEndModif();
  GetString();
}

//-------------------------------------------------------------------------
//! \brief Key Event Processing
void CSecondModifier::ReceiveKey(
  int iKey  // Touch to treat
  )
{
  if (bModif)
  {
	int iPlus = 0;
	int *pDigit = NULL;
	int iPlusDigit = 0;
    switch (iIdxModif)
    {
    case 0:
      switch (iTypeSecModif)
      {
      default:
      case SECOND_HMS: iPlus = 3600; break;
      case SECOND_HM : iPlus = 3600; break;
      case SECOND_MS : iPlus =   60; break;
      }
      break;
    case 1:
      switch (iTypeSecModif)
      {
      default:
      case SECOND_HMS: iPlus =   60; pDigit = &iM; iPlusDigit = 59*60; break;
      case SECOND_HM : iPlus =   60; pDigit = &iM; iPlusDigit = 59*60; break;
      case SECOND_MS : iPlus =    1; pDigit = &iS; iPlusDigit =    59; break;
      }
      break;
    case 2:
    default:
      switch (iTypeSecModif)
      {
      default:
      case SECOND_HMS: iPlus =     1; pDigit = &iS; iPlusDigit = 59; break;
      case SECOND_HM : iPlus =     0; break;
      case SECOND_MS : iPlus =     0; break;
      }
      break;
    }
    switch (iKey)
    {
    case K_UP:
      // We increment the parameter
	  if (pDigit != NULL and *pDigit == 59)
		iTemp -= iPlusDigit;
	  else
        iTemp += iPlus;
      if (iTemp > iMaximum)
        iTemp = iMaximum;
      //Serial.printf("K_UP Val %d\n", *pValue);
      break;
    case K_DOWN:
      // We decrement the digit
	  if (pDigit != NULL and *pDigit == 0)
		iTemp += iPlusDigit;
	  else
		iTemp -= iPlus;
      if (iTemp < iMinimum)
        iTemp = iMinimum;
      //Serial.printf("K_DOWN Val %d\n", *pValue);
      break;
    default:
      CGenericModifier::ReceiveKey(iKey);
    }
    GetString();
    bRedraw = true;
  }
}

//-------------------------------------------------------------------------
//! \brief Returns a parameter string
//! The return chain looks like " Min Freq 120kHz"
char *CSecondModifier::GetString()
{
  if (!bModif)
  {
    iH = *pParam / 3600;
    iM = (*pParam - (iH * 3600)) / 60;
    iS = *pParam - (iH * 3600) - (iM * 60);
  }
  else
  {
    iH = iTemp / 3600;
    iM = (iTemp - (iH * 3600)) / 60;
    iS = iTemp - (iH * 3600) - (iM * 60);
  }
  switch (iTypeSecModif)
  {
  case SECOND_HMS: sprintf( strTime, "%02d:%02d:%02d", iH, iM, iS); break;
  case SECOND_HM : sprintf( strTime, "%02d:%02d"     , iH, iM);     break;
  case SECOND_MS : sprintf( strTime, "%02d:%02d"     , iM, iS);     break;
  }
  sprintf( lineParam, pFormat[iLanguage], strTime);
  //Serial.printf("GetString strTime [%s], lineParam [%s]\n", strTime, lineParam);
  return lineParam;
}

//-------------------------------------------------------------------------
//! \brief Updating the parameter with the displayed string
void CSecondModifier::SetParam()
{
  // Vérification du paramètre
  *pParam = iTemp;
  if (*pParam < iMinimum)
    *pParam = iMinimum;
  else if (*pParam > iMaximum)
    *pParam = iMaximum;
  GetString();
  bModif = false;
}

//-------------------------------------------------------------------------
//! \brief Start changing the parameter
void CSecondModifier::StartModif()
{
  CModificatorInt::StartModif();
}

