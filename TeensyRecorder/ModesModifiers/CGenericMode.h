//-------------------------------------------------------------------------
//! \file CGenericMode.h
//! \brief Generic class for managing a mode of operation
//! \details class CGenericMode, CModeError and LogFile
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 /*
  * - CGenericMode
  * -   CModeError
  * - LogFile
  */
#include <stdio.h>
#include <string.h>
#include <vector>
#include "CKeyManager.h"
#include "ModesModifiers.h"

#ifndef CGENERICMODE_H
#define CGENERICMODE_H

#define NOMODE 0 //!< Value for nomode of operation

//-------------------------------------------------------------------------
// Function to create or activate a new mode
// In dynamic mode, need to first delete the previous mode and then create the new mode
// In static mode, need just to sawp the pointer of the current mode
// User need to define this function. Look at the examples
// CGenericMode* CreateNewMode(
//   CGenericMode* pOldMode,		// Pointer on the previous mode of operation
//   int idxMode					// New mode of operation index
//   )

//-------------------------------------------------------------------------
//! \class CGenericMode
//! \brief Generic class for managing a mode of operation
//! Defines the minimal interface for managing a mode of operation
//! Manages a list of modifiers
class CGenericMode
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor
  //! initialization of parameters to default values
  CGenericMode();

  //-------------------------------------------------------------------------
  //! \brief Destructor
  //! delete the list of modifiers
  virtual ~CGenericMode();
  
  //-------------------------------------------------------------------------
  //! \brief Beginning of the mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  //! \brief End of the mode
  virtual void EndMode();
  
  //-------------------------------------------------------------------------
  //! \brief  Called on end of change of a modifier
  //! \param  idxModifier Index of affected modifier
  //! \return If a mode change, returns the requested mode
  virtual int OnEndChange(
    int idxModifier
    );
  
  //-------------------------------------------------------------------------
  //! \brief  Reading saved settings
  //! \return true if OK
  virtual bool ReadParams();
  
  //-------------------------------------------------------------------------
  //! \brief  Writing parameters to save them
  virtual void WriteParams();
  
  //-------------------------------------------------------------------------
  //! \brief  Mode display on the screen
  //! This method is called regularly by the main loop
  //! By default, it displays the list of modifiers
  virtual void PrintMode();
  
  //-------------------------------------------------------------------------
  //! \brief  To display information outside of modifiers at the begin of PrintMode
  virtual void PrePrint();
  
  //-------------------------------------------------------------------------
  //! \brief  To display information outside of modifiers at the end of PrintMode
  virtual void AddPrint();
  
  //-------------------------------------------------------------------------
  //! \brief  Keyboard order processing
  //! If the key is a mode change key, returns the requested mode
  //! This method is called regularly by the main loop
  //! By default, handles modifiers
  //! \param[in] key Touch to treat
  //! \return If a mode change, returns the requested mode, else return NOMODE
  virtual int KeyManager(
    unsigned short key
    );
  
  //-------------------------------------------------------------------------
  //! \brief  Possible management of fund tasks
  virtual void OnLoop();

  //-------------------------------------------------------------------------
  //! \brief  Update screen (standard behavior = pDisplay->sendBuffer())
  virtual void UpdateScreen();

  //-------------------------------------------------------------------------
  //! \brief  Change curent mode, never call this function in a mode
  //! Always called only in loop
  //! \param[in] iNewMode Index of the new mode
  static void ChangeMode(
    int iNewMode
    );
  
  //-------------------------------------------------------------------------
  //! \brief  Return index of current mode
  //! \return Index of current mode
  static int GetCurrentMode( ) { return idxCurrentMode;};
  
  //-------------------------------------------------------------------------
  //! \brief  Return a pointer on current mode
  //! \return Pointer on current mode
  static CGenericMode *GetpCurrentMode( ) { return pCurrentMode;};

  //-------------------------------------------------------------------------
  //! \brief  Return the language used
  //! \return Language used
  static int GetLanguage() { return iLanguage;};

  //-------------------------------------------------------------------------
  //! \brief  Initialize the language used
  //! \param iL Language index
  static void SetLanguage( int iL);

  //-------------------------------------------------------------------------
  //! \brief  Initializes the indexes of visible lines
  void SetIdxLines();

protected:
  static CGenericMode *pCurrentMode; //! Current mode
  
  static int idxCurrentMode; //! Index of active mode
  
  static int iNewMode; //! New possible mode of operation (default NOMODE) the change of operating mode is mandatory at the output of ManageKey

  static int iLanguage; //! Current language
  
  int bModifParam; //! Indicates whether a parameter is modifying
  
  bool bRedraw; //! Indicates that a complete re-display is necessary

  std::vector<CGenericModifier *> LstModifParams; //! List of Mode Parameter Modifiers

  int iFirstIdxLine; //! Index of first line type modifier (-1 if none)

  int iLastIdxLine; //! Index of last line type modifier (-1 if none)

  int iFirstVisibleIdxLine; //! Index of the first visible line type modifier (-1 if none)
  
  int iLastVisibleIdxLine; //! Index of the first visible line type modifier (-1 if none)

  int iYFirstLine; //! Y position for the first line

  int iNbLines; //! Number of lines visible on the screen
  
  int iMiddle; //! Middle of visible lines
  
  int idxSelParam; //! Index of the selected parameter to modify it
  
  bool bReadParams; //! true (by default) for reading parameters in BeginMode
  
  bool bWriteParams; //! true (by default) for writing parameters in EndMode
  
public:
  static uint8_t uiSDError; //! SD card error code
};

#define MAXCHAR_ERROR 50 //!< Maximum chars for error message

//-------------------------------------------------------------------------
//! \class CModeError
//! \brief Error mode management class
//! Displays the error and waits for a shutdown
class CModeError : public CGenericMode
{
public:
  //-------------------------------------------------------------------------
  //! \brief  Constructor
  //! initialization of parameters to default values
  CModeError();

  //-------------------------------------------------------------------------
  //! \brief Beginning of the mode
  virtual void BeginMode();

  //-------------------------------------------------------------------------
  //! \brief Set error text
  //! \param pStr Error text
  static void SetTxtError(
    char *pStr
    );

  //-------------------------------------------------------------------------
  //! \brief Set new mode on any key
  //! \param iMode new mode
  //! \param uiTime Max time (ms) before stop error mode if different of 0 (default)
  static void SetNewMode(
    int  iMode,
    uint32_t uiTime=0
    );

  //-------------------------------------------------------------------------
  //! \brief Mode display on the screen
  //! This method is called regularly by the main loop
  //! By default, it displays the list of modifiers
  virtual void PrintMode();
  
  //-------------------------------------------------------------------------
  //! \brief  Keyboard order processing
  //! If the key is a mode change key, returns the requested mode
  //! This method is called regularly by the main loop
  //! By default, handles modifiers
  //! \param key Touch to treat
  //! \return If a mode change, returns the requested mode, else return NOMODE
  virtual int KeyManager(
    unsigned short key
    );
  
protected:
  uint32_t uiEndTime;                   //! Mode end time if different of 0
  static char ErrorText[MAXCHAR_ERROR]; //! Error text
  static int  iNewMode;                 //! New mode on key (0 if not)
  static uint32_t uiTimeMax;            //! Max time (ms) before stop error mode if different of 0 (default)
};

//-------------------------------------------------------------------------
//! \enum LEVEL_LOG
//! \brief Levels of log entries
enum LEVEL_LOG {
  LLOG      = 0, //!< Basic level with the dates of entry and exit of a software and its main functions
  LINFO     = 1, //!< Level with additional information
  LDEBUG    = 2, //!< Level with debug info that can affect real-time operation
  LREALTIME = 3  //!< Maximum level with consequences on real time
};

#define MAXSOFTNAME     20  //!< Max char for software name
#define MAXLOGFILENAME  30  //!< Max char for log file name

//-------------------------------------------------------------------------
//! \class LogFile
//! \brief Management class of software flow log file
class LogFile
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  //! \param pSoftName Software name
  //! \param level Log level
  //! \param bCons Info to double the Log with console outputs
  LogFile(
    const char *pSoftName,
    LEVEL_LOG level=LLOG,
    bool bCons=false
    );

  //-------------------------------------------------------------------------
  //! \brief Set Log file name
  //! \param pSoftName Software name
  static void SetLogFileName(
    char *pSoftName
	);
  
  //-------------------------------------------------------------------------
  //! \brief Start the log
  static void StartLog() {bStarting = true;};
  
  //-------------------------------------------------------------------------
  //! \brief Check the number of lines in the log file
  static void CheckLines();
  
  //-------------------------------------------------------------------------
  //! \brief Static function of adding a line in the log file
  //! \param Level Level of the log line, if the level is higher than the active level, the log is not memorized
  //! \param pLogString Pointer on the string (\ n at the end unnecessary)
  //! \param ... List of composition parameters of sprintf style string
  //! \return true if OK, false if open log file problem
  static bool AddLog(
    LEVEL_LOG Level,
    const char *pLogString,
    ...
    );
  
  //-------------------------------------------------------------------------
  //! \brief Set the active level of the log
  //! \param level Level of the log line, if the level is higher than the active level, the log is not memorized
  static void SetLogLevel( LEVEL_LOG level) {LogLevel = level;};
  
  //-------------------------------------------------------------------------
  //! \brief Set console outputs
  //! \param bCons true to print message in console
  static void SetbConsole( bool bCons) {bConsole = bCons;};
  
  //-------------------------------------------------------------------------
  //! \brief Get console outputs
  //! \return bConsole vale
  static bool GetbConsole( ) { return bConsole;};
  
protected:
  static bool bStarting; //! Static info that indicate if log is starting
  
  static bool bCheckLines; //! Static info of checking the number of lines in the log file
  
  static LEVEL_LOG LogLevel; //! Static Info of the active level of the log
  
  static bool bConsole; //! Static Info to double the Log with console outputs
  
  static char softName[MAXSOFTNAME+1]; //! Static info of the software name displayed in each trace

  static char logFileName[MAXLOGFILENAME+1]; //! Log file name

  static char oldFileName[MAXLOGFILENAME+1]; //! Old log file name

  static LogFile *pInstance; //! Pointer on the instance of the class
};


#endif // CGENERICMODE_H
