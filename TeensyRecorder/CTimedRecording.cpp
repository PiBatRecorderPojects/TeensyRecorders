//-------------------------------------------------------------------------
//! \file CTimedRecording.cpp
//! \brief Classe de gestion du mode Enregistreur cadencé
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2020 Vrignault Jean-Do. All rights reserved.
/* 
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <Snooze.h>
#include "SdFat.h"
#include <U8g2lib.h>
#include "CTimedRecording.h"

// Screen manager
extern U8G2 *pDisplay;

// Key manager
extern CKeyManager KeyManag;

// SD Card Manager
extern SdFs sd;

// Gestionnaire du fichier LogPR.txt
extern LogFile logPR;

// Pour la récup de l'heure RTC au réveil
extern time_t getTeensy3Time();

// Drivers du mode veille
extern SnoozeDigital digital;
extern SnoozeAlarm   alarm;
#if defined(__IMXRT1062__) // Teensy 4.1
  extern SnoozeUSBSerial usb;
#endif
// Initialisation de gestionnaire de veille
extern SnoozeBlock configTeensy;

//-------------------------------------------------------------------------
//! \class CTimedRecording
//! \brief Timed Recording mode management class

// Strings for parameters
const char formIDXTRTIMER[]       =  "&$s";
const char *txtIDXTRTIMER[]       = {"%04ds",
                                     "%04ds",
                                     "%04ds",
                                     "%04ds"};
const char formIDXTRRTIME[]       =  "        &$s";
extern const char *txtIDXTRRTIME[];
const char formIDXTRWTIME[]       =  "        &$s";
extern const char *txtIDXTRWTIME[];
const char formIDXTRHEURE[]       =  "&$:&$:&$";
const char *txtIDXTRHEURE[]       = {"%s",
                                     "%s",
                                     "%s",
                                     "%s"};
const char formIDXTRDATE[]        =  "&$/&$/&$";
const char *txtIDXTRDATE[]        = {"%s",
                                     "%s",
                                     "%s",
                                     "%s"};
const char formIDXTRFE[]          =  "&$s";
const char *txtIDXTRFE[]          = {"%d kHz",
                                     "%d kHz",
                                     "%d kHz",
                                     "%d kHz"};
const char formIDXTRSD[]          =  "                  & ";
const char formIDXTRLEAVE[]       =  " Quitter &$$";
extern const char *txtIDXTRLEAVE[];
extern const char *txtIDXSD[];
extern const char *txtOuiNon[];
extern const char *txtIDXRECON[];

// Timed Recording mode screen
//   12345678901234567890  y
// 1 384kHz         [Bat]  0
// 2    Timed Recording    9
// 2 24/03/2020   22:12:32 17
// 3 Wait   3600s          26
// 4 Record 3600s          35
// 5    || 3600s           44
// 6 xxxxWav SD 99%        53
// 7 Clic to stop          62
// X 000000000000000001111
//   001123344566778990012
//   062840628406284062840

//-------------------------------------------------------------------------
//! \brief Constructor (initialization of parameters to default values)
CTimedRecording::CTimedRecording()
{
  // Set parameters
  iRecordMode   = PBREAK;
  iRecordTime   = 0;
  bLeave        = false;
  // Creating Parameter Modifier Lines
  // IDXTRRECORD  Record status
  LstModifParams.push_back(new CPlayRecModifier( &iRecordMode , false, true, 25, 9, 18, 36));
  // IDXTRDOWNC   Downcounter
  LstModifParams.push_back(new CModificatorInt ( formIDXTRRTIME, false, txtIDXTRTIMER, &iDowncounter, 0, 9999, 1, 64, 36));
  // IDXTRTIMER   Record time
  LstModifParams.push_back(new CSecondModifier  ( SECOND_HMS, false, txtIDXTRRTIME, (int *)&paramsOperateur.uiTRECRecord, 1, 86400, 0, 18));
  //LstModifParams.push_back(new CModificatorInt ( formIDXTRRTIME, false, txtIDXTRRTIME, &paramsOperateur.uiTRECRecord, 0, 9999, 1, 0, 18));
  // IDXTRWAIT    Wait time
  LstModifParams.push_back(new CSecondModifier  ( SECOND_HMS, false, txtIDXTRWTIME, (int *)&paramsOperateur.uiTRECWait, 1, 86400, 0, 26));
  //LstModifParams.push_back(new CModificatorInt ( formIDXTRWTIME, false, txtIDXTRWTIME, &paramsOperateur.uiTRECWait, 0, 9999, 1, 0, 26));
  // IDTRBAT      Internal battery charge level in %
  LstModifParams.push_back(new CBatteryModifier( &iNivBatLiPo, 24, 8, 100, 1));
  // IDXTRHOUR    Hour
  LstModifParams.push_back(new CHourModifier( formIDXTRHEURE , false, txtIDXTRHEURE, NULL, 78, 9));
  // IDXTRDATE    Date
  LstModifParams.push_back(new CDateModifier( formIDXTRDATE, false, txtIDXTRDATE, 0, 9));
  // IDXTRFE      FE
  LstModifParams.push_back(new CModificatorInt ( formIDXTRFE, false, txtIDXTRFE, &iFE, 0, 9999, 1, 0, 0));
  // IDXTRSD      SD status
  LstModifParams.push_back(new CSDModifier   ( formIDXTRSD, false, txtIDXSD, "wav", 0, 45));
  // IDXTRLEAVE   To leave mode
  LstModifParams.push_back(new CBoolModifier ( formIDXTRLEAVE, false, false, txtIDXTRLEAVE, &bLeave, (const char **)txtOuiNon, 0, 54));
}

//-------------------------------------------------------------------------
//! \brief Destructor
CTimedRecording::~CTimedRecording()
{
}

//-------------------------------------------------------------------------
//! \brief Creation du gestionnaire d'enregistrement
void CTimedRecording::CreateRecorder()
{
  //Serial.println("CModeGenericRec new CRecorder");
  if (GetRecorderType() >= PASSIVE_STEREO)
  {
    //Serial.println("CModeGenericRec new CRecorder Stereo");
    pRecorder = new CRecorderS( &paramsOperateur, NBBUFFERRECORDT);
  }
  else
  {
    //Serial.println("CModeGenericRec new CRecorder");
    pRecorder = new CRecorder( &paramsOperateur, NBBUFFERRECORDT);
  }
}

//-------------------------------------------------------------------------
//! \brief Beginning of the mode
void CTimedRecording::BeginMode()
{
  // Lecture des paramètres
  CGenericMode::ReadParams();
  // Set some parameters
  paramsOperateur.bTypeSeuil  = false;                     // Relative threshold
  paramsOperateur.iSeuilDet   = 90;
  paramsOperateur.uiDurMax    = paramsOperateur.uiTRECRecord;
  // Call of the parent method without read params
  bReadParams = false;
  CModeGenericRec::BeginMode();
  if (idxSelParam >=0 and idxSelParam <= IDXMAXTREC)
    LstModifParams[idxSelParam]->SetbSelect( false);
  idxSelParam = -1;
  iStatus = 0;
  // Test battery
  CheckBatteries(MAX_WAITMESURE);
  // Next time for check batteries
  uiTimeCheck = millis() + TIMECHECKBATTERY;
  // Initialization
  switch(paramsOperateur.uiFe)
  {
  case FE24KHZ : iFE =  24; break;
  case FE48KHZ : iFE =  48; break;
  case FE96KHZ : iFE =  96; break;
  case FE192KHZ: iFE = 192; break;
  case FE250KHZ: iFE = 250; break;
  case FE384KHZ: iFE = 384; break;
  case FE500KHZ: iFE = 500; break;
  }
  iCurrentSecond = 61;
  LstModifParams[IDXTRRECORD ]->SetEditable( false);
  LstModifParams[IDXTRDOWNC  ]->SetEditable( false);
  LstModifParams[IDXTRTIMER  ]->SetEditable( false);
  LstModifParams[IDXTRWAIT   ]->SetEditable( false);
  LstModifParams[IDTRBAT     ]->SetEditable( false);
  LstModifParams[IDXTRHOUR   ]->SetEditable( false);
  LstModifParams[IDXTRDATE   ]->SetEditable( false);
  LstModifParams[IDXTRFE     ]->SetEditable( false);
  LstModifParams[IDXTRSD     ]->SetEditable( false);
  char sX10[5];
  if (paramsOperateur.bExp10)
    strcpy( sX10, "X10");
  else
    strcpy( sX10, "X1");
  LogFile::AddLog( LLOG, "Timed Recording, Recording start time %s, Waiting start time %s, recording period %d, waiting period for %d s, SF %d kHz, %s", 
    paramsOperateur.sHDebut, paramsOperateur.sHFin, paramsOperateur.uiTRECRecord, paramsOperateur.uiTRECWait, iFE, sX10);
  if (CModeVeille::iWhoWakeUp == 0 or CModeVeille::iWhoWakeUp == PINPUSH)
    // Screen information for 30s
    iScreen = TRSCRENTOPRINT;
  else
    // No screen after wakeup
    iScreen = TRSCREENCLEAR;
  iCounterScreen = 0;
  // Starts a recording period
  StartRecordingPeriod();
}

//-------------------------------------------------------------------------
//! \brief End of the mode
void CTimedRecording::EndMode()
{
  if (pRecorder != NULL and pRecorder->IsRecording())
    pRecorder->StopRecording();
  if (pRecorder != NULL and pRecorder->IsStarting())
    pRecorder->StopAcquisition();
  // We restore the operator parameters
  ReadParams();
  // Call of the parent method
  CModeGenericRec::EndMode();
}

//-------------------------------------------------------------------------
//! \brief Starts a recording period
void CTimedRecording::StartRecordingPeriod()
{
  LogFile::AddLog( LLOG, "*** Timed Recording start of a recording period for %d s, SF %d kHz", paramsOperateur.uiTRECRecord, iFE);
  pRecorder->StartPreampPower();
  // Préparation période d'enregistrement
  iRecordMode    = PREC;
  iDowncounter   = paramsOperateur.uiTRECRecord;
  iCurrentSecond = second();
  iStatus        = TRSTATUSTORECORD;
  LstModifParams[IDXTRSD]->SetbCalcul(true);
  // On lance l'acquisition
  pRecorder->StartAcquisition();
}

//-------------------------------------------------------------------------
//! \brief Stop a recording period and wait for the next one
void CTimedRecording::StopRecordingPeriod()
{
  // On stoppe m'enregistrement et l'acquisition
  pRecorder->StopRecording();
  pRecorder->StopAcquisition();
  iRecordMode = PBREAK;
  if (iScreen != TRSCREENCLEAR)
    // Waiting period with active screen
    iStatus = TRSTATUSWAITINGSCREEN;
  else
    // Waiting period in sleep mode
    iStatus = TRSTATUSWAITING;
  pRecorder->StopPreamPower();

  LogFile::AddLog( LLOG, "*** Timed Recording start of a waiting period for %d s", paramsOperateur.uiTRECWait);
  // Mise en veille pour la durée demandée
  // Mesure des batteries
  CModeGeneric::CheckBatteries(MAX_WAITMESURE);
  // Compteur de durée de la veille
  iDowncounter = paramsOperateur.uiTRECWait;
}

// Save hour just before sleep
static int iHSleep, iMSleep, iSSleep;

//-------------------------------------------------------------------------
//! \brief Standby mode management in future recording waiting mode
void CTimedRecording::SleepOnWaitingPeriod()
{
  // Mise en veille
  Serial.println("=== Début de veille ===");
  int who = 0;
  //digitalWriteFast( 13, HIGH);
  int iAttenteSecondes, iH, iM, iS;
  iAttenteSecondes = iDowncounter;  // Duré courante
  iH = iAttenteSecondes / 3600;
  iM = (iAttenteSecondes - iH*3600) / 60;
  iS = iAttenteSecondes - iH*3600 - iM*60;
  // Save sleep time
  iHSleep = hour();
  iMSleep = minute();
  iSSleep = second();
  //Serial.printf("Wakeup at %02d:%02d:%02d\n", iH_Wakeup, iM_Wakeup, iS_Wakeup);
  // Init du bouton de sortie de veille
  digital.pinMode(PINPUSH, INPUT_PULLUP, RISING); // Push
  //digital.pinMode(PINDOWN, INPUT_PULLUP, RISING); // Down
  // ATTENTION, ne pas mettre l'heure du réveil mais le temps en H/Mn/secondes avant le réveil
  alarm.setRtcTimer( iH, iM, iS);
  delay(200); // Pour permettre l'affichage des traces sur un Teensy 4.1 qui va très vite
  // =====     Et veille    =====
digitalWrite(LED_BUILTIN, LOW);
#if defined(__IMXRT1062__) // Teensy 4.1
#ifdef SNOOZE_WITH_RESET
    // For Teensy 4.1, modified version of Snooze make a CPU restart with who make wake up in SRC_GPR5
    //Snooze.deepSleep( configTeensy);
    Snooze.hibernate( configTeensy);
    // End of sleep mode by Reset CPU
    while (1) ;
#else
    // For Teensy 4.1, modified version of Snooze but work only less than 10. After, you need a reset CPU
    //who = Snooze.sleep( configTeensy);
    //who = Snooze.deepSleep( configTeensy);
    who = Snooze.hibernate( configTeensy);
#endif
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  who = Snooze.hibernate( configTeensy);
#endif
  // ===== Sortie de veille =====
  delay(100);  // This delay is important before initializing the serial link (more than 1ms)
  Serial.begin(115200);
  delay(200); // This delay is important before using the serial link (more than 100ms)
  Serial.println("Sortie de veille");
  //digitalWriteFast( 13, LOW);
  // Fin de veille
  // Initialise l'heure de la libairie Time avec la RTC du Teensy 3.0
  setSyncProvider(getTeensy3Time);
  // Initialisation de la carte SD
  if (!isSDInit())
  {
    Serial.println("SdFatSdio begin() failed !!!");
    if (commonParams.iLanguage == LFRENCH)
      CModeError::SetTxtError((char *)"Carte SD illisible !");
    else
      CModeError::SetTxtError((char *)"SD card unreadable!");
    // Passage en mode erreur
    iNewMode = MERROR;
  }
  else
    sd.vol()->freeClusterCount();
  //Serial.printf("Wake-up scheduled for %02d:%02d:%02d\n", iH_Wakeup, iM_Wakeup, iS_Wakeup);
  // Test si sortie via bouton
  if (who == PINPUSH)
  {
    // Attente ordre opérateur éventuel
    Serial.println("=== Attente ordre ===");
    iScreen = TRSCRENTOPRINT;
    iStatus = TRSTATUSWAITINGSCREEN;
    iCounterScreen = 30;
    // Calcul de la durée de la veille
    int iNow    = hour() * 3600 + minute() * 60 + second();
    int iSleep  = iHSleep * 3600 + iMSleep * 60 + iSSleep;
    if (iNow < iHSleep)
      iNow += 24*3600;
    iSleep = iNow - iSleep;
    // Calcul du temps restant en secondes
    iDowncounter -= iSleep;
    if (iDowncounter <= 0)
      // On relance une période d'enregistrement
      StartRecordingPeriod();
  }
  else
  {
    Serial.println("=== Fin de veille ===");
    // On relance une période d'enregistrement
    StartRecordingPeriod();
  }
}
  
//-------------------------------------------------------------------------
//! \brief Gestion des tâches de fonds
void CTimedRecording::OnLoop()
{
  // Appel de la fonction de base
  CModeGenericRec::OnLoop();
  // Si en acquisition et pas encore en enregistrement, on lance l'enregistrement
  if (iStatus == TRSTATUSTORECORD and pRecorder->IsStarting() and pRecorder->IsNoiseOK() and !pRecorder->IsRecording())
  {
    // On démarre l'enregistrement
    pRecorder->StartRecording();
    iStatus = TRSTATUSRECORDING;
    //Serial.println("CTimedRecording::OnLoop StartRecording");
  }
  // Vérification de la durée d'enregistrement
  else if (iStatus == TRSTATUSRECORDING and iDowncounter == 0)
  {
    // Fin de la période d'enregistrement
    iStatus = TRSTATUSENDRECORD;
    StopRecordingPeriod();
    //Serial.println("CTimedRecording::OnLoop StopRecordingPeriod");
  }
  // Vérification fin de période d'attente avec écran allumé
  else if ((iStatus == TRSTATUSWAITINGSCREEN or iStatus == TRSTATUSWAITING) and iDowncounter == 0)
  {
    StartRecordingPeriod();
  }
  else if (iStatus == TRSTATUSWAITING)
    // Go in sleep mode
    SleepOnWaitingPeriod();
}

//-------------------------------------------------------------------------
//! \brief Affichage du mode sur l'écran
//! Cette méthode est appelée régulièrement par le loop principal
//! à charge des différents modes d'afficher les informations nécessaires
void CTimedRecording::PrintMode()
{
  if (second() != iCurrentSecond)
  {
    // Décomptage
    if (iCounterScreen > 0)
      iCounterScreen--;
    iDowncounter--;
    if (iDowncounter < 0)
      iDowncounter = 0;
    iCurrentSecond = second();
  }
  switch (iScreen)
  {
  case TRSCRENTOPRINT :  //!< Screen need to print
    if (idxSelParam >=0 and idxSelParam <= IDXMAXTREC)
      LstModifParams[idxSelParam]->SetbSelect( false);
    idxSelParam = IDXTRLEAVE;
    LstModifParams[idxSelParam]->SetbSelect( true);
    iScreen = TRSCREENOK;
    iCounterScreen = 30;
    // To check battery when screen is on
    uiTimeCheck = millis() + 100;
    break;
  case TRSCREENOK     :  //!< Screen with informations
    CModeGenericRec::PrintMode();
    if (iCounterScreen <= 0)
      iScreen = TRSCREENTOCLEAR;
    break;
  case TRSCREENTOCLEAR:  //!< Screen need to clear
    pDisplay->clearDisplay();
    pDisplay->display();
    iScreen = TRSCREENCLEAR;
    if (iStatus == TRSTATUSWAITINGSCREEN)
      // Waiting period in sleep mode
      iStatus = TRSTATUSWAITING;
    break;
  case TRSCREENCLEAR  :  //!< Without screen
    // Never check battery when screen is off
    uiTimeCheck = millis() + TIMECHECKBATTERY;
    break;
  }
}
  
//-------------------------------------------------------------------------
//! \brief Traitement des ordres claviers
//! Si la touche est une touche de changement de mode, retourne le mode demandé
//! Cette méthode est appelée régulièrement par le loop principal
//! à charge des différents modes de traiter les actions opérateurs
//! \param key Touch to treat
int CTimedRecording::KeyManager(
  unsigned short key
  )
{
  // Appel de la fonction de base
  int iNewMode = NOMODE;
  if (iScreen == TRSCREENOK)
    // Fonctionnement normal écran alumé avec prise en compte des ordres opérateur
    iNewMode = CModeGenericRec::KeyManager( key);
  else if (iScreen == TRSCREENCLEAR and (key == K_PUSH or key == K_MODEA))
    // Passage en mode attente opérateur
    iScreen = TRSCRENTOPRINT;
  // Test si temps de la mesure batteries et pas en mode stéréo (en stéréo l'ADC des batteries externe est déjà utilisé)
  if (millis() > uiTimeCheck and !(GetRecorderType() >= PASSIVE_STEREO and paramsOperateur.iStereoRecMode == SMSTEREO))
  {
    if (max17043.isOK() and !bMAXPowerOn)
    {
      // Power On MAX and wait 125ms for mesure
      CheckBatteries(MAX_POWER);
      uiTimeCheck = millis() + 125;
    }
    else
    {
      if (max17043.isOK())
        // Test battery
        CheckBatteries(MAX_MESURE);
      else
        // Test battery
        CheckBatteries(MAX_WAITMESURE);
      // Next time for check batteries
      uiTimeCheck = millis() + TIMECHECKBATTERY;
    }
  }
  // Test si temps de passer en veille
  if (IsTimeVeille())
    iNewMode = MVEILLE;
  return iNewMode;
}

//-------------------------------------------------------------------------
//! \brief Called on end of change of a modifier
//! If a mode change, returns the requested mode
//! \param idxModifier Index of affected modifier
int CTimedRecording::OnEndChange(
  int idxModifier
  )
{
  int iNewMode = NOMODE;
  if (idxModifier == IDXTRLEAVE)
  {
    if (bLeave)
    {
      if (GetRecorderType() == ACTIVE_RECORDER)
        // In Active Recorder K_MODEA change mode : Record mode -> Player -> Parameters -> Record mode
        iNewMode = MPLAYER;
      else
        iNewMode = MPARAMS;
    }
  }
  return iNewMode;
}
