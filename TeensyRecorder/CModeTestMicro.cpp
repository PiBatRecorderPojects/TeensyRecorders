/* 
 * File:   CModeTestMicro.cpp
   PassiveRecorder Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "SdFat.h"
#include <Wire.h>
#include <U8g2lib.h>
#include "Const.h"
#include "TimeLib.h"
#include "CModeTestMicro.h"
#include "WriteBMP16C.h"
#if defined(__MK66FX1M0__) // Teensy 3.6
  #include "RamMonitor.h"
#endif

// Gestionnaire du fichier LogPR.txt
extern LogFile logPR;

// Gestionnaire de carte SD
extern SdFs sd;

// Screen manager
extern U8G2 *pDisplay;

// Serial nuber string with recorder name
extern char sSerialName[MAXNAMERECORDER];

#if defined(__MK66FX1M0__) // Teensy 3.6
  // Teensy 3.6 Ram monitoring (https://github.com/veonik/Teensy-RAM-Monitor)
  extern RamMonitor ramMonitor;
#endif

// Fréquence minimale du graphe BMP et du test détat du micro (kHz)
#define FRMINTEST 10
// Indice correspondant à FRMINTEST
#define IDXMINBMP 7
// Fréquence maximale du graphe BMP et du test détat du micro (kHz)
#define FRMAXTEST 180
// Indice correspondant à FRMAXTEST
#define IDXMAXBMP 120
// Indice correspondant aux canaux de moindre poids (150kHz)
#define IDXMAXBD  93

// List of 128 channels
const float fTbCnx[] =     {   
//   0      1      2      3      4      5      6      7      8      8     10     11     12     13     14     15     16     17     18     19
    0.0,   1.5,   3.0,   4.5,   6.0,   7.5,   9.0,  10.5,  12.0,  13.5,  15.0,  16.5,  18.0,  19.5,  21.0,  22.5,  24.0,  25.5,  27.0,  28.5,
//  20     21     22     23     24     25     26     27     28     29     30     31     32     33     34     35     36     37     38     39
   30.0,  31.5,  33.0,  34.5,  36.0,  37.5,  39.0,  40.5,  42.0,  43.5,  45.0,  46.5,  48.0,  49.5,  51.0,  52.5,  54.0,  55.5,  57.0,  58.5,
//  40     41     42     43     44     45     46     47     48     49     50     51     52     53     54     55     56     57     58     59
   60.0,  61.5,  63.0,  64.5,  66.0,  67.5,  69.0,  70.5,  72.0,  73.5,  75.0,  76.5,  78.0,  79.5,  81.0,  82.5,  84.0,  85.5,  87.0,  88.5,
//  60     61     62     63     64     65     66     67     68     69     70     71     72     73     74     75     76     77     78     79
   90.0,  91.5,  93.0,  94.5,  96.0,  97.5,  99.0, 100.5, 102.0, 103.5, 105.0, 106.5, 108.0, 109.5, 111.0, 112.5, 114.0, 115.5, 117.0, 118.5,
//  80     81     82     83     84     85     86     87     88     89     90     91     92     93     94     95     96     97     98     99
  120.0, 121.5, 123.0, 124.5, 126.0, 127.5, 129.0, 130.5, 132.0, 133.5, 135.0, 136.5, 138.0, 139.5, 141.0, 142.5, 144.0, 145.5, 147.0, 148.5,
// 100    101    102    103    104    105    106    107    108    109    110    111    112    113    114    115    116    117    118    119
  149.0, 151.5, 153.0, 154.5, 156.0, 157.5, 159.0, 160.5, 162.0, 163.5, 165.0, 166.5, 168.0, 169.5, 171.0, 172.5, 174.0, 175.5, 177.0, 178.5,
// 120    121    122    123    124    125    126    127
  179.0, 181.5, 183.0, 184.5, 186.0, 187.5, 189.0, 190.5 };

// Minimum acceptable signal test template for MEMS microphone
const int iTbTemplateMinMEMS[] = {  
//    0.0    1.5    3.0    4.5    6.0    7.5    9.0   10.5   12.0   13.5   15.0   16.5   18.0   19.5   21.0   22.5   24.0   25.5   27.0  28.5
    -88,   -88,   -88,   -88,   -88,   -88,   -88,   -88,   -88,   -88,   -88,   -80,   -80,   -80,   -80,   -80,   -82,   -84,   -86,  -88,
//   30.0   31.5   33.0   34.5   36.0   37.5   39.0   40.5   42.0   43.5   45.0   46.5   48.0   49.5   51.0   52.5   54.0   55.5   57.0  58.5 
    -90,   -90,   -90,   -92,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -92,   -90,   -90,  -90,
//   60.0   61.5   63.0   64.5   66.0   67.5   69.0   70.5   72.0   73.5   75.0   76.5   78.0   79.5   81.0   82.5   84.0   85.5   87.0  88.5 
    -90,   -91,   -91,   -92,   -92,   -93,   -93,   -94,   -94,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,  -95,
//   90.0   91.5   93.0   94.5   96.0   97.5   99.0  100.5  102.0  103.5  105.0  106.5  108.0  109.5  111.0  112.5  114.0  115.5  117.0 118.5 
    -95,   -95,   -95,   -95,   -95,   -95,   -95,   -98,   -98,   -98,   -98,   -98,   -98,   -98,   -98,   -98,   -98,   -98,   -98,  -98,
//  120.0  121.5  123.0  124.5  126.0  127.5  129.0  130.5  132.0  133.5  135.0  136.5  138.0  139.5  141.0  142.5  144.0  145.5  147.0 148.5
   -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100, -100,
//  149.0  151.5  153.0  154.5  156.0  157.5  159.0  160.5  162.0  163.5  165.0  166.5  168.0  169.5  171.0  172.5  174.0  175.5  177.0 178.5
   -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100, -100,
//  179.0  181.5  183.0  184.5  186.0 187.5  189.0  190.5
   -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100};
    
// Minimum acceptable signal test template for FG microphone
const int iTbTemplateMinFG[] = {  
//    0.0    1.5    3.0    4.5    6.0    7.5    9.0   10.5   12.0   13.5   15.0   16.5   18.0   19.5   21.0   22.5   24.0   25.5   27.0  28.5
    -88,   -88,   -88,   -88,   -88,   -88,   -88,   -88,   -88,   -88,   -88,   -80,   -80,   -80,   -80,   -80,   -82,   -84,   -86,  -88,
//   30.0   31.5   33.0   34.5   36.0   37.5   39.0   40.5   42.0   43.5   45.0   46.5   48.0   49.5   51.0   52.5   54.0   55.5   57.0  58.5 
    -90,   -90,   -90,   -92,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -92,   -90,   -90,  -90,
//   60.0   61.5   63.0   64.5   66.0   67.5   69.0   70.5   72.0   73.5   75.0   76.5   78.0   79.5   81.0   82.5   84.0   85.5   87.0  88.5 
    -90,   -91,   -91,   -92,   -92,   -93,   -93,   -94,   -94,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,   -95,  -95,
//   90.0   91.5   93.0   94.5   96.0   97.5   99.0  100.5  102.0  103.5  105.0  106.5  108.0  109.5  111.0  112.5  114.0  115.5  117.0 118.5 
    -95,   -95,   -95,   -95,   -95,   -95,   -95,   -98,   -98,   -98,   -98,   -98,   -98,   -98,   -98,   -98,   -98,   -98,   -98,  -98,
//  120.0  121.5  123.0  124.5  126.0  127.5  129.0  130.5  132.0  133.5  135.0  136.5  138.0  139.5  141.0  142.5  144.0  145.5  147.0 148.5
   -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100, -100,
//  149.0  151.5  153.0  154.5  156.0  157.5  159.0  160.5  162.0  163.5  165.0  166.5  168.0  169.5  171.0  172.5  174.0  175.5  177.0 178.5
   -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100, -100,
//  179.0  181.5  183.0  184.5  186.0 187.5  189.0  190.5
   -100,  -100,  -100,  -100,  -100,  -100,  -100,  -100};
    
// Maximum acceptable silence test template
const int iTbTemplateMax[] = {
//    0.0    1.5    3.0    4.5    6.0    7.5    9.0   10.5   12.0   13.5   15.0   16.5   18.0   19.5   21.0   22.5   24.0   25.5   27.0   28.5
   -105,  -105,  -105,  -105,  -105,  -105,  -105,  -105,  -105,  -105,  -105,  -105,  -105,  -105,  -105,  -105,  -105,  -105,  -105,  -105,
//   30.0   31.5   33.0   34.5   36.0   37.5   39.0   40.5   42.0   43.5   45.0   46.5   48.0   49.5   51.0   52.5   54.0   55.5   57.0   58.5 
   -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,
//   60.0   61.5   63.0   64.5   66.0   67.5   69.0   70.5   72.0   73.5   75.0   76.5   78.0   79.5   81.0   82.5   84.0   85.5   87.0   88.5 
   -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,
//   90.0   91.5   93.0   94.5   96.0   97.5   99.0  100.5  102.0  103.5  105.0  106.5  108.0  109.5  111.0  112.5  114.0  115.5  117.0  118.5 
   -108,  -108,  -108,  -108,  -108,  -108,  -108,  -108,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,
//  120.0  121.5  123.0  124.5  126.0  127.5  129.0  130.5  132.0  133.5  135.0  136.5  138.0  139.5  141.0  142.5  144.0  145.5  147.0  148.5
   -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,
//  149.0  151.5  153.0  154.5  156.0  157.5  159.0  160.5  162.0  163.5  165.0  166.5  168.0  169.5  171.0  172.5  174.0  175.5  177.0  178.5
   -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113,
//  179.0  181.5  183.0  184.5  186.0 187.5  189.0  190.5
   -113,  -113,  -113,  -113,  -113,  -113,  -113,  -113};

//                                   123456789012345678901
extern const char *txtTestMicroA[];
extern const char *txtTestMicroB[];
extern const char *txtTestMicroC[];
extern const char *txtTestMicroD[];
extern const char *txtTestSilenceA[];
extern const char *txtTestSilenceB[];
extern const char *txtTestSilenceC[];
extern const char *txtTestSignalA[];
extern const char *txtTestSignalB[];
extern const char *txtTestSignalC[];
extern const char *txtTestResultA[];
extern const char *txtTestResultAD[];
extern const char *txtTestResultAG[];
extern const char *txtTestResultAA[];
extern const char *txtTestResultAAD[];
extern const char *txtTestResultAAG[];
extern const char *txtTestResultD[];
extern const char *txtTestResultDD[];
extern const char *txtTestResultDG[];
extern const char *txtLogTestResultE[];
extern const char *txtLogTestResultES[];
extern const char *txtTestResultF[];
extern const char *txtTestResultG[];
extern const char *txtTestResultH[];
extern const char *txtTestResultI[];
extern const char *txtLogNivMono[];
extern const char *txtLogNivStereo[];
extern const char *txtTestResultB[MAXLANGUAGE][5];
extern const char *txtTestResultC[MAXLANGUAGE][6];

using namespace std;

//! Taille de la bitmap de résultat du test micro amélioré
static const int BMP_WIDTH  = 700;
static const int BMP_HEIGHT = 300;

/*-----------------------------------------------------------------------------
 Classe de gestion du mode test micro
-----------------------------------------------------------------------------*/

//! \enum IDXPARAMS
//! \brief Index des paramètres du mode Test micro
enum IDXPARAMS {
  IDXBG_MONO,     //!< Barre graphe mono
  IDXBG_STEREOD,  //!< Barre graphe stéréo canal droit
  IDXBG_STEREOG   //!< Barre graphe stéréo canal gauche
};
//using std::ofstream;
//-------------------------------------------------------------------------
// Constructeur
CModeTestMicro::CModeTestMicro(): graphe( 9)
{
  // Initialisation des variables
  iFreq = 0;
  iNivMax_D      = -120;
  iNivMax_G      = -120;
  iNivMaxBG_D    = -120;
  iNivMaxBG_G    = -120;
  iNivMaxTxt_D   = -120;
  iNivMaxTxt_G   = -120;
  iNivMaxTotal_D = -120;
  iNivMaxTotal_G = -120;
  bStereo        = false;
  // IDXBG_MONO, Barregraphe de la position du niveau de détection en mono
  LstModifParams.push_back(new CDrawBarModifier( &iNivMaxBG_D, 80, 8, 0, 50, -120, -50));
  // IDXBG_STEREOD, Barregraphe de la position du niveau de détection du canal droit en stéréo
  LstModifParams.push_back(new CDrawBarModifier( &iNivMaxBG_D, 78, 6, 0, 49, -120, -50));
  // IDXBG_STEREOG, Barregraphe de la position du niveau de détection du canal gauche en stéréo
  LstModifParams.push_back(new CDrawBarModifier( &iNivMaxBG_G, 78, 6, 0, 57, -120, -50));
  if (GetRecorderType() >= PASSIVE_STEREO and paramsOperateur.iStereoRecMode == SMSTEREO)
    // Test micro en stéréo
    LstModifParams[IDXBG_MONO]->SetbValid(false );
  else
  {
    // Test micro en mono
    LstModifParams[IDXBG_STEREOD]->SetbValid(false );
    LstModifParams[IDXBG_STEREOG]->SetbValid(false );    
  }
  iTestPhase = TEST_LEVEL;
  ulTimePhases = 0;
  iTestResult  = TEST_UNKNOW;
}

//-------------------------------------------------------------------------
// Début du mode
void CModeTestMicro::BeginMode()
{
  if (CModeGeneric::bDebug)
    Serial.println("CModeTestMicro::BeginMode");
  // Mode test micro, on impose certains paramètres
  paramsOperateur.iModeRecord= PTSTMICRO;                 // On impose le test micro (cas du retour en test micro suite à un test micro)
  paramsOperateur.uiDurMin   = 0;                         // 0s pour ne pas enregistrer
  paramsOperateur.uiDurMax   = 0;                         // Durée min = durée max
  paramsOperateur.bExp10     = true;                      // Expansion de temps X10
  paramsOperateur.iNbDetect  = 1;                         // Nb détection à 1 pour détecter en permanence
  paramsOperateur.iFreqFiltreHighPass = 0;                // Pas de filtre passe haut
  paramsOperateur.bFiltre    = false;                     // Pas de filtre passe bas ou linéaire
  paramsOperateur.uiFreqMin  = (FRMINTEST-2)*1000;        // FMin/FMax de test du micro (Hz)
  paramsOperateur.uiFreqMax  = (FRMAXTEST+2)*1000;
  paramsOperateur.uiFe       = FE384KHZ;                  // On impose 384kHz
  paramsOperateur.bTypeSeuil = false;                     // On impose le seuil relatif
  paramsOperateur.iSeuilDet  = 10;                        // On impose un seuil relatif de 10dB
#if defined(__MK66FX1M0__) // Teensy 3.6
  if (F_CPU < 144000000)
  {
    paramsOperateur.uiFreqMax  = 125000;
    paramsOperateur.uiFe       = FE250KHZ;                  // On impose 250kHz
    if (CModeGeneric::GetRecorderType() >= PASSIVE_STEREO)
    {
      paramsOperateur.uiFreqMax= 96000;
      paramsOperateur.uiFe     = FE192KHZ;                  // On impose 192kHz en stéréo
    }
  }
#endif
  if (CModeGeneric::GetRecorderType() >= PASSIVE_STEREO)
    paramsOperateur.iStereoRecMode = SMSTEREO;            // On impose un test stéréo
  bStartSilence = false;
  bStartSignal  = false;
  // Appel de la méthode de base sans lecture des paramètres
  bReadParams = false;
  CModeGenericRec::BeginMode();
  if (GetRecorderType() >= PASSIVE_STEREO and paramsOperateur.iStereoRecMode == SMSTEREO)
    bStereo = true;
  // Initialisation des niveaux mémorisée
  iMemoNivD = -121;
  iMemoNivG = -121;
  iMemoFreqD = 0;
  iMemoFreqG = 0;
  // On force l'affichage
  uiCurentTime = 0;
  uiCurentTimeMax_D = 0;
  uiCurentTimeMax_G = 0;
  iNivMaxTotal = -120;
  /*// Print Ram usage
  PrintRamUsage( (char *)txtRamUsageTstMicro);
  bool bMemo = LogFile::GetbConsole();
  logPR.SetbConsole( true);
  if (pRecorder != NULL)
    LogFile::AddLog( LLOG, "Size of Record buffer %d bytes (int16_t * MAXSAMPLE %d * Nb Buffer %d)", ((CRecorder *)pRecorder)->GetSizeoffSamplesRecordBuffer(), MAXSAMPLE, ((CRecorder *)pRecorder)->GetiNbSampleBuffer());
  logPR.SetbConsole( bMemo);*/
  //Serial.println("CModeTestMicro avant StartAcquisition");
  // On lance l'acquisition
  if (pRecorder != NULL)
    pRecorder->StartAcquisition();
  //Serial.println("CModeTestMicro après StartAcquisition");
}

//-------------------------------------------------------------------------
// Fin du mode
void CModeTestMicro::EndMode()
{
  //Serial.println("CModeTestMicro::EndMode");
  // Appel de la méthode de base qui stoppe l'acquisition sans écriture des paramètres
  bWriteParams = false;
  CModeGenericRec::EndMode();
  // Ecriture dans le log des niveaux mémorisée
  if (bStereo)
    LogFile::AddLog( LLOG, txtLogNivStereo[commonParams.iLanguage], iMemoNivD, iMemoFreqD, iMemoNivG, iMemoFreqG);
  else
    LogFile::AddLog( LLOG, txtLogNivMono[commonParams.iLanguage], iMemoNivD, iMemoFreqD);
  if (iTestResult < TEST_UNKNOW)
  {
    if (bStereo)
      LogFile::AddLog( LLOG, txtLogTestResultES[commonParams.iLanguage], txtTestResultC[commonParams.iLanguage][iTestResult], txtTestResultC[commonParams.iLanguage][iTestResultG]);
    else
      LogFile::AddLog( LLOG, txtLogTestResultE[commonParams.iLanguage], txtTestResultC[commonParams.iLanguage][iTestResult]);
  }
  // On restitue les paramètres opérateur et on passe hors test micro
  //Serial.println("CModeTestMicro::EndMode avant ReadParams");
  ReadParams();
  if (CModeGeneric::bDebug)
    Serial.println("CModeTestMicro::EndMode OK");
}

//-------------------------------------------------------------------------
//! \brief Passage en mode test silence
void CModeTestMicro::SwitchToSilencePhase()
{
  iTestPhase = TEST_SILENCE;
  iDecounter = 10;
  iDecounterSecond = second();
  LstModifParams[IDXBG_MONO   ]->SetbValid(false );
  LstModifParams[IDXBG_STEREOD]->SetbValid(false );
  LstModifParams[IDXBG_STEREOG]->SetbValid(false );    
}

//-------------------------------------------------------------------------
//! \brief Switching to signal test mode
void CModeTestMicro::SwitchToSignalPhase()
{
  // Retrieving the results of the silent mode (150kHz for the table, 120kHz for the graph)
  uint32_t *pTbMag  = pRecorder->GetTbMagnitudes();
  uint32_t *pTbMagG = pRecorder->GetTbMagnitudesG();
  uint32_t iMagR=0, iMagL=0;
  for (int i=1; i<FFTLEN/2; i++)
  {
    // Calculation of the average FFT magnitude of the channel
    iMagR = pTbMag[i] / MAXFFTSILENCE;
    if (bStereo)
      iMagL = pTbMagG[i] / MAXFFTSILENCE;
    if (paramsOperateur.iMicrophoneType == MTFG23329)
    {
      // In the case of an FG type microphone, we add 18dB
      iMagR = iMagR << 3;
      if (bStereo)
        iMagL = iMagL << 3;
    }
    // Transformation of magnitude in dB
    iTbSilenceCnx[i] = CGenericRecorder::GetNivDB(iMagR) / 10;
    if (bStereo)
      iTbSilenceCnxG[i] = CGenericRecorder::GetNivDB(iMagL) / 10;
    else
      iTbSilenceCnxG[i] = -121;
    graphe.SetFrequence( max(iTbSilenceCnx[i], iTbSilenceCnxG[i]), (int)CGenericRecorder::GetFreqIdx(i));
  }
  // Switching to signal mode
  iTestPhase = TEST_SIGNAL;
  iDecounter = 15;
  iDecounterSecond = second();
}

//-------------------------------------------------------------------------
//! \brief Test result
//! \param pSilence
//! \param pSignal
//! \return a value of TEST_MICRO
int CModeTestMicro::ReturnTestResult(
  int *pSilence,
  int *pSignal
  )
{
  /*
  Measurement on 94 channels of 1.5kHz wide. From 10.5kHz (7) to 180kHz (120). 
  
  Test Silence
  Calculation of the number of channels above the silence template = NbNoisyCnx
  
  Test Signal
  Calculation of the number of channels under the + 6dB signal template = NbGoodCnx
  Calculation of the number of channels under the signal template       = NbWeakCnx
  
  The microphone has 5 possible states:
  - Excellent if NbNoisyCnx = 0  and NbGoodCnx < 30
           or if NbNoisyCnx <= 5 and NbGoodCnx < 25
  - Good      if NbNoisyCnx = 0  and NbWeakCnx = 0
           or if NbNoisyCnx < 5  and NbWeakCnx = 0
  - Weak      if NbNoisyCnx > 5  and NbWeakCnx <= 10
           or if NbNoisyCnx = 0  and NbWeakCnx between 1 and 19
           or if NbNoisyCnx <= 5 and NbWeakCnx <= 10
  - Noisy     if NbNoisyCnx > 5  and NbWeakCnx = 0
  - Broken    if NbNoisyCnx > 5  and NbWeakCnx > 10
           or if NbNoisyCnx = 0  and NbWeakCnx >=20
           or if NbNoisyCnx <= 5 and NbWeakCnx > 10
  Noisy is likely never to be found, as a noisy mic is usually low
  */

  int iResult = TEST_UNKNOW;
  // Silent mode test
  int NbNoisyCnx = 0;
  for (int i=IDXMINBMP; i<=IDXMAXBMP; i++)
  {
    // Number of measurements above the max template
    if (pSilence[i] > iTbTemplateMax[i])
      NbNoisyCnx++;
  }
  // Signal mode test
  int NbGoodCnx = 0;
  int NbWeakCnx = 0;
  int iPlus = 10;
  for (int i=IDXMINBMP; i<=IDXMAXBMP; i++)
  {
    // Number of measurements under the template min + 6dB
    // And number of measurements under the template min
	if (i >= IDXMAXBD)
	  // A partir de 15kHz, on donne un poids de 0.2 à chaque canaux
	  iPlus = 2;
    if (paramsOperateur.iMicrophoneType == MTFG23329)
    {
      if (pSignal[i] < iTbTemplateMinFG[i]+6)
        NbGoodCnx += iPlus;
      if (pSignal[i] < iTbTemplateMinFG[i])
        NbWeakCnx += iPlus;
    }
    else
    {
      if (pSignal[i] < iTbTemplateMinMEMS[i]+6)
        NbGoodCnx += iPlus;
      if (pSignal[i] < iTbTemplateMinMEMS[i])
        NbWeakCnx += iPlus;
    }
  }
  NbGoodCnx /= 10;
  NbWeakCnx /= 10;
  // Test result
  if (NbNoisyCnx > 5)
  {
    // Microphone with over 5 noisy channels
    if (NbGoodCnx == 0 or NbWeakCnx == 0)
      // Excellent or good signal but noisy = Noisy
      iResult = TEST_NOISY;
    else if (NbWeakCnx >= 10)
      // More than 10 weak channels + noisy = Broken
      iResult = TEST_BROKEN;
    else
      // Max of 10 weak channels + noisy = Weak
      iResult = TEST_WEAK;
  }
  else if (NbNoisyCnx == 0)
  {
    // No noise on this mic
    if (NbGoodCnx < 30)
      // Excellent signal (+ 6dB above the template for more than 60% of the channels)
      iResult = TEST_EXCELLENT;
    else if (NbWeakCnx == 0)
      // Good signal (no channels under the template)
      iResult = TEST_GOOD;
    else if (NbWeakCnx <= 20)
      // Weak signal (at most 20 channels under the template)
      iResult = TEST_WEAK;
    else
      // Broken (more than 20 channels under the template)
      iResult = TEST_BROKEN;
  }
  else
  {
    // Limit noisy microphone (between 1 and 5 channels above the template)
    if (NbGoodCnx < 25)
      // Excellent signal (+ 6dB above the template for more than 60% of the channels)
      iResult = TEST_EXCELLENT;
    else if (NbWeakCnx == 0)
      // Good signal (no channels under the template)
      iResult = TEST_GOOD;
    else if (NbWeakCnx <= 10)
      // Weak signal (at most 10 channels under the template in addition to noisy channels)
      iResult = TEST_WEAK;
    else
      // Broken (more than 10 channels under the template in addition to noisy channels)
      iResult = TEST_BROKEN;
  }
  //Serial.printf( "ReturnTestResult NbNoisyCnx %d, NbGoodCnx %d, NbWeakCnx %d %s\n", NbNoisyCnx, NbGoodCnx, NbWeakCnx, txtTestResultC[commonParams.iLanguage][iResult]);
  return iResult;
}

//-------------------------------------------------------------------------
//! \brief Switching to result mode
void CModeTestMicro::SwitchToResultPhase()
{
  // Recovery of signal mode results (180kHz for the table, 120kHz for the graph)
  uint32_t *pTbMag  = pRecorder->GetTbMagnitudes();
  uint32_t *pTbMagG = pRecorder->GetTbMagnitudesG();
  uint32_t iMagR=0, iMagL=0;
  for (int i=1; i<FFTLEN/2; i++)
  {
    // Calculation of the average FFT magnitude of the channel
    iMagR = pTbMag[i] / MAXFFTSIGNAL;
    if (bStereo)
      iMagL = pTbMagG[i] / MAXFFTSIGNAL;
    if (paramsOperateur.iMicrophoneType == MTFG23329)
    {
      // In the case of an FG type microphone, we add 18dB
      iMagR = iMagR << 3;
      if (bStereo)
        iMagL = iMagL << 3;
    }
    // Transformation of magnitude in dB
    iTbSignalCnx[i] = CGenericRecorder::GetNivDB(iMagR) / 10;
    if (bStereo)
      iTbSignalCnxG[i] = CGenericRecorder::GetNivDB(iMagL) / 10;
    else
      iTbSignalCnxG[i] = -121;
    graphe.SetFrequence( max(iTbSignalCnx[i], iTbSignalCnxG[i]), (int)CGenericRecorder::GetFreqIdx(i));
    //graphe.SetFrequence( -70, (int)CGenericRecorder::GetFreqIdx(i));
  }

  // We stop the acquisition
  if (pRecorder != NULL and pRecorder->IsStarting())
    pRecorder->StopAcquisition();
  // Destruction of the recorder to free memory for the creation of the results bitmap
  if (pRecorder != NULL)
  {
    delete pRecorder;
    pRecorder = NULL;
  }
  //CModeGeneric::PrintRamUsage( "Ram après delete pRecorder");

  // Mono or right channel result
  iTestResult = ReturnTestResult( iTbSilenceCnx, iTbSignalCnx);
  if (bStereo)
    // Résultat mono ou canal droit
    iTestResultG = ReturnTestResult( iTbSilenceCnxG, iTbSignalCnxG);

  // Saving the result in a csv file
  /*
   * Test du 13/03/2020 15:51
   * Canaux (kHz),010.5,012.0,013.5,...
   * Signal (dB),-60,-71,-68,...
   * Min template,-60,-71,-68...
   * Max template,-120,-118,-119...
   * Silence (dB),-120,-118,-119,...
   */
  bool bTrace = false;
  //bTrace = true;
  char sFileName[40];
  char sTxt[700];
  char sTemp[20];
  FsFile dataFile;  // Info backup file
  sprintf( sFileName, "%s%s_MicroTests.csv", paramsOperateur.sPrefixe, sSerialName);
  // Ouverture du fichier
  if (dataFile.open(sFileName, O_CREAT | O_WRITE | O_APPEND))
  {
    // Write the 1st line of the result
    if (bStereo)
      sprintf( sTxt, txtTestResultAAD[commonParams.iLanguage], year(), month(), day(), hour(), minute(), second(), txtTestResultC[commonParams.iLanguage][iTestResult]);
    else
      sprintf( sTxt, txtTestResultAA[commonParams.iLanguage], year(), month(), day(), hour(), minute(), second(), txtTestResultC[commonParams.iLanguage][iTestResult]);
    // Result table
    dataFile.println( sTxt);
    if (bTrace) Serial.println (sTxt);
    if (bTrace) Serial.printf( "Taille chaine %d\n", strlen(sTxt));
    // Line frequency channels, from channel 7 (10.5kHz) to channel 150 (180kHz)
    strcpy( sTxt, txtTestResultB[commonParams.iLanguage][0]);
    for (int i=IDXMINBMP; i<=IDXMAXBMP; i++)
    {
      sprintf(sTemp, "\t%5.1f", fTbCnx[i]);
      strcat(sTxt, sTemp);
    }
    dataFile.println( sTxt);
    if (bTrace) Serial.println (sTxt);
    if (bTrace) Serial.printf( "Taille chaine %d\n", strlen(sTxt));
    // Line of levels of signal
    strcpy( sTxt, txtTestResultB[commonParams.iLanguage][1]);
    for (int i=IDXMINBMP; i<=IDXMAXBMP; i++)
    {
      sprintf(sTemp, "\t%03d", iTbSignalCnx[i]);
      strcat(sTxt, sTemp);
    }
    dataFile.println( sTxt);
    // Min signal template line
    if (bTrace) Serial.println (sTxt);
    if (bTrace) Serial.printf( "Taille chaine %d\n", strlen(sTxt));
    strcpy( sTxt, txtTestResultB[commonParams.iLanguage][2]);
    for (int i=IDXMINBMP; i<=IDXMAXBMP; i++)
    {
      if (paramsOperateur.iMicrophoneType == MTFG23329)
        sprintf(sTemp, "\t%03d", iTbTemplateMinFG[i]);
      else
        sprintf(sTemp, "\t%03d", iTbTemplateMinMEMS[i]);
      strcat(sTxt, sTemp);
    }
    dataFile.println( sTxt);
    if (bTrace) Serial.println (sTxt);
    if (bTrace) Serial.printf( "Taille chaine %d\n", strlen(sTxt));
    // Max silence template line
    strcpy( sTxt, txtTestResultB[commonParams.iLanguage][3]);
    for (int i=IDXMINBMP; i<=IDXMAXBMP; i++)
    {
      sprintf(sTemp, "\t%03d", iTbTemplateMax[i]);
      strcat(sTxt, sTemp);
    }
    dataFile.println( sTxt);
    if (bTrace) Serial.println (sTxt);
    if (bTrace) Serial.printf( "Taille chaine %d\n", strlen(sTxt));
    // Line of levels of silence
    strcpy( sTxt, txtTestResultB[commonParams.iLanguage][4]);
    for (int i=IDXMINBMP; i<=IDXMAXBMP; i++)
    {
      sprintf(sTemp, "\t%03d", iTbSilenceCnx[i]);
      strcat(sTxt, sTemp);
    }
    dataFile.println( sTxt);
    if (bTrace) Serial.println (sTxt);
    if (bTrace) Serial.printf( "Taille chaine %d\n", strlen(sTxt));

    if (bStereo)
    {
      sprintf( sTxt, txtTestResultAAG[commonParams.iLanguage], year(), month(), day(), hour(), minute(), second(), txtTestResultC[commonParams.iLanguage][iTestResultG]);
      // Tableau du résultat
      dataFile.println( sTxt);
      if (bTrace) Serial.println (sTxt);
      if (bTrace) Serial.printf( "Taille chaine %d\n", strlen(sTxt));
      // Line frequency channels, from channel 7 (10.5kHz) to channel 150 (180kHz)
      strcpy( sTxt, txtTestResultB[commonParams.iLanguage][0]);
      for (int i=IDXMINBMP; i<=IDXMAXBMP; i++)
      {
        sprintf(sTemp, "\t%5.1f", fTbCnx[i]);
        strcat(sTxt, sTemp);
      }
      dataFile.println( sTxt);
      if (bTrace) Serial.println (sTxt);
      if (bTrace) Serial.printf( "Taille chaine %d\n", strlen(sTxt));
      // Line of levels of signal
      strcpy( sTxt, txtTestResultB[commonParams.iLanguage][1]);
      for (int i=IDXMINBMP; i<=IDXMAXBMP; i++)
      {
        sprintf(sTemp, "\t%03d", iTbSignalCnxG[i]);
        strcat(sTxt, sTemp);
      }
      dataFile.println( sTxt);
      // Min signal template line
      if (bTrace) Serial.println (sTxt);
      if (bTrace) Serial.printf( "Taille chaine %d\n", strlen(sTxt));
      strcpy( sTxt, txtTestResultB[commonParams.iLanguage][2]);
      for (int i=IDXMINBMP; i<=IDXMAXBMP; i++)
      {
        if (paramsOperateur.iMicrophoneType == MTFG23329)
          sprintf(sTemp, "\t%03d", iTbTemplateMinFG[i]);
        else
          sprintf(sTemp, "\t%03d", iTbTemplateMinMEMS[i]);
        strcat(sTxt, sTemp);
      }
      dataFile.println( sTxt);
      if (bTrace) Serial.println (sTxt);
      if (bTrace) Serial.printf( "Taille chaine %d\n", strlen(sTxt));
      // Max silence template line
      strcpy( sTxt, txtTestResultB[commonParams.iLanguage][3]);
      for (int i=IDXMINBMP; i<=IDXMAXBMP; i++)
      {
        sprintf(sTemp, "\t%03d", iTbTemplateMax[i]);
        strcat(sTxt, sTemp);
      }
      dataFile.println( sTxt);
      if (bTrace) Serial.println (sTxt);
      if (bTrace) Serial.printf( "Taille chaine %d\n", strlen(sTxt));
      // Line of levels of silence
      strcpy( sTxt, txtTestResultB[commonParams.iLanguage][4]);
      for (int i=IDXMINBMP; i<=IDXMAXBMP; i++)
      {
        sprintf(sTemp, "\t%03d", iTbSilenceCnxG[i]);
        strcat(sTxt, sTemp);
      }
      dataFile.println( sTxt);
      if (bTrace) Serial.println (sTxt);
      if (bTrace) Serial.printf( "Taille chaine %d\n", strlen(sTxt));
    }
    
    // Empty the buffers and close the file
    dataFile.sync();
    dataFile.close();
  }
  else
    Serial.printf("Erreur ouverture %s !!!\n", sFileName);

  // Mémoire disponible avant la création de l'image
  /*CModeGeneric::PrintRamUsage( "Ram avant création de l'image");
  // Test si suffisamment de place
  uint32_t iSizeBMP = BMP_WIDTH * BMP_HEIGHT / 2;
  if (ramMonitor.heap_free() < iSizeBMP)
    Serial.printf("### Taille mémoire insufisante, width %d, height %d, taille BMP %d, free heap %d\n", BMP_WIDTH, BMP_HEIGHT, iSizeBMP, ramMonitor.heap_free());
  else
    Serial.printf("Taille mémoire OK, width %d, height %d, taille BMP %d, free heap %d\n", BMP_WIDTH, BMP_HEIGHT, iSizeBMP, ramMonitor.heap_free());*/
  // Creating the BMP file on the SD card
  if (bStereo)
    sprintf( sFileName, txtTestResultDD[commonParams.iLanguage], year(), month(), day(), hour(), minute(), second());
  else
    sprintf( sFileName, txtTestResultD[commonParams.iLanguage], year(), month(), day(), hour(), minute(), second());
  if (bStereo)
    sprintf( sTxt, txtTestResultAD[commonParams.iLanguage], year(), month(), day(), hour(), minute(), second(), txtTestResultC[commonParams.iLanguage][iTestResult]);
  else
    sprintf( sTxt, txtTestResultA[commonParams.iLanguage], year(), month(), day(), hour(), minute(), second(), txtTestResultC[commonParams.iLanguage][iTestResult]);
  Graphe16C *pGraphe = new Graphe16C( BMP_WIDTH, BMP_HEIGHT, COLORWHITE, -120, -55, 10, FRMINTEST, FRMAXTEST, 10, sTxt, sFileName);
  //CModeGeneric::PrintRamUsage( "Ram après création de l'image");
  pGraphe->SetGraphe( (char *)txtTestResultF[commonParams.iLanguage], COLORGREEN,  TR_CURVE, FFTLEN/2, iTbSignalCnx,  (float *)fTbCnx);
  if (paramsOperateur.iMicrophoneType == MTFG23329)
    pGraphe->SetGraphe( (char *)txtTestResultG[commonParams.iLanguage], COLORORANGE, TR_MIN,   FFTLEN/2, (int *)iTbTemplateMinFG, (float *)fTbCnx);
  else
    pGraphe->SetGraphe( (char *)txtTestResultG[commonParams.iLanguage], COLORORANGE, TR_MIN,   FFTLEN/2, (int *)iTbTemplateMinMEMS, (float *)fTbCnx);
  pGraphe->SetGraphe( (char *)txtTestResultH[commonParams.iLanguage], COLORRED,    TR_MAX,   FFTLEN/2, (int *)iTbTemplateMax, (float *)fTbCnx);
  pGraphe->SetGraphe( (char *)txtTestResultI[commonParams.iLanguage], COLORBLUE,   TR_CURVE, FFTLEN/2, iTbSilenceCnx, (float *)fTbCnx);
  pGraphe->WriteFile();
  delete pGraphe;

  if (bStereo)
  {
    // Mémoire disponible avant la création de l'image
    /*CModeGeneric::PrintRamUsage( "Ram avant création de l'image G");
    // Test si suffisamment de place
    uint32_t iSizeBMP = BMP_WIDTH * BMP_HEIGHT / 2;
    if (ramMonitor.heap_free() < iSizeBMP)
      Serial.printf("### G Taille mémoire insufisante, width %d, height %d, taille BMP %d, free heap %d\n", BMP_WIDTH, BMP_HEIGHT, iSizeBMP, ramMonitor.heap_free());
    else
      Serial.printf("G Taille mémoire OK, width %d, height %d, taille BMP %d, free heap %d\n", BMP_WIDTH, BMP_HEIGHT, iSizeBMP, ramMonitor.heap_free());*/
    // Création de l'image sur la carte SD
    sprintf( sTxt, txtTestResultAG[commonParams.iLanguage], year(), month(), day(), hour(), minute(), second(), txtTestResultC[commonParams.iLanguage][iTestResultG]);
    sprintf( sFileName, txtTestResultDG[commonParams.iLanguage], year(), month(), day(), hour(), minute(), second());
    pGraphe = new Graphe16C( BMP_WIDTH, BMP_HEIGHT, COLORWHITE, -120, -55, 10, FRMINTEST, FRMAXTEST, 10, sTxt, sFileName);
    //CModeGeneric::PrintRamUsage( "Ram après création de l'image G");
    pGraphe->SetGraphe( (char *)txtTestResultF[commonParams.iLanguage], COLORGREEN,  TR_CURVE, FFTLEN/2, iTbSignalCnxG,  (float *)fTbCnx);
    if (paramsOperateur.iMicrophoneType == MTFG23329)
      pGraphe->SetGraphe( (char *)txtTestResultG[commonParams.iLanguage], COLORORANGE, TR_MIN,   FFTLEN/2, (int *)iTbTemplateMinFG,  (float *)fTbCnx);
    else
      pGraphe->SetGraphe( (char *)txtTestResultG[commonParams.iLanguage], COLORORANGE, TR_MIN,   FFTLEN/2, (int *)iTbTemplateMinMEMS,  (float *)fTbCnx);
    pGraphe->SetGraphe( (char *)txtTestResultH[commonParams.iLanguage], COLORRED,    TR_MAX,   FFTLEN/2, (int *)iTbTemplateMax,  (float *)fTbCnx);
    pGraphe->SetGraphe( (char *)txtTestResultI[commonParams.iLanguage], COLORBLUE,   TR_CURVE, FFTLEN/2, iTbSilenceCnxG, (float *)fTbCnx);
    pGraphe->WriteFile();
    delete pGraphe;
  }

  // Switching to result mode
  iTestPhase = TEST_RESULT;
}
  
//--------------------------------------------------- ----------------------
// Affichage du mode sur l'écran
// Cette méthode est appelée régulièrement par le loop principal
// à charge des différents modes d'afficher les informations nécessaires
void CModeTestMicro::PrintMode()
{
  //Serial.println("CModeTestMicro::PrintMode");
  // Test si on doit afficher
  if (millis() > uiCurentTime)
  {
    // Temps du prochain affichage
    uiCurentTime = millis() + 300;
    switch( iTestPhase)
    {
    default:
    case TEST_LEVEL  : // Microphone test with barregraph
      PrintLevel();
      break;
    case TEST_SILENCE: // Silence microphone test
      PrintSilence();
      break;
    case TEST_SIGNAL : // Signal microphone test (keys or BatPlayer)
      PrintSignal();
      break;
    case TEST_RESULT : // Microphone test result
      PrintResult();
      break;
    }
  }
}

//-------------------------------------------------------------------------
//! \brief Affichage du mode Level
void CModeTestMicro::PrintLevel()
{
  int iNivMaxD, iNivMaxG, iFreqD, iFreqG;
  if (millis() > uiCurentTimeMax_D)
    iNivMaxTotal_D = -120;
  if (millis() > uiCurentTimeMax_G)
    iNivMaxTotal_G = -120;
  // On décrémente les barregraphes doucement
  if (iNivMaxBG_D > -120)
    iNivMaxBG_D -= 10;
  else
  {
    iNivMaxBG_D  = -120;
    iNivMaxTxt_D = -120;
  }
  if (iNivMaxBG_G > -120)
    iNivMaxBG_G -= 10;
  else
  {
    iNivMaxBG_G  = -120;
    iNivMaxTxt_G = -120;
  }

  // Récupération des infos à afficher
  if (pRecorder != NULL)
    ((CRecorder *)pRecorder)->GetNiveauMax( &iNivMaxD, &iFreqD, &iNivMaxG, &iFreqG);   
  int iMemNiv = iNivMaxBG_D;
  iNivMaxBG_D  = max(iNivMaxBG_D, iNivMaxD);
  if (iNivMaxBG_D > iMemNiv)
    iNivMaxTxt_D = iNivMaxD;
  if (iNivMaxTotal_D < iNivMaxD)
  {
    iNivMaxTotal_D = iNivMaxD;
    uiCurentTimeMax_D = millis() + 10000;
  }
  iMemNiv = iNivMaxBG_G;
  iNivMaxBG_G  = max(iNivMaxBG_G, iNivMaxG);
  if (iNivMaxBG_G > iMemNiv)
    iNivMaxTxt_G = iNivMaxG;
  if (iNivMaxTotal_G < iNivMaxG)
  {
    iNivMaxTotal_G = iNivMaxG;
    uiCurentTimeMax_G = millis() + 10000;
  }
  if (iNivMaxD > -120 and iNivMaxD > iNivMaxG)
  {
    iFreq = iFreqD;
    iNivMaxTotal = iNivMaxD;
  }
  else if (iNivMaxG > -120 and iNivMaxG > iNivMaxD)
  {
    iFreq = iFreqG;
    iNivMaxTotal = iNivMaxG;
  }
  // Mémo du niveau max rencontré lors du test
  if (iNivMaxD > -120 and iNivMaxD > iMemoNivD)
  {
    iMemoNivD = iNivMaxD;
    iMemoFreqD = iFreqD;
  }
  if (iNivMaxG > -120 and iNivMaxG > iMemoNivG)
  {
    iMemoNivG = iNivMaxG;
    iMemoFreqG = iFreqG;
  }
  
  // Affichage du mode
  char Message[30];
  // Effacement écran
  pDisplay->clearBuffer();
  pDisplay->setCursor(0,0);
  pDisplay->println(      txtTestMicroA[commonParams.iLanguage]);
  pDisplay->setCursor(0,9);
  pDisplay->println(      txtTestMicroB[commonParams.iLanguage]);
  pDisplay->setCursor(0,18);
  pDisplay->println(      txtTestMicroC[commonParams.iLanguage]);
  pDisplay->setCursor(0,27);
  pDisplay->println(      txtTestMicroD[commonParams.iLanguage]);
  pDisplay->setCursor(0,38);
  sprintf( Message,     "F.:%03dkHz Max %ddB", iFreq, iNivMaxTotal);
  pDisplay->println(      Message);
  if (GetRecorderType() >= PASSIVE_STEREO and paramsOperateur.iStereoRecMode == SMSTEREO)
  {
    // Test micro en stéréo
    pDisplay->setCursor(81,46);
    if (commonParams.iLanguage == LFRENCH)
      sprintf( Message, "D %ddB", iNivMaxTxt_D);
    else
      sprintf( Message, "R %ddB", iNivMaxTxt_D);
    pDisplay->println( Message);
    // Affichage de la barre de niveau
    LstModifParams[IDXBG_STEREOD]->SetRedraw( true);
    LstModifParams[IDXBG_STEREOD]->PrintParam( 0, 49);
    pDisplay->setCursor(81,55);
    if (commonParams.iLanguage == LFRENCH)
      sprintf( Message, "G %ddB", iNivMaxTxt_G);
    else
      sprintf( Message, "L %ddB", iNivMaxTxt_G);
    pDisplay->println( Message);
    // Affichage de la barre de niveau
    LstModifParams[IDXBG_STEREOG]->SetRedraw( true);
    LstModifParams[IDXBG_STEREOG]->PrintParam( 0, 57);
  }
  else
  {
    // Test micro en mono
    pDisplay->setCursor(83,50);
    sprintf( Message, "%ddB", iNivMaxTxt_D);
    pDisplay->println( Message);
    // Affichage de la barre de niveau
    LstModifParams[IDXBG_MONO]->SetRedraw( true);
    LstModifParams[IDXBG_MONO]->PrintParam( 0, 50);
  }
  UpdateScreen();
}

//-------------------------------------------------------------------------
//! \brief Affichage du mode Silence
void CModeTestMicro::PrintSilence()
{
  char Message[30];
  if (second() != iDecounterSecond)
  {
    iDecounter--;
    iDecounterSecond = second();
  }
  if (iDecounter == 0)
    SwitchToSignalPhase();
  // Effacement écran
  pDisplay->clearBuffer();
  // Affichage du message
  int iWidth = pDisplay->getStrWidth(txtTestSilenceA[commonParams.iLanguage]);
  pDisplay->setCursor((128-iWidth)/2,0);
  pDisplay->println(      txtTestSilenceA[commonParams.iLanguage]);
  iWidth = pDisplay->getStrWidth(txtTestSilenceB[commonParams.iLanguage]);
  pDisplay->setCursor((128-iWidth)/2,9);
  pDisplay->println(      txtTestSilenceB[commonParams.iLanguage]);
  sprintf( Message, "%d s", iDecounter);
  iWidth = pDisplay->getStrWidth(Message);
  pDisplay->setCursor((128-iWidth)/2,27);
  pDisplay->println(      Message);
  if (iDecounter < 8 and iDecounter > 3)
  {
    iWidth = pDisplay->getStrWidth(txtTestSilenceC[commonParams.iLanguage]);
    pDisplay->setCursor((128-iWidth)/2,40);
    pDisplay->println(      txtTestSilenceC[commonParams.iLanguage]);
  }
  if (iDecounter == 5 and !bStartSilence)
  {
    pRecorder->StartSilence();
    bStartSilence = true;
  }
  UpdateScreen();
}

//-------------------------------------------------------------------------
//! \brief Affichage du mode Signal
void CModeTestMicro::PrintSignal()
{
  char Message[30];
  if (second() != iDecounterSecond)
  {
    iDecounter--;
    iDecounterSecond = second();
  }
  if (iDecounter == 0)
    SwitchToResultPhase();
  // Effacement écran
  pDisplay->clearBuffer();
  // Affichage du message
  int iWidth = pDisplay->getStrWidth(txtTestSignalA[commonParams.iLanguage]);
  pDisplay->setCursor((128-iWidth)/2,0);
  pDisplay->println(      txtTestSignalA[commonParams.iLanguage]);
  iWidth = pDisplay->getStrWidth(txtTestSignalB[commonParams.iLanguage]);
  pDisplay->setCursor((128-iWidth)/2,9);
  pDisplay->println(      txtTestSignalB[commonParams.iLanguage]);
  sprintf( Message, "%d s", iDecounter);
  iWidth = pDisplay->getStrWidth(Message);
  pDisplay->setCursor((128-iWidth)/2,27);
  pDisplay->println(      Message);
  if (iDecounter < 13 and iDecounter > 3)
  {
    iWidth = pDisplay->getStrWidth(txtTestSignalC[commonParams.iLanguage]);
    if (iDecounter % 2 == 0)
      pDisplay->setCursor((128-iWidth)/2-15,40);
    else
      pDisplay->setCursor((128-iWidth)/2+15,40);
    pDisplay->println(    txtTestSignalC[commonParams.iLanguage]);
  }
  if (iDecounter == 8 and !bStartSignal)
  {
    pRecorder->StartSignal();
    bStartSignal = true;
  }
  UpdateScreen();
}

//-------------------------------------------------------------------------
//! \brief Affichage du mode Result
void CModeTestMicro::PrintResult()
{
  char sTitle[25];
  // Effacement écran
  pDisplay->clearBuffer();
  // Affichage du message à l'écran
  if (bStereo)
    // Left / Right
    sprintf( sTitle, "%s/%s", txtTestResultC[commonParams.iLanguage][iTestResultG], txtTestResultC[commonParams.iLanguage][iTestResult]);
  else
    sprintf( sTitle, "%s", txtTestResultC[commonParams.iLanguage][iTestResult]);
  int iWidth = pDisplay->getStrWidth(sTitle);
  pDisplay->setCursor((128-iWidth)/2,0);
  pDisplay->println( sTitle);
  // Affichage du graphe
  graphe.PrintGraphe();
  UpdateScreen();
}
  
//-------------------------------------------------------------------------
// Traitement des ordres claviers
// Si la touche est une touche de changement de mode, retourne le mode demandé
// Cette méthode est appelée régulièrement par le loop principal
// à charge des différents modes de traiter les actions opérateurs
int CModeTestMicro::KeyManager(
  unsigned short key  // Touche sollicitée par l'opérateur
  )
{
  int newMode = NOMODE;
  // Gestion éventuelle des touches
  if (key == K_UP and iTestPhase == TEST_LEVEL and F_CPU >= 144000000)
    // Passage en mode test complet du micro
    SwitchToSilencePhase();
  else if (key == K_UP and iTestPhase == TEST_RESULT and F_CPU >= 144000000)
    // On recommence un test micro
    newMode = MRESTARTTEST;
  else if (key != K_NO)
    // Sur n'importe quelle touche, on repasse en mode paramètres
    newMode = MPARAMS;
  return newMode;
}

//-------------------------------------------------------------------------
// Management class of a graph from 0 to 128kHz (1 pixel per kHz) on 55 lines
// 14 pixels for the scale and 40 pixels for the graph

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CGrapheTestMicro::CGrapheTestMicro(
  int y         // Position y from the top of the graph
  )
{
  // Set parameters
  iY       = y;
  iNbLines = 40;
  iFrMin   = 10;
  iFrMax   = 120;
  // Create and clear graphe bitmap
  iSizeGraph = 16*iNbLines;
  pTbGraphe = new uint8_t[iSizeGraph];
  memset( pTbGraphe, 0, iSizeGraph);
}

//-------------------------------------------------------------------------
// Destructor
CGrapheTestMicro::~CGrapheTestMicro()
{
  delete [] pTbGraphe;
}

//-------------------------------------------------------------------------
// Initialization of a point in the graph
void CGrapheTestMicro::SetFrequence(
  int iLevel, // Level in dB
  int iFreq   // Frequency in kHz
  )
{
  uint8_t tbBits[] = {0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01};
  if (iFreq >= 20 and iFreq < 128)
  {
    // Level index, -60 to -120 (60dB for 40 pixels)
    // dB index
    int iNbDB = -60 - iLevel;
    float yDB = (-60.0 + 120.0) / 40.0;
    float fDB = (float)iNbDB / yDB;
    int iDB = (int)fDB;
    if (iDB >= 40)
      iDB = 39;
    // Fr index
    int iFr = iFreq / 8;
    // Bit index
    int iBit = iFreq - (iFr*8);
    // Index bitmap
    int idx = iDB * 16 + iFr;
    // Set point
    pTbGraphe[idx] |= tbBits[iBit];
    //Serial.printf("SetFrequence %ddB, %dkHz, iNbDB %d, yDB %f, fDB %f, iDB %d, iFR %d, iBit %d, idx %d, iSizeGraph %d, iNbLines %d\n", iLevel, iFreq, iNbDB, yDB, fDB, iDB, iFr, iBit, idx, iSizeGraph, iNbLines);
  }
}

//-------------------------------------------------------------------------
// Print graphe
void CGrapheTestMicro::PrintGraphe()
{
  // Scale preparation
  char sTemp[10];
  int x, y;
  pDisplay->setFont(u8g2_font_4x6_mf);
  // Display value every 20kHz
  for (int i=20; i<125; i+=20)
  {
    sprintf( sTemp, "%d", i);
    x = i;
    int iW = pDisplay->getStrWidth( sTemp);
    // Display frequency
    pDisplay->setCursor( x-(iW/2), iY);
    pDisplay->print( sTemp);
    // Display of the frequency mark
    pDisplay->drawLine( x, iY+6, x, iY+8);
  }
  // Horizontal line display
  pDisplay->drawLine( iFrMin, iY+9, iFrMax, iY+9);

  // Bitmap display
  pDisplay->drawBitmap( 0, iY+10, 16, iNbLines, pTbGraphe);
  // Display dB scale (-40 -60 -80 -90, plage de -40 à -120 soit 60dB pour 40 pixels)
  y = iY + 10;
  pDisplay->setCursor( 0, y);
  pDisplay->print( "-60");
  y += 6;
  pDisplay->setCursor( 0, y);
  pDisplay->print( "-70");
  y += 7;
  pDisplay->setCursor( 0, y);
  pDisplay->print( "-80");
  y += 6;
  pDisplay->setCursor( 0, y);
  pDisplay->print( "-90");
  y += 7;
  pDisplay->setCursor( 0, y);
  pDisplay->print( "-100");
  y += 6;
  pDisplay->setCursor( 0, y);
  pDisplay->print( "-110");
  // Vertical line display
  pDisplay->drawLine( 12, iY + 10, 12, 63);
  
  pDisplay->setFont(u8g2_font_6x10_mf);
}
