/* 
 * File:   ConstChar.c
   Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.
   Many thanks to Edwin Houwertjes for the translations in German and Dutch.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "ModesModifiersConfig.h"

//------------------------------------------------------
// Strings used in the software to easily add a language
//------------------------------------------------------

/*
  To add a language, you must:
  - In ModModifier library, add the language to the LANGUAGE enum at the start of the ModesModifiersConfig.h file
  - In ModModifier library, add to each string of the ConstChar.c file the translation of the message in the new language
  - Add to each string of the this ConstChar.c file the translation of the message in the new language
  Order of languages :
  LENGLISH    = 0
  LFRENCH     = 1
  LGERMAN     = 2
  LDUTCH      = 3
  
  244 sentences to translate.
*/

//***********************************************************************************************
// WARNING, Pay attention to the formats of the variables inside the sentences (% d,% 04d,% f,% s ...).
//          They must be left in the right place for translation. The comment above shows the full sentence in English.
// WARNING, Messages intended for the screen, the font is poor in characters with accents
//          Messages for the Log file (last part of this file), no problem with accents
// WARNING, on the screen the size of the strings is important. Keep the same size as in English
//***********************************************************************************************

//------------------------------------------------------
// Parameters screen
//------------------------------------------------------

//------------------------------------------------------
// CModeGeneric.cpp
// Standby and wake up screen
//                                 12345678901234567890
const char *txtVeilleA[]       = {"Standby in:"  ,
                                  "Veille dans:" ,
                                  "Standby in:"  ,
                                  "Standby in:"} ;
const char *txtVeilleB[]       = {"%02d seconds"  ,
                                  "%02d secondes" ,
                                  "%02d seconds"  ,
                                  "%02d seconds"} ; 
const char *txtVeilleC[]       = {"Wake up at %s" ,
                                  "Reveil a %s"   ,
                                  "Wake up at %s" ,
                                  "Wake up at %s"};
const char *txtVeilleD[]       = {"Click to switch to"  ,
                                  "Clic pour passer en" ,
                                  "Click to switch to"  ,
                                  "Click to switch to"} ;
const char *txtVeilleE[]       = {"settings mode"   ,
                                  "mode parametres" ,
                                  "settings mode"   ,
                                  "settings mode"}  ;
// Default names of profiles                         RhinoLogger    RhinoLogger    RhinoLogger    RhinoLogger    RhinoLogger
const char *txtDefProfilesNames[MAXLANGUAGE][5] = {{"Default"    , "Bats 384kHz", "Bats 500kHz", "RhinoLogger", "Audio 48kHz"},
                                                   {"Defaut"     , "Bats 384kHz", "Bats 500kHz", "RhinoLogger", "Audio 48kHz"},
                                                   {"Default"    , "Bats 384kHz", "Bats 500kHz", "RhinoLogger", "Audio 48kHz"},
                                                   {"Default"    , "Bats 384kHz", "Bats 500kHz", "RhinoLogger", "Audio 48kHz"}};
// CModeGeneric.cpp CModeTestSD
//                                 123456789012345678901
// SD status line                  [SD 128GO 99% 0022f.]
const char *txtIDSD[]          = {"[SD %3.0fGO %d%% %04df.]" ,
                                  "[SD %3.0fGO %d%% %04df.]" ,
                                  "[SD %3.0fGO %d%% %04df.]" ,
                                  "[SD %3.0fGO %d%% %04df.]"};
// SD format line                  [Formatting SD  No ]
const char *txtIDXFORMAT[]     = {"[Formatting SD %s]" ,
                                  "[Formatage  SD %s]" ,
                                  "[Formatting SD %s]" ,
                                  "[Formatting SD %s]"};
// Yes, No and "wait" textes for formatting
const char *txtFYesNo[MAXLANGUAGE][5]    = {{" No ", "Yes ", "..  ", " .. ", "  .."} ,
                                            {"Non ", "Oui ", "..  ", " .. ", "  .."} ,
                                            {"Nein", " Ja ", "..  ", " .. ", "  .."} ,
                                            {"Nee ", " Ja ", "..  ", " .. ", "  .."}};
// Return to parameters line
const char *txtIDXSDRET[]      = {"[Go back to params]" ,
                                  "[Retour parametres]" ,
                                  "[Go back to params]" ,
                                  "[Go back to params]"};
// SD type line                    [SD 128 GO FAT32]
const char *txtIDXSDTST[]      = {"[SD % 3dGO FAT%d]" ,
                                  "[SD % 3dGO FAT%d]" ,
                                  "[SD % 3dGO FAT%d]" ,
                                  "[SD % 3dGO FAT%d]"};
// Reste after SD formating
const char *txtRestFormating[] = {"Reset after formating" ,
                                  "Reset apres formatage" ,
                                  "Reset after formating" ,
                                  "Reset after formating"};


//------------------------------------------------------
// CModeTestMicro.cpp                123456789012345678901
const char *txtTestMicroA[]      = {"Microphone test" ,
                                    "Test micro"      ,
                                    "Microphone test" ,
                                    "Microphone test"};
const char *txtTestMicroB[]      = {"Rub your fingers in" ,
                                    "Frottez vos doigts"  ,
                                    "Rub your fingers in" ,
                                    "Rub your fingers in"};
const char *txtTestMicroC[]      = {"front of the micro." ,
                                    "devant le micro"     ,
                                    "front of the micro." ,
                                    "front of the micro."};
const char *txtTestMicroD[]      = {"Expected lev. >-90dB" ,
                                    "Niv. attendu > -90dB" ,
                                    "Expected lev. >-90dB" ,
                                    "Expected lev. >-90dB"};
const char *txtTestSilenceA[]    = {"Silence for"     ,
                                    "Silence pendant" ,
                                    "Silence for"     ,
                                    "Silence for"}    ;
const char *txtTestSilenceB[]    = {"10s..." ,
                                    "10s..." ,
                                    "10s..." ,
                                    "10s..."};
const char *txtTestSilenceC[]    = {"Hush!"    ,
                                    "Silence!" ,
                                    "Hush!"    ,
                                    "Hush!"}   ;
const char *txtTestSignalA[]     = {"Move a bunch of" ,
                                    "Bougez des cles" ,
                                    "Move a bunch of" ,
                                    "Move a bunch of"};
const char *txtTestSignalB[]     = {"keys at 20cm for 15s" ,
                                    "pendant 15s a 20cm"   ,
                                    "keys at 20cm for 15s" ,
                                    "keys at 20cm for 15s"};
const char *txtTestSignalC[]     = {"Click" ,
                                    "Clic"  ,
                                    "Click" ,
                                    "Click"};
// Strings with Date Hour            Microphone test 26/06/21 15:26:32
const char *txtTestResultA[]     = {"Microphone test %02d/%02d/%02d %02d:%02d:%02d %s" ,
                                    "Test micro %02d/%02d/%02d %02d:%02d:%02d %s"      ,
                                    "Microphone test %02d/%02d/%02d %02d:%02d:%02d %s" ,
                                    "Microphone test %02d/%02d/%02d %02d:%02d:%02d %s"};
const char *txtTestResultAD[]    = {"Right microphone test %02d/%02d/%02d %02d:%02d:%02d %s" ,
                                    "Test micro droit %02d/%02d/%02d %02d:%02d:%02d %s"      ,
                                    "Right microphone test %02d/%02d/%02d %02d:%02d:%02d %s" ,
                                    "Right microphone test %02d/%02d/%02d %02d:%02d:%02d %s"};
const char *txtTestResultAG[]    = {"Left microphone test %02d/%02d/%02d %02d:%02d:%02d %s" ,
                                    "Test micro gauche %02d/%02d/%02d %02d:%02d:%02d %s"    ,
                                    "Left microphone test %02d/%02d/%02d %02d:%02d:%02d %s" ,
                                    "Left microphone test %02d/%02d/%02d %02d:%02d:%02d %s"};
const char *txtTestResultAA[]    = {"Microphone test\t%02d/%02d/%02d\t%02d:%02d:%02d\t%s" ,
                                    "Test micro\t%02d/%02d/%02d\t%02d:%02d:%02d\t%s"      ,
                                    "Microphone test\t%02d/%02d/%02d\t%02d:%02d:%02d\t%s" ,
                                    "Microphone test\t%02d/%02d/%02d\t%02d:%02d:%02d\t%s"};
const char *txtTestResultAAD[]   = {"Right microphone test\t%02d/%02d/%02d\t%02d:%02d:%02d\t%s" ,
                                    "Test micro droit\t%02d/%02d/%02d\t%02d:%02d:%02d\t%s"      ,
                                    "Right microphone test\t%02d/%02d/%02d\t%02d:%02d:%02d\t%s" ,
                                    "Right microphone test\t%02d/%02d/%02d\t%02d:%02d:%02d\t%s"};
const char *txtTestResultAAG[]   = {"Left microphone test\t%02d/%02d/%02d\t%02d:%02d:%02d\t%s" ,
                                    "Test micro gauche\t%02d/%02d/%02d\t%02d:%02d:%02d\t%s"    ,
                                    "Left microphone test\t%02d/%02d/%02d\t%02d:%02d:%02d\t%s" ,
                                    "Left microphone test\t%02d/%02d/%02d\t%02d:%02d:%02d\t%s"};
const char *txtTestResultD[]     = {"MicrophoneTest%02d%02d%02d%02d%02d%02d" ,
                                    "MicrophoneTest%02d%02d%02d%02d%02d%02d"};
const char *txtTestResultDD[]    = {"RightMicrophoneTest%02d%02d%02d%02d%02d%02d" ,
                                    "RightMicrophoneTest%02d%02d%02d%02d%02d%02d"};
const char *txtTestResultDG[]    = {"LeftMicrophoneTest%02d%02d%02d%02d%02d%02d" ,
                                    "LeftMicrophoneTest%02d%02d%02d%02d%02d%02d"};
// Result test for csv file ou bmp file
const char *txtTestResultF[]     = {"Signal" ,
                                    "Signal" ,
                                    "Signal" ,
                                    "Signal"};
const char *txtTestResultG[]     = {"Min template" ,
                                    "Gabarit min"  ,
                                    "Min template" ,
                                    "Min template"};
const char *txtTestResultH[]     = {"Max template" ,
                                    "Gabarit max"  ,
                                    "Max template" ,
                                    "Max template"};
const char *txtTestResultI[]     = {"Silence" ,
                                    "Silence" ,
                                    "Silence" ,
                                    "Silence"};
const char *txtTestResultB[MAXLANGUAGE][5] = {{"Channels (kHz)", "Signal (dB)", "Min template", "Max template", "Silence (dB)"} ,
                                              {"Canaux (kHz)"  , "Signal (dB)", "Gabarit min" , "Gabarit max" , "Silence (dB)"} ,
                                              {"Channels (kHz)", "Signal (dB)", "Min template", "Max template", "Silence (dB)"} ,
                                              {"Channels (kHz)", "Signal (dB)", "Min template", "Max template", "Silence (dB)"}};
// Sentences for log
const char *txtLogTestResultE[]  = {"Microphone: %s" ,
                                    "Micro: %s"      ,
                                    "Microphone: %s" ,
                                    "Microphone: %s"};
const char *txtLogTestResultES[] = {"Microphone: R %s, L %s" ,
                                    "Micro: D %s, G %s"      ,
                                    "Microphone: R %s, L %s" ,
                                    "Microphone: R %s, L %s"};
const char *txtLogNivMono[]      = {"Max level : %ddB %03dkHz"  ,
                                    "Niveau max : %ddB %03dkHz" ,
                                    "Max level : %ddB %03dkHz"  ,
                                    "Max level : %ddB %03dkHz"} ;
const char *txtLogNivStereo[]    = {"Max level : R %ddB %03dkHz, L %ddB %03dkHz"  ,
                                    "Niveau max : D %ddB %03dkHz, G %ddB %03dkHz" ,
                                    "Max level : R %ddB %03dkHz, L %ddB %03dkHz"  ,
                                    "Max level : R %ddB %03dkHz, L %ddB %03dkHz"} ;
//                                              123456789012345678901
// Microphone test result            In stereo "Excellent/Excellent"
const char *txtTestResultC[MAXLANGUAGE][6] = {{"Excellent", "Good", "Weak"  , "Noisy"  , "Broken"  , "Unknow" } ,
                                              {"Excellent", "Bon" , "Faible", "Bruyant", "En panne", "Inconnu"} ,
                                              {"Excellent", "Good", "Weak"  , "Noisy"  , "Broken"  , "Unknow" } ,
                                              {"Excellent", "Good", "Weak"  , "Noisy"  , "Broken"  , "Unknow" }};

//------------------------------------------------------
// CModeParams.cpp, strings for parameters. Do not change the size of the strings
const char *txtOuiNon[MAXLANGUAGE][2]    = {{" No" , "Yes"} ,
                                            {"Non" , "Oui"} ,
                                            {"Nein", "Ja" } ,
                                            {"Nee" , "Ja" }};
const char *txtPPPNon[MAXLANGUAGE][2]    = {{" No" , "..."} ,
                                            {"Non" , "..."} ,
                                            {"Nein", "..." } ,
                                            {"Nee" , "..." }};
const char *txtYesNo[MAXLANGUAGE][2]     = {{"N", "Y"} ,
                                            {"N", "O"} ,
                                            {"N", "J"} ,
                                            {"N", "J"}};
const char *txtProtocoles[MAXLANGUAGE][10]={{"Auto record    ", "Walkin. Protoc.", "Road Protocol  ", "Fixed P. Proto.", "RhinoLogger    ", "Microphone test", "Heterodyne     ", "Timed recording", "Audio Rec.     ", "Synchro Rec.   "} ,
                                            {"Enreg. Auto.   ", "Proto. Pedestr.", "Proto. Routier ", "Proto. P. Fixe ", "RhinoLogger    ", "Test Micro     ", "Heterodyne     ", "Enregi. Cadence", "Enreg. Audio   ", "Enreg. Synchro."} ,
                                            {"Auto record    ", "Walkin. Protoc.", "Road Protocol  ", "Fixed P. Proto.", "RhinoLogger    ", "Microphone test", "Heterodyne     ", "Timed recording", "Audio Rec.     ", "Synchro Rec.   "} ,
                                            {"Auto record    ", "Walkin. Protoc.", "Road Protocol  ", "Fixed P. Proto.", "RhinoLogger    ", "Microphone test", "Heterodyne     ", "Timed recording", "Audio Rec.     ", "Synchro Rec.   "}};
const char *txtLED[MAXLANGUAGE][8]       = {{"No       ", "Recordin.", "Acquisit.", "IS DMA   ", "Tr. Loop ", "Cal Noise", "Det. Act.", "Buf. Los."} ,
                                            {"Non      ", "Enregist.", "Acquisit.", "IT DMA   ", "Tr. Loop ", "Cal Bruit", "Det. Act.", "Pert. Buf"} ,
                                            {"No       ", "Recordin.", "Acquisit.", "IS DMA   ", "Tr. Loop ", "Cal Noise", "Det. Act.", "Buf. Los."} ,
                                            {"No       ", "Recordin.", "Acquisit.", "IS DMA   ", "Tr. Loop ", "Cal Noise", "Det. Act.", "Buf. Los."}};
const char *txtStereoMode[MAXLANGUAGE][3]= {{"Stereo     ", "Mono Right ", "Mono Left  "} ,
                                            {"Stereo     ", "Mono Droit ", "Mono Gauche"} ,
                                            {"Stereo     ", "Mono Right ", "Mono Left  "} ,
                                            {"Stereo     ", "Mono Right ", "Mono Left  "}};
const char *txtMasterSlave[MAXLANGUAGE][10]={{"Master   ", "Slave 1  ", "Slave 2  ", "Slave 3  ", "Slave 4  ", "Slave 5  ", "Slave 6  ", "Slave 7  ", "Slave 8  ", "Slave 9  "} ,
                                             {"Maitre   ", "Esclave 1", "Esclave 2", "Esclave 3", "Esclave 4", "Esclave 5", "Esclave 6", "Esclave 7", "Esclave 8", "Esclave 9"} ,
                                             {"Master   ", "Slave 1  ", "Slave 2  ", "Slave 3  ", "Slave 4  ", "Slave 5  ", "Slave 6  ", "Slave 7  ", "Slave 8  ", "Slave 9  "} ,
                                             {"Master   ", "Slave 1  ", "Slave 2  ", "Slave 3  ", "Slave 4  ", "Slave 5  ", "Slave 6  ", "Slave 7  ", "Slave 8  ", "Slave 9  "}};
const char *txtLanguage[MAXLANGUAGE][MAXLANGUAGE] = {{"English", "Francais", "Deutsch", "Nederlands",} ,
                                                     {"English", "Francais", "Deutsch", "Nederlands",} ,
                                                     {"English", "Francais", "Deutsch", "Nederlands",} ,
                                                     {"English", "Francais", "Deutsch", "Nederlands",}};
const char *txtLevelUser[MAXLANGUAGE][3]  ={{"Beginner", " Expert ", " Debug  "} ,
                                            {"Debutant", " Expert ", " Debug  "} ,
                                            {"Beginner", " Expert ", " Debug  "} ,
                                            {"Beginner", " Expert ", " Debug  "}};
const char *txtModeSeuil[MAXLANGUAGE][2] = {{"relative", "absolute"} ,
                                            {"relatif ", "absolu  "} ,
                                            {"relative", "absolute"} ,
                                            {"relative", "absolute"}};
const char *txtModeHetRec[MAXLANGUAGE][2] = {{"Manual", " Auto "} ,
                                             {"Manuel", " Auto "} ,
                                             {"Manual", " Auto "} ,
                                             {"Manual", " Auto "}};
const char *txtLEDSynch[MAXLANGUAGE][3] = {{" No     ", " Record ", "3 Record"} ,
                                           {" Non    ", " Enreg. ", "3 Enreg."} ,
                                           {" No     ", " Record ", "3 Record"} ,
                                           {" No     ", " Record ", "3 Record"}};
const char *txtIDXMODER[]      = {" Go back to record..." ,
                                  " Retour enregistre..." ,
                                  " Go back to record..." ,
                                  " Go back to record..."};
const char *txtIDXMODEF[]      = {" Mode %s" ,
                                  " Mode %s" ,
                                  " Mode %s" ,
                                  " Mode %s"};
const char *txtIDXULEV[]       = {" User level %s" ,
                                  " Niv. Util. %s" ,
                                  " User level %s" ,
                                  " User level %s"};
const char *txtIDXBAT[]        = {" Int. Batt. %s" ,
                                  " Batt. Int. %s" ,
                                  " Int. Batt. %s" ,
                                  " Int. Batt. %s"};
const char *txtIDXEXT[]        = {" Ext. Batt. %s" ,
                                  " Batt. Ext. %s" ,
                                  " Ext. Batt. %s" ,
                                  " Ext. Batt. %s"};
const char *txtIDXSD[]         = {" SD %3.0fGO %d%% %04dwav" ,
                                  " SD %3.0fGO %d%% %04dwav" ,
                                  " SD %3.0fGO %d%% %04dwav" ,
                                  " SD %3.0fGO %d%% %04dwav"};
const char *txtIDXLUM[]        = {" Min light    %s " ,
                                  " Lumiere min. %s " ,
                                  " Min light    %s " ,
                                  " Min light    %s "};
const char *txtIDXAGC[]        = {"[AGC %s]" ,
                                  "[CAG %s]" ,
                                  "[AGC %s]" ,
                                  "[AGC %s]"};
const char *txtIDXBLUT[]       = {" Bluetooth    %s " ,
                                  " Bluetooth    %s " ,
                                  " Bluetooth    %s " ,
                                  " Bluetooth    %s "};
const char *txtIDXINITBL[]     = {" Init. Bluetooth %s " ,
                                  " Bluetooth init. %s " ,
                                  " Init. Bluetooth %s " ,
                                  " Init. Bluetooth %s "};
const char *txtIDXDATE[]       = {" Date  %s " ,
                                  " Date  %s " ,
                                  " Datum %s " ,
                                  " Datum %s "};
const char *txtIDXHEURE[]      = {" Hour  %s" ,
                                  " Heure %s" ,
                                  " Uhr   %s" ,
                                  " Tijd  %s"};
                                // 01234567890123456789
const char *txtIDXPRETRIG[]    = {" Pre-Trigger   %02d s" ,
                                  " Pre-Trigger   %02d s" ,
                                  " Pre-Trigger   %02d s" ,
                                  " Pre-Trigger   %02d s"};
const char *txtIDXTSEUIL[]     = {" Threshold %s" ,
                                  " Seuil     %s" ,
                                  " Threshold %s" ,
                                  " Threshold %s"};
const char *txtIDXSEUILR[]     = {" Rel. threshold %02ddB" ,
                                  " Seuil relatif  %02ddB" ,
                                  " Rel. threshold %02ddB" ,
                                  " Rel. threshold %02ddB"};
const char *txtIDXSEUILA[]     = {" Abs. Threshold%04ddB" ,
                                  " Seuil absolu  %04ddB" ,
                                  " Abs. Threshold%04ddB" ,
                                  " Abs. Threshold%04ddB"};
const char *txtIDXTHRECM[]     = {" Het. Record %s" ,
                                  " Enr. Heter. %s" ,
                                  " Het. Record %s" ,
                                  " Het. Record %s"};
const char *txtIDXHGRAPH[]     = {" Het. Graph  %s" ,
                                  " Graphe Het. %s" ,
                                  " Het. Graph  %s" ,
                                  " Het. Graph  %s"};
const char *txtIDXAUTOPLAY[]   = {" Het. auto play  %s" ,
                                  " Het. auto lect. %s" ,
                                  " Het. auto play  %s" ,
                                  " Het. auto play  %s"};
const char *txtIDXHETAGC[]     = {" Het. A.G.C. %s" ,
                                  " Het. C.A.G. %s" ,
                                  " Het. A.G.C. %s" ,
                                  " Het. A.G.C. %s"};
const char *txtIDXTEMP[]       = {" Probe " ,
                                  " Sonde " ,
                                  " Probe " ,
                                  " Probe "};
const char *txtIDXHDEB[]       = {" Start time  %s " ,
                                  " Heure debut %s " ,
                                  " Start time  %s " ,
                                  " Start time  %s "};
const char *txtIDXHFIN[]       = {" Stop time   %s " ,
                                  " Heure fin   %s " ,
                                  " Stop time   %s " ,
                                  " Stop time   %s "};
const char *txtIDXFMIN[]       = {" Min frequenc. %3dkHz" ,
                                  " Frequence min %3dkHz" ,
                                  " Min frequenc. %3dkHz" ,
                                  " Min frequenc. %3dkHz"};
const char *txtIDXFMAX[]       = {" Max frequenc. %3dkHz" ,
                                  " Frequence max %3dkHz" ,
                                  " Max frequenc. %3dkHz" ,
                                  " Max frequenc. %3dkHz"};
const char *txtIDXFMINA[]      = {" Min frequen. %04.1fkHz" ,
                                  " Frequen. min %04.1fkHz" ,
                                  " Min frequen. %04.1fkHz" ,
                                  " Min frequen. %04.1fkHz"};
const char *txtIDXFMAXA[]      = {" Max frequen. %04.1fkHz" ,
                                  " Frequen. max %04.1fkHz" ,
                                  " Max frequen. %04.1fkHz" ,
                                  " Max frequen. %04.1fkHz"};
const char *txtIDXDMIN[]       = {" Min duration  %02d s " ,
                                  " Duree min     %02d s " ,
                                  " Min duration  %02d s " ,
                                  " Min duration  %02d s "};
const char *txtIDXDMAX[]       = {" Max duration %03d s " ,
                                  " Duree max    %03d s " ,
                                  " Max duration %03d s " ,
                                  " Max duration %03d s "};
const char *txtIDXFECH[]       = {" Sample rate   %skHz" ,
                                  " Freq. Echant. %skHz" ,
                                  " Sample rate   %skHz" ,
                                  " Sample rate   %skHz"};
const char *txtIDXGNUM[]       = {" Digital gain   +%sdB",
                                  " Gain numerique +%sdB",
                                  " Digital gain   +%sdB",
                                  " Digital gain   +%sdB"};
const char *txtIDXFILTL[]      = {" Linear filter   %s" ,
                                  " Filtre lineaire %s" ,
                                  " Linear filter   %s" ,
                                  " Linear filter   %s"};
const char *txtIDXFILT192[]    = {" 90kHz LP filter %s" ,
                                  " Filtre PB 90kHz %s" ,
                                  " 90kHz LP filter %s" ,
                                  " 90kHz LP filter %s"};
const char *txtIDXFILT96[]     = {" 45kHz LP filter %s" ,
                                  " Filtre PB 45kHz %s" ,
                                  " 45kHz LP filter %s" ,
                                  " 45kHz LP filter %s"};
const char *txtIDXFILT48[]     = {" 20kHz LP filter %s" ,
                                  " Filtre PB 20kHz %s" ,
                                  " 20kHz LP filter %s" ,
                                  " 20kHz LP filter %s"};
const char *txtIDXFILT24[]     = {" 11kHz LP filter %s" ,
                                  " Filtre PB 11kHz %s" ,
                                  " 11kHz LP filter %s" ,
                                  " 11kHz LP filter %s"};
const char *txtIDXHPFILT[]     = {" High p. filter %02dkHz" ,
                                  " Filtre pa.haut %02dkHz" ,
                                  " High p. filter %02dkHz" ,
                                  " High p. filter %02dkHz"};
//                                 123456789012345678901
//                                  High p.filt. 22.1kHz
const char *txtIDXFHPFILT[]    = {" High p.filt. %04.1fkHz" ,
                                  " Filt. p.haut %04.1fkHz" ,
                                  " High p.filt. %04.1fkHz" ,
                                  " High p.filt. %04.1fkHz"};
const char *txtIDXEXP[]        = {" T. Expansion X10 %s" ,
                                  " Expansion t. X10 %s" ,
                                  " T. Expansion X10 %s" ,
                                  " T. Expansion X10 %s"};
const char *txtIDXPREF[]       = {" Prefix  wav [%s]" ,
                                  " Prefixe wav [%s]" ,
                                  " Prefix  wav [%s]" ,
                                  " Prefix  wav [%s]"};
const char *txtIDXNDET[]       = {" Nb. Detections %d/8 " ,
                                  " Nb. Detections %d/8 " ,
                                  " Nb. Detections %d/8 " ,
                                  " Nb. Detections %d/8 "};
//                                 123456789012345678901
const char *txtIDXCORBAT[]     = {" Battery measure %3.1fV" ,
                                  " Mesure batterie %3.1fV" ,
                                  " Battery measure %3.1fV" ,
                                  " Battery measure %3.1fV"};
const char *txtIDXCOREXT[]     = {" Exter. measure %04.1fV" ,
                                  " Mesure externe %04.1fV" ,
                                  " Exter. measure %04.1fV" ,
                                  " Exter. measure %04.1fV"};
const char *txtIDXCORBATX[]    = {" Battery fix      %s" ,
                                  " Correc. Batterie %s" ,
                                  " Battery fix      %s" ,
                                  " Battery fix      %s"};
const char *txtIDXCOREXTX[]    = {" Exter. power fix %s" ,
                                  " Correc. Al. Ext. %s" ,
                                  " Exter. power fix %s" ,
                                  " Exter. power fix %s"};
const char *txtIDXLANG[]       = {" Language: %s" ,
                                  " Langue :  %s" ,
                                  " Sprache:  %s" ,
                                  " Taal:     %s"};
const char *txtIDXLED[]        = {" LED use   %s" ,
                                  " Util. LED %s" ,
                                  " LED use   %s" ,
                                  " LED use   %s"};
const char *txtIDXDEF[]        = {" Default settings %s" ,
                                  " Para. par defaut %s" ,
                                  " Default settings %s" ,
                                  " Default settings %s"};
const char *txtIDXBCOR[]       = {" Batcorder file %s" ,
                                  " Fic. Batcorder %s" ,
                                  " Batcorder file %s" ,
                                  " Batcorder file %s"};
const char *txtLogStartPar[]   = {"Starting parameters" ,
                                  "Parametres en cours" ,
                                  "Starting parameters" ,
                                  "Starting parameters"};
const char *txtLogSDPWait[]    = {"SD card reading..."  ,
                                  "Lecture carte SD..." ,
                                  "SD card reading..."  ,
                                  "SD card reading..."} ;
const char *txtIDXPTEM[]       = {" Period  T/H %04d s" ,
                                  " Periode T/H %04d s" ,
                                  " Period  T/H %04d s" ,
                                  " Period  T/H %04d s"};
const char *txtIDXBDRL[]       = {" RhinoLogger bands " ,
                                  " Bandes RhinoLogger" ,
                                  " RhinoLogger bands " ,
                                  " RhinoLogger bands "};
const char *txtIDXTSTSD[]      = {" Test SD card " ,
                                  " Test carte SD" ,
                                  " Test SD card " ,
                                  " Test SD card "};
const char *txtIDXPROFILE[]    = {" Profiles modif.   " ,
                                  " Modif. des profils" ,
                                  " Profiles modif.   " ,
                                  " Profiles modif.   "};
const char *txtIDXTHV[]        = {" Temp/Hu in standby %s" ,
                                  " Temp/Hum en veille %s" ,
                                  " Temp/Hu in standby %s" ,
                                  " Temp/Hu in standby %s"};
const char *txtIDXSNOISE[]     = {" Noise file    %s" ,
                                  " Fichier bruit %s" ,
                                  " Noise file    %s" ,
                                  " Noise file    %s"};
const char *txtIDXRLPERM[]     = {" Display RL Perma %s" ,
                                  " Aff RL Permanent %s" ,
                                  " Display RL Perma %s" ,
                                  " Display RL Perma %s"};
const char *txtIDXBOOT[]       = {" Bootloader mode %s" ,
                                  " Bootloader mode %s" ,
                                  " Bootloader mode %s" ,
                                  " Bootloader mode %s"};
const char *txtIDXRAFGRAPH[]   = {" Refresh graph  %3.1fs" ,
                                  " Periode graphe %3.1fs" ,
                                  " Refresh graph  %3.1fs" ,
                                  " Refresh graph  %3.1fs"};
const char *txtIDXLEVELH[]     = {" H signal level %3.1fs" ,
                                  " Niv Signal Het %3.1fs" ,
                                  " H signal level %3.1fs" ,
                                  " H signal level %3.1fs"};
const char *txtIDXPROFIL[]     = {" Profile %s" ,
                                  " Profil  %s" ,
                                  " Profile %s" ,
                                  " Profile %s"};
//                                 123456789012345678901
//                                  Rec. time  02:00:00
const char *txtIDXRECON[]      = {" Rec. time  %s" ,
                                  " Temps Enr. %s" ,
                                  " Rec. time  %s" ,
                                  " Rec. time  %s"};
const char *txtIDXRECOFF[]     = {" Wait. time %s" ,
                                  " Temps Att. %s" ,
                                  " Wait. time %s" ,
                                  " Wait. time %s"};
const char *txtIDXSMODE[]      = {" Record   %s" ,
                                  " Enregis. %s" ,
                                  " Record   %s" ,
                                  " Record   %s"};
const char *txtIDXCOPYR[]      = {" Copyright ..." ,
                                  " Copyright ..." ,
                                  " Copyright ..." ,
                                  " Copyright ..."};
const char *txtIDXMICT[]       = {" Mic. type %s" ,
                                  " Type mic. %s" ,
                                  " Mic. type %s" ,
                                  " Mic. type %s"};
const char *txtIDXMSLAVE[]     = {" %s" ,
                                  " %s" ,
                                  " %s" ,
                                  " %s"};
const char *txtIDXFRTOP[]      = {" Top Frequency %02dkHz" ,
                                  " Frequence Top %02dkHz" ,
                                  " Top Frequency %02dkHz" ,
                                  " Top Frequency %02dkHz"};
const char *txtIDXTOPDUR[]     = {" Top Dur.   %s" ,
                                  " Duree Top  %s" ,
                                  " Top Dur.   %s" ,
                                  " Top Dur.   %s"};
const char *txtIDXTOPPER[]     = {" Top Period    %02ds" ,
                                  " Periode Top   %02ds" ,
                                  " Top Period    %02ds" ,
                                  " Top Period    %02ds"};
//                                 123456789012345678901
const char *txtIDXLEDS[]       = {" LED Synch %s" ,
                                  " LED Synch %s" ,
                                  " LED Synch %s" ,
                                  " LED Synch %s"};
// CModeModifProfiles. Do not change the size of the strings
const char *txtIDXMPRETURN[]   = {" Return to params..."  ,
                                  " Retour parametres..." ,
                                  " Return to params..."  ,
                                  " Return to params..."} ;
const char *txtIDXMPWRITE[]    = {" Write Profiles.ini " ,
                                  " Ecrit. Profiles.ini" ,
                                  " Write Profiles.ini " ,
                                  " Write Profiles.ini "};
const char *txtIDXMPREAD[]     = {" Read Profiles files" ,
                                  " Lect. fic. Profiles" ,
                                  " Read Profiles files" ,
                                  " Read Profiles files"};
const char *txtIDXMPPROF1[]    = {" Profi. 1 %s" ,
                                  " Profil 1 %s" ,
                                  " Profi. 1 %s" ,
                                  " Profi. 1 %s"};
const char *txtIDXMPPROF2[]    = {" Profi. 2 %s" ,
                                  " Profil 2 %s" ,
                                  " Profi. 2 %s" ,
                                  " Profi. 2 %s"};
const char *txtIDXMPPROF3[]    = {" Profi. 3 %s" ,
                                  " Profil 3 %s" ,
                                  " Profi. 3 %s" ,
                                  " Profi. 3 %s"};
const char *txtIDXMPPROF4[]    = {" Profi. 4 %s" ,
                                  " Profil 4 %s" ,
                                  " Profi. 4 %s" ,
                                  " Profi. 4 %s"};

//------------------------------------------------------
// CModePlayer.cpp. Do not change the size of the strings
const char *txtTPLAY[MAXLANGUAGE][4] = {{"  X1   ", "  X10  ", " Heter ", "  Del  "} ,
                                        {"  X1   ", "  X10  ", " Heter ", "  Sup. "} ,
                                        {"  X1   ", "  X10  ", " Heter ", "  Del  "} ,
                                        {"  X1   ", "  X10  ", " Heter ", "  Del  "}};
//------------------------------------------------------
// CModePlayer.cpp. Do not change the size of the strings
const char *txtTPLAYL[MAXLANGUAGE][4]= {{"X1 ", "X10", "Het", "Del"} ,
                                        {"X1 ", "X10", "Het", "Sup"} ,
                                        {"X1 ", "X10", "Het", "Del"} ,
                                        {"X1 ", "X10", "Het", "Del"}};
const char *txtNoFile[]    = {" No file!            " ,
                              " Pas de fichier !    " ,
                              " No file!            " ,
                              " No file!            "};
const char *txtIDXDELFILE[]    = {"Del file %s" ,
                                  "Sup. wav %s" ,
                                  "Del file %s" ,
                                  "Del file %s"};

//------------------------------------------------------
// CModeRecorder.cpp. Do not change the size of the strings
const char *txtErrorWriteSD[]  = {"Error write SD ! " ,
                                  "Err. Ecritu. SD !" ,
                                  "Error write SD ! " ,
                                  "Error write SD ! "};
const char *txtErrFullSD[]     = {"SD card full !   " ,
                                  "Carte SD pleine !" ,
                                  "SD card full !   " ,
                                  "SD card full !   "};
const char *txtRecordAcq[]     = {"Acquisi. %s-%s" ,
                                  "Acquisi. %s-%s" ,
                                  "Acquisi. %s-%s" ,
                                  "Acquisi. %s-%s"};
const char *txtRecordYes[]     = {"Y" ,
                                  "O" ,
                                  "J" ,
                                  "J"};
const char *txtRecordNo[]      = {"N" ,
                                  "N" ,
                                  "N" ,
                                  "N"};
// SF Sampling Frequency, LF Low Filter, HPF Hight Pass Filter
const char *txtRecordSFFLT[]   = {"SF%03dkHz LF %s HPF %02d" ,
                                  "Fe%03dkHz FL %s FPH %02d" ,
                                  "SF%03dkHz LF %s HPF %02d" ,
                                  "SF%03dkHz LF %s HPF %02d"};
const char *txtRecordSFFLTA[]  = {"SF%03dkHz LF %s HPF %.1f" ,
                                  "Fe%03dkHz FL %s FPH %.1f" ,
                                  "SF%03dkHz LF %s HPF %.1f" ,
                                  "SF%03dkHz LF %s HPF %.1f"};
// R. T. Relative Threshold
const char *txtRecordThrR[]    = {"R. T. %ddB %ddt. NG%d" ,
                                  "S. R. %ddB %ddt. GN%d" ,
                                  "R. T. %ddB %ddt. NG%d" ,
                                  "R. T. %ddB %ddt. NG%d"};
// A. T. Absolute Threshold
const char *txtRecordThrA[]    = {"A.T.%ddB %ddt. NG%d" ,
                                  "S.A.%ddB %ddt. GN%d" ,
                                  "A.T.%ddB %ddt. NG%d" ,
                                  "A.T.%ddB %ddt. NG%d"};
// Frequency Band min-maxkHz
const char *txtRecordBFr[]     = {"Freq. Bd. %d-%dkHz" ,
                                  "Bd. Freq. %d-%dkHz" ,
                                  "Freq. Bd. %d-%dkHz" ,
                                  "Freq. Bd. %d-%dkHz"};
const char *txtRecordBFrA[]    = {"Freq. Bd. %4.1f-%4.1fHz" ,
                                  "Bd. Freq. %4.1f-%4.1fHz" ,
                                  "Freq. Bd. %4.1f-%4.1fHz" ,
                                  "Freq. Bd. %4.1f-%4.1fHz"};
const char *txtRecordWSD[]     = {"Wav %d-%ds SD %d%%",
                                  "Wav %d-%ds SD %d%%",
                                  "Wav %d-%ds SD %d%%",
                                  "Wav %d-%ds SD %d%%"};
const char *txtRecordNoise[]   = {"Noise%ddB %d/%d" ,
                                  "Bruit%ddB %d/%d" ,
                                  "Noise%ddB %d/%d" ,
                                  "Noise%ddB %d/%d"};
const char *txtProtRoutier[]   = {"Road Protocol"     ,
                                  "Protocole Routier" ,
                                  "Road Protocol"     ,
                                  "Road Protocol"}    ;
const char *txtProtPed[]       = {"Walking protocol"   ,
                                  "Protocole Pedestre" ,
                                  "Walking protocol"   ,
                                  "Walking protocol"}  ;
const char *txtProtFixe[]      = {"Fixed Pt. protocol" ,
                                  "Protocole Pt. Fixe" ,
                                  "Fixed Pt. protocol" ,
                                  "Fixed Pt. protocol"};
const char *txtProtParcours[]  = {"Course %d"   ,
                                  "Parcours %d" ,
                                  "Course %d"   ,
                                  "Course %d"}  ;
const char *txtProtParcoursR[] = {"Course %d %02d:%02d..."   ,
                                  "Parcours %d %02d:%02d..." ,
                                  "Course %d %02d:%02d..."   ,
                                  "Course %d %02d:%02d..."}  ;
const char *txtProtPoint[]     = {"Point %d" ,
                                  "Point %d" ,
                                  "Point %d" ,
                                  "Point %d"};
const char *txtProtPointR[]    = {"Point %d %02d:%02d..." ,
                                  "Point %d %02d:%02d..." ,
                                  "Point %d %02d:%02d..." ,
                                  "Point %d %02d:%02d..."};
const char *txtProtParcoursP[] = {"Break course %d -"   ,
                                  "Pause parcours %d -" ,
                                  "Break course %d -"   ,
                                  "Break course %d -"}  ;
const char *txtProtEnCoursR[]  = {"in progress" ,
                                  "en cours"    ,
                                  "in progress" ,
                                  "in progress"};
const char *txtProtEnCoursP[]  = {"in progress (6mn)" ,
                                  "en cours (6mn)"    ,
                                  "in progress (6mn)" ,
                                  "in progress (6mn)"};
const char *txtProtStart[]     = {"Click to start"   ,
                                  "Clic pour lancer" ,
                                  "Click to start"   ,
                                  "Click to start"}  ;
const char *txtProtRestart[]   = {"Click to restart"   ,
                                  "Clic pour relancer" ,
                                  "Click to restart"   ,
                                  "Click to restart"}  ;
const char *txtProtStop[]      = {"Click to stop"     ,
                                  "Clic pour stopper" ,
                                  "Click to stop"     ,
                                  "Click to stop"}    ;
const char *txtProtStopP[]     = {"Down to stop point" ,
                                  "Bas stop point"     ,
                                  "Down to stop point" ,
                                  "Down to stop point"};
const char *txtProtPause[]     = {"Up to break"     ,
                                  "Haut pour pause" ,
                                  "Up to break"     ,
                                  "Up to break"}    ;
const char *txtAbandon[]       = {"Down to give up"     ,
                                  "Bas abandon protoc." ,
                                  "Down to give up"     ,
                                  "Down to give up"}    ;
const char *txtProtEnd[]       = {"End Protocol"     ,
                                  "Fin du protocole" ,
                                  "End Protocol"     ,
                                  "End Protocol"}    ;
const char *txtProtClicEnd[]   = {"Click to end"       ,
                                  "Clic pour terminer" ,
                                  "Click to end"       ,
                                  "Click to end"}      ;
const char *txtGANoise[]       = {"Noise %ddB Min/Max %d/%d" ,
                                  "Bruit %ddB Min/Max %d/%d" ,
                                  "Noise %ddB Min/Max %d/%d" ,
                                  "Noise %ddB Min/Max %d/%d"};

//------------------------------------------------------
// CModeRecorder.cpp CModeSynchro. Do not change the size of the strings
// Mast Master, Sl Slave
const char *txtMastSlave[MAXLANGUAGE][10]= {{"Mast %s", "Sl 1 %s", "Sl 2 %s", "Sl 3 %s", "Sl 4 %s", "Sl 5 %s", "Sl 6 %s", "Sl 7 %s", "Sl 8 %s", "Sl 9 %s"} ,
                                            {"Mait %s", "Es 1 %s", "Es 2 %s", "Es 3 %s", "Es 4 %s", "Es 5 %s", "Es 6 %s", "Es 7 %s", "Es 8 %s", "Es 9 %s"} ,
                                            {"Mast %s", "Sl 1 %s", "Sl 2 %s", "Sl 3 %s", "Sl 4 %s", "Sl 5 %s", "Sl 6 %s", "Sl 7 %s", "Sl 8 %s", "Sl 9 %s"} ,
                                            {"Mait %s", "Es 1 %s", "Es 2 %s", "Es 3 %s", "Es 4 %s", "Es 5 %s", "Es 6 %s", "Es 7 %s", "Es 8 %s", "Es 9 %s"}};
// "W ESP" Wait for ESP Processor, "W SLA" Wait for Slave, "W Ver" Wait Version of ESP Processor, "T ESP" Timeout for ESP Processor, "T SLA" Timeout Slave,
// "W PAR" Wait for parameters, "W SEN" Wait for send message, "OK" Slave OK, "STOP" Stop by master, "T MAS" Timeout master, "ERR P" error decoding message parameters, " ER  " Error
const char *txtStatusMS[MAXLANGUAGE][15] =
     {{"----", "W ESP", "W ESP", "W SLA", " OK  ", "W Ver", "T ESP", "T SLA", "W PAR", "W SEN", "  OK ", "Stop ", "T MAS", "ERR P", " ER  "} ,
      {"----", "A ESP", "A ESP", "A ESC", " OK  ", "W ver", "T ESP", "T ESC", "A PAR", "A EMI", "  OK ", "Stop ", "T MAI", "ERR P", " ER  "} ,
      {"----", "W ESP", "W ESP", "W SLA", " OK  ", "W Ver", "T ESP", "T SLA", "W PAR", "W SEN", "  OK ", "Stop ", "T MAS", "ERR P", " ER  "} ,
      {"----", "W ESP", "W ESP", "W SLA", " OK  ", "W Ver", "T ESP", "T SLA", "W PAR", "W SEN", "  OK ", "Stop ", "T MAS", "ERR P", " ER  "}};
                                // 123456789012345678901
const char *txtOperatorCmdU[]  = {"Up Valid. config.  " ,
                                  "Haut Valid. config." ,
                                  "Up Valid. config.  " ,
                                  "Up Valid. config.  "};
const char *txtOperatorCmdD[]  = {"Down > Parameters" ,
                                  "Bas  > Parametres" ,
                                  "Down > Parameters" ,
                                  "Down > Parameters"};
// Print error in synchro mode
const char *txtESP32Errors[MAXLANGUAGE][11] =
//  123456789012345678901   123456789012345678901   123456789012345678901   123456789012345678901   123456789012345678901   123456789012345678901   123456789012345678901   123456789012345678901   123456789012345678901   123456789012345678901   123456789012345678901
 {{"ESP32 OK             ","LORA Error           ","Top Error            ","Parameters Error     ","Slave Error          ","CRC Error            ","Serial Num mess error","Lora Num mess error  ","Time out error       ","Master/Slave state er","Uknow error          "} ,
  {"ESP32 OK             ","Erreur LORA          ","Erreur Top           ","Erreur Parametres    ","Erreur Esclave       ","Erreur CRC           ","Err. Num. Mess. Serie","Err. Num. Mess. LORA ","Depassement de temps ","Er. etat Maitre/Escla","Erreur inconnue      "} ,
  {"ESP32 OK             ","LORA Error           ","Top Error            ","Parameters Error     ","Slave Error          ","CRC Error            ","Serial Num mess error","Lora Num mess error  ","Time out error       ","Master/Slave state er","Uknow error          "} ,
  {"ESP32 OK             ","LORA Error           ","Top Error            ","Parameters Error     ","Slave Error          ","CRC Error            ","Serial Num mess error","Lora Num mess error  ","Time out error       ","Master/Slave state er","Uknow error          "}};

//------------------------------------------------------
// CModeRhinoLogger.cpp. Strongs for Log file
const char *txtFBdINVALID[]      = {" %s Invalid " ,
                                    " %s Invalide" ,
                                    " %s Invalid " ,
                                    " %s Invalid "};
const char *txtFBdFM[]           = {" %s  FM %d det. %6.2f-%06.2fkHz %02.0fkHz" ,
                                    " %s  FM %d det. %6.2f-%06.2fkHz %02.0fkHz" ,
                                    " %s  FM %d det. %6.2f-%06.2fkHz %02.0fkHz" ,
                                    " %s  FM %d det. %6.2f-%06.2fkHz %02.0fkHz"};
const char *txtFBdRH[]           = {" %s  CF %d det. %6.2f-%06.2fkHz %02dms" ,
                                    " %s  FC %d det. %6.2f-%06.2fkHz %02dms" ,
                                    " %s  CF %d det. %6.2f-%06.2fkHz %02dms" ,
                                    " %s  CF %d det. %6.2f-%06.2fkHz %02dms"};
const char *txtFBdCF[]           = {" %s QCF %d det. %6.2f-%06.2fkHz %02dms %02dms" ,
                                    " %s QFC %d det. %6.2f-%06.2fkHz %02dms %02dms" ,
                                    " %s QCF %d det. %6.2f-%06.2fkHz %02dms %02dms" ,
                                    " %s QCF %d det. %6.2f-%06.2fkHz %02dms %02dms"};

//------------------------------------------------------
// CTimedRecording.cpp. String for screen parameters
//                                    123456789012345678901
const char *txtIDXTRRTIME[]       = {"Record  %s" ,
                                     "Enreg.  %s" ,
                                     "Record  %s" ,
                                     "Record  %s"};
const char *txtIDXTRWTIME[]       = {"Wait    %s" ,
                                     "Attente %s" ,
                                     "Wait    %s" ,
                                     "Wait    %s"};
const char *txtIDXTRLEAVE[]       = {" Quit    %s" ,
                                     " Quitter %s" ,
                                     " Quit    %s" ,
                                     " Quit    %s"};

//------------------------------------------------------
// CAudioRecorder.cpp. String for screen parameters
//                                   123456789012345678901
const char *txtIDXASEUILR[]      = {"Th.%02ddB" ,
                                    "Se.%02ddB" ,
                                    "Th.%02ddB" ,
                                    "Th.%02ddB"};

//------------------------------------------------------
// CModeHeterodyne.cpp. String for screen parameters
const char *txtModesHT3_6[MAXLANGUAGE][4] = {{"[M]", "[A]", "[m]", "[a]"} ,   // Manual Auto
                                             {"[M]", "[A]", "[m]", "[a]"} ,
                                             {"[M]", "[A]", "[m]", "[a]"} ,
                                             {"[M]", "[A]", "[m]", "[a]"}};
const char *txtModesH[MAXLANGUAGE][4] = {{"M", "A", "m", "a"} ,   // Manual Auto
                                         {"M", "A", "m", "a"} ,
                                         {"M", "A", "m", "a"} ,
                                         {"M", "A", "m", "a"}};
const char *txtSELH[MAXLANGUAGE][4]   = {{"-NS", "-Se", "-ns", "-se"} ,   // Selectiv or not
                                         {"-NS", "-Se", "-ns", "-se"} ,
                                         {"-NS", "-Se", "-ns", "-se"} ,
                                         {"-NS", "-Se", "-ns", "-se"}};
const char *txtCMPAR[MAXLANGUAGE][3]  = {{"AGC %s", "Th.%ddB", "Het.%s"} ,   // Selectiv or not
                                         {"CAG %s", "Se.%ddB", "Het.%s"} ,
                                         {"AGC %s", "Th.%ddB", "Het.%s"} ,
                                         {"AGC %s", "Th.%ddB", "Het.%s"}};
const char *txtFrHet[]           = {"HF:" , // Hétérodyne frequency
                                    "FH:" ,
                                    "HF:" ,
                                    "HF:"};
const char *txtIDXLEVEL[]        = {"L %03.1f" , // Level
                                    "N %03.1f" ,
                                    "L %03.1f" ,
                                    "L %03.1f"};
const char *txtCERDMA[]          = {"DMA err." , // Error DMA (Direct Memory Acces)
                                    "Err. DMA" ,
                                    "DMA err." ,
                                    "DMA err."};
const char *txtIDXHSELF[]        = {" Het. selecti. %s" ,
                                    " Het. selectif %s" ,
                                    " Het. selecti. %s" ,
                                    " Het. selecti. %s"};
const char *txtHSYesNo[MAXLANGUAGE][4]    = {{"No    ", "Yes   ", "Never ", "Always"} ,
                                             {"Non   ", "Oui   ", "Jamais", "Toujrs"} ,
                                             {"Nein  ", "Ja    ", "Never ", "Always"} ,
                                             {"Nee   ", "Ja    ", "Never ", "Always"}};
const char *txtIDXHAUTOMAN[]     = {" Mode Het. %s" ,
                                    " Het. mode %s" ,
                                    " Het. mode %s" ,
                                    " Het. mode %s"};
const char *txtHMANAUTO[MAXLANGUAGE][4]   = {{"Manual    ", "Auto      ", "Alwa. Man.", "Alwa. Aut."} ,
                                             {"Manuel    ", "Auto      ", "Touj. Man.", "Touj. Aut."} ,
                                             {"Manual    ", "Auto      ", "Alwa. Man.", "Alwa. Aut."} ,
                                             {"Manual    ", "Auto      ", "Alwa. Man.", "Alwa. Aut."}};
//------------------------------------------------------
// CModifBandRL.cpp. String for screen parameters
const char *txtTypeBand[MAXLANGUAGE][4]  = {{"No ", "MF ", "CF ", "QCF"} , // Not use, Modulation Frequency, Constant Frequency, Quasi Constant Frequency
                                            {"Non", "FM ", "FC ", "QFC"} ,
                                            {"No ", "MF ", "CF ", "QCF"} ,
                                            {"No ", "MF ", "CF ", "QCF"}};
const char *txtIDXRet[]        = {"[Return]" ,
                                  "[Retour]" ,
                                  "[Return]" ,
                                  "[Return]"};
const char *txtIDXNbD[]        = {"[Nb.D. %02d]" , // Number of detections
                                  "[Nb.D. %02d]" ,
                                  "[Nb.D. %02d]" ,
                                  "[Nb.D. %02d]"};
const char *txtIDXTBD[]        = {"[Type %s]" , // Band type
                                  "[Type %s]" ,
                                  "[Type %s]" ,
                                  "[Type %s]"};
const char *txtIDXFMINB[]      = {"[Min F. %06.2f kHz]" , // Min frequency
                                  "[F. Min %06.2f kHz]" ,
                                  "[Min F. %06.2f kHz]" ,
                                  "[Min F. %06.2f kHz]"};
const char *txtIDXFMAXB[]      = {"[Max F. %06.2f kHz]" , // Max frequency
                                  "[F. Max %06.2f kHz]" ,
                                  "[Max F. %06.2f kHz]" ,
                                  "[Max F. %06.2f kHz]"};
const char *txtIDXMIND[]       = {"[Min Dur. %02d ms]" ,  // Min duration
                                  "[Dur. Min %02d ms]" ,
                                  "[Min Dur. %02d ms]" ,
                                  "[Min Dur. %02d ms]"};
const char *txtIDXMAXD[]       = {"[Max Dur. %02d ms]" ,  // Max duration
                                  "[Dur. Max %02d ms]" ,
                                  "[Max Dur. %02d ms]" ,
                                  "[Max Dur. %02d ms]"};
const char *txtIDXWidth[]      = {"[Min width %04.1f kHz]" , // Min width
                                  "[Larg. Min %04.1f kHz]" ,
                                  "[Min width %04.1f kHz]" ,
                                  "[Min width %04.1f kHz]"};

//--------------------------------------------------------------------------------------------------------------------------------------------
// Screen Error mode messages (max 21 char no accents)
//--------------------------------------------------------------------------------------------------------------------------------------------
//                                  123456789012345678901123456789012345678901
// Low battery
const char *txtLOWBAT[]         = {"SD card unreadable!  Low battery"     ,
                                   "Carte SD illisible ! Batterie faible" ,
                                   "Batterie zu Schwach !" ,
                                   "Batterij te zwak !"   };
// Error SD card
const char *txtSDERROR[]        = {"SD card unreadable!"   ,
                                   "Carte SD illisible !"  ,
                                   "SG karte unleserlich!" ,
                                   "SD kaart onleesbaar !"};
// Error write SD card
const char *txtSDWERROR[]       = {"Error write SD !"      ,
                                   "Erreur ecriture SD !"  ,
                                   "Error write SD !"      ,
                                   "Error write SD !"     };
// Error software
const char *txtSWError[]        = {"Software error !"      ,
                                   "Erreur logiciel !"     ,
                                   "Software  Fehler !"    ,
                                   "Software fout !"      };
// Error read profiles
const char *txtErrorRProfiles[] = {"Error read Profiles!" ,
                                   "Err. lect. Profils !" ,
                                   "Error read Profiles!" ,
                                   "Error read Profiles!"};

//--------------------------------------------------------------------------------------------------------------------------------------------
// Log file messages (no problem with accents)
//--------------------------------------------------------------------------------------------------------------------------------------------

//------------------------------------------------------
// Log in TeensyRecorder.ino
// Error SDFat Timeout
const char *txtSDFATTO[]   = {"In SDFat library, SdioTeensy.cpp set BUSY_TIMEOUT_MICROS to 800000"             ,
                              "Dans la bibliothèque SDFat, SdioTeensy.cpp mettre BUSY_TIMEOUT_MICROS à 800000" ,
                              "In SDFat library, SdioTeensy.cpp set BUSY_TIMEOUT_MICROS to 800000"             ,
                              "In SDFat library, SdioTeensy.cpp set BUSY_TIMEOUT_MICROS to 800000"}            ;
// Error SDFat config
const char *txtSDFATCFG[]  = {"Optimization SdFat: SdFatConfig.h put MAINTAIN_FREE_CLUSTER_COUNT to 1!"     ,
                              "Optimisation SdFat: SdFatConfig.h mettre MAINTAIN_FREE_CLUSTER_COUNT à 1 !"  ,
                              "Optimierung SdFat: SdFatConfig.h mettre MAINTAIN_FREE_CLUSTER_COUNT à 1 !"   ,
                              "Optimalisatie SdFat: SdFatConfig.h mettre MAINTAIN_FREE_CLUSTER_COUNT à 1 !"};
// Error SDFat begin
const char *txtSDFATBEGIN[]= {"SdFatSdio begin() failed !!! sd.card()->errorCode() = %d" ,
                              "SdFatSdio begin() erreur !!! sd.card()->errorCode() = %d" ,
                              "SdFatSdio begin() failed !!! sd.card()->errorCode() = %d" ,
                              "SdFatSdio begin() failed !!! sd.card()->errorCode() = %d"};
// Start Passive Recorder
const char *txtSTARTPR[]   = {"Starting Passive Recorder serial number %lu, %s, CPU %d, %s"    ,
                              "Démarrage Passive Recorder numéro de série %lu, %s, CPU %d, %s" ,
                              "Starten Passive Recorder seriennummer %lu, %s, CPU %d, %s"      ,
                              "Starten Passive Recorder serienummer %lu, %s, CPU %d, %s"}      ;
// Start Active Recorder
const char *txtSTARTAR[]   = {"Starting Active Recorder serial number %lu, %s, CPU %d, %s"    ,
                              "Démarrage Active Recorder numéro de série %lu, %s, CPU %d, %s" ,
                              "Starten Active Recorder seriennummer %lu, %s, CPU %d, %s"      ,
                              "Starten Active Recorder serienummer %lu, %s, CPU %d, %s"}      ;
// Start Passive Recorder Stereo
const char *txtSTARTPRS[]  = {"Starting Passive Recorder Stereo serial number %lu, %s, CPU %d, %s"    ,
                              "Démarrage Passive Recorder Stéréo numéro de série %lu, %s, CPU %d, %s" ,
                              "Starten Passive Recorder Stereo seriennummer %lu, %s, CPU %d, %s"      ,
                              "Starten Passive Recorder Stereo serienummer %lu, %s, CPU %d, %s"}      ;
// Start Passive Recorder Stereo Synchro
const char *txtSTARTPRSS[] = {"Starting Passive Recorder Stereo Synchro serial number %lu, %s, CPU %d, %s"    ,
                              "Démarrage Passive Recorder Stéréo Synchro numéro de série %lu, %s, CPU %d, %s" ,
                              "Starten Passive Recorder Stereo Synchro seriennummer %lu, %s, CPU %d, %s"      ,
                              "Starten Passive Recorder Stereo Synchro serienummer %lu, %s, CPU %d, %s"}      ;
// Start Passive Recorder Mono Synchro
const char *txtSTARTPRMS[] = {"Starting Passive Recorder Mono Synchro serial number %lu, %s, CPU %d, %s"    ,
                              "Démarrage Passive Recorder Mono Synchro numéro de série %lu, %s, CPU %d, %s" ,
                              "Starten Passive Recorder Mono Synchro seriennummer %lu, %s, CPU %d, %s"      ,
                              "Starten Passive Recorder Mono Synchro serienummer %lu, %s, CPU %d, %s"}      ;

//------------------------------------------------------
// Log in CModeGeneric.cpp
const char *txtLogParams[]     = {"### Switch to Settings mode"    ,
                                  "### Passage en mode Paramètres" ,
                                  "### Switch to Settings mode"    ,
                                  "### Switch to Settings mode"}   ;
const char *txtLogVeille[]     = {"### Switch to standby mode" ,
                                  "### Passage en mode Veille" ,
                                  "### Switch to standby mode" ,
                                  "### Switch to standby mode"};
const char *txtLogAuto[]       = {"### Switch to Auto Recording Mode"       ,
                                  "### Passage en mode Enregistrement Auto" ,
                                  "### Switch to Auto Recording Mode"       ,
                                  "### Switch to Auto Recording Mode"}      ;
const char *txtLogAutoS[]      = {"### Switch to Synchro Auto Recording Mode"       ,
                                  "### Passage en mode Enregistrement Auto Synchro" ,
                                  "### Switch to Synchro Auto Recording Mode"       ,
                                  "### Switch to Synchro Auto Recording Mode"}      ;
const char *txtLogProPed[]     = {"### Switch in Walking Protocol mode"    ,
                                  "### Passage en mode Protocole Pédestre" ,
                                  "### Switch in Walking Protocol mode"    ,
                                  "### Switch in Walking Protocol mode"}   ;
const char *txtLogProRou[]     = {"### Switch in Road Protocol mode"      ,
                                  "### Passage en mode Protocole Routier" ,
                                  "### Switch in Road Protocol mode"      ,
                                  "### Switch in Road Protocol mode"}     ;
const char *txtLogProPFixe[]   = {"### Switch in Fixed point Protocol mode"  ,
                                  "### Passage en mode Protocole Point fixe" ,
                                  "### Switch in Fixed point Protocol mode"  ,
                                  "### Switch in Fixed point Protocol mode"} ;
const char *txtLogRhinoLog[]   = {"### Switch in RhinoLogger mode"  ,
                                  "### Passage en mode RhinoLogger" ,
                                  "### Switch in RhinoLogger mode"  ,
                                  "### Switch in RhinoLogger mode"} ;
const char *txtLogTstMic[]     = {"### Switch to Micro Test Mode"  ,
                                  "### Passage en mode Test Micro" ,
                                  "### Switch to Micro Test Mode"  ,
                                  "### Switch to Micro Test Mode"} ;
const char *txtLogHeterodyne[] = {"### Switch to Heterodyne Mode"  ,
                                  "### Passage en mode Hétérodyne" ,
                                  "### Switch to Heterodyne Mode"  ,
                                  "### Switch to Heterodyne Mode"} ;
const char *txtLogPlayer[]     = {"### Switch to Player Mode"   ,
                                  "### Passage en mode Lecteur" ,
                                  "### Switch to Player Mode"   ,
                                  "### Switch to Player Mode"}  ;
const char *txtLogError[]      = {"### Switch to Error mode"   ,
                                  "### Passage en mode Erreur" ,
                                  "### Switch to Error mode"   ,
                                  "### Switch to Error mode"}  ;
const char *txtLogBDRL[]       = {"### Switch to RhinoLogger bands"        ,
                                  "### Passage en mode bandes RhinoLogger" ,
                                  "### Switch to RhinoLogger bands"        ,
                                  "### Switch to RhinoLogger bands"}       ;
const char *txtLogTSTSD[]      = {"### Switch to test SD card"        ,
                                  "### Passage en mode test carte SD" ,
                                  "### Switch to test SD card"        ,
                                  "### Switch to test SD card"}       ;
const char *txtLogAudioRec[]   = {"### Switch to Audio Rec. mode"    ,
                                  "### Passage en mode Enreg. Audio" ,
                                  "### Switch to Audio Rec. mode"    ,
                                  "### Switch to Audio Rec. mode"}   ;
const char *txtLogTimedRec[]   = {"### Switch to Timed Rec. mode"      ,
                                  "### Passage en mode Enreg. Cadencé" ,
                                  "### Switch to Timed Rec. mode"      ,
                                  "### Switch to Timed Rec. mode"}     ;
const char *txtLogProfilesM[]  = {"### Switch to modification of operator profiles" ,
                                  "### Passage en mode modification des profils"    ,
                                  "### Switch to modification of operator profiles" ,
                                  "### Switch to modification of operator profiles"};
const char *txtLogProfilesR[]  = {"### Switch to read of operator profiles" ,
                                  "### Passage en mode lecture des profils" ,
                                  "### Switch to read of operator profiles" ,
                                  "### Switch to read of operator profiles"};
const char *txtLogCopyright[]  = {"### Switch to copyright mode"  ,
                                  "### Passage en mode copyright" ,
                                  "### Switch to copyright mode"  ,
                                  "### Switch to copyright mode"} ;
const char *txtLogIndet[]      = {"### Switch to indeterminate mode!" ,
                                  "### Passage en mode indéterminé !" ,
                                  "### Switch to indeterminate mode!" ,
                                  "### Switch to indeterminate mode!"};
const char *txtLogEndSleep[]   = {"Wakeup in progress" ,
                                  "Reveil en cours"    ,
                                  "Wakeup in progress" ,
                                  "Wakeup in progress"};
const char *txtLogSDWait[]     = {"SD card reading..."  ,
                                  "Lecture carte SD..." ,
                                  "SD card reading..."  ,
                                  "SD card reading..."} ;
const char *txtLogRAMD[]       = {"Ram after delete mode"         ,
                                  "Ram après destruction du mode" ,
                                  "Ram after delete mode"         ,
                                  "Ram after delete mode"}        ;
const char *txtLogRAMB[]       = {"Ram before first new mode"  ,
                                  "Ram avant 1er nouveau mode" ,
                                  "Ram before first new mode"  ,
                                  "Ram before first new mode"} ;
const char *txtLogRAMA[]       = {"Ram after new mode"         ,
                                  "Ram apres création du mode" ,
                                  "Ram after new mode"         ,
                                  "Ram after new mode"}        ;
const char *txtLogCPULOW[]     = {"With CPU frequency < 180MHZ, stereo is not possible !"           ,
                                  "Avec une fréquence CPU < 180MHZ, la stéréo n'est pas possible !" ,
                                  "With CPU frequency < 180MHZ, stereo is not possible !"           ,
                                  "With CPU frequency < 180MHZ, stereo is not possible !"}          ;
const char *txtLogFileTemp[]   = {"MemoTemperature impossible to create file %s !"      ,
                                  "MemoTemperature Creation du fichier %s impossible !" ,
                                  "MemoTemperature impossible to create file %s !"      ,
                                  "MemoTemperature impossible to create file %s !"}     ;
const char *txtLogSection[]    = {"Reading %s, %s section not found !"   ,
                                  "Lecture %s, section %s introuvable !" ,
                                  "Reading %s, %s section not found !"   ,
                                  "Reading %s, %s section not found !"} ;
const char *txtLogDecode[]     = {"Reading %s, error on decode %s"     ,
                                  "Lecture %s, erreur sur decodage %s" ,
                                  "Reading %s, error on decode %s"     ,
                                  "Reading %s, error on decode %s"}    ;
const char *txtLogStandbyPR[]  = {"Go to standby, wakeup at %s, Internal bat. %3.1lf %2d%%, External bat. %3.1lf %2d%%" ,
                                  "Mise en veille, réveil à %s, Bat. Interne %3.1lf %2d%%, Bat. Externe %3.1lf %2d%%"   ,
                                  "Go to standby, wakeup at %s, Internal bat. %3.1lf %2d%%, External bat. %3.1lf %2d%%" ,
                                  "Go to standby, wakeup at %s, Internal bat. %3.1lf %2d%%, External bat. %3.1lf %2d%%"};
const char *txtLogStandbyAR[]  = {"Go to standby, wakeup at %s, Internal bat. %3.1lf %2d%%" ,
                                  "Mise en veille, réveil à %s, Bat. Interne %3.1lf %2d%%"  ,
                                  "Go to standby, wakeup at %s, Internal bat. %3.1lf %2d%%" ,
                                  "Go to standby, wakeup at %s, Internal bat. %3.1lf %2d%%"};
const char *txtLogSWakeupPR[]  = {"Standby output, Internal bat. %3.1lf %2d%%, External bat. %3.1lf %2d%%" ,
                                  "Sortie de veille, Bat. Interne %3.1lf %2d%%, Bat. Externe %3.1lf %2d%%" ,
                                  "Standby output, Internal bat. %3.1lf %2d%%, External bat. %3.1lf %2d%%" ,
                                  "Standby output, Internal bat. %3.1lf %2d%%, External bat. %3.1lf %2d%%"};
const char *txtLogSWakeupAR[]  = {"Standby output, Internal bat. %3.1lf %2d%%"  ,
                                  "Sortie de veille, Bat. Interne %3.1lf %2d%%" ,
                                  "Standby output, Internal bat. %3.1lf %2d%%"  ,
                                  "Standby output, Internal bat. %3.1lf %2d%%"} ;
const char *txtLogAutoTime[]   = {"Set time with AutoTime.ini %02d/%02d/%04d %02d:%02d:%02d"        ,
                                  "Init Date-Heure avec AutoTime.ini %02d/%02d/%04d %02d:%02d:%02d" ,
                                  "Set time with AutoTime.ini %02d/%02d/%04d %02d:%02d:%02d"        ,
                                  "Set time with AutoTime.ini %02d/%02d/%04d %02d:%02d:%02d"}       ;
const char *txtLogAutoProfiles[]= {"Set parameters with AutoProfiles.ini"      ,
                                   "Init des paramètres avec AutoProfiles.ini" ,
                                   "Set parameters with AutoProfiles.ini"      ,
                                   "Set parameters with AutoProfiles.ini"}     ;

//------------------------------------------------------
// Log in CRecorder.cpp
const char *txtLogAdjustNbBufferH[] = {"CRecorder Unallocated Heap %d, Nb buffer %d, Size buffer %d, pre-trigger max %dms"  ,
                                       "CRecorder Heap non alloué %d, Nb buffer %d, taille buffer %d, pre-trigger max %dms" ,
                                       "CRecorder Unallocated Heap %d, Nb buffer %d, Size buffer %d, pre-trigger max %dms"  ,
                                       "CRecorder Unallocated Heap %d, Nb buffer %d, Size buffer %d, pre-trigger max %dms"} ;
const char *txtLogAdjustNbBufferF[] = {"CRecorder Free ram %d, Nb buffer %d, Size buffer %d, pre-trigger max %dms"         ,
                                       "CRecorder Ram non alloué %d, Nb buffer %d, taille buffer %d, pre-trigger max %dms" ,
                                       "CRecorder Free ram %d, Nb buffer %d, Size buffer %d, pre-trigger max %dms"         ,
                                       "CRecorder Free ram %d, Nb buffer %d, Size buffer %d, pre-trigger max %dms"}        ;
const char *txtLogAdjustNbBufferT41[]={"CRecorder Nb buffer %d, Size buffer %d, pre-trigger max %dms"         ,
                                       "CRecorder Nb buffer %d, taille buffer %d, pre-trigger max %dms"       ,
                                       "CRecorder Nb buffer %d, Size buffer %d, pre-trigger max %dms"         ,
                                       "CRecorder Nb buffer %d, Size buffer %d, pre-trigger max %dms"}        ;
const char *txtLogResetMemA[]       = {"CRecorder memory problem for iNbSampleBuffer (%d) !"    ,
                                       "CRecorder problème mémoire pour iNbSampleBuffer (%d) !" ,
                                       "CRecorder memory problem for iNbSampleBuffer (%d) !"    ,
                                       "CRecorder memory problem for iNbSampleBuffer (%d) !"}   ;
const char *txtLogResetMemB[]       = {"CRecorder memory problem for pSamplesRecord !"    ,
                                       "CRecorder problème mémoire pour pSamplesRecord !" ,
                                       "CRecorder memory problem for pSamplesRecord !"    ,
                                       "CRecorder memory problem for pSamplesRecord !"}   ;
const char *txtLogResetMemC[]       = {"Memory problem !"   ,
                                       "Probleme memoire !" ,
                                       "Memory problem !"   ,
                                       "Memory problem !"}  ;
const char *txtLogBufferLossR[]     = {"*** Loss of samples in recording, break in real time!"               ,
                                       "*** Perte d'échantillons en enregistrement, rupture du temps réel !" ,
                                       "*** Loss of samples in recording, break in real time!"               ,
                                       "*** Loss of samples in recording, break in real time!"}              ;
const char *txtLogBufferLossA[]     = {"*** Loss of samples during acquisition, break in real time!"       ,
                                       "*** Perte d'échantillons en acquisition, rupture du temps réel !"  ,
                                       "*** Loss of samples during acquisition, break in real time!"       ,
                                       "*** Loss of samples during acquisition, break in real time!"}      ;

//------------------------------------------------------
// Log in CModeParams.cpp
const char *txtLogModifUserLevel[]  = {"User level selection %s"            ,
                                       "Sélection du niveau utilisateur %s" ,
                                       "User level selection %s"            ,
                                       "User level selection %s"}           ;
const char *txtLogModifProfile[]    = {"Profile selection %s"   ,
                                       "Sélection du profil %s" ,
                                       "Profile selection %s"   ,
                                       "Profile selection %s"}  ;
const char *txtLogErrProfile[]      = {"Error on read %s !"  ,
                                       "Erreur lecture %s !" ,
                                       "Error on read %s !"  ,
                                       "Error on read %s !"} ;
const char *txtLogLoadProfile[]     = {"%s file loading"       ,
                                       "Chargement fichier %s" ,
                                       "%s file loading"       ,
                                       "%s file loading"}      ;

//------------------------------------------------------
// Log in CModePlayer.cpp
const char *txtLogErrPlayFile[]     = {"Reader.SetWaveFile error reading file [%s]"     ,
                                       "Reader.SetWaveFile erreur lecture fichier [%s]" ,
                                       "Reader.SetWaveFile error reading file [%s]"     ,
                                       "Reader.SetWaveFile error reading file [%s]"}    ;
const char *txtLogStartPlayFile[]   = {"Start reading [%s]"     ,
                                       "Lancement lecture [%s]" ,
                                       "Start reading [%s]"     ,
                                       "Start reading [%s]"}    ;
const char *txtLogStopPlayFile[]    = {"Operator stop reading"   ,
                                       "Arrêt lecture opérateur" ,
                                       "Operator stop reading"   ,
                                       "Operator stop reading"}  ;

//------------------------------------------------------
// Log in CModeRecorder.cpp
const char *txtParameters[]    = {"Parameters : " ,
                                  "Paramètres : " ,
                                  "Parameters : " ,
                                  "Parameters : "};
const char *txtProbeTH[]       = {"Temperature/humidity probe present, reading every %ds"          ,
                                  "Sonde température/hygrométrie présente, lecture toutes les %ds" ,
                                  "Temperature/humidity probe present, reading every %ds"          ,
                                  "Temperature/humidity probe present, reading every %ds"}         ;
const char *txtNoProbeTH[]     = {"No Temperature/humidity probe"         ,
                                  "Sonde température/hygrométrie absente" ,
                                  "No Temperature/humidity probe"         ,
                                  "No Temperature/humidity probe"}        ;
const char *txtBatteries[]     = {"Internal batteries %3.1fV (%d%%)" ,
                                  "Batteries internes %3.1fV (%d%%)" ,
                                  "Internal batteries %3.1fV (%d%%)" ,
                                  "Internal batteries %3.1fV (%d%%)"};
const char *txtBatteriesMAX[]  = {"Internal batteries %3.1fV (%d%%) (MAX17043)" ,
                                  "Batteries internes %3.1fV (%d%%) (MAX17043)" ,
                                  "Internal batteries %3.1fV (%d%%) (MAX17043)" ,
                                  "Internal batteries %3.1fV (%d%%) (MAX17043)"};
const char *txtBatteriesMCP[]  = {"Internal batteries %3.1fV (%d%%) (MCP3221)" ,
                                  "Batteries internes %3.1fV (%d%%) (MCP3221)" ,
                                  "Internal batteries %3.1fV (%d%%) (MCP3221)" ,
                                  "Internal batteries %3.1fV (%d%%) (MCP3221)"};
const char *txtLogSDErrSize[]  = {"Error SD free space %fGO"      ,
                                  "Erreur SD place restante %fGO" ,
                                  "Error SD free space %fGO"      ,
                                  "Error SD free space %fGO"}     ;
const char *txtLogSDPbSize[]   = {"%d problem SD free space %fGO"                ,
                                  "%d problème de taille SD place restante %fGO" ,
                                  "%d problem SD free space %fGO"                ,
                                  "%d problem SD free space %fGO"}               ;
const char *txtLogTopSynchro[] = {", Top %dkHz %d ms %ds" ,
                                  ", Top %dkHz %d ms %ds" ,
                                  ", Top %dkHz %d ms %ds" ,
                                  ", Top %dkHz %d ms %ds"};
const char *txtLogSynchroOK[] =  {"CModeSynchro config OK" ,
                                  "CModeSynchro config OK" ,
                                  "CModeSynchro config OK" ,
                                  "CModeSynchro config OK"};
const char *txtLogStopSlave[] =  {"CModeSynchro Slave, Master stop mode" ,
                                  "CModeSynchro Slave, Master stop mode" ,
                                  "CModeSynchro Slave, Master stop mode" ,
                                  "CModeSynchro Slave, Master stop mode"};
const char *txtLogStopMaster[] = {"CModeSynchro Master stop mode" ,
                                  "CModeSynchro Master stop mode" ,
                                  "CModeSynchro Master stop mode" ,
                                  "CModeSynchro Master stop mode"};

//------------------------------------------------------
// Log in CModeRhinoLogger.cpp
const char *txtLogThAbs[]      = {"Parameters: Absolute Threshold %ddB, Digital gain %sdB" ,
                                  "Paramètres: Seuil absolu %ddB, Gain numérique %sdB"     ,
                                  "Parameters: Absolute Threshold %ddB, Digital gain %sdB" ,
                                  "Parameters: Absolute Threshold %ddB, Digital gain %sdB"};
const char *txtLogThRel[]      = {"Parameters: Relative Threshold %ddB, Digital gain %sdB" ,
                                  "Paramètres: Seuil absolu %ddB, Gain numérique %sdB"     ,
                                  "Parameters: Relative Threshold %ddB, Digital gain %sdB" ,
                                  "Parameters: Relative Threshold %ddB, Digital gain %sdB"};

//------------------------------------------------------
// Log in RecorderRL.cpp
const char *txtLogRLMaxCnx[]   = {"RhinoLogger mode, the number of RH/FC band channels (%d) exceeds the memory capacity"    ,
                                  "Mode RhinoLogger, le nombre de canaux des bandes RH/FC (%d) dépasse la capacité mémoire" ,
                                  "RhinoLogger mode, the number of RH/FC band channels (%d) exceeds the memory capacity"    ,
                                  "RhinoLogger mode, the number of RH/FC band channels (%d) exceeds the memory capacity"}   ;
const char *txtLogPbFileRL[]   = {"Impossible to create file %s !"      ,
                                  "Création du fichier %s impossible !" ,
                                  "Impossible to create file %s !"      ,
                                  "Impossible to create file %s !"}     ;

//------------------------------------------------------
// CTimedRecording.cpp
const char *txtLogTRecStart[]  = {"Timed Recording, Recording start time %s, Waiting start time %s, recording period %d, waiting period for %d s, SF %d kHz, %s"                      ,
                                  "Enregistrement cadencé, Heure de départ enregistrement %s, Heure de veille %s, période d'enregistrement %d, Période d'attente %d s, FE %d kHz, %s" ,
                                  "Timed Recording, Recording start time %s, Waiting start time %s, recording period %d, waiting period for %d s, SF %d kHz, %s"                      ,
                                  "Timed Recording, Recording start time %s, Waiting start time %s, recording period %d, waiting period for %d s, SF %d kHz, %s"}                     ;
const char *txtLogTRec[]       = {"Timed Recording start of a recording period for %d s, SF %d kHz"                   ,
                                  "Enregistrement cadencé, début d'une période d'enregistrement pour %d s, SF %d kHz" ,
                                  "Timed Recording start of a recording period for %d s, SF %d kHz"                   ,
                                  "Timed Recording start of a recording period for %d s, SF %d kHz"}                  ;
const char *txtLogTWait[]      = {"Timed Recording start of a waiting period for %d s"              ,
                                  "Enregistrement cadencé, début d'une période de veille pour %d s" ,
                                  "Timed Recording start of a waiting period for %d s"              ,
                                  "Timed Recording start of a waiting period for %d s"}             ;
