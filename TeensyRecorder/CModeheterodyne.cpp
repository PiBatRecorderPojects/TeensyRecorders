/* 
 * File:   ModeHeterodyne.cpp
   Teensy Recorder Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <arduino.h>
#include <U8g2lib.h>
#include "CModeheterodyne.h"
#include "CModePlayer.h"

// Screen manager
extern U8G2 *pDisplay;

// Gestionnaire du fichier LogPR.txt
extern LogFile logPR;

// Temps d'affichage du multiparamètres en s
#define TIMEAFFMULPAR 4

// Strings for parameters
const char formIDXHMODEH[]       =  "&$$";
const char *txtIDXHMODEH[]       = {"%s",
                                    "%s",
                                    "%s",
                                    "%s"};
extern const char *txtModesH[MAXLANGUAGE][4];
extern const char *txtModesHT3_6[MAXLANGUAGE][4];
const char formIDXMPAR[]         =  "&";
const char *txtIDXMPAR[]         = {"%s",
                                    "%s",
                                    "%s",
                                    "%s"};
extern const char *txtMPAR[MAXLANGUAGE][3];
const char formIDXHTIMER[]       =  "&$s";
const char *txtIDXHTIMER[]       = {"%03ds",
                                    "%03ds",
                                    "%03ds",
                                    "%03ds"};
const char formIDXHSEUILR[]      =  "Se.&$dB";
extern const char *txtIDXHSEUILR[];
const char formIDXHFME[]         =  "&$$$$";
const char *txtIDXHFME[]         = {"%05.1fkHz",
                                    "%05.1fkHz",
                                    "%05.1fkHz",
                                    "%05.1fkHz"};
const char formIDXHNIV[]         =  "&$$dB";
const char *txtIDXHNIV[]         = {"%03ddB",
                                    "%03ddB",
                                    "%03ddB",
                                    "%03ddB"};
extern const char *txtFrHet[];
const char formIDXHHEURE[]       =  "&$:&$:&$";
const char *txtIDXHHEURE[]       = {"%s",
                                    "%s",
                                    "%s",
                                    "%s"};
const char formIDXLEVEL[]        =  "&$$";
extern const char *txtIDXLEVEL[];
const char formIDXERDMA[]        =  "&&&&& ";
const char *txtIDXERDMA[]        = {"%s",
                                    "%s",
                                    "%s",
                                    "%s"};
extern const char *txtCERDMA[];
extern const char *txtIDXASEUILR[];
const char formIDXAGC[]          =  "[&]";
extern const char *txtCMPAR[MAXLANGUAGE][3];
extern const char *txtIDXAGC[];
extern const char *txtYesNo[MAXLANGUAGE][2];

//-------------------------------------------------------------------------
// Heterodyne mode management class

// Heterodyne mode screen
//   12345678901234567890  y
// 1 110.5KHz[M] 384[Bat]  0
// 2 22:12:32 20° 80% [O]  12
// 2 Se. 15dB 20° 80% [O]  12
// 3 110.5KHz -100dB 100s  21
// 4  20 40 60 80 100 120  30
// 5                       38
// 6      Graphe           45
// 7                       54
// X 000000000000000001111
//   001123344566778990012
//   062840628406284062840

// Keys : K_UP K_DOWN +/- Threshold
//        K_PUSH      Auto/Manual
//        K_MODEB     Stop/Start Record
//        K_MODEA     Change mode

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CModeHeterodyne::CModeHeterodyne():graphe( 20, 30)
{
  // Set parameters
  fFreqH        = 45.0;
  fFME          = 40.0;
  iMaxLevel     = -120;
  iMemoMaxLevel = -120;
  iRecordMode   = PBREAK;
  iRecordTime   = 0;
  bModifMulPar  = false;
  bModifTh      = false;
  bAutoPlay     = false;
  iCptMulPar    = -1;
  iCptAffWav    = -1;
  // Creating Parameter Modifier Lines
  // IDXHRECORD   Record status
  LstModifParams.push_back(new CPlayRecModifier( &iRecordMode , false, true, 25, 9, 100, 11));
  // IDXHTIMER    Record time
  LstModifParams.push_back(new CModificatorInt ( formIDXHTIMER, false, txtIDXHTIMER, &iRecordTime, 0, 999, 1, 96, 21));
  // IDXHMODEH    Mode of operation of heterodyne (manual, auto)
#if defined(__IMXRT1062__) // Teensy 4.1
#if defined(SELFILTERH10KHZ) || defined(SELFILTERH20KHZ)
  LstModifParams.push_back(new CEnumModifier   ( formIDXHMODEH, false, true, txtIDXHMODEH, (int *)&paramsOperateur.iAutoHeter, HAM_MAX, (const char **)txtModesH, 60, 2));
#else
  LstModifParams.push_back(new CEnumModifier   ( formIDXHMODEH, false, true, txtIDXHMODEH, (int *)&paramsOperateur.iAutoHeter, HAM_MAX, (const char **)txtModesHT3_6, 60, 2));
#endif
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  LstModifParams.push_back(new CEnumModifier   ( formIDXHMODEH, false, true, txtIDXHMODEH, (int *)&paramsOperateur.iAutoHeter, HAM_MAX, (const char **)txtModesHT3_6, 60, 2));
#endif
  // IDXMULTIPAR  Enum to modify 3 parameters in heterodyne mode
  LstModifParams.push_back(new CLineEnumModifier( formIDXMPAR, false, false, txtIDXMPAR, &iMultiPar, HMUL_MAX, (const char **)txtPMPAR, 0, 12));
  // IDXHSEUILR   Relative detection threshold in dB (5-99)
  LstModifParams.push_back(new CModificatorInt ( formIDXHSEUILR, false, txtIDXASEUILR, &paramsOperateur.iSeuilDet, 5, 99, 3, 37, 12));
  // IDXHAGC      AGC On Off
  LstModifParams.push_back(new CBoolModifier ( formIDXAGC, false, false, txtIDXAGC, &paramsOperateur.bAGCHet, (const char **)txtYesNo, 87, 21));
  // IDXHFME      Detected frequency of maximum energy
  LstModifParams.push_back(new CFloatModifier  ( formIDXHFME, false, txtIDXHFME, &fFME, 10, 120, 0.1, 0, 21));
  // IDXHNIVA     Maximum level detected in numbers
  LstModifParams.push_back(new CModificatorInt ( formIDXHNIV, false, txtIDXHNIV, &iMaxLevel, -120, -40, 1, 52, 21));
  // IDXHBAT      Internal battery charge level in %
  LstModifParams.push_back(new CBatteryModifier( &iNivBatLiPo, 22, 8, 102, 1));
  // IDXHHOUR     Heure courante                                                     Heure 09/12/22
  LstModifParams.push_back(new CHourModifier( formIDXHHEURE , false, txtIDXHHEURE  , NULL, 0, 12));
  // IDXHFILEA    File name A to C if no graph
  int y = 30;
  for (int i=0; i<MAXFILESNAMENOGR; i++)
  {
    LstModifParams.push_back(new CCharModifier (formIDXERDMA, false, txtIDXERDMA, LstFilesScreen[i], 8, false, true, false, true, true, 0, y));
    // Empty files name
    strcpy( LstFilesScreen[i], " ");
    y += 8;
 }
  // IDXHERDMA    DMA error
  LstModifParams.push_back(new CCharModifier (formIDXERDMA, false, txtIDXERDMA, txtERDMA, 8, false, true, false, true, true, 0, 12));
  strcpy( txtERDMA, txtCERDMA[CModeGeneric::GetCommonsParams()->iLanguage]);
  // Préparation des chaines de l'énuméré multiparamètres
  iMultiPar = 0;
  int iL = CModeGeneric::GetCommonsParams()->iLanguage;
  sprintf( txtMPAR[HMUL_AGC], txtCMPAR[iL][HMUL_AGC], txtYesNo[iL][paramsOperateur.bAGCHet]);
  sprintf( txtMPAR[HMUL_THR], txtCMPAR[iL][HMUL_THR], paramsOperateur.iSeuilDet);
  sprintf( txtMPAR[HMUL_HAM], txtCMPAR[iL][HMUL_HAM], txtModesH[iL][paramsOperateur.iAutoHeter]);
  for (int i=0; i<MAXLANGUAGE; i++)
  {
    txtPMPAR[i][HMUL_AGC] = txtMPAR[HMUL_AGC];
    txtPMPAR[i][HMUL_THR] = txtMPAR[HMUL_THR];
    txtPMPAR[i][HMUL_HAM] = txtMPAR[HMUL_HAM];
  }
  if (paramsOperateur.iAutoHeter > HAM_AUTO)
    ((CLineEnumModifier *)LstModifParams[IDXMULTIPAR])->SetElemValidity( HMUL_HAM, false);
}

//-------------------------------------------------------------------------
//! \brief Creation du gestionnaire d'enregistrement
void CModeHeterodyne::CreateRecorder()
{
  //Serial.println("CModeHeterodyne::CreateRecorder");
  pRecorder = new CRecorderH( &paramsOperateur, NBBUFFERRECORDH);
}

const char txtRamUsageHet[]       = "Ram after CModeHeterodyne::BeginMode";

//-------------------------------------------------------------------------
// Beginning of the mode
void CModeHeterodyne::BeginMode()
{
  // Lecture des paramètres
  CGenericMode::ReadParams();
  // Set some parameters
  paramsOperateur.bTypeSeuil  = false;                     // Relative threshold
  paramsOperateur.bFiltre     = false;                     // No low pass filter or linear filter (using DSP)
  paramsOperateur.bExp10      = true;                      // Expansion de temps X10
  paramsOperateur.iModeRecord = PHETER;                    // Recording mode
  if (paramsOperateur.iFreqFiltreHighPass < 10)
    paramsOperateur.iFreqFiltreHighPass = 10;              // Min high pass filter to 10kHz
  // Call of the parent method without read params
  bReadParams = false;
  CModeGenericRec::BeginMode();
  /*if (paramsOperateur.bAutoRecH)
    Serial.println("Enregistrement hétérodyne automatique");
  else
    Serial.println("Enregistrement hétérodyne manuel");*/
  // Limit sample frequency if CPU is below 180MHz
  if (F_CPU < 179000000 and paramsOperateur.uiFe >= FE384KHZ)
    paramsOperateur.uiFe = FE250KHZ;
  // Initialization
  iCurrentSecond = 61;
  LstModifParams[IDXHRECORD ]->SetEditable( false);
  LstModifParams[IDXHTIMER  ]->SetEditable( false);
  LstModifParams[IDXHTIMER  ]->SetbValid  ( false);
  LstModifParams[IDXHMODEH  ]->SetEditable( false);
  LstModifParams[IDXHSEUILR ]->SetEditable( false);
  LstModifParams[IDXHFME    ]->SetEditable( false);
  LstModifParams[IDXHNIVA   ]->SetEditable( false);
  LstModifParams[IDXHHOUR   ]->SetEditable( false);
  LstModifParams[IDXHERDMA  ]->SetEditable( false);
  LstModifParams[IDXHAGC    ]->SetEditable( false);
  LstModifParams[idxSelParam]->SetbSelect ( false);
  // Par défaut, heure visible, multiparamètres et erreur DMA invisibles
  LstModifParams[IDXHSEUILR ]->SetbValid(   false);
  LstModifParams[IDXHERDMA  ]->SetbValid(   false);
  LstModifParams[IDXMULTIPAR]->SetbValid(   false);
  LstModifParams[IDXHFME    ]->SetbValid(   false);
  LstModifParams[IDXHNIVA   ]->SetbValid(   false);
  // Liste des 3 fichiers non modifiables
    for (int i=0; i<MAXFILESNAMENOGR; i++)
    LstModifParams[IDXHFILEA+i]->SetEditable( false);
  if (paramsOperateur.bGraphHet)
  {
    // Liste des 3 fichiers invisible
    for (int i=0; i<MAXFILESNAMENOGR; i++)
      LstModifParams[IDXHFILEA+i]->SetbValid( false);
    // On repositionne le 1er au niveau de la ligne d'affichage temporaire
    LstModifParams[IDXHFILEA]->SetY(21);
  }
  else
    // Initialisation de la liste des 3 derniers fichiers
    ListFiles();
  uiMinIntFreq = paramsOperateur.uiFreqMin / 1000;
  uiMaxIntFreq = paramsOperateur.uiFreqMax / 1000;
  graphe.SetMinMaxScale( uiMinIntFreq, uiMaxIntFreq);
  //Serial.printf("CModeHeterodyne::BeginMode uiFreqMin %d, uiFreqMax %d\n", paramsOperateur.uiFreqMin, paramsOperateur.uiFreqMax);
  int iRafGrape = (int)(paramsOperateur.fRefreshGRH * 1000.0);
  graphe.SetTimeScroll( iRafGrape);
  // Test battery
  uiTimeCheck = millis() + 100;
  // Heterodyne wheel ADC Initialization
  ADC0Init( true);
  fStepADC  = 110.0 / 4096.0;
  fStepAuto = 20.0  / 4096.0;
  // Width of a FFT channel in Hz
  uint32_t uiFe = 384000; 
  switch (paramsOperateur.uiFe)
  {
  case FE24KHZ : uiFe =  24000; break;
  case FE48KHZ : uiFe =  48000; break;
  case FE96KHZ : uiFe =  96000; break;
  case FE192KHZ: uiFe = 192000; break;
  case FE250KHZ: uiFe = 250000; break;
  case FE384KHZ: uiFe = 384000; break;
  case FE500KHZ: uiFe = 500000; break;
  }
  uiFFTChannel = (uiFe/2)/(FFTLEN/2);
  // Print Ram usage
  /*PrintRamUsage( (char *)txtRamUsageHet);
  bool bMemo = logPR.GetbConsole();
  logPR.SetbConsole( true);
  LogFile::AddLog( LLOG, "Size of Record buffer %d bytes (int16_t * MAXSAMPLE %d * Nb Buffer %d)", ((CRecorder *)pRecorder)->GetSizeoffSamplesRecordBuffer(), MAXSAMPLE, ((CRecorder *)pRecorder)->GetiNbSampleBuffer());
  logPR.SetbConsole( bMemo);*/
  // On lance l'acquisition
  pRecorder->StartAcquisition();
#if defined(__IMXRT1062__) // Teensy 4.1
  if (paramsOperateur.iHeterSel == HSF_NOSEL)
    ((CRecorderH *)pRecorder)->SetFiltreSel( false);
  else if (paramsOperateur.iHeterSel == HSF_SELECTIVE)
    ((CRecorderH *)pRecorder)->SetFiltreSel( true);
#endif
  // Autorisation audio
  TurnAudioON();
#ifdef WITHBLUETOOTH
  // Autorisation éventuelle du Bluetooth
  if (commonParams.bBluetooth)
    digitalWriteFast( OPT_ALIM_BLE, LOW);
  uiFMEHoldingTime = 0;
#endif
  ((CRecorderH *)pRecorder)->SetAGC(paramsOperateur.bAGCHet);
  //Serial.println("CModeHeterodyne::BeginMode OK");
}

//-------------------------------------------------------------------------
// End of the mode
void CModeHeterodyne::EndMode()
{
  // Stop alimentation audio et Bluetooth
  TurnAudioOFF();
#ifdef WITHBLUETOOTH
  digitalWriteFast( OPT_ALIM_BLE, HIGH);
#endif
  // We restore the operator parameters
  ReadParams();
  // Call of the parent method
  CModeGenericRec::EndMode();
}

//-------------------------------------------------------------------------
// Reading saved settings
// return true if OK
bool CModeHeterodyne::ReadParams()
{
  // Do nothing
  return true;
}

//#define TESTTIMEPRINT
#ifdef TESTTIMEPRINT
  // Total Print time every 300ms 82/84ms
  // PrePrint      = 1ms
  // AddPrint      = 22.3ms
  // Generic print = 60ms
  unsigned long uiTestTimePrint1, uiTestTimePrint2, uiTestTimePrint3, uiTestTimePrint4;
#endif

//-------------------------------------------------------------------------
//! \brief  To display information outside of modifiers at the begin of PrintMode
void CModeHeterodyne::PrePrint()
{
  //Serial.println("CModeHeterodyne::PrePrint");
#ifdef TESTTIMEPRINT
  uiTestTimePrint1 = micros();
#endif
  char sTemp[25];
  // Read heterodyne wheel (<1ms)
  uint16_t uiVal = ADC0Read( ANALOGPINHETERODYNE);
  if (paramsOperateur.iAutoHeter == HAM_AUTO or paramsOperateur.iAutoHeter == HAM_ALWAYSAUTO)
    // +/-10kHz around FME
    fFreqH = fFME + (((float)uiVal * fStepAuto) - 10.0);
    //Serial.printf("H Auto FME %6.2fkHz, fFreqH %6.2fkHz, fStepAuto %6.2fkHz\n", fFME, fFreqH, fStepAuto);
  else
    // Direct frequency
    fFreqH = 10.0 + (float)uiVal * fStepADC;
  //Serial.printf("fStepADC %f, uiVal %d, fFreqH %f\n", fStepADC, uiVal, fFreqH);

  // Heterodyne frequency display (~2ms)
  sprintf( sTemp, "%5.1f", fFreqH);
  pDisplay->setFont(u8g2_font_7x13B_mf);
  //pDisplay->setCursor( 0, 0);
  //pDisplay->print( txtFrHet[commonParams.iLanguage]);
  //pDisplay->setCursor( 57, 0);
  pDisplay->setCursor( 37, 0);
  pDisplay->print( "kHz");
  pDisplay->setDrawColor(0);
  //pDisplay->setCursor( 20, 0);
  pDisplay->setCursor( 0, 0);
  pDisplay->print( sTemp);
  pDisplay->drawLine( 0, 13, 70, 13);
  pDisplay->setDrawColor(1);
  // Restitution de la fonte normale
  pDisplay->setFont(u8g2_font_6x10_mf);
#ifdef TESTTIMEPRINT
  Serial.printf("CModeHeterodyne::PrePrint %dµs\n", micros()-uiTestTimePrint1);
#endif
  //Serial.println("CModeHeterodyne::PrePrint OK");
}

//-------------------------------------------------------------------------
// To display information outside of modifiers
void CModeHeterodyne::AddPrint()
{
  //Serial.println("CModeHeterodyne::AddPrint");
#ifdef TESTTIMEPRINT
  uiTestTimePrint1 = micros();
#endif
  char sTemp[25];
  if (iCurrentSecond != second())
  {
    iCurrentSecond = second();
    // Test si erreur DMA
    if (pRecorder->IsDMAError())
    {
      iCptMulPar = -1; // Erreur toujours affichée, seul un A/M corrige le problème
      LstModifParams[IDXHHOUR   ]->SetbValid( false);
      LstModifParams[IDXMULTIPAR]->SetbValid( false);
      LstModifParams[IDXHERDMA  ]->SetbValid( true );
    }
    // Set hour or thresold
    if (iCptMulPar == 0)
    {
      // Fin de la visualisation du multiparamètres ou d'une erreur DMA
      LstModifParams[IDXHHOUR   ]->SetbValid( true );
      LstModifParams[IDXHRECORD ]->SetbValid( true );
      LstModifParams[IDXMULTIPAR]->SetbValid( false);
      LstModifParams[IDXHERDMA  ]->SetbValid( false);
      LstModifParams[IDXMULTIPAR]->StopModif();
      // Effacement de la ligne
      pDisplay->setCursor( 0, 12);
      pDisplay->print( "                     ");
      bModifMulPar = false;
      bModifParam  = false;
      iCptMulPar--;
    }
    else if (iCptMulPar > 0 and !bModifTh)
      iCptMulPar--;
    if (iCptAffWav == 0)
    {
      // Fin de la visualisation du nom du dernier fichier wav
      if (paramsOperateur.bGraphHet)
        LstModifParams[IDXHFILEA]->SetbValid( false);
      LstModifParams[IDXHAGC  ]->SetbValid(  true);
      iCptAffWav--;
    }
    else
      iCptAffWav--;
  }
  // Set recording status
  if (pRecorder->IsRecording())
    iRecordTime = (int)((CRecorder *)pRecorder)->GetRecordDuration();
  if (pRecorder->IsRecording() and iRecordMode != PREC)
  {
    // Start recording
    iRecordMode = PREC;
    LstModifParams[IDXHAGC  ]->SetbValid( false);
    LstModifParams[IDXHTIMER]->SetbValid( true );
  }
  else if (!pRecorder->IsRecording() and iRecordMode == PREC)
  {
    // Stop recording
    if (paramsOperateur.bAutoPlay)
    {
      bAutoPlay = true;
      CModePlayer::SetAutomaticPlay();
    }
    iRecordMode = PBREAK;
    LstModifParams[IDXHTIMER]->SetbValid( false);
    if (paramsOperateur.bGraphHet)
    {
      iCptAffWav = 5;
      LstModifParams[IDXHFILEA]->SetbValid( true);
      LstModifParams[IDXHAGC  ]->SetbValid(false);
    }
  }
  
  // Get FME
  int iNivMaxR, iNivMaxL, iFreqR, iFreqL;
  ((CRecorder *)pRecorder)->GetNiveauMax( &iNivMaxR, &iFreqR, &iNivMaxL, &iFreqL);
  //Serial.printf("iNivMaxR %d, iFreqR %d, iMemoMaxLevel %d\n", iNivMaxR, iFreqR, iMemoMaxLevel);
  float fNewFME = (float)iFreqR;
  if (iNivMaxR > iMemoMaxLevel and fNewFME >= (float)uiMinIntFreq and fNewFME <= (float)uiMaxIntFreq)
  {
    iMaxLevel = iNivMaxR;
    iMemoMaxLevel = iNivMaxR;
    // Get FME
    if (fNewFME >= 15.0 and fNewFME <= 120.0)
      fFME = fNewFME;
    //Serial.printf("Det on %6.2fkHz = %ddB, FMin %d\n", fFME, iMaxLevel, paramsOperateur.uiFreqMin);
    // Minimum time to keep a detection
    uiFMEHoldingTime = millis() + FMEHOLDINGTIME;
    LstModifParams[IDXHFME ]->SetbValid( true);
    LstModifParams[IDXHNIVA]->SetbValid( true);
  }
  else if (millis() > uiFMEHoldingTime)
  {
    // No detection
    //Serial.printf("No Det %ddB\n", iMaxL);
    iMaxLevel = -120;
    uiFMEHoldingTime = millis() + FMEHOLDINGTIME;
    LstModifParams[IDXHFME ]->SetbValid( false);
    LstModifParams[IDXHNIVA]->SetbValid( false);
  }
  else
  {
    // The frequency is no longer active, it decreases the max level to give it less importance
    iMemoMaxLevel -= 1;
  }
  if (paramsOperateur.bGraphHet)
  {
    // Get actives frequencies
    //Serial.printf("CModeHeterodyne::AddPrint idxFMin %d, idxFMax %d\n", pRecorder->GetidxFMin(), pRecorder->GetidxFMax());
    uint8_t *pTbDetect = ((CRecorder *)pRecorder)->GetTbNbDetect();
    for (int i=pRecorder->GetidxFMin(); i<=pRecorder->GetidxFMax(); i++)
    {
      if (pTbDetect[i] >= paramsOperateur.iNbDetect)
        graphe.SetFrequence( (int)pRecorder->GetFreqIdx(i));
    }
    ((CRecorder *)pRecorder)->RAZTbNbDetect();
    
    // Graph display (~2ms)
    graphe.PrintGraphe( (int)fFreqH);
  }    

  // Sample frequency display
  pDisplay->setFont(u8g2_font_5x7_mf);
  pDisplay->setCursor( 86, 0);
  switch (paramsOperateur.uiFe)
  {
  case FE250KHZ: pDisplay->print( "250"); break;
  case FE500KHZ: pDisplay->print( "500"); break;
  default:
  case FE384KHZ: pDisplay->print( "384"); break;
  }
  pDisplay->setCursor( 86, 6);
  pDisplay->print( "kHz");

  // Restitution de la fonte normale
  pDisplay->setFont(u8g2_font_6x10_mf);
  // Si sonde présente, affichage temérature et humidité
  if (IsTempSensor() and !bModifMulPar)
  {
    // Temperature with BMP280 and BME280
    sprintf( sTemp, "%+02d%c", (int)GetTemperature(), 176);
    pDisplay->setCursor( 52, 12);
    pDisplay->print( sTemp);
    if (GetHumidity() > 5.0)
    {
      // Humidity only on BME280
      sprintf( sTemp, "%2d%%", GetHumidity());
      pDisplay->setCursor( 77, 12);
      pDisplay->print( sTemp);
    }
  }
#if defined(__IMXRT1062__) // Teensy 4.1
#ifdef WITHBLUETOOTH
  if (commonParams.bBluetooth)
  {
    // Dessin Blutooth
    int x=94, y=0;  // Coin haut gauche, dimensions 4x8 pixels
    pDisplay->drawLine( x+2, y  , x+2, y+8);
    pDisplay->drawLine( x+2, y  , x+4, y+2);
    pDisplay->drawLine( x+4, y+2, x  , y+6);
    pDisplay->drawLine( x+2, y+8, x+4, y+6);
    pDisplay->drawLine( x+4, y+6, x  , y+2);
  }
#endif
#endif
  // Initialise la fréquence hétérodyne
  ((CRecorderH *)pRecorder)->SetFrHeterodyne( fFreqH);

  if (millis() > uiTimeCheck)
  {
    if (!max17043.isOK())
    {
      // En absence du module max, mesure classique de la batterie
      CheckBatteries();
      // Next time for check batteries
      uiTimeCheck = millis() + TIMECHECKBATTERY;
      // Mesure temperature
      LectureTemperature();
      // Init ADC for hétérodyne
      ADC0Init( true);
    }
    else if (max17043.isOK() and !bMAXError)
    {
      // Module Max présent et non en erreur
      if (!bMAXPowerOn)
      {
        // Power On MAX and wait 125ms for mesure
        CheckBatteries(MAX_POWER);
        uiTimeCheck = millis() + 125;
      }
      else
      {
        bool bOK = false;
        if (max17043.isOK())
          // Test battery
          bOK = CheckBatteries(MAX_MESURE);
        else
          // Test battery
          bOK = CheckBatteries(MAX_WAITMESURE);
        if (!bOK)
        {
          LogFile::AddLog( LLOG, "Error CheckBatteries !");
          bMAXError = true;
        }
        else
          // Next time for check batteries
          uiTimeCheck = millis() + TIMECHECKBATTERY;
      }
      // Mesure temperature
      LectureTemperature();
      // Init ADC for hétérodyne
      ADC0Init( true);
    }
  }
#ifdef TESTTIMEPRINT
  Serial.printf("CModeHeterodyne::AddPrint %dµs\n", micros()-uiTestTimePrint1);
#endif
  //Serial.println("CModeHeterodyne::AddPrint OK");
}
  
//-------------------------------------------------------------------------
// Keyboard order processing
// If the key is a mode change key, returns the requested mode
// This method is called regularly by the main loop
// By default, handles modifiers
int CModeHeterodyne::KeyManager(
  unsigned short key  // Touch to treat
  )
{
  //Serial.println("CModeHeterodyne::KeyManager");
  int iNewMode = NOMODE;
  switch( key)
  {
  case K_UP  :
    if (bModifTh)
      return CModeGeneric::KeyManager( key);
    else if (bModifMulPar)
    {
      LstModifParams[IDXMULTIPAR]->StopModif();
      iCptMulPar = TIMEAFFMULPAR;
      iMultiPar++;    // Next parameter
      if (iMultiPar > HMUL_HAM or (iMultiPar > HMUL_THR and paramsOperateur.iAutoHeter > HAM_AUTO))
        iMultiPar = HMUL_AGC;
      LstModifParams[IDXMULTIPAR]->GetString();
      LstModifParams[IDXMULTIPAR]->StartModif();
    }
    break;
  case K_DOWN:
    if (bModifTh)
      return CModeGenericRec::KeyManager( key);
    if (bModifMulPar)
    {
      LstModifParams[IDXMULTIPAR]->StopModif();
      iCptMulPar = TIMEAFFMULPAR;
      iMultiPar--;    // Prev parameter
      if (iMultiPar < HMUL_AGC and paramsOperateur.iAutoHeter > HAM_AUTO)
        iMultiPar = HMUL_THR;
      else if (iMultiPar < HMUL_AGC)
        iMultiPar = HMUL_HAM;
      LstModifParams[IDXMULTIPAR]->GetString();
      LstModifParams[IDXMULTIPAR]->StartModif();
    }
    break;
  case K_PUSH:
    if (bModifTh)
    {
      // Arret de la modif du seuil
      LstModifParams[IDXHSEUILR ]->SetParam();
      LstModifParams[IDXHSEUILR ]->StopModif();
      LstModifParams[IDXMULTIPAR]->StartModif();
      LstModifParams[IDXHSEUILR ]->SetbValid( false);
      LstModifParams[IDXMULTIPAR]->SetbValid( true);
      LstModifParams[IDXHSEUILR ]->SetEditable( false);
      sprintf( txtMPAR[HMUL_THR], txtCMPAR[CModeGeneric::GetCommonsParams()->iLanguage][HMUL_THR], paramsOperateur.iSeuilDet);
      bModifTh = false;
      // Calcul du seuil de chaque canal
      pRecorder->ThresholdCalculation();
    }
    else if (!bModifMulPar)
    {
      // On affiche le multiparamètres en mode modification pendant 3s
      iCptMulPar = TIMEAFFMULPAR;
      bModifMulPar = true;
      LstModifParams[IDXHHOUR   ]->SetbValid( false);
      LstModifParams[IDXHRECORD ]->SetbValid( false);
      LstModifParams[IDXMULTIPAR]->SetbValid( true );
      LstModifParams[IDXMULTIPAR]->StartModif();
      idxSelParam = IDXMULTIPAR;
      bModifParam = true;
      // Effacement de la ligne
      pDisplay->setCursor( 0, 12);
      pDisplay->print( "                     ");
    }
    else
    {
      iCptMulPar = TIMEAFFMULPAR;
      switch (iMultiPar)
      {
      case HMUL_AGC: // AGC On/Off
        paramsOperateur.bAGCHet = !paramsOperateur.bAGCHet;
        ((CRecorderH *)pRecorder)->SetAGC(paramsOperateur.bAGCHet);
        break;
      case HMUL_THR: // Thresold
        bModifTh = true;
        idxSelParam = IDXHSEUILR;
        LstModifParams[IDXMULTIPAR]->StopModif();
        LstModifParams[IDXHSEUILR ]->StartModif();
        LstModifParams[IDXMULTIPAR]->SetbValid( false);
        LstModifParams[IDXHSEUILR ]->SetbValid( true);
        LstModifParams[IDXHSEUILR ]->SetEditable( true);
        break;
      case HMUL_HAM: // Auto On/Off
        if (paramsOperateur.iAutoHeter == HAM_MANUAL)
          paramsOperateur.iAutoHeter = HAM_AUTO;
        else
          paramsOperateur.iAutoHeter = HAM_MANUAL;
        break;
      }
      int iL = CModeGeneric::GetCommonsParams()->iLanguage;
      sprintf( txtMPAR[HMUL_AGC], txtCMPAR[iL][HMUL_AGC], txtYesNo[iL][paramsOperateur.bAGCHet]);
      sprintf( txtMPAR[HMUL_THR], txtCMPAR[iL][HMUL_THR], paramsOperateur.iSeuilDet);
      sprintf( txtMPAR[HMUL_HAM], txtCMPAR[iL][HMUL_HAM], txtModesH[iL][paramsOperateur.iAutoHeter]);
    }
    break;
  case K_MODEB:
    // We go into recording mode or we stop it
    if (iRecordMode == PBREAK and ((CRecorder *)pRecorder)->IsRecording() == false)
    {
      // Beging recording
      pRecorder->StartRecording();
      // Prepare file name list
      for (int i=MAXFILESNAMENOGR-1; i>0; i--)
        strcpy( LstFilesScreen[i], LstFilesScreen[i-1]);
      String str = PrepareStrFile( ((CRecorder *)pRecorder)->GetFileName());
      char sTemp[MAX_LINEPARAM+1];
      str.toCharArray( sTemp, MAX_LINEPARAM);
      strcpy( LstFilesScreen[0], sTemp);
    }
    else if (iRecordMode == PREC and ((CRecorder *)pRecorder)->IsRecording() and ((CRecorder *)pRecorder)->GetRecordDuration() > 1)
    {
      // Stop recording
      pRecorder->StopRecording();
    }
    break;
  case K_MODEA:
    // Swap to player mode : Record mode -> Player -> Parameters -> Record Mode
    iNewMode = MPLAYER;
    break;
  }
  if (bAutoPlay)
    iNewMode = MPLAYER;
  //Serial.println("CModeHeterodyne::KeyManager OK");
  return iNewMode;
}

//-------------------------------------------------------------------------
// Returns the string to display, taking only the n most significant characters
// 123456789012345678901
//  PR001_190126_221812.wav
//  PR001_190126_221812
String CModeHeterodyne::PrepareStrFile(
  String str // Chaine à vérifier
  )
{
  String newStr = str;
  // Copie des 20 derniers caractères (sauf .wav)
  int max  = MAX_LINEPARAM - 1;
  if ((int)newStr.length() > max and (int)newStr.length() <= max+4)
    // On enlève .wav
    newStr = newStr.substring( 0, max);
  else if ((int)newStr.length() > max)
  {
    // On enlève .wav
    newStr = newStr.substring( 0, newStr.length()-4);
    // On enlève les 1er caractères
    newStr = newStr.substring( newStr.length()-max);
  }
  else if ((int)newStr.length() < max)
  {
    // On complète a 15 caractéres éventuellement
    while ((int)newStr.length() < max)
      newStr += " ";
  }
  return newStr;
}

//-------------------------------------------------------------------------
// List the wav files in the root directory
void CModeHeterodyne::ListFiles()
{
  //! List of wav files in the root directory
  std::vector<String> lstWaveFiles;
  char name[80];
  char namelwr[80];
  FsFile file;
  FsFile root;
  lstWaveFiles.clear();
  // Select root directory
  if (!root.open("/"))
    Serial.printf("CModeGeneric::ListFilesErreur sd.chdir(/)\n");
  int iNbWav = 0;
  // Directory files are searched
  while (file.openNext(&root, O_RDONLY))
  {
    // Recovering the file name
    file.getName( name, 80);
    strcpy( namelwr, name);
    strlwr( namelwr);
    // We do not take into account the "System Volume Information" directory nor directories and files other than wav
    if (strcmp( name, "System Volume Information") != 0 and file.isDir() == false and strstr(namelwr, ".wav") != NULL and  strstr(namelwr, "temp.wav") == NULL)
    {
      lstWaveFiles.push_back( name);
      iNbWav++;
    }
    file.close();
    if (iNbWav >= 20)
      break;
  }
  if (lstWaveFiles.size() > 0)
  {
    int iNb = lstWaveFiles.size();
    // Sort ascending alphabetically
    std::sort (lstWaveFiles.begin(), lstWaveFiles.end());
    // 1er fichier
    String str = PrepareStrFile( lstWaveFiles[iNb-1]);
    strcpy( LstFilesScreen[0], str.c_str());
    if (lstWaveFiles.size() > 1)
    {
      str = PrepareStrFile( lstWaveFiles[iNb-2]);
      strcpy( LstFilesScreen[1], str.c_str());
    }
    if (lstWaveFiles.size() > 2)
    {
      str = PrepareStrFile( lstWaveFiles[lstWaveFiles.size()-3]);
      strcpy( LstFilesScreen[2], str.c_str());
    }
  }
}

//-------------------------------------------------------------------------
// Management class of a graph from 0 to 128kHz (1 pixel per kHz) on n lines
// 24 pixels high, 14 pixels for the scale and n pixels for the graph

//-------------------------------------------------------------------------
// Constructor (initialization of parameters to default values)
CGraphe128kHz::CGraphe128kHz(
  int iNLines,  // Nb lines
  int y         // Position y from the top of the graph
  )
{
  // Set parameters
  iY       = y;
  iNbLines = iNLines;
  iFrMin   = 0;
  iFrMax   = 127;
  // Create and clear graphe bitmap
  iSizeGraph = 16*iNbLines;
  pTbGraphe = new uint8_t[iSizeGraph];
  memset( pTbGraphe, 0, iSizeGraph);
  // Defaut scroll time 1s
  uiTimeScroll = 1000;
  uiNextScroll = millis() + uiTimeScroll;
}

//-------------------------------------------------------------------------
// Destructor
CGraphe128kHz::~CGraphe128kHz()
{
  delete [] pTbGraphe;
}

//-------------------------------------------------------------------------
// Set the min max frequencies of the scale
void CGraphe128kHz::SetMinMaxScale(
  int iFMin, // Min frequency in kHz
  int iFMax  // Max frequency in kHz
  )
{
  iFrMin   = iFMin;
  iFrMax   = iFMax;
}
  
//-------------------------------------------------------------------------
// Initialization of the scrolling time of the graph (1s by default)
void CGraphe128kHz::SetTimeScroll(
  int iScroll // Time between two scrolls in ms
  )
{
  uiTimeScroll = iScroll;
  uiNextScroll = millis() + uiTimeScroll;
}

//-------------------------------------------------------------------------
// Initialization of a point in the graph
void CGraphe128kHz::SetFrequence(
  int iFreq // Frequency in kHz
  )
{
  uint8_t tbBits[] = {0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01};
  // Byte index
  int iOctet = iFreq / 8;
  // Bit index
  int iBit = iFreq - (iOctet*8);
  // Set point
  pTbGraphe[iOctet] |= tbBits[iBit];
}

//#define TIMEPRINTGRAPHE
#ifdef TIMEPRINTGRAPHE
  // PrintGraphe = T3.6 3.4ms, T4.1 
  unsigned long uiTestPrintGR;
#endif

//-------------------------------------------------------------------------
// Print graphe
void CGraphe128kHz::PrintGraphe(
  int iFreqH // Heterodyne frequency in kHz
  )
{
#ifdef TIMEPRINTGRAPHE
  uiTestPrintGR = micros();
#endif
  // Scale preparation
  char sTemp[10];
  int x;
  pDisplay->setFont(u8g2_font_4x6_mf);
  // Display value every 20kHz
  for (int i=20; i<125; i+=20)
  {
    sprintf( sTemp, "%d", i);
    x = i;
    int iW = pDisplay->getStrWidth( sTemp);
    // Display frequency
    pDisplay->setCursor( x-(iW/2), iY);
    pDisplay->print( sTemp);
    // Display of the frequency mark
    pDisplay->drawLine( x, iY+6, x, iY+8);
  }
  pDisplay->setFont(u8g2_font_6x10_mf);
  // Horizontal line display
  pDisplay->drawLine( iFrMin, iY+9, iFrMax, iY+9);
  pDisplay->drawLine( iFrMin, iY+6, iFrMin, iY+8);
  pDisplay->drawLine( iFrMax, iY+6, iFrMax, iY+8);
  // Display of the heterodyne frequency slider
  pDisplay->drawLine( iFreqH-1, iY+6, iFreqH-1, iY+12);
  pDisplay->drawLine( iFreqH, iY+6, iFreqH, iY+12);
  pDisplay->drawLine( iFreqH+1, iY+6, iFreqH+1, iY+12);

  // Bitmap display
  pDisplay->drawBitmap( 0, iY+13, 16, iNbLines, pTbGraphe);

  // If it's time to scroll
  if (millis() > uiNextScroll)
  {
    // One point scroll down
    memmove( &pTbGraphe[16], pTbGraphe, iSizeGraph-16);
    // Clear first line
    memset( pTbGraphe, 0, 16);
    // Time for next scroll
    uiNextScroll = millis() + uiTimeScroll;
  }
#ifdef TIMEPRINTGRAPHE
  Serial.printf("PrintGraphe %dµs\n", micros()-uiTestPrintGR);
#endif
}
