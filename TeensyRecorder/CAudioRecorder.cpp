//-------------------------------------------------------------------------
//! \file CAudioRecorder.cpp
//! \brief Classe de gestion du mode Enregistreur Audio
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/* 
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "SdFat.h"
#include <U8g2lib.h>
#include "CAudioRecorder.h"

// Screen manager
extern U8G2 *pDisplay;

// Gestionnaire carte SD
extern SdFs sd;

// Gestionnaire du fichier LogPR.txt
extern LogFile logPR;

// Strings for parameters
const char formIDXATIMER[] = "&$s";
const char formIDXASEUILR[] = "Se. &$dB";
const char formIDXAFME[] = "&$$$$";
const char formIDXANIV[] = "&$$dB";
const char formIDXAHEURE[] = "&$:&$:&$";
const char *txtIDXATIMER[] = { "%03ds",
                               "%03ds",
                               "%03ds",
                               "%03ds" };
const char *txtIDXAFME[] = { "%05.1fkHz",
                             "%05.1fkHz",
                             "%05.1fkHz",
                             "%05.1fkHz" };
const char *txtIDXANIV[] = { "%03ddB",
                             "%03ddB",
                             "%03ddB",
                             "%03ddB" };
const char *txtIDXAHEURE[] = { "%s",
                               "%s",
                               "%s",
                               "%s" };
extern const char *txtIDXASEUILR[];

// Audi recorder mode screen
//   12345678901234567890  y
// 1             384[Bat]  0
// 2 22:12:32 20° 80% [O]  12
// 2 Se. 15dB 20° 80% [O]  12
// 3 110.5KHz -100dB 100s  21
// 4  20 40 60 80 100 120  30
// 5                       38
// 6      Graphe           45
// 7                       54
// X 000000000000000001111
//   001123344566778990012
//   062840628406284062840

//-------------------------------------------------------------------------
//! \class CAudioRecorder
//! \brief Heterodyne mode management class

//-------------------------------------------------------------------------
//! \brief Constructor (initialization of parameters to default values)
CAudioRecorder::CAudioRecorder()
  : graphe(48000, 20, 30) {
  // Set parameters
  fFME = 0.0;
  iMaxLevel = -120;
  iMemoMaxLevel = -120;
  iRecordMode = PBREAK;
  iRecordTime = 0;
  // Creating Parameter Modifier Lines
  // IDXARECORD   Record status
  LstModifParams.push_back(new CPlayRecModifier(&iRecordMode, false, true, 25, 9, 100, 11));
  // IDXATIMER    Record time
  LstModifParams.push_back(new CModificatorInt(formIDXATIMER, false, txtIDXATIMER, &iRecordTime, 0, 999, 1, 96, 21));
  // IDXASEUILR   Relative detection threshold in dB (5-99)
  LstModifParams.push_back(new CModificatorInt(formIDXASEUILR, false, txtIDXASEUILR, &paramsOperateur.iSeuilDet, 5, 99, 3, 0, 12));
  // IDXAFME      Detected frequency of maximum energy
  LstModifParams.push_back(new CFloatModifier(formIDXAFME, false, txtIDXAFME, &fFME, 10, 120, 0.1, 0, 21));
  // IDXANIVA     Maximum level detected in numbers
  LstModifParams.push_back(new CModificatorInt(formIDXANIV, false, txtIDXANIV, &iMaxLevel, -120, -40, 1, 54, 21));
  // IDXABAT      Internal battery charge level in %
  LstModifParams.push_back(new CBatteryModifier(&iNivBatLiPo, 24, 8, 100, 1));
  // IDXAHOUR     Heure courante                                                     Heure 09/12/22
  LstModifParams.push_back(new CHourModifier(formIDXAHEURE, false, txtIDXAHEURE, NULL, 0, 12));
  // IDXBARG      Barregraph for max level
  LstModifParams.push_back(new CDrawBarModifier(&iNivMaxBG, 75, 8, 0, 0, -120, -50));
}

//-------------------------------------------------------------------------
//! \brief Creation du gestionnaire d'enregistrement
void CAudioRecorder::CreateRecorder() {
  //Serial.println("CAudioRecorder::CreateRecorder");
  pRecorder = new CRecorderA(&paramsOperateur, 2);
}

const char txtRamUsageARec[] = "Ram after CAudioRecorder::BeginMode";

//-------------------------------------------------------------------------
//! \brief Beginning of the mode
void CAudioRecorder::BeginMode() {
  // Lecture des paramètres
  CGenericMode::ReadParams();
  // Set some parameters
  paramsOperateur.bTypeSeuil = false;  // Relative threshold
  paramsOperateur.bFiltre = true;      // Low pass filter (using DSP)
  //paramsOperateur.bFiltre   = false;                     // Low pass filter (using DSP)
  paramsOperateur.bExp10 = false;           // Pas en expansion de temps X10
  paramsOperateur.iFreqFiltreHighPass = 0;  // No high pass filter
  paramsOperateur.uiGainNum = GAIN0dB;      // Numeric gain 0dB
  bModifSeuil = false;
  // Call of the parent method without read params
  bReadParams = false;
  CModeGenericRec::BeginMode();
  // Initialization
  iCurrentSecond = 61;
  LstModifParams[IDXARECORD]->SetEditable(false);
  LstModifParams[IDXATIMER]->SetEditable(false);
  LstModifParams[IDXATIMER]->SetbValid(false);
  LstModifParams[IDXASEUILR]->SetEditable(false);
  LstModifParams[IDXAFME]->SetEditable(false);
  LstModifParams[IDXANIVA]->SetEditable(false);
  LstModifParams[IDXAHOUR]->SetEditable(false);
  LstModifParams[idxSelParam]->SetbSelect(false);
  // Par défaut, heure visible et seuil invisible
  LstModifParams[IDXASEUILR]->SetbValid(false);
  iCptTh = 0;
  int iRafGrape = (int)(paramsOperateur.fRefreshGRH * 1000.0);
  graphe.SetTimeScroll(iRafGrape);
  // Test battery
  CheckBatteries(MAX_WAITMESURE);
  uiTimeCheck = millis() + TIMECHECKBATTERY;
  // Width of a FFT channel in Hz
  uint32_t uiFe = 48000;
  int iMinScale, iMaxScale;
  switch (paramsOperateur.uiFeA) {
    default:
    case FE24KHZ:
      uiFe = 24000;
      iMinScale = 1;
      iMaxScale = 12000;
      break;
    case FE48KHZ:
      uiFe = 48000;
      iMinScale = 1;
      iMaxScale = 20000;
      break;
    case FE96KHZ:
      uiFe = 96000;
      iMinScale = 1;
      iMaxScale = 45000;
      break;
    case FE192KHZ:
      uiFe = 192000;
      iMinScale = 1;
      iMaxScale = 90000;
      break;
  }
  if (paramsOperateur.uiFreqMinA > iMaxScale) {
    //Serial.printf("CAudioRecorder::BeginMode A paramsOperateur.uiFreqMinA %d, iMaxScale %d\n", paramsOperateur.uiFreqMinA, iMaxScale);
    paramsOperateur.uiFreqMinA = iMinScale;
    paramsOperateur.uiFreqMaxA = iMaxScale;
  } else if (paramsOperateur.uiFreqMaxA > iMaxScale) {
    //Serial.printf("CAudioRecorder::BeginMode B paramsOperateur.uiFreqMinA %d, iMaxScale %d\n", paramsOperateur.uiFreqMinA, iMaxScale);
    paramsOperateur.uiFreqMaxA = iMaxScale;
  } else {
    //Serial.printf("CAudioRecorder::BeginMode C paramsOperateur.uiFreqMinA %d, iMaxScale %d\n", paramsOperateur.uiFreqMinA, iMaxScale);
    iMinScale = paramsOperateur.uiFreqMinA;
    iMaxScale = paramsOperateur.uiFreqMaxA;
  }
  graphe.SetFe(uiFe);
  graphe.SetMinMaxScale(iMinScale, iMaxScale);
  fMinIntFreq = (float)iMinScale / 1000;
  fMaxIntFreq = (float)iMaxScale / 1000;
  uiFFTChannel = (uiFe / 2) / (FFTLEN / 2);
  // Print Ram usage
  PrintRamUsage((char *)txtRamUsageARec);
  bool bMemo = logPR.GetbConsole();
  logPR.SetbConsole(true);
  LogFile::AddLog(LLOG, "Size of Record buffer %d bytes (int16_t * MAXSAMPLE %d * Nb Buffer %d)", ((CRecorder *)pRecorder)->GetSizeoffSamplesRecordBuffer(), MAXSAMPLE, ((CRecorder *)pRecorder)->GetiNbSampleBuffer());
  logPR.SetbConsole(bMemo);
  // On lance l'acquisition
  pRecorder->StartAcquisition();
  // Autorisation audio
  TurnAudioON();
#ifdef WITHBLUETOOTH
  // Autorisation éventuelle du Bluetooth
  if (commonParams.bBluetooth) {
    digitalWriteFast(OPT_ALIM_BLE, LOW);
    delay(100);
    //digitalWriteFast(OPT_INIT_BLE, LOW);
    //uiBLETime = millis() + 15000;
  }
#endif
  uiFMEHoldingTime = 0;
}

//-------------------------------------------------------------------------
//! \brief End of the mode
void CAudioRecorder::EndMode() {
  // Stop alimentation audio et Bluetooth
  TurnAudioOFF();
#ifdef WITHBLUETOOTH
  digitalWriteFast(OPT_ALIM_BLE, HIGH);
#endif
  // We restore the operator parameters
  ReadParams();
  // Call of the parent method
  CModeGenericRec::EndMode();
}

//-------------------------------------------------------------------------
//! \brief  Reading saved settings
//! \return true if OK
bool CAudioRecorder::ReadParams() {
  // Do nothing
  return true;
}

//-------------------------------------------------------------------------
//! \brief To display information outside of modifiers
void CAudioRecorder::AddPrint() {
  char sTemp[25];
  if (iCurrentSecond != second()) {
    iCurrentSecond = second();

    // Set hour or thresold
    if (iCptTh == 0) {
      LstModifParams[IDXAHOUR]->SetbValid(true);
      LstModifParams[IDXASEUILR]->SetbValid(false);
      LstModifParams[IDXASEUILR]->StopModif();
      bModifSeuil = false;
      bModifParam = false;
    } else if (iCptTh > 0)
      iCptTh--;
  }
  // Set max level
  int iNivMaxL, iFreqR, iFreqL;
  ((CRecorder *)pRecorder)->GetNiveauMax(&iNivMaxBG, &iFreqR, &iNivMaxL, &iFreqL);
  // Affichage de la barre de niveau
  LstModifParams[IDXBARG]->SetRedraw(true);
  LstModifParams[IDXBARG]->PrintParam(0, 52);
  // Set recording status
  if (((CRecorder *)pRecorder)->IsRecording()) {
    if (iRecordMode != PREC)
      // Recording
      iRecordMode = PREC;
    LstModifParams[IDXATIMER]->SetbValid(true);
    iRecordTime = (int)((CRecorder *)pRecorder)->GetRecordDuration();
  } else  // if (iRecordMode == PREC)
  {
    // not recording
    iRecordMode = PBREAK;
    LstModifParams[IDXATIMER]->SetbValid(false);
  }

  // Get FME
  float fNewFME = (float)iFreqR;
  if (iNivMaxL > iMemoMaxLevel and fNewFME >= fMinIntFreq and fNewFME <= fMinIntFreq) {
    iMaxLevel = iNivMaxL;
    iMemoMaxLevel = iNivMaxL;
    // Get FME
    fFME = fNewFME;
    //Serial.printf("Det on %6.2fkHz = %ddB\n", fFME, iMaxLevel);
    // Minimum time to keep a detection
    uiFMEHoldingTime = millis() + FMEHOLDINGTIME;
    LstModifParams[IDXAFME]->SetbValid(true);
    LstModifParams[IDXANIVA]->SetbValid(true);
  } else if (millis() > uiFMEHoldingTime) {
    // No detection
    //Serial.printf("No Det %ddB\n", iMaxL);
    iMaxLevel = -120;
    if (fFME < 0.1)
      // By default, FME=40kHz, else, last FME
      fFME = 40.0;
    uiFMEHoldingTime = millis() + FMEHOLDINGTIME;
    LstModifParams[IDXAFME]->SetbValid(false);
    LstModifParams[IDXANIVA]->SetbValid(false);
  } else {
    // The frequency is no longer active, it decreases the max level to give it less importance
    iMemoMaxLevel -= 1;
  }
  // Get actives frequencies
  uint8_t *pTbDetect = ((CRecorder *)pRecorder)->GetTbNbDetect();
  for (int i = pRecorder->GetidxFMin(); i <= pRecorder->GetidxFMax(); i++) {
    if (pTbDetect[i] >= paramsOperateur.iNbDetect)
      graphe.SetFrequence(i);
  }
  ((CRecorder *)pRecorder)->RAZTbNbDetect();

  // Graph display (~2ms)
  graphe.PrintGraphe();

  // Sample frequency display
  pDisplay->setFont(u8g2_font_5x7_mf);
  pDisplay->setCursor(79, 0);
  switch (paramsOperateur.uiFeA) {
    case FE24KHZ: pDisplay->print("24"); break;
    case FE48KHZ: pDisplay->print("48"); break;
    case FE96KHZ: pDisplay->print("96"); break;
    default:
    case FE192KHZ: pDisplay->print("192"); break;
  }
  pDisplay->setCursor(79, 6);
  pDisplay->print("kHz");

  // Restitution de la fonte normale
  pDisplay->setFont(u8g2_font_6x10_mf);
  // Si sonde présente, affichage temérature et humidité
  if (IsTempSensor()) {
    sprintf(sTemp, "%+02d%c", (int)GetTemperature(), 176);
    pDisplay->setCursor(52, 12);
    pDisplay->print(sTemp);
    if (GetHumidity() > 5.0) {
      // Humidity only on BME280
      sprintf(sTemp, "%2d%%", GetHumidity());
      pDisplay->setCursor(77, 12);
      pDisplay->print(sTemp);
    }
  }
#if defined(__IMXRT1062__)  // Teensy 4.1
#ifdef WITHBLUETOOTH
  if (commonParams.bBluetooth) {
    // Dessin Blutooth
    int x = 94, y = 0;  // Coin haut gauche, dimensions 4x8 pixels
    pDisplay->drawLine(x + 2, y, x + 2, y + 8);
    pDisplay->drawLine(x + 2, y, x + 4, y + 2);
    pDisplay->drawLine(x + 4, y + 2, x, y + 6);
    pDisplay->drawLine(x + 2, y + 8, x + 4, y + 6);
    pDisplay->drawLine(x + 4, y + 6, x, y + 2);
  }
#endif
#endif

  if (millis() > uiTimeCheck) {
    if (!max17043.isOK()) {
      // En absence du module max, mesure classique de la batterie
      CheckBatteries();
      // Next time for check batteries
      uiTimeCheck = millis() + TIMECHECKBATTERY;
      // Mesure temperature
      LectureTemperature();
      // Init ADC for hétérodyne
      ADC0Init(true);
    } else if (max17043.isOK() and !bMAXError) {
      // Module Max présent et non en erreur
      if (!bMAXPowerOn) {
        // Power On MAX and wait 125ms for mesure
        CheckBatteries(MAX_POWER);
        uiTimeCheck = millis() + 125;
      } else {
        bool bOK = false;
        if (max17043.isOK())
          // Test battery
          bOK = CheckBatteries(MAX_MESURE);
        else
          // Test battery
          bOK = CheckBatteries(MAX_WAITMESURE);
        if (!bOK) {
          LogFile::AddLog(LLOG, "Error CheckBatteries !");
          bMAXError = true;
        } else
          // Next time for check batteries
          uiTimeCheck = millis() + TIMECHECKBATTERY;
      }
      // Mesure temperature
      LectureTemperature();
      // Init ADC for hétérodyne
      ADC0Init(true);
    }
  }
  /*if (uiBLETime > 0 and millis() > uiBLETime)
  {
    uiBLETime = 0;
    digitalWriteFast(OPT_INIT_BLE, HIGH);
  }*/
}

//-------------------------------------------------------------------------
//! \brief Keyboard order processing
//! If the key is a mode change key, returns the requested mode
//! This method is called regularly by the main loop
//! By default, handles modifiers
//! \param key Touch to treat
int CAudioRecorder::KeyManager(
  unsigned short key) {
  int iNewMode = NOMODE;
  switch (key) {
    case K_UP:
    case K_DOWN:
      if (!bModifSeuil) {
        // On affiche le seuil en mode modification pendant 3s
        iCptTh = 3;  // To see threshold 3s
        bModifSeuil = true;
        LstModifParams[IDXAHOUR]->SetbValid(false);
        LstModifParams[IDXASEUILR]->SetbValid(true);
        LstModifParams[IDXASEUILR]->StartModif();
        idxSelParam = IDXASEUILR;
        bModifParam = true;
      } else {
        // Seuil en cours de modification
        CModeGeneric::KeyManager(key);
        iCptTh = 3;
      }
      break;
    case K_PUSH:
      if (bModifSeuil) {
        // Stop de la modification du seuil
        LstModifParams[IDXASEUILR]->SetParam();
        LstModifParams[IDXASEUILR]->StopModif();
        LstModifParams[IDXAHOUR]->SetbValid(true);
        LstModifParams[IDXASEUILR]->SetbValid(false);
        bModifSeuil = false;
        bModifParam = false;
        iCptTh = -1;
        // Calcul du seuil de chaque canal
        pRecorder->ThresholdCalculation();
      }
      break;
    case K_MODEB:
      // We go into recording mode or we stop it
      if (iRecordMode == PBREAK and ((CRecorder *)pRecorder)->IsRecording() == false) {
        // Beging recording
        pRecorder->StartRecording();
        iRecordMode = PREC;
      } else if (iRecordMode == PREC and ((CRecorder *)pRecorder)->IsRecording()) {
        // Stop recording
        pRecorder->StopRecording();
        iRecordMode = PBREAK;
      }
      break;
    case K_MODEA:
      // Swap to player mode : Record mode -> Player -> Parameters -> Record Mode
      iNewMode = MPLAYER;
      break;
  }
  return iNewMode;
}

//-------------------------------------------------------------------------
//! \class CGrapheXkHz
//! \brief Management class of a graph from 0 to X kHz on n lines
//! 24 pixels high, 14 pixels for the scale and n pixels for the graph

//-------------------------------------------------------------------------
//! \brief Constructor (initialization of parameters to default values)
//! \param ife Sample frequency (48, 96 or 192 kHz)
//! \param iNLines Nb lines
//! \param y Position y from the top of the graph
CGrapheXkHz::CGrapheXkHz(
  int ife,
  int iNLines,
  int y) {
  iNbLines = iNLines;
  iY = y;
  iFrMin = 0;
  iFrMax = 127;
  SetFe(ife);
  // Create and clear graphe bitmap
  iSizeGraph = 16 * iNbLines;
  pTbGraphe = new uint8_t[iSizeGraph];
  memset(pTbGraphe, 0, iSizeGraph);
  // Defaut scroll time 1s
  uiTimeScroll = 1000;
  uiNextScroll = millis() + uiTimeScroll;
}

//-------------------------------------------------------------------------
//! \brief Destructor
CGrapheXkHz::~CGrapheXkHz() {
  delete[] pTbGraphe;
}

//-------------------------------------------------------------------------
//! \brief Set the sample frequency
//! \param ife Sample frequency (48, 96 or 192 kHz)
void CGrapheXkHz::SetFe(
  int ife) {
  iFe = ife;
  switch (iFe) {
    case 48000:
      fChannel = (48.0 / 2.0) / (256.0 / 2.0);
      fMaxFr = 48.0 / 2.0;
      fPixel = 128.0 / fMaxFr;
      fScale = 5.0;
      fMinFR = 5.0;
      break;
    case 96000:
      fChannel = (96.0 / 2.0) / (256.0 / 2.0);
      fMaxFr = 96.0 / 2.0;
      fPixel = 128.0 / fMaxFr;
      fScale = 10.0;
      fMinFR = 5.0;
      break;
    case 192000:
      fChannel = (192.0 / 2.0) / (256.0 / 2.0);
      fMaxFr = 192.0 / 2.0;
      fPixel = 128.0 / fMaxFr;
      fScale = 10.0;
      fMinFR = 10.0;
      break;
  }
}

//-------------------------------------------------------------------------
//! \brief Set the min max frequencies of the scale
//! \param iFMin Min frequency in Hz
//! \param iFMax Max frequency in Hz
void CGrapheXkHz::SetMinMaxScale(
  int iFMin,
  int iFMax) {
  iFrMin = (float)iFMin / 1000.0 * fPixel;
  iFrMax = (float)iFMax / 1000.0 * fPixel;
}

//-------------------------------------------------------------------------
//! \brief Initialization of the scrolling time of the graph (1s by default)
//! \param iScroll Time between two scrolls in ms
void CGrapheXkHz::SetTimeScroll(
  int iScroll) {
  uiTimeScroll = iScroll;
  uiNextScroll = millis() + uiTimeScroll;
}

//-------------------------------------------------------------------------
//! \brief Initialization of a point in the graph
//! \param iIdx Frequency index
void CGrapheXkHz::SetFrequence(
  int iIdx) {
  uint8_t tbBits[] = { 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01 };
  // Byte index
  int iOctet = iIdx / 8;
  // Bit index
  int iBit = iIdx - (iOctet * 8);
  // Set point
  pTbGraphe[iOctet] |= tbBits[iBit];
}

//-------------------------------------------------------------------------
//! \brief Print graph
//! \param iFreqH Heterodyne frequency in kHz
void CGrapheXkHz::PrintGraphe() {
  // Scale preparation
  char sTemp[10];
  float fX;
  int x;
  pDisplay->setFont(u8g2_font_4x6_mf);
  // Display value every fScale
  for (float f = fMinFR; f < fMaxFr; f += fScale) {
    sprintf(sTemp, "%2.0f", f);
    fX = fPixel * f;
    x = (int)fX;
    int iW = pDisplay->getStrWidth(sTemp);
    // Display frequency
    pDisplay->setCursor(x - (iW / 2), iY);
    pDisplay->print(sTemp);
    // Display of the frequency mark
    pDisplay->drawLine(x, iY + 6, x, iY + 8);
  }
  pDisplay->setFont(u8g2_font_6x10_mf);
  // Horizontal line display
  pDisplay->drawLine(iFrMin, iY + 9, iFrMax, iY + 9);
  pDisplay->drawLine(iFrMin, iY + 6, iFrMin, iY + 8);
  pDisplay->drawLine(iFrMax, iY + 6, iFrMax, iY + 8);

  // Bitmap display
  pDisplay->drawBitmap(0, iY + 13, 16, iNbLines, pTbGraphe);

  // If it's time to scroll
  if (millis() > uiNextScroll) {
    // One point scroll down
    memmove(&pTbGraphe[16], pTbGraphe, iSizeGraph - 16);
    // Clear first line
    memset(pTbGraphe, 0, 16);
    // Time for next scroll
    uiNextScroll = millis() + uiTimeScroll;
  }
}
