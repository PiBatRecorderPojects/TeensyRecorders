//-------------------------------------------------------------------------
//! \file CSynchroLink.h
//! \brief Classe de gestion de la liaison avec l'ESP32 pour les enregistrements stéréo synchro
//! \details class ESP32Link
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <arduino.h>
#include "Const.h"
#include "SyncTopESP32/CCodeLink.h"

/*
 * CSynchroLink
 *   CMasterLink
 *   CSlaveLink
 */
 
#ifndef CESP32LLINK
#define CESP32LLINK

//! \enum MASTERSTATUS
//! \brief Master or slaves status
enum MASTERSTATUS {
  MSSINIT         = 0,  //!< Init mode at switching on
  MSWAITINGESP32  = 1,  //!< Master or Slave waiting ESP32 on
  MSWAITEESP32    = 2,  //!< Waite 300ms for ESP32 initialization
  MSWAITINGSLAVES = 3,  //!< Master waiting Slaves
  MSMASTEROK      = 4,  //!< Master OK
  MSWAITINGVERS   = 5,  //!< Waiting version
  MSTIMEOUTESP32  = 6,  //!< Timeout waiting ESP32
  MSTIMEOUTSLAVES = 7,  //!< Timeout waiting slaves
  MSWAITINGPARAMS = 8,  //!< Slave waiting params from master
  MSWAITINGSEND   = 9,  //!< Slave waiting before sending SLAVEOK
  MSSLAVEOK       = 10, //!< Slave OK
  MSSLAVESTOP     = 11, //!< Slave after receive STOPMODE
  MSTIMEOUTMASTER = 12, //!< Slave, Timeout waiting parameters from Master
  MSERRORPARAMS   = 13, //!< Slave, error decoding message parameters
  MSSLAVEERROR    = 14, //!< Slave in error
#if defined(__MK66FX1M0__) // Teensy 3.6
  MSSMAX          = 15  //!<
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  MSMASTERRECORD  = 15, //!< Master recording
  MSSLAVEWAITREC  = 16, //!< Slave waiting to record
  MSSLAVERECORD   = 17, //!< Slave recording
  MSSLAVEWAITSREC = 18, //!< Salve waiting to stop recording
  MSSMAX          = 19  //!<
#endif
};

// With T4.1, I2C Slave address is 0x41 for slave 1, 0x42 for slave 2...
#define I2CSLAVEADR 0x40

//-------------------------------------------------------------------------
//! \class CSynchroLink
//! \brief Classe de base de gestion de la liaison avec l'ESP32 pour les enregistrements stéréo synchro avec un Teensy 3.6
//! \brief Avec un Teensy 4.1, gère la liaison I2C avec les 2 autres PRS
/*
 * Gestion des messages en mode I2C Teensy 4.1
 * A l'initialisation le maître regarde les esclaves présents (liaison I2C ouverte)
 * LMPARAMS   Paramètres émis par le maître vers les esclaves. Réponse LMSLAVE (Slave OK or not)
 * LMSTARTREC Départ mode enregistrement. Pas de réponse des esclaves
 * LMSTOPREC  Stop mode enregistrement. Pas de réponse des esclaves
 * LMTOP      Top synchro. Pas de réponse des esclaves
 * LMSTOPMODE Arrêt du mode synchro. Pas de réponse des esclaves
 */
class CSynchroLink
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  CSynchroLink();

  //-------------------------------------------------------------------------
  //! \brief Destructor
  virtual ~CSynchroLink();

  //-------------------------------------------------------------------------
  //! \brief Set type of the recorder and begin link
  //! \param iType Master or Slave n (enum MASTERSLAVES)
  //! \param pPar  Parameter to send if Master or to modify if Slave
  virtual void SetLinkType(
    int iType,
    ParamsOperator *pPar
    );
  
  //-------------------------------------------------------------------------
  //! \brief Send Stop Mode
  virtual void StopMode();
  
  //-------------------------------------------------------------------------
  //! \brief Test link on loop
  virtual void OnLoop();

  //-------------------------------------------------------------------------
  //! \brief Requesting the software version of the ESP32
  virtual void VersionRequest();
  
  //-------------------------------------------------------------------------
  //! \brief Processing message receptions
  //! \return Returns the type of the received message (LMUNKNOW if not)
  virtual int OnReceive();

  //-------------------------------------------------------------------------
  //! \brief Processing message receptions
  //! \param iMessage the type of the received message (LinkMessages)
  virtual void MessageProcessing(
    int iMessage
    );

  //-------------------------------------------------------------------------
  //! \brief Reset Slaves status
  void ResetStatus();

  //-------------------------------------------------------------------------
  //! \brief Set status
  //! \param iNewStatus New status of the PR Type
  void SetStatus(
    int iNewStatus
    );

  //-------------------------------------------------------------------------
  //! \brief Return status of Master or one Slave
  //! \param iType Master or Slave n (enum MASTERSLAVES)
  //! \return status (MASTERSLAVESTATUS)
  int GetStatus(
    int iType
    ) {return iStatus[iType];};

  //-------------------------------------------------------------------------
  //! \brief Get valid config or not
  //! \return bValid true if config is valid
  static bool GetValidConfig()
    { return bValidConfig;};

  //-------------------------------------------------------------------------
  //! \brief Reset de la config validée
  static void ResetValidConfig() {bValidConfig = false;};
  
  //-------------------------------------------------------------------------
  //! \brief Set debug mode
  //! \param bDbg true for debug mode
  void SetDebug( bool bDbg) {bDebug = bDbg;};

  //-------------------------------------------------------------------------
  //! \brief return ESP32 status
  int GetESP32Status() {return iESP32Status;};

  //-------------------------------------------------------------------------
  //! \brief Reset ESP32 status
  void ResetESP32Status() {iESP32Status = ES_OK;};

  //-------------------------------------------------------------------------
  //! \brief return ESP32 software version
  static char *GetESP32Version() {return sESP32Version;};

  //-------------------------------------------------------------------------
  //! \brief return Reception buffer
  char *GetReceptionBuffer() {return sMessageReception;};

#if defined(__IMXRT1062__) // Teensy 4.1
  //-------------------------------------------------------------------------
  //! \brief Start record
  virtual void StartRecord();

  //-------------------------------------------------------------------------
  //! \brief StopRecord
  virtual void StopRecord();

  //-------------------------------------------------------------------------
  //! \brief Send Top Synchro to each slave
  void SendTopSynchro();

  //-------------------------------------------------------------------------
  //! \brief On Timer Top synchro
  static void OnTimer();
#endif

  //! Debug mode
  bool bDebug;

protected:
  //! PR Type (Master or Slave n)
  int iPRType;
  
  //! Etats du maitre et des différents esclaves (MASTERSLAVESTATUS)
  int iStatus[MAXMS];

  //! Parameter to send if Master or to modify if Slave
  ParamsOperator *pParams;
  
  //! Link manager
  static CCodeLink link;
  
  //! For timeout management
  unsigned long iTimeOut;

  //! For second message management
  unsigned long iTimeSecondmessage;

  //! Receive position
  int iPos;
  
  //! Reception buffer
  char sMessageReception[127];

  //! false after switch on, indicate if config is validate by operator
  static bool bValidConfig;

  //! For ESP32 status message
  unsigned long iTimeStatus;
  int iESP32Status;
  unsigned long iTimeLoop;
  unsigned long iTimePrintLoop;

  // For test message
  unsigned long iTimeTest;

  // For managing message
  bool bFirst;
  bool bEnd;

  // ESP32 software version
  static char sESP32Version[10];

#if defined(__IMXRT1062__) // Teensy 4.1
  //! Pointeur sur l'instance pour les fonctions statiques
  static CSynchroLink *pLink;

  //! Timer de gestion des tops synchro
  IntervalTimer timerTop;

  //! Liste des esclaves présents
  bool bSlaveOK[MAXMS];

  //! Durée du top synchro
  static int iDurTop;

  //! Fréquence du top synchro
  static int iFreqTop;
#endif
};

//-------------------------------------------------------------------------
//! \class CMasterLink
//! \brief Gère le cas Master
class CMasterLink: public CSynchroLink
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  CMasterLink();

  //-------------------------------------------------------------------------
  //! \brief Destructor
  ~CMasterLink();

  //-------------------------------------------------------------------------
  //! \brief Set type of the recorder and begin link
  //! \param iType Master or Slave n (enum MASTERSLAVES)
  //! \param pPar  Parameter to send if Master or to modify if Slave
  virtual void SetLinkType(
    int iType,
    ParamsOperator *pPar
    );
  
  //-------------------------------------------------------------------------
  //! \brief Set valid configuration by operator
  void SetValidConfig();

  //-------------------------------------------------------------------------
  //! \brief Test link on loop
  virtual void OnLoop();

  //-------------------------------------------------------------------------
  //! \brief Processing message receptions
  //! \param iMessage the type of the received message (LinkMessages)
  virtual void MessageProcessing(
    int iMessage
    );

#if defined(__IMXRT1062__) // Teensy 4.1
  //-------------------------------------------------------------------------
  //! \brief Start record
  virtual void StartRecord();

  //-------------------------------------------------------------------------
  //! \brief StopRecord
  virtual void StopRecord();
#endif

protected:
  //! Esclaves présents en fonctions de la configuration validée
  static bool bConfig[MAXMS];
};

//-------------------------------------------------------------------------
//! \class CSlaveLink
//! \brief Gère le cas Slave
class CSlaveLink: public CSynchroLink
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  CSlaveLink();

  //-------------------------------------------------------------------------
  //! \brief Destructor
  ~CSlaveLink();

  //-------------------------------------------------------------------------
  //! \brief Set type of the recorder and begin link
  //! \param iType Master or Slave n (enum MASTERSLAVES)
  //! \param pPar  Parameter to send if Master or to modify if Slave
  virtual void SetLinkType(
    int iType,
    ParamsOperator *pPar
    );
  
  //-------------------------------------------------------------------------
  //! \brief Test link on loop
  virtual void OnLoop();

  //-------------------------------------------------------------------------
  //! \brief Processing message receptions
  //! \param iMessage the type of the received message (LinkMessages)
  virtual void MessageProcessing(
    int iMessage
    );

#if defined(__IMXRT1062__) // Teensy 4.1
  //----------------------------------------------------------------------------
  //! \brief Fonction de réception des octets de la liaison I2C esclave
  //! \param bytesReceived Nombre d'octets à recevoir
  static void i2cReceive(int bytesReceived);

  //----------------------------------------------------------------------------
  //! \brief Fonction sollicitée pour l'émission de la liaison I2C. Un esclave ne peut émettre que s'il est sollicité par le maître
  static void i2cRequest();
#endif

private:
  //! true if received params are OK
  bool bParamsOK;

#if defined(__IMXRT1062__) // Teensy 4.1
  //! Message à émettre
  static char sSlaveSend[127];

  //! Indice réception message
  static int idxMes;
#endif
};

#endif
