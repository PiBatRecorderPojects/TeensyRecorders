//-------------------------------------------------------------------------
//! \file CModeHeterodyne.h
//! \brief Classe de gestion du mode hétérodyne
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/* 
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "ModesModifiers.h"
#include "CModeRecorder.h"

#ifndef MODEHETERODYNE
#define MODEHETERODYNE

//! \enum IDXHPARAMS
//! \brief Index of Heterodyne mode parameters
enum IDXHPARAMS {
  IDXHRECORD , //!< Record status
  IDXHTIMER  , //!< Record time
  IDXHMODEH  , //!< Mode of operation of heterodyne (manual, auto)
  IDXMULTIPAR, //!< Enum to modify 3 parameters in heterodyne mode
  IDXHSEUILR , //!< Relative detection threshold in dB (5-99)
  IDXHAGC,     //!< AGC On Off
  IDXHFME    , //!< Detected frequency of maximum energy
  IDXHNIVA   , //!< Maximum level detected in numbers
  IDXHBAT    , //!< Internal battery charge level in %
  IDXHHOUR   , //!< Hour
  IDXHFILEA  , //!< File name A if no graph
  IDXHFILEB  , //!< File name B if no graph
  IDXHFILEC  , //!< File name C if no graph
  IDXHERDMA  , //!< DMA error
  IDXMAXHET
};

// Nb files name if no graph
#define MAXFILESNAMENOGR 3

//-------------------------------------------------------------------------
//! \class CGraphe128kHz
//! \brief Management class of a graph from 0 to 128kHz (1 pixel per kHz) on n lines
//! 24 pixels high, 14 pixels for the scale and n pixels for the graph
class CGraphe128kHz
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  //! \param iNLines Nb lines
  //! \param y Position y from the top of the graph
  CGraphe128kHz(
    int iNLines,
    int y       
    );

  //-------------------------------------------------------------------------
  //! \brief Destructor
  ~CGraphe128kHz();

  //-------------------------------------------------------------------------
  //! \brief Set the min max frequencies of the scale
  //! \param iFMin Min frequency in kHz
  //! \param iFMax Max frequency in kHz
  void SetMinMaxScale(
    int iFMin,
    int iFMax 
    );
  
  //-------------------------------------------------------------------------
  //! \brief Initialization of the scrolling time of the graph (1s by default)
  //! \param iScroll Time between two scrolls in ms
  void SetTimeScroll(
    int iScroll
    );
  
  //-------------------------------------------------------------------------
  //! \brief Initialization of a point in the graph
  //! \param iFreq Frequency in kHz
  void SetFrequence(
    int iFreq
    );
  
  //-------------------------------------------------------------------------
  //! \brief Print graph
  //! \param iFreqH Heterodyne frequency in kHz
  void PrintGraphe(
    int iFreqH
    );
  
protected:
  //! Position y from the top of the graph
  uint8_t iY;
  
  //! Time between two scrolls in ms
  unsigned long uiTimeScroll;

  //! Next scroll time
  unsigned long uiNextScroll;
  
  //! Bitmap for the graph 128 x n points is 16 x n bytes
  uint8_t *pTbGraphe;

  //! Nb lines
  int iNbLines;

  //! Min scale frequencies in kHz
  int iFrMin;

  //! Max scale frequencies in kHz
  int iFrMax;

  //! Size of graphe in bytes
  int iSizeGraph;
};

//! \enum HeterodyneMode
//! \brief Heterodyne mode
enum HeterodyneMode
{
  MANUAL     = 0,  //!< Manual
  AUTO       = 1,  //!< Auto
  MANU_NSEL  = 2,  //!< Manual / Non selective
  MANU_SEL   = 3,  //!< Manual / Selective
  AUTO_NSEL  = 4,  //!< Auto   / Non selective
  AUTO_SEL   = 5,  //!< Auto   / Selective
  MAXHETMODE = 6
};


//-------------------------------------------------------------------------
//! \class CModeHeterodyne
//! \brief Heterodyne mode management class
class CModeHeterodyne: public CModeGenericRec
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  CModeHeterodyne();

  //-------------------------------------------------------------------------
  //! \brief Creation du gestionnaire d'enregistrement
  virtual void CreateRecorder();
  
  //-------------------------------------------------------------------------
  //! \brief Beginning of the mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  //! \brief End of the mode
  virtual void EndMode();

  //-------------------------------------------------------------------------
  //! \brief  Reading saved settings
  //! \return true if OK
  virtual bool ReadParams();
  
  //-------------------------------------------------------------------------
  //! \brief  To display information outside of modifiers at the begin of PrintMode
  virtual void PrePrint();

  //-------------------------------------------------------------------------
  //! \brief To display information outside of modifiers
  virtual void AddPrint();
  
  //-------------------------------------------------------------------------
  //! \brief Keyboard order processing
  //! If the key is a mode change key, returns the requested mode
  //! This method is called regularly by the main loop
  //! By default, handles modifiers
  //! \param key Touch to treat
  virtual int KeyManager(
    unsigned short key
    );

  //-------------------------------------------------------------------------
  //! \brief Returns the string to display, taking only the n most significant characters
  //! 123456789012345678901
  //!  PR001_190126_221812.wav
  //!  PR001_190126_221812
  //! \param str string to verify
  String PrepareStrFile(
    String str
    );

  //-------------------------------------------------------------------------
  //! \brief List the wav files in the root directory
  void ListFiles();
    
protected:
  //! Heterodyne frequency in kHz
  float fFreqH;

  //! Maximum energie frequency in kHz
  float fFME;

  //! Maximum level in dB of FME in dB
  int iMaxLevel;
  //! Maximum level in dB
  int iMemoMaxLevel;

  //! Record status (PBREAK or PREC)
  int iRecordMode;

  //! Recording time in seconds
  int iRecordTime;
  
  //! Frequency graph
  CGraphe128kHz graphe;

  //! Step Value of the converter for calculating the heterodyne frequency in manual mode
  float fStepADC;

  //! Step Value of the converter for calculating the hétérodyne frequency in auto mode
  float fStepAuto;

  //! FME holding time
  unsigned long uiFMEHoldingTime;
  
  //! Time to check battery
  unsigned long uiTimeCheck;

  //! Minimum interest frequency in kHz
  unsigned int uiMinIntFreq;

  //! Maximum interest frequency in kHz
  unsigned int uiMaxIntFreq;

  //! Width of a FFT channel in Hz
  uint32_t uiFFTChannel;

  //! Counter for visibility of multi parameters
  uint8_t iCptMulPar;

  //! Current second
  uint8_t iCurrentSecond;

  // Texte Erreur DMA
  char txtERDMA[12];

  // Mode hétérodyne
  int iModeHeter;

  // Indique que le multiparamètres est en cours de modification
  bool bModifMulPar;

  // Indique que le seuil est en cours de modification
  bool bModifTh;

  // For Automatic playback of the last recorded file
  bool bAutoPlay;

  // For multiparameter modifier
  int iMultiPar;

  // Multiparameter enum string
  char txtMPAR[3][12];
  char *txtPMPAR[MAXLANGUAGE][3];

  // Files names if no graph
  char LstFilesScreen[MAXFILESNAMENOGR][MAX_LINEPARAM+1];

  //! Counter for visibility of last wave name
  uint8_t iCptAffWav;
};
#endif // MODEHETERODYNE
