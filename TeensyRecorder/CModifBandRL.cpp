//-------------------------------------------------------------------------
//! \file CModifBandRL.cpp
//! \brief Classes gd'analyse en mode RhinoLogger
//! \details class CAnalyzerRL, CFCAnalyzerRL, CFMAnalyzerRL
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <U8g2lib.h>
#include "Const.h"
#include "CModifBandRL.h"

extern const char *txtTypeBand[MAXLANGUAGE][4];
const char formIDXBand[]       =  "[&$]";
const char *txtIDXBand[]       = {"[%02d]",
                                  "[%02d]"};
const char formIDXRet[]        =  "[&$$$$$]";
extern const char *txtIDXRet[];
const char formIDXName[]       =  "[&&&]";
const char *txtIDXName[]       = {"[%s]",
                                  "[%s]"};
const char formIDXNbD[]        =  "[Nb.D. &$]";
extern const char *txtIDXNbD[];
const char formIDXTBD[]        =  "[Type &$$]";
extern const char *txtIDXTBD[];
const char formIDXFMIN[]       =  "[Min F. &$$$$$ kHz]";
extern const char *txtIDXFMINB[];
const char formIDXFMAX[]       =  "[Min F. &$$$$$ kHz]";
extern const char *txtIDXFMAXB[];
const char formIDXMIND[]       =  "[Min Dur. &$ ms]";
extern const char *txtIDXMIND[];
const char formIDXMAXD[]       =  "[Max Dur. &$ ms]";
extern const char *txtIDXMAXD[];
const char formIDXWidth[]      =  "[Min width &$$$ kHz]";
extern const char *txtIDXWidth[];

//-------------------------------------------------------------------------
// Management Class of Bands Edit Mode
// Displays the different parameters and allows you to modify them

// Bands Edit mode screen
//   12345678901234567890  y
// 1 [Return] [00] [BD1]   0
// 2 [Type Bar][Nb.D. 00]  9
// 3 [F. Min 120.00 kHz]   18
// 4 [F. Max 120.00 kHz]   27
// 5 [Min Dur. 99 ms]      36
// 6 [Max Dur. 99 ms]      45
// 7 [Min width 99.0 kHz]  54
// X 000000000000000001111
//   001123344566778990012
//   062840628406284062840
//-------------------------------------------------------------------------
// Constructor
CModeBandsRL::CModeBandsRL()
{
  // Creating modifiers
  // IDXRETURN   Return to parameters mode
  LstModifParams.push_back(new CPushButtonModifier( formIDXRet, false, txtIDXRet, 0, 0));
  // IDXNUMBAND  Band Index
  LstModifParams.push_back(new CModificatorInt    ( formIDXBand,  false, txtIDXBand, (int *)&idxBand, 1, MAXBD, 1, 60, 0));
  // IDXNAME     Name of the band
  LstModifParams.push_back(new CCharModifier      ( formIDXName,  false, txtIDXName, (char *)&tmpBand.sName, MAXNAMEBD, false, true, false, true, true, 90, 0));
  // IDXTYPE     Type of the band
  LstModifParams.push_back(new CEnumModifier      ( formIDXTBD,   false, true, txtIDXTBD, (int *)&tmpBand.iType, BDTYPEMAX, (const char **)txtTypeBand, 0, 9));
  // IDXNBDET    Minimum detections to consider a positive second
  LstModifParams.push_back(new CModificatorInt    ( formIDXNbD,   false, txtIDXNbD, (int *)&tmpBand.iNbDetections, 1, 5, 1, 60, 9));
  // IDXBDFMIN   Minimum frequency of the band
  LstModifParams.push_back(new CFloatModifier     ( formIDXFMIN,  false, txtIDXFMINB, &tmpBand.fMin,  8.0, 125.0, 0.25, 0, 18));
  // IDXBDFMAX   Maximum frequency of the band
  LstModifParams.push_back(new CFloatModifier     ( formIDXFMAX,  false, txtIDXFMAXB, &tmpBand.fMax, 10.0, 125.0, 0.25, 0, 27));
  // IDXMINDUR   Minimum duration of the band
  LstModifParams.push_back(new CModificatorInt    ( formIDXMIND,  false, txtIDXMIND, (int *)&tmpBand.iMinDuration, 4, 96, 4, 0, 36));
  // IDXMAXDUR   Maximum duration of the band
  LstModifParams.push_back(new CModificatorInt    ( formIDXMAXD,  false, txtIDXMAXD, (int *)&tmpBand.iMaxDuration, 4, 96, 4, 0, 45));
  // IDXMINWIDTH Minimum width of the band
  LstModifParams.push_back(new CFloatModifier     ( formIDXWidth, false, txtIDXWidth, &tmpBand.fMinFWidth, 0.5, 99.0, 0.5, 0, 54));
}

//-------------------------------------------------------------------------
// Beginning of the mode
void CModeBandsRL::BeginMode()
{
  // Call of the parent method
  CModeGeneric::BeginMode();
  // Copy first band
  idxBand = 1;
  tmpBand = paramsOperateur.batBand[0];
  idxSelParam = IDXRETURN;
  ControleParams();
}

//-------------------------------------------------------------------------
// Traitement des ordres claviers
int CModeBandsRL::KeyManager(
  unsigned short key  // Touch to treat
  )
{
  int newMode = NOMODE;
  if (idxSelParam == IDXNUMBAND)
  {
    int iOldBand = idxBand;
    if (key == K_PUSH or key == K_RIGHT)
    {
      idxBand++;
      if (idxBand > MAXBD)
        idxBand = 1;
    }
    else if (key == K_LEFT)
    {
      idxBand--;
      if (idxBand < 1)
        idxBand = MAXBD;
    }
    else
      // Call of the parent method
      newMode = CModeGeneric::KeyManager( key);
    if (idxBand != iOldBand)
    {
      // Save old band parameters
      paramsOperateur.batBand[iOldBand-1] = tmpBand;
      // Set new band parameters
      tmpBand = paramsOperateur.batBand[idxBand-1];
      ControleParams();
    }
  }
  else
    // Call of the parent method
    newMode = CModeGeneric::KeyManager( key);
  return newMode;
}

//-------------------------------------------------------------------------
// Called on end of change of a modifier
// If a mode change, returns the requested mode
int CModeBandsRL::OnEndChange(
  int idxModifier // Index of affected modifier
  )
{
  if (idxModifier == IDXRETURN)
  {
    // Save selected band parameters
    paramsOperateur.batBand[idxBand-1] = tmpBand;
    // Go back to params mode
    iNewMode = MPARAMS;
  }
  else if (idxModifier == IDXBDFMIN)
  {
    // Test FMax
    if (tmpBand.fMin > tmpBand.fMax)
      tmpBand.fMax = tmpBand.fMin + 2.0;
  }
  else if (idxModifier == IDXBDFMAX)
  {
    // Test FMin
    if (tmpBand.fMax < tmpBand.fMin)
      tmpBand.fMin = tmpBand.fMax - 2.0;
  }
  else if (idxModifier == IDXMINDUR)
  {
    // Test max Duration
    if (tmpBand.iMinDuration > tmpBand.iMaxDuration)
      tmpBand.iMaxDuration = tmpBand.iMinDuration + 8;
  }
  else if (idxModifier == IDXMAXDUR)
  {
    // Test min Duration
    if (tmpBand.iMaxDuration < tmpBand.iMinDuration)
      tmpBand.iMinDuration = tmpBand.iMaxDuration - 8;
  }
  else if (idxModifier == IDXTYPE)
    ControleParams();
  // Mémorisation des paramètres
  WriteParams();
  return iNewMode;
}

//-------------------------------------------------------------------------
// Consistency between parameters
void CModeBandsRL::ControleParams()
{
  switch (tmpBand.iType)
  {
  case BDINVALID:
    LstModifParams[IDXNAME    ]->SetbValid( false);
    LstModifParams[IDXBDFMIN  ]->SetbValid( false);
    LstModifParams[IDXBDFMAX  ]->SetbValid( false);
    LstModifParams[IDXMINDUR  ]->SetbValid( false);
    LstModifParams[IDXMAXDUR  ]->SetbValid( false);
    LstModifParams[IDXMINWIDTH]->SetbValid( false);
    LstModifParams[IDXNBDET   ]->SetbValid( false);
    break;
  case BDFM:
    LstModifParams[IDXNAME    ]->SetbValid( true);
    LstModifParams[IDXBDFMIN  ]->SetbValid( true);
    LstModifParams[IDXBDFMAX  ]->SetbValid( true);
    LstModifParams[IDXMINDUR  ]->SetbValid( false);
    LstModifParams[IDXMAXDUR  ]->SetbValid( false);
    LstModifParams[IDXMINWIDTH]->SetbValid( true);
    LstModifParams[IDXNBDET   ]->SetbValid( true);
    break;
  case BDFC:
    LstModifParams[IDXNAME    ]->SetbValid( true);
    LstModifParams[IDXBDFMIN  ]->SetbValid( true);
    LstModifParams[IDXBDFMAX  ]->SetbValid( true);
    LstModifParams[IDXMINDUR  ]->SetbValid( true);
    LstModifParams[IDXMAXDUR  ]->SetbValid( false);
    LstModifParams[IDXMINWIDTH]->SetbValid( false);
    LstModifParams[IDXNBDET   ]->SetbValid( true);
    break;
  case BDQFC:
    LstModifParams[IDXNAME    ]->SetbValid( true);
    LstModifParams[IDXBDFMIN  ]->SetbValid( true);
    LstModifParams[IDXBDFMAX  ]->SetbValid( true);
    LstModifParams[IDXMINDUR  ]->SetbValid( true);
    LstModifParams[IDXMAXDUR  ]->SetbValid( true);
    LstModifParams[IDXMINWIDTH]->SetbValid( false);
    LstModifParams[IDXNBDET   ]->SetbValid( true);
    break;
  }
}
  
