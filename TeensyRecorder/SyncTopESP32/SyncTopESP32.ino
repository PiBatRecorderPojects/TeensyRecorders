//! \file SyncTopESP32.ino
//! \brief ESP32 Software for Audio Top synchro in Passive Recorder Stereo. 
//! \details Generation of sync radio tops
//! ESP32 Dev Module
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2019 Vrignault Jean-Do. All rights reserved.
/*
  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#include "WiFi.h"
#include "CManager.h"
#include "soc/rtc_wdt.h"

//!< Manager
CManager *pManager;

//-------------------------------------------------------------------------
//! \brief Software setup
void setup()
{
  delay(500);
  Serial.begin(115200);
  delay(500);
  // Set pins
/*
  GPIO PINS
                 TEENSY   ESP32
  ESPON          23 Out          1 to put ESP32 ON
  ESP32_READY    22 In    23 Out 0 when ready
  MASTER         04 Out   32 In  0 for slave and 1 for master
  SLEEP          01 Out   12 In  1 for work abd 0 for sleep
  RECORD MASTER  00 Out   13 In  0 for standby and 1 for recording
  RECORD SLAVE   00 In    13 Out 0 for standby and 1 for recording
*/
  gpio_set_direction((gpio_num_t)PIN_ESP32READY, GPIO_MODE_OUTPUT);
  gpio_set_direction((gpio_num_t)PIN_MASTER    , GPIO_MODE_INPUT);
  gpio_set_pull_mode((gpio_num_t)PIN_MASTER    , GPIO_PULLUP_ONLY);
  gpio_set_direction((gpio_num_t)PIN_SLEEP     , GPIO_MODE_INPUT);
  gpio_set_pull_mode((gpio_num_t)PIN_SLEEP     , GPIO_PULLUP_ONLY);
  gpio_set_direction((gpio_num_t)PIN_LED       , GPIO_MODE_OUTPUT);
  gpio_set_pull_mode((gpio_num_t)PIN_BUTTON    , GPIO_PULLUP_ONLY);
  digitalWrite( PIN_LED, LOW);
  // Test if ESP32 ON

  Serial.println("============================================");
  Serial.println("=============== SyncTopESP32 ===============");

  rtc_wdt_protect_off();
  rtc_wdt_disable();

  // Stop Wifi and bluetooth
  WiFi.mode(WIFI_OFF);
  btStop();
  
  // Test Master or Slave
  delay(1000);
  if (digitalRead(PIN_MASTER) == HIGH)
  {
    Serial.println("New CMasterManager");
    pManager = new CMasterManager();
  }
  else
  {
    Serial.println("New CSlaveManager");
    pManager = new CSlaveManager();
  }
  // Init manager
  pManager->Init();

  // Set ESP32 ready for Teensy
  digitalWrite( PIN_ESP32READY, LOW);
}

int iSecond = 0;
//-------------------------------------------------------------------------
//! \brief Main loop of the software
void loop()
{
  //Serial.println("loop");
  // Manager traitment
  pManager->OnLoop();
  /*Serial.println("loop OK");
  delay(1000);*/
}
