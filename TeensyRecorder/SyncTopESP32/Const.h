//-------------------------------------------------------------------------
//! \file Const.h
//! \brief Constants of the software
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2019 Vrignault Jean-Do. All rights reserved.
/* 
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * V0.10
 * - Initial version of SyncTopESP32
 */
#define VERSIONESP32 "V0.10"

#ifndef CONSTESP32_H
#define CONSTESP32_H

/*
 * Utilisation de la LED
 * ESCLAVE
 * - Attente params Maitre         3 brefs de 100ms sur 1s
 * - Params reçus, attente Teensy  2 brefs de 100ms sur 1s
 * - OK, Veille                    Eteinte
 * - OK Acquisition                1 bref de 100ms sur 1s
 * - OK Enregistrement             500ms alllumée, 500ms éteinte
 * - En erreur                     800ms allumée, 200ms éteinte
 * 
 * MAITRE
 * - Attente esclaves              Clignote 200ms
 * - OK, Veille                    Eteinte
 * - OK Acquisition                1x200ms sur 1s
 * - OK Enregistrement             500ms alllumée, 500ms éteinte
 * - En erreur                     800ms allumée, 200ms éteinte
*/
//#define USE_LED

//! Pin definetion of LoRa module
#define LORA_SCK     5    // GPIO5  -- SX127x's SCK
#define LORA_MISO    19   // GPIO19 -- SX127x's MISO
#define LORA_MOSI    27   // GPIO27 -- SX127x's MOSI
#define LORA_SS      18   // GPIO18 -- SX127x's CS
#define LORA_RST     14   // GPIO14 -- SX127x's RESET
#define LORA_DI0     26   // GPIO26 -- SX127x's IRQ(Interrupt Request)

// Config puce LoRa par défaut
// Boost Power 15dB, SPF 7, Band 125kHz, Rate 5, Preambule 8, Sync 0x12, No CRC
//#define SETDEFAULTLORA

// 10 canaux utilisables (MHz) en Europe 867.1, 867.3, 867.5, 867.7, 867.9, 868.1, 868.3, 868.5, 868.7, 868.9
// Les canaux 868.1, 868.3 et 868.5 sont utilisés prioritairement par les réseaux LORAWAN
#define LORACNX        867300000
// Bandwidth, largeurs utilisables (kHz) : 7.8, 10.4, 15.6, 20.8, 31.25, 41.7E3, 62.5, 125, 250
#ifdef SETDEFAULTLORA
  #define BANDWIDTH    125000
#else
  #define BANDWIDTH    125000
#endif
// CodingRate 5, 6, 7 ou 8 pour une correction d'erreur avec un facteur respectif de 1.25, 1.5, 1.75, 2
#ifdef SETDEFAULTLORA
  #define CODINGRATE  5
#else
  #define CODINGRATE  5
#endif
// SpreadingFactor, facteur d'étalement de 6, 7, 8, 9, 10, 11 ou 12
// Attention, plus l'étalement est fort plus la vitesse d'émission est lente
#ifdef SETDEFAULTLORA
  #define SPREADINGFACTOR 7
#else
  #define SPREADINGFACTOR 7
#endif
// TX power, si PABOOST=true, de 0 à 14 (+2 à +17dBm) et si PABOOST=false, de 2 à 17 (-4dBm à +11dBm)
// Si PABOOST=true  0= 2dBm, 1= 3dBm, 2= 4dBm, 3= 5dBm, 4=6dBm, 5=7dBm, 6=8dBm, 7=9dBm,  8=10dBm,  9=11dBm, 10=12dBm, 11=13dBm, 12=14dBm, 13=15dBm, 14=16dBm
// Si PABOOST=false 2=-4dBm, 3=-3dBm, 4=-2dBm, 5=-1dBm, 6=0dBm, 7=1dBm, 8=2dBm, 9=3dBm, 10= 4dBm, 11= 5dBm, 12= 6dBm, 13= 7dBm, 14= 8dBm, 15= 9dBm, 16=10dBm, 17=11dBm
// En Europe, la puissance max autorisée est de +14dBm
#ifdef SETDEFAULTLORA
  #define TXPOWER 13
  #define PABOOST true
#else
  #define TXPOWER 10
  #define PABOOST true
#endif
// Longueur du préambule
#ifdef SETDEFAULTLORA
  #define PREAMBLE_LENGTH 8
#else
  #define PREAMBLE_LENGTH 8
#endif
// Mot de synchro (DC2)
#ifdef SETDEFAULTLORA
  #define SYNC_WORD 0x12
#else
  #define SYNC_WORD 0x12
#endif
// CRC
#ifdef SETDEFAULTLORA
  #define LORACRC false
#else
  #define LORACRC false
#endif

/*
 * Calcul du temps de transmission (Time On Air) d'un message LORA sur https://www.loratools53.nl/#/airtime
 * Paramètres SPF 7 à 12, Band 125kHz, Coding Rate 5, Preambule 8, No CRC, No Explicit header
 * Taille des messages LoRa (octets) et temps de transmission avec les paramètres LoRa (ms) en fonction du Spreading Factor
 *        octets  SF 7   SF 8    SF 9    SF 10    SF 11   SF 12
 * LMPARAMS   53, 97,54 174,59  308,22  575,49  1232,90 2301,95 Send parameters
 * LMSLAVE     9, 30,98  61,95  123,90  206,85   413,70  827,39 Slave OK
 * LMSTARTREC  3, 25,86  41,47   82,94  165,89   331,78  663,55 Start recording
 * LMSTOPREC   3, 25,86  41,47   82,94  165,89   331,78  663,55 Stop recording
 * LMTOP       1, 20,74  41,47   82,94  165,89   331,78  663,55 Top synchro
 * LMSTOPMODE  3, 25,86  41,47   82,94  165,89   331,78  663,55 Stop synchro mode
 * LMTEST      3, 25,86  41,47   82,94  165,89   331,78  663,55 Test message
 */

#if SPREADINGFACTOR == 7
  #define TIMELMPARAMS 100
  #define TIMELMSLAVE   31
  #define TIMELMSTART   31
  #define TIMELMSTOP    31
  #define TIMELMSTM     31
  #define TIMELMTST     31
  #define TIMELMTOP     21
  #define PLUSTOP       0
#endif
#if SPREADINGFACTOR == 8
  #define TIMELMPARAMS 180
  #define TIMELMSLAVE   62
  #define TIMELMSTART   42
  #define TIMELMSTOP    42
  #define TIMELMSTM     42
  #define TIMELMTST     42
  #define TIMELMTOP     42
  #define PLUSTOP       21
#endif
#if SPREADINGFACTOR == 9
  #define TIMELMPARAMS 310
  #define TIMELMSLAVE  125
  #define TIMELMSTART   83
  #define TIMELMSTOP    83
  #define TIMELMSTM     83
  #define TIMELMTST     83
  #define TIMELMTOP     83
  #define PLUSTOP       62
#endif

//! Definition of the dialogue pins with the recorder
#define PIN_MASTER      32  //!< Master when 1 else slave
#define PIN_ESP32READY  23  //!< ESP32 ready when 0 (pullup by Teensy)
#define PIN_SLEEP       12  //!< Sleep when 1
#define PIN_RECORD      21  //!< Record when 1
#define PIN_RX          35  //!< Serial RX
#define PIN_TX          12  //!< Serial TX
#define PIN_LED         2   //!< LED pin
#define PIN_BUTTON      0   //!< Button

// I2S PINs
#define PIN_BCK_I2S     26  //!< this is BCK pin
#define PIN_LRCK_I2S    25  //!< this is LRCK pin
#define PIN_OUT_I2S     17  //!< this is DATA output pin
#define PIN_IN_I2S      -1  //!< Not used

//! Buffer size for samples of the top audio
#define BUFFSIZE 4096

//! Definition PI * 2
#define DEUXPI 6.2831853

//! Master delay for audio top in µs
#define MASTER_DELAY 800 // Top slave in relation to the master -75µs to 15µs

//! Nombre max de caractères du préfixe des fichiers wav
#define MAXPREFIXE 5

//! Taille de la chaine de sauvegarde des heures de début et fin (style hh:mm)
#define MAXH 6

//! Taille de la chaine de sauvegarde de la date et de l'heure courante (style jj/mm/yy ouhh:mm:ss )
#define MAXDH 9

//! \enum FREQECH
//! \brief Fréquences d'échantillonnages
enum FREQECH {
  FE24KHZ  = 0, //!< 24kHz
  FE48KHZ  = 1, //!< 48kHz
  FE96KHZ  = 2, //!< 96kHz
  FE192KHZ = 3, //!< 192kHz
  FE250KHZ = 4, //!< 250kHz
  FE384KHZ = 5, //!< 384kHz
  FE500KHZ = 6, //!< 500kHz
  MAXFE    = 7
};

//! \enum GAINNUM
//! \brief Gain numérique
enum GAINNUM {
  GAIN0dB  = 0, //!< 0dB
  GAIN6dB  = 1, //!< +6dB
  GAIN12dB = 2, //!< +12dB
  GAIN18dB = 3, //!< +18dB
  GAIN24dB = 4, //!< + 24dB
  MAXGAIN  = 5
};

//! \enum MASTERSLAVES
//! \brief Synchro modes
enum MASTERSLAVES {
  MASTER  = 0,  //!< Master mode
  SLAVE01 = 1,  //!< Slave 1
  SLAVE02 = 2,  //!< Slave 2
  SLAVE03 = 3,  //!< Slave 3
  SLAVE04 = 4,  //!< Slave 4
  SLAVE05 = 5,  //!< Slave 5
  SLAVE06 = 6,  //!< Slave 6
  SLAVE07 = 7,  //!< Slave 7
  SLAVE08 = 8,  //!< Slave 8
  SLAVE09 = 9,  //!< Slave 9
  MAXMS   = 10  //!< Nombre max de mode synchro Master / Slave
};

//! \enum TOPSIZE
//! \brief Audio top size in samples
enum TOPSIZE
{
  TS256  = 0,  //!< 256 samples
  TS512  = 1,  //!< 512 samples
  TS1024 = 2,  //!< 1024 samples
  TS2048 = 3,  //!< 2048 samples
  TS4096 = 4,  //!< 4096 samples
  TSMAX  = 5
};

//! \enum UTILLEDSYNCH
//! \brief Types d'utilisation de la LED en mode synchro
enum UTILLEDSYNCH {
  NOLEDSYNCH  = 0,  //!< LED non utilisée
  LEDRECSYNCH = 1,  //!< LED allumée sur enregistrement d'un fichier
  LED3RECS    = 2,  //!< LED allumée sur les 3 er enregistrement seulement
  MAXLEDSYNCH = 3
};

//! \struct ParamsOperator
//! \brief Paramètres opérateur d'enregistrement
struct ParamsOperator
{
  char     sDateHour[25];           //!< Current date time
  char     sHDebut[MAXH];           //!< Auto recording start time (hh:mm)
  char     sHFin[MAXH];             //!< Auto recording end time (hh:mm)
  int      uiDurMax;                //!< Maximum duration in seconds (10-999)
  int      uiFe;                    //!< Sampling frequency in kHz (48, 96, 192, 250, 384, 500)
  int      uiGainNum;               //!< Numeric gain (0, 6, 12, 18, 24dB)
  int      iFreqFiltreHighPass;     //!< High pass filter in kHz (0-25)
  bool     bExp10;                  //!< Time expansion x10 if true
  int      iMasterSlave;            //!< Master or Slave n (0 Master or Slave 1 to 9 step 1)
  int      iTopAudioFreq;           //!< Top audio frequency in kHz (1 to 50 kHz step 1)
  int      iDurTop;                 //!< Top audio duration in samples (256, 512 or 1024)
  int      iTopPer;                 //!< Top audio period (0 for one at the start of the recording, 1 to 10 step 1 for one every 1 to 10 seconds)
  int      iLEDSynchro;             //!< Using LED in synchro mode (No, Record ou 3 record only)
};

#endif
