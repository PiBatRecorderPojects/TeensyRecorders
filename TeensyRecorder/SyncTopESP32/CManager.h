//! \file CManager.h
//! \brief Definition of the CCodeLink class for the serial Teensy Recorder to ESP32 dialogue and Lora message
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2019 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Const.h"
#include "CLoraLink.h"
#include "CCommPR.h"
#include "CTopGeneration.h"

/*
 * - CFlag
 * - CManager
 * -   CMasterManager
 * -   CSlaveManager
 */

#ifndef CMANAGER_H
#define CMANAGER_H

//-------------------------------------------------------------------------
//! \class CFlag
//! \brief Class for multitasking flag management
class CFlag
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor
  CFlag()
  {
    bFlag = false;
    SectionCritiqueMux = portMUX_INITIALIZER_UNLOCKED;
  };

  //-------------------------------------------------------------------------
  //! \brief Destructor
  ~CFlag() {portEXIT_CRITICAL_ISR( &SectionCritiqueMux);};

  //-------------------------------------------------------------------------
  //! \brief Set Flag
  void SetFlag()
  {
    portENTER_CRITICAL_ISR( &SectionCritiqueMux);
    bFlag = true;
    portEXIT_CRITICAL_ISR ( &SectionCritiqueMux);
  };
  
  //-------------------------------------------------------------------------
  //! \brief Reset Flag
  void ResetFlag()
  {
    portENTER_CRITICAL_ISR( &SectionCritiqueMux);
    bFlag = false;
    portEXIT_CRITICAL_ISR ( &SectionCritiqueMux);
  };
  
  //-------------------------------------------------------------------------
  //! \brief read Flag
  bool ReadFlag()
  {
    bool bReadFlag;
    portENTER_CRITICAL_ISR( &SectionCritiqueMux);
    bReadFlag = bFlag;
    portEXIT_CRITICAL_ISR ( &SectionCritiqueMux);
    return bReadFlag;
  };
  
  //-------------------------------------------------------------------------
  //! \brief read Flag
  bool ReadAndResetFlag()
  {
    bool bReadFlag;
    portENTER_CRITICAL_ISR( &SectionCritiqueMux);
    bReadFlag = bFlag;
    bFlag = false;
    portEXIT_CRITICAL_ISR ( &SectionCritiqueMux);
    return bReadFlag;
  };
  
protected:
  // Critical section
  portMUX_TYPE SectionCritiqueMux;

  // Flag value
  bool bFlag;
};

//-------------------------------------------------------------------------
//! \class CManager
//! \brief Generic Class for Master or Slave manager
class CManager
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor
  CManager();

  //-------------------------------------------------------------------------
  //! \brief Destructor
  virtual ~CManager();
  
  //-------------------------------------------------------------------------
  //! \brief Init manager
  virtual void Init();

  //-------------------------------------------------------------------------
  //! \brief OnLoop traitment
  virtual void OnLoop();

  //-------------------------------------------------------------------------
  //! \brief Set debug mode
  //! \param bDbg true for debug mode
  void SetDebug( bool bDbg) {bDebug = bDbg;};

#ifdef USE_LED
  //-------------------------------------------------------------------------
  //! \brief Set LED mode
  void SetLEDMode();
#endif

protected:
  //! Parameters received by parameters message
  static ParamsOperator Params;

  //! Serial link between Teensy 3.6 and ESP32
  CCommPR serialLink;
  
  //! LORA link between Master and Slaves
  CLoRaLink loraLink;
  
  //! Audio Tops manager
  CTopGeneration audioTops;

  //! To generate Top
  CFlag flagTop;

  // Section critique pour les ravaux sous interruption
  static portMUX_TYPE SectionCritiqueMux;

  // For status message
  int iESP32Status;
  unsigned long iTimeStatus;

  // For test message
  unsigned long iTimeTest;

  // For test Loop
  unsigned long iTimeLoop;

  // For time test
  unsigned long iTimeRec;
  unsigned long iTimeSend;
  unsigned long iTimeTop;

  //! Master status
  bool bMaster;

  //! Record status
  bool bRecord;

  //! True if waiting master params
  bool bWaitingtMaster;

  //! True if waiting first slave OK
  bool bWaitingtSlave;

  //! True if waiting teensy
  bool bWaitingTeensy;

  //! True if error
  bool bError;

  //! Debug mode
  bool bDebug;

#ifdef USE_LED
  //! 10 phases de 100ms sur 1s
  bool bLED[10];

  //! Compteur phases LED
  int iLed;

  //! Time LED
  unsigned long iTimeLed;
#endif
};

//-------------------------------------------------------------------------
//! \class CMasterManager
//! \brief Class for Master manager
class CMasterManager: public CManager
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor
  CMasterManager();

  //-------------------------------------------------------------------------
  //! \brief Destructor
  virtual ~CMasterManager();
  
  //-------------------------------------------------------------------------
  //! \brief Init manager
  virtual void Init();

  //-------------------------------------------------------------------------
  //! \brief OnLoop traitment
  virtual void OnLoop();

  //-------------------------------------------------------------------------
  //! \brief Start Timer for Master and Slave Top
  void StartTimer();

  //-------------------------------------------------------------------------
  //! \brief Stop Timer for Master and Slave Top
  void StopTimer();

  //-------------------------------------------------------------------------
  //! \brief Record pin traitment
  void PinRecord();

  //-------------------------------------------------------------------------
  //! \brief Slave Timer traitment
  void OnTopTimer();

protected:
  //! Timer for Slave Audio Top
  hw_timer_t *pTopTimer;

  // Time to start timer
  int64_t iTimeStartTimer;
};

//-------------------------------------------------------------------------
//! \class CSlaveManager
//! \brief Class for Slave manager
class CSlaveManager: public CManager
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor
  CSlaveManager();

  //-------------------------------------------------------------------------
  //! \brief Destructor
  virtual ~CSlaveManager();
  
  //-------------------------------------------------------------------------
  //! \brief Init manager
  virtual void Init();

  //-------------------------------------------------------------------------
  //! \brief OnLoop traitment
  virtual void OnLoop();
};

#endif
