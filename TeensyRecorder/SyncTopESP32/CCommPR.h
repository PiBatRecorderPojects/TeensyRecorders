//! \file CCommPR.h
//! \brief Definition of the CCommPR class for the serial Teensy Recorder dialogue
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2019 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <HardwareSerial.h>
#include "Const.h"
#include "CCodeLink.h"

#ifndef CCOMMPR_H
#define CCOMMPR_H

//-------------------------------------------------------------------------
//! \class CCommPR
//! \brief Class for the serial Teensy Recorder dialogue
//! Messages exchanged:
//! - $Params for parameters from Passive Recorder to ESP32 and a slave
//! - $SlaveOK to signal the good reception of the parameters from a slave to Passive Recorder
//! - $SlaveN  to transfert slave number from Passive Recorder
class CCommPR
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor
  CCommPR();

  //-------------------------------------------------------------------------
  //! \brief Initialization of serial link
  //! \return Return true if OK
  bool Init();

  //-------------------------------------------------------------------------
  //! \brief Set Master or Slave status
  //! \param bMas Master or Slave
  void SetMasSlv(
    bool bMas
    ) {bMaster = bMas;};

  //-------------------------------------------------------------------------
  //! \brief Transmitting parameters to a slave Passive Recorder
  //! \param pParams Recording Parameters
  void SendParams(
    ParamsOperator *pParams
    );

  //-------------------------------------------------------------------------
  //! \brief Transmitting slave result about parameters received
  //! \param iSlave Slave number
  //! \param bOK    true if slave parameters are OK
  void SendSlaveOK(
    int iSlave, bool bOK
    );

  //-------------------------------------------------------------------------
  //! \brief Transmitting stop mode from master to all slaves
  void SendStopMode();

  //-------------------------------------------------------------------------
  //! \brief Transmitting ESP32 status to Teensy
  //! \param iStatus ESP32 status
  void SendESP32Status(
    int  iStatus
    );

  //-------------------------------------------------------------------------
  //! \brief Transmitting Test message to Teensy
  void SendTest();

  //-------------------------------------------------------------------------
  //! \brief Processing message receptions
  //! \return Returns the number of the received message
  int OnReceive();

  //-------------------------------------------------------------------------
  //! \brief Return parameters received by parameters message
  //! \return RecordingParams pointer
  ParamsOperator *GetParameters() {return pParams;};

  //-------------------------------------------------------------------------
  //! \brief Return the received slave number
  //! \return Slave number
  int GetSlaveNum() {return iSlaveOK;};

  //-------------------------------------------------------------------------
  //! \brief Return the received slave status
  //! \return Slave status
  bool GetSlaveStatus() {return bSlaveStatus;};

  //-------------------------------------------------------------------------
  //! \brief Set debug mode
  //! \param bDbg true for debug mode
  void SetDebug( bool bDbg) {bDebug = bDbg;};

  //-------------------------------------------------------------------------
  //! \brief Set Parameter
  //! \param *params Pointer on parameters
  void SetpParams( ParamsOperator *params) {pParams = params;};

  //-------------------------------------------------------------------------
  //! \brief Return error or not (ESP32Status)
  int GetError() {return iErreur;};

protected:
  //! Debug mode
  bool bDebug;

  //! Master or Slave
  bool bMaster;

  //! Parameters received by parameters message
  ParamsOperator *pParams;

  //! Slave number on received SlaveOK message
  int iSlaveOK;

  //! Slave status on received SlaveOK message
  bool bSlaveStatus;

  //! Serial manager
  HardwareSerial hSerial;

  //! Receive position
  int iPos;

  //! To manage message reception
  bool bFirst;
  bool bEnd;

  //! Reception buffer
  char sMessageReception[255];

  //! Gestionnaire des messages
  CCodeLink link;

  //! Eventuelle erreur (ESP32Status)
  int iErreur;
  unsigned long iTimeStatus;
};

#endif
