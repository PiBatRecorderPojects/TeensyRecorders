//! \file CLoraLink.cpp
//! \brief Definition of the CLoRaLink class for the radio dialogue
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2019 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <SPI.h>
#include "TTGO-LoRa.h"
#include "Const.h"
#include "CLoraLink.h"

//-------------------------------------------------------------------------
// Print LoRa confuguration
void PrintConfigLoRaWAN()
{
  bool bBoost = LoRa.getPABoost();
  int  iPower = LoRa.getTxPower();
  long iFreq  = LoRa.getFrequency();
  int  iSPF   = LoRa.getSpreadingFactor();
  long iBand  = LoRa.getSignalBandwidth();
  int  iRate  = LoRa.getCodingRate4();
  int  iLen   = LoRa.getPreambleLength();
  int  iSync  = LoRa.getSyncWord();
  if (bBoost)
    Serial.printf("Config Boost, Power %ddB, Freq %dHz, SPF %d, Band %dHz, Rate %d, Len %d, Sync %0xd\n", iPower, iFreq, iSPF, iBand, iRate, iLen, iSync);
  else
    Serial.printf("Config RFO, Power %ddB, Freq %dHz, SPF %d, Band %dHz, Rate %d, Len %d, Sync %0xd\n", iPower, iFreq, iSPF, iBand, iRate, iLen, iSync);
}

//-------------------------------------------------------------------------
//! \class CLoRaLink
//! \brief Class for the radio dialogue between master and slaves
//! Messages exchanged:
//! - $Params for parameters from master to slave
//! - $SlaveOK to signal the good reception of the parameters from a slve to master
//! - $Record to start a record from master to all slaves
//! - $Top for synch radio tops from master to all slaves
//! - $Stop to stop a record from master to all slaves

//-------------------------------------------------------------------------
// Constructor
CLoRaLink::CLoRaLink()
{
  link.SetLoRaLink();
  bDebug = false;
  //bDebug = true;
  bLora  = false;
  iErreur = ES_OK;
  sMessageReception[0] = 0;
}

//-------------------------------------------------------------------------
//! \brief Initialization of the connection
//! \return Return true if OK
bool CLoRaLink::Init()
{
  // Initialisation du broches LORA
  SPI.begin( LORA_SCK, LORA_MISO, LORA_MOSI, LORA_SS);
  LoRa.setPins( LORA_SS, LORA_RST, LORA_DI0);
#ifndef SETDEFAULTLORA
  // A réaliser avant begin
  LoRa.setTxPower(TXPOWER);
  LoRa.setSpreadingFactor(SPREADINGFACTOR);
#endif
  // Démarrage du service LORA
  if (!LoRa.begin( LORACNX, PABOOST))
  {
    Serial.println("Erreur initialisation du service Lora !");
    bLora = false;
  }
  else
  {
#ifndef SETDEFAULTLORA
    LoRa.setSignalBandwidth(BANDWIDTH);
    LoRa.setCodingRate4(CODINGRATE);
    LoRa.setPreambleLength(PREAMBLE_LENGTH);
    LoRa.setSyncWord(SYNC_WORD);
    if (LORACRC)
      LoRa.enableCrc();
    else
      LoRa.disableCrc();
#endif
    bLora = true;
    sMessageReception[0] = 0;
    LoRa.receive();
    if (bDebug) Serial.println("CLoRaLink::Init OK");
    if (bDebug) PrintConfigLoRaWAN();
  }
  iTimeLastM = 0; 
  return bLora;
}

//-------------------------------------------------------------------------
//! \brief Transmitting parameters to a slave
//! \param pParams Recording Parameters
//! \return Return true if OK
bool CLoRaLink::SendParams(
  ParamsOperator *pParams
  )
{
  if (bLora)
  {
    char *pMessage = link.CodeParams(pParams);
    //Serial.printf("CLoRaLink::SendParams [%s]\n", pMessage);
    bLora = SendMessage(pMessage);
    iTimeLastM = millis() + TIMELMPARAMS;
    if (bDebug) Serial.printf("CLoRaLink::SendParams [%s] bLora %d %d octets\n", pMessage, bLora, strlen(pMessage));
  }
  return bLora;
}

//-------------------------------------------------------------------------
//! \brief Transmitting start record from master to all slaves
//! \return Return true if OK
bool  CLoRaLink::SendStart()
{
  if (bLora)
  {
    char *pMessage = link.CodeStartRec();
    bLora = SendMessage(pMessage);
    iTimeLastM = millis() + TIMELMSTART;
    if (bDebug) Serial.printf("CLoRaLink::SendStart [%s] %d octets\n", pMessage, strlen(pMessage));
  }
  return bLora;
}

//-------------------------------------------------------------------------
//! \brief Transmitting sync top from master to all slaves
//! \return Return true if OK
bool  CLoRaLink::SendTop()
{
  if (bLora)
  {
    char *pMessage = link.CodeTopSynchro();
    bLora = SendMessage(pMessage);
    iTimeLastM = millis() + TIMELMSTART;
    if (bDebug) Serial.printf("CLoRaLink::SendTop [%s] %d octets\n", pMessage, strlen(pMessage));
  }
  return bLora;
}

//-------------------------------------------------------------------------
//! \brief Transmitting stop record from master to all slaves
//! \return Return true if OK
bool  CLoRaLink::SendStop()
{
  if (bLora)
  {
    char *pMessage = link.CodeStopRec();
    bLora = SendMessage(pMessage);
    iTimeLastM = millis() + TIMELMSTOP;
    if (bDebug) Serial.printf("CLoRaLink::SendStop [%s] %d octets\n", pMessage, strlen(pMessage));
  }
  return bLora;
}

//-------------------------------------------------------------------------
//! \brief Transmitting slave OK
//! \param iSlaveNumber Number of the slave
//! \param bOK Status of slave
//! \return Return true if OK
bool CLoRaLink::SendSlaveOK(
  int iSlaveNumber, bool bOK
  )
{
  if (bLora)
  {
    char *pMessage = link.CodeSlave( iSlaveNumber, bOK);
    bLora = SendMessage(pMessage);
    iTimeLastM = millis() + TIMELMSLAVE;
    if (bDebug) Serial.printf("CLoRaLink::SendSlaveOK [%s] %d octets\n", pMessage, strlen(pMessage));
  }
  return bLora;
}

//-------------------------------------------------------------------------
//! \brief Transmitting stop mode from master to all slaves
//! \return Return true if OK
bool CLoRaLink::SendStopMode()
{
  if (bLora)
  {
    char *pMessage = link.CodeStopMode();
    bLora = SendMessage(pMessage);
    iTimeLastM = millis() + TIMELMSTM;
    if (bDebug) Serial.printf("CLoRaLink::SendStopMode [%s] %d octets\n", pMessage, strlen(pMessage));
  }
  return bLora;
}

//-------------------------------------------------------------------------
//! \brief Transmitting Test message to Teensy
//! \return Return true if OK
bool CLoRaLink::SendTest()
{
  if (bLora)
  {
    char *pMessage = link.CodeTest();
    bLora = SendMessage(pMessage);
    iTimeLastM = millis() + TIMELMTST;
    if (bDebug) Serial.printf("CLoRaLink::SendTest [%s] %d octets\n", pMessage, strlen(pMessage));
  }
  return bLora;
}

//-------------------------------------------------------------------------
//! \brief Emission of the prepared message
//! \param pMessage string to send
//! \return Return true if OK
bool CLoRaLink::SendMessage(
  char *pMessage)
{
  if (iTimeLastM != 0 and millis() < iTimeLastM)
    // Wait sending last message
    delay(iTimeLastM-millis());
  iTimeLastM = 0;
  bool bOK = true;
  if (LoRa.beginPacket() < 1)
  {
    if (bDebug) Serial.printf("CLoRaLink::SendMessage(%s) Erreur beginPacket\n");
    bOK = false;
  }
  if (LoRa.print(pMessage) < 1)
  {
    if (bDebug) Serial.printf("CLoRaLink::SendMessage(%s) Erreur print\n");
    bOK = false;
  }
  if (LoRa.endPacket() < 1)
  {
    if (bDebug) Serial.printf("CLoRaLink::SendMessage(%s) Erreur endPacket\n");
    bOK = false;
  }
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief Processing message receptions
//! \return Returns the number of the received message
int CLoRaLink::OnReceive()
{
  //Serial.println("CLoRaLink::OnReceive");
  int iReturn = LMNOMESS;
  // Test if there are received characters
  int packetSize = LoRa.parsePacket();
  if (packetSize)
  {
    sMessageReception[0] = 0;
    //if (bDebug) Serial.printf("CLoRaLink::OnReceive packetSize %d\n", packetSize);
    // Reading bytes of the message
    int iPos = 0;
    while (LoRa.available())
    {
      sMessageReception[iPos] = (char)LoRa.read();
      iPos++;
      if (iPos > 250)
        break;
    }
    sMessageReception[iPos] = 0;
    iRSSI = LoRa.packetRssi();
    iErreur = ES_OK;
    if (bDebug) Serial.printf("CLoRaLink::OnReceive [%s] %d octets\n", sMessageReception, strlen(sMessageReception));
    iReturn = LMUNKNOW;
    // Lora message from PRSS
    iReturn = link.DecodeMessageType( sMessageReception);
    switch( iReturn)
    {
    case LMPARAMS  :  //!< Send parameters
      //Serial.println("Avant DecodeParams");
      if (!link.DecodeParams( pParams, sMessageReception))
      {
        if (link.IsErrorCRC())
          iErreur = ES_ERCRC;
        else
          iErreur = ES_ERPARAM;
        Serial.printf("CLoRaLink::OnReceive %s", link.GetErrorString());
      }
      //Serial.println("Après DecodeParams");
      break;
    case LMSLAVE   :  //!< Slave OK
      if (!link.DecodeSlave( &iSlaveNumber, &bSlaveStatus, sMessageReception))
      {
        if (link.IsErrorCRC())
          iErreur = ES_ERCRC;
        else
          iErreur = ES_ERSLAVE;
        Serial.printf("CLoRaLink::OnReceive %s", link.GetErrorString());
      }
      break;
    case LMTEST    :  //!< Test message
      Serial.println("CLoRaLink::OnReceive LMTEST");
      break;
    case LMOTHERLORA:
      // Nothing to do, Lora message send by an other device
      break;
    case LMSTARTREC:
    case LMSTOPREC:
    case LMTOP:
    case LMSTOPMODE:
      // Nothing to do
      break;
    default:
      Serial.printf("CLoRaLink::OnReceive error decoding num message %d [%s]\n", iReturn, sMessageReception);
      iErreur = ES_ERLORMESS;
    }
  }
  return iReturn;
}
