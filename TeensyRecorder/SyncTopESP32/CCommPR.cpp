//! \file CCommPR.cpp
//! \brief Definition of the CCommPR class for the serial Teensy Recorder dialogue
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2019 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "CCommPR.h"

//-------------------------------------------------------------------------
//! \class CCommPR
//! \brief Class for the serial Teensy Recorder dialogue
//! Messages exchanged:
//! - $Params for parameters from Passive Recorder to ESP32 and a slave
//! - &SlaveOK to signal the good reception of the parameters from a slave to Passive Recorder
//! - $SlaveN  to transfert slave number from Passive Recorder

//-------------------------------------------------------------------------
//! \brief Constructor
CCommPR::CCommPR():hSerial(2)
{
  bDebug = false;
  //bDebug = true;
  bMaster = false;
  sMessageReception[0] = 0;
  iPos   = 0;
  bFirst = false;
  bEnd = false;
  iErreur = ES_OK;
  iTimeStatus = 0;
}

//-------------------------------------------------------------------------
//! \brief Initialization of serial link
//! \return Return true if OK
bool CCommPR::Init()
{
  hSerial.begin(19200, SERIAL_8N1, PIN_RX, PIN_TX);
  iPos   = 0;
  bFirst = false;
  bEnd = false;
  return true;
}

//-------------------------------------------------------------------------
//! \brief Transmitting parameters to a slave Passive Recorder
//! \param pParams Recording Parameters
void CCommPR::SendParams(
  ParamsOperator *pParams
  )
{
  char *pMessage = link.CodeParams(pParams);
  hSerial.write( (const uint8_t *)pMessage, strlen(pMessage));
  //hSerial.println( pMessage);
  if (bDebug) Serial.printf("CCommPR::SendParams [%s]\n", pMessage);
}

//-------------------------------------------------------------------------
//! \brief Transmitting slave result about parameters received
//! \param iSlave Slave number
//! \param bOK    true if slave parameters are OK
void CCommPR::SendSlaveOK(
  int iSlave, bool bOK
  )
{
  char *pMessage = link.CodeSlave( iSlave, bOK);
  hSerial.write( (const uint8_t *)pMessage, strlen(pMessage));
  if (bDebug) Serial.printf("CCommPR::SendSlaveOK [%s]\n", pMessage);
}

//-------------------------------------------------------------------------
//! \brief Transmitting stop mode from master to all slaves
void CCommPR::SendStopMode()
{
  char *pMessage = link.CodeStopMode();
  hSerial.write( (const uint8_t *)pMessage, strlen(pMessage));
  if (bDebug) Serial.printf("CCommPR::SendStopMode [%s]\n", pMessage);
}

//-------------------------------------------------------------------------
//! \brief Transmitting ESP32 status to Teensy
//! \param iStatus ESP32 status
void CCommPR::SendESP32Status(
  int  iStatus
  )
{
  char *pMessage = link.CodeESP32Status( bMaster, iStatus);
  hSerial.write( (const uint8_t *)pMessage, strlen(pMessage));
  if (bDebug)
  {
    if (iTimeStatus != 0)
      Serial.printf("CCommPR::SendESP32Status [%s] %dms\n", pMessage, millis()-iTimeStatus);
    else
      Serial.printf("CCommPR::SendESP32Status [%s]\n", pMessage);
    iTimeStatus = millis();
  }
}

//-------------------------------------------------------------------------
//! \brief Transmitting Test message to Teensy
void CCommPR::SendTest()
{
  char *pMessage = link.CodeTest();
  hSerial.write( (const uint8_t *)pMessage, strlen(pMessage));
  if (bDebug) Serial.printf("CCommPR::SendTest [%s]\n", pMessage);
}

//-------------------------------------------------------------------------
//! \brief Processing message receptions
//! \return Returns the number of the received message
int CCommPR::OnReceive()
{
  //Serial.println("CCommPR::OnReceive");
  int iReturn = LMNOMESS;
  bool bOK  = false;
  while (hSerial.available())
  {
    sMessageReception[iPos] = (char)hSerial.read();
    if (sMessageReception[iPos] == '!' and bFirst)
    {
      // End of a message
      sMessageReception[iPos+1] = 0;
      bEnd = true;
      break;
    }
    else if (sMessageReception[iPos] == '$')
      bFirst = true;
    if (bFirst)
    {
      iPos++;
      if (iPos > 250)
        break;
    }
  }
  if (bEnd)
  {
    iErreur = ES_OK;
    if (bDebug) Serial.printf("CCommPR::OnReceive [%s]\n", sMessageReception);
    // Decoding the message
    iReturn = link.DecodeMessageType( sMessageReception);
    switch( iReturn)
    {
    case LMPARAMS  :  //!< Send parameters
      bOK = link.DecodeParams( pParams, sMessageReception);
      if (bDebug)
      {
        if (bOK)
          Serial.printf("CCommPR::OnReceive LMPARAMS Date %s, HD %s, HF %s, MaxDur %d, FE %d, GN %d, HPF %d, EXP10 %d, Type %d, FrqTop %d, DurTop %d, PerTop %d\n",
            pParams->sDateHour, pParams->sHDebut, pParams->sHFin, pParams->uiDurMax, pParams->uiFe, pParams->uiGainNum, 
            pParams->iFreqFiltreHighPass, pParams->bExp10, pParams->iMasterSlave, pParams->iTopAudioFreq, pParams->iDurTop, pParams->iTopPer);
         else
         {
           Serial.printf( link.GetErrorString());
           iErreur = ES_ERPARAM;
         }
      }
      break;
    case LMSLAVE   :  //!< Slave OK
      if (!link.DecodeSlave( &iSlaveOK, &bSlaveStatus, sMessageReception))
         iErreur = ES_ERSLAVE;
      if (bDebug) Serial.printf("CCommPR::OnReceive LMSLAVE iSlaveOK %d, bSlaveStatus %d\n", iSlaveOK, bSlaveStatus);
      break;
    case LMSTOPMODE:
      if (bDebug) Serial.printf("CCommPR::OnReceive LMSTOPMODE\n");
      break;
    case LMTEST    :  //!< Test message
      Serial.println("CCommPR::OnReceive LMTEST");
      break;
    case LMRVERSION :  //<! Version message
      {
        char *pMessage = link.CodeVersion( VERSIONESP32);
        hSerial.write( (const uint8_t *)pMessage, strlen(pMessage));
        if (bDebug) Serial.printf("SendVersion [%s]\n", pMessage);
      }
      break;
    default:
      Serial.printf("CCommPR::OnReceive error decoding num message [%s]\n", sMessageReception);
      iErreur = ES_ERSERMESS;
    }
    // RAZ message
    iPos = 0;
    sMessageReception[0] = 0;
    bFirst = false;
    bEnd = false;
  }
  return iReturn;
}
