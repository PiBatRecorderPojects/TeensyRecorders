//! \file CTopGeneration.h
//! \brief Definition of the CTopGeneration class for the generation of audio tops
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2019 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "arduino.h"
#include "driver/i2s.h"
#include "esp_err.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include "Const.h"

#ifndef CTOPGENERATION_H
#define CTOPGENERATION_H

//-------------------------------------------------------------------------
//! \class CTopGeneration
//! \brief Class for the generation of audio tops
/* Audio Top duration in ms
Fe (kHz) 256   512   1024   2048  4096 – Nb Ech
192      1,33  2,67  5,33  10,67  21,33
250      1,02  2,05  4,10   8,19  16,38
384      0,67  1,33  2,67   5,33  10,67
*/

class CTopGeneration
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor
  CTopGeneration();

  //-------------------------------------------------------------------------
  //! \brief Destructor
  ~CTopGeneration();
  
  //-------------------------------------------------------------------------
  //! \brief Initialisation
  //! \param iFe Sample rate in enum FREQECH
  //! \param iFreq Frequency on the top in Hz
  //! \param iNb Samples number in enum TOPSIZE
  //! \param iCore Core number used by reading (0 or 1)
  //! \param bTh Indicate thread or not to generate top
  //! \return Return true if OK
  bool Init(
    int iFe, int iFreq, int iNb, int iCore, bool bTh
    );

  //-------------------------------------------------------------------------
  //! \brief Start audio top
  //! \param iDelay Delay in µs before sending top
  void StartTop(
    int iDelay
    );

  //-------------------------------------------------------------------------
  //! \brief Stop audio top
  void StopTop();

  //-------------------------------------------------------------------------
  //! \brief Displays an eventual I2S Manager error
  static void AffESP_Err (char *MessA, esp_err_t code, char *MessB);

protected:
  //-------------------------------------------------------------------------
  //! \brief Audio generation thread
  static void TopGenerationTask( void *arg);

  //-------------------------------------------------------------------------
  //! \brief Audio playing
  //! \param iStopTop CPU time to stop top
  static void TopPlaying(
    int64_t iStopTop
    );

  //! Thread handle
  static TaskHandle_t hTacheLecture;
  
  //! Thread core number
  static int iCoreTask;
  
  //! I2S port number
  static int i2s_num;

  //! I2S configuration
  static i2s_config_t i2s_config;

  //! I2S configuration pins
  static i2s_pin_config_t pin_config;

  //! Bool true if i2s drviver is install
  static bool bDiverI2S;

  //! Buffer samples of the top audio
  static uint16_t samplesBuffer[BUFFSIZE+1];

  //! Number of sample
  static int iNbSample;

  //! Sample rate in Hz
  static int iFech;

  //! Sample Top size in bytes
  static int iSizeTop;

  //! \struct complexe
  //! \brief Definition of a complex for the calculation of the samples of the top audio
  struct complexe
  {
    float Re; //!< Real part
    float Im; //!< Imaginary part
  };

  //! Top duration in µs
  static int64_t iTopDur;

  //! Time top start if delay
  static int64_t iTimeStart;

  //! Bool to send Top
  static bool bTop;

  //! Critical section for multitasking iTimeStart usage
  static portMUX_TYPE SectionCritiqueMux;

  // Indicate thread or not to generate top
  bool bThread;
};

#endif
