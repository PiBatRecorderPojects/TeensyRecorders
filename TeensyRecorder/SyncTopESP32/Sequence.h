//! \file Sequence.h
//! \brief Definition of the CCommPR class for the serial Teensy Recorder dialogue
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2019 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// Complete sequence of exchanges over a recording period
//
// -----> Hardware pin
// >>>>>> Serial Link
// =====> Radio Lora Link
//
// PR Master                                        TopSync                                        TopSync                                        PR Slave
// Teensy 3.6                                        ESP32                                          ESP32                                         Teensy 3.6
//
// Start or wakeup                                                                                                                                Start or wakeup
// Set ESPON to 1
// Set Sleep to 1 ---------------------------------> Start                                          Start <--------------------------------------- Set Sleep to 1
// Set Master -------------------------------------> Master                                         Slave <--------------------------------------- Set Slave
// Init Master                                       Wait                                           Wait                                           Wait in init Slave
// Sending Params to slave 1 >>>>>>>>>>>>>>>>>>>>>>> Sending Params to Slave 1 ===================> Sending Params to Slave 1 >>>>>>>>>>>>>>>>>>>> Slave 1 Receives Params
// Slave 1 OK <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Slave 1 OK <================================== Slave 1 OK <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Slave 1 OK
//                                                                                                                                                 Slave 1 switch to recording mode
// Sending Params to slave n >>>>>>>>>>>>>>>>>>>>>>> Sending Params to Slave n ===================> Sending Params to Slave n >>>>>>>>>>>>>>>>>>>> Slave n Receives Params
// Slave n OK <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Slave n OK <================================== Slave n OK <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Slave n OK
// All slaves OK switch to recording mode                                                                                                          Slave n switch to recording mode
// Wait for signal
// Signal, begin recording wave
// Set Record -------------------------------------> Send StartRecord ============================> Set Record ----------------------------------> Begin recording wave
// Each second Top in wave ------------------------> Send Top ====================================> Generate Audio Top
//                                                   800µs later Generate Audio Top
// ...
// Stop wave recording
// Reset Record -----------------------------------> Send StopRecord =============================> Reset Record --------------------------------> Stop wave recording
// ...
// Time to Sleep, switch to standby mode                                                                                                           Time to sleep, switch to stanby mode
// Set Sleep to 0 ---------------------------------> Stop                                           Stop <---------------------------------------- Set Sleep to 0
// Set ESPON to 0

/*
	GPIO PINS
			           TEENSY	  ESP32
	ESPON          23 Out		       1 to put ESP32 ON
	ESP32_READY	   22 In	  23 Out 0 when ready
	MASTER	       04 Out	  32 In  0 for slave and 1 for master
	SLEEP	         01 Out   12 In  1 for work abd 0 for sleep
	RECORD MASTER	 00 Out   13 In  0 for standby and 1 for recording
	RECORD SLAVE   00 In    13 Out 0 for standby and 1 for recording
*/

/*
	Link and Radio messages
	
	$PAR for parameters from master to slave
		Send by Link from Teensy Master to ESP32 Master
		Send by Radio from ESP32 Master to ESP32 Slave
		Send by Link from ESP32 Slave to Teensy Slave
	"$PAR,SlaveNum,DateHour,BeginHour,EndHour,AbsThres,Theshold,Fe,NumGain,TimeExp,HPFFreq,FreqTop,DurTop,TopPer!"
	- DateHour   Date and Hour format DD/MM/YY-HH:mm:SS
	- BeginHour  Hour to beging recording format HH:mm
	- EndHour    Hour to stop recording format HH:mm
	- AbsThres   Threshold type, false (0) relative, true (1) absolute
	- Theshold   Threshold in dB, (5 to 99 in relative or -110 to -30 in absolute)
	- Fe         Sample rate (2 for 192kHz, 3 for 250kHz, 4 for 384kHz)
	- NumGain    Numeric gain (0 for 0dB, 1 for +6dB, 2 for +12dB, 3 for +18dB, 4 for +24dB)
	- TimeExp    Time expansion, true (1) for X10, false (0) for X1
	- HPFFreq    Hight pass filter frequency in kHz (0 to 25 kHz)
	- FreqTop    Top audio frequency in kHz (1 to 50 kHz)
	- DurTop     Top audio duration in samples (256, 512 or 1024)
	- TopPer     Top audio period (0 for one at the start of the recording, 1 to 10 for one every 1 to 10 seconds)
	
	&Slave to signal the good reception of the parameters from a slave to master
		Send by Link from Teensy Slave to ESP32 Slave
		Send by Radio from ESP32 Slave to ESP32 Master
		Send by Link from ESP32 Master to Teensy Master
	"$SLAVE,OK,SlaveNum!"
	- OK		 1 if OK else 0
	- SlaveNum   Number of the slave (1 to 10)
	
	$Start to start a record from master to all slaves
		Send by Link from Teensy Master to ESP32 Master
		Send by Radio from ESP32 Master to ESP32 Slave
		Send by Link from ESP32 Slave to Teensy Slave
	"$START!"
	
	$Top for synch radio tops from master to all slaves
		Send by Link from Teensy Master to ESP32 Master
		Send by Radio from ESP32 Master to ESP32 Slave
		Send by Link from ESP32 Slave to Teensy Slave
	"$T!"
	
	$Stop to stop a record from master to all slaves
		Send by Link from Teensy Master to ESP32 Master
		Send by Radio from ESP32 Master to ESP32 Slave
		Send by Link from ESP32 Slave to Teensy Slave
	"$STOP!"
*/
