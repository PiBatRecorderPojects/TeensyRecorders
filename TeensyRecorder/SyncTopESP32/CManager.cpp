//! \file CManager.cpp
//! \brief Definition of the CCodeLink class for the serial Teensy Recorder to ESP32 dialogue and Lora message
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2019 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "CManager.h"

#define PRINTLOOP 5000
//#define MESSAGETESTSERIAL
//#define MESSAGETESTLORA
/*
 * - CManager
 * -   CMasterManager
 * -   CSlaveManager
 */

CMasterManager *pMasterManager = NULL;

//-------------------------------------------------------------------------
//! \brief Audio Top Timer traitment
void IRAM_ATTR OnTimerTop()
{
  pMasterManager->OnTopTimer();
}

//-------------------------------------------------------------------------
//! \class CManager
//! \brief Generic Class for Master or Slave manager

//-------------------------------------------------------------------------
//! \brief Constructor
CManager::CManager()
{
  bDebug = false;
  //bDebug = true;
  bError = false;
  // No test link
  iTimeTest = 0;
  // To test link
#ifdef MESSAGETESTSERIAL
  iTimeTest = millis() + 1000;
#endif
#ifdef MESSAGETESTLORA
  iTimeTest = millis() + 1000;
#endif
  iTimeLoop = 0;
}

//-------------------------------------------------------------------------
//! \brief Destructor
CManager::~CManager()
{
}

//-------------------------------------------------------------------------
//! \brief Init manager
void CManager::Init()
{
  // Initialize status message
  iESP32Status = ES_OK;
  iTimeStatus = millis() + TIMESTATUSESP;
  // Initialize default parameters
  Params.iMasterSlave = MASTER;     // Number of the slave
  Params.uiDurMax     = 10;         // Max duration in seconds
  Params.uiFe         = FE384KHZ;   // Sample rate (2 for 192kHz, 3 for 250kHz, 4 for 384kHz)
  Params.uiGainNum    = GAIN0dB;    // Numeric gain (0 for 0dB, 1 for +6dB, 2 for +12dB, 3 for +18dB, 4 for +24dB
  Params.bExp10       = true;       // Time expansion (true for X10)
  Params.iFreqFiltreHighPass = 0;   // Hight pass filter frequency in kHz
  Params.iTopAudioFreq= 10;         // Audio top frequency in kHz
  Params.iDurTop      = TS512;      // Audio top sample number (256, 512 or 1024)
  Params.iTopPer      = 0;          // Audio top period (0 to 10)
  Params.iLEDSynchro  = NOLEDSYNCH; // Using LED in synchro mode (No, Record ou 3 record only)
  strcpy( Params.sDateHour,  "09/05/19-13:49:53"); //!< Date and Hour format DD/MM/YY-HH:mm:SS
  strcpy( Params.sHDebut, "22:00");                //!< Hour to beging recording format HH:mm
  strcpy( Params.sHFin,   "06:00");                //!< Hour to stop recording format HH:mm
  // Initialize serial link between ESP32 and Teensy
  serialLink.SetpParams( &Params);
  serialLink.Init();
  // Initialize LORA link between Master and Slaves
  loraLink.SetpParams( &Params);
  if (!loraLink.Init())
  {
    iESP32Status = ES_ERLORA;
    bError = true;
  }
  if (bDebug) iTimeLoop = millis() + PRINTLOOP;
#ifdef USE_LED
  iTimeLed = 0;
  iLed = 5;
  SetLEDMode();
#endif
}

//-------------------------------------------------------------------------
//! \brief OnLoop traitment
void CManager::OnLoop()
{
  if (millis() > iTimeStatus)
  {
    serialLink.SendESP32Status( iESP32Status);
    //Serial.printf("CManager::CodeESP32Status(%d)\n", iESP32Status);
    iTimeStatus = millis() + TIMESTATUSESP;
  }
#ifdef MESSAGETESTSERIAL
  if (iTimeTest != 0 and millis() > iTimeTest)
  {
    serialLink.SendTest();
    //Serial.println("CManager::OnLoop SendTest");
    iTimeTest = millis() + 1000;
  }
#endif
#ifdef MESSAGETESTLORA
  if (iTimeTest != 0 and millis() > iTimeTest)
  {
    if (!loraLink.SendTest()
      iESP32Status = ES_ERLORA;
    //Serial.println("CManager::OnLoop SendTest");
    iTimeTest = millis() + 1000;
  }
#endif
  if (serialLink.GetError() != ES_OK)
    iESP32Status = serialLink.GetError();
  if (loraLink.GetError() != ES_OK)
    iESP32Status = loraLink.GetError();
#ifdef USE_LED
  if (millis() > iTimeLed)
  {
    iTimeLed = millis() + 100;
    iLed++;
    if (iLed >= 9)
      iLed = 0;
    if (bLED[iLed])
      digitalWrite( PIN_LED, HIGH);
    else
      digitalWrite( PIN_LED, LOW);
  }
#endif
#ifdef USE_LED
  if (!bError and iESP32Status != ES_OK)
  {
    bError = true;
    SetLEDMode();
  }
#endif
}

#ifdef USE_LED
  //-------------------------------------------------------------------------
  //! \brief Set LED mode
  void CManager::SetLEDMode()
  {
    if (bError)
    {
      // 900ms allumée et 100ms étainte
      bLED[0]= true; bLED[1]= true; bLED[2]= true; bLED[3]= true; bLED[4]= true; bLED[5]= true; bLED[6]= true; bLED[7]= true; bLED[8]= true; bLED[9]=false;
    }
    else if (bMaster)
    {
      if (bWaitingtSlave)
      {
        // 200ms allumée et 300ms étainte
        bLED[0]= true; bLED[1]= true; bLED[2]=false; bLED[3]=false; bLED[4]=false; bLED[5]= true; bLED[6]= true; bLED[7]=false; bLED[8]=false; bLED[9]=false;
      }
      else if (bRecord)
      {
        // 500ms allumée, 500ms éteinte
        bLED[0]= true; bLED[1]= true; bLED[2]= true; bLED[3]= true; bLED[4]= true; bLED[5]=false; bLED[6]=false; bLED[7]=false; bLED[8]=false; bLED[9]=false;
      }
      else
      {
        // 100ms allumée et 800ms éteinte
        bLED[0]= true; bLED[1]=false; bLED[2]=false; bLED[3]=false; bLED[4]=false; bLED[5]=false; bLED[6]=false; bLED[7]=false; bLED[8]=false; bLED[9]=false;
      }
    }
    else
    {
      if (bWaitingtMaster)
      {
        // 3 brefs de 100ms sur 1s
        bLED[0]= true; bLED[1]=false; bLED[2]= true; bLED[3]=false; bLED[4]= true; bLED[5]=false; bLED[6]=false; bLED[7]=false; bLED[8]=false; bLED[9]=false;
      }
      else if (bWaitingTeensy)
      {
        // 2 brefs de 100ms sur 1s
        bLED[0]= true; bLED[1]=false; bLED[2]= true; bLED[3]=false; bLED[4]=false; bLED[5]=false; bLED[6]=false; bLED[7]=false; bLED[8]=false; bLED[9]=false;
      }
      else if (bRecord)
      {
        // 500ms allumée, 500ms éteinte
        bLED[0]= true; bLED[1]= true; bLED[2]= true; bLED[3]= true; bLED[4]= true; bLED[5]=false; bLED[6]=false; bLED[7]=false; bLED[8]=false; bLED[9]=false;
      }
      else
      {
        // 100ms allumée et 800ms éteinte
        bLED[0]= true; bLED[1]=false; bLED[2]=false; bLED[3]=false; bLED[4]=false; bLED[5]=false; bLED[6]=false; bLED[7]=false; bLED[8]=false; bLED[9]=false;
      }
    }
  }
#endif

//! Parameters received by parameters message
ParamsOperator CManager::Params;

//-------------------------------------------------------------------------
//! \class CMasterManager
//! \brief Class for Master manager

//-------------------------------------------------------------------------
//! \brief Constructor
CMasterManager::CMasterManager()
{
  pMasterManager  = this;
  pTopTimer       = NULL;
  iTimeStartTimer = 0;
  bMaster         = true;
  bRecord         = false;
  bWaitingtMaster = false;
  bWaitingtSlave  = true;
  bWaitingTeensy  = false;
  serialLink.SetMasSlv( true);
}

//-------------------------------------------------------------------------
//! \brief Destructor
CMasterManager::~CMasterManager()
{
}

//-------------------------------------------------------------------------
//! \brief Init manager
void CMasterManager::Init()
{
  // Call basic function
  CManager::Init();
/*
  GPIO PINS
                 TEENSY   ESP32
  ESPON          23 Out          1 to put ESP32 ON
  ESP32_READY    22 In    23 Out 0 when ready
  MASTER         04 Out   32 In  0 for slave and 1 for master
  SLEEP          01 Out   12 In  1 for work abd 0 for sleep
  RECORD MASTER  00 Out   13 In  0 for standby and 1 for recording
  RECORD SLAVE   00 In    13 Out 0 for standby and 1 for recording
*/
  // Set Pin record as input
  gpio_set_direction((gpio_num_t)PIN_RECORD, GPIO_MODE_INPUT);
  gpio_set_pull_mode((gpio_num_t)PIN_RECORD, GPIO_PULLUP_ONLY);
  bRecord = false;
  iTimeStartTimer = 0;
}

//-------------------------------------------------------------------------
//! \brief OnLoop traitment
void CMasterManager::OnLoop()
{
  if (iTimeLoop != 0 and millis() > iTimeLoop)
  {
    iTimeLoop = millis() + PRINTLOOP;
    Serial.println("CMasterManager::OnLoop()");
  }
  //Serial.println("CMasterManager::OnLoop");
  // Test if receive message from Teensy
  int iSerialMessage = serialLink.OnReceive();
  if (bDebug and iSerialMessage != LMNOMESS) Serial.printf("CMasterManager::OnLoop serialLink.OnReceive = %d\n", iSerialMessage);
  if (iSerialMessage == LMPARAMS)
  {
    // Initialize audio tops manager
    if (!audioTops.Init( Params.uiFe, Params.iTopAudioFreq*1000, Params.iDurTop, 1, false))
      iESP32Status = ES_ERTOP;
    int iNbSample = 0;
    switch(Params.iDurTop)
    {
    case TS256 : iNbSample =  256; break; //  256 samples
    default:
    case TS512 : iNbSample =  512; break; //  512 samples
    case TS1024: iNbSample = 1024; break; // 1024 samples
    case TS2048: iNbSample = 2048; break; // 2048 samples
    case TS4096: iNbSample = 4096; break; // 4096 samples
    }
    if (bDebug) Serial.printf("Params : Freq %dkHz, Samples %d, Periode %ds\n", Params.iTopAudioFreq, iNbSample, Params.iTopPer);
    // Send parameters to all slave
    if (!loraLink.SendParams( &Params))
    {
      iESP32Status = ES_ERLORA;
      bError = true;
      Serial.println("Erreur sur loraLink.SendParams !");
    }
  }
  else if (iSerialMessage == LMSTOPMODE)
  {
    if (bDebug) Serial.println("CMasterManager::OnLoop Send SendStopMode ont LoRa\n");
    // Send stop mode to all slave
    if (!loraLink.SendStopMode())
      iESP32Status = ES_ERLORA;
  }
  else if (iSerialMessage == LMTEST)
    Serial.println("CMasterManager::OnLoop receive LMTEST");

  // Test if receive message from LORA
  int iLoraMessage = loraLink.OnReceive();
  if (bDebug and iLoraMessage != LMNOMESS) Serial.printf("CMasterManager::OnLoop loraLink.OnReceive = %d\n", iLoraMessage);
  if (iLoraMessage == LMSLAVE)
  {
    if (bDebug) Serial.printf("CMasterManager::OnLoop serialLink.SendSlaveOK( %d, %d)\n", loraLink.GetSlaveNumber(), loraLink.GetSlaveStatus());
    // Send message to Teensy
    serialLink.SendSlaveOK( loraLink.GetSlaveNumber(), loraLink.GetSlaveStatus());
    bWaitingtSlave = false;
#ifdef USE_LED
    SetLEDMode();
#endif
  }

  // Record pin traitment
  PinRecord();

  // Test if it is time to start timer
  if (iTimeStartTimer > 0 and iTimeStartTimer < esp_timer_get_time())
  {
    // First Top
    if (bDebug) Serial.println("CMasterManager::OnLoop First Top SetFlag");
    flagTop.SetFlag();
    if (Params.iTopPer > 0)
      // Start Top Timer
      StartTimer();
    iTimeStartTimer = 0;
    //if (bDebug) Serial.println("StartTimer OK");
  }
  
  // Test si génération Top
  if (flagTop.ReadAndResetFlag())
  {
    if (iTimeSend == 0)
      iTimeSend = esp_timer_get_time();
    if (bDebug) Serial.println("audioTops.StartTop");
    // Send Top to all slaves
    if (!loraLink.SendTop())
      iESP32Status = ES_ERLORA;
    if (iTimeTop == 0)
      iTimeTop = esp_timer_get_time();
    // Start master audio Top
    audioTops.StartTop( MASTER_DELAY+PLUSTOP);
    //Serial.println("Après audioTops.StartTop");
  }

  CManager::OnLoop();
  //Serial.println("End CMasterManager::OnLoop");
}

//-------------------------------------------------------------------------
//! \brief Record pin traitment
void CMasterManager::PinRecord()
{
  if (digitalRead( PIN_RECORD) == HIGH and !bRecord)
  {
    iTimeTop  = 0;
    iTimeSend = 0;
    iTimeRec = esp_timer_get_time();
    // Start recording
    if (bDebug) Serial.println("CMasterManager::OnRecord Record");
    bRecord = true;
    // Send start record to all slaves
    if (!loraLink.SendStart())
      iESP32Status = ES_ERLORA;
    //if (bDebug) Serial.println("SendStart OK");
    // Start time to start top timer in 100ms
    iTimeStartTimer = esp_timer_get_time() + 300000;
#ifdef USE_LED
    SetLEDMode();
#endif
  }
  else if (digitalRead( PIN_RECORD) == LOW and bRecord)
  {
    Serial.printf("Master diff. Rec-SendTop %dµs, Rec-Top = %dµs\n", iTimeSend-iTimeRec, iTimeTop-iTimeRec);
    // Stop recording
    if (bDebug) Serial.println("CMasterManager::OnRecord Stop record");
    bRecord = false;
    // Stop Top timer
    StopTimer();
    //if (bDebug) Serial.println("StopTimer OK");
    // Send stop record to all slaves
    if (!loraLink.SendStop())
      iESP32Status = ES_ERLORA;
    //if (bDebug) Serial.println("SendStop OK");
#ifdef USE_LED
    SetLEDMode();
#endif
  }
}

//-------------------------------------------------------------------------
//! \brief Start Timer forSlave Top
void CMasterManager::StartTimer()
{
  // Use 1st timer of 4 (counted from zero)
  // Set 80 divider for prescaler (see ESP32 Technical Reference Manual for more info)
  pTopTimer = timerBegin(0, 80, true);
  // Attach onTimer function to our timer
  timerAttachInterrupt(pTopTimer, OnTimerTop, true);
  // Set alarm to call onTimer function every n second (value in microseconds)
  // Repeat the alarm if not 0
  if (Params.iTopPer == 0)
    // Only one timer
    timerAlarmWrite(pTopTimer, 1000000, false);
  else
    // Repeat timer
    timerAlarmWrite(pTopTimer, Params.iTopPer*1000000, true);
  // Start timer
  timerAlarmEnable(pTopTimer);
}

//-------------------------------------------------------------------------
//! \brief Stop Timer for Master and Slave Top
void CMasterManager::StopTimer()
{
  if (pTopTimer)
  {
    // Stop and free timer
    timerEnd(pTopTimer);
    pTopTimer = NULL;
  }
}

//-------------------------------------------------------------------------
//! \brief Audio Top Timer traitment
void CMasterManager::OnTopTimer()
{
  flagTop.SetFlag();
  Serial.println("CMasterManager::OnTopTimer SetFlag");
}


//-------------------------------------------------------------------------
//! \class CSlaveManager
//! \brief Class for Slave manager

//-------------------------------------------------------------------------
//! \brief Constructor
CSlaveManager::CSlaveManager()
{
  bMaster = false;
  bRecord = false;
  bWaitingtMaster = true;
  bWaitingtSlave  = false;
  bWaitingTeensy  = false;
  serialLink.SetMasSlv( false);
}

//-------------------------------------------------------------------------
//! \brief Destructor
CSlaveManager::~CSlaveManager()
{
}

//-------------------------------------------------------------------------
//! \brief Init manager
void CSlaveManager::Init()
{
  CManager::Init();
/*
  GPIO PINS
                 TEENSY   ESP32
  ESPON          23 Out          1 to put ESP32 ON
  ESP32_READY    22 In    23 Out 0 when ready
  MASTER         04 Out   32 In  0 for slave and 1 for master
  SLEEP          01 Out   12 In  1 for work abd 0 for sleep
  RECORD MASTER  00 Out   13 In  0 for standby and 1 for recording
  RECORD SLAVE   00 In    13 Out 0 for standby and 1 for recording
*/
  // Set Pin record as output and 0
  //gpio_set_direction((gpio_num_t)PIN_RECORD, GPIO_MODE_OUTPUT);
  gpio_set_direction((gpio_num_t)PIN_RECORD, GPIO_MODE_OUTPUT_OD);
  digitalWrite( PIN_RECORD, LOW);
}

//-------------------------------------------------------------------------
//! \brief OnLoop traitment
void CSlaveManager::OnLoop()
{
  if (iTimeLoop != 0 and millis() > iTimeLoop)
  {
    iTimeLoop = millis() + PRINTLOOP;
    Serial.println("CSlaveManager::OnLoop()");
  }
  // Test if receive message from Lora
  int iNbSample = 0;
  int iMessLora = loraLink.OnReceive();
  switch(iMessLora)
  {
  case LMPARAMS:
    // Set parameters
    Params = *(loraLink.GetParameters());
    // Initialize audio tops manager
    audioTops.Init( Params.uiFe, Params.iTopAudioFreq*1000, Params.iDurTop, 1, false);
    // Send parameters to Slave Passive Recorder
    serialLink.SendParams( &Params);
    
    switch(Params.iDurTop)
    {
    case TS256 : iNbSample =  256; break; //  256 samples
    default:
    case TS512 : iNbSample =  512; break; //  512 samples
    case TS1024: iNbSample = 1024; break; // 1024 samples
    case TS2048: iNbSample = 2048; break; // 2048 samples
    case TS4096: iNbSample = 4096; break; // 4096 samples
    }
    if (bDebug)
      Serial.printf("Params : Freq %dkHz, Samples %d, Periode %ds\n", Params.iTopAudioFreq, iNbSample, Params.iTopPer);
    bWaitingtMaster = false;
    bWaitingTeensy  = true;
#ifdef USE_LED
    SetLEDMode();
#endif
    break;
  case LMTOP:
    if (!bRecord)
      // Start record
      digitalWrite( PIN_RECORD, HIGH);
    bRecord = true;
    if (iTimeTop == 0)
      iTimeTop = esp_timer_get_time();
    // Start audio Top
    audioTops.StartTop( 0);
    break;
  case LMSTARTREC:
    iTimeTop = 0;
    iTimeRec = esp_timer_get_time();
    // Start record
    digitalWrite( PIN_RECORD, HIGH);
    bRecord = true;
#ifdef USE_LED
    SetLEDMode();
#endif
    break;
  case LMSTOPREC:
    if (bDebug) Serial.printf("Slave diff. Rec-Top = %d µs\n", iTimeTop-iTimeRec);
    // Stop record
    digitalWrite( PIN_RECORD, LOW);
    bRecord = false;
#ifdef USE_LED
    SetLEDMode();
#endif
    break;
  case LMSTOPMODE:
    // Send Stop Mode to PR
    serialLink.SendStopMode();
    break;
  case LMTEST:
    Serial.println("CSlaveManager::OnLoop receive LMTEST");
    break;
  }
  // Test if receive message from Teensy
  int iSerialMessage = serialLink.OnReceive();
  switch (iSerialMessage)
  {
  case LMSLAVE:
    // Send Slave OK to Lora Link
    if (!loraLink.SendSlaveOK( serialLink.GetSlaveNum(), serialLink.GetSlaveStatus()))
      iESP32Status = ES_ERLORA;
    bWaitingTeensy = false;
#ifdef USE_LED
    SetLEDMode();
#endif
    break;
  default:
    // Do nothing
    break;
  }
  CManager::OnLoop();
}
