//! \file CTopGeneration.cpp
//! \brief Definition of the CTopGeneration class for the generation of audio tops
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2019 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "esp_err.h"
#include "CTopGeneration.h"

//-------------------------------------------------------------------------
//! \class CTopGeneration
//! \brief Class for the generation of audio tops

//-------------------------------------------------------------------------
//! \brief Constructor
CTopGeneration::CTopGeneration()
{
  hTacheLecture = NULL;
  bDiverI2S = false;
  bTop = false;
  iTimeStart = 0;
}

//-------------------------------------------------------------------------
//! \brief Destructor
CTopGeneration::~CTopGeneration()
{
  StopTop();
}

#define MAXDAC  65535
#define DEMIDAC 32767

//-------------------------------------------------------------------------
//! \brief Initialisation
//! \param iFe Sample rate in enum FREQECH
//! \param iFreq Frequency on the top in Hz
//! \param iNb Samples number in enum TOPSIZE
//! \param iCore Core number used by reading (0 or 1)
//! \param bTh Indicate thread or not to generate top
//! \return Return true if OK
bool CTopGeneration::Init(
  int iFe, int iFreq, int iNb, int iCore, bool bTh
  )
{
  char sTmp[255];
  char sTmp2[100];
  StopTop();
  iTimeStart = 0;
  switch(iNb)
  {
  case TS256 : iNbSample =  256; sprintf( sTmp, "CTopGeneration::Init, Freq %dkHz,  256 Ech, ", iFreq); break; //  256 samples
  default:
  case TS512 : iNbSample =  512; sprintf( sTmp, "CTopGeneration::Init, Freq %dkHz,  512 Ech, ", iFreq); break; //  512 samples
  case TS1024: iNbSample = 1024; sprintf( sTmp, "CTopGeneration::Init, Freq %dkHz, 1024 Ech, ", iFreq); break; // 1024 samples
  case TS2048: iNbSample = 2048; sprintf( sTmp, "CTopGeneration::Init, Freq %dkHz, 2048 Ech, ", iFreq); break; // 2048 samples
  case TS4096: iNbSample = 4096; sprintf( sTmp, "CTopGeneration::Init, Freq %dkHz, 4096 Ech, ", iFreq); break; // 4096 samples
  }
  switch(iFe)
  {
  case FE192KHZ: iFech = 192000; strcat( sTmp, "Fe 192kHz"); break; // 192kHz
  case FE250KHZ: iFech = 250000; strcat( sTmp, "Fe 250kHz"); break; // 250kHz
  default:
  case FE384KHZ: iFech = 384000; strcat( sTmp, "Fe 384kHz"); break; // 384kHz
  }
  double dDur = (1 / (double)iFech) * (double)iNbSample * 1000000.0;
  iTopDur = (int)dDur;
  sprintf( sTmp2, ", Dur %dµs", iTopDur);
  strcat( sTmp, sTmp2);
  Serial.println( sTmp);
  iCoreTask    = iCore;
  // Sample generation
  unsigned long uiTime = millis();
  complexe ej2PiFoTe;
  complexe ej2PiFoNTe;
  // Preparation of carrier generation information
  float fAmplitude = DEMIDAC;
  float fPorteuse  = (float)iFreq / (float)iFech;
  ej2PiFoTe.Re  = cosf( DEUXPI * fPorteuse );
  ej2PiFoTe.Im  = sinf( DEUXPI * fPorteuse );
  ej2PiFoNTe.Re = cosf( 0.0 )  * fAmplitude ;
  ej2PiFoNTe.Im = sinf( 0.0 )  * fAmplitude ;
  // All samples calculation
  for (int i=0; i<BUFFSIZE; i++)
  {
    if (i < iNbSample)
    {
      // Memo of the current transpo
      float VarTemp  = ej2PiFoNTe.Re;
      // Calculation of the next transpo
      float VarTemp2 = ej2PiFoTe.Re * ej2PiFoNTe.Re - ej2PiFoTe.Im * ej2PiFoNTe.Im;
      ej2PiFoNTe.Im  = ej2PiFoTe.Re * ej2PiFoNTe.Im + ej2PiFoTe.Im * ej2PiFoNTe.Re;
      ej2PiFoNTe.Re  = VarTemp2;
      samplesBuffer[i] = (uint16_t)VarTemp + DEMIDAC;
    }
    else
      samplesBuffer[i] = DEMIDAC;
  }
  // Set samples size
  iSizeTop  = sizeof(uint16_t) * iNbSample;
  //Serial.println("Samples calculation OK");

  // Prepare I2S channel
  // Initialize i2s with configurations
  esp_err_t tErr = i2s_driver_install((i2s_port_t)i2s_num, &i2s_config, 0, NULL);
  if (tErr != ESP_OK)
    AffESP_Err( "Erreur i2s_driver_install=", tErr, "!!!");
  if (tErr == ESP_OK)
  {
    tErr = i2s_set_pin((i2s_port_t)i2s_num, NULL);
    if (tErr != ESP_OK)
      AffESP_Err( "Erreur i2s_set_pin=", tErr, "!!!");
  }
  if (tErr == ESP_OK)
  {
    // Enable I2S built-in DAC right channel, maps to DAC channel 1 on GPIO25
    tErr = i2s_set_dac_mode(I2S_DAC_CHANNEL_RIGHT_EN);
    if (tErr != ESP_OK)
      AffESP_Err( "Erreur i2s_set_dac_mode=", tErr, "!!!");
  }
  if (tErr == ESP_OK)
  {
    // Sample rate initialization
    tErr = i2s_set_sample_rates((i2s_port_t)i2s_num, iFech); 
    if (tErr != ESP_OK)
      AffESP_Err( "Erreur i2s_set_sample_rates=", tErr, "!!!");
  }
  if (tErr == ESP_OK)
  {
    // Set DMA buffer to 0
    tErr = i2s_zero_dma_buffer((i2s_port_t)i2s_num); 
    if (tErr != ESP_OK)
      AffESP_Err( "Erreur i2s_zero_dma_buffer=", tErr, "!!!");
  }
  if (tErr == ESP_OK)
    bDiverI2S = true;
  //Serial.println("Avant lancement tâche");

  bThread = bTh;
  if (bThread)
  {
    // Start thread
    // We start the playing thread
    BaseType_t sTask = xTaskCreatePinnedToCore( CTopGeneration::TopGenerationTask, "TopTask", 10000, NULL, 1, &hTacheLecture, iCoreTask);
    if (sTask != pdPASS)
      Serial.println("Erreur création tache TopGeneration !!!");
  }
  return true;
}

//-------------------------------------------------------------------------
//! \brief Start audio top
//! \param iDelay Delay in µs before sending top
void CTopGeneration::StartTop(
  int iDelay
  )
{
  Serial.printf("CTopGeneration::StartTop\n");
  if (bThread)
  {
    portENTER_CRITICAL_ISR( &SectionCritiqueMux);
    bTop = true;
    if (iDelay > 0)
      iTimeStart = esp_timer_get_time() + iDelay;
    else
      iTimeStart = 0;
    portEXIT_CRITICAL_ISR ( &SectionCritiqueMux);
  }
  else
  {
    int64_t iTimeTop;
    iTimeStart = esp_timer_get_time() + iDelay;
    while( esp_timer_get_time() < iTimeStart);
    TopPlaying( esp_timer_get_time() + iTopDur);
  }
}

//-------------------------------------------------------------------------
// Audio generation thread
void CTopGeneration::TopGenerationTask( void *arg)
{
  Serial.print("TopGenerationTask running on core ");
  Serial.println(xPortGetCoreID());
  esp_err_t tErr;
  bool bPlay;
  int64_t iTimeTop;
  int64_t iDuration;
  while( true)
  {
    bPlay = false;
    iTimeTop = 0;
    // Set if to be playing
    portENTER_CRITICAL_ISR( &SectionCritiqueMux);
    bPlay = bTop;
    bTop = false;
    iTimeTop = esp_timer_get_time() + iTimeStart;
    iDuration = iTopDur;
    portEXIT_CRITICAL_ISR ( &SectionCritiqueMux);
    //Test playing
    if (bPlay)
    {
      while( esp_timer_get_time() < iTimeStart);
      TopPlaying( esp_timer_get_time() + iDuration);
    }
  }
}

//-------------------------------------------------------------------------
//! \brief Audio playing
//! \param iStopTop CPU time to stop top
void CTopGeneration::TopPlaying(
  int64_t iStopTop
  )
{
  esp_err_t tErr;
  //Serial.println("TOP");
  // Start playing
  tErr = i2s_start((i2s_port_t)i2s_num);
  if (tErr != ESP_OK)
    AffESP_Err( "Erreur i2s_start=", tErr, " !!!");
  // Playing TOP samples
  size_t bytes_written;
  //Serial.printf("Play %d samples\n", iNbSample);
  tErr = i2s_write((i2s_port_t)i2s_num, (const char *)samplesBuffer, iSizeTop, &bytes_written, 100);
  if (tErr != ESP_OK)
    AffESP_Err( "Erreur i2s_write =", tErr, " !!!");
  // Wait for duration
  while ( true)
  {
    if (esp_timer_get_time() > iStopTop)
      break;
  }
  // Stop playing
  tErr = i2s_stop((i2s_port_t)i2s_num);
  if (tErr != ESP_OK)
    AffESP_Err( "Erreur i2s_stop=", tErr, " !!!");
}

//-------------------------------------------------------------------------
//! \brief Stop audio top
void CTopGeneration::StopTop()
{
  esp_err_t tErr;
  // Stop thread
  if (hTacheLecture != NULL)
    vTaskDelete( hTacheLecture);
  if (bDiverI2S)
  {
    // Stop I2S driver
    tErr = i2s_stop((i2s_port_t)i2s_num);
    if (tErr != ESP_OK)
      AffESP_Err( "Erreur i2s_stop=", tErr, " !!!");
    // Closing the I2S driver
    tErr = i2s_driver_uninstall((i2s_port_t)i2s_num); //stop & destroy i2s driver 
    if (tErr != ESP_OK)
      AffESP_Err( "Erreur i2s_driver_uninstall=", tErr, " !!!");
    bDiverI2S = false;
  }
}

//-------------------------------------------------------------------------
// Displays an eventual I2S Manager error
void CTopGeneration::AffESP_Err (char *MessA, esp_err_t code, char *MessB)
{
  if (code != ESP_OK)
  {
    Serial.print( MessA);
    switch (code)
    {
    case ESP_ERR_NO_MEM           : Serial.print(" ESP_ERR_NO_MEM ")          ; break;
    case ESP_ERR_INVALID_ARG      : Serial.print(" ESP_ERR_INVALID_ARG ")     ; break;
    case ESP_ERR_INVALID_STATE    : Serial.print(" ESP_ERR_INVALID_STATE ")   ; break;
    case ESP_ERR_INVALID_SIZE     : Serial.print(" ESP_ERR_INVALID_SIZE ")    ; break;
    case ESP_ERR_NOT_FOUND        : Serial.print(" ESP_ERR_NOT_FOUND ")       ; break;
    case ESP_ERR_NOT_SUPPORTED    : Serial.print(" ESP_ERR_NOT_SUPPORTED ")   ; break;
    case ESP_ERR_TIMEOUT          : Serial.print(" ESP_ERR_TIMEOUT ")         ; break;
    case ESP_ERR_INVALID_RESPONSE : Serial.print(" ESP_ERR_INVALID_RESPONSE "); break;
    case ESP_ERR_INVALID_CRC      : Serial.print(" ESP_ERR_INVALID_CRC ")     ; break;
    case ESP_ERR_INVALID_VERSION  : Serial.print(" ESP_ERR_INVALID_VERSION ") ; break;
    case ESP_ERR_INVALID_MAC      : Serial.print(" ESP_ERR_INVALID_MAC ")     ; break;
    default                       : Serial.print(" Erreur inconnue ");
    }
    Serial.println( MessB);
  }
}

// Thread handle
TaskHandle_t CTopGeneration::hTacheLecture;

// Thread core number
int CTopGeneration::iCoreTask;

// I2S port number
int CTopGeneration::i2s_num = 0;

// I2S configuration
i2s_config_t CTopGeneration::i2s_config = {
     .mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_TX | I2S_MODE_DAC_BUILT_IN),
     .sample_rate           = 48000,                      // Fe par défaut
     .bits_per_sample       = I2S_BITS_PER_SAMPLE_16BIT,  // the DAC module will only take the 8bits from MSB
     .channel_format        = I2S_CHANNEL_FMT_ONLY_RIGHT,
     .communication_format  = (i2s_comm_format_t)I2S_COMM_FORMAT_I2S_MSB,
     .intr_alloc_flags      = ESP_INTR_FLAG_LEVEL3,       // high interrupt priority Interrupt level 1
     //.intr_alloc_flags    = 0,                          // default
     .dma_buf_count         = 2,                          // number of buffers,
     .dma_buf_len           = 256,                        // size of each buffer (in numbers of samples)
     .use_apll              = 0
    };

// I2S configuration pins
i2s_pin_config_t CTopGeneration::pin_config = {
    .bck_io_num   = PIN_BCK_I2S,
    .ws_io_num    = PIN_LRCK_I2S,
    .data_out_num = PIN_OUT_I2S,
    .data_in_num  = PIN_IN_I2S
    };

//! Bool true if i2s drviver is install
bool CTopGeneration::bDiverI2S = false;

//! Buffer samples of the top audio
uint16_t CTopGeneration::samplesBuffer[BUFFSIZE+1];

//! Number of sample
int CTopGeneration::iNbSample;

//! Sample rate in Hz
int CTopGeneration::iFech;

//! Sample Top size in bytes
int CTopGeneration::iSizeTop = 0;

//! Top duration in µs
int64_t CTopGeneration::iTopDur = 0;

//! Time top start
int64_t CTopGeneration::iTimeStart;

//! Bool to send Top
bool CTopGeneration::bTop;

//! Critical section for multitasking iTimeStart usage
portMUX_TYPE CTopGeneration::SectionCritiqueMux = portMUX_INITIALIZER_UNLOCKED;
