//! \file CCodeLink.h
//! \brief Definition of the CCodeLink class for the serial Teensy Recorder to ESP32 dialogue and Lora message
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2019 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <TimeLib.h>
#include <sys/time.h>                   // struct timeval
#include <algorithm>
#include <string.h>
#include <stdio.h>

#ifndef CCODELINK_H
#define CCODELINK_H

//! \enum LinkMessages
//! \brief Messages type
enum LinkMessages
{
  LMPARAMS     = 0,  //!< Send parameters
  LMSLAVE      = 1,  //!< Slave OK
  LMSTARTREC   = 2,  //!< Start recording (only on LORA link)
  LMSTOPREC    = 3,  //!< Stop recording  (only on LORA link)
  LMTOP        = 4,  //!< Top synchro     (only on LORA link)
  LMSTOPMODE   = 5,  //!< Stop synchro mode
  LMSTATUS     = 6,  //!< ESP32 status to Teensy
  LMTEST       = 7,  //!< Test message
  LMRVERSION   = 8,  //!< PR request ESP32 software version
  LMSVERSION   = 9,  //!< ESP32 software version message
  LMUNKNOW     = 10, //!< Unknow message
  LMOTHERLORA  = 11  //!< For other Lora messages 
};

//! \enum ESP32Status
//! \brief ESP32 Status
enum ESP32Status
{
  ES_OK        = 0,  //!< Status OK
  ES_ERLORA    = 1,  //!< Error on LORA
  ES_ERTOP     = 2,  //<! Error on DAC Top
  ES_ERPARAM   = 3,  //<! Error decoding params message
  ES_ERSLAVE   = 4,  //<! Error decoding Slave message
  ES_ERSERMESS = 5,  //<! Error decoding num message Serial Link
  ES_ERLORMESS = 6,  //<! Error decoding num message Lora Link
  ES_TIMEOUT   = 7,  //<! Error time out
  ES_UNKNOWER  = 8   //<! Unknow error
};

// Time between to LMSTATUS message
#define TIMESTATUSESP 3000

//-------------------------------------------------------------------------
//! \class CCodeLink
//! \brief Class for coding and decoding messages from ESP32/TEensy and Radio link
class CCodeLink
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor
  CCodeLink() {bLoRa=false;};

  //-------------------------------------------------------------------------
  //! \brief Set LoRa link type
  void SetLoRaLink() {bLoRa=true;};

  //-------------------------------------------------------------------------
  //! \brief Decode message type
  //! \param pMessage Message to decode
  //! \return Return message type
  int DecodeMessageType(
    char *pMessage
    )
  {
    int iMessage = LMOTHERLORA;
    if (bLoRa)
    {
      iMessage = LMUNKNOW;
      // Vérification de l'entête des messages
      if (strstr( pMessage, "PAR") == pMessage)
        iMessage = LMPARAMS;
      else if (strstr( pMessage, "SLV") == pMessage)
        iMessage = LMSLAVE;
      else if (strstr( pMessage, "STA") == pMessage)
        iMessage = LMSTARTREC;
      else if (strstr( pMessage, "STO") == pMessage)
        iMessage = LMSTOPREC;
      else if (strstr( pMessage, "T") == pMessage)
        iMessage = LMTOP;
      else if (strstr( pMessage, "STM") == pMessage)
        iMessage = LMSTOPMODE;
      else if (strstr( pMessage, "ES") == pMessage)
        iMessage = LMSTATUS;
      else if (strstr( pMessage, "TST") == pMessage)
        iMessage = LMTEST;
      else if (strstr( pMessage, "VER") == pMessage)
        iMessage = LMRVERSION;
      else if (strstr( pMessage, "VES,") == pMessage)
        iMessage = LMSVERSION;
    }
    else
    {
      // Vérification de la fin du message
      if (pMessage[0] == '$' and pMessage[strlen(pMessage)-1] == '!')
      {
        iMessage = LMUNKNOW;
        // Vérification de l'entête des messages
        if (strstr( pMessage, "$PAR") == pMessage)
          iMessage = LMPARAMS;
        else if (strstr( pMessage, "$SLAVE") == pMessage)
          iMessage = LMSLAVE;
        else if (strstr( pMessage, "$START!") == pMessage)
          iMessage = LMSTARTREC;
        else if (strstr( pMessage, "$STOP!") == pMessage)
          iMessage = LMSTOPREC;
        else if (strstr( pMessage, "$T!") == pMessage)
          iMessage = LMTOP;
        else if (strstr( pMessage, "$STOPM!") == pMessage)
          iMessage = LMSTOPMODE;
        else if (strstr( pMessage, "$ES") == pMessage)
          iMessage = LMSTATUS;
        else if (strstr( pMessage, "$TEST!") == pMessage)
          iMessage = LMTEST;
        else if (strstr( pMessage, "$VERR!") == pMessage)
          iMessage = LMRVERSION;
        else if (strstr( pMessage, "$VERS,") == pMessage)
          iMessage = LMSVERSION;
      }
    }
    return iMessage;
  };

  /*
  $PAR for parameters from master to slave
    Send by Link from Teensy Master to ESP32 Master
    Send by Radio from ESP32 Master to ESP32 Slave
    Send by Link from ESP32 Slave to Teensy Slave
  "$PAR,SlaveNum,DateHour,BeginHour,EndHour,AbsThres,Theshold,Fe,NumGain,TimeExp,HPFFreq,FreqTop,DurTop,TopPer!"
  - SlaveNum   Number of the slave (1 to 10)
  - DateHour   Date and Hour format DD/MM/YY-HH:mm:SS
  - BeginHour  Hour to beging recording format HH:mm
  - EndHour    Hour to stop recording format HH:mm
  - AbsThres   Threshold type, false (0) relative, true (1) absolute
  - Theshold   Threshold in dB, (5 to 99 in relative or -110 to -30 in absolute)
  - Fe         Sample rate (2 for 192kHz, 3 for 250kHz, 4 for 384kHz)
  - NumGain    Numeric gain (0 for 0dB, 1 for +6dB, 2 for +12dB, 3 for +18dB, 4 for +24dB)
  - TimeExp    Time expansion, true (1) for X10, false (0) for X1
  - HPFFreq    Hight pass filter frequency in kHz (0 to 25 kHz)
  - FreqTop    Top audio frequency in kHz (1 to 50 kHz)
  - DurTop     Top audio duration in samples (256, 512 or 1024)
  - TopPer     Top audio period (0 for one at the start of the recording, 1 to 10 for one every 1 to 10 seconds)
  */
  //-------------------------------------------------------------------------
  //! \brief Code Parameters message
  //! \param pParams Parameters
  //! \return Return string of the message
  char *CodeParams(
    ParamsOperator *pParams
    )
  {
    char sTime[20];
#if defined(__MK66FX1M0__) // Teensy 3.6
    // Date Time with current hour
    if (bLoRa)
      sprintf( sTime, "%02d%02d%02d%02d%02d%02d", day(), month(), year()-2000, hour(), minute(), second());
    else
      sprintf( sTime, "%02d,%02d,%02d,%02d,%02d,%02d", day(), month(), year()-2000, hour(), minute(), second());
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
    // Copy date time
    if (bLoRa)
    {
      int x=0;
      int y=0;
      for (int i=0; i<6; i++)
      {
        sTime[y] = pParams->sDateHour[x];
        x++;
        y++;
        sTime[y] = pParams->sDateHour[x];
        x+=2;
        y++;
      }
    }
    else
      strcpy( sTime, pParams->sDateHour);
#endif
    int iThreshold = pParams->iSeuilDet;
    if (pParams->bTypeSeuil)
      iThreshold = pParams->iSeuilAbs;
    if (bLoRa)
      sprintf( sMessageEmission, "PAR%s,%s,%s,%1d,%d,%d,%d,%d,%d,%d,%d,%d", sTime, pParams->sHDebut, pParams->sHFin, pParams->bTypeSeuil, iThreshold,
        pParams->uiFe, pParams->uiGainNum, pParams->bExp10, pParams->iFreqFiltreHighPass, pParams->iTopAudioFreq, pParams->iDurTop, pParams->iTopPer);
    else
      sprintf( sMessageEmission, "$PAR,%s,%s,%s,%1d,%d,%d,%d,%d,%d,%d,%d,%d!", sTime, pParams->sHDebut, pParams->sHFin, pParams->bTypeSeuil, iThreshold,
        pParams->uiFe, pParams->uiGainNum, pParams->bExp10, pParams->iFreqFiltreHighPass, pParams->iTopAudioFreq, pParams->iDurTop, pParams->iTopPer);
    for (int i=0; i<(int)strlen(sMessageEmission); i++)
      if (sMessageEmission[i] == '/' or sMessageEmission[i] == '-' or sMessageEmission[i] == ':')
        sMessageEmission[i] = ',';
    return sMessageEmission;
  };

  //-------------------------------------------------------------------------
  //! \brief Decode Parameters message
  //! \param pParams Parameters
  //! \param pMessage Message to decode
  //! \return Return true if decoding is OK
  bool DecodeParams(
    ParamsOperator *pParams,
    char *pMessage
    )
  {
    bool bOK = true;
    int iDay, iMonth, iYear, iHour, iMinute, iSecond, iHD, iMD, iHF, iMF, iTypeTH, iThreshold, iFe, iNumGain, iTimeExp, iFilt, iFreqTop, iDurTop, iTopPer;
    bool bThreshold, bTimeExp;
    if (bLoRa)
    {
      // $PAR09,08,20,16,52,18,22:00,06:00,0,18,4,0,1,0,2,0,0
      sscanf( pMessage, "PAR%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d", &iDay, &iMonth, &iYear, &iHour, &iMinute, &iSecond,
        &iHD, &iMD, &iHF, &iMF, &iTypeTH, &iThreshold, &iFe, &iNumGain, &iTimeExp, &iFilt, &iFreqTop, &iDurTop, &iTopPer);
    }
    else
    {
      // $PAR,09,08,20,16,52,18,22:00,06:00,0,18,4,0,1,0,2,0,0!
      sscanf( pMessage, "$PAR,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d!", &iDay, &iMonth, &iYear, &iHour, &iMinute, &iSecond,
        &iHD, &iMD, &iHF, &iMF, &iTypeTH, &iThreshold, &iFe, &iNumGain, &iTimeExp, &iFilt, &iFreqTop, &iDurTop, &iTopPer);
    }
    bThreshold = (bool)iTypeTH;
    bTimeExp   = (bool)iTimeExp;
    // Vérification date heure
    if (iDay<1 or iDay>31 or iMonth<1 or iMonth>12 or iYear<20 or iYear>99 or iHour<0 or iHour>23 or iMinute<0 or iMinute>59 or iSecond<0 or iSecond>59)
    {
      sprintf( sError, "DecodeParams erreur Date/Heure D %d, M %d, A %d, H %d, m %d, S %d\n", iDay, iMonth, iYear, iHour, iMinute, iSecond);
      bOK = false;
    }
    else
    {
#if defined(__MK66FX1M0__) // Teensy 3.6
      // Update to the system date
      setTime( iHour, iMinute, iSecond, iDay, iMonth, iYear);
      // Set internal Teensy 3.6 clock
      Teensy3Clock.set(now());
#endif
#ifdef ARDUINO_ARCH_ESP32 // ESP32
      // Copy date time
      sprintf( pParams->sDateHour, "%02d,%02d,%02d,%02d,%02d,%02d", iDay, iMonth, iYear, iHour, iMinute, iSecond);
#endif
    }
    if (bOK)
    {
      // Vérification heure de début
      if (iHD<0 or iHD>23 or iMD<0 or iMD>59)
      {
        sprintf( sError, "DecodeParams erreur heure de début %d:%d\n", iHD, iMD);
        bOK = false;
      }
      else
        sprintf( pParams->sHDebut, "%02d:%02d", iHD, iMD);
    }
    if (bOK)
    {
      // Vérification heure de fin
      if (iHF<0 or iHF>23 or iMF<0 or iMF>59)
      {
        sprintf( sError, "DecodeParams erreur heure de fin %d:%d\n", iHF, iMF);
        bOK = false;
      }
      else
        sprintf( pParams->sHFin, "%02d:%02d", iHF, iMF);
    }
    if (bOK)
    {
      // Vérification du seuil
      if (bThreshold and (iThreshold < -110 or iThreshold > -30))
      {
        sprintf( sError, "DecodeParams erreur seuil absolue %d\n", iThreshold);
        bOK = false;
      }
      else if (!bThreshold and (iThreshold < 5 or iThreshold > 99))
      {
        sprintf( sError, "DecodeParams erreur seuil relatif %d\n", iThreshold);
        bOK = false;
      }
      else
      {
        pParams->bTypeSeuil = bThreshold;
        if (bThreshold)
          pParams->iSeuilAbs = iThreshold;
        else
          pParams->iSeuilDet = iThreshold;
      }
    }
    if (bOK)
    {
      // Vérification de la Fe
      if (iFe < FE192KHZ or iFe > FE384KHZ)
      {
        sprintf( sError, "DecodeParams erreur Fe %d\n", iFe);
        bOK = false;
      }
      else
        pParams->uiFe = iFe;
    }
    if (bOK)
    {
      // Vérification du gain numérique
      if (iNumGain < GAIN0dB or iNumGain >= MAXGAIN)
      {
        sprintf( sError, "DecodeParams erreur gain numérique %d\n", iNumGain);
        bOK = false;
      }
      else
        pParams->uiGainNum = iNumGain;
    }
    if (bOK)
    {
      // Vérification expansion de temps
      if (bTimeExp < 0 or bTimeExp > 1)
      {
        sprintf( sError, "DecodeParams erreur expansion de temps %d\n", bTimeExp);
        bOK = false;
      }
      else
        pParams->bExp10 = bTimeExp;
    }
    if (bOK)
    {
      // Vérification filtre passe haut
      if (iFilt < 0 or iFilt > 25)
      {
        sprintf( sError, "DecodeParams erreur filtre passe haut %d\n", iFilt);
        bOK = false;
      }
      else
        pParams->iFreqFiltreHighPass = iFilt;
    }
    if (bOK)
    {
      // Vérification de la fréquence du top
      if (iFreqTop < 1 or iFreqTop > 50)
      {
        sprintf( sError, "DecodeParams erreur fréquence du top %d\n", iFreqTop);
        bOK = false;
      }
      else
        pParams->iTopAudioFreq = iFreqTop;
    }
    if (bOK)
    {
      // Vérification de la durée du top
      if (iDurTop < TS256 or iDurTop >= TSMAX)
      {
        sprintf( sError, "DecodeParams erreur durée du top %d\n", iDurTop);
        bOK = false;
      }
      else
        pParams->iDurTop = iDurTop;
    }
    if (bOK)
    {
      // Vérification de la période du top
      if (iTopPer < 0 or iTopPer > 10)
      {
        sprintf( sError, "DecodeParams erreur période du top %d\n", iTopPer);
        bOK = false;
      }
      else
        pParams->iTopPer = iTopPer;
    }
    return bOK;
  };

  /*
  &Slave to signal the good reception of the parameters from a slave to master
    Send by Link from Teensy Slave to ESP32 Slave
    Send by Radio from ESP32 Slave to ESP32 Master
    Send by Link from ESP32 Master to Teensy Master
  "$SLAVE,OK,SlaveNum!"
  - OK         1 if OK else 0
  - SlaveNum   Number of the slave (1 to 10)
  */
  //-------------------------------------------------------------------------
  //! \brief Code Slave message
  //! \param iSlave Number of the slave (1 to 10)
  //! \param bOK    Indicate if slave is OK
  //! \return Return string of the message
  char *CodeSlave(
    int  iSlave,
    bool bOK
    )
  {
    if (bLoRa)
      sprintf( sMessageEmission, "SLV%1d,%02d", bOK, iSlave);
    else
      sprintf( sMessageEmission, "$SLAVE,%1d,%02d!", bOK, iSlave);
    return sMessageEmission;
  }

  //-------------------------------------------------------------------------
  //! \brief Decode Slave message
  //! \param piSlave Number of the slave (1 to 10)
  //! \param pbOK    Indicate if slave is OK
  //! \param pMessage Message to decode
  //! \return Return true if decoding is OK
  bool DecodeSlave(
    int  *piSlave,
    bool *pbOK,
    char *pMessage
    )
  {
    bool bOK = true;
    int  iSlave, iOK;
    if (bLoRa)
      sscanf( pMessage, "SLV%d,%d", &iOK, &iSlave);
    else
      sscanf( pMessage, "$SLAVE,%d,%d!", &iOK, &iSlave);
    // Vérification OK
    if (iOK < 0 or iOK > 1)
      bOK = false;
    else
      *pbOK = (bool)iOK;
    if (bOK)
    {
      // Vérification numéro slave
      if (iSlave <= MASTER or iSlave >= MAXMS)
        bOK = false;
      else
        *piSlave = iSlave;
    }
    return bOK;
  };

  /*
  $Start to start a record from master to all slaves
    Send by Link from Teensy Master to ESP32 Master
    Send by Radio from ESP32 Master to ESP32 Slave
    Send by Link from ESP32 Slave to Teensy Slave
  "$START!"
  */
  //-------------------------------------------------------------------------
  //! \brief Code Start recording message
  //! \return Return string of the message
  char *CodeStartRec()
  {
    if (bLoRa)
      strcpy( sMessageEmission, "STA");
    else
      strcpy( sMessageEmission, "$START!");
    return sMessageEmission;
  };

  //-------------------------------------------------------------------------
  //! \brief Decode start recording message
  //! \param pMessage Message to decode
  //! \return Return true if decoding is OK
  bool DecodeStartRec(
    char *pMessage
    )
  {
    return true;
  };

  /*
  $Stop to stop a record from master to all slaves
    Send by Link from Teensy Master to ESP32 Master
    Send by Radio from ESP32 Master to ESP32 Slave
    Send by Link from ESP32 Slave to Teensy Slave
  "$STOP!"
  */
  //-------------------------------------------------------------------------
  //! \brief Code Stop recording message
  //! \return Return string of the message
  char *CodeStopRec()
  {
    if (bLoRa)
      strcpy( sMessageEmission, "STO");
    else
      strcpy( sMessageEmission, "$STOP!");
    return sMessageEmission;
  };

  //-------------------------------------------------------------------------
  //! \brief Decode Stop recording message
  //! \param pMessage Message to decode
  //! \return Return true if decoding is OK
  bool DecodeStopRec(
    char *pMessage
    )
  {
    return true;
  };

  /*
  $Top for synch radio tops from master to all slaves
    Send by Link from Teensy Master to ESP32 Master
    Send by Radio from ESP32 Master to ESP32 Slave
    Send by Link from ESP32 Slave to Teensy Slave
  "$T!"
  */
  //-------------------------------------------------------------------------
  //! \brief Code Top synchro message
  //! \return Return string of the message
  char *CodeTopSynchro()
  {
    if (bLoRa)
      strcpy( sMessageEmission, "T");
    else
      strcpy( sMessageEmission, "$T!");
    return sMessageEmission;
  };

  //-------------------------------------------------------------------------
  //! \brief Decode Top synchro message
  //! \param pMessage Message to decode
  //! \return Return true if decoding is OK
  bool DecodeTopSynchro(
    char *pMessage
    )
  {
    return true;
  };

  //-------------------------------------------------------------------------
  //! \brief Code Stop mode message
  //! \return Return string of the message
  char *CodeStopMode()
  {
    if (bLoRa)
      strcpy( sMessageEmission, "STM");
    else
      strcpy( sMessageEmission, "$STOPM!");
    return sMessageEmission;
  };

  //-------------------------------------------------------------------------
  //! \brief Decode Stop mode message
  //! \param pMessage Message to decode
  //! \return Return true if decoding is OK
  bool DecodeStopMode(
    char *pMessage
    )
  {
    return true;
  };

  /*
  &ESP32 status to Teensy
    Send by Link from ESP32 to Teensy
  "$ES,0!"
  - 0         ESP32 statut
  */
  //-------------------------------------------------------------------------
  //! \brief Code ESP32 status message
  //! \param iStatus ESP32 status
  //! \return Return string of the message
  char *CodeESP32Status(
    int  iStatus
    )
  {
    if (bLoRa)
      sprintf( sMessageEmission, "ES%d", iStatus);
    else
      sprintf( sMessageEmission, "$ES,%d!", iStatus);
    return sMessageEmission;
  }

  //-------------------------------------------------------------------------
  //! \brief Decode ESP32 status message
  //! \param pMessage Message to decode
  //! \return Return ESP32 status
  int DecodeESP32Status(
    char *pMessage
    )
  {
    int  iStatus = ES_OK;
    if (bLoRa)
      sscanf( pMessage, "ES%d", &iStatus);
    else
      sscanf( pMessage, "$ES,%d!", &iStatus);
    return iStatus;
  };

  //-------------------------------------------------------------------------
  //! \brief Code Test message
  //! \return Return string of the message
  char *CodeTest()
  {
    if (bLoRa)
      strcpy( sMessageEmission, "TST");
    else
      strcpy( sMessageEmission, "$TEST!");
    return sMessageEmission;
  };

  //-------------------------------------------------------------------------
  //! \brief Decode Test message
  //! \param pMessage Message to decode
  //! \return Return true if decoding is OK
  bool DecodeTest(
    char *pMessage
    )
  {
    return true;
  };

  //-------------------------------------------------------------------------
  //! \brief Code ESP32 request version message
  //! \return Return string of the message
  char *CodeVersionRequest()
  {
    if (bLoRa)
      strcpy( sMessageEmission, "VER");
    else
      strcpy( sMessageEmission, "$VERR!");
    return sMessageEmission;
  };

  //-------------------------------------------------------------------------
  //! \brief Decode request version message
  //! \return Return true if decoding is OK
  bool DecodeVersionRequest(
    char *pMessage
    )
  {
    return true;
  };

  //-------------------------------------------------------------------------
  //! \brief Code ESP32 version message
  //! \param pVersion ESP32 Software version
  //! \return Return string of the message
  char *CodeVersion(
    char *pVersion
    )
  {
    if (bLoRa)
      sprintf( sMessageEmission, "VES%s", pVersion);
    else
      sprintf( sMessageEmission, "$VERS,%s!", pVersion);
    return sMessageEmission;
  };

  //-------------------------------------------------------------------------
  //! \brief Decode version message
  //! \return Return version string
  char *DecodeVersion(
    char *pMessage
    )
  {
    char *p;
    if (bLoRa)
      p = &(pMessage[3]);
    else
    {
      pMessage[strlen(pMessage)-1] = 0;
      p = &(pMessage[6]);
    }
    return p;
  };

  //-------------------------------------------------------------------------
  //! \brief Return error message
  //! \return Return error string
  char *GetErrorString() {return sError;};

protected:
  //! Emission buffer
  char sMessageEmission[127];

  //! Message d'erreur
  char sError[128];

  //! Mode LORA (messages courts)
  bool bLoRa;
};

#endif
