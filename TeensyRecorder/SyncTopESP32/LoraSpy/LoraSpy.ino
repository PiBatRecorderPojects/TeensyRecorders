#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include "CLoraLink.h"
#include "U8g2lib.h"

#define PIN_RECORD      33  //!< Record when 1
#define PIN_LED         32  //!< LED pin

// Screen manager
U8G2_SSD1306_128X64_NONAME_F_HW_I2C Display(U8G2_R0, /* reset=*/ U8X8_PIN_NONE); // <<<<<<<<<< Fonctionne avec TTGO LORA OLED

CLoRaLink loraLink;

const char *sOKHS[2]  = {"HS", "OK"};
char sMessages[7][255] = {"LORA SPY 0 mes.", "01-", "02-", "03-", "04-", "05-", "06-"};;
int iNbMess = 0;
unsigned long iTimeAff = 0;
unsigned long iTimePinRec = 0;
bool bPinRec = false;

void Affichage()
{
  int y = 0;
  Display.clearBuffer();
  Display.setCursor( 0, 0);
  for (int i=0; i<7; i++, y+=9)
  {
    Display.setCursor( 0, y);
    Display.print( sMessages[i]);
  }
  Display.sendBuffer();
}

void setup()
{
  // Initializing the console output
  Serial.begin(115200);
  Serial.println("========================================");
  Serial.println("=============== LORA SPY ===============");
  // Init de l'afficheur
  Display.begin();
  Display.setFontPosTop();
  Display.setFontMode(0);
  Display.enableUTF8Print();    // enable UTF8 support for the Arduino print() function
  Display.setFont(u8g2_font_6x10_mf);

  sprintf( sMessages[0], "Lora Spy %d mes.", iNbMess);
  for (int i=1; i<7; i++)
    sprintf( sMessages[i], " ", i);

  // Init de la liaison LORA
  if (!loraLink.Init())
    Serial.print("Erreur Lora !");
  iTimeAff = esp_timer_get_time() + 100000;
  gpio_set_direction((gpio_num_t)PIN_LED       , GPIO_MODE_OUTPUT);
  digitalWrite( PIN_LED, LOW);
  //gpio_set_direction((gpio_num_t)PIN_RECORD, GPIO_MODE_OUTPUT_OD);
  gpio_set_direction((gpio_num_t)PIN_RECORD, GPIO_MODE_OUTPUT);
  digitalWrite( PIN_RECORD, LOW);
  iTimePinRec = esp_timer_get_time() + 1000000;
}

void loop()
{
  // Test si réception d'un message LORA
  if (loraLink.OnReceive() > -1)
  {
    iNbMess++;
    // Mémo du message
    char *pMessage = loraLink.GetMessage();
    char sMessage[32];
    if (strlen(pMessage) > 20)
      strncpy( sMessage, pMessage, 20);
    else
      strcpy( sMessage, pMessage);
    sMessage[20] = 0;
    Serial.printf("Message Lora [%s]\n", sMessage);
    sprintf(sMessages[0], "%d messages %ddB", iNbMess, loraLink.GetiRSSi());
    if (iNbMess <= 6)
      strcpy( sMessages[iNbMess], sMessage);
    else
    {
      for (int i=1; i<6; i++)
        strcpy( sMessages[i], sMessages[i+1]);
      strcpy( sMessages[6], sMessage);
    }
  }
  // Affichage
  if (esp_timer_get_time() > iTimeAff)
  {
    // 123456789012345678901
    // Lora Spy OK 142 mes.
    //sprintf( sMessages[0], "Lora Spy %d mes.", iNbMess);
    //sprintf( sMessages[0], "Lora Spy %d mes.", iNbMess);
    //Serial.println("Affichage");
    Affichage();
    //Serial.println("Affichage OK");
    iTimeAff = esp_timer_get_time() + 100000;
  }
  /*if (esp_timer_get_time() > iTimePinRec)
  {
    iTimePinRec = esp_timer_get_time() + 1000000;
    if (bPinRec)
    {
      digitalWrite( PIN_RECORD, LOW);
      digitalWrite( PIN_LED, LOW);
      bPinRec = false;
      Serial.println("PIN_RECORD LOW");
    }
    else
    {
      digitalWrite( PIN_RECORD, HIGH);
      digitalWrite( PIN_LED, HIGH);
      bPinRec = true;
      Serial.println("PIN_RECORD HIGH");
    }
  }*/
}
