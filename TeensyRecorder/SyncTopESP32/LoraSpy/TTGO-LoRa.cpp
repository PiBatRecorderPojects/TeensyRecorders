#include "TTGO-LoRa.h"

// registers
#define REG_FIFO                 0x00
#define REG_OP_MODE              0x01
#define REG_FRF_MSB              0x06
#define REG_FRF_MID              0x07
#define REG_FRF_LSB              0x08
#define REG_PA_CONFIG            0x09
#define REG_LNA                  0x0c
#define REG_FIFO_ADDR_PTR        0x0d
#define REG_FIFO_TX_BASE_ADDR    0x0e
#define REG_FIFO_RX_BASE_ADDR    0x0f
#define REG_FIFO_RX_CURRENT_ADDR 0x10
#define REG_IRQ_FLAGS            0x12
#define REG_RX_NB_BYTES          0x13
#define REG_PKT_RSSI_VALUE       0x1a
#define REG_PKT_SNR_VALUE        0x1b
#define REG_MODEM_CONFIG_1       0x1d
#define REG_MODEM_CONFIG_2       0x1e
#define REG_PREAMBLE_MSB         0x20
#define REG_PREAMBLE_LSB         0x21
#define REG_PAYLOAD_LENGTH       0x22
#define REG_MODEM_CONFIG_3       0x26
#define REG_RSSI_WIDEBAND        0x2c
#define REG_DETECTION_OPTIMIZE   0x31
#define REG_DETECTION_THRESHOLD  0x37
#define REG_SYNC_WORD            0x39
#define REG_DIO_MAPPING_1        0x40
#define REG_VERSION              0x42

// modes
#define MODE_LONG_RANGE_MODE     0x80
#define MODE_SLEEP               0x00
#define MODE_STDBY               0x01
#define MODE_TX                  0x03
#define MODE_RX_CONTINUOUS       0x05
#define MODE_RX_SINGLE           0x06

// PA config
#define PA_BOOST                 0x80
#define RFO                      0x70
// IRQ masks
#define IRQ_TX_DONE_MASK           0x08
#define IRQ_PAYLOAD_CRC_ERROR_MASK 0x20
#define IRQ_TIMEOUT                0x80
#define IRQ_RX_DONE_MASK           0x40

#define MAX_PKT_LENGTH           255

LoRaClass::LoRaClass() :
  _spiSettings(8E6, MSBFIRST, SPI_MODE0),
  _ss(LORA_DEFAULT_SS_PIN), _reset(LORA_DEFAULT_RESET_PIN), _dio0(LORA_DEFAULT_DIO0_PIN),
  _frequency(0),
  _packetIndex(0),
  _implicitHeaderMode(0),
  _onReceive(NULL),
  _iPower(12),
  _iSFactor(7)
{
  // overide Stream timeout value
  setTimeout(0);
  _bCRCErr = false;
  _bTimeoutErr = false;
}

bool LoRaClass::isCRCError()
{
  bool bReturn = _bCRCErr;
  _bCRCErr = false;
  return bReturn;
}

bool LoRaClass::isTimeoutError()
{
  bool bReturn = _bTimeoutErr;
  _bTimeoutErr = false;
  return bReturn;
}
  

int LoRaClass::begin(long frequency, bool PABOOST)
{
  //Serial.printf("LoRaClass::begin( frequency %d, PABOOST %d)\n", frequency, PABOOST);
  // setup pins
  pinMode(_ss, OUTPUT);
  pinMode(_reset, OUTPUT);
  pinMode(_dio0, INPUT);
  // perform reset
  digitalWrite(_reset, LOW);
  delay(20);
  digitalWrite(_reset, HIGH);
  delay(50);
  // set SS high
  digitalWrite(_ss, HIGH);
  // start SPI
  SPI.begin();
  // check version
  uint8_t version = readRegister(REG_VERSION);
  if (version != 0x12) { 
    return 0; 
  }
  // put in sleep mode
  sleep();
  // set frequency
  setFrequency(frequency);
  //long lF = getFrequency();
  //Serial.printf("Après setFrequency %d\n", lF);
  // set base addresses
  writeRegister(REG_FIFO_TX_BASE_ADDR, 0);
  writeRegister(REG_FIFO_RX_BASE_ADDR, 0);
  // set LNA boost
  writeRegister(REG_LNA, readRegister(REG_LNA) | 0x03);
  // set auto AGC
  writeRegister(REG_MODEM_CONFIG_3, 0x04);
  // set output power to 12 dBm
  setTxPower(_iPower, PABOOST);  //rfo
  // set Spreading Factor to 7 (6~12)
  setSpreadingFactor(_iSFactor);
  // put in standby mode
  idle();
  return 1;
}

void LoRaClass::end()
{
  // put in sleep mode
  sleep();
  // stop SPI
  SPI.end();
}

int LoRaClass::beginPacket(int implicitHeader)
{
  // put in standby mode
  idle();
  if (implicitHeader) {
    implicitHeaderMode();
  } else {
    explicitHeaderMode();
  }
  // reset FIFO address and paload length
  writeRegister(REG_FIFO_ADDR_PTR, 0);
  writeRegister(REG_PAYLOAD_LENGTH, 0);
  return 1;
}

int LoRaClass::endPacket()
{
  // put in TX mode
  writeRegister(REG_OP_MODE, MODE_LONG_RANGE_MODE | MODE_TX);
  // wait for TX done
  while((readRegister(REG_IRQ_FLAGS) & IRQ_TX_DONE_MASK) == 0);
  // clear IRQ's
  writeRegister(REG_IRQ_FLAGS, IRQ_TX_DONE_MASK);
  return 1;
}

int LoRaClass::parsePacket(int size)
{
  int packetLength = 0;
  int irqFlags = readRegister(REG_IRQ_FLAGS);
  if (irqFlags & IRQ_PAYLOAD_CRC_ERROR_MASK != 0)
    _bCRCErr = true;
  if (irqFlags & IRQ_TIMEOUT != 0)
    _bTimeoutErr = true;

  if (size > 0) {
    implicitHeaderMode();
    writeRegister(REG_PAYLOAD_LENGTH, size & 0xff);
  } else {
    explicitHeaderMode();
  }

  // clear IRQ's
  writeRegister(REG_IRQ_FLAGS, irqFlags);

  if ((irqFlags & IRQ_RX_DONE_MASK) && (irqFlags & IRQ_PAYLOAD_CRC_ERROR_MASK) == 0) {
    // received a packet
    _packetIndex = 0;
    // read packet length
    if (_implicitHeaderMode) {
      packetLength = readRegister(REG_PAYLOAD_LENGTH);
    } else {
      packetLength = readRegister(REG_RX_NB_BYTES);
    }
    // set FIFO address to current RX address
    writeRegister(REG_FIFO_ADDR_PTR, readRegister(REG_FIFO_RX_CURRENT_ADDR));
    // put in standby mode
    idle();
  }
  else if (readRegister(REG_OP_MODE) != (MODE_LONG_RANGE_MODE | MODE_RX_SINGLE)) {
    // not currently in RX mode
    // reset FIFO address
    writeRegister(REG_FIFO_ADDR_PTR, 0);
    // put in single RX mode
    writeRegister(REG_OP_MODE, MODE_LONG_RANGE_MODE | MODE_RX_SINGLE);
  }
  return packetLength;
}

int LoRaClass::packetRssi()
{
  int result = 0;
  int snr = readRegister(REG_PKT_SNR_VALUE);
  // Packet Strength (dBm) = -157 + PacketRssi + PacketSnr * 0.25 (when using the HF port and SNR < 0)
  // or
  // Packet Strength (dBm) = -164 + PacketRssi + PacketSnr * 0.25 (when using the LF port and SNR < 0)
  if (_frequency < 860E6)
  {
    if (snr < 0)
      result = -164 + readRegister(REG_PKT_RSSI_VALUE) + snr * 0.25;
    else
      result = -164 + readRegister(REG_PKT_RSSI_VALUE);
  }
  else
  {
    if (snr < 0)
      result = -157 + readRegister(REG_PKT_RSSI_VALUE) + snr * 0.25;
    else
      result = -157 + readRegister(REG_PKT_RSSI_VALUE);
  }
  return result;
}

float LoRaClass::packetSnr()
{
  return ((int8_t)readRegister(REG_PKT_SNR_VALUE)) * 0.25;
}

size_t LoRaClass::write(uint8_t byte)
{
  return write(&byte, sizeof(byte));
}

size_t LoRaClass::write(const uint8_t *buffer, size_t size)
{
  int currentLength = readRegister(REG_PAYLOAD_LENGTH);
  // check size
  if ((currentLength + size) > MAX_PKT_LENGTH) {
    size = MAX_PKT_LENGTH - currentLength;
  }
  // write data
  for (size_t i = 0; i < size; i++) {
    writeRegister(REG_FIFO, buffer[i]);
  }
  // update length
  writeRegister(REG_PAYLOAD_LENGTH, currentLength + size);
  return size;
}

int LoRaClass::available()
{
  return (readRegister(REG_RX_NB_BYTES) - _packetIndex);
}

int LoRaClass::read()
{
  if (!available()) {
    return -1; 
  }
  _packetIndex++;
  return readRegister(REG_FIFO);
}

int LoRaClass::peek()
{
  if (!available()) {
    return -1; 
  }
  // store current FIFO address
  int currentAddress = readRegister(REG_FIFO_ADDR_PTR);
  // read
  uint8_t b = readRegister(REG_FIFO);
  // restore FIFO address
  writeRegister(REG_FIFO_ADDR_PTR, currentAddress);
  return b;
}

void LoRaClass::flush()
{
}

void LoRaClass::onReceive(void(*callback)(int))
{
  _onReceive = callback;

  if (callback) {
    writeRegister(REG_DIO_MAPPING_1, 0x00);
    attachInterrupt(digitalPinToInterrupt(_dio0), LoRaClass::onDio0Rise, RISING);
//    attachInterrupt(digitalPinToInterrupt(_dio0), LoRaClass::onDio0Rise, RISING);
  } else {
    detachInterrupt(digitalPinToInterrupt(_dio0));
  }
}

void LoRaClass::receive(int size)
{
  if (size > 0) {
    implicitHeaderMode();
    writeRegister(REG_PAYLOAD_LENGTH, size & 0xff);
  } else {
    explicitHeaderMode();
  }

  writeRegister(REG_OP_MODE, MODE_LONG_RANGE_MODE | MODE_RX_CONTINUOUS);
}

void LoRaClass::idle()
{
  writeRegister(REG_OP_MODE, MODE_LONG_RANGE_MODE | MODE_STDBY);
}

void LoRaClass::sleep()
{
  writeRegister(REG_OP_MODE, MODE_LONG_RANGE_MODE | MODE_SLEEP);
}

void LoRaClass::setTxPower(int level, int outputPin)
{
  if (PA_OUTPUT_RFO_PIN == outputPin)
  {
    // RFO
    if (level < 0)
      level = 0;
    else if (level > 14)
      level = 14;
    writeRegister(REG_PA_CONFIG, RFO | (level + 1));
    //spiWrite(RH_RF95_REG_09_PA_CONFIG, RH_RF95_MAX_POWER | (power + 1));
    //writeRegister(REG_PA_CONFIG, RFO | level);
  }
  else
  {
    // PA BOOST
    if (level < 2)
      level = 2;
    else if (level > 17)
      level = 17;
    writeRegister(REG_PA_CONFIG, PA_BOOST | (level - 2));
    //writeRegister(REG_PA_CONFIG, PA_BOOST | (level - 2));
  }
  _iPower = level;
}

int LoRaClass::getPABoost()
{
  uint8_t reg = readRegister(REG_PA_CONFIG);
  bool paboost = false;
  if (reg & 0x80 == 0x80)
    paboost = true;
  return paboost;
}

int LoRaClass::getTxPower()
{
  uint8_t reg = readRegister(REG_PA_CONFIG);
  bool paboost = false;
  if (reg & 0x80 == 0x80)
    paboost = true;
  int result = 0;
  if (paboost)
    result = (reg & 0x0f) + 2;
  else
    result = (reg & 0x0f) - 1;
  return result;
}

void LoRaClass::setFrequency(long frequency)
{
  _frequency = frequency;
  uint64_t frf = ((uint64_t)frequency << 19) / 32000000;
  //Serial.printf("setFrequency( %d) frf %d\n", _frequency, frf);
  writeRegister(REG_FRF_MSB, (uint8_t)(frf >> 16));
  writeRegister(REG_FRF_MID, (uint8_t)(frf >> 8));
  writeRegister(REG_FRF_LSB, (uint8_t)(frf >> 0));
}

long LoRaClass::getFrequency()
{
  long regMSB = readRegister(REG_FRF_MSB);
  long regMID = readRegister(REG_FRF_MID);
  long regLSB = readRegister(REG_FRF_LSB);
  uint64_t frf = (regMSB << 16) + (regMID << 8) + regLSB;
  frf = (frf * 32000000) >> 19;
  long result = (long)frf;
  return result;
}

void LoRaClass::setSpreadingFactor(int sf)
{
  if (sf < 6) {
    sf = 6; 
  }
  else if (sf > 12) {
    sf = 12; 
    }
  if (sf == 6) {
    writeRegister(REG_DETECTION_OPTIMIZE, 0xc5);
    writeRegister(REG_DETECTION_THRESHOLD, 0x0c);
  } else {
    writeRegister(REG_DETECTION_OPTIMIZE, 0xc3);
    writeRegister(REG_DETECTION_THRESHOLD, 0x0a);
  }
  writeRegister(REG_MODEM_CONFIG_2, (readRegister(REG_MODEM_CONFIG_2) & 0x0f) | ((sf << 4) & 0xf0));
  _iSFactor = sf;
}

int LoRaClass::getSpreadingFactor()
{
  int result = 0;
  if (readRegister(REG_DETECTION_OPTIMIZE) == 0xc5 and readRegister(REG_DETECTION_THRESHOLD) == 0x0c)
    result = 6;
  else
  {
    uint8_t reg = readRegister(REG_MODEM_CONFIG_2);
    result = (reg & 0xf0) >> 4;
  }
  return result;
}

void LoRaClass::setSignalBandwidth(long sbw)
{
  int bw;

  if      (sbw <=   7800) { bw = 0; }
  else if (sbw <=  10400) { bw = 1; }
  else if (sbw <=  15600) { bw = 2; }
  else if (sbw <=  20800) { bw = 3; }
  else if (sbw <=  31250) { bw = 4; }
  else if (sbw <=  41700) { bw = 5; }
  else if (sbw <=  62500) { bw = 6; }
  else if (sbw <= 125000) { bw = 7; }
  else if (sbw <= 250000) { bw = 8; }
  else                    { bw = 9; }
  //Serial.printf("setSignalBandwidth sbw %d, bw %d\n", sbw, bw);
  writeRegister(REG_MODEM_CONFIG_1,(readRegister(REG_MODEM_CONFIG_1) & 0x0f) | (bw << 4));
}

long LoRaClass::getSignalBandwidth()
{
  int8_t reg = readRegister(REG_MODEM_CONFIG_1);
  reg = (reg & 0xf0) >> 4;
  long result = 0;
  switch (reg)
  {
  case 0: result =   7800; break;
  case 1: result =  10400; break;
  case 2: result =  15600; break;
  case 3: result =  20800; break;
  case 4: result =  31250; break;
  case 5: result =  41700; break;
  case 6: result =  62500; break;
  case 7: result = 125000; break;
  case 8: result = 250000; break;
  case 9: result = 500000; break;
  }
  return result;
}

void LoRaClass::setCodingRate4(int denominator)
{
  if (denominator < 5) {
    denominator = 5;
  } else if (denominator > 8) {
    denominator = 8;
  }
  int cr = denominator - 4;
  //Serial.printf("setCodingRate4 denominator %d, cr %d\n", denominator, cr);
  writeRegister(REG_MODEM_CONFIG_1, (readRegister(REG_MODEM_CONFIG_1) & 0xf1) | (cr << 1));
}

int LoRaClass::getCodingRate4()
{
  uint8_t reg = readRegister(REG_MODEM_CONFIG_1);
  int result = ((reg & 0x0e) >> 1) + 4;
  return result;
}

void LoRaClass::setPreambleLength(long length)
{
  writeRegister(REG_PREAMBLE_MSB, (uint8_t)(length >> 8));
  writeRegister(REG_PREAMBLE_LSB, (uint8_t)(length >> 0));
}

uint16_t LoRaClass::getPreambleLength()
{
  uint16_t result = (readRegister(REG_PREAMBLE_MSB) << 8) + readRegister(REG_PREAMBLE_LSB);
  return result;
}

void LoRaClass::setSyncWord(int sw)
{
  writeRegister(REG_SYNC_WORD, sw);
}

uint8_t LoRaClass::getSyncWord()
{
  return readRegister(REG_SYNC_WORD);
}

void LoRaClass::enableCrc()
{
  writeRegister(REG_MODEM_CONFIG_2, readRegister(REG_MODEM_CONFIG_2) | 0x04);
}

void LoRaClass::disableCrc()
{
  writeRegister(REG_MODEM_CONFIG_2, readRegister(REG_MODEM_CONFIG_2) & 0xfb);
}

bool LoRaClass::isCrc()
{
  uint8_t iReg = readRegister(REG_MODEM_CONFIG_2);
  bool bResult = false;
  if (iReg & 0x04 != 0)
    bResult = true;
}

byte LoRaClass::random()
{
  return readRegister(REG_RSSI_WIDEBAND);
}

void LoRaClass::setPins(int ss, int reset, int dio0)
{
  _ss = ss;
  _reset = reset;
  _dio0 = dio0;
}

void LoRaClass::setSPIFrequency(uint32_t frequency)
{
  _spiSettings = SPISettings(frequency, MSBFIRST, SPI_MODE0);
}

void LoRaClass::dumpRegisters(Stream& out)
{
  for (int i = 0; i < 128; i++) {
    out.print("0x");
    out.print(i, HEX);
    out.print(": 0x");
    out.println(readRegister(i), HEX);
  }
}

void LoRaClass::explicitHeaderMode()
{
  _implicitHeaderMode = 0;
  writeRegister(REG_MODEM_CONFIG_1, readRegister(REG_MODEM_CONFIG_1) & 0xfe);
}

void LoRaClass::implicitHeaderMode()
{
  _implicitHeaderMode = 1;
  writeRegister(REG_MODEM_CONFIG_1, readRegister(REG_MODEM_CONFIG_1) | 0x01);
}

void LoRaClass::handleDio0Rise()
{
  int irqFlags = readRegister(REG_IRQ_FLAGS);
  // clear IRQ's
  writeRegister(REG_IRQ_FLAGS, irqFlags);
  if ((irqFlags & IRQ_PAYLOAD_CRC_ERROR_MASK) == 0) {
    // received a packet
    _packetIndex = 0;
    // read packet length
    int packetLength = _implicitHeaderMode ? readRegister(REG_PAYLOAD_LENGTH) : readRegister(REG_RX_NB_BYTES);
    // set FIFO address to current RX address
    writeRegister(REG_FIFO_ADDR_PTR, readRegister(REG_FIFO_RX_CURRENT_ADDR));
    if (_onReceive) { _onReceive(packetLength); }
    // reset FIFO address
    writeRegister(REG_FIFO_ADDR_PTR, 0);
  }
}

uint8_t LoRaClass::readRegister(uint8_t address)
{
  return singleTransfer(address & 0x7f, 0x00);
}

void LoRaClass::writeRegister(uint8_t address, uint8_t value)
{
  singleTransfer(address | 0x80, value);
}

uint8_t LoRaClass::singleTransfer(uint8_t address, uint8_t value)
{
  uint8_t response;
  digitalWrite(_ss, LOW);
  SPI.beginTransaction(_spiSettings);
  SPI.transfer(address);
  response = SPI.transfer(value);
  SPI.endTransaction();
  digitalWrite(_ss, HIGH);
  return response;
}

void LoRaClass::onDio0Rise()
{
  LoRa.handleDio0Rise();
}

LoRaClass LoRa;
