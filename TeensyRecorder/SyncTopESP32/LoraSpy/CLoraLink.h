//! \file CLoraLink.h
//! \brief Definition of the CLoRaLink class for the radio dialogue
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2019 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "Const.h"
#include "CCodeLink.h"

#ifndef CLORALINK_H
#define CLORALINK_H
//! \struct ParamsOperator
//! \brief Paramètres opérateur d'enregistrement

//-------------------------------------------------------------------------
//! \class CLoRaLink
//! \brief Class for the radio dialogue between master and slaves
//! Messages exchanged:
//! - $Params for parameters from master to slave
//! - &SlaveOK to signal the good reception of the parameters from a slave to master
//! - $Record to start a record from master to all slaves
//! - $Top for synch radio tops from master to all slaves
//! - $Stop to stop a record from master to all slaves
class CLoRaLink
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor
  CLoRaLink();

  //-------------------------------------------------------------------------
  //! \brief Initialization of the connection
  //! \return Return true if OK
  bool Init();

  //-------------------------------------------------------------------------
  //! \brief Transmitting parameters to a slave
  //! \param pParams Recording Parameters
  //! \return Return true if OK
  bool SendParams(
    ParamsOperator *pParams
    );

  //-------------------------------------------------------------------------
  //! \brief Transmitting start record from master to all slaves
  //! \return Return true if OK
  bool SendStart();

  //-------------------------------------------------------------------------
  //! \brief Transmitting sync top from master to all slaves
  //! \return Return true if OK
  bool SendTop();

  //-------------------------------------------------------------------------
  //! \brief Transmitting stop record from master to all slaves
  //! \return Return true if OK
  bool SendStop();

  //-------------------------------------------------------------------------
  //! \brief Transmitting slave OK
  //! \param iSlaveNumber Number of the slave
  //! \param bOK Status of slave
  //! \return Return true if OK
  bool SendSlaveOK(
    int iSlaveNumber, bool bOK
    );

  //-------------------------------------------------------------------------
  //! \brief Transmitting stop mode from master to all slaves
  //! \return Return true if OK
  bool SendStopMode();

  //-------------------------------------------------------------------------
  //! \brief Transmitting Test message to Teensy
  //! \return Return true if OK
  bool SendTest();

  //-------------------------------------------------------------------------
  //! \brief Processing message receptions
  //! \return Returns the number of the received message
  int OnReceive();

  //-------------------------------------------------------------------------
  //! \brief Return slave number received by LMSLAVE message
  //! \return Returns slave number
  int GetSlaveNumber() {return iSlaveNumber;};

  //-------------------------------------------------------------------------
  //! \brief Return the received slave status
  //! \return Slave status
  bool GetSlaveStatus() {return bSlaveStatus;};

  //-------------------------------------------------------------------------
  //! \brief Return parameters received by parameters message
  //! \return ParamsOperator pointer
  ParamsOperator *GetParameters() {return pParams;};

  //-------------------------------------------------------------------------
  //! \brief Set debug mode
  //! \param bDbg true for debug mode
  void SetDebug( bool bDbg) {bDebug = bDbg;};

  //-------------------------------------------------------------------------
  //! \brief Set Parameter
  //! \param *params Pointer on parameters
  void SetpParams( ParamsOperator *params) {pParams = params;};

  //-------------------------------------------------------------------------
  //! \brief Return error or not (ESP32Status)
  int GetError() {return iErreur;};

  //-------------------------------------------------------------------------
  //! \brief Return reception message
  char *GetMessage() {return sMessageReception;};

  //-------------------------------------------------------------------------
  //! \brief Return level reception of the last message
  int GetiRSSi() {return iRSSI;};

protected:
  //-------------------------------------------------------------------------
  //! \brief Emission of the prepared message
  //! \param pMessage string to send
  //! \return Return true if OK
  bool SendMessage(
    char *pMessage
    );

  //! Indicates whether the LoRa handler is OK
  bool bLora;

  //! Debug mode
  bool bDebug;

  //! Slave number on received SlaveOK message
  int iSlaveNumber;

  //! Slave status on received SlaveOK message
  bool bSlaveStatus;

  //! Parameters received by parameters message
  ParamsOperator *pParams;
  
  //! Reception buffer
  char sMessageReception[255];

  //! Gestionnaire des messages
  CCodeLink link;

  //! Eventuelle erreur (ESP32Status)
  int iErreur;

  //! Niveau de réception du dernier message
  int iRSSI;
};

#endif
