//! \file CLoraLink.cpp
//! \brief Definition of the CLoRaLink class for the radio dialogue
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2019 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <SPI.h>
#include "TTGO-LoRa.h"
#include "Const.h"
#include "CLoraLink.h"

//-------------------------------------------------------------------------
//! \class CLoRaLink
//! \brief Class for the radio dialogue between master and slaves
//! Messages exchanged:
//! - $Params for parameters from master to slave
//! - $SlaveOK to signal the good reception of the parameters from a slve to master
//! - $Record to start a record from master to all slaves
//! - $Top for synch radio tops from master to all slaves
//! - $Stop to stop a record from master to all slaves

//-------------------------------------------------------------------------
// Constructor
CLoRaLink::CLoRaLink()
{
  bDebug = false;
  bDebug = true;
  bLora  = false;
  iErreur = ES_OK;
  sMessageReception[0] = 0;
  link.SetLoRaLink();
}

//-------------------------------------------------------------------------
//! \brief Initialization of the connection
//! \return Return true if OK
bool CLoRaLink::Init()
{
  // Initialisation du broches LORA
  SPI.begin( LORA_SCK, LORA_MISO, LORA_MOSI, LORA_SS);
  LoRa.setPins( LORA_SS, LORA_RST, LORA_DI0);
  // A réaliser avant begin
  LoRa.setTxPower(TXPOWER);
  LoRa.setSpreadingFactor(SPREADINGFACTOR);
  // Démarrage du service LORA (Canaux LORA France EU863-870)
  if (!LoRa.begin( LORACNX, PABOOST))
  {
    Serial.println("Erreur initialisation du service Lora !");
    bLora = false;
  }
  else
  {
    bLora = true;
    LoRa.setSignalBandwidth(BANDWIDTH);
    LoRa.setCodingRate4(CODINGRATE);
    LoRa.setPreambleLength(PREAMBLE_LENGTH);
    LoRa.setSyncWord(SYNC_WORD);
    if (LORACRC)
      LoRa.enableCrc();
    else
      LoRa.disableCrc();
    sMessageReception[0] = 0;
    LoRa.receive();
    if (bDebug)
      Serial.println("CLoRaLink::Init OK");
  }
  return bLora;
}

//-------------------------------------------------------------------------
//! \brief Transmitting parameters to a slave
//! \param pParams Recording Parameters
//! \return Return true if OK
bool CLoRaLink::SendParams(
  ParamsOperator *pParams
  )
{
  if (bLora)
  {
    char *pMessage = link.CodeParams(pParams);
    bLora = SendMessage(pMessage);
    if (bDebug)
      Serial.printf("CLoRaLink::SendParams [%s]\n", pMessage);
  }
  else
    Serial.println("CLoRaLink::SendParams bLoRa = false !");
  return bLora;
}

//-------------------------------------------------------------------------
//! \brief Transmitting start record from master to all slaves
//! \return Return true if OK
bool  CLoRaLink::SendStart()
{
  if (bLora)
  {
    char *pMessage = link.CodeStartRec();
    bLora = SendMessage(pMessage);
    if (bDebug)
      Serial.printf("CLoRaLink::SendStart [%s]\n", pMessage);
  }
  return bLora;
}

//-------------------------------------------------------------------------
//! \brief Transmitting sync top from master to all slaves
//! \return Return true if OK
bool  CLoRaLink::SendTop()
{
  if (bLora)
  {
    char *pMessage = link.CodeTopSynchro();
    bLora = SendMessage(pMessage);
    if (bDebug)
      Serial.printf("CLoRaLink::SendTop [%s]\n", pMessage);
  }
  return bLora;
}

//-------------------------------------------------------------------------
//! \brief Transmitting stop record from master to all slaves
//! \return Return true if OK
bool  CLoRaLink::SendStop()
{
  if (bLora)
  {
    char *pMessage = link.CodeStopRec();
    bLora = SendMessage(pMessage);
    if (bDebug)
      Serial.printf("CLoRaLink::SendStop [%s]\n", pMessage);
  }
  return bLora;
}

//-------------------------------------------------------------------------
//! \brief Transmitting slave OK
//! \param iSlaveNumber Number of the slave
//! \param bOK Status of slave
//! \return Return true if OK
bool CLoRaLink::SendSlaveOK(
  int iSlaveNumber, bool bOK
  )
{
  if (bLora)
  {
    char *pMessage = link.CodeSlave( iSlaveNumber, bOK);
    bLora = SendMessage(pMessage);
    if (bDebug)
      Serial.printf("CLoRaLink::SendSlaveOK [%s]\n", pMessage);
  }
  return bLora;
}

//-------------------------------------------------------------------------
//! \brief Transmitting stop mode from master to all slaves
//! \return Return true if OK
bool CLoRaLink::SendStopMode()
{
  if (bLora)
  {
    char *pMessage = link.CodeStopMode();
    bLora = SendMessage(pMessage);
    if (bDebug)
      Serial.printf("CLoRaLink::SendStopMode [%s]\n", pMessage);
  }
  return bLora;
}

//-------------------------------------------------------------------------
//! \brief Transmitting Test message to Teensy
//! \return Return true if OK
bool CLoRaLink::SendTest()
{
  if (bLora)
  {
    char *pMessage = link.CodeTest();
    bLora = SendMessage(pMessage);
    if (bDebug)
      Serial.printf("CLoRaLink::SendTest [%s]\n", pMessage);
  }
  return bLora;
}

//-------------------------------------------------------------------------
//! \brief Emission of the prepared message
//! \param pMessage string to send
//! \return Return true if OK
bool CLoRaLink::SendMessage(
  char *pMessage)
{
  bool bOK = true;
  if (LoRa.beginPacket() < 1)
    bOK = false;
  if (LoRa.print(pMessage) < 1)
    bOK = false;
  if (LoRa.endPacket() < 1)
    bOK = false;
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief Processing message receptions
//! \return Returns the number of the received message
int CLoRaLink::OnReceive()
{
  //Serial.println("CLoRaLink::OnReceive");
  int iReturn = -1;
  // Test if there are received characters
  int packetSize = LoRa.parsePacket();
  if (packetSize)
  {
    if (bDebug) Serial.printf("CLoRaLink::OnReceive packetSize %d\n", packetSize);
    // RAZ message
    int iPos = 0;
    sMessageReception[0] = 0;
    // Reading bytes of the message
    while (LoRa.available())
    {
      sMessageReception[iPos] = (char)LoRa.read();
      iPos++;
      if (iPos > 250)
        break;
    }
    sMessageReception[iPos] = 0;
    iRSSI = LoRa.packetRssi();
    iErreur = ES_OK;
    if (bDebug)
      Serial.printf("CLoRaLink::OnReceive [%s]\n", sMessageReception);
    iReturn = LMOTHERLORA;
    if (sMessageReception[0] == '$' and sMessageReception[strlen(sMessageReception)-1] == '!')
      // Lora message from PRSS
      iReturn = link.DecodeMessageType( sMessageReception);
    switch( iReturn)
    {
    case LMPARAMS  :  //!< Send parameters
      if (!link.DecodeParams( pParams, sMessageReception))
        iErreur = ES_ERPARAM;
      break;
    case LMSLAVE   :  //!< Slave OK
      if (!link.DecodeSlave( &iSlaveNumber, &bSlaveStatus, sMessageReception))
        iErreur = ES_ERSLAVE;
      break;
    case LMTEST    :  //!< Test message
      Serial.println("CLoRaLink::OnReceive LMTEST");
      break;
    case LMOTHERLORA:
      // Nothing to do, Lora message send by an other device
      break;
    case LMSTARTREC:
    case LMSTOPREC:
    case LMTOP:
    case LMSTOPMODE:
      // Nothing to do
      break;
    default:
      Serial.printf("CLoRaLink::OnReceive error decoding num message %d [%s]\n", iReturn, sMessageReception);
      iErreur = ES_ERLORMESS;
    }
  }
  if (LoRa.isCRCError())
    Serial.println("LoRa chip, CRC Error !");
  if (LoRa.isTimeoutError())
    Serial.println("LoRa chip, Timeout Error !");
  return iReturn;
}
