//-------------------------------------------------------------------------
//! \file CTHSensor.h
//! \brief Management class of temperature / humidity sensors type BMP280, BME280 or AHT10
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
//! BPM   from https://github.com/ControlEverythingCommunity/BME280
//! AHT10 from https://github.com/Thinary/AHT10
/* 
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifndef THSensor
#define THSensor

#include <stdint.h>

//-------------------------------------------------------------------------
//! \class CTHSensor
//! \brief Management class of temperature / humidity sensors type BMP280, BME280 or AHT10
class CTHSensor
{
  public:
    //-------------------------------------------------------------------------
    //! \brief Constructor (initialization of parameters to default values)
    CTHSensor();
    
    //-------------------------------------------------------------------------
    //! \brief Test if the sensor is present
    //! \return True if a sensor is present
    bool Begin();
    
    //-------------------------------------------------------------------------
    //! \brief Read humidity
    //! \return Humidity in percentage
    float ReadHumidity();
 
    //-------------------------------------------------------------------------
    //! \brief Read temperature
    //! \return Temperature in degrees celsius
    float ReadTemperature();

  protected:
    //-------------------------------------------------------------------------
    //! \brief Read AHT10 sensor
    //! \return Temperature in degrees celsius
    unsigned long readAHT10Sensor(bool GetDataCmd);

    //-------------------------------------------------------------------------
    //! \brief Read BMP sensor
    //! \return Temperature in degrees celsius
    float readBMPSensor(bool GetDataCmd);

    //! Indicates that the sensor initialization is OK
    bool bInitOK;

    //! Indicates that the sensor is BMP type
    bool bBMPType;

    //! Humidity value for BMP sensor
    float fHumidity;

    //! Temperature value for BMP sensor
    float fTemperature;
};

//-------------------------------------------------------------------------
//! \class CADCVoltage
//! \brief Management class of an MCP3221 for reading battery voltages
class CADCVoltage {
  public:
    //-------------------------------------------------------------------------
    //! \brief Constructor (initialization of parameters to default values)
    CADCVoltage();

    //-------------------------------------------------------------------------
    //! \brief Test if the MCP3221 is present
    //! \return True if a MCP3221 is present
    bool begin();

    //-------------------------------------------------------------------------
    //! \brief Read humidity
    //! \return Batterie voltage in volt
    float getVoltage();

  protected:
    //-------------------------------------------------------------------------
    //! \brief Read ADC MCP3221
    //! \return ADC value
    uint16_t read();

    //! Indicates that the MCP initialization is OK
    bool bInitOK;
};

#endif
