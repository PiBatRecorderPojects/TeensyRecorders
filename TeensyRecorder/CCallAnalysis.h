//-------------------------------------------------------------------------
//! \file CCallAnalysis.h
//! \brief Call analysis classes
//! \details class CCallAnalysis, CCallMem, CQFCCall
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2023 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 /*
  * Classes :
  * - CCallProcessing
  *   - CQFCProcessing
  * - CCallAnalysis
  */
#include <Arduino.h>
#include "Const.h"

#ifndef CCALLANALYSIS_H
#define CCALLANALYSIS_H

// Maximum number of calls processed in parallel
#define MAXNBCALLS 10

// Maximum number of calls handlers
#define MAXNBCALLHANDLER 10

//-------------------------------------------------------------------------
//! \class CCallProcessing
//! \brief Generic call processing class
class CCallProcessing
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  CCallProcessing();

  //-------------------------------------------------------------------------
  //! \brief Destructor
  virtual ~CCallProcessing();

  //-------------------------------------------------------------------------
  //! \brief Initialization of the characteristics of the analysis
  //! \param iMinCh Minimum channel
  //! \param iMaxCh Minimum channel
  virtual void InitializingAnalysis(
    int iMinCh,
    int iMaxCh
    );

  //-------------------------------------------------------------------------
  //! \brief Processing FFT results
  //! \param piTbSeuilCnx Table of thresholds
  //! \param pFFTResults Table of FFT levels
  //! \return true if call detection
  virtual bool FFTProcessing(
    volatile uint32_t *piTbSeuilCnx,
    volatile  int32_t *pFFTResults
    );

protected:
  //! Minimum channel
  int  iMinChannel;
  //! Maximum channel
  int  iMaxChannel;
};

//-------------------------------------------------------------------------
//! \class CQFCProcessing
//! \brief QFC call processing class
class CQFCProcessing: public CCallProcessing
{
public:
	//! \struct CurrentQFC
	//! \brief Parameter of a current QFC
	struct CurrentQFC
	{
	  int   iMinChannel;          //!< Minimum channel
	  int   iMaxChannel;          //!< Maximum channel
	  int   iCurrentChannel;      //!< Last detection channel
	  int   iNbDetections;        //!< Number of detections
    bool  bCurrentDet;          //!< Indicates if a detection is in progress
	};

  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  CQFCProcessing();

  //-------------------------------------------------------------------------
  //! \brief Destructor
  virtual ~CQFCProcessing();

  //-------------------------------------------------------------------------
  //! \brief Initialization of the characteristics of the QFC
  //! \param iMinCh Minimum frequency in channel
  //! \param iMaxCh Maximum frequency in channel
  //! \param iMinBd Number of channels of the minimum band
  //! \param iMaxBd Number of channels of the maximum band
  //! \param iMinDu Minimum duration in number of FFT
  //! \param iMaxDu Maximum duration in number of FFT
  virtual void InitializingAnalysis(
    int iMinCh,
    int iMaxCh,
    int iMinBd,
    int iMaxBd,
    int iMinDu,
    int iMaxDu
    );

  //-------------------------------------------------------------------------
  //! \brief Processing FFT results
  //! \param piTbSeuilCnx Table of thresholds
  //! \param pFFTResults Table of FFT levels
  //! \return true if call detection
  virtual bool FFTProcessing(
    volatile uint32_t *piTbSeuilCnx,
    volatile  int32_t *pFFTResults
    );

  //-------------------------------------------------------------------------
  //! \brief Processing a detection
  //! \param iCh Detection channel
  void ProcessingDetection(
    int iCh
    );

  //-------------------------------------------------------------------------
  //! \brief Test if a QFC is detected
  //! \return true if call detection
  bool TestFC();

protected:
  //! Number of channels of the minimum band
  int iMinBand;
  //! Number of channels of the maximum band
  int iMaxBand;
  //! Minimum duration in number of FFT
  int iMinDuration;
  //! Maximum duration in number of FFT
  int iMaxDuration;
  //! Memorization of current QFCs
  CurrentQFC tbQFC[MAXNBCALLS];
  //! Indicates that the maximum number of QFCs has been exceeded
  bool bMaxQFC;
};

//-------------------------------------------------------------------------
//! \class CCallAnalysis
//! \brief Generic class for call analyzing
class CCallAnalysis
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  CCallAnalysis();

  //-------------------------------------------------------------------------
  //! \brief Destructor
  virtual ~CCallAnalysis();

  //-------------------------------------------------------------------------
  //! \brief Initialization of the characteristics of the analysis
  //! \param iFe Sampling frequency in Hz
  //! \param iFFTLen FFT len in points
  virtual void InitializingAnalysis(
    uint32_t iFe,
    uint16_t iFFTLen
    );

  //-------------------------------------------------------------------------
  //! \brief Added QFC parser
  //! \param iMinFr Minimum band frequency in Hz
  //! \param iMaxFr Maximum band frequency in Hz
  //! \param iMinBW Minimum bandwidth in Hz
  //! \param iMaxBW Maximum bandwidth in Hz
  //! \param iMinDu Minimum duration in µs
  //! \param iMaxDu Maximum duration in µs
  virtual void AddQFCCall(
    uint32_t iMinFr,
    uint32_t iMaxFr,
    uint32_t iMinBW,
    uint32_t iMaxBW,
    uint32_t iMinDu,
    uint32_t iMaxDu
    );

  //-------------------------------------------------------------------------
  //! \brief Processing FFT results
  //! \param piTbSeuilCnx Table of thresholds
  //! \param pFFTResults Table of FFT levels
  //! \return true if call detection
  virtual bool FFTProcessing(
    volatile uint32_t *piTbSeuilCnx,
    volatile  int32_t *pFFTResults
    );

protected:
  //! Width of a channel in Hz
  int  iCanalWidth;
  //! Duration between 2 FFTs in µs
  int  iFFTDur;
  //! Number of FFT channels
  int iNbFFTChannels;
  //! Calls Managers Table
  CCallProcessing *tbpCallProcessing[MAXNBCALLHANDLER];
};

#endif
