//-------------------------------------------------------------------------
//! \file CRecorder.h
//! \brief Classe de gestion de l'enregistrement
//! classe CRecorder et CRecorderH
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/* 
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 /*
  * - CFIRFilter
  * - CGenericRecorder
  * -   CRecorder
  * -    CRecorderS
  * -    CRecorderH
  * -    CRecorderA
  * - CHeterodyne
  */
#include <Arduino.h>
#include "arm_math.h"
#include "SdFat.h"
#include "Acquisition.h"
#include "Restitution.h"
#include "CWaveFile.h"
#include "CCallAnalysis.h"

#ifndef CRECORDER_H
#define CRECORDER_H

// Définition PI * 2
#define DEUXPI 6.2831853
#if defined(__MK66FX1M0__) // Teensy 3.6, DAC interne 12 bits
  // Max des échantillons 12 bits signés du DAC
  #define MAXDAC 2047.0
#endif
#if defined(__IMXRT1062__) // Teensy 4.1, DAC externe 16 bits
  // Max des échantillons 16 bits signés du DAC
  #define MAXDAC 32767.0
#endif

// Max table de décimation
#define MAXTBDECIM 10

// Avec ouverture d'un fichier wav temporaire pour aller vite lors du démarrage d'un enregistrement
#define STARTWAVETEMP

//! \enum BUFFER_PROCESSING_MODE
//! \brief List of sample buffer processing mode
enum BUFFER_PROCESSING_MODE {
  BPM_SILENCE       = 0, //!< Extended microphone test, silence processing
  BPM_SIGNAL        = 1, //!< Extended microphone test, signal processing
  BPM_NOISEFLT      = 2, //!< Noise treatment with filter 
  BPM_NOISEFLTDECIM = 3, //!< Noise treatment with filter and decimation
  BPM_NOISENOFLT    = 4, //!< Noise processing without filter
  BPM_STEREO        = 5, //!< Stereo processing without filter
  BPM_STEREODECIM   = 6, //!< Stereo processing with filter and decimation
  BPM_MONOFLT       = 7, //!< Mono treatment with filter
  BPM_MONONOFLT     = 8, //!< Mono treatment without filter
  BPM_MONOFLTDECIM  = 9  //!< Mono treatment with filter and decimation
};

//!< Nombre max de coefficients pour le DSP
#define FIR_MAX_COEFFS 128

//!< Nombre d'échantillons à traiter par le DSP
#ifndef BLOCK_SAMPLES
  #if defined(__MK20DX128__) || defined(__MK20DX256__) || defined(__MK64FX512__) || defined(__MK66FX1M0__) || defined(__IMXRT1062__)
    #define BLOCK_SAMPLES  128
  #elif defined(__MKL26Z64__)
    #define BLOCK_SAMPLES  64
  #endif
#endif

//-------------------------------------------------------------------------
//! \class CFIRFilter
//! \brief Classe qui implémente le calcul d'un filtre FIR via le DSP
/*
 * A 180MHz :
 * Temps moyen pour  32 Taps avec 128 ech 50µs  (64 tours) total 8192 ech 3226µs
 * Temps moyen pour  48 Taps avec 128 ech 67µs  (64 tours) total 8192 ech 4332µs
 * Temps moyen pour  64 Taps avec 128 ech 85µs  (64 tours) total 8192 ech 5442µs
 * Temps moyen pour  96 Taps avec 128 ech 119µs (64 tours) total 8192 ech 7655µs
 * Temps moyen pour 128 Taps avec 128 ech 154µs (64 tours) total 8192 ech 9868µs
 * A 144MHz :
 * Temps moyen pour  32 Taps avec 128 ech  63µs (64 tours) total 8192 ech 4034µs
 * Temps moyen pour  48 Taps avec 128 ech  84µs (64 tours) total 8192 ech 5419µs
 * Temps moyen pour  64 Taps avec 128 ech 106µs (64 tours) total 8192 ech 6802µs
 * Temps moyen pour  96 Taps avec 128 ech 149µs (64 tours) total 8192 ech 9571µs
 * Temps moyen pour 128 Taps avec 128 ech 192µs (64 tours) total 8192 ech 12340µs
 */
class CFIRFilter
{
  public:
    //-------------------------------------------------------------------------
    //! \brief Constructeur (initialisation de la classe)
    CFIRFilter();

    //-------------------------------------------------------------------------
    //! \brief Initialisation des coefficients du filtre
    //! \param iNTaps Nombre de coefficients du filtre (max FIR_MAX_COEFFS)
    //! \param pfCoef Tableau des 32 coefficients du filtre
    //! \return Retourne true si initialisation OK
    bool InitFIRFilter(
      int iNTaps,
      const double *pfCoef
      );

    //-------------------------------------------------------------------------
    //! \brief Initialisation des coefficients du filtre
    //! \param iNTaps Nombre de coefficients du filtre (max FIR_MAX_COEFFS)
    //! \param pfCoef Tableau des 32 coefficients du filtre
    //! \return Retourne true si initialisation OK
    bool InitFIRFilter(
      int iNTaps,
      const int16_t *piCoef
      );

    //-------------------------------------------------------------------------
    //! \brief Applique le filtre sur un tableau d'échantillons
    //! \param pFirIn Tableau des échantillons en entrée du filtre (taille BLOCK_SAMPLES)
    //! \param pFirOut Tableau des échantillons en sortie du filtre (taille BLOCK_SAMPLES)
    void SetFIRFilter(
      q15_t *pFirIn,
      q15_t *pFirOut
      );

  protected:
    //! Liste des coefficients en format q15_t (max FIR_MAX_COEFFS coefficients)
    volatile q15_t lstCoeff[FIR_MAX_COEFFS];

    //! Instance du filtre ARM DSP Math library
    arm_fir_instance_q15 fir_inst;

    //! Tableau des coefficients du filtre
    volatile q15_t StateQ15[(BLOCK_SAMPLES + FIR_MAX_COEFFS)*4];
};
  
//-------------------------------------------------------------------------
//! \class CGenericRecorder
//! \brief Classe de base des différents enregistreurs
//! - Définie les méthodes de base virtuelles mais ne fait rien
class CGenericRecorder
{
  public:
    //-------------------------------------------------------------------------
    //! \brief Constructeur (initialisation de la classe)
    //! \param pParams Pointeur sur les paramètres opérateur
    //! \param fftLen Taille de la FFT
    CGenericRecorder(
      ParamsOperator *pParams,
      int fftLen
      );
  
    //-------------------------------------------------------------------------
    //! \brief Destructeur
    virtual ~CGenericRecorder();
  
    //-------------------------------------------------------------------------
    //! \brief Calcul pré-démarrage acquisition
    virtual void CalculPreAcquisition();
  
    //-------------------------------------------------------------------------
    //! \brief Démarre l'acquisition
    //! Retourne true si démarrage OK
    virtual bool StartAcquisition();
  
    //-------------------------------------------------------------------------
    //! \brief Stoppe l'acquisition
    //! Retourne true si arrêt OK
    virtual bool StopAcquisition();
  
    //-------------------------------------------------------------------------
    //! \brief Démarre l'enregistrement en auto ou non
    //! \param bAuto true pour lancer l'enregistrement automatique sur détection d'énergie
    virtual void SetAutoRecord(
      bool bAuto
      );
  
    //-------------------------------------------------------------------------
    //! \brief Démarre le mode enregistrement
    //! Retourne true si démarrage OK
    virtual bool StartRecording();
  
    //-------------------------------------------------------------------------
    //! \brief Stoppe le mode enregistrement
    //! Retourne true si arrêt OK
    virtual bool StopRecording();
  
    //-------------------------------------------------------------------------
    //! \brief Stoppe ou relance l'acquisition
    //! \param bAcq true pour lancer l'acquisition, false pour la suspendre
    virtual void SetAcquisition(
      bool bAcq
      );
  
    //-------------------------------------------------------------------------
    //! \brief Gestion de l'IT DMA lorsqu'un buffer d'acquisition est plein
    virtual void OnDMA_ISR();
  
    //-------------------------------------------------------------------------
    //! \brief Gestion de l'IT DMA de lecture
    virtual void OnITDMA_Reader();
  
    //-------------------------------------------------------------------------
    //! \brief Processing under interruption of a full acquisition buffer
    virtual void BufferProcessing();
    
    //-------------------------------------------------------------------------
    //! \brief Traitement hétérodyne de chaque échantillon (de base, ne fait rien)
    //! \param sample Echantillon à traiter
    virtual void Traitementheterodyne(
      int16_t sample);
    
    //-------------------------------------------------------------------------
    //! \brief Traitement détections
    virtual void TraitementDetections();
    
    //-------------------------------------------------------------------------
    //! \brief Traitement du bruit
    virtual void NoiseTreatment();
    
    //-------------------------------------------------------------------------
    //! \brief Calcul du seuil de chaque canal
    void ThresholdCalculation();
    
    //-------------------------------------------------------------------------
    //! \brief Traitements dans la boucle principale du programme
    virtual void OnLoop();
  
    //----------------------------------------------------
    //! \brief Retourne true si en acquisition
    bool IsStarting() {return bStarting;};
    
    //----------------------------------------------------
    //! \brief Retourne true si en enregistrement
    bool IsRecording() {return bRecording;};
    
    //----------------------------------------------------
    //! \brief Retourne true si le bruit est calculé
    bool IsNoiseOK() {return bNoiseOK;};
    
    //----------------------------------------------------
    //! \brief Retourne le niveau de bruit en dB
    int GetNoise() {return iBruitdB/10;};
    
    //----------------------------------------------------
    //! \brief Fonction de transformation des niveaux en dB
    //! Retourne le niveau en dizièmes de dB
    //! \param iNiv Niveau en magnitude
    static short GetNivDB(
      short iNiv
      );
    
    //----------------------------------------------------
    //! \brief Fonction de transformation des niveaux en magnitude
    //! Retourne le niveau en magnitude
    //! \param iNiv Niveau en dixième de dB
    short GetNivMag(
      short iNiv
      );
    
    //----------------------------------------------------
    //! \brief Retourne le bruit moyen min en dB
    short GetMinNoise() {return idBMinNoise / 10;};
    
    //----------------------------------------------------
    //! \brief Retourne le bruit moyen max en dB
    short GetMaxNoise() {return idBMaxNoise / 10;};
    
    //----------------------------------------------------
    //! \brief Retourne le nombre de pics de plus de 5dB au-dessus du bruit moyen
    short GetNbPics() {return iNbPics;};
    
    //----------------------------------------------------
    //! \brief Retourne la fréquence du pic max en kHz
    float GetFreqMaxNoise() {return fFreqMaxPic;};
    
    //----------------------------------------------------
    //! \brief Mémorisation du fichier de bruit
    void MemoFileNoise();
    
    //----------------------------------------------------
    // Retourne une fréquence en kHz à partir de l'indice FFT
    static float GetFreqIdx( short idx);
    
    //! \brief Instance unique de la classe
    static CGenericRecorder *pInstance;

    //----------------------------------------------------
    //! \brief Retourne true en cas d'erreur DMA
    static bool IsDMAError() {return bErrDMA;};
    
    //----------------------------------------------------
    //! \brief Retourne un message d'erreur en cas d'erreur DMA
    static char *GetDMAError() {return sErrDMA;};
    
    //----------------------------------------------------
    //! \brief Traitement erreur DMA
    //! \param DMAErr Code d'erreur du DMA
    void OnErreurDMA(
      uint32_t DMAErr
      );

    //----------------------------------------------------
    //! \brief Retourne l'indice de la fréquence minimale d'intérêt
    uint16_t GetidxFMin() { return idxFMin;};
    
    //----------------------------------------------------
    //! \brief Retourne l'indice de la fréquence maximale d'intérêt
    uint16_t GetidxFMax() { return idxFMax;};
    
    //----------------------------------------------------
    //! \brief Lance l'aquisition en mode mesure du silence (test micro)
    void StartSilence();

    //----------------------------------------------------
    //! \brief Lance l'aquisition en mode mesure du signal (test micro)
    void StartSignal();
    
    //----------------------------------------------------
    //! \brief Retourne un pointeur sur le tableau du cumul des magnitudes pour le test micro (silence ou signal)
    uint32_t *GetTbMagnitudes() {return (uint32_t *)iTbMagCnx;};

    //----------------------------------------------------
    //! \brief Retourne un pointeur sur le tableau du cumul des magnitudes pour le test micro canal gauche (silence ou signal)
    uint32_t *GetTbMagnitudesG() {return (uint32_t *)iTbMagCnxG;};

    //-------------------------------------------------------------------------
    //! \brief Traitement du test micro en mode silence
    virtual void TraitementSilence();
    
    //-------------------------------------------------------------------------
    //! \brief Traitement du test micro en mode signal
    virtual void TraitementSignal();

    //-------------------------------------------------------------------------
    //! \brief Retourne le nombre d'erreurs d'écriture d'un fichier wave
    int getNbWavefileError() {return iNbWavefileError;};
    
    //-------------------------------------------------------------------------
    //! \brief Starts the power supply of the preamp
    void StartPreampPower();
    
    //-------------------------------------------------------------------------
    //! \brief starts the power supply of the preamp
    void StopPreamPower();

    //-------------------------------------------------------------------------
    //! \brief High pass filter and DC removal on Right chanel or mono chanel
    //! \param iSample Sample to be processed
    //! \return Sample processed
    inline int16_t HighPassDCRemovalR(int16_t iSample)
    {
      // here DC is removed, Frank DD4WH 2018_10_02
      scaledPreviousFilterOutput = (int32_t)(fDCBlockingFactor * previousFilterOutput);
      filteredOutput = iSample - previousSample + scaledPreviousFilterOutput;
      previousSample = iSample;
      previousFilterOutput = filteredOutput;
      return (int16_t)filteredOutput;
    }    
    
    //-------------------------------------------------------------------------
    //! \brief High pass filter and DC removal on Left chanel
    //! \param iSample Sample to be processed
    //! \return Sample processed
    inline int16_t HighPassDCRemovalL(int16_t iSample)
    {
      // here DC is removed, Frank DD4WH 2018_10_02
      scaledPreviousFilterOutputL = (int32_t)(fDCBlockingFactor * previousFilterOutputL);
      filteredOutputL = iSample - previousSampleL + scaledPreviousFilterOutputL;
      previousSampleL = iSample;
      previousFilterOutputL = filteredOutputL;
      return (int16_t)filteredOutputL;
    }    
    
  protected:
    //! Paramètres opérateurs
    ParamsOperator *pParamsOp;

#if defined(__MK66FX1M0__) // Teensy 3.6
    //! Deux buffers d'échantillons utilisés par le DMA. Echantillons non signés
    volatile uint16_t samplesDMA[MAXSAMPLE*2];
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
    //! Deux buffers d'échantillons utilisés par le DMA. Echantillons non signés
    static DMAMEM uint16_t samplesDMA[MAXSAMPLE*2] /*__attribute__((aligned(32)))*/;
#endif
    //! Adresse du demi buffer
    volatile uint32_t *pHalfDMASamples;

    //! Taille de la FFT
    int iFFTLen;
    
    //! Indice du canal FFT de la Fréquence d'intéret minimale
    volatile uint16_t  idxFMin;
    //! Indice du canal FFT de la Fréquence d'intéret maximale
    volatile uint16_t  idxFMax;
    //! Nombre d'échantillons pour la durée minimale d'un fichier wav
    volatile uint32_t  iNbMinEch;
    //! Nombre d'échantillons pour la durée maximale d'un fichier wav
    volatile uint32_t  iNbMaxEch;
    //! Nombre d'échantillons du pré-trigger pour prise en compte dans la durée min
    volatile uint32_t iNbEchPreTrig;
 
    //! Magnitude correspondante du bruit + seuil relatif
    volatile uint16_t  iSeuilMag;
    //! Tableau des magnitudes totales pour le calcul du bruit de chaque canaux (FFTLEN/2)
    volatile uint32_t  *iTbMagCnx /*__attribute__((aligned(32)))*/;
    //! Tableau des magnitudes totales pour le calcul du bruit de chaque canaux (FFTLEN/2) canal gauche
    volatile uint32_t  *iTbMagCnxG /*__attribute__((aligned(32)))*/;
    //! Tableau des magnitudes seuil de détection de chaque canaux (FFTLEN/2)
    volatile uint32_t  *iTbSeuilCnx /*__attribute__((aligned(32)))*/;
    //! Total des magnitudes pour le calcul du bruit
    volatile uint32_t  iTotalBruitMag;
    //! Nombre de magnitudes dans le total
    volatile uint32_t  iNbMagNoise;
    //! Magnitude et niveau en dB du bruit min
    volatile uint32_t  iMagMinNoise;
    volatile short     idBMinNoise;
    //! Magnitude et niveau en dB du bruit max
    volatile uint32_t  iMagMaxNoise;
    volatile short     idBMaxNoise;
    //! Nombre de pics de bruit supérieur de 5dB à la moyenne
    short iNbPics;
    //! Fréquence en kHz du canal avec le plus de bruit
    float fFreqMaxPic;
    //! Nombre de FFT ayant servies au calcul du bruit
    volatile int iNbFFTNoise;
    //! Nombre de FFT ayant servies au calcul du bruit du canal gauche
    volatile int iNbFFTNoiseG;
    //! Bruit moyen en magnitude
    volatile short  iBruitMag;
    //! Seuil en dB
    volatile short  iSeuildB;
    //! Bruit moyen en dB
    volatile short  iBruitdB;
    //! Indique que le bruit est calculé
    volatile bool bNoiseOK;
    //! Indicateur acquisition active
    volatile bool bStarting;
    //! Indicateur enregistrement en cours
    volatile bool bRecording;
    //! Fréquence d'échantillonnage des fichiers wav en Hz
    uint32_t      uiFeWav;
    //! Fréquence d'échantillonnage de l'acquisition en Hz
    uint32_t      uiFeAcq;
    //! Largeur d'un canal en Hz
    static float  fCanal;

    //! Indique que le Test micro est en route en mode silence
    bool bTestSilence;
    //! Indique que le Test micro est en route en mode signal
    bool bTestSignal;

    //! Buffer FFT
    volatile int32_t *bufferCpxFFT /*__attribute__((aligned(32)))*/;
    
    //! Structure de gestion de la FFT
    arm_cfft_radix4_instance_q15 cpx_fft_inst;

    // Gestionnaire d'un éventuel filtre FIR (mono, canal droit)
    CFIRFilter firFilter;

    // Gestionnaire d'un éventuel filtre FIR (stéréo, canal gauche)
    CFIRFilter firFilterL;

    //! Gain numérique à appliquer (en fait décalage en nombre de bits)
    int8_t shift;

    //! Erreur éventuelle sur le DMA
    static bool bErrDMA;
    static char sErrDMA[80];
    //! Utilisation de la LED
    int iUtilLED;

    //! Indice pour le traitement des échantillons
    unsigned int   iSample;
    //! Pointeur de sauvegarde des échantillons
    int16_t       *pSaveEch;

    //! Indique le nombre d'erreur d'écriture d'un fichier wav sur la carte SD
    int iNbWavefileError;

    //! Indique si mode stéréo
    bool bStereo;
    //! En mode stéréo, indique si traitement canal droite ou gauche
    bool bRight;

    //! Tableau du bruit moyen par canal
    volatile short *iTbNoise /*__attribute__((aligned(32)))*/;
    
    //! \brief Buffer processing mode
    int iBufferProcessMode;

    //! \brief Indicate if FFT is necessary in synchro mode (FFT only on master)
    bool bFFTSynchro;

    //! Pour le filtre passe haut (Frank DD4WH)
    int32_t        filteredOutput, filteredOutputL;
    int32_t        scaledPreviousFilterOutput, scaledPreviousFilterOutputL;
    int32_t        previousSample, previousSampleL;
    int32_t        previousFilterOutput, previousFilterOutputL;
    //! Coefficient du filtre passe haut (Frank DD4WH)
    float          fDCBlockingFactor;
    //! Décimation éventuelle
    int iDecim;
    //! Tableau de décimation
    uint8_t iDecimationTable[MAXTBDECIM];
    //! Indice tableau de sortie des échantillons en cas de décimation
    int iSampleOut;
    //! Indice tableau FFT
    int iFFT;
    //! Indice tableau filtre FIR
    int iFIR;

    //! Indique le mode utilisateur debug
    bool bModeDebug;
};

//-------------------------------------------------------------------------
//! \class CRecorder
//! \brief Classe rassemblant toutes les opérations nécessaire pour l'enregistrement
//! - Acquisition (ADC via DMA cadencé par un PDB, traitement dans l'IT DMA)
//! - Traitements (dans l'IT DMA)
//! - FFT pour vérifier la présence d'un signal (dans l'IT DMA)
//! - Enregistrement dans un fichier wav si activité (dans le loop du programme principal)
class CRecorder: public CGenericRecorder
{
  public:
    //-------------------------------------------------------------------------
    //! \brief Constructeur (initialisation de la classe)
    //! \param pParams Pointeur sur les paramètres opérateur
    //! \param iNbSmpBuffer Nombre de buffers de taille MAXSAMPLE pour l'enregistrement
    CRecorder(
      ParamsOperator *pParams,
      int iNbSmpBuffer
      );
  
    //-------------------------------------------------------------------------
    //! \brief Destructeur
    virtual ~CRecorder();
  
    //-------------------------------------------------------------------------
    //! \brief Création du gestionnaire d'acquisition
    virtual void CreateAcquisition();
  
    //-------------------------------------------------------------------------
    //! \brief Démarre l'acquisition
    //! Retourne true si démarrage OK
    virtual bool StartAcquisition();
  
    //-------------------------------------------------------------------------
    //! \brief Stoppe l'acquisition
    //! Retourne true si arrêt OK
    virtual bool StopAcquisition();
  
    //-------------------------------------------------------------------------
    //! \brief Démarre l'enregistrement en auto
    //! \param bAuto true pour lancer l'enregistrement automatique sur détection d'énergie
    virtual void SetAutoRecord(
      bool bAuto
      );
  
    //-------------------------------------------------------------------------
    //! \brief Démarre le mode enregistrement
    //! Retourne true si démarrage OK
    bool StartRecording();
  
    //-------------------------------------------------------------------------
    //! \brief Stoppe le mode enregistrement
    //! Retourne true si arrêt OK
    bool StopRecording();
  
    //-------------------------------------------------------------------------
    //! \brief Stoppe ou relance l'acquisition
    //! \param bAcq true pour lancer l'acquisition, false pour la suspendre
    void SetAcquisition(
      bool bAcq
      );
  
    //----------------------------------------------------
    //! \brief A la fin d'un enregistrement, initialise le pointeur du dernier élément à enregistrer
    //! au minimum du buffer de pré-enregistrement pour ne pas avoir de recouvrement
    void SetSampRecordMin();

    //-------------------------------------------------------------------------
    //! \brief Retourne le pointeur sur la zone de réception et prépare le pointeur pour la prochaine réception
    int16_t *GetpSaveSample();
    
    //-------------------------------------------------------------------------
    //! \brief Retourne le pointeur sur la zone d'enregistrement wav et prépare le pointeur pour le prochain enregistrement
    //! NULL si aucun échantillons à enregistrer
    int16_t *GetpRecordSample();
    
    //-------------------------------------------------------------------------
    //! \brief Gestion de l'IT DMA lorsqu'un buffer d'acquisition est plein
    void OnDMA_ISR();
  
    //-------------------------------------------------------------------------
    //! \brief Processing under interruption of a full acquisition buffer
    void BufferProcessing();

    //-------------------------------------------------------------------------
    //! \brief Extended microphone test, silence processing
    virtual void MicrophoneTestSilenceProcessing();
    
    //-------------------------------------------------------------------------
    //! \brief Extended microphone test, signal processing
    virtual void MicrophoneTestSignalProcessing();
    
    //-------------------------------------------------------------------------
    //! \brief Sample processing for noise calculation without filter 
    virtual void NoiseSampleProcessWithoutFilter();
    
    //-------------------------------------------------------------------------
    //! \brief Sample processing for noise calculation with filter without decimation
    virtual void NoiseSampleProcessWithFilter();
    
    //-------------------------------------------------------------------------
    //! \brief Sample processing for noise calculation with filter and decimation
    virtual void NoiseSampleProcessWithFilterDecimation();
    
    //-------------------------------------------------------------------------
    //! \brief Sample processing for stereo mode without filter
    virtual void SampleProcessingStereo();

    //-------------------------------------------------------------------------
    //! \brief SStereo processing with filter and decimation
    virtual void SampleProcessingStereoDecim();
    
    //-------------------------------------------------------------------------
    //! \brief Sample processing for mono mode with filter without decimation
    virtual void SampleProcessingMonoFilter();
    
    //-------------------------------------------------------------------------
    //! \brief Sample processing for mono mode with filter and decimation
    virtual void SampleProcessingMonoFilterDecim();
    
    //-------------------------------------------------------------------------
    //! \brief Sample processing for mono mode without filter without decimation
    virtual void SampleProcessingMonoWithoutFilter();
    
    //-------------------------------------------------------------------------
    //! \brief Processing of mono or right channel detections
    void TraitementDetections();
    
    //-------------------------------------------------------------------------
    //! \brief TProcessing of left channel detections
    void TraitementDetectionsGauche();
    
    //-------------------------------------------------------------------------
    //! \brief Traitements dans la boucle principale du programme
    virtual void OnLoop();
  
    //----------------------------------------------------
    //! \brief Retourne true si en acquisition
    bool IsStarting() {return bStarting;};
    
    //----------------------------------------------------
    //! \brief Retourne les informations de détection max pour le mode test micro
    //! \param *pNivMaxR Niveau max du canal droit
    //! \param *pFrMaxR  Fréquence en kHz du maximum du canal droit
    //! \param *pNivMaxL Niveau max du canal gauche
    //! \param *pFrMaxL  Fréquence en kHz du maximum du canal gauche
    void GetNiveauMax(
      int *pNivMaxR,
      int *pFrMaxR,
      int *pNivMaxL,
      int *pFrMaxL
      );

    //----------------------------------------------------
    //! \brief Initialise la fréquence de coupure du filtre passe-haut
    //! \param iHighPassFreq Fréquence du coupure en Hz
    void SetFreqHighPass(
      int iHighPassFreq);

    //----------------------------------------------------
    //! \brief Retourne le tableau du nombre de détections par canaux sur les 8 dernières FFT (taille FFTLEN / 2)
    uint8_t *GetTbNbDetect() { return (uint8_t *)TbMemoNbDetect;};
    
    //----------------------------------------------------
    //! \brief RAZ du tableau de mémorisation du nombre de détections par canaux sur les 8 dernières FFT (taille FFTLEN / 2)
    void RAZTbNbDetect() { memset( (void *)TbMemoNbDetect, 0, sizeof(TbMemoNbDetect));};
    
    //----------------------------------------------------
    //! \brief Retourne l'indice de la fréquence minimale d'intérêt
    uint16_t GetidxFMin() { return idxFMin;};
    
    //----------------------------------------------------
    //! \brief Retourne l'indice de la fréquence maximale d'intérêt
    uint16_t GetidxFMax() { return idxFMax;};
    
    //----------------------------------------------------
    //! \brief Retourne la durée de l'enregistrement courant en secondes
    uint32_t GetRecordDuration();
    
    //----------------------------------------------------
    //! \brief Retourne la taille du buffer d'enregistrement
    uint32_t GetSizeoffSamplesRecordBuffer() {return sizeof(int16_t)*MAXSAMPLE*iNbSampleBuffer;};
    
    //----------------------------------------------------
    //! \brief Retourne le nombre de buffer d'enregistrement
    uint32_t GetiNbSampleBuffer() {return iNbSampleBuffer;};
    
    //----------------------------------------------------
    //! \brief Creation of the log file in Batcorder mode
    void CreateBatcorderLogFile();
    
    //----------------------------------------------------
    //! \brief Addition of a recording period in the Batcorder log
    //! \param bOn true for begin period
    void AddNewPeriodBatcorderLogFile(
      bool bOn);
    
    //----------------------------------------------------
    //! \brief Adding a wav file in the Batcorder log
    void AddNewWavBatcorderLogFile();
    
    //----------------------------------------------------
    //! \brief Adding a temperature in the Batcorder log
    //! \param fTemp Temperature
    void AddNewTemperatureBatcorderLogFile(
      float fTemp);
    
    //----------------------------------------------------
    //! \brief Return wave file name
    char *GetFileName() { return sWaveFileName; };

  protected:
    //-------------------------------------------------------------------------
    //! \brief Ouverture du fichier wav courant
    void OpenWave();
  
    //-------------------------------------------------------------------------
    //! \brief Fermeture du fichier wav courant
    void CloseWave();

    //----------------------------------------------------
    //! \brief Initialisation du filtre éventuel
    //! Retourne false en cas d'erreur
    bool InitFiltre();

    //! Gestionnaire d'acquisition
    Acquisition *pAcquisition;

    //! Un grand buffer  d'échantillons signés après filtrage pour enregistrement
    //! Buffer créé dynamiquement pour optimiser sa taille en fonction du type d'enregistrement.
    //! Plus la taille est importante, plus le pré-trigger est grand
    //! Mais il faut laisser suffisamment de place à la pile du processeur...
    volatile int16_t  *pSamplesRecord /*__attribute__(( aligned(32)))*/;

    //! Nombre de buffers de taille MAXSAMPLE pour l'enregistrement
    int iNbSampleBuffer;
  
    //! Pointeur de réception du prochain bloc dans le buffer d'entregistrement
    volatile int16_t *pNextSampIn;
    //! Pointeur de réception courant dans le buffer d'entregistrement
    volatile int16_t *pCurrentSampIn;
    //! Pointeur sur le dernier bloc reçu dans le buffer d'entregistrement
    volatile int16_t *pPrevSampIn;
    //! Pointeur sur le 1er bloc à enregistrer dans le buffer d'entregistrement
    volatile int16_t *pSampRecord;
    //! Pointeur max du buffer d'enregistrement
    volatile int16_t *pSampMax;
    //! Indique si on a perdu un buffer en enregistrement (l'enregistrement des wav ne va pas assez vite
    bool bBufferLoss;

    //! Pointeur sur le buffer à traiter
    volatile uint16_t *pBufferATraiter;
    
    //! Gestionnaire de fichiers wav
    CWaveFile wavefile;

#ifdef STARTWAVETEMP
    //! Nom du fichier wav temporaire
    char sWaveFileTemp[MAXNAMEFILES];
#endif
    //! Nom du fichier wav
    char sWaveFileName[MAXNAMEFILES];

    //! Indicateur d'activité
    volatile bool bActivite;

    //! Indicateur activité canal gauche
    volatile bool bActivGauche;
  
    //! Total des échantillons du fichier en cours d'enregistrement
    volatile uint32_t iTotalEchRec;

    //! Total des échantillons depuis la dernière activité
    volatile uint32_t iNbEchLastA;

    //! Tableau des détections positives des différents canaux FFT sur les 8 dernières FFT, 1 bit à 1 représente une détection
    volatile uint8_t TbBitsDetect[FFTLEN/2];
    volatile uint8_t TbBitsDetectL[FFTLEN/2];

    //! Tableau du nombre de détections positives des différents canaux FFT sur les 8 dernières FFT 
    volatile uint8_t TbNbDetect[FFTLEN/2];
    volatile uint8_t TbNbDetectL[FFTLEN/2];

    //! Tableau de mémorisation du nombre de détections positives des différents canaux FFT (max 8 détections)
    volatile uint8_t TbMemoNbDetect[FFTLEN/2];
    volatile uint8_t TbMemoNbDetectL[FFTLEN/2];

    //! Un buffer temporaire pour le filtre canal droit
    volatile q15_t   samplesFirIn [BLOCK_SAMPLES];
    volatile q15_t   samplesFirOut[BLOCK_SAMPLES];

    //! Un buffer temporaire pour le filtre canal gauche
    volatile q15_t   samplesFirInL [BLOCK_SAMPLES];
    volatile q15_t   samplesFirOutL[BLOCK_SAMPLES];
    
    //! Indique si on est en mode enregistrement auto
    bool           bAutorecord;
    //! Indique si on est en mode test micro
    bool           bTestMicro;
    //! Niveau max détecté en magnitude pour le test micro (dB)
    int            iNiveauMax;         
    //! Indice du canal de détection pour le test micro
    int            iCnxDetect;
    //! Niveau max du canal gauche détecté en magnitude pour le test micro (dB)
    int            iNiveauMaxL;         
    //! Indice du canal de détection du canal gauche pour le test micro
    int            iCnxDetectL;
    //! Fréquence en Hz du filtre passe haut
    int            iFreqHighPass;
    //! For file number in Batcoder file name mode (set to 0 when switch on only)
    static int     iFileNumber;
    //! Recorder name in Batcorder file name mode (max 4 chars)
    char           sPrefixe[MAXPREFIXE+1];
    //! For wave file time log in Batcorder file name mode
    char           sTimeWave[60];
    //! Indique un mode Maitre en mode synchro
    bool bMasterSynchro;
    //! Indique un mode Master ou Slave en mode synchro
    int iMasterSlaveSynchro;
    //! Indique le nombre d'enregistrements pour allumer la LED en mode synchro
    int iNbSynchRec;

    //! Pointeur sur le gestionnaire éventuel de QFC
    CCallAnalysis *pCallAnalysis;
};

//-------------------------------------------------------------------------
//! \class CRecorderS
//! \brief Classe dérivée qui implémente la fonction d'enregistrement auto stéréo
class CRecorderS: public CRecorder
{
  public:
    //-------------------------------------------------------------------------
    //! \brief Constructeur (initialisation de la classe)
    //! \param pParams Pointeur sur les paramètres opérateur
    //! \param iNbSmpBuffer Nombre de buffers de taille MAXSAMPLE pour l'enregistrement
    CRecorderS(
      ParamsOperator *pParams,
      int iNbSmpBuffer
      );
  
  	//-------------------------------------------------------------------------
  	//! \brief Création du gestionnaire d'acquisition
  	virtual void CreateAcquisition();

#if defined(__IMXRT1062__) // Teensy 4.1
    //-------------------------------------------------------------------------
    //! \brief Traitements dans la boucle principale du programme
    virtual void OnLoop();
#endif
  
};

/* Table du coeficient pour le controle automatique du gain hétérodyne (max 1)
Max Sample       50  100  150 200 250 300 350 400 450
Coefficient     0.01 0.1 0.25 0.4 0.6 0.7 0.8 0.9 1.0
Rang (Max/50-1)  0   1    2   3   4   5   6   7   8
*/
const float tbCoef[] = {0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.8, 1.0};
#define MAXTBCOEF 8
// Seuil adaptatif pour monter rapidement
#define HAGC_THR  50
// Temps pour décendre doucement 256=0.66ms, 512=1.33ms, 1024=2.66ms, 2048=5.3ms, 4096=10.6ms (à 384kHz)
#define HAGC_TIME 4096
// Coefficient maximum
#define HAGC_CMAX 1.0
// Coefficient minimum
#define HAGC_CMIN 0.01
// Niveau maximum pour un gain max
#define HAGC_SEUILMAX 500

//-------------------------------------------------------------------------
//! \class CHeterodyne
//! \brief Classe qui implémente le calcul hétérodyne
class CHeterodyne
{
  public:
    //-------------------------------------------------------------------------
    //! \brief Constructeur (initialisation de la classe)
    CHeterodyne();
  
    //-------------------------------------------------------------------------
    //! \brief Démarre l'hétérodyne
    //! \param uiFe Fréquence d'échantillonnage en Hz
    void StartHeterodyne(
      uint32_t uiFe
      );
  
    //-------------------------------------------------------------------------
    //! \brief Initialisation de la fréquence hétérodyne
    //! \param fH Fréquence hétérodyne en kHz
    void SetFreqHeterodyne(
      float fH
      );
  
    //-------------------------------------------------------------------------
    //! \brief Calcul du signal hétérodyne de chaque échantillon
    //! \param sample Echantillon signé à traiter
    //! \return Résultat du calcul htérodyne
    inline int16_t CalculHeterodyne(
      int16_t sample)
    {
      if (bAGCAuto)
      {
        // Mémo du niveau max
        if (sample > iNivMax)
          iNivMax = sample;
        iNbSampleMax++;
        // On monte rapidement le gain et on baisse doucement
        if (iNivMax > iSeuilNiv)
        {
          // Le niveau monte, donc RAZ période temporelle
          iNbSampleMax = 0;
          // On monte le gain adaptatif
          if (iNivMax > HAGC_SEUILMAX)
            // On mode le gain instentanément en présence d'un niveau fort
            iSeuilNiv = HAGC_SEUILMAX;
          else
          {
            // Sinon, on monte le gain plus lentement
            iSeuilNiv += HAGC_THR;
            if (iSeuilNiv > HAGC_SEUILMAX)
              iSeuilNiv = HAGC_SEUILMAX;
          }
          // On calcule le nouveau coefficient du signal hétérodyne
          int iTb = (iSeuilNiv / 50) + 1;
          if (iTb < 0)
            iTb = 0;
          else if (iTb > MAXTBCOEF)
            iTb = MAXTBCOEF;
          fHCoef = tbCoef[iTb];
          //Serial.printf("Plus iNivMax %d, iSeuilNiv %d, fHCoef %f\n", iNivMax, iSeuilNiv, fHCoef);
          iNivMax = 0;
        }
        if (iNbSampleMax >= HAGC_TIME)
        {
          // Période temporelle sans monté du niveau max
          // On descend le gain adaptatif
          iSeuilNiv -= HAGC_THR;
          if (iSeuilNiv < HAGC_THR)
            iSeuilNiv = HAGC_THR;
          // On calcule le nouveau niveau du signal hétérodyne
          int iTb = (iSeuilNiv / 50) + 1;
          if (iTb < 0)
            iTb = 0;
          else if (iTb > MAXTBCOEF)
            iTb = MAXTBCOEF;
          fHCoef = tbCoef[iTb];
          //Serial.printf("Moins iNivMax %d, iSeuilNiv %d, fHCoef %f\n", iNivMax, iSeuilNiv, fHCoef);
          // On prépare la nouvelle période de test du niveau max
          iNbSampleMax = 0;
          iNivMax = 0;
        }
      }
      // Memo de la transpo courante
      float VarTemp  = ej2PiFoNTe.Re;
      // Calcul de la transpo suivante
      float VarTemp2 = ej2PiFoTe.Re * ej2PiFoNTe.Re - ej2PiFoTe.Im * ej2PiFoNTe.Im;
      ej2PiFoNTe.Im  = ej2PiFoTe.Re * ej2PiFoNTe.Im + ej2PiFoTe.Im * ej2PiFoNTe.Re;
      ej2PiFoNTe.Re  = VarTemp2;
      if (bAGCAuto and fHCoef < 0.99)
        // Application AGC
        VarTemp = VarTemp * fHCoef;
      // Transposition de l'échantillon (Signal.Re * SinTranspo)
#if defined(__MK66FX1M0__) // Teensy 3.6, sortie mono sur DAC 12 bits
      VarTemp *= (float)sample;
#endif
#if defined(__IMXRT1062__) // Teensy 4.1, sortie stéréo sur I2S 16 bits
      VarTemp *= (float)(sample << 4);
#endif
      // Normalisation du résultat en int16
      return (int16_t)(VarTemp / MAXDAC);
    }

    //-------------------------------------------------------------------------
    //! \brief Initialise l'AGC auto ou non
    //! \param level Niveau du signal de 0.0 à 1.0
    void SetAGC(
      bool bAuto); // true si AGC auto

  protected:
    // Maximum level for hétérodyne signal (0.0 to 1.0)
    float          fLevel;
    //! Fréquence hétérodyne
    float          fFreqH;
    //! Fréquence d'échantillonnage de l'acquisition en mode hétérodyne
    uint32_t       uiFeH; 
    //! \struct complexe
    //! \brief Définition d'un complexe pour le calcul des échantillons de la fréquence hétérodyne
    struct complexe
    {
      float Re; //!< Partie réelle
      float Im; //!< Partie imaginaire
    };
    complexe ej2PiFoTe;
    complexe ej2PiFoNTe;
    //! Timer to reinit complexe if frequency don't change
    uint32_t uiTimeMax;
    //! Mémo du niveau max sur la période de calcul
    int16_t        iNivMax;
    //! Seuil adaptatif
    int16_t        iSeuilNiv;
    //! Nombre d'échantillon de la période de calcul
    int            iNbSampleMax;
    //! Coefficient de changement du niveau du signal hétérodyne (0.1 to 0.9)
    float          fHCoef;
    //! Mode AGC
    bool           bAGCAuto;
};

//-------------------------------------------------------------------------
//! \class CRecorderH
//! \brief Classe dérivée qui implémente la fonction hétérodyne
class CRecorderH: public CRecorder
{
  public:
    //-------------------------------------------------------------------------
    //! \brief Constructeur (initialisation de la classe)
    //! \param pParams Pointeur sur les paramètres opérateur
    //! \param iNbSmpBuffer Nombre de buffers de taille MAXSAMPLE pour l'enregistrement
    CRecorderH(
      ParamsOperator *pParams,
      int iNbSmpBuffer
      );
  
    //-------------------------------------------------------------------------
    //! \brief Destructeur
    virtual ~CRecorderH();
  
    //-------------------------------------------------------------------------
    //! \brief Démarre l'acquisition
    //! Retourne true si démarrage OK
    virtual bool StartAcquisition();
  
    //-------------------------------------------------------------------------
    //! \brief Stoppe l'acquisition
    //! Retourne true si arrêt OK
    virtual bool StopAcquisition();
  
    //-------------------------------------------------------------------------
    //! \brief Gestion de l'IT DMA de lecture
    virtual void OnITDMA_Reader();
  
    //----------------------------------------------------
    //! \brief Initialise la fréquence hétérodyne
    //! \param fH Fréquence hétérodyne en kHz
    void SetFrHeterodyne( float fH);
    
    //-------------------------------------------------------------------------
    //! \brief Traitement détections
    void TraitementDetections();
    
    //-------------------------------------------------------------------------
    //! \brief Traitement hétérodyne de chaque échantillon (de base, ne fait rien)
    //! \param sample Echantillon signé à traiter
    virtual void Traitementheterodyne(
		int16_t sample);
    
    //----------------------------------------------------
    //! \brief Initialisation du filtre passe bas du signal hétérodyne avant décimation
    //! \return Retourne false en cas d'erreur
    bool InitFiltreH();

#if defined(__IMXRT1062__) // Teensy 4.1, filtre hétérodyne sélectif
    //----------------------------------------------------
    //! \brief Initialisation du filtre sélectif
    //! \return Retourne false en cas d'erreur
    bool InitFiltreSel();

    //----------------------------------------------------
    //! \brief Initialisation de l'état sélectif ou non
    //! \param bSel true si avec filtre sélectif
    void SetFiltreSel( bool bSel);
#endif
    
    //-------------------------------------------------------------------------
    //! \brief Initialise l'AGC auto ou non
    //! \param level Niveau du signal de 0.0 à 1.0
    void SetAGC(
      bool bAuto) // true si AGC auto
      {Heterodyne.SetAGC(bAuto);};

  protected:
    // Gestionnaire de calcul hétérodyne
    CHeterodyne Heterodyne;
    
    //! Indique si l'hétérodyne est actif
    bool           bHeterodyneActif;
    //! Fréquence hétérodyne en kHz
    float          fFreqH;
    //! Modulo de la Fe hétérodyne par rapport à la Fe enregistrement
    int            iModHet;
    //! Fréquence d'échantillonnage de l'hétérodyne
    uint32_t       uiFeH;
    //! Double buffers des échantillons à jouer sur le DAC
#if defined(__MK66FX1M0__) // Teensy 3.6, sortie mono sur DAC 12 bits
    volatile uint16_t samplesDAC[MAXSAMPLE_HET*2];
#endif
#if defined(__IMXRT1062__) // Teensy 4.1, sortie stéréo sur I2S 16 bits
    static DMAMEM uint16_t samplesDAC[MAXSAMPLE_HET*4] /*__attribute__((aligned(32)))*/;
#endif
    //! Nombre max d'échantillons dans le DMA
    int            iMaxDMA;
    //! Indice de la moitié du buffer des échantillons DMA
    int            iHalfDMA;
    //! Indice de gestion du buffer des échantillons à jouer sur le DAC
    int            iSampleDAC;
    //! Décimation (par 10 à 250 ou 500kHz et par 12 à 384kHz)
    int            iDecimation;
    //! Indice de gestion de la décimation dans le buffer des rsultat du filtre FIR (décimation par 10 ou 12 et buffer filtre FIR de 128)
    int            idxDecim;
    //! Incrément de la décimation
    int            iPlusDecim;
    //! Indique si la restitution est active
    bool           bRestitution;
    //! Gestionnaire de restitution audio sur le DAC0
    Restitution    restitution;
    //! Pour la mesure la température toute les 10s
    unsigned long  uiTimeTempH;
    //! Indice du filtre passe bas hétérodyne
    unsigned int   uiFiltFir;
    //! Indique si init Restitution OK
    bool           bInitRestitution;
#if defined(__IMXRT1062__) // Teensy 4.1, filtre hétérodyne sélectif
    // >Indique si le filtre sélectif est actif
    bool bFiltreSel;
    // Gestionnaire du filtre FIR sélectif
    CFIRFilter SelectiveFilter;
    // Indice du filtre sélectif utilisé
    int idxSelFilter;
    // Indice courant du sample
    int idxSampleSelFilter;
    //! Buffers pour le filtre sélectif
    volatile q15_t samplesFirSelIn [BLOCK_SAMPLES];
    volatile q15_t samplesFirSelOut[BLOCK_SAMPLES];
#endif
};

//-------------------------------------------------------------------------
//! \class CRecorderA
//! \brief Classe dérivée qui implémente la fonction enregistrement audio (mode hétérodyne si Fe < 192kHz)
class CRecorderA: public CRecorder
{
  public:
    //-------------------------------------------------------------------------
    //! \brief Constructeur (initialisation de la classe)
    //! \param pParams Pointeur sur les paramètres opérateur
    //! \param iNbSmpBuffer Nombre de buffers de taille MAXSAMPLE pour l'enregistrement
    CRecorderA(
      ParamsOperator *pParams,
      int iNbSmpBuffer
      );
  
    //-------------------------------------------------------------------------
    //! \brief Destructeur
    virtual ~CRecorderA();
  
    //-------------------------------------------------------------------------
    //! \brief Démarre l'acquisition
    //! Retourne true si démarrage OK
    virtual bool StartAcquisition();
  
    //-------------------------------------------------------------------------
    //! \brief Stoppe l'acquisition
    //! Retourne true si arrêt OK
    virtual bool StopAcquisition();
  
    //-------------------------------------------------------------------------
    //! \brief Gestion de l'IT DMA de lecture
    virtual void OnITDMA_Reader();
  
    //-------------------------------------------------------------------------
    //! \brief Traitement hétérodyne de chaque échantillon (de base, ne fait rien)
    //! \param sample Echantillon à traiter
    virtual void Traitementheterodyne(
    int16_t sample);
    
  protected:   
    //! Fréquence d'échantillonnage de la restitution audio
    uint32_t       uiFeA; 
    //! Double buffers des échantillons à jouer sur le DAC
#if defined(__MK66FX1M0__) // Teensy 3.6
    //! Double buffers des échantillons DMA
    volatile uint16_t samplesAudio[MAXSAMPLE_AUDIOR*2];
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
    //! Sur Teensy 4.1, buffer stéréo
    static DMAMEM int16_t samplesAudio[MAXSAMPLE_AUDIOR*4];
#endif
    //! Indice de gestion du buffer des échantillons à jouer sur le DAC
    int            iSampleA;
    //! Gestionnaire de restitution audio sur le DAC0
    Restitution    restitution;
    //! Pour la mesure de la température toute les 10s
    unsigned long  uiTimeTempH;
};

#endif // CRECORDER_H
