#if defined(__IMXRT1062__)

#include "Arduino.h"
#include "common.h"
#include "hal.h"

#define COMPARE_WAKE    34
#define ALARM_WAKE      35
#define TIMER_WAKE      36
#define TOUCH_WAKE      37

#define CCM_ANALOG_PFD_480_PFD2_CLKGATE    ((uint32_t)(1 << 23))
#define CCM_ANALOG_PFD_528_PFD2_CLKGATE    ((uint32_t)(1 << 23))
#define XTALOSC24M_LOWPWR_CTRL_SET_OSC_SEL ((uint32_t)(0x10U))
//#define CCM_ANALOG_MISC0_XTAL_24M_PWD      ((uint32_t)(0x40000000U))
#define XTALOSC24M_LOWPWR_CTRL_LPBG_SEL     (0x20U)

#define CCM_ANALOG_PFD_528_PFD0_STABLE ((uint32_t)(1<<6))
#define CCM_ANALOG_PFD_528_PFD2_STABLE ((uint32_t)(1<<22))
#define XTALOSC24M_LOWPWR_CTRL_XTALOSC_PWRUP_STAT ((uint32_t)(1<<16))

#define XTALOSC24M_LOWPWR_CTRL_SET_OSC_SEL_MASK  (0x10U)
#define CCM_ANALOG_MISC0_XTAL_24M_PWD_MASK       (0x40000000U)

#define DCDC_REG2_LOOPCTRL_EN_RCSCALE1(n)        ((uint32_t)(((n) & 0x07) << 9))

// SCB D-Cache Clean by Set-way Register Definitions
#define SCB_DCCSW_SET_POS   5
#define SCB_DCCSW_SET_MASK  (0x1FF << SCB_DCCSW_SET_POS)

#define SCB_DCCSW_WAY_POS   30
#define SCB_DCCSW_WAY_Msk   (3 << SCB_DCCSW_WAY_POS)

// Cache Size ID Register Macros
#define CCSIDR_WAYS(x)  (((x) & SCB_CCSIDR_ASSOCIATIVITY_MASK) >> SCB_CCSIDR_ASSOCIATIVITY_POS)
#define CCSIDR_SETS(x)  (((x) & SCB_CCSIDR_NUMSETS_MASK) >> SCB_CCSIDR_NUMSETS_POS      )

#define SCB_CCSIDR_ASSOCIATIVITY_POS    3
#define SCB_CCSIDR_ASSOCIATIVITY_MASK   (0x3FF << SCB_CCSIDR_ASSOCIATIVITY_POS)

#define SCB_CCSIDR_NUMSETS_POS          13
#define SCB_CCSIDR_NUMSETS_MASK         (0x7FFF << SCB_CCSIDR_NUMSETS_POS)

#define SCB_DCCISW_SET_POS              5
#define SCB_DCCISW_SET_MASK             (0x1FF << SCB_DCCISW_SET_POS)

#define SCB_DCCISW_WAY_POS              30
#define SCB_DCCISW_WAY_MASK              (3 << SCB_DCCISW_WAY_POS)

#define SCB_CCR_DC_POS                  16
#define SCB_CCR_DC_MASK                 (1 << SCB_CCR_DC_POS)

uint32_t pll_sys;
uint32_t pll_usb1;
uint32_t pll_usb2;
uint32_t pll_audio;
uint32_t pll_video;
uint32_t pll_enet;
uint32_t pfd_480;
uint32_t pfd_528;
uint32_t usbphy1;
uint32_t usbphy2;

extern volatile uint32_t usb_cdc_line_rtsdtr_millis;
extern volatile uint8_t  usb_cdc_line_rtsdtr;
extern volatile uint32_t set_arm_clock(uint32_t frequency);

FASTRUN uint32_t set_osc_clock(uint32_t frequency);

#ifdef __cplusplus
extern "C" {
#endif
    //extern void start_usb_pll(void);
	extern FLASHMEM void usb_pll_start();
#ifdef __cplusplus
}
#endif

void start_up( );

volatile DMAMEM int wake_source = -1;

void ( * clear_flags )( uint32_t );

/*
// Context Restore
extern unsigned long _estack;
// R4, R5, R6, R7, R8, R9, R10, R11, R12, LR
#define CORE_SAVE_COMMON_REGS_NUM  12
// PRIMEMASK, CONTROL, MSP, PSP
#define CORE_SAVE_SPECIAL_REGS_NUM 4
DMAMEM uint32_t s_common_regs_context_buf[CORE_SAVE_COMMON_REGS_NUM+2] = { 0 };
DMAMEM uint32_t s_special_regs_context_buf[CORE_SAVE_SPECIAL_REGS_NUM+2] = { 0 };
DMAMEM uint32_t *gpComRegsContextStackTop = s_common_regs_context_buf + CORE_SAVE_COMMON_REGS_NUM;
DMAMEM uint32_t *gpComRegsContextStackBottom = s_common_regs_context_buf;
DMAMEM uint32_t *gpSpecRegsContextStackTop = s_special_regs_context_buf + CORE_SAVE_SPECIAL_REGS_NUM;
DMAMEM uint32_t *gpSpecRegsContextStackBottom = s_special_regs_context_buf;
 */
//----------------------------------------------------------------------------------
void SCB_CleanDCache (void) {
    uint32_t ccsidr;
    uint32_t sets;
    uint32_t ways;
    
    SCB_ID_CSSELR = 0U; // Level 1 data cache
    __asm volatile ( "dsb \n" );
    
    ccsidr = SCB_ID_CCSIDR;
    
    // clean D-Cache
    sets = (uint32_t)(CCSIDR_SETS(ccsidr));
    do {
        ways = (uint32_t)(CCSIDR_WAYS(ccsidr));
        do {
            SCB_CACHE_DCCSW = (((sets << SCB_DCCSW_SET_POS) & SCB_DCCSW_SET_MASK) |
                               ((ways << SCB_DCCSW_WAY_POS) & SCB_DCCSW_WAY_Msk)  );
        } while (ways-- != 0U);
    } while(sets-- != 0U);
    __asm volatile ( "dsb \n" );
    __asm volatile ( "isb \n" );
}
//----------------------------------------------------------------------------------
void SCB_DisableDCache (void) {
    register uint32_t ccsidr;
    register uint32_t sets;
    register uint32_t ways;
    
    SCB_ID_CSSELR = 0U; // Level 1 data cache
    __asm volatile ( "dsb \n" );
    
    SCB_CCR &= ~(uint32_t)SCB_CCR_DC_MASK;  // disable D-Cache
    __asm volatile ( "dsb \n" );
    
    ccsidr = SCB_ID_CCSIDR;
    
    // clean & invalidate D-Cache
    sets = (uint32_t)(CCSIDR_SETS(ccsidr));
    do {
        ways = (uint32_t)(CCSIDR_WAYS(ccsidr));
        do {
            SCB_CACHE_DCCISW = (((sets << SCB_DCCISW_SET_POS) & SCB_DCCISW_SET_MASK) |
                                ((ways << SCB_DCCISW_WAY_POS) & SCB_DCCISW_WAY_MASK)  );
        } while (ways-- != 0U);
    } while(sets-- != 0U);
    __asm volatile ( "dsb \n" );
    __asm volatile ( "isb \n" );
}
//---------------------------------------------------------------------------------
int nvic_execution_priority( void ) {
    uint32_t priority=256;
    uint32_t primask, faultmask, basepri, ipsr;
    
    // full algorithm in ARM DDI0403D, page B1-639
    // this isn't quite complete, but hopefully good enough
    __asm__ volatile( "mrs %0, faultmask\n" : "=r" ( faultmask ):: );
    if ( faultmask ) return -1;
    __asm__ volatile( "mrs %0, primask\n" : "=r" ( primask ):: );
    if ( primask ) return 0;
    __asm__ volatile( "mrs %0, ipsr\n" : "=r" ( ipsr ):: );
    if ( ipsr) {
        if ( ipsr < 16 ) priority = 0; // could be non-zero
        else priority = NVIC_GET_PRIORITY( ipsr - 16 );
    }
    __asm__ volatile( "mrs %0, basepri\n" : "=r" ( basepri ):: );
    if ( basepri > 0 && basepri < priority ) priority = basepri;
    return priority;
}
//---------------------------------------------------------------------------------
FASTRUN uint32_t set_clock_osc( uint32_t frequency ) {
    if ( 24000000 % frequency || frequency > 24000000 ) return 0;
    uint32_t cbcdr = CCM_CBCDR;
    uint32_t cbcmr = CCM_CBCMR;
    uint32_t dcdc = DCDC_REG3;
    
    uint32_t div_clk2 = 1;
    uint32_t div_ahb = 1;
    uint32_t freq_div = 24000000/frequency;
    for ( int i = 1; i < 9; i++ ) {
        div_ahb = freq_div / i;
        div_clk2 = i;
        if ( div_ahb <= 8 ) break;
    }
    
    cbcdr &= ~CCM_CBCDR_PERIPH_CLK2_PODF_MASK;
    cbcdr |= CCM_CBCDR_PERIPH_CLK2_PODF( div_clk2 - 1 );
    CCM_CBCDR = cbcdr;
    
    cbcmr &= ~CCM_CBCMR_PERIPH_CLK2_SEL_MASK;
    cbcmr |= CCM_CBCMR_PERIPH_CLK2_SEL( 1 );
    CCM_CBCMR = cbcmr;
    while ( CCM_CDHIPR & CCM_CDHIPR_PERIPH2_CLK_SEL_BUSY );
    
    cbcdr &= ~CCM_CBCDR_AHB_PODF_MASK;
    cbcdr |= CCM_CBCDR_AHB_PODF( div_ahb - 1 );
    CCM_CBCDR = cbcdr;
    while ( CCM_CDHIPR & CCM_CDHIPR_AHB_PODF_BUSY );
    
    cbcdr |= CCM_CBCDR_PERIPH_CLK_SEL;
    CCM_CBCDR = cbcdr;
    while ( CCM_CDHIPR & CCM_CDHIPR_PERIPH_CLK_SEL_BUSY );
    
    cbcdr &= ~CCM_CBCDR_IPG_PODF_MASK;
    cbcdr |= CCM_CBCDR_IPG_PODF( 0 );
    
    CCM_CBCDR = cbcdr;
    
    CCM_ANALOG_PLL_ARM_SET = CCM_ANALOG_PLL_ARM_BYPASS;
    CCM_ANALOG_PLL_ARM_SET = CCM_ANALOG_PLL_ARM_POWERDOWN;
    
    F_CPU_ACTUAL = frequency;
    F_BUS_ACTUAL = frequency;
    scale_cpu_cycles_to_microseconds = 0xFFFFFFFFu / ( uint32_t )( frequency / 1000000u );
    
    dcdc &= ~DCDC_REG3_TRG_MASK;
    dcdc |= DCDC_REG3_TRG( 6 );
    DCDC_REG3 = dcdc;
    while ( !( DCDC_REG0 & DCDC_REG0_STS_DC_OK ) );
    
    return frequency;
}
//----------------------------------------------------------------------------------
FASTRUN void set_clock_rc_osc( void ) {
	// 15.6.2 XTAL OSC (LP) Control Register
    XTALOSC24M_LOWPWR_CTRL_SET = XTALOSC24M_LOWPWR_CTRL_SET_OSC_SEL;// Select the source for the 24MHz clock, RC OSC
	// 14.8.17 Miscellaneous Register 0
    CCM_ANALOG_MISC0_SET = CCM_ANALOG_MISC0_XTAL_24M_PWD;			// This field powers down the 24M crystal oscillator if set true.
	// 14.7.15 CCM Divider Handshake In-Process Register
    while ( CCM_CDHIPR != 0 );										// Wait nothing busy
    for ( uint32_t i = 0; i < 250; i++ ) asm volatile( "nop \n" );	// Wait ~12µs (F_CPU 40MHz 1 nop ~48ns)
	// 14.8.17 Miscellaneous Register 0
    CCM_ANALOG_MISC0_SET = CCM_ANALOG_MISC0_OSC_I( 3 );				// 11 MINUS_37_5_PERCENT — Decrease current in the 24MHz oscillator by 37.5%
	// 16.6.4 Digital Regulator Core Register
    PMU_REG_CORE_SET = PMU_REG_CORE_FET_ODRIVE;						// If set, increases the gate drive on power gating FETs to reduce leakage in the off state. Care must be
																	// taken to apply this bit only when the input supply voltage to the power FET is less than 1.1V.
																	// NOTE: This bit should only be used in low-power modes where the external input supply voltage is nominally 0.9V.
	// 14.8.17 Miscellaneous Register 0
    CCM_ANALOG_MISC0_SET = CCM_ANALOG_MISC0_DISCON_HIGH_SNVS;  		// This bit controls a switch from VDD_HIGH_IN to VDD_SNVS_IN, Turn off the switch
	// 16.6.5 Miscellaneous Register 0
    PMU_MISC0_SET = 0x00000004;										// REFTOP_LOWPOWER Control bit to enable the low-power mode in the analog bandgap
	// 15.6.2 XTAL OSC (LP) Control Register
    XTALOSC24M_LOWPWR_CTRL_SET = XTALOSC24M_LOWPWR_CTRL_LPBG_SEL;	// Bandgap select, Low power bandgap
	// 16.6.5 Miscellaneous Register 0
    PMU_MISC0_SET = CCM_ANALOG_MISC0_REFTOP_PWD;					// Control bit to enable the low-power mode in the analog bandgap
    while ( CCM_CDHIPR != 0 );										// Wait nothing busy
    for ( uint32_t i = 0; i < 250; i++ ) asm volatile( "nop \n" );	// Wait ~12µs (F_CPU 40MHz 1 nop ~48ns)
	// 14.7.1 CCM Control Register
    CCM_CCR = ( CCM_CCR_COSC_EN										// On chip oscillator enable bit, enable on chip oscillator
	          | CCM_CCR_OSCNT( 0xAF ) );							// Oscillator ready counter value
	// 17.6.1 GPC Interface control register
    GPC_CNTR |= GPC_CNTR_PDRAM0_PGE;								// FlexRAM PDRAM0 Power Gate Enable, FlexRAM PDRAM0 domain will be powered down when the CPU core is powered down
}
//----------------------------------------------------------------------------------
FASTRUN void set_clock_xtal_osc( void ) {
	// 14.8.17 Miscellaneous Register 0
    CCM_ANALOG_MISC0_CLR = CCM_ANALOG_MISC0_XTAL_24M_PWD;			// This field powers on the 24M crystal oscillator if set true.
    while ((XTALOSC24M_LOWPWR_CTRL & XTALOSC24M_LOWPWR_CTRL_XTALOSC_PWRUP_STAT) == 0);	// Wait osc
	// 14.8.17 Miscellaneous Register 0
    CCM_ANALOG_MISC0_SET = CCM_ANALOG_MISC0_OSC_XTALOK_EN;			// This bit enables the detector that signals when the 24MHz crystal oscillator is stable
    while ((CCM_ANALOG_MISC0 & CCM_ANALOG_MISC0_OSC_XTALOK) == 0);	// Wait 24MHz crystal oscillator is stable
    CCM_ANALOG_MISC0_CLR = CCM_ANALOG_MISC0_OSC_XTALOK_EN;			// Disables the detector that signals when the 24MHz crystal oscillator is stable
	// 15.6.2 XTAL OSC (LP) Control Register
    XTALOSC24M_LOWPWR_CTRL_CLR = XTALOSC24M_LOWPWR_CTRL_SET_OSC_SEL;// Select the source for the 24MHz clock, XTAL-OSC
    while ( CCM_CDHIPR != 0 );										// Wait nothing busy
    for ( uint32_t i = 0; i < 200; i++ ) asm volatile( "nop \n" );	// Wait ~9.6µs (F_CPU 40MHz 1 nop ~48ns)
	// 14.8.17 Miscellaneous Register 0
    CCM_ANALOG_MISC0_CLR = CCM_ANALOG_MISC0_REFTOP_PWD;				// Control bit to disable the low-power mode in the analog bandgap
    while ((CCM_ANALOG_MISC0 & CCM_ANALOG_MISC0_REFTOP_VBGUP) == 0);// wait for stable
	// 15.6.2 XTAL OSC (LP) Control Register
    XTALOSC24M_LOWPWR_CTRL_CLR = XTALOSC24M_LOWPWR_CTRL_LPBG_SEL;   // Bandgap select, disable Low power bandgap
	// 16.6.5 Miscellaneous Register 0
    PMU_MISC0_CLR = 0x00000004;										// REFTOP_LOWPOWER Control bit to disable the low-power mode in the analog bandgap
    while ( CCM_CDHIPR != 0 );										// Wait nothing busy
    for ( int i = 0; i < 625; i++ ) __asm volatile ( "nop \n" );    // Wait ~30µs (F_CPU 40MHz 1 nop ~48ns)
}
//----------------------------------------------------------------------------------
FASTRUN void set_clock_lp_dcdc( void ) {
    uint32_t dcdc = DCDC_REG0 &
    ~(DCDC_REG0_XTAL_24M_OK | DCDC_REG0_DISABLE_AUTO_CLK_SWITCH | DCDC_REG0_SEL_CLK |
      DCDC_REG0_PWD_OSC_INT);
    
    dcdc |= DCDC_REG0_DISABLE_AUTO_CLK_SWITCH;
    DCDC_REG0 = dcdc;
}
//---------------------------------------------------------------------------------
FASTRUN void start_usb_pll( void ) {
    while ( 1 ) {
        uint32_t n = CCM_ANALOG_PLL_USB1;
        if ( n & CCM_ANALOG_PLL_USB1_DIV_SELECT ) {
            // bypass 24 MHz
            CCM_ANALOG_PLL_USB1_CLR = 0xC000;
            CCM_ANALOG_PLL_USB1_SET = CCM_ANALOG_PLL_USB1_BYPASS; // bypass
            CCM_ANALOG_PLL_USB1_CLR = CCM_ANALOG_PLL_USB1_POWER | // power down
            CCM_ANALOG_PLL_USB1_DIV_SELECT |                      // use 480 MHz
            CCM_ANALOG_PLL_USB1_ENABLE |                          // disable
            CCM_ANALOG_PLL_USB1_EN_USB_CLKS;                      // disable usb
            continue;
        }
        if ( !( n & CCM_ANALOG_PLL_USB1_ENABLE ) ) {
            CCM_ANALOG_PLL_USB1_SET = CCM_ANALOG_PLL_USB1_ENABLE;
            continue;
        }
        if ( !( n & CCM_ANALOG_PLL_USB1_POWER ) ) {
            CCM_ANALOG_PLL_USB1_SET = CCM_ANALOG_PLL_USB1_POWER;
            continue;
        }
        if ( !( n & CCM_ANALOG_PLL_USB1_LOCK ) ) {
            
            continue;
        }
        if ( n & CCM_ANALOG_PLL_USB1_BYPASS ) {
            CCM_ANALOG_PLL_USB1_CLR = CCM_ANALOG_PLL_USB1_BYPASS;
            continue;
        }
        if ( !( n & CCM_ANALOG_PLL_USB1_EN_USB_CLKS ) ) {
            CCM_ANALOG_PLL_USB1_SET = CCM_ANALOG_PLL_USB1_EN_USB_CLKS;
            continue;
        }
        break;
    }
}
//---------------------------------------------------------------------------------
FASTRUN void disable_plls( void ) {
	// Saves PLL registers
    pll_sys   = CCM_ANALOG_PLL_SYS;
    pll_usb1  = CCM_ANALOG_PLL_USB1;
    pll_usb2  = CCM_ANALOG_PLL_USB2;
    pll_audio = CCM_ANALOG_PLL_AUDIO;
    pll_video = CCM_ANALOG_PLL_VIDEO;
    pll_enet  = CCM_ANALOG_PLL_ENET;
    pfd_480   = CCM_ANALOG_PFD_480;
    pfd_528   = CCM_ANALOG_PFD_528;
    usbphy1   = USBPHY1_CTRL;
    usbphy2   = USBPHY2_CTRL;

	// 14.8.4 Analog System PLL Control Register
	// if PLL not Powers down 
    if ( !( pll_sys & CCM_ANALOG_PLL_SYS_POWERDOWN ) ) {
        CCM_ANALOG_PLL_SYS_SET = CCM_ANALOG_PLL_SYS_BYPASS;			// Bypass the PLL
        CCM_ANALOG_PLL_SYS_SET = CCM_ANALOG_PLL_SYS_POWERDOWN;		// Powers down the PLL
		// 14.8.16 528MHz Clock (PLL2) Phase Fractional Divider Control Register
		//                                                             33222222222211111111110000000000
		//                                                             10987654321098765432109876543210
        //CCM_ANALOG_PFD_528 |= 0x80808080;							// 10000000100000001000000010000000 Bit 7, 15, 23, 31
		CCM_ANALOG_PFD_528_SET = ((uint32_t)(1 <<  7));             // 07 PFD0_CLKGATE If set to 1, the IO fractional divider clock (reference ref_pfd0) is off (power savings). 0: ref_pfd0 fractional divider clock is enabled. Need to assert this bit before PLL powered down
		CCM_ANALOG_PFD_528_SET = ((uint32_t)(1 << 15)); 			// 15 PFD1_CLKGATE IO Clock Gate. If set to 1, the IO fractional divider clock (reference ref_pfd1) is off (power savings). 0: ref_pfd1 fractional divider clock is enabled. Need to assert this bit before PLL powered down
		CCM_ANALOG_PFD_528_SET = ((uint32_t)(1 << 23)); 			// 23 PFD2_CLKGATE IO Clock Gate. If set to 1, the IO fractional divider clock (reference ref_pfd2) is off (power savings). 0: ref_pfd2 fractional divider clock is enabled. Need to assert this bit before PLL powered down
		CCM_ANALOG_PFD_528_SET = ((uint32_t)(1 << 31)); 			// 31 PFD3_CLKGATE IO Clock Gate. If set to 1, the 3rd fractional divider clock (reference ref_pfd3) is off (power savings). 0: ref_pfd3 fractional divider clock is enabled. Need to assert this bit before PLL powered down
    }
    
	// 14.8.2 Analog USB1 480MHz PLL Control Register
    if ( pll_usb1 & CCM_ANALOG_PLL_USB1_POWER ) {
		CCM_ANALOG_PLL_USB1_SET = CCM_ANALOG_PLL_USB1_BYPASS;		// Bypass the PLL
		CCM_ANALOG_PLL_USB1_CLR = CCM_ANALOG_PLL_USB1_EN_USB_CLKS;	// Powers the 9-phase PLL outputs for USBPHYn. Additionally, the UTMI clock gate must be deasserted in
																	// the USBPHYn to enable USBn operation (clear CLKGATE bit in USBPHYn_CTRL). This bit will be set
																	// automatically when USBPHYn remote wakeup event occurs.
																	// 0 PLL outputs for USBPHYn off. 1 PLL outputs for USBPHYn on.
		CCM_ANALOG_PLL_USB1_CLR = CCM_ANALOG_PLL_USB1_ENABLE;	    // Disable PLL
		CCM_ANALOG_PLL_USB1_CLR = CCM_ANALOG_PLL_USB1_POWER;		// Power down
     }
    
	// 14.8.3 Analog USB2 480MHz PLL Control Register
	// If POWER (Powers up the PLL. This bit will be set automatically when USBPHY1 remote wakeup event happens)
    if ( pll_usb2 & CCM_ANALOG_PLL_USB2_POWER ) {
        CCM_ANALOG_PLL_USB2_SET = CCM_ANALOG_PLL_USB2_BYPASS;		// Bypass the PLL
        CCM_ANALOG_PLL_USB2_CLR = CCM_ANALOG_PLL_USB2_POWER;		// Clear Powers up the PLL. This bit will be set automatically when USBPHY1 remote wakeup event happens
    }
    
	// 14.8.8 Analog Audio PLL control Register
	// If not POWER_DOWN
    if ( ! ( pll_audio & CCM_ANALOG_PLL_AUDIO_POWERDOWN ) ) {
        CCM_ANALOG_PLL_AUDIO_SET = CCM_ANALOG_PLL_AUDIO_BYPASS;		// Bypass the PLL
        CCM_ANALOG_PLL_AUDIO_SET = CCM_ANALOG_PLL_AUDIO_POWERDOWN;	// Powers down the PLL
    }
    
	// 14.8.11 Analog Video PLL control Register
	// If not POWER_DOWN
    if ( ! ( pll_video & CCM_ANALOG_PLL_VIDEO_POWERDOWN ) ) {
        CCM_ANALOG_PLL_VIDEO_SET = CCM_ANALOG_PLL_VIDEO_BYPASS;		// Bypass the PLL
        CCM_ANALOG_PLL_VIDEO_SET = CCM_ANALOG_PLL_VIDEO_POWERDOWN;	// Powers down the PLL
    }
    
	// 14.8.14 Analog ENET PLL Control Register
	// If not POWER_DOWN
    if ( ! ( pll_enet & CCM_ANALOG_PLL_ENET_POWERDOWN ) ) {
        CCM_ANALOG_PLL_ENET_SET = CCM_ANALOG_PLL_ENET_BYPASS;		// Bypass the PLL
        CCM_ANALOG_PLL_ENET_SET = CCM_ANALOG_PLL_ENET_POWERDOWN;	// Powers down the PLL
    }
}

FASTRUN void enable_plls( void ) {
    //CCM_ANALOG_PLL_ARM_CLR = CCM_ANALOG_PLL_ARM_POWERDOWN;
    //CCM_ANALOG_PLL_ARM_SET = CCM_ANALOG_PLL_ARM_ENABLE | CCM_ANALOG_PLL_ARM_BYPASS;

 	// 14.8.4 Analog System PLL Control Register
	// if PLL not Powers down before sleep
    if ( !( pll_sys & CCM_ANALOG_PLL_SYS_POWERDOWN ) ) {
        CCM_ANALOG_PLL_SYS_CLR = CCM_ANALOG_PLL_SYS_POWERDOWN;		// Clear POWERDOWN so Power On the PLL
        CCM_ANALOG_PLL_SYS_CLR = CCM_ANALOG_PLL_SYS_BYPASS;			// Clear Bypass the PLL
        CCM_ANALOG_PLL_SYS = pll_sys;								// Restore Analog System PLL Control Register
        while ( ( CCM_ANALOG_PLL_SYS & CCM_ANALOG_PLL_SYS_LOCK ) == 0 );	// Wait PLL is currently locked
		// 14.8.16 528MHz Clock (PLL2) Phase Fractional Divider Control Register
		// 13.3.2.2.1 General PLL Control and Status Functions p982
		// When the related PLL is powered up from the power down state or made to go through a
		// relock cycle due to PLL reprogramming, it is required that the related PFDx_CLKGATE
		// bit in CCM_ANALOG_PFD_480n or CCM_ANALOG_PFD_528n, be cycled on and off
		// (1 to 0) after PLL lock. The PFDs can be in the clock gated state during PLL relock but
		// must be clock un-gated only after lock is achieved. See the engineering bulletin,
		// Configurati	on of Phase Fractional Dividers (EB790) at www.nxp.com for procedure details.
		CCM_ANALOG_PFD_528_CLR = ((uint32_t)(1 <<  7));             // 07 PFD0_CLKGATE If set to 1, the IO fractional divider clock (reference ref_pfd0) is off (power savings). 0: ref_pfd0 fractional divider clock is enabled. Need to assert this bit before PLL powered down
		CCM_ANALOG_PFD_528_CLR = ((uint32_t)(1 << 15)); 			// 15 PFD1_CLKGATE IO Clock Gate. If set to 1, the IO fractional divider clock (reference ref_pfd1) is off (power savings). 0: ref_pfd1 fractional divider clock is enabled. Need to assert this bit before PLL powered down
		CCM_ANALOG_PFD_528_CLR = ((uint32_t)(1 << 23)); 			// 23 PFD2_CLKGATE IO Clock Gate. If set to 1, the IO fractional divider clock (reference ref_pfd2) is off (power savings). 0: ref_pfd2 fractional divider clock is enabled. Need to assert this bit before PLL powered down
		CCM_ANALOG_PFD_528_CLR = ((uint32_t)(1 << 31)); 			// 31 PFD3_CLKGATE IO Clock Gate. If set to 1, the 3rd fractional divider clock (reference ref_pfd3) is off (power savings). 0: ref_pfd3 fractional divider clock is enabled. Need to assert this bit before PLL powered down
        CCM_ANALOG_PFD_528 = pfd_528;								// Restore 528MHz Clock (PLL2) Phase Fractional Divider Control Register
     }

     
	// 14.8.3 Analog USB2 480MHz PLL Control Register
	// If POWER before sleep
    if ( pll_usb2 & CCM_ANALOG_PLL_USB2_POWER ) {
        CCM_ANALOG_PLL_USB2_SET = CCM_ANALOG_PLL_USB2_POWER;		// Set POWER so Power On the PLL
        CCM_ANALOG_PLL_USB2_CLR = CCM_ANALOG_PLL_USB2_BYPASS;		// Clear Bypass the PLL
        CCM_ANALOG_PLL_USB2  = pll_usb2;							// Restore Analog USB2 480MHz PLL Control Register
        while ( ( CCM_ANALOG_PLL_USB2 & CCM_ANALOG_PLL_USB2_LOCK ) == 0 );	// Wait PLL is currently locked
		// 14.8.15 480MHz Clock (PLL3) Phase Fractional Divider Control Register
        CCM_ANALOG_PFD_480 = pfd_480;								// Restore 480MHz Clock (PLL3) Phase Fractional Divider Control Register
		// 43.4.4 USB PHY General Control Register
        USBPHY2_CTRL = usbphy2;                                     // Restore USB2 PHY General Control Register
   }
    
	// 14.8.8 Analog Audio PLL control Register
	// If not POWER_DOWN before sleep
    if ( !( pll_audio & CCM_ANALOG_PLL_AUDIO_POWERDOWN ) ) {
        CCM_ANALOG_PLL_AUDIO_CLR = CCM_ANALOG_PLL_AUDIO_POWERDOWN;	// Clear POWERDOWN so Power On the PLL
        CCM_ANALOG_PLL_AUDIO_CLR = CCM_ANALOG_PLL_AUDIO_BYPASS;		// Clear Bypass the PLL
        CCM_ANALOG_PLL_AUDIO = pll_audio;							// Restore Analog Audio PLL control Register
        while ( ( CCM_ANALOG_PLL_AUDIO & CCM_ANALOG_PLL_AUDIO_LOCK ) == 0 );	// Wait PLL is currently locked
    }
    
	// 14.8.11 Analog Video PLL control Register
	// If not POWER_DOWN before sleep
    if ( !( pll_video & CCM_ANALOG_PLL_VIDEO_POWERDOWN ) ) {
        CCM_ANALOG_PLL_VIDEO_CLR = CCM_ANALOG_PLL_VIDEO_POWERDOWN;	// Clear POWERDOWN so Power On the PLL
        CCM_ANALOG_PLL_VIDEO_CLR = CCM_ANALOG_PLL_VIDEO_BYPASS;		// Clear Bypass the PLL
        CCM_ANALOG_PLL_VIDEO = pll_video;							// Restore Analog Video PLL control Register
        while ( ( CCM_ANALOG_PLL_VIDEO & CCM_ANALOG_PLL_VIDEO_LOCK ) == 0 );	// Wait PLL is currently locked
    }
    
	// 14.8.14 Analog ENET PLL Control Register
	// If not POWER_DOWN before sleep
    if ( !( pll_enet & CCM_ANALOG_PLL_ENET_POWERDOWN ) ) {
        CCM_ANALOG_PLL_ENET_CLR = CCM_ANALOG_PLL_ENET_POWERDOWN;	// Clear POWERDOWN so Power On the PLL
        CCM_ANALOG_PLL_ENET_CLR = CCM_ANALOG_PLL_ENET_BYPASS;		// Clear Bypass the PLL
        CCM_ANALOG_PLL_ENET = pll_enet;
        while ( ( CCM_ANALOG_PLL_ENET & CCM_ANALOG_PLL_ENET_LOCK ) == 0 );		// Wait PLL is currently locked
    }
    
    if ( pll_usb1 & CCM_ANALOG_PLL_USB1_POWER ) {
        CCM_ANALOG_PLL_USB1_SET = CCM_ANALOG_PLL_USB1_POWER;		// Set POWER so Power On the PLL
		CCM_ANALOG_PLL_USB1_SET = CCM_ANALOG_PLL_USB1_ENABLE;	    // Enable PLL
        CCM_ANALOG_PLL_USB1_CLR = CCM_ANALOG_PLL_USB1_BYPASS;		// Clear Bypass the PLL
        while ( ( CCM_ANALOG_PLL_USB1 & CCM_ANALOG_PLL_USB1_LOCK ) == 0 );	// Wait PLL is currently locked
		CCM_ANALOG_PLL_USB1_SET = CCM_ANALOG_PLL_USB1_EN_USB_CLKS;  // Enable usb
		// 14.8.15 480MHz Clock (PLL3) Phase Fractional Divider Control Register
        CCM_ANALOG_PFD_480 = pfd_480;								// Restore 480MHz Clock (PLL3) Phase Fractional Divider Control Register
		// 43.4.4 USB PHY General Control Register
        USBPHY1_CTRL = usbphy1;                                     // Restore USB1 PHY General Control Register
    }
}
//----------------------------------------------------------------------------------
void wakeup_isr( void ) {
// Begin Jean-Do.
#ifdef SNOOZE_WITH_RESET
	int iCpt = SRC_GPR5 & 0xfff0000;
	SRC_GPR5 = iCpt | UNKNOW_ISR; // By default, unknow ISR
#endif
    int source = -1;
    uint32_t ipsr;
    __asm__ volatile( "mrs %0, ipsr\n" : "=r" ( ipsr ):: );
    switch ( ipsr - 16 ) {
        case IRQ_SNVS_ONOFF: {
            source = ALARM_WAKE;
            break;
        }
        case IRQ_SNVS_SECURITY: {
            source = ALARM_WAKE;
            break;
        }
        case IRQ_SNVS_IRQ: {
            source = ALARM_WAKE;
            break;
        }
        case IRQ_ACMP1: {
            source = COMPARE_WAKE;
            break;
        }
        case IRQ_ACMP2: {
            source = COMPARE_WAKE;
            break;
        }
        case IRQ_ACMP3: {
            source = COMPARE_WAKE;
            break;
        }
        case IRQ_ACMP4: {
            source = COMPARE_WAKE;
            break;
        }
        case IRQ_GPT1: {
            source = TIMER_WAKE;
            break;
        }
        case IRQ_GPIO1_0_15:
        case IRQ_GPIO1_16_31:
        case IRQ_GPIO2_0_15:
        case IRQ_GPIO2_16_31:
        case IRQ_GPIO3_0_15:
        case IRQ_GPIO3_16_31:
        case IRQ_GPIO4_0_15:
        case IRQ_GPIO4_16_31: {
            uint32_t isfr_1 = GPIO1_ISR & GPIO1_IMR;
            uint32_t isfr_2 = GPIO2_ISR & GPIO2_IMR;
            uint32_t isfr_3 = GPIO3_ISR & GPIO3_IMR;
            uint32_t isfr_4 = GPIO4_ISR & GPIO4_IMR;
            
            if ( isfr_1 & CORE_PIN0_BITMASK ) source = 0;
            else if ( isfr_1 & CORE_PIN1_BITMASK ) source = 1;
            else if ( isfr_1 & CORE_PIN14_BITMASK ) source = 14;
            else if ( isfr_1 & CORE_PIN15_BITMASK ) source = 15;
            else if ( isfr_1 & CORE_PIN16_BITMASK ) source = 16;
            else if ( isfr_1 & CORE_PIN17_BITMASK ) source = 17;
            else if ( isfr_1 & CORE_PIN18_BITMASK ) source = 18;
            else if ( isfr_1 & CORE_PIN19_BITMASK ) source = 19;
            else if ( isfr_1 & CORE_PIN20_BITMASK ) source = 20;
            else if ( isfr_1 & CORE_PIN21_BITMASK ) source = 21;
            else if ( isfr_1 & CORE_PIN22_BITMASK ) source = 22;
            else if ( isfr_1 & CORE_PIN23_BITMASK ) source = 23;
            else if ( isfr_1 & CORE_PIN24_BITMASK ) source = 24;
            else if ( isfr_1 & CORE_PIN25_BITMASK ) source = 25;
            else if ( isfr_1 & CORE_PIN26_BITMASK ) source = 26;
            else if ( isfr_1 & CORE_PIN27_BITMASK ) source = 27;
            else if ( isfr_1 & CORE_PIN38_BITMASK ) source = 38;
            else if ( isfr_1 & CORE_PIN39_BITMASK ) source = 39;
            //else if ( isfr_6 & CORE_PIN40_BITMASK ) source = 40;
            //else if ( isfr_6 & CORE_PIN41_BITMASK ) source = 41;
            else if ( isfr_2 & CORE_PIN6_BITMASK ) source = 6;
            else if ( isfr_2 & CORE_PIN7_BITMASK ) source = 7;
            else if ( isfr_2 & CORE_PIN8_BITMASK ) source = 8;
            else if ( isfr_2 & CORE_PIN9_BITMASK ) source = 9;
            else if ( isfr_2 & CORE_PIN10_BITMASK ) source = 10;
            else if ( isfr_2 & CORE_PIN11_BITMASK ) source = 11;
            else if ( isfr_2 & CORE_PIN12_BITMASK ) source = 12;
            else if ( isfr_2 & CORE_PIN13_BITMASK ) source = 13;
            else if ( isfr_2 & CORE_PIN32_BITMASK ) source = 32;
            else if ( isfr_2 & CORE_PIN33_BITMASK ) source = 33;
            else if ( isfr_2 & CORE_PIN34_BITMASK ) source = 34;
            else if ( isfr_2 & CORE_PIN35_BITMASK ) source = 35;
            else if ( isfr_2 & CORE_PIN36_BITMASK ) source = 36;
            else if ( isfr_2 & CORE_PIN37_BITMASK ) source = 37;
            else if ( isfr_3 & CORE_PIN28_BITMASK ) source = 28;
            else if ( isfr_3 & CORE_PIN30_BITMASK ) source = 30;
            else if ( isfr_3 & CORE_PIN31_BITMASK ) source = 31;
            //else if ( isfr_3 & CORE_PIN42_BITMASK ) source = 42;
            //else if ( isfr_3 & CORE_PIN43_BITMASK ) source = 43;
            //else if ( isfr_3 & CORE_PIN44_BITMASK ) source = 44;
            //else if ( isfr_3 & CORE_PIN45_BITMASK ) source = 45;
            //else if ( isfr_3 & CORE_PIN46_BITMASK ) source = 46;
            else if ( isfr_4 & CORE_PIN2_BITMASK ) source = 2;
            else if ( isfr_4 & CORE_PIN3_BITMASK ) source = 3;
            else if ( isfr_4 & CORE_PIN4_BITMASK ) source = 4;
            else if ( isfr_4 & CORE_PIN5_BITMASK ) source = 5;
            else if ( isfr_4 & CORE_PIN29_BITMASK ) source = 29;
            else if ( isfr_4 & CORE_PIN33_BITMASK ) source = 33;
            break;
        }
            
        case IRQ_GPIO6789: {
            uint32_t isfr_6 = GPIO6_ISR & GPIO6_IMR;
            uint32_t isfr_7 = GPIO7_ISR & GPIO7_IMR;
            uint32_t isfr_8 = GPIO8_ISR & GPIO8_IMR;
            uint32_t isfr_9 = GPIO9_ISR & GPIO9_IMR;
            
            if ( isfr_6 & CORE_PIN0_BITMASK ) source = 0;
            else if ( isfr_6 & CORE_PIN1_BITMASK ) source = 1;
            else if ( isfr_6 & CORE_PIN14_BITMASK ) source = 14;
            else if ( isfr_6 & CORE_PIN15_BITMASK ) source = 15;
            else if ( isfr_6 & CORE_PIN16_BITMASK ) source = 16;
            else if ( isfr_6 & CORE_PIN17_BITMASK ) source = 17;
            else if ( isfr_6 & CORE_PIN18_BITMASK ) source = 18;
            else if ( isfr_6 & CORE_PIN19_BITMASK ) source = 19;
            else if ( isfr_6 & CORE_PIN20_BITMASK ) source = 20;
            else if ( isfr_6 & CORE_PIN21_BITMASK ) source = 21;
            else if ( isfr_6 & CORE_PIN22_BITMASK ) source = 22;
            else if ( isfr_6 & CORE_PIN23_BITMASK ) source = 23;
            else if ( isfr_6 & CORE_PIN24_BITMASK ) source = 24;
            else if ( isfr_6 & CORE_PIN25_BITMASK ) source = 25;
            else if ( isfr_6 & CORE_PIN26_BITMASK ) source = 26;
            else if ( isfr_6 & CORE_PIN27_BITMASK ) source = 27;
            else if ( isfr_6 & CORE_PIN38_BITMASK ) source = 38;
            else if ( isfr_6 & CORE_PIN39_BITMASK ) source = 39;
            //else if ( isfr_6 & CORE_PIN40_BITMASK ) source = 40;
            //else if ( isfr_6 & CORE_PIN41_BITMASK ) source = 41;
            else if ( isfr_7 & CORE_PIN6_BITMASK ) source = 6;
            else if ( isfr_7 & CORE_PIN7_BITMASK ) source = 7;
            else if ( isfr_7 & CORE_PIN8_BITMASK ) source = 8;
            else if ( isfr_7 & CORE_PIN9_BITMASK ) source = 9;
            else if ( isfr_7 & CORE_PIN10_BITMASK ) source = 10;
            else if ( isfr_7 & CORE_PIN11_BITMASK ) source = 11;
            else if ( isfr_7 & CORE_PIN12_BITMASK ) source = 12;
            else if ( isfr_7 & CORE_PIN13_BITMASK ) source = 13;
            else if ( isfr_7 & CORE_PIN32_BITMASK ) source = 32;
            else if ( isfr_7 & CORE_PIN33_BITMASK ) source = 33;
            else if ( isfr_7 & CORE_PIN34_BITMASK ) source = 34;
            else if ( isfr_7 & CORE_PIN35_BITMASK ) source = 35;
            else if ( isfr_7 & CORE_PIN36_BITMASK ) source = 36;
            else if ( isfr_7 & CORE_PIN37_BITMASK ) source = 37;
            else if ( isfr_8 & CORE_PIN28_BITMASK ) source = 28;
            else if ( isfr_8 & CORE_PIN30_BITMASK ) source = 30;
            else if ( isfr_8 & CORE_PIN31_BITMASK ) source = 31;
            //else if ( isfr_8 & CORE_PIN42_BITMASK ) source = 42;
            //else if ( isfr_8 & CORE_PIN43_BITMASK ) source = 43;
            //else if ( isfr_8 & CORE_PIN44_BITMASK ) source = 44;
            //else if ( isfr_8 & CORE_PIN45_BITMASK ) source = 45;
            //else if ( isfr_8 & CORE_PIN46_BITMASK ) source = 46;
            else if ( isfr_9 & CORE_PIN2_BITMASK ) source = 2;
            else if ( isfr_9 & CORE_PIN3_BITMASK ) source = 3;
            else if ( isfr_9 & CORE_PIN4_BITMASK ) source = 4;
            else if ( isfr_9 & CORE_PIN5_BITMASK ) source = 5;
            else if ( isfr_9 & CORE_PIN29_BITMASK ) source = 29;
            else if ( isfr_9 & CORE_PIN33_BITMASK ) source = 33;
			//digitalWriteFast( 13, HIGH);
            break;
        }
        default:
            source = -1;
            break;
    }
	if (source == -1)
		source = UNKNOW_ISR;
// Begin Jean-Do.
#ifdef SNOOZE_WITH_RESET
	if (source != -1)
	{
		//digitalWriteFast( 13, HIGH);
		int iCpt = SRC_GPR5 & 0xfff0000;
		SRC_GPR5 = iCpt | source; // We memorize the source of the wake up in SRC_GPR5
		if (source == 2 || source == 3 || source == 4 || source == 5 || source == 29 || source == 33)
		{
			// GPIO9_ISR does not wake up the CPU !
			// Reset CPU
			SCB_AIRCR = 0x05FA0004;
			while (1) ;
		}
	}
#endif
// End Jean-Do.
    clear_flags( ipsr );
    wake_source = source;
//digitalWrite(LED_BUILTIN, HIGH);
}
//----------------------------------------------------------------------------------
//void save_context( uint32_t *common, uint32_t *special );
//void save_context( uint32_t *common, uint32_t *special ) {
void save_context( void ) {
    //volatile uint32_t *common  = &s_common_regs_context_buf;
    //volatile uint32_t *special = &s_special_regs_context_buf;
    /*__asm__ volatile (
     "mov r0, %[com] \n"
     "stmia r0,{r4-r12, lr}"
     :  [com] "+r" ( common ) // outputs
     :  // inputs
     :  // clobbers
     );
     
     __asm__ volatile (
     "mov r1, %[spec] \n"
     "mrs r2, msp \n"
     "mrs r3, psp \n"
     "mrs r4, primask \n"
     "mrs r5, control \n"
     "stmia r1,{r2-r5}"
     :  [spec] "+r" ( special ) // outputs
     :  // inputs
     :  "r2", "r3", "r4", "r5"// clobbers
     );*/
}
//----------------------------------------------------------------------------------
void load_context( void ) {
    //volatile uint32_t *common  = &s_common_regs_context_buf;
    //volatile uint32_t *special = &s_special_regs_context_buf;
    /*__asm__ volatile (
     "mov r0, %[com] \n"
     "ldmia r0,{r4-r12, lr}"
     :  // outputs
     :  [com] "r" ( common ) // inputs
     :  // clobbers
     );
     
     __asm__ volatile (
     "mov   r1, %[spec] \n"
     "ldmia r1,{r2-r5} \n"
     "msr   msp, r2 \n"
     "msr   psp, r3 \n"
     "msr   primask, r4 \n"
     "msr   control, r5 \n"
     :  // outputs
     :  [spec] "r" ( special ) // inputs
     :  "r2", "r3", "r4", "r5"// clobbers
     );*/
}
//----------------------------------------------------------------------------------
void start_up( void ) {
    /*__asm__ volatile( "LDR     R1, =gpSpecRegsContextStackBottom" );
     __asm__ volatile( "LDR     R0, [R1]" );
     __asm__ volatile( "LDMIA   R0!, {R2-R5}" );
     __asm__ volatile( "MSR     MSP, R2" );
     __asm__ volatile( "MSR     PSP, R3" );
     __asm__ volatile( "MSR     PRIMASK, R4" );
     __asm__ volatile( "MSR     CONTROL, R5" );
     __asm__ volatile( "LDR     R1, =gpComRegsContextStackBottom" );
     __asm__ volatile( "LDR R0, [R1]" );
     __asm__ volatile( "LDMIA   R0!, {R4-R12, LR}" );
     __asm__ volatile( "MOV R0, LR" );
     __asm__ volatile( "ADD     LR, LR, #12" );
     __asm__ volatile( "MOV PC, LR" );*/
}
//----------------------------------------------------------------------------------
/*FLASHMEM void startup_early_hook( void ) {
    uint32_t OR_D_GPR = IOMUXC_GPR_GPR4 | IOMUXC_GPR_GPR7 | IOMUXC_GPR_GPR8 | IOMUXC_GPR_GPR12;
    if ( OR_D_GPR > 0x0 ) {
		IOMUXC_GPR_GPR1 &= ~IOMUXC_GPR_GPR1_GINT;
        //GPIO1_ISR = 0;//0xFFFFFFFF;
        // GPIO2_ISR = 0;//0xFFFFFFFF;
        // GPIO3_ISR = 0;//0xFFFFFFFF;
        // GPIO4_ISR = 0;//0xFFFFFFFF;
        // GPIO5_ISR = 0xFFFFFFFF;
        // GPIO6_ISR = 0xFFFFFFFF;
        // GPIO7_ISR = 0xFFFFFFFF;
        // GPIO8_ISR = 0xFFFFFFFF;
        // GPIO9_ISR = 0xFFFFFFFF;
        SNVS_LPSR = 0xffffffff;
        // recover handshaking
        IOMUXC_GPR_GPR4  = 0x00000000;
        IOMUXC_GPR_GPR7  = 0x00000000;
        IOMUXC_GPR_GPR8  = 0x00000000;
        IOMUXC_GPR_GPR12 = 0x00000000;
        //load_context( );
        //start_up( );
    }
//	digitalWrite(LED_BUILTIN, HIGH);
    
    //
}*/
//----------------------------------------------------------------------------------
void hal_initialize( void ( * ptr )( uint32_t ) ) {
    clear_flags = ptr;
}
//----------------------------------------------------------------------------------
void hal_idle ( void ) {
    
}
//----------------------------------------------------------------------------------
int source ( void ) {
    return wake_source;
}
//----------------------------------------------------------------------------------
int hal_sleep ( void ) {
// Begin Jean-Do.
#ifdef SNOOZE_WITH_RESET
	int iCpt = SRC_GPR5 & 0xfff0000;
	SRC_GPR5 = iCpt | UNKNOW_WAKE; // By default, unknow wake up source
#endif
// End Jean-Do.
 	// *********************************
	// **** Go to deep sleep mode
	// *********************************
    uint32_t f_cpu = F_CPU_ACTUAL;				// Stores the current CPU frequency
// A - System Control Register
	// B3.2.7 System Control Register, SCR
    SCB_SCR |= SCB_SCR_SLEEPDEEP;				// Put SLEEPDEEP bit ON (Provides a qualifying hint indicating that waking from sleep might take longer)
	// SysTick Control and Status Register
    SYST_CSR &= ~SYST_CSR_TICKINT;
// B - IOMUXC_GPR_GPR 1 register
	// 11.3.2 GPR1 General Purpose Register
    IOMUXC_GPR_GPR1 |= IOMUXC_GPR_GPR1_GINT;	// Gets ENET2 TX reference clock from the ENET2_TX_CLK pin. In this use case, an external OSC provides the clock for both the external PHY and the internal controll
    GPC_IMR2 &= ~0x200;
// C - CCM Low Power Control Register
	// 14.7.16 CCM Low Power Control Register
	uint32_t MemClpcr = CCM_CLPCR;
    uint32_t clpcr = CCM_CLPCR & ( ~( 0x03 | CCM_CLPCR_ARM_CLK_DIS_ON_LPM ) );
    clpcr = CCM_CLPCR 
		| CCM_CLPCR_LPM( 0x01 ) 				// Transfer to wait mode
		| CCM_CLPCR_ARM_CLK_DIS_ON_LPM 			// Arm clock disabled on wait mode
		| CCM_CLPCR_STBY_COUNT( 0x03 ) 			// Standby counter definition, CCM will wait (15*pmic_delay_scaler)+1 ckil clock cycles
		| 0x1C 									// Bits 2, 3 and 4, This field is reserved ???
		| 0x08280000;							// Bit 19, BYPASS_LPM_HS1, Bypass low power mode handshake
		                                        // Bit 21, BYPASS_LPM_HS0, Bypass low power mode handshake
                                                // Bit 27, MASK_L2CC_IDLE, Mask L2CC IDLE for entering low power mode
    CCM_CLPCR = clpcr;
    GPC_IMR2 |= 0x200;							// Enable IRQ bit 9
// D - CCM Clock Gating Register 0 to 6
	// 14.7.21 CCM Clock Gating Register 0 to 14.7.27 CCM Clock Gating Register 6
    uint32_t ccgr0 = CCM_CCGR0;					// Saves registers CCM_CCGR0 to CCM_CCGR6 for return to normal mode
    uint32_t ccgr1 = CCM_CCGR1;
    uint32_t ccgr2 = CCM_CCGR2;
    uint32_t ccgr3 = CCM_CCGR3;
    uint32_t ccgr4 = CCM_CCGR4;
    uint32_t ccgr5 = CCM_CCGR5;
    uint32_t ccgr6 = CCM_CCGR6;
	// These bits are used to turn on/off the clock to each module independently. The following
	// table details the possible clock activity conditions for each module
	// 00 Clock is off during all modes. Stop enter hardware handshake is disabled.
	// 01 Clock is on in run mode, but off in WAIT and STOP modes
	// 10 Not applicable (Reserved).
	// 11 Clock is on during all modes, except STOP mode.
    CCM_CCGR0 = 0xC5;	// 1100 0101			// aips_tz1 and aips_tz2 Clock is on in run mode, but off in WAIT and STOP modes
                                                // mqs clock Clock is off during all modes
                                                // bit 6-7 Reserved ??? Clock is on during all modes, except STOP mode									
    CCM_CCGR1 = 0 | ( CCM_CCGR1 & ( 
		  CCM_CCGR1_GPT1_SERIAL( 3 )			// gpt1 serial clock, Clock is on during all modes, except STOP mode
		| CCM_CCGR1_GPT1_BUS( 3 ) ) );			// gpt1 bus clock, Clock is on during all modes, except STOP mode
    CCM_CCGR2 = 0x3     // 0011                 // ocram_exsc Clock is on during all modes, except STOP mode
	 | ( CCM_CCGR2 & CCM_CCGR2_IOMUXC_SNVS(3));	// lpi2c2 Clock is on during all modes, except STOP mode
    CCM_CCGR3 = 0x10000000 						// lpuart6 Not applicable ???
	 | ( CCM_CCGR3 & ( CCM_CCGR3_ACMP1( 3 )		// acmp1 Clock is on during all modes, except STOP mode
	                 | CCM_CCGR3_ACMP2( 3 )		// acmp2 Clock is on during all modes, except STOP mode
					 | CCM_CCGR3_ACMP3( 3 )		// acmp3 Clock is on during all modes, except STOP mode
					 | CCM_CCGR3_ACMP4( 3 )));	// acmp4 Clock is on during all modes, except STOP mode
    CCM_CCGR4 = 0x300;  // 0011 0000 0000		// sim_m7 Clock is on during all modes, except STOP mode
    CCM_CCGR5 = 0x50001105; // 0101 0000 0000 0000 0001 0001 0000 0101
												// 01, rom, flexio1, kpp clock, aipstz4, snvs_hp, snvs_lp, Clock is on in run mode, but off in WAIT and STOP modes
												// 00, wdog3, dma, wdog2, spdif, sim_main, sai1, sai2, sai3, lpuart1, lpuart7, Clock is off during all modes, except STOP mode
    // We can enable DCDC when need to config it and close it after configuration
    CCM_CCGR6 = 0x400000;   // 0000 0000 0100 0000 0000 0000 0000 0000
												// 01, anadig, Clock is on in run mode, but off in WAIT and STOP modes
												// 00, usboh3, usdhc1, usdhc2, dcdc, ipmux4, flexspi, trng, lpuart8, timer4, aips_tz3, sim_per, lpi2c4, timer1, timer2, timer3, Clock is off during all modes, except STOP mode
												// flexspi NOTE: sim_ems_clk_enable must also be cleared, when flexspi_clk_enable is cleared ???
// E - disable_plls, to do BEFORE set_arm_clock
    disable_plls( );
    
// F - Set ARM clock to 132MHz
    set_arm_clock( 132000000 );
    
// G - DCDC Register 1 and 0
	// 18.5.1.3 DCDC Register 1
    DCDC_REG1 &= ~DCDC_REG1_REG_RLOAD_SW;		// 0 This controls the load resistor of the internal regulator of DCDC. Load resistor disconnected   
    
// H - DCDC Register 0
    // 18.5.1.2 DCDC Register 0
	uint32_t MemDCDCReg0 = DCDC_REG0;
    uint32_t reg0 = DCDC_REG0 & ~( 
		DCDC_REG0_XTAL_24M_OK					// 0 DCDC uses internal ring OSC
	  | DCDC_REG0_DISABLE_AUTO_CLK_SWITCH		// 0 If DISABLE_AUTO_CLK_SWITCH is set to 0 and 24M xtal is OK, the clock source will switch from internal ring OSC to 24M xtal automatically
	  | DCDC_REG0_SEL_CLK						// 0 DCDC uses internal ring oscillator
	  |	DCDC_REG0_PWD_OSC_INT );				// 0 Internal oscillator powered up
    reg0 |= DCDC_REG0_DISABLE_AUTO_CLK_SWITCH   // 1 If DISABLE_AUTO_CLK_SWITCH is set to 1, SEL_CLK will determine which clock source the DCDC uses
	     |  DCDC_REG0_SEL_CLK					// 1 DCDC uses 24M xtal
		 |  DCDC_REG0_PWD_OSC_INT;				// 1 Internal oscillator powered down
    DCDC_REG0 = reg0;
    
// I - IOMUXC_GPR_GPR 8 and 12 registers
	// 11.3.9 GPR8 General Purpose Register
	uint32_t MemIOMuxGPR8 = IOMUXC_GPR_GPR8;
    IOMUXC_GPR_GPR8  = 0xaaaaaaaa;				// Put in doze mode LPI2C1, LPI2C2, LPI2C3, LPI2C4, LPSPI1, LPSPI2, LPSPI3, LPSPI4, LPUART1, LPUART2, LPUART3, LPUART4, LPUART5, LPUART6, LPUART7, LPUART8
	// 11.3.13 GPR12 General Purpose Register
	uint32_t MemIOMuxGPR12 = IOMUXC_GPR_GPR12;
    IOMUXC_GPR_GPR12 = 0x0000002a;      		// Put in doze mode FLEXIO1, FLEXIO2 and FlexIO3 in stop mode (??? FlexIO3 in doze mode)
    
    __asm volatile ( "dsb \n" );							// Data Synchronization Barrier, Ensure visibility of the data cleaned from the cache
    __asm volatile ( "wfi \n" );							// Wait For Interrupt is a hint instruction. It suspends execution, in the lowest power state available consistent
	// CPU is dormant waiting for an interrupt
    __asm volatile ( "isb \n" );                            // Instruction Synchronization Barrier, Synchronize fetched instruction stream

	// *********************************
	// **** Go to normal mode
	// *********************************
    
// Begin Jean-Do.
#ifdef SNOOZE_WITH_RESET
    // Reset CPU
    SCB_AIRCR = 0x05FA0004;
    while (1) ;
#endif
// End Jean-Do.
    
//digitalWriteFast(LED_BUILTIN, HIGH);
// C - CCM Low Power Control Register
	// 14.7.16 CCM Low Power Control Register
    //CCM_CLPCR = 0x78;
	CCM_CLPCR = MemClpcr;

// D - CCM Clock Gating Register 0 to 6
	// 14.7.21 CCM Clock Gating Register 0 to 14.7.27 CCM Clock Gating Register 6
    CCM_CCGR0 = ccgr0;										// Reload the stored values
    CCM_CCGR1 = ccgr1;
    CCM_CCGR2 = ccgr2;
    CCM_CCGR3 = ccgr3;
    CCM_CCGR4 = ccgr4;
    CCM_CCGR5 = ccgr5;
    CCM_CCGR6 = ccgr6;
    
// G - DCDC Register 1 and 0
	// 18.5.1.3 DCDC Register 1
    DCDC_REG1 |= DCDC_REG1_REG_RLOAD_SW;		// 1 This controls the load resistor of the internal regulator of DCDC. Load resistor connected 
    
// I - IOMUXC_GPR_GPR 8 and 12 registers
	// 11.3.9 GPR8 General Purpose Register
	IOMUXC_GPR_GPR8  = MemIOMuxGPR8;
	// 11.3.13 GPR12 General Purpose Register
	IOMUXC_GPR_GPR12 = MemIOMuxGPR12;
    
// H - DCDC Register 0
    // 18.5.1.2 DCDC Register 0
	DCDC_REG0 = MemDCDCReg0;
    
// E - disable_plls, to do BEFORE set_arm_clock
    enable_plls( );
    
// F - Set ARM clock to F_CPU
    set_arm_clock( f_cpu );

// A - System Control Register
	// B3.2.7 System Control Register, SCR
    SCB_SCR &= ~SCB_SCR_SLEEPDEEP;				// Put SLEEPDEEP bit OFF (Provides a qualifying hint indicating that waking from sleep might take longer)
 	// SysTick Control and Status Register
    SYST_CSR |= SYST_CSR_TICKINT;

    return wake_source;
}
//----------------------------------------------------------------------------------
int hal_deepSleep ( void ) {
// Begin Jean-Do.
#ifdef SNOOZE_WITH_RESET
	int iCpt = SRC_GPR5 & 0xfff0000;
	SRC_GPR5 = iCpt | UNKNOW_WAKE; // By default, unknow wake up source
#endif
// End Jean-Do.
	// *********************************
	// **** Go to deep sleep mode
	// *********************************
    uint32_t f_cpu = F_CPU_ACTUAL;				// Stores the current CPU frequency
// A - System Control Register
	// B3.2.7 System Control Register, SCR
    SCB_SCR |= SCB_SCR_SLEEPDEEP;				// Put SLEEPDEEP bit ON (Provides a qualifying hint indicating that waking from sleep might take longer)
	// SysTick Control and Status Register
    SYST_CSR &= ~SYST_CSR_TICKINT;
// B - IOMUXC_GPR_GPR 8 and 12 registers
	// 11.3.9 GPR8 General Purpose Register
	uint32_t MemIOMuxGPR8 = IOMUXC_GPR_GPR8;
    IOMUXC_GPR_GPR8  = 0xaaaaaaaa;				// Put in doze mode LPI2C1, LPI2C2, LPI2C3, LPI2C4, LPSPI1, LPSPI2, LPSPI3, LPSPI4, LPUART1, LPUART2, LPUART3, LPUART4, LPUART5, LPUART6, LPUART7, LPUART8
	// 11.3.13 GPR12 General Purpose Register
	uint32_t MemIOMuxGPR12 = IOMUXC_GPR_GPR12;
    IOMUXC_GPR_GPR12 = 0x0000002a;      		// Put in doze mode FLEXIO1, FLEXIO2 and FlexIO3 in stop mode (??? FlexIO3 in doze mode)
// C - IOMUXC_GPR_GPR 1 register
	// 11.3.2 GPR1 General Purpose Register
    IOMUXC_GPR_GPR1 |= IOMUXC_GPR_GPR1_GINT;	// Gets ENET2 TX reference clock from the ENET2_TX_CLK pin. In this use case, an external OSC provides the clock for both the external PHY and the internal controll
	// 17.6.2 IRQ masking register 1
    GPC_IMR2 &= ~0x200;							// Mask IRQ bit 9
// D - CCM Low Power Control Register
	// 14.7.16 CCM Low Power Control Register
	uint32_t MemClpcr = CCM_CLPCR;
    uint32_t clpcr = CCM_CLPCR 
		| CCM_CLPCR_LPM( 0x01 ) 				// Transfer to wait mode
		| CCM_CLPCR_ARM_CLK_DIS_ON_LPM 			// Arm clock disabled on wait mode
		| CCM_CLPCR_STBY_COUNT( 0x03 ) 			// Standby counter definition, CCM will wait (15*pmic_delay_scaler)+1 ckil clock cycles
		| 0x1C 									// Bits 2, 3 and 4, This field is reserved ???
		| 0x08280000;							// Bit 19, BYPASS_LPM_HS1, Bypass low power mode handshake
		                                        // Bit 21, BYPASS_LPM_HS0, Bypass low power mode handshake
                                                // Bit 27, MASK_L2CC_IDLE, Mask L2CC IDLE for entering low power mode
    CCM_CLPCR = clpcr;
    GPC_IMR2 |= 0x200;							// Enable IRQ bit 9
    //NVIC_DISABLE_IRQ(IRQ_GPR_IRQ);
    //NVIC_CLEAR_PENDING(IRQ_GPR_IRQ);
    /**************************************************************/
// E - CCM Clock Gating Register 0 to 6
	// 14.7.21 CCM Clock Gating Register 0 to 14.7.27 CCM Clock Gating Register 6
    uint32_t ccgr0 = CCM_CCGR0;					// Saves registers CCM_CCGR0 to CCM_CCGR6 for return to normal mode
    uint32_t ccgr1 = CCM_CCGR1;
    uint32_t ccgr2 = CCM_CCGR2;
    uint32_t ccgr3 = CCM_CCGR3;
    uint32_t ccgr4 = CCM_CCGR4;
    uint32_t ccgr5 = CCM_CCGR5;
    uint32_t ccgr6 = CCM_CCGR6;
	// These bits are used to turn on/off the clock to each module independently. The following
	// table details the possible clock activity conditions for each module
	// 00 Clock is off during all modes. Stop enter hardware handshake is disabled.
	// 01 Clock is on in run mode, but off in WAIT and STOP modes
	// 10 Not applicable (Reserved).
	// 11 Clock is on during all modes, except STOP mode.
    CCM_CCGR0 = 0xC5;	// 1100 0101			// aips_tz1 and aips_tz2 Clock is on in run mode, but off in WAIT and STOP modes
                                                // mqs clock Clock is off during all modes, except STOP mode
                                                // bit 6-7 Reserved ??? Clock is on during all modes, except STOP mode									
    CCM_CCGR1 = 0 | ( CCM_CCGR1 & ( 
		  CCM_CCGR1_GPT1_SERIAL( 3 )			// gpt1 serial Clock is on during all modes, except STOP mode
		| CCM_CCGR1_GPT1_BUS( 3 ) ) );			// gpt1 bus Clock is on during all modes, except STOP mode
    CCM_CCGR2 = 0x3     // 0011                 // ocram_exsc Clock is off during all modes, except STOP mode
	 | ( CCM_CCGR2 & CCM_CCGR2_IOMUXC_SNVS(3));	// lpi2c2 clock Clock is off during all modes, except STOP mode
    CCM_CCGR3 = 0x10000000 						// lpuart6 Not applicable ???
	 | ( CCM_CCGR3 & ( CCM_CCGR3_ACMP1( 3 )		// acmp1 Clock is on during all modes, except STOP mode
	                 | CCM_CCGR3_ACMP2( 3 )		// acmp2 Clock is on during all modes, except STOP mode
					 | CCM_CCGR3_ACMP3( 3 )		// acmp3 Clock is on during all modes, except STOP mode
					 | CCM_CCGR3_ACMP4( 3 )));	// acmp4 Clock is on during all modes, except STOP mode
    CCM_CCGR4 = 0x300;  // 0011 0000 0000		// sim_m7 Clock is on during all modes, except STOP mode
    CCM_CCGR5 = 0x50001105; // 0101 0000 0000 0000 0001 0001 0000 0101
												// 01, rom, flexio1, kpp clock, aipstz4, snvs_hp, snvs_lp, Clock is on in run mode, but off in WAIT and STOP modes
												// 00, wdog3, dma, wdog2, spdif, sim_main, sai1, sai2, sai3, lpuart1, lpuart7, Clock is off during all modes, except STOP mode
    // We can enable DCDC when need to config it and close it after configuration
    CCM_CCGR6 = 0x400000;   // 0000 0000 0100 0000 0000 0000 0000 0000
												// 01, anadig, Clock is on in run mode, but off in WAIT and STOP modes
												// 00, usboh3, usdhc1, usdhc2, dcdc, ipmux4, flexspi, trng, lpuart8, timer4, aips_tz3, sim_per, lpi2c4, timer1, timer2, timer3, Clock is off during all modes, except STOP mode
												// flexspi NOTE: sim_ems_clk_enable must also be cleared, when flexspi_clk_enable is cleared ???
// F - disable_plls
    disable_plls( );
    
// G - Set ARM clock to 24MHz
    set_arm_clock( 24000000 );								// Set processor clock to 24MHz	!!! incompatible avec set_clock_xtal_osc !!!!

// H - DCDC Register 0
    // 18.5.1.2 DCDC Register 0
	uint32_t MemDCDCReg0 = DCDC_REG0;
    uint32_t reg0 = DCDC_REG0 & ~( 
		DCDC_REG0_XTAL_24M_OK					// 0 DCDC uses internal ring OSC
	  | DCDC_REG0_DISABLE_AUTO_CLK_SWITCH		// 0 If DISABLE_AUTO_CLK_SWITCH is set to 0 and 24M xtal is OK, the clock source will switch from internal ring OSC to 24M xtal automatically
	  | DCDC_REG0_SEL_CLK						// 0 DCDC uses internal ring oscillator
	  |	DCDC_REG0_PWD_OSC_INT );				// 0 Internal oscillator powered up
    DCDC_REG0 = reg0;
    
// I - DCDC Register 1 and 0
	// 18.5.1.3 DCDC Register 1
    DCDC_REG1 &= ~DCDC_REG1_REG_RLOAD_SW;		// 0 This controls the load resistor of the internal regulator of DCDC. Load resistor disconnected   
    // 18.5.1.2 DCDC Register 0
    DCDC_REG0 |= DCDC_REG0_PWD_CMP_OFFSET;		// 1 Output range comparator powered down
 
// J - Analog ARM PLL control Register
	// 14.8.1 Analog ARM PLL control Register
    CCM_ANALOG_PLL_ARM_SET = CCM_ANALOG_PLL_ARM_BYPASS;		// Bypass the PLL	!!! Fait un reset si set_arm_clock(24MHz) !!!
	// 14.8.1 Analog ARM PLL control Register
    CCM_ANALOG_PLL_ARM_SET = CCM_ANALOG_PLL_ARM_POWERDOWN;	// Powers down the PLL

// K - Regulator
	// 16.6.3 Regulator 2P5 Register
    PMU_REG_2P5_SET = PMU_REG_2P5_ENABLE_WEAK_LINREG;		// Enables the weak 2p5 regulator
    PMU_REG_2P5_CLR = PMU_REG_2P5_ENABLE_LINREG;			// Disable the regulator output
	// 16.6.1 Regulator 1P1 Register
    PMU_REG_1P1_SET = PMU_REG_1P1_ENABLE_WEAK_LINREG;		// Enables the weak 1p1 regulator.
    PMU_REG_1P1_CLR = PMU_REG_1P1_ENABLE_LINREG;			// Disable the regulator output

// L - set_clock_rc_osc
    __disable_irq();
	uint32_t ccmCcr         = CCM_CCR;						// Save CCM_CCR register
	uint32_t gpcCntr        = GPC_CNTR;						// Save GPC_CNTR register
    set_clock_rc_osc( );									// Switch to RC-OSC and Turn off XTAL-OSC	!!!! Réveil par alarme ne fonctionne plus !!!!
    __enable_irq();
    
    __asm volatile ( "dsb \n" );							// Data Synchronization Barrier, Ensure visibility of the data cleaned from the cache
    __asm volatile ( "wfi \n" );							// Wait For Interrupt is a hint instruction. It suspends execution, in the lowest power state available consistent
	// CPU is dormant waiting for an interrupt
    __asm volatile ( "isb \n" );                            // Instruction Synchronization Barrier, Synchronize fetched instruction stream

	// *********************************
	// **** Go to normal mode
	// *********************************
// Begin Jean-Do.
#ifdef SNOOZE_WITH_RESET
    // Reset CPU
    SCB_AIRCR = 0x05FA0004;
    while (1) ;
#endif
// End Jean-Do.

// L - set_clock_xtal_osc
    // Switch to XTAL-OSC
    __disable_irq();
    set_clock_xtal_osc();
    __enable_irq();
	CCM_CCR  = ccmCcr;										// Restore CCM_CCR register
	GPC_CNTR = gpcCntr;										// Restore GPC_CNTR register
// K - Regulator
	// 16.6.3 Regulator 2P5 Register
    PMU_REG_2P5_SET = PMU_REG_2P5_ENABLE_LINREG;			// Enable regular 2P5 and wait for stable
    while ((PMU_REG_2P5 & PMU_REG_2P5_OK_VDD2P5) == 0);
    PMU_REG_2P5_CLR = PMU_REG_2P5_ENABLE_WEAK_LINREG;		// Turn off weak 2P5
	// 16.6.1 Regulator 1P1 Register
    PMU_REG_1P1_SET = PMU_REG_1P1_ENABLE_LINREG;			// Enable regular 1P1 and wait for stable
    while ((PMU_REG_1P1 & PMU_REG_1P1_OK_VDD1P1) == 0);    
    PMU_REG_1P1_CLR = PMU_REG_1P1_ENABLE_WEAK_LINREG;		// Turn off weak 1P1
// J - Analog ARM PLL control Register
	// 14.8.1 Analog ARM PLL control Register
    CCM_ANALOG_PLL_ARM_CLR = CCM_ANALOG_PLL_ARM_POWERDOWN;	// Powers on the PLL
	// 14.8.1 Analog ARM PLL control Register
    CCM_ANALOG_PLL_ARM_CLR = CCM_ANALOG_PLL_ARM_BYPASS;		// Disable Bypass the PLL
// I - DCDC Register 1 and 0
    // 18.5.1.2 DCDC Register 0
    DCDC_REG0 &= ~DCDC_REG0_PWD_CMP_OFFSET;		// 0 Output range comparator powered on
	// 18.5.1.3 DCDC Register 1
    DCDC_REG1 |= DCDC_REG1_REG_RLOAD_SW;		// 1 This controls the load resistor of the internal regulator of DCDC. Load resistor connected
// H - DCDC Register 0
	// 18.5.1.3 DCDC Register 1
    DCDC_REG1 |= DCDC_REG1_REG_RLOAD_SW;		// 1 This controls the load resistor of the internal regulator of DCDC. Load resistor connected 
    // 18.5.1.2 DCDC Register 0
	DCDC_REG0 = MemDCDCReg0;
// E - CCM Clock Gating Register 0 to 6
	// 14.7.21 CCM Clock Gating Register 0 to 14.7.27 CCM Clock Gating Register 6
    CCM_CCGR0 = ccgr0;							// Reload the stored values
    CCM_CCGR1 = ccgr1;
    CCM_CCGR2 = ccgr2;
    CCM_CCGR3 = ccgr3;
    CCM_CCGR4 = ccgr4;
    CCM_CCGR5 = ccgr5;
    CCM_CCGR6 = ccgr6;
// D - CCM Low Power Control Register
	CCM_CLPCR = MemClpcr;
// C - IOMUXC_GPR_GPR 1 register
	// 17.6.2 IRQ masking register 1
    GPC_IMR2 |= 0x200;							// Unmask IRQ bit 9
	// 11.3.2 GPR1 General Purpose Register
    IOMUXC_GPR_GPR1 &= ~IOMUXC_GPR_GPR1_GINT;	// Gets ENET2 TX reference clock from the ENET2_TX_CLK pin. In this use case, an external OSC provides the clock for both the external PHY and the internal controll
// B - IOMUXC_GPR_GPR 8 and 12 registers
	// 11.3.9 GPR8 General Purpose Register
	IOMUXC_GPR_GPR8  = MemIOMuxGPR8;
	// 11.3.13 GPR12 General Purpose Register
	IOMUXC_GPR_GPR12 = MemIOMuxGPR12;
// F - enable_plls
    enable_plls( );
// G - Set ARM clock to F_CPU
	set_arm_clock( f_cpu );
// A - System Control Register
	// B3.2.7 System Control Register, SCR
    SCB_SCR &= ~SCB_SCR_SLEEPDEEP;				// Put SLEEPDEEP bit OFF (Provides a qualifying hint indicating that waking from sleep might take longer)
 	// SysTick Control and Status Register
    SYST_CSR |= SYST_CSR_TICKINT;
    
	// 14.8.17 Miscellaneous Register 0
    CCM_ANALOG_MISC0_CLR = CCM_ANALOG_MISC0_DISCON_HIGH_SNVS;		// This bit controls a switch from VDD_HIGH_IN to VDD_SNVS_IN, Turn off the switch
    
	// 14.8.17 Miscellaneous Register 0
    CCM_ANALOG_MISC0_CLR = CCM_ANALOG_MISC0_OSC_I(0x03);			// 11 MINUS_37_5_PERCENT — Increase current in the 24MHz oscillator to normal
    
	// 16.6.4 Digital Regulator Core Register
    PMU_REG_CORE_CLR = PMU_REG_CORE_FET_ODRIVE;						// If set, increases the gate drive on power gating FETs to reduce leakage in the off state. Care must be
																	// taken to apply this bit only when the input supply voltage to the power FET is less than 1.1V.
																	// NOTE: This bit should only be used in low-power modes where the external input supply voltage is nominally 0.9V.

    return wake_source;
}
//----------------------------------------------------------------------------------
int hal_hibernate ( void ) {
// Begin Jean-Do.
#ifdef SNOOZE_WITH_RESET
	int iCpt = SRC_GPR5 & 0xfff0000;
	SRC_GPR5 = iCpt | UNKNOW_WAKE; // By default, unknow wake up source
#endif
// End Jean-Do.
	// *********************************
	// **** Go to deep sleep mode
	// *********************************
    uint32_t f_cpu = F_CPU_ACTUAL;				// Stores the current CPU frequency
// A - System Control Register, gain 1mA
	// B3.2.7 System Control Register, SCR
    SCB_SCR |= SCB_SCR_SLEEPDEEP;				// Put SLEEPDEEP bit ON (Provides a qualifying hint indicating that waking from sleep might take longer)
	// SysTick Control and Status Register
    SYST_CSR &= ~SYST_CSR_TICKINT;

// B - IOMUXC_GPR_GPR 8 and 12 registers, gain 1mA
	// 11.3.9 GPR8 General Purpose Register
	uint32_t MemIOMuxGPR8 = IOMUXC_GPR_GPR8;
    IOMUXC_GPR_GPR8  = 0xaaaaaaaa;				// Put in doze mode LPI2C1, LPI2C2, LPI2C3, LPI2C4, LPSPI1, LPSPI2, LPSPI3, LPSPI4, LPUART1, LPUART2, LPUART3, LPUART4, LPUART5, LPUART6, LPUART7, LPUART8
	// 11.3.13 GPR12 General Purpose Register
	uint32_t MemIOMuxGPR12 = IOMUXC_GPR_GPR12;
    IOMUXC_GPR_GPR12 = 0x0000002a;      		// Put in doze mode FLEXIO1, FLEXIO2 and FlexIO3 in stop mode (??? FlexIO3 in doze mode)

// C - IOMUXC_GPR_GPR 1 register gain <1mA
	// 11.3.2 GPR1 General Purpose Register
    IOMUXC_GPR_GPR1 |= IOMUXC_GPR_GPR1_GINT;	// Gets ENET2 TX reference clock from the ENET2_TX_CLK pin. In this use case, an external OSC provides the clock for both the external PHY and the internal controll

// D - CCM Low Power Control Register, gain <1mA
	// 17.6.2 IRQ masking register 1
    GPC_IMR2 &= ~0x200;							// Mask IRQ bit 9
	// 14.7.16 CCM Low Power Control Register
	uint32_t MemClpcr = CCM_CLPCR;
    uint32_t clpcr = CCM_CLPCR 
		| CCM_CLPCR_LPM( 0x01 ) 				// Transfer to wait mode
		| CCM_CLPCR_ARM_CLK_DIS_ON_LPM 			// Arm clock disabled on wait mode
		| CCM_CLPCR_STBY_COUNT( 0x03 ) 			// Standby counter definition, CCM will wait (15*pmic_delay_scaler)+1 ckil clock cycles
		| 0x1C 									// Bits 2, 3 and 4, This field is reserved ???
		| 0x08280000;							// Bit 19, BYPASS_LPM_HS1, Bypass low power mode handshake
		                                        // Bit 21, BYPASS_LPM_HS0, Bypass low power mode handshake
                                                // Bit 27, MASK_L2CC_IDLE, Mask L2CC IDLE for entering low power mode
    CCM_CLPCR = clpcr;
    GPC_IMR2 |= 0x200;							// Enable IRQ bit 9

// E - CCM Clock Gating Register 0 to 6, gain 1mA
	// 14.7.21 CCM Clock Gating Register 0 to 14.7.27 CCM Clock Gating Register 6
    uint32_t ccgr0 = CCM_CCGR0;					// Saves registers CCM_CCGR0 to CCM_CCGR6 for return to normal mode
    uint32_t ccgr1 = CCM_CCGR1;
    uint32_t ccgr2 = CCM_CCGR2;
    uint32_t ccgr3 = CCM_CCGR3;
    uint32_t ccgr4 = CCM_CCGR4;
    uint32_t ccgr5 = CCM_CCGR5;
    uint32_t ccgr6 = CCM_CCGR6;
	// These bits are used to turn on/off the clock to each module independently. The following
	// table details the possible clock activity conditions for each module
	// 00 Clock is off during all modes. Stop enter hardware handshake is disabled.
	// 01 Clock is on in run mode, but off in WAIT and STOP modes
	// 10 Not applicable (Reserved).
	// 11 Clock is on during all modes, except STOP mode.
    CCM_CCGR0 = 0xC5;	// 1100 0101			// aips_tz1 and aips_tz2 Clock is on in run mode, but off in WAIT and STOP modes
                                                // mqs clock Clock is off during all modes, except STOP mode
                                                // bit 6-7 Reserved ??? Clock is on during all modes, except STOP mode									
    CCM_CCGR1 = 0 | ( CCM_CCGR1 & ( 
		  CCM_CCGR1_GPT1_SERIAL( 3 )			// gpt1 serial Clock is on during all modes, except STOP mode
		| CCM_CCGR1_GPT1_BUS( 3 ) ) );			// gpt1 bus Clock is on during all modes, except STOP mode
    CCM_CCGR2 = 0x3     // 0011                 // ocram_exsc Clock is off during all modes, except STOP mode
	 | ( CCM_CCGR2 & CCM_CCGR2_IOMUXC_SNVS(3));	// lpi2c2 clock Clock is off during all modes, except STOP mode
    CCM_CCGR3 = 0x10000000 						// lpuart6 Not applicable ???
	 | ( CCM_CCGR3 & ( CCM_CCGR3_ACMP1( 3 )		// acmp1 Clock is on during all modes, except STOP mode
	                 | CCM_CCGR3_ACMP2( 3 )		// acmp2 Clock is on during all modes, except STOP mode
					 | CCM_CCGR3_ACMP3( 3 )		// acmp3 Clock is on during all modes, except STOP mode
					 | CCM_CCGR3_ACMP4( 3 )));	// acmp4 Clock is on during all modes, except STOP mode
    CCM_CCGR4 = 0x300;  // 0011 0000 0000		// sim_m7 Clock is on during all modes, except STOP mode
    CCM_CCGR5 = 0x50001105; // 0101 0000 0000 0000 0001 0001 0000 0101
												// 01, rom, flexio1, kpp clock, aipstz4, snvs_hp, snvs_lp, Clock is on in run mode, but off in WAIT and STOP modes
												// 00, wdog3, dma, wdog2, spdif, sim_main, sai1, sai2, sai3, lpuart1, lpuart7, Clock is off during all modes, except STOP mode
    // We can enable DCDC when need to config it and close it after configuration
    CCM_CCGR6 = 0x400000;   // 0000 0000 0100 0000 0000 0000 0000 0000
												// 01, anadig, Clock is on in run mode, but off in WAIT and STOP modes
												// 00, usboh3, usdhc1, usdhc2, dcdc, ipmux4, flexspi, trng, lpuart8, timer4, aips_tz3, sim_per, lpi2c4, timer1, timer2, timer3, Clock is off during all modes, except STOP mode
												// flexspi NOTE: sim_ems_clk_enable must also be cleared, when flexspi_clk_enable is cleared ???
// F - disable_plls
    disable_plls( );
    
// G - Set ARM clock to 24MHz, gain 6mA
    set_arm_clock( 24000000 );					// Set processor clock to 24MHz	!!! incompatible avec set_clock_xtal_osc !!!!

// H - Turn off FlexRAM, gain <1mA
    // 17.6.1 GPC Interface control register
    GPC_CNTR |= GPC_CNTR_PDRAM0_PGE;			// FlexRAM PDRAM0 Power Gate Enable, FlexRAM PDRAM0 domain will be powered down when the CPU core is powered down

// I - DCDC Register 0, 1 and 2, gain 4mA
    // 18.5.1.2 DCDC Register 0
	uint32_t MemDCDCReg0 = DCDC_REG0;
    uint32_t reg0 = DCDC_REG0 & ~( 
		DCDC_REG0_XTAL_24M_OK					// 0 DCDC uses internal ring OSC
	  | DCDC_REG0_DISABLE_AUTO_CLK_SWITCH		// 0 If DISABLE_AUTO_CLK_SWITCH is set to 0 and 24M xtal is OK, the clock source will switch from internal ring OSC to 24M xtal automatically
	  | DCDC_REG0_SEL_CLK						// 0 DCDC uses internal ring oscillator
	  |	DCDC_REG0_PWD_OSC_INT );				// 0 Internal oscillator powered up
    DCDC_REG0 = reg0;
	// 18.5.1.3 DCDC Register 1
    DCDC_REG1 &= ~DCDC_REG1_REG_RLOAD_SW;		// 0 This controls the load resistor of the internal regulator of DCDC. Load resistor disconnected   
    // 18.5.1.2 DCDC Register 0
    DCDC_REG0 |= DCDC_REG0_PWD_CMP_OFFSET;		// 1 Output range comparator powered down
    DCDC_REG0 &= ~( DCDC_REG0_PWD_ZCD			// Power Down Zero Cross Detection, 0 Zero cross detetion function powered up
				  | DCDC_REG0_PWD_CMP_OFFSET);	// Power down output range comparator, 0 Output range comparator powered up

	// 18.5.1.4 DCDC Register 2
	uint32_t dcdc_reg2 = DCDC_REG2;
    DCDC_REG2 = ( ~0xE00 & DCDC_REG2 )			// 1110 0000 0000 Enable RC Scale 000 This results in lower current consumption
	  | DCDC_REG2_LOOPCTRL_EN_RCSCALE1( 0x4 )	// Enable RC Scale, 0x4 for DCM
	  | DCDC_REG2_DCM_SET_CTRL;					// DCM Set Control, 1 is recommended when supporting DCM mode

// J - Analog ARM PLL control Register, gain 7mA
	// 14.8.1 Analog ARM PLL control Register
    CCM_ANALOG_PLL_ARM_SET = CCM_ANALOG_PLL_ARM_BYPASS;		// Bypass the PLL
	// 14.8.1 Analog ARM PLL control Register
    CCM_ANALOG_PLL_ARM_SET = CCM_ANALOG_PLL_ARM_POWERDOWN;	// Powers down the PLL

// K - Regulator, gain 2mA
	// 16.6.3 Regulator 2P5 Register
    PMU_REG_2P5_SET = PMU_REG_2P5_ENABLE_WEAK_LINREG;		// Enables the weak 2p5 regulator
    PMU_REG_2P5_CLR = PMU_REG_2P5_ENABLE_LINREG;			// Disable the regulator output
	// 16.6.1 Regulator 1P1 Register
    PMU_REG_1P1_SET = PMU_REG_1P1_ENABLE_WEAK_LINREG;		// Enables the weak 1p1 regulator.
    PMU_REG_1P1_CLR = PMU_REG_1P1_ENABLE_LINREG;			// Disable the regulator output

// L - set_clock_rc_osc, gain 8mA
    __disable_irq();
	uint32_t ccmCcr         = CCM_CCR;						// Save CCM_CCR register
	uint32_t gpcCntr        = GPC_CNTR;						// Save GPC_CNTR register
    set_clock_rc_osc( );									// Switch to RC-OSC and Turn off XTAL-OSC
    __enable_irq();

// M - Disconnect vdd_high_in and connect vdd_snvs_in, gain <1mA
	// 14.8.17 Miscellaneous Register 0
    CCM_ANALOG_MISC0_SET = CCM_ANALOG_MISC0_DISCON_HIGH_SNVS;		// This bit controls a switch from VDD_HIGH_IN to VDD_SNVS_IN, Turn off the switch
	uint32_t MemstopMode = (CCM_ANALOG_MISC0 & 0x00000C00) >> 10;
    CCM_ANALOG_MISC0 &= ~0x00000C00;						// Configure the analog behavior in stop mode, 00 All analog except RTC powered down on stop mode assertion

// N - Switch to on chip oscillator, gain 1mA
	// 17.6.2 IRQ masking register 1 to 5
    uint32_t gpc_imr1 = GPC_IMR1;				// Save register
    uint32_t gpc_imr2 = GPC_IMR2;
    uint32_t gpc_imr3 = GPC_IMR3;
    uint32_t gpc_imr4 = GPC_IMR4;
    uint32_t gpc_imr5 = GPC_IMR5;
	uint32_t ccm_ccr  = CCM_CCR;
    GPC_IMR1 = 0xFFFFFFFF;						// 1-IRQ masked
    GPC_IMR2 = 0xFFFFFFFF;
    GPC_IMR3 = 0xFFFFFFFF;
    GPC_IMR4 = 0xFFFFFFFF;
    GPC_IMR5 = 0xFFFFFFFF;
	// 14.7.1 CCM Control Register
    CCM_CCR = ( CCM_CCR & ~0x7E00000 )			// 0111 1110 0000 0000 0000 0000 0000 REG_BYPASS_COUNT
		| CCM_CCR_REG_BYPASS_COUNT( 0x3 )		// Counter for analog_reg_bypass signal assertion after standby voltage request by PMIC_STBY_REQ. 
												// Should be zeroed and reconfigured after exit from low power mode. 3 CKIL clock period delay
		| CCM_CCR_OSCNT( 0xAF );				// Oscillator ready counter value, These bits define value of 32KHz counter, that serve as counter for oscillator lock time
    CCM_CCR |= ( CCM_CCR_RBC_EN					// Enable for REG_BYPASS_COUNTER
		       | CCM_CCR_COSC_EN  );			// On chip oscillator enable bit
    for ( int i = 0; i < 250; i++ ) __asm volatile ( "nop \n" );	// Wait ~12µs (F_CPU 40MHz 1 nop ~48ns)
    // Restore IRQ
    GPC_IMR1 = gpc_imr1;
    GPC_IMR2 = gpc_imr2 | ( 0x20000200 );
    GPC_IMR3 = gpc_imr3;
    GPC_IMR4 = gpc_imr4;
    GPC_IMR5 = gpc_imr5;

// O - General purpose registers, gain <1mA
	// 11.3.5 GPR4 General Purpose Register
	uint32_t iomux_gpr4 = IOMUXC_GPR_GPR4 & 0x0000ffff;
    IOMUXC_GPR_GPR4 = 0x0000ffff;				// All stop request
	// 11.3.8 GPR7 General Purpose Register
	uint32_t iomux_gpr7 = IOMUXC_GPR_GPR7 & 0x0000ffff;
    IOMUXC_GPR_GPR7 = 0x0000ffff;				// All stop request
	// 11.3.9 GPR8 General Purpose Register
	uint32_t iomux_gpr8 = IOMUXC_GPR_GPR8 & 0x0000ffff;
    IOMUXC_GPR_GPR8 = 0xfffcffff;				// All stop request
	// 11.3.13 GPR12 General Purpose Register
	uint32_t iomux_gpr12 = IOMUXC_GPR_GPR12 & 0xa;
    IOMUXC_GPR_GPR12 = 0x0000000a;				// FLEXIO1 and FLEXIO2 in doze mode

    __asm volatile ( "dsb \n" );							// Data Synchronization Barrier, Ensure visibility of the data cleaned from the cache
    __asm volatile ( "wfi \n" );							// Wait For Interrupt is a hint instruction. It suspends execution, in the lowest power state available consistent
	// CPU is dormant waiting for an interrupt
    __asm volatile ( "isb \n" );                            // Instruction Synchronization Barrier, Synchronize fetched instruction stream

	// *********************************
	// **** Go to normal mode
	// *********************************
	//digitalWrite(LED_BUILTIN, HIGH);
    
// Begin Jean-Do.
#ifdef SNOOZE_WITH_RESET
    // Reset CPU
    SCB_AIRCR = 0x05FA0004;
    while (1) ;
#endif
// End Jean-Do.

//digitalWrite(LED_BUILTIN, HIGH);
// O - General purpose registers
	// 11.3.5 GPR4 General Purpose Register
	IOMUXC_GPR_GPR4 = iomux_gpr4;
	// 11.3.8 GPR7 General Purpose Register
	IOMUXC_GPR_GPR7 = iomux_gpr7;
	// 11.3.9 GPR8 General Purpose Register
	IOMUXC_GPR_GPR8 = iomux_gpr8;
	// 11.3.13 GPR12 General Purpose Register
	IOMUXC_GPR_GPR12 = iomux_gpr12;

// N - Switch to on chip oscillator
	// 17.6.2 IRQ masking register 1 to 5
    GPC_IMR1 = 0xFFFFFFFF;						// 1-IRQ masked
    GPC_IMR2 = 0xFFFFFFFF;
    GPC_IMR3 = 0xFFFFFFFF;
    GPC_IMR4 = 0xFFFFFFFF;
    GPC_IMR5 = 0xFFFFFFFF;
	// 14.7.1 CCM Control Register
	CCM_CCR = ccm_ccr;
    for ( int i = 0; i < 250; i++ ) __asm volatile ( "nop \n" );	// Wait ~12µs (F_CPU 40MHz 1 nop ~48ns)
    // Restore IRQ
    GPC_IMR1 = gpc_imr1;
    GPC_IMR2 = gpc_imr2 | ( 0x20000200 );
    GPC_IMR3 = gpc_imr3;
    GPC_IMR4 = gpc_imr4;
    GPC_IMR5 = gpc_imr5;
    for ( int i = 0; i < 2084; i++ ) __asm volatile ( "nop \n" );	// Wait ~100µs (F_CPU 40MHz 1 nop ~48ns)

// M - Disconnect vdd_high_in and connect vdd_snvs_in
	// 14.8.17 Miscellaneous Register 0
    CCM_ANALOG_MISC0_CLR = CCM_ANALOG_MISC0_DISCON_HIGH_SNVS;		// This bit controls a switch from VDD_HIGH_IN to VDD_SNVS_IN, Turn off the switch
    CCM_ANALOG_MISC0 |= MemstopMode;								// Configure the analog behavior in stop mode
    for ( int i = 0; i < 600; i++ ) __asm volatile ( "nop \n" );	// Wait ~29µs (F_CPU 40MHz 1 nop ~48ns)

// L - set_clock_xtal_osc
    // Switch to XTAL-OSC
    __disable_irq();
    set_clock_xtal_osc();
    __enable_irq();
	CCM_CCR  = ccmCcr;										// Restore CCM_CCR register
	GPC_CNTR = gpcCntr;										// Restore GPC_CNTR register

// K - Regulator
	// 16.6.3 Regulator 2P5 Register
    PMU_REG_2P5_SET = PMU_REG_2P5_ENABLE_LINREG;			// Enable regular 2P5 and wait for stable
    while ((PMU_REG_2P5 & PMU_REG_2P5_OK_VDD2P5) == 0);
    PMU_REG_2P5_CLR = PMU_REG_2P5_ENABLE_WEAK_LINREG;		// Turn off weak 2P5
	// 16.6.1 Regulator 1P1 Register
    PMU_REG_1P1_SET = PMU_REG_1P1_ENABLE_LINREG;			// Enable regular 1P1 and wait for stable
    while ((PMU_REG_1P1 & PMU_REG_1P1_OK_VDD1P1) == 0);    
    PMU_REG_1P1_CLR = PMU_REG_1P1_ENABLE_WEAK_LINREG;		// Turn off weak 1P1

// J - Analog ARM PLL control Register
	// 14.8.1 Analog ARM PLL control Register
    CCM_ANALOG_PLL_ARM_CLR = CCM_ANALOG_PLL_ARM_POWERDOWN;	// Powers on the PLL
	// 14.8.1 Analog ARM PLL control Register
    CCM_ANALOG_PLL_ARM_CLR = CCM_ANALOG_PLL_ARM_BYPASS;		// Disable Bypass the PLL

// I - DCDC Register 1 and 2
	// 18.5.1.4 DCDC Register 2
	DCDC_REG2 = dcdc_reg2;
	// 18.5.1.3 DCDC Register 1
    DCDC_REG1 |= DCDC_REG1_REG_RLOAD_SW;		// 1 This controls the load resistor of the internal regulator of DCDC. Load resistor connected

// H - Turn off FlexRAM
	// 17.7.1 PGC Mega Control Register
    PGC_MEGA_CTRL &= ~PGC_MEGA_CTRL_PCR;		// Power Control, Switch off power when pdn_req is asserted

// E - CCM Clock Gating Register 0 to 6
	// 14.7.21 CCM Clock Gating Register 0 to 14.7.27 CCM Clock Gating Register 6
    CCM_CCGR0 = ccgr0;							// Reload the stored values
    CCM_CCGR1 = ccgr1;
    CCM_CCGR2 = ccgr2;
    CCM_CCGR3 = ccgr3;
    CCM_CCGR4 = ccgr4;
    CCM_CCGR5 = ccgr5;
    CCM_CCGR6 = ccgr6;
	
// D - CCM Low Power Control Register
	// 17.6.2 IRQ masking register 1
    GPC_IMR2 &= ~0x200;							// Mask IRQ bit 9
	CCM_CLPCR = MemClpcr;
	// 17.6.2 IRQ masking register 1
    GPC_IMR2 |= 0x200;							// Unmask IRQ bit 9
	
// C - IOMUXC_GPR_GPR 1 register
	// 11.3.2 GPR1 General Purpose Register
    IOMUXC_GPR_GPR1 &= ~IOMUXC_GPR_GPR1_GINT;	// Gets ENET2 TX reference clock from the ENET2_TX_CLK pin. In this use case, an external OSC provides the clock for both the external PHY and the internal controll

// B - IOMUXC_GPR_GPR 8 and 12 registers
	// 11.3.9 GPR8 General Purpose Register
	IOMUXC_GPR_GPR8  = MemIOMuxGPR8;
	// 11.3.13 GPR12 General Purpose Register
	IOMUXC_GPR_GPR12 = MemIOMuxGPR12;

// F - enable_plls
    enable_plls( );

// G - Set ARM clock to F_CPU
	set_arm_clock( f_cpu );

// A - System Control Register
	// B3.2.7 System Control Register, SCR
    SCB_SCR &= ~SCB_SCR_SLEEPDEEP;				// Put SLEEPDEEP bit OFF (Provides a qualifying hint indicating that waking from sleep might take longer)
 	// SysTick Control and Status Register
    SYST_CSR |= SYST_CSR_TICKINT;
 
// I' - DCDC Register 1 and 2
    // 18.5.1.2 DCDC Register 0
	DCDC_REG0 = MemDCDCReg0;

	// 14.8.17 Miscellaneous Register 0
    CCM_ANALOG_MISC0_CLR = CCM_ANALOG_MISC0_DISCON_HIGH_SNVS;		// This bit controls a switch from VDD_HIGH_IN to VDD_SNVS_IN, Turn off the switch
    
	// 14.8.17 Miscellaneous Register 0
    CCM_ANALOG_MISC0_CLR = CCM_ANALOG_MISC0_OSC_I(0x03);			// 11 MINUS_37_5_PERCENT — Increase current in the 24MHz oscillator to normal
    
	// 16.6.4 Digital Regulator Core Register
    PMU_REG_CORE_CLR = PMU_REG_CORE_FET_ODRIVE;						// If set, increases the gate drive on power gating FETs to reduce leakage in the off state. Care must be
																	// taken to apply this bit only when the input supply voltage to the power FET is less than 1.1V.
																	// NOTE: This bit should only be used in low-power modes where the external input supply voltage is nominally 0.9V.

    return wake_source;
}
#endif /* IMXRT1062 */
