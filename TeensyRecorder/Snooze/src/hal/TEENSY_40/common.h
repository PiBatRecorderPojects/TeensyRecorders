#if defined(__IMXRT1062__)

#ifndef common_h
#define common_h

// If define Snooze with RESET CPU on wakeup
#define SNOOZE_WITH_RESET

#define COMPARE_WAKE    34
#define ALARM_WAKE      35
#define TIMER_WAKE      36
#define TOUCH_WAKE      37
#define UNKNOW_WAKE     0x5A5A
#define UNKNOW_ISR      0x5555
// End Jean-Do.

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

#endif /* common_h */
#endif /* __IMXRT1062__ */
