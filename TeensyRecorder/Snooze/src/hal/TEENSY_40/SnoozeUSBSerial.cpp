/***********************************************************************************
 *  SnoozeUSBSerial.h
 *  Teensy 4.0
 *
 * Purpose: USBSerial Driver
 *
 **********************************************************************************/
#if defined(__IMXRT1062__)

#include "SnoozeUSBSerial.h"
#include "usb_dev.h"

#ifdef __cplusplus
extern "C" {
#endif
    //extern void start_usb_pll(void);
	extern FLASHMEM void usb_pll_start();
#ifdef __cplusplus
}
#endif

/*******************************************************************************
 *  Stream virtual functions
 *******************************************************************************/
size_t SnoozeUSBSerial::write( uint8_t b ) {
    return usb_serial_putchar( b );
}

size_t SnoozeUSBSerial::write( const uint8_t *buffer, size_t size ) {
    size_t count = 0;
    if ( Serial ) {
        if ( strlen( print_buffer ) ) {
            char *buf = print_buffer;
            int pb_size  = strlen( print_buffer );
            while ( pb_size-- ) count += write( *buf++ );
            flush( );
            memset( print_buffer, 0, strlen( print_buffer ) );
        }
        if ( buffer == nullptr ) return 0;
        while ( size-- ) count += write( *buffer++ );
        return count;
    } else {
        int pb_size  = strlen( print_buffer );
        if ( pb_size + size > USB_SERIAL_BUFFER_SIZE ) return 0;
        strcat( print_buffer, ( const char * )buffer );
    }
    return count;
}

int SnoozeUSBSerial::availableForWrite( void ) {
    return usb_serial_write_buffer_free( );
}

int SnoozeUSBSerial::available( ) {
    return usb_serial_available( );
}

void SnoozeUSBSerial::flush( void ) {
    usb_serial_flush_output( );
}

int SnoozeUSBSerial::read( ) {
    return usb_serial_getchar( );
}

int SnoozeUSBSerial::peek( ) {
    return usb_serial_peekchar( );
}

/*******************************************************************************
 *  Sets the usb_configuration, usb_cdc_line_rtsdtr and usb_cdc_line_rtsdtr_millis
 *  to initial state so while(!Serial) works after sleeping. Still buggy...
 *******************************************************************************/
void SnoozeUSBSerial::disableDriver( uint8_t mode ) {
	__disable_irq();
	// 43.4.1 USB PHY Power-Down Register, Power on all
	USBPHY1_PWD_CLR = USBPHY_PWD_TXPWDFS | USBPHY_PWD_TXPWDIBIAS | USBPHY_PWD_TXPWDV2I | USBPHY_PWD_RXPWDENV | USBPHY_PWD_RXPWD1PT1 | USBPHY_PWD_RXPWDDIFF | USBPHY_PWD_RXPWDRX;
	// 42.7.31 Port Status & Control
	USB1_PORTSC1 &= ~(USB_PORTSC1_SUSP		// 0 = Port not in suspend state
				    | USB_PORTSC1_PHCD);	// PHY Low Power Suspend, 0 Enable PHY clock
	// 42.7.18 USB Command Register
	USB1_USBCMD |= (USB_USBCMD_ASE			// Asynchronous Schedule Enable, 1 Process the Asynchronous Schedule
    	          | USB_USBCMD_PSE);		// Periodic Schedule Enable- Read/Write, 1 Process the Periodic Schedule
	__enable_irq();
    NVIC_ENABLE_IRQ( IRQ_USB1 );
	//digitalWriteFast(LED_BUILTIN, HIGH);
}
/*******************************************************************************
 *  Turns off usb clock if using 'sleep'.
 *******************************************************************************/
void SnoozeUSBSerial::enableDriver( uint8_t mode ) {
    NVIC_DISABLE_IRQ( IRQ_USB1 );
	/*
	42.4.3.1 Entering Low Power Suspend Mode (USB1)
	In Host operation mode, low power suspend mode is entered as follows:
	1. Clear the ASE and PSE bits in USB_USBCMD, and wait until the AS and PS bits in
	USB_USBSTS become "0".
	2. Set the "SUSPEND" bit in USB_PORTSC1
	3. Set the "PHCD" bit in USB_PORTSC1
	4. Set all PWD bits in USBPHYx_PWD
	5. Set CLKGATE in USBPHYx_CTRL
	NOTE
	Step 3,4,5 shall be done in atomic operation. That is, interrupt
	should be disabled during these three steps.	
	*/
	__disable_irq();
	// 42.7.18 USB Command Register
	USB1_USBCMD &= ~(USB_USBCMD_ASE			// Asynchronous Schedule Enable, 0 Do not process the Asynchronous Schedule
    	           | USB_USBCMD_PSE);		// Periodic Schedule Enable- Read/Write, 0 Do not process the Periodic Schedule
	// 42.7.19 USB Status Register
    while ( ( USB1_USBSTS & (USB_USBSTS_AS | USB_USBSTS_PS) ) != 0 ); // Wait AS and PS to 0
	// 42.7.31 Port Status & Control
	USB1_PORTSC1 |= (USB_PORTSC1_SUSP		// 1 = Port in suspend state
				   | USB_PORTSC1_PHCD);		// PHY Low Power Suspend, 1 Disable PHY clock
	// 43.4.1 USB PHY Power-Down Register, Power down all
	USBPHY1_PWD_SET = USBPHY_PWD_TXPWDFS | USBPHY_PWD_TXPWDIBIAS | USBPHY_PWD_TXPWDV2I | USBPHY_PWD_RXPWDENV | USBPHY_PWD_RXPWD1PT1 | USBPHY_PWD_RXPWDDIFF | USBPHY_PWD_RXPWDRX;
	__enable_irq();
}
/*******************************************************************************
 *  not used
 *******************************************************************************/
void SnoozeUSBSerial::clearIsrFlags( uint32_t ipsr ) {
    
}
/*******************************************************************************
 *  not used
 *******************************************************************************/
void SnoozeUSBSerial::isr( void ) {
    
}
#endif /* __IMXRT1062__ */
