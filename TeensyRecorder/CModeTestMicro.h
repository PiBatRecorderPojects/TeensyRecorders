//-------------------------------------------------------------------------
//! \file CModeTestMicro.h
//! \brief Classes de gestion du mode Test micro
//! \author Jean-Do. Vrignault
/* 
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "CModeRecorder.h"
#include "CRecorder.h"

#ifndef CMODETESTMICRO_H
#define CMODETESTMICRO_H

//-------------------------------------------------------------------------
//! \class CGraphe128kHz
//! \brief Management class of a graph from 0 to 128kHz (1 pixel per kHz) on 50 lines
//! 14 pixels for the scale and 36 pixels for the graph
class CGrapheTestMicro
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  //! \param y Position y from the top of the graph
  CGrapheTestMicro(
    int y       
    );

  //-------------------------------------------------------------------------
  //! \brief Destructor
  ~CGrapheTestMicro();

  //-------------------------------------------------------------------------
  //! \brief Initialization of a point in the graph
  //! \param iLevel Level in dB
  //! \param iFreq Frequency in kHz
  void SetFrequence(
    int iLevel,
    int iFreq
    );
  
  //-------------------------------------------------------------------------
  //! \brief Print graphe
  //! \param iFreqH Heterodyne frequency in kHz
  void PrintGraphe();
  
protected:
  //! Position y from the top of the graph
  uint8_t iY;
  
  //! Time between two scrolls in ms
  unsigned long uiTimeScroll;

  //! Next scroll time
  unsigned long uiNextScroll;
  
  //! Bitmap for the graph 128 x n points is 16 x n bytes
  uint8_t *pTbGraphe;

  //! Nb lines
  int iNbLines;

  //! Min scale frequencies in kHz
  int iFrMin;

  //! Max scale frequencies in kHz
  int iFrMax;

  //! Size of graphe in bytes
  int iSizeGraph;
};

//! \enum TEST_MICRO
//! \brief List of microphone status
enum TEST_MICRO {
  TEST_EXCELLENT = 0, //!< Excellent microphone
  TEST_GOOD      = 1, //!< Good microphone
  TEST_WEAK      = 2, //!< Weak microphone
  TEST_NOISY     = 3, //!< Noisy microphone
  TEST_BROKEN    = 4, //!< Broken microphone
  TEST_UNKNOW    = 5  //!< Unknow status
};

//! \enum PHASE_TEST_MICRO
//! \brief List of microphone test phases
enum PHASE_TEST_MICRO {
  TEST_LEVEL    = 0, //!< Microphone test with barregraph
  TEST_SILENCE  = 1, //!< Silence microphone test
  TEST_SIGNAL   = 2, //!< Signal microphone test (keys or BatPlayer)
  TEST_RESULT   = 3  //!< Microphone test result
};

//-------------------------------------------------------------------------
//! \class CModeTestMicro
//! \brief Classe de gestion du mode test micro
class CModeTestMicro : public CModeGenericRec
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructeur
  CModeTestMicro();

  //-------------------------------------------------------------------------
  //! \brief Début du mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  //! \brief Fin du mode
  virtual void EndMode();
  
  //-------------------------------------------------------------------------
  //! \brief Test result
  //! \param pSilence
  //! \param pSignal
  //! \return a value of TEST_MICRO
  int ReturnTestResult(
    int *pSilence,
    int *pSignal
    );

  //-------------------------------------------------------------------------
  //! \brief Affichage du mode sur l'écran
  //! Cette méthode est appelée régulièrement par le loop principal
  //! à charge des différents modes d'afficher les informations nécessaires
  virtual void PrintMode();
  
  //-------------------------------------------------------------------------
  //! \brief Affichage du mode Level
  void PrintLevel();
  
  //-------------------------------------------------------------------------
  //! \brief Affichage du mode Silence
  void PrintSilence();
  
  //-------------------------------------------------------------------------
  //! \brief Affichage du mode Signal
  void PrintSignal();
  
  //-------------------------------------------------------------------------
  //! \brief Affichage du mode Result
  void PrintResult();
  
  //-------------------------------------------------------------------------
  //! \brief Passage en mode test silence
  void SwitchToSilencePhase();
  
  //-------------------------------------------------------------------------
  //! \brief Passage en mode test signal
  void SwitchToSignalPhase();
  
  //-------------------------------------------------------------------------
  //! \brief Passage en mode résultat
  void SwitchToResultPhase();
  
  //-------------------------------------------------------------------------
  //! \brief Traitement des ordres claviers
  //! Si la touche est une touche de changement de mode, retourne le mode demandé
  //! Cette méthode est appelée régulièrement par le loop principal
  //! à charge des différents modes de traiter les actions opérateurs
  //! \param key Touch to treat
  virtual int KeyManager(
    unsigned short key
    );

protected:
  //! Temps courant pour une gestion de l'affichage toute les 300ms
  unsigned long uiCurentTime;

  //! Temps courant pour une gestion de l'affichage du max
  unsigned long uiCurentTimeMax_D, uiCurentTimeMax_G;

  //! Niveau max courant détecté en dB
  int iNivMax_D, iNivMax_G;

  //! Niveau max affiché sur le barregraph
  int iNivMaxBG_D, iNivMaxBG_G;

  //! Niveau max commun
  int iNivMaxTotal;

  //! Niveau max affiché en texte
  int iNivMaxTxt_D, iNivMaxTxt_G;

  // Niveau max total détecté en dB
  int iNivMaxTotal_D, iNivMaxTotal_G;

  //! Fréquence de détection
  int iFreq;

  //! Mémorisation des niveaux et fréquences maximum lors du test
  int iMemoNivD, iMemoNivG;
  int iMemoFreqD, iMemoFreqG;

  //! Phase de test
  int iTestPhase;

  //! Time to manage test phases
  unsigned long ulTimePhases;

  //! Decounter for test phases
  int iDecounter;

  //! Decounter time
  int iDecounterSecond;

  //! Tableau des niveaux en dB pour le test micro silence canal droit
  int iTbSilenceCnx[FFTLEN/2];
  
  //! Tableau des niveaux en dB pour le test micro signal canal droit
  int iTbSignalCnx[FFTLEN/2];

  //! Tableau des niveaux en dB pour le test micro silence canal gauche
  int iTbSilenceCnxG[FFTLEN/2];
  
  //! Tableau des niveaux en dB pour le test micro signal canal gauche
  int iTbSignalCnxG[FFTLEN/2];

  //! Résultat du test micro (enum TEST_MICRO) canal droit
  int iTestResult;

  //! Résultat du test micro (enum TEST_MICRO) canal gauche
  int iTestResultG;

  //! Indique si le départ du test silence a été effectué
  bool bStartSilence;
  
  //! Indique si le départ du test signal a été effectué
  bool bStartSignal;
  
  //! Indique si on est en mode stéréo
  bool bStereo;

  //! Screen Frequency graph
  CGrapheTestMicro graphe;
};

#endif
