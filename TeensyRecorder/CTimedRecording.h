//-------------------------------------------------------------------------
//! \file CTimedRecording.h
//! \brief Classe de gestion du mode Enregistreur cadencé
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2020 Vrignault Jean-Do. All rights reserved.
/* 
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "ModesModifiers.h"
#include "CModeRecorder.h"

#ifndef MODETIMEDRECORDING
#define MODETIMEDRECORDING

//! \enum IDXAPARAMS
//! \brief Index of Audio Recorder mode parameters
enum IDXTRPARAMS {
  IDXTRRECORD, //!< Record status
  IDXTRDOWNC , //!< Downcounter
  IDXTRTIMER , //!< Record time
  IDXTRWAIT  , //!< Wait time
  IDTRBAT    , //!< Internal battery charge level in %
  IDXTRHOUR  , //!< Hour
  IDXTRDATE  , //!< Date
  IDXTRFE    , //!< FE
  IDXTRSD    , //!< SD status
  IDXTRLEAVE , //!< To leave mode
  IDXMAXTREC
};

//! \enum TRSTATUS
//! \brief Status of the mode
enum TRSTATUS {
  TRSTATUSTORECORD ,        //!< Waiting for recording
  TRSTATUSRECORDING,        //!< Recording
  TRSTATUSENDRECORD,        //!< Record OK
  TRSTATUSWAITINGSCREEN,    //!< Wait time with screen management
  TRSTATUSWAITING,          //!< Wait time without screen management (sleep mode)
};

//! \enum TRSCREENSTATUS
//! \brief Status of the screen
enum TRSCREENSTATUS {
  TRSCRENTOPRINT ,  //!< Screen need to print
  TRSCREENOK,       //!< Screen with informations
  TRSCREENTOCLEAR,  //!< Screen need to clear
  TRSCREENCLEAR     //!< Without screen
};

//-------------------------------------------------------------------------
//! \class CTimedRecording
//! \brief Timed Recording mode management class
class CTimedRecording: public CModeGenericRec
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  CTimedRecording();

  //-------------------------------------------------------------------------
  //! \brief Destructor
  ~CTimedRecording();

  //-------------------------------------------------------------------------
  //! \brief Creation du gestionnaire d'enregistrement
  virtual void CreateRecorder();
  
  //-------------------------------------------------------------------------
  //! \brief Beginning of the mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  //! \brief End of the mode
  virtual void EndMode();

  //-------------------------------------------------------------------------
  //! \brief Called on end of change of a modifier
  //! If a mode change, returns the requested mode
  //! \param idxModifier Index of affected modifier
  virtual int OnEndChange(
    int idxModifier
    );
  
  //-------------------------------------------------------------------------
  //! \brief Starts a recording period
  void StartRecordingPeriod();
  
  //-------------------------------------------------------------------------
  //! \brief Stop a recording period and wait for the next one
  void StopRecordingPeriod();
  
  //-------------------------------------------------------------------------
  //! \brief Standby mode management in future recording waiting mode
  void SleepOnWaitingPeriod();
  
  //-------------------------------------------------------------------------
  //! \brief Gestion des tâches de fonds
  virtual void OnLoop();

  //-------------------------------------------------------------------------
  //! \brief Affichage du mode sur l'écran
  //! Cette méthode est appelée régulièrement par le loop principal
  //! à charge des différents modes d'afficher les informations nécessaires
  virtual void PrintMode();
  
  //-------------------------------------------------------------------------
  //! \brief Traitement des ordres claviers
  //! Si la touche est une touche de changement de mode, retourne le mode demandé
  //! Cette méthode est appelée régulièrement par le loop principal
  //! à charge des différents modes de traiter les actions opérateurs
  //! \param key Touch to treat
  virtual int KeyManager(
    unsigned short key
    );

protected:
  //! Record status (PBREAK or PREC)
  int iRecordMode;

  //! Curent status of the mode
  int iStatus;

  //! Recording time in seconds
  int iRecordTime;

  //! Downcounter for the period
  int iDowncounter;
  
  //! Time to check battery
  unsigned long uiTimeCheck;

  //! Current second
  uint8_t iCurrentSecond;

  //! Boolean to leave mode
  bool bLeave;

  //! Sample frequency
  int iFE;

  //! Status of the screen
  int iScreen;

  //! Counter for operator screen for 30s
  int iCounterScreen;
};
#endif // MODETIMEDRECORDING
