//-------------------------------------------------------------------------
//! \file CModeParams.h
//! \brief Classe de gestion du mode paramètres
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/* 
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
//#define USBSDCARD
#ifdef USBSDCARD
#include "MTPsd.h"
#endif
#include "CModeGeneric.h"
#include "CSynchroLink.h"

 /*
  * Classes :
  * - CModeParams
  * - CModifCorectMesure
  * - CModeModifProfiles
  * - CModeLoadProfiles
  * - CModeCopyright
  */

#ifndef CMODEPARAMS_H
#define CMODEPARAMS_H

//! \enum IDXPARAMS
//! \brief Index des paramètres du mode Params
enum IDXPARAMS {
  IDXMODER,     //!< Entering recording mode (Go)
  IDXPROFILE,   //!< Selected profile
  IDXMODEF,     //!< Operating mode (Auto record, Walkin. Protoc., Road Protocol, Fixed P. Proto., RhinoLogger, Microphone test, Heterodyne, Audio Rec., Synchro)
  IDXSYNCHRO,   //!< Master or slave n
  IDXBAT  ,     //!< Internal battery charge level in %
  IDXEXT  ,     //!< External battery charge level in %
  IDXSD   ,     //!< SD card occupancy
  IDXTEMP ,     //!< Temperature and humidity probe reading
  IDXLUM  ,     //!< Max light (Y/N)
  IDXDATE ,     //!< Current date
  IDXHEURE,     //!< Current hour
  IDXUSERLEVEL, //!< User level (Beginner, Expert, Debug)
#ifdef WITHBLUETOOTH
  IDXBLUETTH,   //!< Bluetooth On/Off
  IDXBLUEINIT,  //!< Bluetooth init
#endif
  IDXTSEUIL,    //!< Threshold type (false relative, true absolute)
  IDXSEUILR,    //!< Relative detection threshold in dB (5-99)
  IDXSEUILA,    //!< Absolute detection threshold in dB (-110/-30)
  IDXHRECM,     //!< Heterodyne recording mode (false manual, true auto)
  IDXHAUTOMAN,  //!< Manual (false) / Auto (true) heterodyne mode
  IDXHSELF,     //!< Heterodyne with selective filter or not (Tennsy 4.1 only)
  IDXHGRAPH,    //!< Heterodyne with graphe or not
  IDXHREFGR,    //!< Refresh period of the heterodyne graph (0.2/2.0)
  IDXAUTOPLAY,  //!< Automatic play (X10) after recording in heterodyne mode
  IDXHETAGC,    //!< Automatic Gain Control On Off
  IDXHDEB ,     //!< Auto recording start time (hh:mm)
  IDXHFIN ,     //!< Auto recording end time (hh:mm)
  IDXRECON ,    //!< Recording time in Timed Recording mode in second (1 to x depend samplig freq)
  IDXRECOFF,    //!< Waiting time in Timed Recording mode (1s to 24h)
  IDXFMIN ,     //!< Minimum frequency of interest in kHz (1-150)
  IDXFMAX ,     //!< Maximum frequency of interest in kHz (1-150)
  IDXFMINA,     //!< Minimum frequency of interest in Hz (1-18 step 100) for audio mode
  IDXFMAXA,     //!< Maximum frequency of interest in Hz (1/48 step 100) for audio mode
  IDXPRETRIGAUT,//!< Pre-trigger duration for Teensy 4.1 with extended RAM on Automatic recording
  IDXPRETRIGHET,//!< Pre-trigger duration for Teensy 4.1 with extended RAM on Heterodyne mode
  IDXDMIN ,     //!< Minimum duration in seconds (1-10)
  IDXDMAX ,     //!< Maximum duration in seconds (10-999)
  IDXFECH ,     //!< Sampling frequency in kHz (24, 48, 96, 192, 250, 384, 500)
  IDXFECHA,     //!< Sampling frequency for audio mode in kHz (24, 48, 96, 192)
  IDXSMODE,     //!< For PRS, stereo recording mode (Stereo, Mono Right or Mono Left)
  IDXGNUM ,     //!< Numeric gain (0, 6, 12, 18, 24dB)
  IDXFILT ,     //!< Low pass filter (SFr<384) or linear filter (SFr>= 384) (Y/N)
  IDXHPFILT,    //!< High pass filter in kHz (0-25) for ultrasound rec
  IDXFHPFILT,   //!< High pass filter in kHz (0-25) for audio rec
  IDXMICTYPE,   //!< Microphone type for linear filter at 250 and 384kHz
  IDXEXP  ,     //!< Time expansion x10 (Y/N)
  IDXBATCORDER, //!< Batcorder log and Wave file name type (false=wildlife, true=batcorder)
  IDXPREF ,     //!< Wav file prefix (PRec_)
  IDXNDET ,     //!< Number of detections (1-8)
  IDXPETM ,     //!< Temperature measurement period in second (10-3600)
  IDXTHV  ,     //!< T/H measurement including in standby (Y/N)
  IDXBDRL ,     //!< Editing RhinoLogger bands (Go)
  IDXAFFRL,     //!< Permanent RhinoLogger display (Y/N)
  IDXFRTOP,     //!< Top audio frequency in kHz (1 to 50 kHz)
  IDXTOPDUR,    //!< Top audio duration in samples (256, 512 or 1024)
  IDXTOPPER,    //!< Top audio period (0 for one at the start of the recording, 1 to 10 for one every 1 to 10 seconds)
  IDXLEDSYNCH,  //!< Using LED in synchro mode (NO, REC or 3 REC only)
  IDXSWCORBAT,  //!< To switch in modification of the measurement correction of the internal batteries
  IDXCORBAT,    //!< Modification of the measurement correction of the internal batteries
  IDXSWCOREXT,  //!< To switch in modification of the measurement correction of the external batteries
  IDXCOREXT,    //!< Modification of the measurement correction of the external batteries
  IDXLANG ,     //!< Language used (English/French)
  IDXTSTSD,     //!< SD card test
  IDXDEF  ,     //!< Default settings (Y/N)
  IDXPROFILES,  //!< Goto modification of operator profiles
  IDXSNOISE,    //!< Save noise file (Y/N)
  IDXLED  ,     //!< Using the LED (No, Recordin., Acquisit., IS DMA, Tr. Loop, Cal Noise, Det. Act., Buf. Los.)
  IDXBOOT,      //!< Goto Bootloader to load a new version in Active Recorder (Yes/No)
  IDXCOPYR      //!< Gopyright menu
};

//-------------------------------------------------------------------------
//! \class CGenericMode
//! \brief Classe de gestion du mode paramètres
//! Affiche les différents paramètres et permets de les modifier
class CModeParams : public CModeGeneric
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructeur
  CModeParams();

  //-------------------------------------------------------------------------
  //! \brief Début du mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  //! \brief Fin du mode
  virtual void EndMode();
  
  //-------------------------------------------------------------------------
  // Affichage du mode sur l'écran
  // Formatage chaine température et humidité
  void FormatTH();
  
  //-------------------------------------------------------------------------
  //! \brief Affichage du mode sur l'écran
  //! Cette méthode est appelée régulièrement par le loop principal
  //! à charge des différents modes d'afficher les informations nécessaires
  virtual void PrintMode();
  
  //-------------------------------------------------------------------------
  //! \brief Traitement des ordres claviers
  //! \param key Touch to treat
  virtual int KeyManager(
    unsigned short key
    );
  
  //-------------------------------------------------------------------------
  //! \brief Called on end of change of a modifier
  //! If a mode change, returns the requested mode
  //! \param idxModifier Index of affected modifier
  virtual int OnEndChange(
    int idxModifier
    );
  
  //-------------------------------------------------------------------------
  //! \brief Cohérence entre les différents paramètres
  void Coherenceparams();
  
  //-------------------------------------------------------------------------
  //! \brief Mesure de la batterie
  void CheckBatterie();
  
  //-------------------------------------------------------------------------
  //! \brief  Possible management of fund tasks
  virtual void OnLoop();

protected:
  //! Format d'affichage du paramètre de visualisation de la batterie interne
  char FormatBatInt[MAX_LINEPARAM+10];
  //! Format d'affichage du paramètre de visualisation de la batterie externe
  char FormatBatExt[MAX_LINEPARAM+10];
  //! Nombre de secondes sans activité pour basculer automatiquement en enregistrement après 15mn
  int iSecondesInactives;
  //! Seconde courante
  int iCurrentSecond;
  //! Paramètre pour la mise aux valeurs par défaut
  bool bDefault;
  //! Paramètre pour passer en mode Bootloader
  bool bBoot;
#ifdef USBSDCARD
  //! For reading SD card by USB
  MTPStorage_SD storage;
  MTPD          mtpd;
#endif
  //! Profiles names
  char *pStrProfiles[2][5];
  char strProfiles[2][5][12];
  // Paramètres de la bandes d'intéret en kHz
  int iFreqMinkHz, iFreqMaxkHz;
  // Paramètres de la bandes d'intéret en Hz
  float fFreqMinAkHz, fFreqMaxAkHz;
  // Chaine d'affichage température humidité
  char TempH[20];
#ifdef WITHBLUETOOTH
  //! Paramètre pour passer initialiser le Bluetooth
  bool bInitBluetooth;
  // Time to init Bluetooth
  uint32_t uiBLETime;
#endif
  //! Pour passer en correction batterie
  bool bMesureBat;
  //! Mesure réelle de la batterie
  float fMesureBat;
  //! Pour passer en correction alimentation externe
  bool bMesureExt;
  //! Mesure réelle de l'alimentation externe
  float fMesureExt;
  //! Mémorisation du mode d'enregistrement courant pour le garder si basculement en mode micro
  int iMemoRecordMode;
};

//! \enum IDXMODPROF
//! \brief Operator profile modification parameters index
enum IDXMODPROF {
  IDXMPRETURN,  //!< Return to parameters mode
  IDXMPPROF1 ,  //!< Changing the name of profile 1
  IDXMPPROF2 ,  //!< Changing the name of profile 2
  IDXMPPROF3 ,  //!< Changing the name of profile 3
  IDXMPPROF4 ,  //!< Changing the name of profile 4
  IDXMPWRITE ,  //!< Writes Profiles in the Profiles.ini file
  IDXMPREAD     //!< Read Profiles from the Profiles.ini file
};

//! \enum IDXMODPROF
//! \brief Operator profile modification parameters index
enum PROFILMODIF {
  PMNORMAL,  //!< Normal screen
  PMWRITE ,  //!< Write screen
  PMREAD     //!< Read screen
};

//-------------------------------------------------------------------------
//! \class CModeModifProfiles
//! \brief Modification of operator profiles
class CModeModifProfiles : public CModeGeneric
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructeur
  CModeModifProfiles();

  //-------------------------------------------------------------------------
  //! \brief Début du mode
  virtual void BeginMode();

  //-------------------------------------------------------------------------
  //! \brief Called on end of change of a modifier
  //! If a mode change, returns the requested mode
  //! \param idxModifier Index of affected modifier
  virtual int OnEndChange(
    int idxModifier
    );
  
  //-------------------------------------------------------------------------
  //! \brief To display information outside of modifiers
  virtual void AddPrint();
  
  //-------------------------------------------------------------------------
  //! \brief Traitement des ordres claviers
  //! Si la touche est une touche de changement de mode, retourne le mode demandé
  //! Cette méthode est appelée régulièrement par le loop principal
  //! à charge des différents modes de traiter les actions opérateurs
  //! \param key Touche sollicitée par l'opérateur
  virtual int KeyManager(
    unsigned short key
    );

protected:
  //! Mode screen
  int iScreen;

  //! Current second
  int iSecond;

  //! Profiles names
  char ProfilesNames[MAXPROFILES][MAXCHARPROFILE+1];
};

#define MAXLINESINI   6  //!< Number of files displayed on the screen
#define MIDDLELINEINI 3  //!< Middle line to display files

//! \enum IDXLOADPROFILES
//! \brief Index of load profiles mode parameters
enum IDXLOADPROFILES {
  IDXLPRETURN, //!< Heterodyne frequency
  IDXLPINIA  , //!< Ini file line A
  IDXLPINIB  , //!< Ini file line B
  IDXLPINIC  , //!< Ini file line C
  IDXLPINID  , //!< Ini file line D
  IDXLPINIE  , //!< Ini file line E
  IDXLPINIF  , //!< Ini file line E
  IDXMAXLP
};

//-------------------------------------------------------------------------
//! \class CModeLoadProfiles
//! \brief Profile file selection class
class CModeLoadProfiles: public CModeGeneric
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  CModeLoadProfiles();

  //-------------------------------------------------------------------------
  //! \brief Beginning of the mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  //! \brief End of the mode
  virtual void EndMode();

  //-------------------------------------------------------------------------
  //! \brief Called on end of change of a modifier
  //! If a mode change, returns the requested mode
  //! \param idxModifier Index of affected modifier
  virtual int OnEndChange(
    int idxModifier
    );
  
  //-------------------------------------------------------------------------
  //! \brief Keyboard order processing
  //! If the key is a mode change key, returns the requested mode
  //! This method is called regularly by the main loop
  //! By default, handles modifiers
  //! \param key Touch to treat
  virtual int KeyManager(
    unsigned short key
    );
    
  //-------------------------------------------------------------------------
  //! \brief Returns the string to display, taking only the n most significant characters
  //! 123456789012345678901
  //!  PR001_190126_221812.ini
  //!  PR001_190126_221812
  //! \param str string to verify
  String PrepareStrFile(
    String str
    );

  //-------------------------------------------------------------------------
  //! \brief Initializes the list of displayed files according to the 1st selected
  void InitLstAffFiles();
    
  //-------------------------------------------------------------------------
  //! \brief List the ini files in the root directory
  void ListIniFiles();
    
protected:
  //! list of ini files displayed on the screen
  char LstFilesScreen[MAXLINESINI][MAX_LINEPARAM+1];

  //! List of pointer on ini files displayed on the screen
  char *pLstFilesScreen[MAXLINESINI][MAXLANGUAGE];

  //! List of ini files in the root directory
  std::vector<String> lstIniFiles;

  //! Index of selectd file
  int iSelectedFile;
};

//-------------------------------------------------------------------------
//! \class CModeCopyright
//! \brief Classe d'affichage des copyright
class CModeCopyright : public CModeGeneric
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructeur
  CModeCopyright();

  //-------------------------------------------------------------------------
  //! \brief Destructeur
  ~CModeCopyright();

  //-------------------------------------------------------------------------
  //! \brief Début du mode
  virtual void BeginMode();

  //-------------------------------------------------------------------------
  //! \brief Affichage du mode sur l'écran
  //! Cette méthode est appelée régulièrement par le loop principal
  //! à charge des différents modes d'afficher les informations nécessaires
  virtual void PrintMode();
  
  //-------------------------------------------------------------------------
  //! \brief  Keyboard order processing
  //! If the key is a mode change key, returns the requested mode
  //! This method is called regularly by the main loop
  //! By default, handles modifiers
  //! \param[in] key Touch to treat
  //! \return If a mode change, returns the requested mode, else return NOMODE
  virtual int KeyManager(
    unsigned short key
    );

protected:
  //! Seconde courante pour une gestion de l'affichage
  int iCurrentS;

  //! Pointeur sur la liaison série ESP32 dans le cas de l'option synchro
  CSynchroLink *pEsp32Link;

  //! Time pour la liaison ESP32
  uint32_t iTimeESP32;

  //! Timeout de réception de version
  uint32_t iTimeout;
  bool bESPVersionOK;

  //! Indique si la version ESP32 est reçue
  bool bVersionESP32;

  //! Pour faire défiler les versions en mode synchro
  int iNbSecond;
  bool bPlus;
};

#endif
