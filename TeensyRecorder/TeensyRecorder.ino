/* 
 * File:   TeensyRecorder.ino
   PassiveRecorder Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/*
 * Fichier principal du programme TeensyRecorder permettant de réaliser des enregistrements audio à partir d'une carte Teensy 3.6
 * Enregistrements aux Fe 24kHz, 48kHz, 96kHz, 192kHz, 250kHz, 384kHz ou 500kHz
 * L'acquisition est effectuée par l'ADC1 cadencé par le PIT0 et transféré via le DMA2. Deux buffers sont automatiquement remplis via l'ADC+DMA.
 * En stéréo le canal gauche est géré via l'ADC0 et le DMA3.
 * Les buffers sont de type uint16_t non signé avec des résultats de 0 à 4095
 * Une interruption est lancée à chaque fin de remplisage d'un des 2 buffers
 * Sur cette interruption, les actions suivantes sont effectuées :
 * - Copie du buffer plein dans un second buffer avec transformation du domaine 0/4095 à -2048/+2047 pour enregistrement éventuel
 * - Filtrage éventuel par le DSP
 * - Copie du buffer d'enregistrement dans un troisième buffer pour le calcul FFT avec préparation de la partie immaginaire à 0
 * - FFT 256 points par le DSP et mémorisation des résultats
 * En tache de fond, les détections sont examinées et un enregistrement est lancé ou stoppé
 * 
 * Le programme possède 5 états :
 * - Mode démarrage avec affichage de la version et du logo
 * - Mode paramètres permettant à un opérateur de modifier les paramètres de fonctionnement
 * - Mode acquisition avec enregistrement si activité
 * - Mode Attente du début d'une période d'acquisition avec une consommation réduite
 * - Mode erreur avec affichage de l'erreur
 * - Mode hétérodyne pour la version matériel Active Recorder
 * - Mode lecture pour la version matériel Active Recorder
 * 
 * Le fichier binaire du programme est a recherché sur :
 * IDE 1.x C:\Users\Utilisateur\AppData\Local\Temp\arduino_build_xxxxxx fichier PassiveRecorder.ino.hex
 * IDE 2.x C:\Users\Utilisateur\AppData\Local\Temp\arduino\sketches\D65F03B8E46CAB5AC1FB630BF7E12A06
 * 
 * ATTENTION - ATTENTION - ATTENTION - ATTENTION - ATTENTION
 * Pour un appel court à la fonctionsd.vol()->freeClusterCount() (sauf le 1er appel fait dans l'init) 
 * il faut mettre MAINTAIN_FREE_CLUSTER_COUNT à 1 dans SdFatConfig.h (librairie SdFat à télécharger sur https://github.com/greiman/SdFat)
 * 
 * Dans l'environnement ARDUINO, sélectionner "Outils" > "Type de carte Teensy 3.6" et "CPU speed : 144MHz"
 * Attention, avec l'IDE 2.x les bibliothèques Teensy sont à retrouver sur C:\Users\jeand\AppData\Local\Arduino15\packages\teensy\hardware\avr\1.57.2\libraries
 * Et sur l'IDE 1.x C:\Program Files (x86)\Arduino\hardware\teensy\avr\libraries
*/
/***********************************************************************************************************
 * ATTENTION, in the SDFat library, you have to make 2 modifications:
 * - SdFatConfig.h set MAINTAIN_FREE_CLUSTER_COUNT to 1
 *    to keep the count of free clusters updated.  This will increase the speed of the freeClusterCount()
 *    call after the first call.  Extra flash will be required.
 * - SdioTeensy.cpp set BUSY_TIMEOUT_MICROS to 800000 for 128GB cards to work
************************************************************************************************************/
/***********************************************************************************************************
 * ATTENTION, use specific Snooze library for Teensy 4.1
************************************************************************************************************/
#include "SdFat.h"
#include "TimeLib.h"
#include <U8g2lib.h>
#include "Const.h"
#include "CModeGeneric.h"
#if defined(__MK66FX1M0__) // Teensy 3.6
  #include "RamMonitor.h"
#endif

#include "CRecorder.h"
#include "CRecorderRL.h"
#include "CModeRhinoLogger.h"
#include <Snooze.h>

// Screen manager
U8G2 *pDisplay = NULL;

extern "C" volatile uint32_t set_arm_clock(uint32_t frequency);

// To display logo indefinitely
//#define SCREENSIZE_SH1106

// SDFat V1, Test d'une carte SD 16GO vierge (moyenne sur 20 tests)
// CModeTestSD carte 16GO avec SdFatSdioEX = Open   5364µs, Write(8192)  931µs, Close 5823µs
// CModeTestSD carte 16GO avec SdFatSdio   = Open   3924µs, Write(8192) 2285µs, Close 4757µs
// Test d'une carte SD 16GO avec 581 fichiers wav (moyenne sur 20 tests)
// CModeTestSD carte 16GO avec SdFatSdioEX = Open  50798µs, Write(8192) 1128µs, Close 12136µs
// CModeTestSD carte 16GO avec SdFatSdio   = Open 272262µs, Write(8192) 3182µs, Close  7726µs
// SDFat V2
// SD_FAT_TYPE 0 TESTSD_SDIO CModeTestSD Test à 144MHz avec une 64GO = Open 10ms, Write 920µs, Close 7ms
// SD_FAT_TYPE 1 TESTSD_SDIO CModeTestSD Test à 144MHz avec une 64GO = Open 14ms, Write 852µs, Close 10ms

// Gestionnaire carte SD
// Gestionnaire de carte SD spécifique Teensy 3.6
SdFs sd;

#if defined(__MK66FX1M0__) // Teensy 3.6
  // Teensy 3.6 Ram monitoring (https://github.com/veonik/Teensy-RAM-Monitor)
  extern RamMonitor ramMonitor;
#endif

// Key manager
CKeyManager KeyManag;

// Gestionnaire du fichier LogPR.txt
LogFile logPR( "PR");

// Bitmap du Logo
extern const unsigned char LogoGR [];

// Strings
extern const char *txtLOWBAT[];
extern const char *txtSDERROR[];
extern const char *txtSDWERROR[];
extern const char *txtSWError[];
extern const char *txtSDFATTO[];
extern const char *txtSDFATCFG[];
extern const char *txtSDFATBEGIN[];
extern const char *txtSTARTPR[];
extern const char *txtSTARTAR[];
extern const char *txtSTARTPRS[];
extern const char *txtSTARTPRSS[];
extern const char *txtSTARTPRMS[];
extern const char *txtLogSDErrSize[];
extern const char *txtErrFullSD[];

// Pointeur sur l'utilisation de la LED
int *piUtilLed;

// Teensy 3.6 Serial Number
uint32_t uiSerialNumber = 0;

// Serial nuber string with recorder name
char sSerialName[MAXNAMERECORDER];

// Taille de l'éventuelle RAM supplémentaire
extern "C" uint8_t external_psram_size;

//----------------------------------------------------
// Lecture du numéro de série  suivant la discussion https://forum.pjrc.com/threads/91-teensy-3-MAC-address et https://github.com/sstaub/TeensyID
uint32_t GetSerialNumber()
{
    uint32_t num;
#if defined(__MK66FX1M0__) // Teensy 3.6
    unsigned char sreg_backup = SREG;     // Sauvegarde de l'état des interruptions
    cli();                                // On stoppe les interruptions
    uint64_t num64;
    FTFL_FSTAT = FTFL_FSTAT_RDCOLERR | FTFL_FSTAT_ACCERR | FTFL_FSTAT_FPVIOL;
    *(uint32_t *)&FTFL_FCCOB3 = 0x41070000;
    FTFL_FSTAT = FTFL_FSTAT_CCIF;
    while (!(FTFL_FSTAT & FTFL_FSTAT_CCIF)) ; // wait
    num64 = *(uint64_t *)&FTFL_FCCOB7;
    SREG = sreg_backup;                   // On retaure les interruptions
    num = num64>>32;
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
    num = HW_OCOTP_MAC0 & 0xFFFFFF;
#endif
    return num;
}

// Gestion du temps entre deux lectures des touches
#if defined(__MK66FX1M0__) // Teensy 3.6
  #define TIMEKEY 70  // Temps (ms) entre deux examen de l'état des touches
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  #define TIMEKEY 30  // Temps (ms) entre deux examen de l'état des touches
#endif
unsigned long uiTimeKey = 0;
// Gestion du temps entre deux affichages d'un mode
#define TIMEAFF 300 // Temps (ms) entre 2 affichages
unsigned long uiTimeAff = 0;

//----------------------------------------------------
// Fonction permettant de dater les fichiers
void dateTime(uint16_t* date, uint16_t* time)
{
  // return date using FAT_DATE macro to format fields
  *date = FAT_DATE(year(), month(), day());

  // return time using FAT_TIME macro to format fields
  *time = FAT_TIME(hour(), minute(), second());
}

//-------------------------------------------------------------------------
// Création du gestionnaire d'écran
void CreateGestScreen()
{
#ifdef SCREENSIZE_SH1106
  pDisplay = new U8G2_SH1106_128X64_NONAME_F_HW_I2C(U8G2_R0, U8X8_PIN_NONE);
#else
  // Création du gestionnaire d'écran
  switch (CModeGeneric::GetCommonsParams()->iScreenType)
  {
  case ST_SSD1306:
    pDisplay = new U8G2_SSD1306_128X64_NONAME_F_HW_I2C(U8G2_R0, U8X8_PIN_NONE);
    break;
  default:
    pDisplay = new U8G2_SH1106_128X64_NONAME_F_HW_I2C(U8G2_R0, U8X8_PIN_NONE);
  }
#endif
  // Initialisation
  pDisplay->begin();
  pDisplay->setFontPosTop();
  pDisplay->clearBuffer();
  pDisplay->setFont(u8g2_font_6x10_mf);  // Hauteur 7
  pDisplay->sendBuffer();
}

//----------------------------------------------------
// Affichage du logo de départ
void AffLogo()
{
  pDisplay->clearBuffer();
  pDisplay->drawXBM(0, 0, 128, 64, LogoGR);
  pDisplay->setFont(u8g2_font_6x13B_mf);  // Hauteur 13
  pDisplay->setCursor(3,25);
  // Reading the recorder type
  if (CModeGeneric::GetRecorderType() == PASSIVE_STEREO)
    pDisplay->println("P.R.");
  else if (CModeGeneric::GetRecorderType() == PASSIVE_SYNCHRO)
    pDisplay->println("P.R.S.");
  else if (CModeGeneric::GetRecorderType() == PASSIVE_MSYNCHRO)
    pDisplay->println("P.M.S.");
  else if (CModeGeneric::GetRecorderType() == PASSIVE_RECORDER)
    pDisplay->println("Passive");
  else
    pDisplay->println("Active");
  pDisplay->setCursor(3,37);
  if (CModeGeneric::GetRecorderType() == PASSIVE_STEREO)
    pDisplay->println("Stereo");
  else if (CModeGeneric::GetRecorderType() >= PASSIVE_SYNCHRO)
    pDisplay->println("Synchro");
  else
    pDisplay->println("Recorder");
  pDisplay->setFont(u8g2_font_micro_mr);  // Hauteur 5
  pDisplay->setCursor( 3, 2);
  switch (CModeGeneric::GetCommonsParams()->iScreenType)
  {
  case ST_SSD1306: pDisplay->print( "SSD1306"); break;
  default        : pDisplay->print( "SH1106") ; break;
  }
  // Print CPU speed
  char temp[20];
  sprintf(temp, "CPU %dMHz", F_CPU/1000000);
  pDisplay->setCursor( 3, 9);
  pDisplay->print( temp);
  // If problem, print SDFat
  if (MAINTAIN_FREE_CLUSTER_COUNT != 1)
  {
    pDisplay->setCursor( 3, 16);
    pDisplay->print( "SDFat cluster !");
  }
  else
  {
    // Print serial number
    sprintf(temp, "%lu", uiSerialNumber);
    pDisplay->setCursor( 3, 16);
    pDisplay->print( temp);
  }
  // Print CPU type
  pDisplay->setCursor( 70, 2);
#if defined(__IMXRT1062__) // Teensy 4.1
  if (external_psram_size == 0)
    pDisplay->print( "T4.1");
  else
  {
    pDisplay->setCursor( 61, 2);
    pDisplay->printf( "T4.1 %dMO", external_psram_size);
  }
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  pDisplay->print( "T3.6");
#endif
  // Print version
  pDisplay->setFont(u8g2_font_6x10_mf);  // Hauteur 7
  pDisplay->setCursor( 3, 50);
  pDisplay->print( VERSION);
  pDisplay->drawRFrame( 0, 0, 128, 64, 0);
  pDisplay->sendBuffer();
#ifdef SCREENSIZE_SH1106
  while (true);
#endif
  if (KeyManag.GetKey() == K_LEFT)
    delay(30000);
}

//-------------------------------------------------------------------------
// Initialisation de la carte SD
bool InitSD()
{
  bool bReturn = false;
  //Serial.printf("sd.begin() type SD_FAT_TYPE SDFAT_FILE_TYPE = %d\n", SDFAT_FILE_TYPE);
  if (!sd.begin(SdioConfig(FIFO_SDIO)))
  {
    if (sd.card()->errorCode() == SD_CARD_ERROR_ACMD41)
      LogFile::AddLog( LLOG, txtSDFATTO[CModeGeneric::GetCommonsParams()->iLanguage]);
    else
      LogFile::AddLog( LLOG, txtSDFATBEGIN[CModeGeneric::GetCommonsParams()->iLanguage], sd.card()->errorCode());

    //sd.errorPrint();
  }
  else
  {
    sd.chvol();
    bReturn = true;
  }
  return bReturn;
}

//-------------------------------------------------------------------------
// Récupération de l'heure de la RTC intégrée au Teensy 3.0
time_t getTeensy3Time()
{
  return Teensy3Clock.get();
}

/*------------------------------------------------------------------------------------------------------
 * Initialisation du programme
 * - Affichage du logo puis passage en mode enregistrement
------------------------------------------------------------------------------------------------------*/
void setup()
{
  char sWho[80];
  sWho[0] = 0;
  // Init des broches d'options
  pinMode(OPT_B0, INPUT_PULLUP);
  pinMode(OPT_B1, INPUT_PULLUP);
  pinMode(OPT_B2, INPUT_PULLUP);
  pinMode(OPT_B4, INPUT_PULLUP);
#if defined(__IMXRT1062__) // Teensy 4.1
#ifdef WITHBLUETOOTH
  // Init de l'option Bluetooth sur T4.1
  pinMode(OPT_BLUETOOTH, INPUT_PULLUP);
  // Init de la commande alimentation Bluetooth sur T4.1
  pinMode(OPT_ALIM_BLE, OUTPUT);
  digitalWriteFast( OPT_ALIM_BLE, HIGH);  // Bluetooth off
  // Init de la commande reconnaissance Bluetooth sur T4.1
  pinMode(OPT_INIT_BLE, OUTPUT);
  digitalWriteFast( OPT_INIT_BLE, HIGH);  // Bluetooth init off
#endif
#endif
  // Set ESP32 software version
  strcpy(CSynchroLink::GetESP32Version(), "Vx.x");
  // Lecture du numéro de série de la carte processeur (5 tentatives)
  uiSerialNumber = GetSerialNumber();
  // Initialise l'heure de la libairie Time avec la RTC du Teensy 3.0
  setSyncProvider(getTeensy3Time);
  setSyncInterval( 10);
  // Initialisation de la liaison série de debug
  Serial.begin(115200);
  //LogFile::bConsole = true;
  delay(100);
  CModeVeille::iWhoWakeUp = 0;  // By default, normal start
  // Init du mot de controle des versions des paramètres
  // 0xABCD A = VPARAMS, B, C et D = Version du logiciel (V0.92=1092)
  int  A = VPARAMS;
  char B = VERSION[1];
  char C = VERSION[3];
  char D = VERSION[4];
  CModeGeneric::uiCtrlVersion = A + (((int)B-48) << 8) + (((int)C-48) << 4) + ((int)D-48);
  Serial.printf("Setup VERSION %s, uiCtrlVersion %X\n", VERSION, CModeGeneric::uiCtrlVersion);
  bool bPrintStart = true;
#if defined(__IMXRT1062__) // Teensy 4.1
#ifdef SNOOZE_WITH_RESET
  int iWho = SRC_GPR5 & 0xffff;
  int iCpt = SRC_GPR5 >> 16;
  if (iWho != 0)
  {
    iCpt++;
    SRC_GPR5 = iCpt << 16;
  }
  else
    // Reset SRC_GPR5 for next restart
    SRC_GPR5 = 0;
  Serial.printf("SRC_GPR5 at restart %X, Cpt %d\n", iWho, iCpt);
  if (iWho == UNKNOW_WAKE)
  {
    sprintf(sWho, "Wakeup by unknow... Cpt %d", iCpt);
    CModeVeille::iWhoWakeUp = PINPUSH;  // Unknow wake is like PINPUSH
    bPrintStart = false;
  }
  else if (iWho == UNKNOW_ISR)
  {
    sprintf(sWho, "Wakeup by unknow ISR... Cpt %d", iCpt);
    CModeVeille::iWhoWakeUp = PINPUSH;  // Unknow ISR is like PINPUSH
    bPrintStart = false;
  }
  else if (iWho == ALARM_WAKE)
  {
    sprintf(sWho, "Wakeup by ALARM... Cpt %d", iCpt);
    CModeVeille::iWhoWakeUp = ALARM_WAKE;
    bPrintStart = false;
  }
  // Any key
  else if (iWho == PINPUSH or iWho == PINRIGHT or iWho == PINLEFT or iWho == PINUP or iWho == PINDOWN or iWho == PINMODEA or iWho == PINMODEB or iWho == PINPUSH_ART41 or iWho == PINLEFT_PRST41)
  {
    sprintf(sWho, "Wakeup by PINPUSH... Cpt %d", iCpt);
    CModeVeille::iWhoWakeUp = PINPUSH;
    bPrintStart = false;
  }
#endif
#endif
  if (bPrintStart) Serial.println("===== Teensy Recorder =====");
/*#if SDFAT_FILE_TYPE == 0
  Serial.println("SDFAT_FILE_TYPE = 0");
#elif SDFAT_FILE_TYPE == 1
  Serial.println("SDFAT_FILE_TYPE = 1, for FAT16/FAT32");
#elif SDFAT_FILE_TYPE == 2
  Serial.println("SDFAT_FILE_TYPE = 2, for exFAT");
#elif SDFAT_FILE_TYPE == 3
  Serial.println("SDFAT_FILE_TYPE = 3, for FAT16/FAT32 and exFAT");
#else  // SDFAT_FILE_TYPE
  Serial.println("SDFAT_FILE_TYPE Invalide !");
#endif  // SDFAT_FILE_TYPE */
  // Initialisation de la broche de commande de l'alimentation du pré-ampli micro
  pinMode( PIN_START_PREAMP, OUTPUT);
  digitalWriteFast( PIN_START_PREAMP, HIGH);
  // Init sortie LED
  pinMode( 13, OUTPUT);
  digitalWriteFast( 13, LOW);
  // Le 1er traitement d'un float dans un printf génère 252 octets de scrories en mémoire heap qui ne sont pas libérés !
  // Pour que ces scrories ne soit pas au milieu de la mémoire, on appelle la procédure au départ
  float fTemp = 4.1;
  char sTemp[6];
  //Serial.printf( "### Avant dtostrf      %d\n", ramMonitor.free());
  dtostrf( fTemp, 3, 1, sTemp);
  //Serial.printf( "### Après 1er  dtostrf %d\n", ramMonitor.free());
  dtostrf( fTemp, 3, 1, sTemp);
  //Serial.printf( "### Après 2ème dtostrf %d\n", ramMonitor.free());
  /* Résultats :
   *  ### Avant dtostrf      239820
   *  ### Après 1er  dtostrf 239568
   *  ### Après 2ème dtostrf 239568
   */
  // Read recorder type
  int iType = CModeGeneric::ReadRecorderType();
  // Initialisation du nom de l'enregistreur en fonction de son type et de son numéro de série
  switch(iType)
  {
  default:
  case PASSIVE_RECORDER:
    sprintf( sSerialName, "PR%lu", uiSerialNumber);
    break;
  case ACTIVE_RECORDER:
    sprintf( sSerialName, "AR%lu", uiSerialNumber);
    break;
  case PASSIVE_STEREO:
    sprintf( sSerialName, "PS%lu", uiSerialNumber);
    break;
  case PASSIVE_SYNCHRO:
    sprintf( sSerialName, "PS%lu", uiSerialNumber);
    break;
  case PASSIVE_MSYNCHRO:
    sprintf( sSerialName, "PM%lu", uiSerialNumber);
    break;
  }
  logPR.SetLogFileName( (char *)sSerialName);
  //Serial.printf("sSerialName [%s]\n", sSerialName);

  // Initialisation du gestionnaire des boutons
  if (iType == ACTIVE_RECORDER)
  {
    // Initialisation de la broche d'arrêt audio
    pinMode( PIN_STOP_AUDIO, OUTPUT);
    digitalWriteFast( PIN_STOP_AUDIO, HIGH);
    // Initialisation des boutons
#if defined(__MK66FX1M0__) // Teensy 3.6
    KeyManag.Begin( PINPUSH, PINUP, PINDOWN, 0, 0, PINMODEA, PINMODEB);
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
    KeyManag.Begin( PINPUSH_ART41, PINUP, PINDOWN, 0, 0, PINMODEA, PINMODEB);
#endif
  }
  else if (digitalReadFast(OPT_B4) == 0)
    KeyManag.Begin( PINPUSH_PRST41, PINUP, PINDOWN, PINRIGHT, PINLEFT_PRST41);
  else
    KeyManag.Begin( PINPUSH, PINUP, PINDOWN, PINRIGHT, PINLEFT);
  //Serial.println("Keys OK");
  // Initialisation aux paramètres par défaut
  CModeGeneric::SetDefault();
  // Lecture des paramètres en EEPROM
  CModeGeneric::StaticReadParams();
  if (CModeGeneric::GetCommonsParams()->iUserLevel == ULDEBUG)
    CModeGeneric::bDebug = true;
  else
    CModeGeneric::bDebug = false;
  // Attente pour laisser le temps de lecture des éventuelles touches K_UP et K_DOWN
  delay(500);

  // Lecture clavier pour éventuel mode debug et changement d'écran
  int key = KeyManag.GetKey();
  // Test si mode debug
  if (key == K_UP)
  {
    Serial.println("Teensy Recorder - Mode Debug");
    CModeGeneric::bDebug = true;
    logPR.SetbConsole( true);
    digitalWriteFast( 13, HIGH);
    delay(1000);
    digitalWriteFast( 13, LOW);
  }
  logPR.SetbConsole( true);
  
  // Test si changement de type d'écran
  char sScreen[30];
  sScreen[0] = 0;
  if (key == K_DOWN)
  {
    // On signale la détection du changement d'écran
    for (int i=0; i<3; i++)
    {
      digitalWriteFast( 13, HIGH);
      delay(300);
      digitalWriteFast( 13, LOW);
      delay(300);
    }
    // Changement du type d'écran
    switch (CModeGeneric::GetCommonsParams()->iScreenType)
    {
    case ST_SSD1306: CModeGeneric::GetCommonsParams()->iScreenType = ST_SH1106; break;
    case ST_SH1106 :
    default        : CModeGeneric::GetCommonsParams()->iScreenType = ST_SSD1306;
    }
    // Sauvegarde des paramètres y compris le nouveau choix d'écran
    //Serial.println("StaticWriteParams on Setup modif écran");
    CModeGeneric::StaticWriteParams();
  }
  // Init de la chaine affichée sur le logo
  switch (CModeGeneric::GetCommonsParams()->iScreenType)
  {
  case ST_SSD1306: strcpy( sScreen, "SSD1306"); break;
  case ST_SH1106 :
  default        : strcpy( sScreen, "SH1106" );
  }
  // Création du gestionnaire d'écran
  CreateGestScreen();

  // Si demandé, on baisse la luminosité de l'écran
  if (CModeGeneric::GetCommonsParams()->bLumiereMin)
    pDisplay->setContrast(1);
  else
    pDisplay->setContrast(255);

 // Mémo du temps pour le delai d'affichage du logo
  unsigned long uiTimeLogoA = millis();
  // If normal restart, print logo
  // Else, wakeup restart, logo is not print
  if (CModeVeille::iWhoWakeUp == 0)
    // Affichage du logo et de la version
    AffLogo();
  else
  {
    pDisplay->clearBuffer();
    //pDisplay->setCursor(0,0);
    //pDisplay->println("Wake up");
    pDisplay->sendBuffer();
  }
   Serial.println("Logo OK");
  // Initialisation du mode de fin d'erreur
  CModeError::SetNewMode( MPARAMS);

  // Initialisation du pointeur sur l'utilisation de la LED
  piUtilLed = &(CModeGeneric::GetCommonsParams()->iUtilLed);
   Serial.println("Avant StaticBeginMode");
  // Initialisation lecture tensions batteries
  CModeGeneric::StaticBeginMode();
   Serial.println("Avant initSD");
  // Initialisation de la carte SD
  if (!InitSD())
  {
    // Mesure des batteries
    if (!CModeGeneric::CheckBatteries(MAX_WAITMESURE))
      // A partir de 10%, il est fortement conseillé de charger sa batterie, on impose ce message d'erreur
      CModeError::SetTxtError((char *)txtLOWBAT[CModeGeneric::GetCommonsParams()->iLanguage]);
    else
      CModeError::SetTxtError((char *)txtSDERROR[CModeGeneric::GetCommonsParams()->iLanguage]);
    // Passage en mode erreur
    CModeGeneric::ChangeMode( MERROR);
  }
  else
  {
    Serial.printf("InitSD OK iModeRecord %d\n", CModeGeneric::GetParamsOperator()->iModeRecord);
    LogFile::StartLog();
    if (strlen(sWho) > 0)
      LogFile::AddLog( LLOG, sWho);
    // Test if AutoTime.ini is use
    CModeGeneric::AutoTime();
    // Test if AutoProfiles.ini exist
    CModeGeneric::AutoProfiles();
    //Serial.println("LogFile::StartLog OK");
    // Init de la callback pour dater les fichiers
    SdFile::dateTimeCallback(dateTime);
    //Serial.println("dateTimeCallback OK");
    char txtCPU[30];
#if defined(__IMXRT1062__) // Teensy 4.1
	  if (external_psram_size == 0)
      strcpy( txtCPU, "T4.1");
    else
	    sprintf( txtCPU, "T4.1 %dMO", external_psram_size);
    // Init de la fréquence processeur à 150MHz pour consommer moins
    set_arm_clock(150000000);
    //Serial.printf("setup set_arm_clock(150000000) F_CPU_ACTUAL %d, F_BUS_ACTUAL %d\n", F_CPU_ACTUAL, F_BUS_ACTUAL);
    if (bPrintStart) Serial.printf("Teensy 4.1 F_CPU %d, F_CPU_ACTUAL %d, F_BUS_ACTUAL %d\n", F_CPU, F_CPU_ACTUAL, F_BUS_ACTUAL);
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
    strcpy( txtCPU, "T3.6");
#endif
    bool bOKLogA=true, bOKLogB=true;
    bOKLogA = LogFile::AddLog( LLOG, "==========================================");
    //Serial.println("AddLog OK");
    if (iType == PASSIVE_RECORDER)
    {
      if (bPrintStart)
        bOKLogB = LogFile::AddLog( LLOG, txtSTARTPR[CModeGeneric::GetCommonsParams()->iLanguage], uiSerialNumber, VERSION, F_CPU, txtCPU);
    }
    else if (iType == ACTIVE_RECORDER)
    {
     if (bPrintStart)
       bOKLogB = LogFile::AddLog( LLOG, txtSTARTAR[CModeGeneric::GetCommonsParams()->iLanguage], uiSerialNumber, VERSION, F_CPU, txtCPU);
      // Indique l'absence de bouton Left/Right
      CGenericModifier::SetbLeftRight( false);
    }
    else if (iType == PASSIVE_STEREO)
    {
      if (bPrintStart)
        bOKLogB = LogFile::AddLog( LLOG, txtSTARTPRS[CModeGeneric::GetCommonsParams()->iLanguage], uiSerialNumber, VERSION, F_CPU, txtCPU);
    }
    else if (iType == PASSIVE_SYNCHRO)
    {
#if defined(__MK66FX1M0__) // Teensy 3.6
      // Init des broches de commande de l'ESP32
      pinMode( PIN23ESPON,    OUTPUT);
      pinMode( PIN22ESPREADY, INPUT_PULLUP);
      pinMode( PIN04MASTER,   OUTPUT);
      digitalWriteFast( PIN23ESPON,  LOW); // EPS32 OFF
      digitalWriteFast( PIN04MASTER, LOW); // No master
      // Init de la pin Master/Slave
      if (CModeGeneric::GetParamsOperator()->iMasterSlave == MASTER)
        digitalWriteFast( PIN04MASTER, HIGH); // Master
#endif
      if (bPrintStart)
        bOKLogB = LogFile::AddLog( LLOG, txtSTARTPRSS[CModeGeneric::GetCommonsParams()->iLanguage], uiSerialNumber, VERSION, F_CPU, txtCPU);
    }
    else if (iType == PASSIVE_MSYNCHRO)
    {
#if defined(__MK66FX1M0__) // Teensy 3.6
      // Init des broches de commande de l'ESP32
      pinMode( PIN23ESPON,    OUTPUT);
      pinMode( PIN22ESPREADY, INPUT_PULLUP);
      pinMode( PIN04MASTER,   OUTPUT);
      digitalWriteFast( PIN23ESPON,  LOW); // EPS32 OFF
      digitalWriteFast( PIN04MASTER, LOW); // No master
      // Init de la pin Master/Slave
      if (CModeGeneric::GetParamsOperator()->iMasterSlave == MASTER)
        digitalWriteFast( PIN04MASTER, HIGH); // Master
#endif
      if (bPrintStart)
        bOKLogB = LogFile::AddLog( LLOG, txtSTARTPRMS[CModeGeneric::GetCommonsParams()->iLanguage], uiSerialNumber, VERSION, F_CPU, txtCPU);
    }
    else
      Serial.println("Type indéterminé !!!!");
    if (MAINTAIN_FREE_CLUSTER_COUNT != 1)
      bOKLogB = LogFile::AddLog( LLOG, txtSDFATCFG[CModeGeneric::GetCommonsParams()->iLanguage]);
    // Appel au premier freeClusterCount qui prend du temps avec des cartes >16GO (4.5s sur une 32GO),
    // ensuite, si MAINTAIN_FREE_CLUSTER_COUNT est à 1 dans SdFatConfig.h, l'appel ne prend pas trop de temps
    //unsigned long iTime1 = millis();
    sd.vol()->freeClusterCount();
    bool bSDFull = false;
    float fSize = CSDModifier::GetSDFreeSpace();
    if (fSize <= 0.05)
    {
      // Plus de place sur la carte SD, on passe en erreur carte pleine
      LogFile::AddLog( LLOG, txtLogSDErrSize[CModeGeneric::GetCommonsParams()->iLanguage], fSize);
      bSDFull = true;
    }
    /*unsigned long iTime2 = millis();
    Serial.print("freeClusterCount=");
    Serial.print(iTime2-iTime1);
    Serial.println("ms");*/
    //Serial.printf("Démarrage Mode %d\n", CModeGeneric::GetParamsOperator()->iModeRecord);

    // Delai d'affichage du logo
    unsigned long uiTimeLogoB = millis();
    if (CModeVeille::iWhoWakeUp == 0 and (uiTimeLogoB - uiTimeLogoA) < 2000)
      delay(1500 - (uiTimeLogoB - uiTimeLogoA));
    uiTimeKey  = millis() + TIMEKEY;
    uiTimeAff  = millis() + TIMEAFF;
    /*CModeGeneric::bDebug = true;
    CModeGeneric::ChangeMode( MPARAMS);
    return;*/
    if (bOKLogA == false or bOKLogB == false)
    {
      // Passage en mode erreur
      CModeError::SetTxtError((char *)txtSDWERROR[CModeGeneric::GetCommonsParams()->iLanguage]);
      CModeGeneric::ChangeMode( MERROR);
    }
    else if (bSDFull)
    {
      CModeError::SetNewMode( MPARAMS, 0); // Pas de reset après 2mn, on reste en mode erreur avec une carte saturée
      CModeError::SetTxtError( (char *)txtErrFullSD[CModeGeneric::GetCommonsParams()->iLanguage]);
      CModeGeneric::ChangeMode( MERROR);
    }
#if defined(__IMXRT1062__) // Teensy 4.1
    else if (CModeVeille::iWhoWakeUp != 0 and CModeGeneric::IsTimeVeille())
      // Passage en mode veille
      CModeGeneric::ChangeMode( MVEILLE);
    else if (CModeVeille::iWhoWakeUp != 0 and CModeGeneric::GetParamsOperator()->iModeRecord == TIMEDREC)
      // Passage en mode enregistrement cadencé
      CModeGeneric::ChangeMode( MTRECORD);
#endif
    else if (CModeGeneric::GetParamsOperator()->iModeRecord == PRHINOLOG)
      // Passage en mode RhinoLogger
      CModeGeneric::ChangeMode( MRHINOL);
    else if (CModeGeneric::GetParamsOperator()->iModeRecord == AUDIOREC and iType == ACTIVE_RECORDER)
      // Passage en mode enregistrement audio
      CModeGeneric::ChangeMode( MAUDIOREC);
    else if (CModeGeneric::GetParamsOperator()->iModeRecord == TIMEDREC)
      // Passage en mode enregistrement cadencé
      CModeGeneric::ChangeMode( MTRECORD);
    else if (CModeGeneric::GetParamsOperator()->iModeRecord == PROTFIXE)
      // Passage en mode protocole point fixe
      CModeGeneric::ChangeMode( MPROPFX);
    else if (CModeGeneric::GetParamsOperator()->iModeRecord == SYNCHRO)
      // Passage en mode enregistrement auto synchro
      CModeGeneric::ChangeMode( MSYNCHRO);
    else if (CModeGeneric::GetParamsOperator()->iModeRecord == RECAUTO or iType == PASSIVE_RECORDER or F_CPU < 50000000)
      // Passage en mode enregistrement auto
      CModeGeneric::ChangeMode( MRECORD);
    else if (iType == ACTIVE_RECORDER)
    {
      //delay(200);
      // Switch to Heterodyne mode
      CModeGeneric::ChangeMode( MHETER);
    }      
    else
      // Faute de mieux, passage en mode paramètres
      CModeGeneric::ChangeMode( MPARAMS);
    //CModeGeneric::ChangeMode( MVEILLE);
  }
}

//#define TESTTIMEKEY
//#define TESTTIMEPRINT

#ifdef TESTTIMEKEY
  unsigned long uiTestTimeKey1, uiTestTimeKey2;
#endif

#ifdef TESTTIMEPRINT
  unsigned long uiTestTimePrint1, uiTestTimePrint2;
#endif

//unsigned long uiLoop = 0;
/*------------------------------------------------------------------------------------------------------
 * Tâche de fond, gère les accès claviers et les états
------------------------------------------------------------------------------------------------------*/
void loop()
{
  /*if (uiLoop == 0) uiLoop = millis();
  Serial.printf("loop %dms\n", millis() - uiLoop);*/
  if (*piUtilLed == LEDLOOP)
    digitalWriteFast(LED_BUILTIN, HIGH);
  unsigned long uiMillis = millis();
  /*if (uiMillis > uiLoop)
  {
    Serial.println("loop");
    uiLoop = uiMillis + 100;
  }*/
  // Traitement des tâches de fond du mode courant
  CModeGeneric::GetpCurrentMode()->OnLoop();
  if (uiMillis > uiTimeKey)
  {
#ifdef TESTTIMEKEY
    uiTestTimeKey1 = micros();
#endif
    //Serial.println("loop GetKey");
    // Traitement des boutons par le mode courant
    unsigned short key = KeyManag.GetKey();
    /*if (CModeGeneric::bDebug)
    {
      switch (key)
      {
      case K_UP      : Serial.println("Key Up")           ; break;
      case K_DOWN    : Serial.println("Key Down")         ; break;
      case K_PUSH    : Serial.println("Key Push")         ; break;
      case K_LEFT    : Serial.println("Key Left")         ; break;
      case K_RIGHT   : Serial.println("Key Right")        ; break;
      case K_MODEA   : Serial.println("Key change mode A"); break;
      case K_MODEB   : Serial.println("Key change mode B"); break;
      }
    }*/
    int iNewMode = CGenericMode::GetpCurrentMode()->KeyManager( key);
    if (iNewMode != NOMODE and iNewMode != CGenericMode::GetCurrentMode())
    {
      if (iNewMode == MSYNCHROR)
        // Reset synchro mode
        iNewMode = MSYNCHRO;
      else if (iNewMode == MRESTARTTEST)
        // New microphone test
        iNewMode = MPROTST;
      // Changing the operating mode
      CGenericMode::ChangeMode( iNewMode);
    }
    uiTimeKey = uiMillis + TIMEKEY;
#ifdef TESTTIMEKEY
    // In Heterodyne mode : 4µs
    uiTestTimeKey2 = micros();
    Serial.printf("Loop, button processing time %dµs\n", uiTestTimeKey2-uiTestTimeKey1); 
#endif
    //Serial.println("loop GetKey OK");
  }
  if (uiMillis > uiTimeAff)
  {
#ifdef TESTTIMEPRINT
    uiTestTimePrint1 = micros();
#endif
    // Affichage du mode courant
    CModeGeneric::GetpCurrentMode()->PrintMode();
    uiTimeAff = uiMillis + TIMEAFF;
#ifdef TESTTIMEPRINT
    // Print time every 300ms
    // In Heterodyne mode : 82/84ms
    // In Read       mode : 14ms
    // In Parameters mode : 16.3ms
    uiTestTimePrint2 = micros();
    Serial.printf("Loop, print processing time %dµs\n", uiTestTimePrint2-uiTestTimePrint1); 
#endif
  }
  if (*piUtilLed == LEDLOOP)
  {
    digitalWriteFast(LED_BUILTIN, LOW);
    // Ajout d'un delai de 1ms sinon il est quasi impossible de voire le créneau bas du signal
    delay(1);
  }
  //Serial.println("loop OK");
}
