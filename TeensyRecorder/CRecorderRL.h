//-------------------------------------------------------------------------
//! \file CRecorderRL.h
//! \brief Classe de gestion de l'acquisition en mode RhinoLogger
//! classe CRecorderRL
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/* 
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "arm_math.h"
#include <vector>
#include "SdFat.h"
#include "Acquisition.h"
#include "CAnalyseRL.h"
#include "CRecorder.h"

#ifndef CRECORDERRL_H
#define CRECORDERRL_H

//-------------------------------------------------------------------------
//! \class CRecorderRL
//! \brief Classe de gestion de l'acquisition/nanalyse pour le mode RhinoLogger
class CRecorderRL: public CGenericRecorder
{
  public:
    //-------------------------------------------------------------------------
    //! \brief Constructeur (initialisation de la classe)
    CRecorderRL(
      ParamsOperator *pParams // Pointeur sur les paramètres opérateur
      );
  
    //-------------------------------------------------------------------------
    //! \brief Destructeur
    virtual ~CRecorderRL();
  
    //-------------------------------------------------------------------------
    //! \brief Démarre le mode acquisition
    //! Retourne true si démarrage OK
    virtual bool StartAcquisition();
  
    //-------------------------------------------------------------------------
    //! \brief Stoppe le mode acquisition
    //! Retourne true si arrêt OK
    virtual bool StopAcquisition();
  
    //-------------------------------------------------------------------------
    //! \brief Stoppe ou relance l'acquisition
    //! \param bAcq true pour lancer l'acquisition, false pour la suspendre
    virtual void SetAcquisition(
      bool bAcq
      );
  
    //-------------------------------------------------------------------------
    //! \brief Gestion de l'IT DMA lorsqu'un buffer d'acquisition est plein
    virtual void OnDMA_ISR();
  
    //-------------------------------------------------------------------------
    //! \brief Traitement dans l'IT DMA d'un buffer d'acquisition plein
    virtual void TraiteBuffer();
    
    //-------------------------------------------------------------------------
    //! \brief Préparation du traitement FFT sans filtre
    virtual void PrepareFFTWithoutFilter();
    
    //-------------------------------------------------------------------------
    // ! \briefTraitement détections
    virtual void TraitementDetections();
    
    //----------------------------------------------------------------------------
    //! \brief Fonction de traitement du timer
    static void OnTimer();
    
    //----------------------------------------------------------------------------
    //! \brief Test si activité sur les bandes toutes les 200ms
    void OnPeriodeTimer();
    
    //-------------------------------------------------------------------------
    //! \brief Traitements dans la boucle principale du programme
    virtual void OnLoop();
  
    //----------------------------------------------------
    //! \brief Initialisation de la température, de l'hygrométrie et des tensions batteries dans une structure de mémorisation des activités
    //! \param pMemo Pointeur sur la structure de mémorisation
    void SetTHB(
      MemoTempHygroBat *pMemo
      );

    //----------------------------------------------------
    //! \brief Fonction de mémorisation des infos dans un fichier csv
    //! \param pFileName Nom du fichier
    //! \param data Mémo température, hygro et batteries
    //! \param pTHB Table des résultats à sauver dans le fichier
    //! \param pHour Heure de début des mesures
    //! \param pDate Date de début des mesures
    //! \param bJour Indique si c'est un fichier jour
    void MemoSD(
      char *pFileName,
      ResultAnalyse *data,
      MemoTempHygroBat *pTHB,
      char *pHour,
      char *pDate,
      bool bJour
      );
      
    //----------------------------------------------------------------------------
    //! \brief Récupération des infos de la minute courante d'un bande
    //! \param iBand Rang de la bande
    //! \param pResult Pointeur à renseigner
    void GetCurentMinute( 
      int iBand,
      ResultAnalyse *pResult
      );
    
    //----------------------------------------------------------------------------
    //! \brief Sauvegarde éventuelle des résultats dans les fichiers CSV
    void SaveCSVFiles();
    
    //-------------------------------------------------------------------------
    //! \brief Initialisation de la température de l'humidité et des tensions batteries
    //! \param c Température
    //! \param h Humidité
    //! \param fExt Tension de la batterie externe
    //! \param fInt Tension de la batterie interne
    void SetTHVBat( float c, float h, float fExt, float fInt) { bSonde=true; cTemp=c; humidity=h; fBatExt = fExt; fBatInt = fInt;};
    
    //----------------------------------------------------------------------------
    //! \brief Returns true if the CF memory occupancy is too large
    bool IsFCtooLarge() {return bFCtooLarge;};
    
  protected:
    //! Gestionnaire d'acquisition
    Acquisition acquisition;

    // Indique une trop grande occupation mémoire des FC (trop de canaux)
    bool bFCtooLarge;
    
    //! Pointeur sur le buffer à traiter
    volatile uint16_t *pBufferATraiter;
    //! Coefficients de la fenêtre de Han
    float TbWinHan[FFTLENRL];
    //! Coefficient du filtre passe haut (Frank DD4WH)
    float fDCBlockingFactor;
    //! Pour le filtre passe haut (Frank DD4WH)
    int32_t        filteredOutput;
    int32_t        scaledPreviousFilterOutput;
    static int32_t previousSample;
    static int32_t previousFilterOutput;
    // Nombre d'échantillon à traiter dans une FFT
    int            iNbEchFFT;

    //! List of Bands analysers
    CAnalyzerRL *LstAnalysers[MAXBD];

    //! Nb valid bands
    int iNbBands;

    //! Seconde courante
    int  iCurrentSecond;
    //! Minute courante
    int  iCurrentMinute;
    //! Indique qu'une minute est à sauvegarder
    bool bMinuteOK;
    //! Heure courante
    int  iCurrentHour;
    //! Indique d'une heure est à sauvegarder
    bool bHourOK;
    //! Indique qu'un jour est à sauvegarder
    bool bDayOK;
    //! Chaine de la date de début des mesures
    char sCurrentDateFile[MAXDATESMES];
    char sSaveDateFile   [MAXDATESMES];
    //! Chaine de l'heure de début des mesures
    char sTimeCurrentMinute[MAXTIMEMES];
    char sTimeCurrentHour  [MAXTIMEMES];
    char sTimeCurrentDay   [MAXTIMEMES];
    char sTimeSaveMinute   [MAXTIMEMES];
    char sTimeSaveHour     [MAXTIMEMES];
    char sTimeSaveDay      [MAXTIMEMES];
    //! Memo des températures, hygrométrie et tension batteries
    MemoTempHygroBat THBMinute;
    MemoTempHygroBat THBHour;
    MemoTempHygroBat THBDay;
    //! Nom des fichiers de sauvegarde
    char FileNameMN[MAXFILESNAME];
    char FileNameH [MAXFILESNAME];
    char FileNameJ [MAXFILESNAME];

    //! Timer 200ms
    IntervalTimer TimerPeriod;
    
    //! Indices min et max de la bande GR pour éviter les harmoniques
    static short       idxMinGR, idxMaxGR;
    //! Indice min de la bande RE-PR, le max étant MFFTLENRL
    static short       idxMinREPR;
    //! Indices min et max des bandes
    static short TbIdxMin[MAXBD], TbIdxMax[MAXBD];
    //! Indicateur d'activité
    volatile bool bActivite;
  
    //! RAZ Watchdog pour surveiller le fonctionnement (RAZ dans le loop, incrément dans les IT, si trop grand alors RESET)
    static short iWatchDogITTimer;
    static short iWatchDogITDMA;

    //! Tensions batteries
    float fBatInt, fBatExt;
    // Présence de la sonde de température
    bool bSonde;
    // Température
    float cTemp;
    // Humidité
    float humidity;
};

#endif // CRECORDERRL_H
