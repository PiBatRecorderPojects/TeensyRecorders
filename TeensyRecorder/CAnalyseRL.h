//-------------------------------------------------------------------------
//! \file CAnalyseRL.h
//! \brief Classes gd'analyse en mode RhinoLogger
//! \details class CAnalyzerRL, CQFCAnalyzerRL, CFMAnalyzerRL
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 /*
  * Classes :
  * - CAnalyzerRL
  *   - CFCAnalyzerRL
  *   - CQFCAnalyzerRL
  *   - CFMAnalyzerRL
  */
#include <Arduino.h>
#include "Const.h"

#ifndef CANALYZERRL_H
#define CANALYZERRL_H

//-------------------------------------------------------------------------
//! \class CAnalyzerRL
//! \brief Generic class for analyzing cries
class CAnalyzerRL
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  CAnalyzerRL();

  //-------------------------------------------------------------------------
  //! \brief Destructor
  virtual ~CAnalyzerRL();

  //-------------------------------------------------------------------------
  //! \brief Initialization of the characteristics of the analysis
  //! \param iFe Sampling frequency
  //! \param pBand Band characteristics
  //! \param bDebugDet For debugging bands
  virtual void InitializingAnalysis(
    uint32_t iFe,
    MonitoringBand *pBand,
    bool bDebugDet
    );

  //-------------------------------------------------------------------------
  //! \brief Initialization before the start of the analysis time of the timer period
  virtual void RAZBeforeTimer();
  
  //-------------------------------------------------------------------------
  //! \brief Harmonic processing for Rhinolophus cries
  //! \param piTbSeuilCnx Table of thresholds
  //! \param pFFTResults Table of FFT levels
  static void HarmonicProcessing(
    volatile uint32_t *piTbSeuilCnx,
    volatile  int32_t *pFFTResults
    );

  //-------------------------------------------------------------------------
  //! \brief Harmonic analysis on the timer period
  static void HarmonicAnalysis();
  
  //-------------------------------------------------------------------------
  //! \brief Processing FFT results
  //! \param piTbSeuilCnx Table of thresholds
  //! \param pFFTResults Table of FFT levels
  virtual void FFTProcessing(
    volatile uint32_t *piTbSeuilCnx,
    volatile  int32_t *pFFTResults
    );

  //-------------------------------------------------------------------------
  //! \brief Cries analysis in the timer period
  virtual void OnTimerProcessing();

  //-------------------------------------------------------------------------
  //! \brief Update the current second
  virtual void UpdateCurrentSecond();

  //-------------------------------------------------------------------------
  //! \brief Update the current minute
  virtual void UpdateCurrentMinute();

  //-------------------------------------------------------------------------
  //! \brief Storing the current minute
  virtual void StoreCurrentMinute();

  //-------------------------------------------------------------------------
  //! \brief Update the current hour
  virtual void UpdateCurrentHour();

  //-------------------------------------------------------------------------
  //! \brief Storing the current hour
  virtual void StoreCurrentHour();

  //-------------------------------------------------------------------------
  //! \brief Update the current day
  virtual void UpdateCurrentDay();

  //-------------------------------------------------------------------------
  //! \brief Storing the current day
  virtual void StoreCurrentDay();

  //-------------------------------------------------------------------------
  //! \brief Set debug mode
  static void SetDebug( bool debug) {bDebug = debug;};

  //-------------------------------------------------------------------------
  //! \brief Returns the current minute results
  ResultAnalyse *GetCurrentMinute() {return &CurrentMinute;};

  //-------------------------------------------------------------------------
  //! \brief Returns the last minute saved results
  ResultAnalyse *GetSavedMinute() {return &SaveMinute;};

  //-------------------------------------------------------------------------
  //! \brief Returns the last hour saved results
  ResultAnalyse *GetSavedHour() {return &SaveHour;};

  //-------------------------------------------------------------------------
  //! \brief Returns the last day saved results
  ResultAnalyse *GetSavedDay() {return &SaveDay;};

  //-------------------------------------------------------------------------
  //! \brief Returns the min channel index of the band
  short GetidxMin() {return idxMin;};

  //-------------------------------------------------------------------------
  //! \brief Returns the max channel index of the band
  short GetidxMax() {return idxMax;};

  //-------------------------------------------------------------------------
  //! \brief Returns the min channel index of the Rhino band
  static short GetidxMinRhino() {return idxMinGR;};

  //-------------------------------------------------------------------------
  //! \brief Returns the max channel index of the Rhino band
  static short GetidxMaxRhino() {return MFFTLENRL;};

protected:
  //! Operator characteristics of the band to be analyzed
  MonitoringBand Band;
  //! Indexes of the minimum and maximum channels of the band
  short idxMin, idxMax;
  //! Results of the current timer period
  ResultAnalyseSeconde CurrentTimer;
  //! Results of the current second
  ResultAnalyseSeconde CurrentSecond;
  //! Results of the current minute
  ResultAnalyse        CurrentMinute;
  //! Results of the minute to save
  ResultAnalyse        SaveMinute;
  //! Results of the current hour
  ResultAnalyse        CurrentHour;
  //! Results of the hour to save
  ResultAnalyse        SaveHour;
  //! Results of the current day
  ResultAnalyse        CurrentDay;
  //! Results of the day to save
  ResultAnalyse        SaveDay;
  //! Current second for contacts analyse
  short                iCurentSec;

  //! Variables common to all instances
  //! Debug mode
  static bool bDebug;
  //! Sampling frequency
  static uint32_t    uiFe;
  //! Width of a channel in Hz
  static int         iCanal;
  //! Min and max index of the GR band to avoid harmonics
  static short       idxMinGR, idxMaxGR;
  //! Min index of the RE-PR band, the max being MFFTLENRL-1
  static short       idxMinREPR;
  //! Detection tables of Rhinolophus for the treatment of harmonics
  //! Table of current FC detections
  static DetectionFC TbCurrentRhino[MFFTLENRL];
  //! Backup table for the longest CF on each channel
  static DetectionFC TbSaveRhino[MFFTLENRL];
  //! Activity presence indicator of Rhinolophus to block FM
  static bool        bRhino;
  //! Hidden channels to avoid GR, RE and PR harmonics
  static bool        TbMsq[MFFTLENRL];
  //! For debugging all detections if name = "deb"
  bool               bDebugAll;
  //! Duration of FFT samples (4ms if FFT 1024 without zeropadding, 2ms with zeropadding 512 and 1ms if zeropadding 256)
  static int         iTimeFFTRL;
};

//-------------------------------------------------------------------------
//! \class CFCAnalyzerRL
//! \brief Class for FC Rhinolophus type cries analysis
class CFCAnalyzerRL: public CAnalyzerRL
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  CFCAnalyzerRL();

  //-------------------------------------------------------------------------
  //! \brief Destructor
  virtual ~CFCAnalyzerRL();

  //-------------------------------------------------------------------------
  //! \brief Initialization of the characteristics of the analysis
  //! \param iFe Sampling frequency
  //! \param pBand Band characteristics
  //! \param bDebugDet For debugging bands
  virtual void InitializingAnalysis(
    uint32_t iFe,
    MonitoringBand *pBand,
    bool bDebugDet
    );

  //-------------------------------------------------------------------------
  //! \brief Initialization before the start of the analysis time of the timer period
  virtual void RAZBeforeTimer();
  
  //-------------------------------------------------------------------------
  //! \brief Processing FFT results
  //! \param piTbSeuilCnx Table of thresholds
  //! \param pFFTResults Table of FFT levels
  virtual void FFTProcessing(
    volatile uint32_t *piTbSeuilCnx,
    volatile  int32_t *pFFTResults
    );

  //-------------------------------------------------------------------------
  //! \brief Cries analysis in the timer period
  virtual void OnTimerProcessing();

protected:
  // Number of channels in the band
  int iNbCnx;
  // Detections duration and level max per channel over a timer period
  DetectionFC *pTimerDetections;
};


//-------------------------------------------------------------------------
//! \class CQFCAnalyzerRL
//! \brief Class for FC type cries analysis
class CQFCAnalyzerRL: public CAnalyzerRL
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  CQFCAnalyzerRL();

  //-------------------------------------------------------------------------
  //! \brief Destructor
  virtual ~CQFCAnalyzerRL();

  //-------------------------------------------------------------------------
  //! \brief Initialization of the characteristics of the analysis
  //! \param iFe Sampling frequency
  //! \param pBand Band characteristics
  //! \param bDebugDet For debugging bands
  virtual void InitializingAnalysis(
    uint32_t iFe,
    MonitoringBand *pBand,
    bool bDebugDet
    );

  //-------------------------------------------------------------------------
  //! \brief Initialization before the start of the analysis time of the timer period
  virtual void RAZBeforeTimer();
  
  //-------------------------------------------------------------------------
  //! \brief Processing FFT results
  //! \param piTbSeuilCnx Table of thresholds
  //! \param pFFTResults Table of FFT levels
  virtual void FFTProcessing(
    volatile uint32_t *piTbSeuilCnx,
    volatile  int32_t *pFFTResults
    );

  //-------------------------------------------------------------------------
  //! \brief Cries analysis in the timer period
  virtual void OnTimerProcessing();

protected:
  // Number of channels in the band
  int iNbCnx;
  // CF Detection Charts
  // Table of current FC detections
  DetectionFC *pTbCurrentFC;
  // Longest FC backup table on each channel during timer period
  DetectionFC *pTbSaveFC;
};

//-------------------------------------------------------------------------
//! \class CFMAnalyzerRL
//! \brief Class for FM type cries analysis
class CFMAnalyzerRL: public CAnalyzerRL
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  CFMAnalyzerRL();

  //-------------------------------------------------------------------------
  //! \brief Initialization before the start of the analysis time of the timer period
  virtual void RAZBeforeTimer();
  
  //-------------------------------------------------------------------------
  //! \brief Processing FFT results
  virtual void FFTProcessing(
    volatile uint32_t *piTbSeuilCnx,  // Table of thresholds
    volatile  int32_t *pFFTResults    // Table of FFT levels
    );

  //-------------------------------------------------------------------------
  //! \brief Cries analysis in the timer period
  virtual void OnTimerProcessing();

protected:
  // Table of current FM detections
  DetectionFM currentFM;
  // Memorizing the widest FM
  DetectionFM maxFM;
};

#endif // CANALYZERRL_H
