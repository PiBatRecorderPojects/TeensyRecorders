//-------------------------------------------------------------------------
//! \file WriteBMP16C.h
//! \brief Classe pour la création d'image type BMP 16 couleurs
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/* 
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "SdFat.h"

#ifndef WRITEBMP16C_H
#define WRITEBMP16C_H

//! Taille du header
#define BMP16CHEADERSIZE 50

//! Nombre de couleurs
#define NB_COLORS 16

//-------------------------------------------------------------------------
//! \class BMP16C
//! \brief Classe pour la création d'un fichier image type BMP 16 couleurs
//! \brief Coordonnées base 0 (0,0 = coin haut gauche)
class BMP16C
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructeur (initialisation de la classe)
  //! \param width Largeur en pixel
  //! \param height Hauteur en pixel
  //! \param color Couleur de fond
  BMP16C(
    int width,
    int height,
    int color
    );

  //-------------------------------------------------------------------------
  //! \brief Destructeur
  ~BMP16C();

  //-------------------------------------------------------------------------
  //! \brief Initialise un pixel
  //! \param x Position x du pixel
  //! \param y Position y pixel
  //! \param c Couleur du pixel
  void setPixel(
    int x,
    int y,
    unsigned char c
  );
  
  //-------------------------------------------------------------------------
  //! \brief Dessine une ligne d'un pixel d'épaisseur
  //! \param x0, y0 Position x, y de départ
  //! \param y1, y1 Position x, y d'arrivée
  //! \param c Couleur de remplissage
  void drawLine(
    int x0, int y0,
    int x1, int y1, 
    unsigned char c
  );
  
  //-------------------------------------------------------------------------
  //! \brief Dessine un rectangle avec des lignes d'un pixel
  //! \param x Position x du coin haut gauche
  //! \param y Position y du coin haut gauche
  //! \param width Largeur en pixel
  //! \param height Hauteur en pixel
  //! \param c Couleur des lignes
  void drawRectangle(
    int x,
    int y,
    int width,
    int height,
    unsigned char c
  );
  
  //-------------------------------------------------------------------------
  //! \brief Dessine un rectangle rempli
  //! \param x Position x du coin haut gauche
  //! \param y Position y du coin haut gauche
  //! \param width Largeur en pixel
  //! \param height Hauteur en pixel
  //! \param c Couleur de remplissage
  void fillRectangle(
    int x,
    int y,
    int width,
    int height,
    unsigned char c
  );
  
  //-------------------------------------------------------------------------
  //! \brief Ecriture d'un caractère
  //! \param x Position x du coin haut gauche
  //! \param y Position y du coin haut gauche
  //! \param color Couleur de remplissage
  //! \param c Caractère à afficher
  bool printChar(
    int x,
    int y,
    unsigned char color,
    char c
  );
  
  //-------------------------------------------------------------------------
  //! \brief Ecriture d'une chaine de caractères
  //! \param x Position x du coin haut gauche
  //! \param y Position y du coin haut gauche
  //! \param color Couleur de remplissage
  //! \param pStr Chaine de caractères à afficher
  bool printString(
    int x,
    int y,
    unsigned char color,
    char *pStr
  );
  
  //-------------------------------------------------------------------------
  //! \brief Ecriture du fichier bmp sur la carte SD
  //! \param fileName BMP file name
  bool write(char *fileName);
  
  //-------------------------------------------------------------------------
  //! \brief Retourne la largeur de l'image en pixel
  int getWidth();
  
  //-------------------------------------------------------------------------
  //! \brief Retourne la hauteur de l'image en pixel
  int getHeight();

private:
  //-------------------------------------------------------------------------
  //! Pointeur sur la bitmap de l'image
  unsigned char *pBMP;
  
  //-------------------------------------------------------------------------
  //! Largeur de l'image en pixels
  uint32_t iWidth;

  //-------------------------------------------------------------------------
  //! Largeur d'une ligne en octets, y compris les octets de réserve pour le modulo 4 octets
  uint32_t iSizeLine;

  //-------------------------------------------------------------------------
  //! Taille en octet de la bitmap
  uint32_t iSizeBMP;

  //-------------------------------------------------------------------------
  //! Hauteur de l'image en pixels
  uint32_t iHeight;
};

//-------------------------------------------------------------------------
//! \struct BMP16C_Header
//! \brief Structure de l'entête d'un fichier BMP 16 couleurs
struct BMP16C_Header
{   
  char BM[2];
  uint32_t uiFileSize;
  uint32_t uiReserved;
  uint32_t uiOffset;
  uint32_t uiBitmapInfoSize;
  uint32_t uiWidth;
  uint32_t uiHeight;
  uint16_t uiNbPlan;
  uint16_t uiBitsByPixel;
  uint32_t uiCompression;
  uint32_t uiSizeOfPicture;
  uint32_t uiHorizontalResolution;
  uint32_t uiVerticalResolution;
  uint32_t uiColorsNumber;
};

//-------------------------------------------------------------------------
//! \struct Color
//! \brief Structure pour la gestion des couleurs
struct Color
{   
  //-------------------------------------------------------------------------
  //! \brief Constructeur pour une couleur définie en RVB
  //! \param red Valeur de la couleur rouge
  //! \param green Valeur de la couleur verte
  //! \param blue Valeur de la couleur bleue
  Color(
    unsigned char red,
    unsigned char green,
    unsigned char blue,
    unsigned char alpha = 0
    ): iRed(red), iGreen(green), iBlue(blue), iAlpha(0)
  {
  }

  //-------------------------------------------------------------------------
  //! \brief Constructeur par défaut pour une couleur blanche
  Color() : iRed(255), iGreen(255), iBlue(255), iAlpha(0)
  {
  }

  //-------------------------------------------------------------------------
  //! \brief Retourne la couleur rouge
  static Color red()     { return(Color(255, 0,     0)); }

  //-------------------------------------------------------------------------
  //! \brief Retourne la couleur verte
  static Color green()   { return(Color(0,   255,   0)); }

  //-------------------------------------------------------------------------
  //! \brief Retourne la couleur bleue
  static Color blue()    { return(Color(0,   0,   255)); }


  //-------------------------------------------------------------------------
  //! \brief Retourne la couleur cyan
  static Color cyan()    { return(Color(0,   255, 255)); }

  //-------------------------------------------------------------------------
  //! \brief Retourne la couleur magenta
  static Color magenta() { return(Color(255,   0, 255)); }

  //-------------------------------------------------------------------------
  //! \brief Retourne la couleur jaune
  static Color yellow()  { return(Color(255, 255,   0)); }

  //-------------------------------------------------------------------------
  //! \brief Retourne la couleur noir
  static Color black()   { return(Color(0,   0,     0)); }

  //-------------------------------------------------------------------------
  //! \brief Retourne la couleur blanche
  static Color white()   { return(Color(255, 255, 255)); }

  //-------------------------------------------------------------------------
  //! \brief Retourne la couleur orange
  static Color orange()   { return(Color(255, 128, 0)); }

  //-------------------------------------------------------------------------
  //! \brief Retourne la couleur grise
  static Color grey()   { return(Color(192, 192, 192)); }

  unsigned char iRed;   //! Red color
  unsigned char iGreen; //! Green color
  unsigned char iBlue;  //! Blue color
  unsigned char iAlpha; //! Alpha
};

//! \enum COLORBMP16C
//! \brief Indices des 16 couleurs
enum COLORBMP16C
{
  COLORWHITE    = 0,
  COLORRED      = 1,
  COLORGREEN    = 2,
  COLORBLUE     = 3,
  COLORCYAN     = 4,
  COLORMAGENTA  = 5,
  COLORYELLOW   = 6,
  COLORBLACK    = 7,
  COLORORANGE   = 8,
  COLORREDB     = 9,
  COLORGREENB   = 10,
  COLORBLUEB    = 11,
  COLORCYANB    = 12,
  COLORMAGENTAB = 13,
  COLORGREY1    = 14,
  COLORGREY2    = 15,
};

//-------------------------------------------------------------------------
//! Marge basse du graphe : 8 pixels des kHz, 3 pixels des traits verticaux, 1 pixel du trait horizontal, 4 de marge
#define MARGEBASSE 16

//-------------------------------------------------------------------------
//! Marge haute du graphe : 8 pixels du titre
#define MARGEHAUTE 8

//-------------------------------------------------------------------------
//! Marge gauche du graphe : 1 pixel de marge, 32 pixels des dB (-120), 3 pixels des traits horizontaux, 1 pixel du trait vertical
#define MARGEGAUCHE 37

//-------------------------------------------------------------------------
//! Marge droire du graphe : 1 pixel
#define MARGEDROITE 8

//-------------------------------------------------------------------------
//! Taille d'un tiret : 3 pixel
#define TIRET 3

//! \enum TRACETYPE
//! \brief Type des graphes
enum TRACETYPE
{
  TR_CURVE = 0,
  TR_MAX   = 1,
  TR_MIN   = 2
};

//-------------------------------------------------------------------------
//! \class Graphe16C
//! \brief Classe pour la création d'un graphe dans une BMP 16 couleurs
//! \brief Coordonnées base 0 (0,0 = coin haut gauche)
class Graphe16C
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructeur (initialisation de la classe)
  //! \param width Largeur en pixel
  //! \param height Hauteur en pixel
  //! \param color Couleur de fond
  //! \param dbMin Valeur min de l'échelle des dB
  //! \param dbMax Valeur max de l'échelle des dB
  //! \param dbStep Intervalle de l'échelle des dB
  //! \param khzMin Valeur min de l'échelle des kHz
  //! \param khzMax Valeur max de l'échelle des kHz
  //! \param khzStep Intervelle de l'échelle des kHz
  //! \param sTitle Titre du graphe
  //! \param sName Nom de fichier bmp
  Graphe16C(
    int width,
    int height,
    int color,
    int dbMin,
    int dbMax,
    int dbStep,
    int khzMin,
    int khzMax,
    int khzStep,
    char *sTitle,
    char *sName
    );

  //-------------------------------------------------------------------------
  //! \brief Destructeur
  ~Graphe16C();

  //-------------------------------------------------------------------------
  //! \brief Construction d'une courbe
  //! \param sLegende Légende de la courbe
  //! \param color Couleur de la courbe
  //! \param iType Type de la courbe
  //! \param NbPoints Nombre de points
  //! \param tbidB Tableau des valeurs du point en dB
  //! \param tbfkHz Tableau des valeurs du point en kHz
  void SetGraphe(
    char *sLegende,
    int iType,
    int color,
    int NbPoints,
    int   *tbidB,
    float *tbfkHz
    );
  
  //-------------------------------------------------------------------------
  //! \brief Ecriture du fichier sur la carte SD
  void WriteFile();

protected:
  //-------------------------------------------------------------------------
  //! \brief Retourne la valeur de y à partir d'un nombre de dB
  //! \param dB Nombre de dB
  int getYdB( int dB);

  //-------------------------------------------------------------------------
  //! \brief Retourne la valeur de x à partir d'un nombre de kHz
  //! \param kHz Nombre de kHz
  int getXkHz( int kHz);

  //-------------------------------------------------------------------------
  //! \brief Construction de l'axe des dB
  void AxedB();
  
  //-------------------------------------------------------------------------
  //! \brief Construction de l'axe des kHz
  void AxekHz();
  
private:
  //-------------------------------------------------------------------------
  //! Pointeur sur l'image type BMP
  BMP16C *pImage;

  //-------------------------------------------------------------------------
  //! Couleur de fond
  int BackgroundColor;

  //-------------------------------------------------------------------------
  //! Valeur min de l'échelle des dB
  int idBMin;

  //-------------------------------------------------------------------------
  //! Valeur max de l'échelle des dB
  int idBMax;

  //-------------------------------------------------------------------------
  //! Intervalle de l'échelle des dB
  int idBStep;

  //-------------------------------------------------------------------------
  //! Valeur min de l'échelle des kHz
  int ikHzMin;

  //-------------------------------------------------------------------------
  //! Valeur max de l'échelle des kHz
  int ikHzMax;

  //-------------------------------------------------------------------------
  //! Intervalle de l'échelle des kHz
  int ikHzStep;

  //-------------------------------------------------------------------------
  //! Titre du graphe
  char *psTitle;

  //-------------------------------------------------------------------------
  //! Nom de fichier bmp
  char *psName;

  //-------------------------------------------------------------------------
  //! Taille d'un kHz en pixel
  float fkHzPix;
   
  //-------------------------------------------------------------------------
  //! Taille d'un dB en pixel
  float fdBPix;

  //-------------------------------------------------------------------------
  //! Position de la future légende
  int yLegende;
};
#endif
