//-------------------------------------------------------------------------
//! \file Const.h
//! \brief Constantes du projet Teensy Recorder
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/* 
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <Arduino.h>

// Constantes du projet

#ifndef CONST_H
#define CONST_H

  /*
   * TO DO
   * - Mode plage horaire solaire
   * - Créer le mode Enregistrement programmé avec des périodes d'enregistrements définies par une heure de départ et une durée
   */
#define VERSION "V1.00"
  /*
   * V1.00
   * - Following the corrections of V0.99, correction of an initialization for Teensy 3.6
   */
  /*
   * V0.99
   * - If SD card unreadable at startup and low batteries, more explicit error message
   * - PR and PRS, display of the temperature line in the parameters, even in the absence of a probe
   * - Purging of some warnings
   * - Using Arduino IDE 2.1.1
   * - Heterodyne without graph, display the names of the last 3 recorded files
   * - Max recording time, calculation correction
   * - Consideration of the MCP3221 for the measurement of internal batteries
   * - MAX module, taking error mode into account
   * - Stereo recording, wav file name compatible with the Vigie-Chiro program (PaRecPR4146560_1_AAAAMMJJ_HHMMSSR.wav for Right trig or PaRecPR4146560_0_AAAAMMJJ_HHMMSSR.wav for left)
   * - Added CPU frequency to copyright screen
   * - Extended microphone test, reduction in the impact of frequencies above 150kHz on the final note
   */
  /*
   * V0.98
   * - Consideration of the AGC in the modifiable parameters in heterodyne mode
   * - Added automatic gain control on the heterodyne to reduce noise
   * - Micro test, on a PRS T3.6 at 48MHz, we use the sampling frequency of 192kHz
   * - Fixed external power measurement only for PR and PRS
   * - Fixed battery voltage reading error on some MAX modules
   * - Fixed MAX module handling (thanks Albert)
   * - Using TeensyDuino 1.58.1 with specific Snooze library for Teensy 4.1
   * - AR player, fixed setting to select [X10 Het Del] values
   */
  /*
   * V0.97
   * - AR and PRS with T4.1, button wake-up correction
   * - With pre-trigger >= 1s, if the min duration is equal to the pre-trigger, it is automatically increased by one second. The max duration is always at least 1s longer than the min duration
   * - Despite switching to the micro test, retention of the previous recording mode at the end of the micro test
   * - Teensy 4.1 with 8MB RAM, fixed crash if pre-trigger = 0s
   * - If memory problem, switch to error mode with an explicit message instead of doing a reset
   * - Addition of a parameter to correct the voltage measurement of the batteries
   * - Addition of a parameter to correct the voltage measurement of the external power supply
   * - Fixed max pre-trigger duration to 10s for 384kHz and 7s for 500kHz
   * - Using Arduino IDE 2.1.0
   */
  /*
   * V0.96
   * - Fix for loud clicks in heterodyne with Teensy 4.1
   * - In heterodyne, optional graph to avoid parasitic noises
   */
  /*
   * V0.95
   * - Fixed cyclic reset bug on AR with some T4.1
   * - Small correction in version management
   * - Selective Heterodyne function removed (T4.1 only), bug being analyzed
   */
  /*
   * V0.94
   * - Using Arduino IDE 2.0.3
   * - Using TeensyDuino 1.57.2 with specific Snooze library for Teensy 4.1
   * - PR and PRS, parameter mode, no temperature line if sensor absent
   * - Fixed RhinoLogger mode
   * - Fixed pre-trigger calculation
   * - Fixed cyclic reset bug on Teensy Recorder with some T4.1
   * - Using the "Zero padding" technique to improve timing accuracy of RhinoLogger mode with T4.1 or T3.6 with 144MHz
   */
  /*
   * V0.93
   * - Due to the limitation of the maximum file size (4GB), resumption of the maximum durations of the timed recordings according to the sampling frequency
   * - If the fixed point protocol is selected, this mode is kept, even after a wake or a reset
   * - Improved battery measurement with the MAX17043
   * - Parameters, added a line to display temperature and humidity for PR and PRS
   * - On language change, update of battery level, probe and SD card parameters
   */
  /*
   * V0.92
   * - PRS, right/left correction on microphone test and recording
   * - Timed recording, correction on the maximum duration depending on the sampling frequency
   * - Timed recording, displays recording duration and waiting time in HH:MM:SS
   * - Fixed systematic resetting of interest band to default values
   * - Improved versioning of settings
   * - Using TeensyDuino 1.57 with specific Snooze library for Teensy 4.1
   * - Using U8G2 library V2.33.15
   * - Heterodyne, added auto play mode
   */
  /*
   * V0.91
   * - Heterodyne and Audio Recorder modes, change threshold modification to avoid unintended changes
   * - In player mode, added the possibility to delete the selected file and consequently, change of the mode of choice of the type of reading (X1, X10 and Heterodyne)
   * - Changing Default Profile Names (Default, Bats 384kHz, Bats 500kHz, RhinoLogger, Audio 48kHz)
   * - SD formatting, some cards require a reset after formatting
   * - Fixed threshold handling while running heterodyne and audio mode
   * - At start-up, if the Fixed Point Protocol mode is requested, it automatically switches to this mode
   * - In micro test mode, a low threshold is imposed
   * - Stereo with decimation, correct right/left sample
   * - AR, added more selective heterodyne mode with Teensy 4.1 only
   * - Adaptation of sleep mode to Teensy 4.1 (specific Snooze library)
   * - With a Teensy 4.1 equipped with an additional memory of 8MB, management of a pre-trigger modifiable from 1 to 10
   * - Teensy 4.1 compatible software on PR and AR only
   * - On a PRS and timed recording, stereo recording if requested.
   * - On "Audio" profile, put X10 false by default
   * - Read mode on AR, authorization of heterodyne mode with 500kHz in time expansion
   * - Simplification of cross-platform SDFat use with use of the SdFs class for SD card management
   * - Fix sleep in timed recording mode
   * - Dealing with SD card write errors
   * - AR, playback mode, taking into account stereo files with mixes of the 2 channels and automatic attenuation to stay in the 12-bit domain
   */
  /*
   * V0.90
   * - Stop recording on loss of buffers. At the beginning of recording, the losses of buffers are frequent, especially on AR. We stop only in debug mode and when the minimum duration is reached.
   */
  /*
   * V0.89
   * - Set Teensyduino V1.56
   * - In heterodyne mode, if the sampling frequency is less than 250kHz, we force 384 or 250kHz depending on the CPU frequency
   * - Fixed profile renaming function
   * - For SF 96, 48 and 24kHz, acquisition at 240kHz, application of an anti-aliasing filter of, respectively, 48, 24 and 12kHz, then decimation to SF 96, 46 or 24kHz
   * - Profile selection parameter in second position
   * - Corrections of the saving of the band of interests in the profiles
   * - Temperature reading from the start of heterodyne mode (after 30s on previous versions)
   * - Management of the absence of a humidity temperature sensor or BMP280 (temperature only) or BME280 (temperature and humidity)
   * - Management of an AHT10 type sensor
   */
  /*
   * V0.88
   * - Set SDFAT V2.0.7
   * - Fixed timing mode recording time modification behavior
   * - No more RamUsage in the log
   * - Improved storage in the log of parameters used in audio mode
   * - Playback mode on AR, taking stereo files into account
   * - Playback mode on AR, taking into account the sampling frequencies 44100 and 24000kHz
   * - Heterodyne, elimination of audible pulsation on background noise (in return, some repetitions possible of a block of 42ms at 384kHz)
   * - FFT, improved magnitude calculations in extended microphone test and noise measurement
   * - Heterodyne, correction of the activity on the graph
   * - Switching of the preamp power supply, loop for slowly loading the 470µF capacity on the preamp so as not to disturb the processor and especially access to the SD card
   * - Road and pedestrian protocols, display of the recording time of the current point
   * - Periodic reset of the heterodyne frequency to alleviate a problem of heterodyne signal attenuation when the frequency does not change for a long time
   */ 
  /*
   * V0.87
   * - Set Arduino IDE 1.8.13
   * - Set SDFAT V2.0.6
   * - Timed recording, modifiable recording time and standby in hh:mm:ss
   * - Timed recording, maximum time limitation depending on the sampling frequency
   * - High pass filter, input type xx.xkHz if the sampling frequency is less than 192kHz
   * - On a PRS, if profile by default, we force the stereo mode
   * - First version for PRS-Synchro with SynTopESP32 V0.10
   * - Management at start-up of a possible AutoTime.ini file for setting the time without button or screen
   * - Management at start-up of a possible AutoProfiles.ini file for parameter initialization without button or screen
   * - Time display, correction of delays and untimely jumps of 2 or 3s
   * - Extended mic test result on PRS, Left / Right reverse
   * - On a PRS, crash on the sequence of a second extended microphone test via the "Up" key
   * - On a PRS in microphone test, if the stereo mode is mono L or R, we force the stereo mode
   * - OK test of a 400 GB SD card
   * - On AR correction of standby operation in automatic recording mode (problem on V0.86 in french language)
   */
  /*
   * V0.86
   * - Set U8G2 V2.28.10
   * - Set SDFAT V2.0.4 for working with SDXC in FAT64 (exFAT). Formatting to FAT32 is no longer necessary
   * - Use sd.format of SDFat for formating SD in FAT32 or exFAT
   * - Monitoring the status of the SD card and, in the event of a problem, re-initiation of the card
   * - Management of 4 languages with a single file for translation (ConstChar.c) but just French and English available
   * - Copyright, addition of the SyncTopESP32 software version in case of PRS-S and addition of the name of the recorder
   * - Automatic end of error mode after 2mn with a reset in auto recording mode except in case of SD full
   * - At startup, if the SD card is full (<50MB), switch to error mode
   * - Recorder type reading delay to correct some cases of erroneous reading on AR
   * - Stereo mode, L/R inversion of detections and microphone test
   */
  /*
   * V0.85
   * - Recording in the log of the results of the microphone test
   * - SD write-free test at startup, after standby, at each change of mode and on write wav file
   * - Automatic end of error mode with a reset in auto recording mode
   * - Addition of an improved microphone test mode with production of response curves
   */
  /*
   * V0.84
   * - Update SD Size after formating
   * - Correction right/left channels in stereo
   * - Adding Mono Synchro PR type
   * - Adding Synchro parameters
   * - Adding Synchro mode
   * - Improved reading of ini files
   * - Max duration, minimum authorization 1s in reading ini file
   * - Using setSyncInterval(10s) for better time management
   * - Set SDFAT 1.1.4 for best 128GO management
   * - Set TeensyDuino 1.53
   * - Correction of the processing of the digital low-pass filter too brutally "optimized" on V0.83
   * - Stereo operation setting only on stereo device
   * - AR, respect for the change of operating mode (Rec> Player> Parameters) for all operating modes
   * - Processing of pin D33 to power or not the microphone preamp (with hardware associated, gain 2mA for PR in sleep mode and 4mA pour a PRS)
   * - Adding 24kHz sample frequency for audio recording
   * - Set max Recording time to 18000 (5h) in Timed Recording mode
   * - Addition of a heterodyne bandpass filter but the function is not activated (identical result with or without)
   * - Fixed a memory problem. The 1st use of a real in a printf function reserves 252 bytes in the heap without freeing it. Call to printf in the setup to keep the heap clean
   * - In Heterodyne mode, temperature measurement every 30s
   */
  /*
   * V0.83
   * - Stereo recording mode
   * - For PRS, add Stereo/Mono Left/Mon Right parameter
   * - For PRS, set A2 (left) and A3 (right) and A0 (batteries)
   * - Automatic adjustment of the number of buffers (pre-trigger) according to the non allocated memory size
   * - Adding a Copyright menu
   * - Correction heterodyne in reading which does not work any more since V0.81
   * - Adding a visualization while formatting an SD card
   * - Addition of a parameter to specify the type of microphone used
   * - Addition of a specific linear filter for the microphone ICS40730 at 384 and 250kHz
   * - Correction of the crash in playing when there is no file
   */
  /*
   * V0.82
   * - Use SdFatSdioEX for fast write and more stability with a full SD card
   * - AR, reader mode, limitation to 2000 files if SD card full but without guarantee to view the most recent
   * - SD card test, writing time display in µs if less than 1 ms
   * - Added formatting of SD card
   * - Correction micro test on PR
   * - Begin stereo mode
   */
  /*
   * V0.81
   * - Correction of a bug of reading/writing the digital gain in an ini file
   * - Batcorder mode counter, set to 0 only on power-up
   * - Correction of a random memory bug in Heterodyne mode
   * - Reader, update of the reading time according to the reading mode (X1, X10 or heterodyne)
   * - In user level mode "Debug" allows traces of debug
   * - Correction on the use of the Debug user level
   * - On DMA error, storage in the log and display in heterodyne mode
   * - Add CGenericMode::PrePrint to print heterodyne frequency before hour
   * - Reset the recording buffer before starting the acquisition
   * - Add Restitution::restart to stop/start restitution in heterodyne when restitution goes too fast to avoid DMA error
   */
  /*
   * V0.80
   * - Add Batcorder type file name mode
   * - Passage of heterodyne buffers to 4096 instead of 2048 for more stability (acquisition delay)
   * - Storage of parameters on modification in heterodyne mode
   * - Addition of user level management (Beginner, Expert, Debug)
   * - Addition of 5 user profiles
   * - Possibility to create and read a Profiles.ini files of all the parameters of the 5 profiles
   * - Correction of a bug on the display of the lines at the end of modification of a parameters
   * - Player, if no file, print 0.0k on sampling frequency
   * - Correction bug of start of recording on road and pedestrian protocols
   */
  /*
   * V0.75
   * - Adding Timed Recording Mode
   * - In microphone test, without low pass filter
   */
  /*
   * V0.74
   * - Correction of the bug of return to 48 kHz sampling frequency on restart
   */
  /*
   * V0.73
   * - Restoring operator parameters after a micro test
   * - Correction of a difference in calculation of the heterodyne frequency between the heterodyne and read modes
   * - On heterodyne, adjustment of the decimation by 10 with the FIR buffer of 128
   * - Heterodyne 384kHz, decimation by 16 (24kHz)
   * - Stop heterodyne restitution if buffer samples not yet available
   * - Addition of a parameter for the level of the heterodyne signal
   * - Add audio recorder mode for Active Recorder with a specific sampling frequency and band of interest
   * - Numeric gain to 0dB on Audio recorder
   * - Corrects heterodyne frequency management in auto
   * - Application of digital gain only on samples of wav files
   * - Code optimization for heterodyne processing at 384kHz
   * - Correction bug of modification of the parameters in microphone test
   * - Read mode, improved display of sampling frequency and heterodyne frequency
   * - Modification of the FIR filter to increase the number of coefficients
   */
  /* V0.72
   * - RhinoLogger mode, add a parameter to keep the display permanently
   * - Fixed management of the interest band in RhinoLogger mode
   * - Fixed memory overflow in CFCAnalyzerRL and adding a memory occupancy test for FC type bands
   * - Remove Crutch to bypass the memory bug of the RhinoLogger mode
   * - Correction for 128GB SD cards (in SDFat library, SdioTeensy.cpp set BUSY_TIMEOUT_MICROS to 800000)
   * - Adjust the format of the "SD card" line to take into account 128GB cards
   * - SD Card Test Mode, size adjustment to 16/32/64/128GO standard sizes
   * - Adding a parameter for the refresh time of the heterodyne graph
   * - Fixed minute, hour and day backup in RhinoLogger mode
   * - Added ability to exit error mode
   * - Add a message to the log if the SD card is full
   */
  /* V0.71
   * - Fixed wakeup bug with button in the case of reading temperature in sleep mode
   * - Changing the temperature file backup to avoid writing errors
   * - No atmospheric pressure, too random measure
   * - Saving the parameters at the end of each change of a parameter. Use the update function instead of write to optimize the number of writes
   * - Storing the noise file once and then the device goes back without memorization
   * - Noise file parameter with confirmation
   * - Fix a problem initializing the SD card in standby when storing temperatures
   * - Initialization of the parameters to the default values before reading them in the EEPROM at the start of the program
   * - Limit frequence sample at 250kHz in hétérodyne if CPU is below 180MHz
   * - Add parameter to go in bootloader mode in an Active Recorder (reset button not accessible in this model)
   * - Fix bug impossible to del wav in English language (SD parameter)
   * - Fix bad initialisation in parameters
   * - For an Active Recorder, do not refer to the external battery
   * - Taking into account the current seconds for the return of standby
   * - Crutch to bypass the memory bug of the RhinoLogger mode when leaving (masked reset for a clean restart)
   * - Fix buffer overflow in LogFile::AddLog (buffer overflow for long string)
   * - Test on the EEPROM write error
   */
 /* V0.70
  *  - Adding RhinoLogger mode but not active
  *  - Adding reading of SD card files via micro-USB port in parameters mode
  *  - Improvement of the micro test (cursor -120 to -50 instead of -120 to -25 dB)
  *  - Update micro test in 300ms instead of 1s for a faster reaction
  *  - Resume visualization of parameters according to processor frequency, recorder type and recording mode
  *  - Adding the test mode of the SD card
  *  - Adding heterodyne mode in play
  *  - Correcting the name of the first wave file in protocol mode
  *  - One temperature storage file instead of one file per day (Prefix_THLog.csv)
  *  - Memorization of temperature in tenth of a degree
  *  - Memorization of pressure in hPa
  *  - Temperature storage including standby period
  *  - Test the remaining size on the SD card. If size <50MB, end of recordings and error
  *  - Fixed a bug that limited the maximum frequency to 120kHz. The maximum is now 150kHz
  *  - Reading the serial number of the processor board and display in the log and on the start screen
  *  - Taking into account the serial number of the processor card in the files names created
  *  - Possibility of storing a noise measurement file
  *  - Fix bug on protocol mode settings
  *  - Fix a bug on the progress of fixed point protocol mode
  */
/* V0.65
 *  - Taking into account the probes without the humidity information
 *  - Migration of the temperature/humidity probe management in the CModeGenericRec class except IsTempSensor in CGenericMode class
 *  - Adding and managing a parameter of the reading period of the temperature/humidity probe (10/3600 second step 10)
 *  - If probe T / H present, information in the log as well as the reading period
 *  - Adding CModeGeneric::PrintRamUsage to monitor memory and storage of the memory information in the log at the start of the recording
 *  - Clarification of the linear filter / low pass
 *  - Using a temporary wave file to speed up the recording start (Open file long time with SD card > 16 GO)
 *  - In Active Recorder, use the same change mode button for recording modes other than heterodyne
 *  - Some hidden parameters in heterodyne mode
 */
/* V0.64
 * - Resetting the SD card on wake up to allow the SD card to change during sleep
 * - If MAINTAIN_FREE_CLUSTER_COUNT in SdFatConfig.h, print "SDFat cluster !" in start screen
 * - Backup temperature / humidity every 10 minutes in place of each wav file
 * - If invalid temperature or humidity, writing a blank in the memorization file
 * - Memory optimization between the passive and the active to avoid stack overflow
 * - Editing .h file comments for Doxygen documentation
 * - Temperature display in heterodyne mode (if the sensor is present)
 * - At the end of the sleep mode and switch to parameter mode, operator information to wait in the presence of a large capacity SD card
 * - If CPU Speed is less than 144MHz, max sample frequency is 250 KHz and linear filter always false
 * - Delivery of software for 3 processor frequencies: 144MHz for common use, 180MHz for lossless operation at 500kHz and 48MHz for reduced power consumption but limited to 250kHz sampling frequency
 */
/* V0.63
 * - Correction English screen saver
 * - Correction end of sleep
 * - Correction CSDModifier display
 */
/* V0.62
 * - Correction of the memorization of the temperature and hygrometry
 */
/* V0.61
 * - Adding the temperature probe treatment
 * - Adding LED management for buffering losses
 * - Remains 30s on the logo display if a key is pressed
 */
/* V0.6
 *  - Changing architecture with the use of the ModesModiers library
 *  - Adding relative or absolute threshold management
 *  - Using the U8g2lib library for screen management
 *  - Management of a screen type SSD1306 or SH1106
 *  - On error mode, checking the battery charge to change the error in case of a big discharge
 *  - Division of the function CRecorder::OnDMA_ISR in several inline functions for more clarity in the code
 *  - Improved launch processing to handle both cases Key_Up = debug mode and Key_Down = screen type change
 *  - Add Heterodyne and Player mode for ActiveReccorder
 *  - Manage bits to now the recorder type
 */
/* V0.53
 *  - Add numeric gain and noise in Auto Record screen and LogPR.txt
 *  - Correct internal names of GAINNUM
 */
/* V0.52
 *  - Adjustment of Vigie-Chiro protocol parameters according to MNHM recommendations
 *  - Modification of the road protocol to manage up to 20 points and a pause mode
 *  - Add fixed point Vigie-Chiro protocol
 *  - Add log on point management in protocol
 *  - If High pass filter frequency = 0 coefficient = 0.999 (High pass filter 50Hz)
 *  - Text modification "Sample Freq." by "Sample rate"
 *  - Add 500kHz Sample rate
 *  - CPU frequency and SDFat optimization verifications at startup
 *  - Storing recording parameters in LogPR.txt
 */
/* V0.51
 *  - Correct somes parameters formats
 *  - Correct 0 on Nb Detections when change Prefix
 *  - Correct Hight pas filter to apply filter on FFT samples
 */
/* V0.5
 *  - Add French and English language, parameter "Language: English/Langue : Francais"
 *  - Modification 1st parameter : "Go back to record.../Retour enregistre..."
 *  - Down and Up key active if held pressed
 *  - Changing the « Filtre » setting by "Linear filter/Filtre lineaire"
 *  - Added variable high pass filter (Thanks to Frank Dziock), 0 to 25kHz, 0 no filter "High p. filter 00kHz/Filtre pa.haut 00kHz"
 *  - Wave files name to standard Prefixe_YYYYMMJJ_HHMMSS.wav
 *  - Added protocol abort on "Low" key
 *  - Systematic storage of parameters when leaving the parameter mode, except the operating mode always at "Auto Rec"
 *  - Adding the default setting parameter "Default settings  No/Para. par defaut Non"
 *  - "Use LED" parameter only in debug mode. For Debug mode, held pressed "Up" key and start PR. Red LED blink 5 times and 1 second the last time (4 times without debug)
 */
/* V0.4
 *  - Modification du changement de mode
 *  - Ajout de l'état des batteries dans le log lors des changements veille/sortie veille
 *  - Pas de mémorisation du fichier de bruit
 *  - Pas du tout de LED si NOLED
 *  - Référence 1V pour les mesures des tensions batteries
 *  - Correcion test veille sur changement d'heure de début
*/
// V0.3 - Ajout du test micro, reprise de l'affichage des différents modes d'enregistrement (hors protocole + différents procotoles) et ajout du LogPR.txt
// V0.2 - Ajout de la gestion des protocoles Vigie-Chiro Routier et Pedestre
// V0.1 - Version initiale du PassiveRecorder

//#define TESTRL256 // Pour le test RhinoLogger à 256 points de FFT seulement

//#define WITHBLUETOOTH // With Bluetooth module

//#define SELFILTERH10KHZ // For selective filter 10kHz in Heterodyne

//#define SELFILTERH20KHZ // For selective filter 20kHz in Heterodyne

//! \enum TYPES_TEENSY_RECORDERS
//! \brief List of Teensy Recorder types
enum TYPES_TEENSY_RECORDERS {
  PASSIVE_RECORDER    = 0, //!< Passive Recorder
  ACTIVE_RECORDER     = 1, //!< Active Recorder
  PASSIVE_STEREO      = 2, //!< Passive Recorder Stereo
  PASSIVE_SYNCHRO     = 3, //!< Passive Recorder Stereo Synchro
  PASSIVE_MSYNCHRO    = 4, //!< Passive Recorder Mono Synchro (only for test)
  MAXTYPES_TRECORDER  = 5
};
 
//! Pour un écran 128x64 de type SSD1306, sinon écran 128x64 SH1106
#define SSD1306

//! \enum MODEFONC
//! \brief Modes de fonctionnement
enum MODEFONC {
  MPARAMS      = 1,  //!< Mode d'affichage du menu
  MVEILLE      = 2,  //!< Mode veille
  MRECORD      = 3,  //!< Mode d'enregistrement automatique
  MPROPED      = 4,  //!< Mode enregistrement en mode protocole pédestre
  MPROROU      = 5,  //!< Mode enregistrement en mode protocole routier
  MPROTST      = 6,  //!< Mode enregistrement en mode protocole test micro
  MPROPFX      = 7,  //!< Mode enregistrement en mode protocole point fixe
  MRHINOL      = 8,  //!< Mode RhinoLogger
  MHETER       = 9,  //!< Heterodyne mode
  MAUDIOREC    = 10, //!< Mode enregistreur audio
  MPLAYER      = 11, //!< Player mode
  MBDRL        = 12, //!< RhinoLogger Bands mode
  MTSTSD       = 13, //!< Test SD card mode
  MTRECORD     = 14, //!< Timed Recording Mode
  MPROFILES    = 15, //!< Modification of operator profiles
  MREADINI     = 16, //!< Read profiles ini files
  MCOPYRIGHT   = 17, //!< Copyright mode
  MSYNCHRO     = 18, //!< Synchro automatic recording mode
  MSYNCHROR    = 19, //!< To reset Synchro automatic recording mode
  MRESTARTTEST = 20, //!< To restart Microphone test
  MERROR       = 21, //!< Mode en erreur
  MHIBERNATE   = 22  //!< Mode veille profonde
};

//! \enum StatusProtocole
//! \brief Etats des modes protocoles
enum StatusProtocole {
  PROT_STOP   = 0,  //!< Enregistrement inactif, on attend l'ordre opérateur pour lancer un point d'enregistrement
  PROT_REC    = 1,  //!< En enregistrement d'un point
  PROT_END    = 2,  //!< Fin des points du protocole, attente de fin de l'opérateur
  PROT_PAUSE  = 3   //!< Enregistrement du point suspendu (propre au protocole routier uniquement)
};

//! Définion des fréquences d'intérets min et max
#define MinInterestFreq  100
#define MaxInterestFreq  150000
#define MaxInterestFreqA 48000

//! Temps d'attente en secondes pour les affichages momentanés de veille et acquisition
#define TIMEATTENTEAFF 15

//! Nombre max de caractères du préfixe des fichiers wav
#define MAXPREFIXE 5

//! Taille de la chaine de sauvegarde des heures de début et fin (style hh:mm)
#define MAXH 6

//! Taille de la chaine de sauvegarde de la date et de l'heure courante (style jj/mm/yy ouhh:mm:ss )
#define MAXDH 9

//! \enum FREQECH
//! \brief Fréquences d'échantillonnages
enum FREQECH {
  FE24KHZ  = 0, //!< 24kHz
  FE48KHZ  = 1, //!< 48kHz
  FE96KHZ  = 2, //!< 96kHz
  FE192KHZ = 3, //!< 192kHz
  FE250KHZ = 4, //!< 250kHz
  FE384KHZ = 5, //!< 384kHz
  FE500KHZ = 6, //!< 500kHz
  MAXFE    = 7
};

//! \enum GAINNUM
//! \brief Gain numérique
enum GAINNUM {
  GAIN0dB  = 0, //!<   0dB
  GAIN6dB  = 1, //!< + 6dB
  GAIN12dB = 2, //!< +12dB
  GAIN18dB = 3, //!< +18dB
  GAIN24dB = 4, //!< +24dB
  MAXGAIN  = 5
};

/* In Passive Recorder
 * Start -> Auto record -> Clic -> Parameters -> Return to record menu -> according record mode -> Auto Record     -> Clic -> Parameters
 *                                                                                              -> Protocol XXX    -> Clic -> Parameters
 *                                                                                              -> Microphone test -> Clic -> Parameters
 * In Active Recorder
 * Start -> Heterodyne -> Clic Mode -> Player -> Clic Mode -> Parameters -> Clic Mode -> according record mode -> Heterodyne      -> Clic Mode -> Player...
 *                                                                                                             -> Auto Record     -> Clic Mode -> Player...
 *                                                                                                             -> Protocol XXX    -> Clic Mode -> Parameters...
 *                                                                                                             -> Microphone test -> Clic Mode -> Parameters...
 */
//! \enum PROTOCOLES
//! \brief Mode de fonctionnement de l'enregistrement
enum PROTOCOLES {
  RECAUTO   = 0,  //!< Mode enregistreur automatique
  PROTPED   = 1,  //!< Protocole Vigie-Chiro pédestre
  PROTROUT  = 2,  //!< Protocole Vigie-Chiro routier
  PROTFIXE  = 3,  //!< Protocole Vigie-Chiro point fixe
  PRHINOLOG = 4,  //!< Mode RhinoLogger
  PTSTMICRO = 5,  //!< Protocole test micro
  PHETER    = 6,  //!< Mode hétérodyne
  TIMEDREC  = 7,  //!< Timed recording
  AUDIOREC  = 8,  //!< Mode enregistreur audio
  SYNCHRO   = 9,  //!< Synchro automatic recording mode
  MAXPROT   = 10  //!< Nombre max de mode de fonctionnement de l'enregistrement
};

//! \enum MASTERSLAVES
//! \brief Synchro modes
enum MASTERSLAVES {
  MASTER  = 0,  //!< Master mode
  SLAVE01 = 1,  //!< Slave 1
  SLAVE02 = 2,  //!< Slave 2
  SLAVE03 = 3,  //!< Slave 3
  SLAVE04 = 4,  //!< Slave 4
  SLAVE05 = 5,  //!< Slave 5
  SLAVE06 = 6,  //!< Slave 6
  SLAVE07 = 7,  //!< Slave 7
  SLAVE08 = 8,  //!< Slave 8
  SLAVE09 = 9,  //!< Slave 9
  MAXMS   = 10  //!< Nombre max de mode synchro Master / Slave
};

//! \enum UTILLEDSYNCH
//! \brief Types d'utilisation de la LED en mode synchro
enum UTILLEDSYNCH {
  NOLEDSYNCH  = 0,  //!< LED non utilisée
  LEDRECSYNCH = 1,  //!< LED allumée sur enregistrement d'un fichier
  LED3RECS    = 2,  //!< LED allumée sur les 3 er enregistrement seulement
  MAXLEDSYNCH = 3
};

//! \enum UTILLED
//! \brief Types d'utilisation de la LED
enum UTILLED {
  NOLED,      //!< LED non utilisée
  LEDRECORD,  //!< LED allumée sur enregistrement d'un fichier
  LEDDMA,     //!< LED allumée sur remplissage d'un des 2 buffers DMA
  LEDITDMA,   //!< LED allumée sur traitement IT DMA
  LEDLOOP,    //!< LED allumée sur tâche de fond Loop
  LEDNOISE,   //!< LED allumée sur calcul du bruit
  LEDACTIV,   //!< LED allumée sur détection d'activité
  LEDBUFFER,  //!< LED brievement allumée sur perte d'un buffer
  MAXLED
};

//! \enum SCREENTYPE
//! \brief Type d'écran
enum SCREENTYPE {
  ST_SSD1306 = 0, //!< SD1306 screen type (PR default)
  ST_SH1106  = 1, //!< SH1106 screen type (AR default)
  ST_MAX     = 2
};

// Nombre max de caractères d'un nom de bande
#define MAXNAMEBD 3
// Nombre max de caractères dans l'heure des mesures (12:36)
#define MAXTIMEMES 7
// Nombre max de caractères dans la date des mesures (12/02/2019)
#define MAXDATESMES 12
// Nombre max de caractères des noms des fichiers de sauvegarde
#define MAXFILESNAME 40

// Temps d'attente en secondes pour les affichages momentanés de veille et acquisition
#define TIMEATTENTEAFF 15

//! \enum BANDESRHINO
//! \brief Indices des bandes surveillées en mode RhinoLogger avec utilisation par défaut
enum BANDESRHINO {
  BD1   = 0,  //!< Bande FM
  BD2   = 1,  //!< Bande GR
  BD3   = 2,  //!< Bande RE exclusive
  BD4   = 3,  //!< Bande commune RE/PR
  BD5   = 4,  //!< Bande PR
  BD6   = 5,  //!< Bande non utilisée
  BD7   = 6,  //!< Bande non utilisée
  MAXBD = 7   //!< Nombre max de bandes
};
 
//! \enum BandType
//! \brief Monitoring band type
enum BandType
{
  BDINVALID = 0,  //!< Invalid band
  BDFM      = 1,  //!< Frequency modulation
  BDFC      = 2,  //!< Constant frequency
  BDQFC     = 3,  //!< Quasi Constant frequency
  BDTYPEMAX = 4
};

//! \enum MicrophoneType
//! \brief Microphone Type
enum MicrophoneType
{
  MTSPU0410  = 0,  //!< MEMS KNOWLES SPU0410LR5H
  MTICS40730 = 1,  //!< MEMS InvenSense ICS40730
  MTFG23329  = 2,  //!< Electret KNOWLES FG23329
  MTMAX      = 3
};

//! \enum UserLevel
//! \brief User level
enum UserLevel
{
  ULBEGINNER  = 0,  //!< Beginner
  ULEXPERT    = 1,  //!< Expert
  ULDEBUG     = 2,  //!< Debug
  ULMAX       = 3
};

//! \enum TOPSIZE
//! \brief Audio top size in samples
enum TOPSIZE
{
  TS256  = 0,  //!< 256 samples
  TS512  = 1,  //!< 512 samples
  TS1024 = 2,  //!< 1024 samples
  TS2048 = 3,  //!< 2048 samples
  TS4096 = 4,  //!< 4096 samples
  TSMAX  = 5
};

//! \enum HeterAutoManu
//! \brief Auto or Manuel mode Heterodyne
enum HeterAutoManu
{
  HAM_MANUAL      = 0,  //!< Heterodyne manual
  HAM_AUTO        = 1,  //!< Heterodyne auto
  HAM_ALWAYSMANU  = 2,  //!< Heterodyne Always manual
  HAM_ALWAYSAUTO  = 3,  //!< Heterodyne Always auto
  HAM_MAX         = 4
};

//! \enum HeterMultiParam
//! \brief Multiparaleters modifiers in heterodyne mode
enum HeterMultiParam
{
  HMUL_AGC  = 0,  //!< AGC On/Off
  HMUL_THR  = 1,  //!< Threshold
  HMUL_HAM  = 2,  //!< Heterodyne Auto/Manual
  HMUL_MAX  = 3
};

//! \enum HeterSelFilter
//! \brief Selective filter in Heterodyne
enum HeterSelFilter
{
  HSF_NOSEL       = 0,  //!< Heterodyne Without selective filter
  HSF_SELECTIVE   = 1,  //!< Heterodyne With selective filter
  HSF_ALWAYSNOSEL = 2,  //!< Heterodyne Always Without selective filter
  HSF_ALWAYSSEL   = 3,  //!< Heterodyne Always With selective filter
  HSF_MAX         = 4
};

//! \enum StereoMode
//! \brief Stereo recording mode
enum StereoMode
{
  SMSTEREO  = 0,  //!< Stereo recording
  SMRIGHT   = 1,  //!< Mono recording on right chanel
  SMLEFT    = 2,  //!< Mono recording on left chanel
  SMMAX     = 3
};

//! \struct MonitoringBand
//! \brief Parameters of a frequency band to monitor
struct MonitoringBand
{
  char  sName[MAXNAMEBD+1];   //!< Band name
  int   iType;                //!< Band type (BandType)
  int   iMinDuration;         //!< Minimum duration in ms (1 to 99ms)
  int   iMaxDuration;         //!< Maximum duration in ms for CF and QCF
  float fMinFWidth;           //!< Minimum frequency width in kHz (0.5 to 99, step 0.5)
  float fMin;                 //!< Minimum frequency of band the in kHz
  float fMax;                 //!< Maximum frequency of band the in kHz
  int   iNbDetections;        //!< Minimum detections to consider a positive second (1 to 20, step 1)
};

//! \struct QCFDefinition
//! \brief Parameters of a Quasi Constant Frequency
struct QCFDefinition
{
  float fMinFr; //! Minimum band frequency in kHz
  float fMaxFr; //! Maximum band frequency in kHz
  float fMinBW; //! Minimum bandwidth in kHz
  float fMaxBW; //! Maximum bandwidth in kHz
  float fMinDu; //! Minimum duration in ms
  float fMaxDu; //! Maximum duration in ms
};

//! Number of profiles
#define MAXPROFILES 5

//! Max size of a profile in byte
#define MAXSIZEPROFILE  512

//! Max char for a profile name (max "RhinoLogger")
#define MAXCHARPROFILE 11

//! Version des paramètres
#define VPARAMS 0x1000

//! \struct CommonParams
//! \brief Paramètres opérateur commun et liste de snoms des profils
struct CommonParams
{
  bool     bLumiereMin;             //!< Max light (false) or min (true)
  int      iLanguage;               //!< Language used (English/French)
  int      iScreenType;             //!< Screen type used
  int      iUtilLed;                //!< Using the LED (No, Recordin., Acquisit., IS DMA, Tr. Loop, Cal Noise, Det. Act., Buf. Los.)
  int      iUserLevel;              //!< User level (Beginner, Expert, Debug)
  int      iSelProfile;                                   //!< Selected profile
  char     ProfilesNames[MAXPROFILES][MAXCHARPROFILE+1];  //!< Profiles names
  double   dCorrectBat;             //!< Correction of battery voltage measurement
  double   dCorrectExt;             //!< Correction of external voltage measurement
#ifdef WITHBLUETOOTH
  bool     bBluetooth;              //!< Blutooth on (true) or off (false)
#endif
};

//! \struct ParamsOperator
//! \brief Paramètres opérateur d'enregistrement
struct ParamsOperator
{
  int      iModeRecord;             //!< Operating mode (Auto record, Walkin. Protoc., Road Protocol, Fixed P. Proto., RhinoLogger, Microphone test, Heterodyne, Audio Rec.)
  char     sHDebut[MAXH];           //!< Auto recording start time (hh:mm)
  char     sHFin[MAXH];             //!< Auto recording end time (hh:mm)
  int      uiFreqMin;               //!< Minimum frequency of interest in Hz (100-150000 step 100) in ultrasound (sample frequency >= 192kHz)
  int      uiFreqMax;               //!< Maximum frequency of interest in Hz (100-150000 step 100) in ultrasound (sample frequency >= 192kHz)
  int      uiFreqMinA;              //!< Minimum frequency of interest in Hz (100 to 96000 step 100) in audio (sample frequency < 192kHz)
  int      uiFreqMaxA;              //!< Maximum frequency of interest in Hz (100 to 96000 step 100) in audio (sample frequency < 192kHz)
  int      uiDurMin;                //!< Minimum duration in seconds (1-10)
  int      uiDurMax;                //!< Maximum duration in seconds (10-999)
  int      uiTRECRecord;            //!< Recording time in Timed Recording mode in second (1 to 18000 step 10)
  int      uiTRECWait;              //!< Waiting time in Timed Recording mode (1 to 3600 step 10)
  int      uiFe;                    //!< Sampling frequency in kHz (24, 48, 96, 192, 250, 384, 500)
  int      uiFeA;                   //!< Sampling frequency for audio mode (48, 96, 192)
  int      uiGainNum;               //!< Numeric gain (0, 6, 12, 18, 24dB)
  bool     bFiltre;                 //!< Low pass filter (SFr<384) or linear filter (SFr>= 384) (Y/N)
  int      iFreqFiltreHighPass;     //!< High pass filter in kHz (0-25), for SF >= 192kHz
  bool     bBatcorderLog;           //!< Batcorder log and Wave file name type (false=wildlife, true=batcorder)
  bool     bExp10;                  //!< Time expansion x10 if true
  char     sPrefixe[MAXPREFIXE+1];  //!< Wav file prefix (PRec_)
  int      iNbDetect;               //!< Number of detections on 8 FFTs to declare an activity (1 to 8)
  bool     bTypeSeuil;              //!< Threshold type (false relative, true absolute)
  int      iSeuilAbs;               //!< Absolute detection threshold in dB (-110/-30)
  int      iSeuilDet;               //!< Relative detection threshold in dB (5-99)
  int      iAutoHeter;              //!< Manual (false) / Auto (true) heterodyne mode
  bool     bAutoRecH;               //!< Manual (false) / Auto (true) heterodyne recording mode
  float    fRefreshGRH;             //!< Refresh period of the heterodyne graph
  float    fHetSignalLevel;         //!< Heterodyne signal level (unused)
  bool     bPermanentTempH;         //!< Record environmental variables continuously
  int      iPeriodTemp;             //!< Reading period of the temperature/humidity (10 to 3600 seconds)
  MonitoringBand batBand[MAXBD];    //!< RhinoLogger bands
  bool     bRLAffPerm;              //!< RhinoLogger mode, parameter to keep the display permanently
  bool     bDebugBand;              //!< For debugging detections in bands
  bool     bSaveNoise;              //!< To save noise mesurement
  int      iStereoRecMode;          //!< For PRS, stereo recording mode (Stereo, Mono Right or Mono Left)
  int      iMicrophoneType;         //!< Microphone type for linear filter at 250 and 384kHz
  int      iMasterSlave;            //!< Master or Slave n (0 Master or Slave 1 to 9 step 1)
  int      iTopAudioFreq;           //!< Top audio frequency in kHz (1 to 50 kHz step 1)
  int      iDurTop;                 //!< Top audio duration in samples (256, 512 or 1024)
  int      iTopPer;                 //!< Top audio period (0 for one at the start of the recording, 1 to 10 step 1 for one every 1 to 10 seconds)
  float    fFreqFiltreHighPass;     //!< High pass filter in kHz (0.0-25.0), for SF < 192kHz
  int      iLEDSynchro;             //!< Using LED in synchro mode (No, Record ou 3 record only)
  int      iPreTrigDurAuto;         //!< Pre-Trigger duration in second for Teensy 4.1 with extended RAM in automatic recording
  int      iPreTrigDurHeter;        //!< Pre-Trigger duration in second for Teensy 4.1 with extended RAM in hétérodyne mode
  int      iHeterSel;               //!< Heterodyne with selective filter or not (Tennsy 4.1 only)
  bool     bAutoPlay;               //!< Automatic play (X10) after recording in heterodyne mode
  bool     bGraphHet;               //!< True, with graph in Heterodyne
  bool     bAGCHet;                 //!< True, with AGC in Heterodyne
};

//! \struct oldParamsOperator
//! \brief Paramètres opérateur d'enregistrement avant introduction des profils (Version < 0.8)
struct oldParamsOperator
{
  bool     bLumiereMin;             //!< Indique si la luminosité de l'écran est minimale
  int      iSeuilDet;               //!< Seuil de détection relatif en dB
  char     sHDebut[MAXH];           //!< Heure de début d'enregistrement du style hh:mm
  char     sHFin[MAXH];             //!< Heure de fin d'enregistrement du style hh:mm
  int      uiFreqMin;               //!< Fréquence minimale d'intérêt en kHz
  int      uiFreqMax;               //!< Frequence maximale d'intérêt en kHz
  int      uiDurMin;                //!< Durée minimale d'un fichier wav en secondes
  int      uiDurMax;                //!< Durée maximale d'un fichier wav en secondes
  int      uiFe;                    //!< Fréquence d'échantillonnage (FREQECH)
  int      uiGainNum;               //!< Gain numérique (GAINNUM)
  bool     bFiltre;                 //!< Présence ou non du filtre numérique
  bool     bExp10;                  //!< Fichiers wav en expension de temps x10
  char     sPrefixe[MAXPREFIXE+1];  //!< Préfixe des fichiers wav
  int      iNbDetect;               //!< Nombre de détections sur 8 FFT pour déclarer une activitée (de 1 à 8)
  int      iUtilLed;                //!< Type d'utilisation de la LED (UTILLED)
  int      iModeRecord;             //!< Mode de fonctionnement en enregistrement
  int      iFreqFiltreHighPass;     //!< Fréquence de coupure du filtre passe haut (0 si aucun) en kHz
  int      iLanguage;               //!< Langue utilisée
  int      iScreenType;             //!< Type d'écran utilisé
  bool     bTypeSeuil;              //!< Type de seuil (false relatif, true absolue)
  int      iSeuilAbs;               //!< Seuil de détection absolue
  bool     bAutoHeter;              //!< Manual (false) / Auto (true) heterodyne mode
  bool     bAutoRecH;               //!< Manual (false) / Auto (true) heterodyne recording mode
  int      iPeriodTemp;             //!< Reading period of the temperature/humidity (10 to 3600 seconds)
  MonitoringBand batBand[MAXBD];    //!< Liste des bandes surveiller
  bool     bPermanentTempH;         //!< Record environmental variables continuously
  bool     bDebugBand;              //!< For debugging detections in bands
  bool     bSaveNoise;              //!< To save noise mesurement
  bool     bRLAffPerm;              //!< RhinoLogger mode, parameter to keep the display permanently
  float    fRefreshGRH;             //!< Refresh period of the heterodyne graph
  int      uiFeA;                   //!< Fréquence d'échantillonnage du mode Enregistreur Audio
  float    fHetSignalLevel;         //!< Heterodyne signal level
  int      uiFreqMinA;              //!< Fréquence minimale d'intérêt en kHz pour le mode audio
  int      uiFreqMaxA;              //!< Frequence maximale d'intérêt en kHz pour le mode audio
};

//! FME holding time in heterodyne and audio recorder
#define FMEHOLDINGTIME 5000
//! Temps min en ms des rebonds des boutons
#define TIMEREBOND 50
//! Temps de maintient pour prendre en compte un bouton qui reste en position pressé
#define TIMERMAINTIEN 400
//! Nombre de maintiens pour passer à la vitesse intermédiaire
#define MAINTIENVINT  8
//! Nombre de maintiens pour passer à la vitesse maximale
#define MAINTIENVMAX  15

//! Fréquence de coupure max du filtre passe haut en kHz
#define MAXHIHGPASS 25

//! N° des pins du bouton 5 positions ou poussoirs de l'AR
#define PINPUSH  7
#define PINRIGHT 3
#define PINLEFT  8
#define PINUP    2
#define PINDOWN  6
#define PINMODEA 3
#define PINMODEB 8
#define PINPUSH_ART41  9
#define PINPUSH_PRST41 9
#define PINLEFT_PRST41 11

//! Analogic pin for heterodyne wheel
#define ANALOGPINHETERODYNE 1

//! Définition de la broche d'arrêt audio
#define PIN_STOP_AUDIO  39
//! Définition de la broche d'entrée audio sur PR et AR (A3)
#define PINA3_AUDIO  3
//! Définition de la broche d'entrée audio droite sur PRS (A3)
#define PINA3_AUDIOR 3
//! Définition de la broche d'entrée audio gauche sur PRS (A2)
#define PINA2_AUDIOL 2
//! Définition de la broche d'alimentation du pré-ampli micro
#define PIN_START_PREAMP 33

//! Définition de la broche de mesure de la batterie interne sur PR et AR (A2)
#define PINA2_LIPO  2
//! Définition de la broche de mesure de la batterie interne sur PRS (A7)
#define PINA0_LIPO  7
//! Définition de la broche de mesure de la batterie externe sur PR, AR et PRS (A1)
#define PINA1_EXT   1

//! Définition des bits de reconnaissance du type d'appareil
#define  OPT_B0 37
#define  OPT_B1 36
#define  OPT_B2 35
#define  OPT_B4 34  // Sur PRS-S, signale le design Teensy 4.1
#ifdef WITHBLUETOOTH
//! Définition de l'option Bluetooth sur T4.1
#define OPT_BLUETOOTH 30
//! Définition de la commande alimentation Bluetooth sur T4.1
#define OPT_ALIM_BLE  28
//! Définition de la commande reconnaissance Bluetooth sur T4.1
#define OPT_INIT_BLE 29
#endif

// Définition des broches de communication ESP32
/*
	GPIO PINS
			           TEENSY	  ESP32
	ESPON          23 Out		       1 to put ESP32 ON
	ESP32_READY	   22 In	  23 Out 0 when ready
	MASTER	       04 Out	  32 In  0 for slave and 1 for master
	SLEEP	         01 Out   12 In  1 for work abd 0 for sleep
	RECORD MASTER	 00 Out   13 In  0 for standby and 1 for recording
	RECORD SLAVE   00 In    13 Out 0 for standby and 1 for recording
*/
#define PIN23ESPON    23
#define PIN22ESPREADY 22
#define PIN04MASTER   04
#define PIN00RECORD    0

//! Pins RX TX de la liaison RS232
#define PIN_RX  9
#define PIN_TX 10

//! Pin génération Top synchro sur T4.1
#define PIN_TOP_T41 10

//! Taille de la FFT (ATTENTION la fonction de calcul FFT utilisée - arm_cfft_radix4_q15 - ne fonctionne pas pour 512 échantillons)
#define FFTLEN 256

//! Nombre d'échantillons par buffer d'enregistrement
#define MAXSAMPLE 8192

//! Nombre d'échantillons par buffer en lecture d'un fichier
#define MAXSAMPLE_READ 4096

//! Nombre d'échantillons par buffer de lecture hétérodyne
#define MAXSAMPLE_HET 1024

//! Nombre d'échantillons par buffer de l'enregistreur audio
#define MAXSAMPLE_AUDIOR 4096

//! Nombre de FFT pour le test micro en mode silence (200ms)
#define MAXFFTSILENCE 300

//! Nombre de FFT pour le test micro en mode signal (1s)
#define MAXFFTSIGNAL 1500

//! Nombre de buffer hétérodyn
#define NBBUFFH 3

//! Nombre de buffer enregistrement en mode passif
#define NBBUFFERRECORD 8

//! Nombre de buffer enregistrement en mode hétérodyne
#define NBBUFFERRECORDH 7

//! Nombre de buffer enregistrement en mode cadencé
#define NBBUFFERRECORDT 2

//! Nombre de FFT pour le calcul du bruit moyen
#define MAXFFTNOISE 200

//! Nombre de lignes sur l'écran
#define MAXLINES 7
//! Nombre max de caractères sur une ligne
#define MAXCHARLINE 20
//! Hauteur en pixel d'une ligne de caractères
#define HLINEPIX 9
//! Largeur d'un caractère en pixel
#define LCHARPIX 6
//! Indique la présence des boutons Left/Right
#define BLEFTRIGHT true

/* Infos de calcul pour la transformation en dB
 * Niveau en dB = 20*LOG(Niveau)-120
 * En-dessous de 1024, on a un tableau de short qui nous donne les dB en dixièmes de dB directement (taille du tableau 2048)
 * Ce tableau va de -120.0dB à -59.7dB
 * Au-dessus, on a un tableau de 5120 de byte qui nous donne les dB pour chaque 1/10 de niveau
 * Ce tableau va de -60dB (niveau 1025) à -25.6dB (niveau 52224)
 * Au-dessus, on impose -25dB signifiant la saturation
*/
#define MAXTBDBA    1024   //! Taille de la table A niveau vers dB
#define MAXTBDBB    5120   //! Taille de la table B niveau vers dB

//! Time between two check battery (30s)
#define TIMECHECKBATTERY 30000

//! \enum ModeCheckBatteries
//! \brief Mode de mesure des batteries avec le MAX17043
enum ModeCheckBatteries {
  MAX_WAITMESURE  = 0,  //!< With MAX17043, Set power on, Wait and Mesure, default value
  MAX_POWER       = 1,  //!< With MAX17043, Set power on only
  MAX_MESURE      = 2   //!< With MAX17043, Mesure
};

//! \struct DetectionFC
//! \brief Structure de détection d'une FC (une instance par canal)
struct DetectionFC
{
  bool  bDetect;    //!< Indique une détection de FC en cours sur ce canal
  short iNbDetect;  //!< Nombre de FC détectées sur ce canal
  short iDuration;  //!< Durée de la détection sur le canal (en ms)
  short iMaxMag;    //!< Magnitude max sur le canal
};

//! \struct DetectionFM
//! \brief Structure de détection d'une FM (une instance par bande)
struct DetectionFM
{
  short idxFI;      //!< Index du canal de début de la FM (FI)
  short idxFT;      //!< Index du canal de fin de la FM (FT)
  short iWidth;     //!< Largeur de la FM en nombre de canaux
  short iNivMax;    //!< Magnitude max de la FM
  short idxNivMax;  //!< Index du canal du niveau max (FME)
};

//! \struct ResultAnalyseSeconde
//! \brief Structure de résultat d'analyse de la seconde courante pour chaque bande
struct ResultAnalyseSeconde
{
  short iNbDetAct;    //!< Indicates the number of detected activities
  short iMagMax;      //!< Magnitude maximum
  short iIdxMagMax;   //!< Index de la magnitude max
  short iIdxMin;      //!< Index de la détection minimale
  short iIdxMax;      //!< Index de la détection maximale
};

//! \struct ResultAnalyse
//! \brief Structure de résultat d'analyse de la minute courante pour chaque bande
//! Utilisé aussi pour l'heure courante (nombre de minutes actives)
//! et pour la journée courante (nombre de minutes actives)
struct ResultAnalyse
{
  short iNbSec;             //!< Nombre de secondes ou minutes actives
  short iNbCtx;             //!< Nombre de contacts (5s positives)
  short iNbMin;             //!< Nombre de minutes positives
  short iMagMax;            //!< Magnitude maximum
  short iIdxMagMax;         //!< Index de la magnitude max
  short iIdxMin;            //!< Index de la détection minimale
  short iIdxMax;            //!< Index de la détection maximale
};

//! \struct MemoTempHygroBat
//! \brief Sructure de mémorisation des températures et hygrométrie min/max et les denrières mesures des batteries
struct MemoTempHygroBat
{
  short iTempMin;   //!< Température min de la période
  short iTempMax;   //!< Température max de la période
  short iHygroMin;  //!< Hygrométrie min de la période
  short iHygroMax;  //!< Hygrométrie max de la période
  float fBatInt;    //!< Tension de la batterie interne
  float fBatExt;    //!< Tension de la batterie externe
};

// Fréquences Min/Max des bandes
#define FMINBD   10.0
#define FMAXBD   125.0
// Pas des bandes de fréquences
#define STEPBD  0.25

#ifdef TESTRL256
// Taille de la FFT en mode RhinoLogger (ATTENTION la fonction de calcul FFT utilisée - arm_cfft_radix4_q15 - ne fonctionne pas pour 512 échantillons)
#define FFTLENRL   256
#define MFFTLENRL  128
#else
// Taille de la FFT en mode RhinoLogger (ATTENTION la fonction de calcul FFT utilisée - arm_cfft_radix4_q15 - ne fonctionne pas pour 512 échantillons)
#define FFTLENRL   1024
#define MFFTLENRL  512
// Temps des échantillons d'une FFT RhinoLogger en ms
#endif

// Periode du timer d'analyse en µs (200ms)
#define PERIODETIMERRL 200000

// Macro pour Reset du Teensy 3.6
#define RESTART_ADDR       0xE000ED0C
#define READ_RESTART()     (*(volatile uint32_t *)RESTART_ADDR)
#define WRITE_RESTART(val) ((*(volatile uint32_t *)RESTART_ADDR) = (val))

// Taille max du nom de l'enregistreur (type + numéro de série)
#define MAXNAMERECORDER 16

// Taille max des noms de fichiers
#define MAXNAMEFILES    60

#endif
