//-------------------------------------------------------------------------
//! \file CModeRhinoLogger.h
//! \brief Classes de gestion du mode RhinoLogger
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#include "CModeRecorder.h"

#ifndef CMODERHINOLOGGER_H
#define CMODERHINOLOGGER_H

//-------------------------------------------------------------------------
//! \class CModeRhinoLogger
//! \brief Classe de gestion du mode RhinoLogger
class CModeRhinoLogger : public CModeGenericRec
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructeur
  CModeRhinoLogger();

  //-------------------------------------------------------------------------
  //! \brief Destructeur
  virtual ~CModeRhinoLogger();

  //-------------------------------------------------------------------------
  //! \brief Début du mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  //! \brief Fin du mode
  virtual void EndMode();
  
  //-------------------------------------------------------------------------
  //! \brief Creation du gestionnaire d'enregistrement
  virtual void CreateRecorder();
  
  //-------------------------------------------------------------------------
  //! \brief Affichage du mode sur l'écran
  //! Cette méthode est appelée régulièrement par le loop principal
  //! à charge des différents modes d'afficher les informations nécessaires
  virtual void PrintMode();
  
  //-------------------------------------------------------------------------
  //! \brief Traitement des ordres claviers
  //! Si la touche est une touche de changement de mode, retourne le mode demandé
  //! Cette méthode est appelée régulièrement par le loop principal
  //! à charge des différents modes de traiter les actions opérateurs
  //! \param key Touche sollicitée par l'opérateur
  virtual int KeyManager(
    unsigned short key
    );

protected:
  //! Seconde courante pour une gestion de l'affichage toute les secondes
  uint8_t iCurrentSecond;

  //! Gestion du temps pour la mesure régulière des batteries
  unsigned long uiTimeBat;
  
  //! Décompteur du mode affichage des paramètres en début de mode
  uint8_t iDecompteurAffPar;

  //! Décompteur du mode affichage des résultats des bandes
  uint8_t iDecompteurAffBd;

  //! Place libre en % sur la carte SD
  int iFreeSizeSD;

  //! Nombre de bandes valides
  int iNbBandesValides;

  // Mémo de la bande d'intéret
  int iMemoFMin, iMemoFMax;
};

#endif // CMODERHINOLOGGER_H
