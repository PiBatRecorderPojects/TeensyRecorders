/* 
 * File:   CModeGeneric.cpp
   PassiveRecorder Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
   O, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 /*
  * Classes :
  * - CModeGeneric
  * - CModeVeille
  * - CModeTestSD
  */
#include <U8g2lib.h>
#include "Const.h"
#include <EEPROM.h>
#include <Snooze.h>
#include <Wire.h>
#include <stdio.h>
#include "SdFat.h"
#include "TimeLib.h"
#include "ModesModifiers.h"
#include "CModeGeneric.h"
#include "CModeParams.h"
#include "CModeRecorder.h"
#include "CModeTestMicro.h"
#include "CModeHeterodyne.h"
#include "CModePlayer.h"
#include "CModeRhinoLogger.h"
#include "CModifBandRL.h"
#if defined(__MK66FX1M0__) // Teensy 3.6
  #include "RamMonitor.h"
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  #include <smalloc.h>
#endif
#include "CAudioRecorder.h"
#include "CTimedRecording.h"

// Chaines d'affichage dans les deux langues
extern const char *txtVeilleA[];
extern const char *txtVeilleB[];
extern const char *txtVeilleC[];
extern const char *txtVeilleD[];
extern const char *txtVeilleE[];
extern const char *txtLogParams[];
extern const char *txtLogVeille[];
extern const char *txtSDWERROR[];
extern const char *txtLogAuto[];
extern const char *txtLogAutoS[];
extern const char *txtLogProPed[];
extern const char *txtLogProRou[];
extern const char *txtLogProPFixe[];
extern const char *txtLogRhinoLog[];
extern const char *txtLogTstMic[];
extern const char *txtLogHeterodyne[];
extern const char *txtLogPlayer[];
extern const char *txtLogError[];
extern const char *txtLogBDRL[];
extern const char *txtLogTSTSD[];
extern const char *txtLogAudioRec[];
extern const char *txtLogTimedRec[];
extern const char *txtLogProfilesM[];
extern const char *txtLogProfilesR[];
extern const char *txtLogCopyright[];
extern const char *txtLogIndet[];
extern const char *txtLogEndSleep[];
extern const char *txtLogSDWait[];
extern const char *txtDefProfilesNames[MAXLANGUAGE][5];
extern const char *txtFreqEch[];
extern const char *txtLogParams[];
extern const char *txtLogVeille[];
extern const char *txtLogAuto[];
extern const char *txtLogAutoS[];
extern const char *txtLogProPed[];
extern const char *txtLogProRou[];
extern const char *txtLogProPFixe[];
extern const char *txtLogRhinoLog[];
extern const char *txtLogTstMic[];
extern const char *txtLogHeterodyne[];
extern const char *txtLogPlayer[];
extern const char *txtLogError[];
extern const char *txtLogBDRL[];
extern const char *txtLogTSTSD[];
extern const char *txtLogAudioRec[];
extern const char *txtLogTimedRec[];
extern const char *txtLogProfilesM[];
extern const char *txtLogProfilesR[];
extern const char *txtLogCopyright[];
extern const char *txtLogIndet[];
extern const char *txtLogEndSleep[];
extern const char *txtLogSDWait[];
extern const char *txtLogRAMD[];
extern const char *txtLogRAMB[];
extern const char *txtLogRAMA[];
extern const char *txtLogCPULOW[];
extern const char *txtLogFileTemp[];
extern const char *txtLogSection[];
extern const char *txtLogDecode[];
extern const char *txtLogStandbyPR[];
extern const char *txtLogStandbyAR[];
extern const char *txtLogSWakeupPR[];
extern const char *txtLogSWakeupAR[];
extern const char *txtSWError[];
extern const char *txtSDERROR[];
extern const char *txtSDFATTO[];
extern const char *txtSDFATBEGIN[];
extern const char *txtLogAutoTime[];
extern const char *txtLogAutoProfiles[];
extern const char *txtRestFormating[];
// Noms par défaut des bandes
const char *txtNamesBD[] = {"FM ", "GR ", "RE ", "EP ", "PR ", "No ", "No "};
// Constant char for enums values
const char *sULValues[] = {"Beginner", "Expert", "Debug"};
const char *sOMValues[] = {"Auto record", "Walkin. Protoc.", "Road Protocol", "Fixed P. Proto.", "RhinoLogger", "Microphone test", "Heterodyne", "Timed recording", "Audio Rec.", "Synchro"};
const char *sSFValues[] = {"24", "48", "96", "192", "250", "384", "500"};
const char *sNGValues[] = {"0", "6", "12", "18", "24"};
const char *sBRValues[] = {"Invalid", "FM", "FC", "QFC"};
const char *sSMValues[] = {"Stereo", "MonoRight", "MonoLeft"};
const char *sMTValues[] = {"SPU0410", "ICS43730", "FG23329"};
const char *sTDValues[] = {"256", "512", "1024"};
const char *sLEDValues[]= {"NO", "REC", "3 REC"};

#if defined(__MK66FX1M0__) // Teensy 3.6
  // Teensy 3.6 Ram monitoring (https://github.com/veonik/Teensy-RAM-Monitor)
  RamMonitor ramMonitor;
#endif

// Key manager
extern CKeyManager KeyManag;

// Screen manager
extern U8G2 *pDisplay;

// Gestionnaire carte SD
extern SdFs sd;

// Pour la récup de l'heure RTC au réveil
extern time_t getTeensy3Time();

// Gestionnaire du fichier LogPR.txt
extern LogFile logPR;

// Drivers du mode veille
SnoozeDigital digital;
SnoozeAlarm   alarm;
#if defined(__IMXRT1062__) // Teensy 4.1
/*
 * ATTENTION, bug dans Snooze sur le Teensy 4.1
 * https://forum.pjrc.com/threads/69432-Snooze-not-working-with-any-T4-x-on-TD1-56?highlight=snooze
 * Fichier TEENSY40\hal.c (Line 652):
 * il faut mettre FLASHMEM avant la fonction startup_early_hook
 * //----------------------------------------------------------------------------------
 * FLASHMEM void startup_early_hook( void ) {
 * 
 * ATTENTION, sur Teensy 4.1, le réveil suite à "deepSleep" est équivalent à un reset (passage par Setup)
 * 
 * ATTENTION? sur Teensy 4.1, en absence du driver SnoozeUSBSerial et sur utilisation de "deepSleep" ou "hibernate",
 * le T4.1 passe en veille et en sort immédiatement !
 * Le mode "sleep" fonctionne sans SnoozeUSBSerial et avec Serial
 * Le mode "deepSleep" fonctionne avec SnoozeUSBSerial et avec Serial
 * 
 * Consommations relevées avec une tension d'alimentation à 4V :
 * Mode                   T3.6     T4.1    T4.1    T4.1
 * Fe                    384kHz   384kHz  500kHz  250kHz
 * F CPU                 144MHz   180MHz  280MHz  150MHz
 * Paramètres            80mA      60mA    60mA    60mA
 * Enregistrement Auto   83mA      62mA    74mA    59mA
 * Ecriture Wav         136mA     100mA    110mA   98mA
 * Veille                <1mA      12mA
 */
  // Sur Teensy 4.1, utilisation de SnoozeUSBSerial pour que deepSleep fonctionne
  SnoozeUSBSerial usb;
  // Initialisation de gestionnaire de veille
  SnoozeBlock configTeensy( usb, alarm, digital);
#else
  // Initialisation de gestionnaire de veille
  SnoozeBlock configTeensy( digital, alarm);
#endif

// Serial nuber string with recorder name
extern char sSerialName[MAXNAMERECORDER];

//----------------------------------------------------
// Function to create or activate a new mode
CGenericMode* CreateNewMode(
  CGenericMode* pOldMode,   // Pointer on the previous mode of operation
  int idxMode               // New mode of operation index
  )
{
  bool bOK = true;
  // Print ram usage before delete (for Teensy 3.6), if used heap is not identical after a same mode, delete is missing....
  CGenericMode* pNewMode = NULL;
  // Dynamic creation of operating modes
  // First, destruction of the previous mode to free memory
  if (pOldMode != NULL)
  {
    delete pOldMode;
    // Print ram usage after delete (for Teensy 3.6), if used heap is not identical after a same mode, delete is missing....
    //bOK = CModeGeneric::PrintRamUsage( txtLogRAMD[CModeGeneric::GetCommonsParams()->iLanguage]);
  }
  /*else
    // Print ram usage before first mode (for Teensy 3.6), if used heap is not identical after a same mode, delete is missing....
    bOK = CModeGeneric::PrintRamUsage( txtLogRAMB[CModeGeneric::GetCommonsParams()->iLanguage]);*/
  if (!bOK and !CModeGeneric::isSDInit())
  {
    // SD access problem
    idxMode = MERROR;
  }
  // Second, creating the new mode of operation in memory
  switch (idxMode)
  {
  case MPARAMS   : pNewMode = new CModeParams();            bOK = LogFile::AddLog( LLOG, txtLogParams    [CModeGeneric::GetCommonsParams()->iLanguage]);  break;
  case MVEILLE   : pNewMode = new CModeVeille();            bOK = LogFile::AddLog( LLOG, txtLogVeille    [CModeGeneric::GetCommonsParams()->iLanguage]);  break;
  case MRECORD   : pNewMode = new CModeAutoRecorder();      bOK = LogFile::AddLog( LLOG, txtLogAuto      [CModeGeneric::GetCommonsParams()->iLanguage]);  break;
  case MSYNCHRO  : pNewMode = new CModeSynchro();           bOK = LogFile::AddLog( LLOG, txtLogAutoS     [CModeGeneric::GetCommonsParams()->iLanguage]);  break;
  case MPROPED   : pNewMode = new CModeProtocolePedestre(); bOK = LogFile::AddLog( LLOG, txtLogProPed    [CModeGeneric::GetCommonsParams()->iLanguage]);  break;
  case MPROROU   : pNewMode = new CModeProtocoleRoutier();  bOK = LogFile::AddLog( LLOG, txtLogProRou    [CModeGeneric::GetCommonsParams()->iLanguage]);  break;
  case MPROPFX   : pNewMode = new CModeProtocolePFixe();    bOK = LogFile::AddLog( LLOG, txtLogProPFixe  [CModeGeneric::GetCommonsParams()->iLanguage]);  break;
  case MRHINOL   : pNewMode = new CModeRhinoLogger();       bOK = LogFile::AddLog( LLOG, txtLogRhinoLog  [CModeGeneric::GetCommonsParams()->iLanguage]);  break;
  case MPROTST   : pNewMode = new CModeTestMicro();         bOK = LogFile::AddLog( LLOG, txtLogTstMic    [CModeGeneric::GetCommonsParams()->iLanguage]);  break;
  case MHETER    : pNewMode = new CModeHeterodyne();        bOK = LogFile::AddLog( LLOG, txtLogHeterodyne[CModeGeneric::GetCommonsParams()->iLanguage]);  break;
  case MAUDIOREC : pNewMode = new CAudioRecorder();         bOK = LogFile::AddLog( LLOG, txtLogAudioRec  [CModeGeneric::GetCommonsParams()->iLanguage]);  break;
  case MTRECORD  : pNewMode = new CTimedRecording();        bOK = LogFile::AddLog( LLOG, txtLogTimedRec  [CModeGeneric::GetCommonsParams()->iLanguage]);  break;
  case MPLAYER   : pNewMode = new CModePlayer();            bOK = LogFile::AddLog( LLOG, txtLogPlayer    [CModeGeneric::GetCommonsParams()->iLanguage]);  break;
  case MBDRL     : pNewMode = new CModeBandsRL();           bOK = LogFile::AddLog( LLOG, txtLogBDRL      [CModeGeneric::GetCommonsParams()->iLanguage]);  break;
  case MTSTSD    : pNewMode = new CModeTestSD();            bOK = LogFile::AddLog( LLOG, txtLogTSTSD     [CModeGeneric::GetCommonsParams()->iLanguage]);  break;
  case MPROFILES : pNewMode = new CModeModifProfiles();     bOK = LogFile::AddLog( LLOG, txtLogProfilesM [CModeGeneric::GetCommonsParams()->iLanguage]);  break;
  case MREADINI  : pNewMode = new CModeLoadProfiles();      bOK = LogFile::AddLog( LLOG, txtLogProfilesR [CModeGeneric::GetCommonsParams()->iLanguage]);  break;
  case MCOPYRIGHT: pNewMode = new CModeCopyright();         bOK = LogFile::AddLog( LLOG, txtLogCopyright [CModeGeneric::GetCommonsParams()->iLanguage]);  break;
  case MERROR    : pNewMode = new CModeError();             bOK = LogFile::AddLog( LLOG, txtLogError     [CModeGeneric::GetCommonsParams()->iLanguage]);  break;
  default        :                                          bOK = LogFile::AddLog( LLOG, txtLogIndet     [CModeGeneric::GetCommonsParams()->iLanguage]);
    // Erreur logiciel !
    CModeError::SetTxtError((char *)txtSWError[CModeGeneric::GetCommonsParams()->iLanguage]);
  }
  // Set error mode
  switch (idxMode)
  {
  case MPARAMS   :
  case MHETER    :
  case MAUDIOREC :
  case MPLAYER   :
  case MBDRL     :
  case MTSTSD    :
  case MPROFILES :
  case MREADINI  :
  case MCOPYRIGHT:
    // Manual end error mode
    CModeError::SetNewMode( MPARAMS);
    break;
  default:
    if (idxMode != MERROR)
      // Automatic end error mode (reset after 2mn)
      CModeError::SetNewMode( MPARAMS, 120000);
  }
  //if (bOK)
    // Print ram usage after new (for Teensy 3.6), if used heap is not identical after a same mode, delete is missing....
    //bOK = CModeGeneric::PrintRamUsage( txtLogRAMA[CModeGeneric::GetCommonsParams()->iLanguage]);
  if (!bOK and !CModeGeneric::isSDInit())
  {
    delete pNewMode;
    pNewMode = new CModeError();
  }
  return pNewMode;
}

/*-----------------------------------------------------------------------------
 Classe générique de gestion d'un mode de fonctionnement
 Définie l'interface minimale de gestion d'un mode de fonctionnement
 Gère le menu de changement de mode, l'état de la batterie et la luminosité écran
-----------------------------------------------------------------------------*/

//-------------------------------------------------------------------------
// Constructeur
CModeGeneric::CModeGeneric()
{
  cTemp = 70.0;
  humidity = 0.0;
  pressure = 0.0;
  bADCBat = false;
  bMAXError = false;
}

//-------------------------------------------------------------------------
// Début du mode
void CModeGeneric::BeginMode()
{
  //Serial.printf("sizeof(CommonParams)=%d, sizeof(ParamsOperator)=%d\n", sizeof(CommonParams), sizeof(ParamsOperator));
  //Serial.println("CModeGeneric::BeginMode");
  // Par défaut, pas de lecture écriture des paramètres. Lecture et écritures à faire explicitement
  bWriteParams = false;
  bReadParams  = false;
  // Appel de la méthode parente
  CGenericMode::BeginMode();
  CModeGeneric::StaticBeginMode();
  // Init des Heure/Minute de début et de fin d'acquisition/enregistrement
  CalcTimeDebFin();
  // Réaffichage complet
  bRedraw = true;
  //Serial.println("CModeGeneric::BeginMode OK");
}

//-------------------------------------------------------------------------
// Fin du mode
void CModeGeneric::EndMode()
{
  //Serial.println("CModeGeneric::EndMode");
  // Appel de la méthode parente
  CGenericMode::EndMode();
  //Serial.println("CModeGeneric::EndMode OK");
}

//-------------------------------------------------------------------------
//! \brief Initialising static parameters
void CModeGeneric::StaticBeginMode()
{
  Serial.println("Max begin");
  // Initialisation d'un éventuel MAX17043
  max17043.begin();
  Serial.println("ADC begin");
  // Initialisation d'un éventuel MCP3221
  bADCBat = ADCBat.begin();
  Serial.println("StaticBeginMode OK");
}
  
//-------------------------------------------------------------------------
//! \brief Turns headphone audio circuits ON
void CModeGeneric::TurnAudioON()
{
  // CModeGeneric::TurnAudioON Start 1, 2000, Nb 100, Plus 1, Minus 10, Stop 101, 1000, Time 273439µs OK <<<<<<
  // CModeGeneric::TurnAudioON Start 1, 2000, Nb  50, Plus 1, Minus 10, Stop  51, 1500, Time 148859µs OK
  // CModeGeneric::TurnAudioON Start 1, 2000, Nb  25, Plus 1, Minus 10, Stop  26, 1750, Time  78497µs OK
  // CModeGeneric::TurnAudioON Start 2, 2000, Nb  25, Plus 2, Minus 10, Stop  52, 1750, Time  78552µs HS
  // CModeGeneric::TurnAudioON Start 2, 2000, Nb  50, Plus 2, Minus 10, Stop 102, 1500, Time 149140µs HS
  // CModeGeneric::TurnAudioON Start 2, 2000, Nb 100, Plus 2, Minus 10, Stop 202, 1000, Time 277701µs HS
  // CModeGeneric::TurnAudioON Start 1, 2000, Nb 100, Plus 2, Minus 10, Stop 201, 1000, Time 277714µs HS
  // CModeGeneric::TurnAudioON Start 1, 2000, Nb  75, Plus 1, Minus 10, Stop  76, 1250, Time 214302µs OK
  uint32_t iDelayNs    = 100;
  uint32_t iDelayOn    = 1;
  uint32_t iDelayOff   = 2000;
  uint32_t iDelayPlus  = 1;
  uint32_t iDelayMinus = 10;
  int      iNb         = 100;  
  //uint32_t timeA = micros();
  //Serial.printf("CModeGeneric::TurnAudioON Start %d, %d, Nb %d, Plus %d, Minus %d", iDelayOn, iDelayOff, iNb, iDelayPlus, iDelayMinus);
  for (int i=0; i<iNb; i++)
  {
    //Serial.printf("iDelayOn %d, iDelayOff %d\n", iDelayOn, iDelayOff);
    digitalWriteFast( PIN_STOP_AUDIO, LOW);
    if (iDelayNs <= 1000)
      delayNanoseconds(iDelayNs);
    else
      delayMicroseconds(iDelayOn);
    digitalWriteFast( PIN_STOP_AUDIO, HIGH);
    delayMicroseconds(iDelayOff);
    if (iDelayNs <= 1000)
      iDelayNs  += 100;
    else
      iDelayOn  += iDelayPlus;
    iDelayOff -= iDelayMinus;
  }
  digitalWriteFast( PIN_STOP_AUDIO, LOW);
  //Serial.printf(", Stop %d, %d, Time %dµs\n", iDelayOn, iDelayOff, micros()-timeA);
}

//-------------------------------------------------------------------------
//! \brief Turns headphone audio circuits OFF
void CModeGeneric::TurnAudioOFF()
{
  digitalWriteFast( PIN_STOP_AUDIO, HIGH);
}
  
//-------------------------------------------------------------------------
// Mise aux valeurs par défaut des paramètres
void CModeGeneric::SetDefault()
{
  //Serial.println("CModeGeneric::SetDefault");
  // Initialisation des paramètres par défaut
  // Paramètres communs
  commonParams.bLumiereMin = false;            // Indique si la luminosité de l'écran est minimale
#ifdef WITHBLUETOOTH
  commonParams.bBluetooth  = false;            // Indique si le Bluetooth est utilisée ou non
#endif
  commonParams.iUtilLed    = NOLED;            // Type d'utilisation de la LED (UTILLED)
  commonParams.iLanguage   = LENGLISH;         // Langue anglaise par défaut
  commonParams.iUserLevel  = ULBEGINNER;       // User level (Beginner, Confirmed, Expert)
  if (commonParams.iScreenType < 0 or commonParams.iScreenType >= ST_MAX)
  {
    if (GetRecorderType() == ACTIVE_RECORDER)
      commonParams.iScreenType = ST_SH1106;    // Default screen type for AR
    else
      commonParams.iScreenType = ST_SSD1306;   // Default screen type for PR
  }
  commonParams.iSelProfile = 0;                // Profil sélectionné
  CGenericModifier::SetLanguage( commonParams.iLanguage);
  for (int i=0; i<MAXPROFILES; i++)
    // Nom par défaut des profils
    strcpy( commonParams.ProfilesNames[i], txtDefProfilesNames[commonParams.iLanguage][i]);

  // Paramètres opérateur
  paramsOperateur.iSeuilDet   = 18;            // Seuil de détection relatif en dB
  paramsOperateur.uiFreqMin   =  15000;        // Fréquence minimale d'intérêt en Hz
  paramsOperateur.uiFreqMax   = 120000;        // Frequence maximale d'intérêt en Hz
  paramsOperateur.uiFreqMinA  =   1000;        // Fréquence minimale d'intérêt en Hz
  paramsOperateur.uiFreqMaxA  =  20000;        // Frequence maximale d'intérêt en Hz
  paramsOperateur.uiDurMin    = 1;             // Durée minimale d'un fichier wav en secondes
  paramsOperateur.uiDurMax    = 10;            // Durée maximale d'un fichier wav en secondes
  paramsOperateur.uiTRECRecord = 60;           // Recording time in Timed Recording mode in second (1 to 3600 step 10)
  paramsOperateur.uiTRECWait   = 540;          // Waiting time in Timed Recording mode (1 to 3600 step 10)
  if (F_CPU < 144000000)
    paramsOperateur.uiFe      = FE250KHZ;      // Fréquence d'échantillonnage en Hz
  else
    paramsOperateur.uiFe      = FE384KHZ;      // Fréquence d'échantillonnage en Hz
  paramsOperateur.uiFeA       = FE48KHZ;       // Fréquence d'échantillonnage audio en Hz
  paramsOperateur.uiGainNum   = GAIN0dB;       // Gain numérique
  paramsOperateur.bFiltre     = false;         // Présence ou non du filtre numérique
  paramsOperateur.bExp10      = true;          // Fichiers wav en expension de temps x10
  paramsOperateur.iNbDetect   = 3;             // Nombre de détections sur 8 FFT pour déclarer une activitée (de 1 à 8)
  if (CModeGeneric::GetRecorderType() == ACTIVE_RECORDER)
    paramsOperateur.iModeRecord = PHETER;      // Mode hétérodyne
  else
    paramsOperateur.iModeRecord = RECAUTO;     // Mode enregistrement automatique
  paramsOperateur.iFreqFiltreHighPass = 0;     // Fréquence du filtre passe haut
  paramsOperateur.fFreqFiltreHighPass = 0.0;
  paramsOperateur.bTypeSeuil  = false;         // Type de seuil (false relatif, true absolue)
  paramsOperateur.iSeuilAbs   = -90;           // Seuil de détection absolue
  paramsOperateur.iAutoHeter  = HAM_MANUAL;    // Heterodyne mode (manual)
  paramsOperateur.bAutoRecH   = false;         // Heterodyne Recording mode (manual)
  paramsOperateur.iPeriodTemp = 600;           // Reading period of the temperature/humidity (10mn)
  paramsOperateur.bPermanentTempH = false;     // Record environmental variables continuously
  paramsOperateur.bSaveNoise  = false;         // No noise file save
  paramsOperateur.bRLAffPerm  = false;         // RhinoLogger mode, parameter to keep the display permanently
  paramsOperateur.fRefreshGRH = 1.0;           // Refresh period of the heterodyne graph
  paramsOperateur.fHetSignalLevel = 0.5;       // Heterodyne signal level (0.1/0.9)
  paramsOperateur.bBatcorderLog = false;       // Batcorder log and Wave file name type (false=wildlife, true=batcorder)
  if (CModeGeneric::GetRecorderType() >= PASSIVE_STEREO)
    paramsOperateur.iStereoRecMode = SMSTEREO;   // For PRS, stereo recording mode
  else
    paramsOperateur.iStereoRecMode = SMRIGHT;    // Mono recording on right chanel
  paramsOperateur.iMicrophoneType  = MTICS40730; // Microphone type
  paramsOperateur.iMasterSlave     = MASTER;     // Master or Slave n (0 Master or Slave 1 to 9 step 1)
  paramsOperateur.iTopAudioFreq    = 8;          // Top audio frequency in kHz (1 to 50 kHz step 1)
  paramsOperateur.iDurTop          = TS256;      // Top audio duration in samples (256, 512 or 1024)
  paramsOperateur.iTopPer          = 3;          // Top audio period (0 for one at the start of the recording, 1 to 10 step 1 for one every 1 to 10 seconds)
  paramsOperateur.iLEDSynchro      = LEDRECSYNCH;// Using or nor LED in synchro mode
  paramsOperateur.iPreTrigDurAuto  = 1;          // Pré-trigger duration in second for Teensy 4.1 with extended RAM for automatic recording
  paramsOperateur.iPreTrigDurHeter = 3;          // Pré-trigger duration in second for Teensy 4.1 with extended RAM for hétérodyne mode
  paramsOperateur.iHeterSel        = HSF_NOSEL;  // Heterodyne with selective filter or not (Tennsy 4.1 only)
  paramsOperateur.bAutoPlay        = false;      // Automatic play (X10) after recording in heterodyne mode
  paramsOperateur.bGraphHet        = true;       // With graphe in heterodyne mode
  paramsOperateur.bAGCHet          = true;       // With AGC in heterodyne mode
  strcpy( paramsOperateur.sHDebut,  "22:00");    // Heure de début d'enregistrement du style hh:mm
  strcpy( paramsOperateur.sHFin,    "06:00");    // Heure de fin d'enregistrement du style hh:mm
  strcpy( paramsOperateur.sPrefixe, "PaRec");    // Préfixe des fichiers wav (5 caractères)
  // Initialisation des bandes RhinoLogger
  SetDefaultRLBands();
}
  
//-------------------------------------------------------------------------
// Default setting for RhinoLogger bands
void CModeGeneric::SetDefaultRLBands()
{
  // Initialisation des bandes RhinoLogger
  paramsOperateur.batBand[BD1].fMin         = 40.00; // Bande FM 40-70
  paramsOperateur.batBand[BD1].fMax         = 70.00;
  paramsOperateur.batBand[BD1].iType        = BDFM;
  paramsOperateur.batBand[BD1].iMinDuration = 4;
  paramsOperateur.batBand[BD1].iMaxDuration = 8;
  paramsOperateur.batBand[BD1].fMinFWidth   = 10.0;
  paramsOperateur.batBand[BD1].iNbDetections= 3;
  strcpy( paramsOperateur.batBand[BD1].sName, txtNamesBD[BD1]);
  paramsOperateur.batBand[BD2].fMin         = 76.00; // Bande GR 76-86
  paramsOperateur.batBand[BD2].fMax         = 86.00;
  paramsOperateur.batBand[BD2].iType        = BDFC;
  paramsOperateur.batBand[BD2].iMinDuration = 32;
  paramsOperateur.batBand[BD2].iMaxDuration = 96;
  paramsOperateur.batBand[BD2].fMinFWidth   = 1.0;
  paramsOperateur.batBand[BD2].iNbDetections= 3;
  strcpy( paramsOperateur.batBand[BD2].sName, txtNamesBD[BD2]);
  paramsOperateur.batBand[BD3].fMin         = 100.00; // Bande RE 100-104
  paramsOperateur.batBand[BD3].fMax         = 104.00;
  paramsOperateur.batBand[BD3].iType        = BDFC;
  paramsOperateur.batBand[BD3].iMinDuration = 24;
  paramsOperateur.batBand[BD3].iMaxDuration = 96;
  paramsOperateur.batBand[BD3].fMinFWidth   = 1.0;
  paramsOperateur.batBand[BD3].iNbDetections= 3;
  strcpy( paramsOperateur.batBand[BD3].sName, txtNamesBD[BD3]);
  paramsOperateur.batBand[BD4].fMin         = 104.25; // Bande EP 104.25-106.5
  paramsOperateur.batBand[BD4].fMax         = 106.50;
  paramsOperateur.batBand[BD4].iType        = BDFC;
  paramsOperateur.batBand[BD4].iMinDuration = 32;
  paramsOperateur.batBand[BD4].iMaxDuration = 96;
  paramsOperateur.batBand[BD4].fMinFWidth   = 1.0;
  paramsOperateur.batBand[BD4].iNbDetections= 3;
  strcpy( paramsOperateur.batBand[BD4].sName, txtNamesBD[BD4]);
  paramsOperateur.batBand[BD5].fMin         = 106.75; // Bande PR 106.75-120
  paramsOperateur.batBand[BD5].fMax         = 120.00;
  paramsOperateur.batBand[BD5].iType        = BDFC;
  paramsOperateur.batBand[BD5].iMinDuration = 32;
  paramsOperateur.batBand[BD5].iMaxDuration = 96;
  paramsOperateur.batBand[BD5].fMinFWidth   = 1.0;
  paramsOperateur.batBand[BD5].iNbDetections= 3;
  strcpy( paramsOperateur.batBand[BD5].sName, txtNamesBD[BD5]);
  paramsOperateur.batBand[BD6].fMin         = 40.0; // Bande inutilisée
  paramsOperateur.batBand[BD6].fMax         = 70.00;
  paramsOperateur.batBand[BD6].iType        = BDINVALID;
  paramsOperateur.batBand[BD6].iMinDuration = 12;
  paramsOperateur.batBand[BD6].iMaxDuration = 96;
  paramsOperateur.batBand[BD6].fMinFWidth   = 1.0;
  paramsOperateur.batBand[BD6].iNbDetections= 3;
  strcpy( paramsOperateur.batBand[BD6].sName, txtNamesBD[BD6]);
  paramsOperateur.batBand[BD7].fMin         =  40.00; // Bande inutilisée
  paramsOperateur.batBand[BD7].fMax         =  70.00;
  paramsOperateur.batBand[BD7].iType        = BDINVALID;
  paramsOperateur.batBand[BD7].iMinDuration = 12;
  paramsOperateur.batBand[BD7].iMaxDuration = 96;
  paramsOperateur.batBand[BD7].fMinFWidth   = 1.0;
  paramsOperateur.batBand[BD7].iNbDetections= 3;
  strcpy( paramsOperateur.batBand[BD7].sName, txtNamesBD[BD7]);
}
  
//-------------------------------------------------------------------------
// Lecture des paramètres sauvegardés en EEPROM
// Retourne true si lecture OK
bool CModeGeneric::ReadParams()
{
  //Serial.printf("CModeGeneric::ReadParams sizeof(ParamsOperator)=%d\n", sizeof(ParamsOperator));
  return StaticReadParams();
  /*SetDefault();
  return true;*/
}

//-------------------------------------------------------------------------
// Reading saved settings in static function
bool CModeGeneric::StaticReadParams()
{
  //if (CModeGeneric::bDebug)
  //  Serial.println("CModeGeneric::StaticReadParams");
  // Vérification si les paramètres ont été écris une fois en EEPROM
  // Lecture du mot de controle de la version en EEPROM
  // Version du logiciel (V0.92= VPARAMS + 000 + 90 + 2 = 1092)
  int address = 0;
  uint16_t uiControle;
  char *p = (char *)&uiControle;
  for (int i = 0; i<(int)sizeof(uint16_t); i++)
    *p++ = EEPROM.read(address++);
  //Serial.printf("uiControle %X\n", uiControle);
  // Test en fonction de la version présente
  if ((uiControle & 0xF000) == VPARAMS)
  {
    // Version courante des paramètres à partir de la V0.92
    //Serial.println("StaticReadParams last version");
    if (bReadCommon)
    {
      // Lecture des paramètres communs aux adresses suivantes
      p = (char *)&commonParams;
      for (int i = 0; i<(int)sizeof(CommonParams); i++)
        *p++ = EEPROM.read(address++);
      if ((uiControle & 0x0FFF) < 0x0097)
      {    
        // Nouveau paramètre commun, on initialise les paramètres aux valeurs par défaut
        commonParams.dCorrectBat = 0.0;
        commonParams.dCorrectExt = 0.0;
      }
      //Serial.printf("CModeGeneric::StaticReadParams dCorrectBat %f, dCorrectExt %f\n", commonParams.dCorrectBat, commonParams.dCorrectExt);
      // Vérification des paramètres communs
      StaticCtrlCommonParams();
    }
    // Calcul de l'adresse de lecture en fonction du profil sélectionné
    address = MAXSIZEPROFILE + (MAXSIZEPROFILE * commonParams.iSelProfile);
    // Lecture des paramètres opérateur en fonction du profil sélectionné
    p = (char *)&paramsOperateur;
    for (int i = 0; i<(int)sizeof(ParamsOperator); i++)
      *p++ = EEPROM.read(address++);
    if ((uiControle & 0x0FFF) == 0x0095 and paramsOperateur.bGraphHet == false)
      // 1ère utilisation de ce paramètre (passage de V095 à V096), on le force à true
      paramsOperateur.bGraphHet = true;
    if ((uiControle & 0x0FFF) == 0x0097 and paramsOperateur.bGraphHet == false)
      // 1ère utilisation de ce paramètre (passage de V097 à V098), on le force à true
      paramsOperateur.bAGCHet = true;
    // Controle des paramètres
    StaticControlParams();
  }
  // Test en fonction de la version présente
  else if (uiControle == 0xFA91)
  {
    // Version V0.91
    //Serial.println("StaticReadParams version 0.91");
    if (bReadCommon)
    {
      // Lecture des paramètres communs aux adresses suivantes
      p = (char *)&commonParams;
      for (int i = 0; i<(int)sizeof(CommonParams); i++)
        *p++ = EEPROM.read(address++);
      // Vérification des paramètres communs
      StaticCtrlCommonParams();
    }
    // Calcul de l'adresse de lecture en fonction du profil sélectionné
    address = MAXSIZEPROFILE + (MAXSIZEPROFILE * commonParams.iSelProfile);
    // Lecture du mot de controle présent au début des paramètres d'un profil
    p = (char *)&uiControle;
    for (int i = 0; i<(int)sizeof(uint16_t); i++)
      *p++ = EEPROM.read(address++);
    // Lecture des paramètres opérateur en fonction du profil sélectionné
    p = (char *)&paramsOperateur;
    for (int i = 0; i<(int)sizeof(ParamsOperator); i++)
      *p++ = EEPROM.read(address++);
    // Controle des paramètres
    StaticControlParams();
  }
  else if (uiControle == 0x55AA)
  {
    // Ancienne méthode (version < 0.8), on lit les anciens paramètres puis préparation de l'EEPROM avec les 5 profils
    //Serial.println("StaticReadParams version before profils");
    oldParamsOperator oldParams;
    // Lecture des paramètres aux adresses suivantes
    p = (char *)&oldParams;
    for (int i = 0; i<(int)sizeof(oldParamsOperator); i++)
      *p++ = EEPROM.read(address++);
    // Préparation EEPROM
    PreparationEEPROM( oldParams.sPrefixe, oldParams.iLanguage, oldParams.iScreenType);
  }
  else if (uiControle == 0xAA55 or uiControle == 0x5A5A or uiControle == 0xA5A5)
  {
    // Version >= 0.8 and < 0.84 (0xAA55) or > 0.84 (0x5A5A) or > 0.88 (0x5A5A);
    //Serial.println("StaticReadParams version >= 0.8 and < V0.91");
    if (bReadCommon)
    {
      // Lecture des paramètres communs aux adresses suivantes
      p = (char *)&commonParams;
      for (int i = 0; i<(int)sizeof(CommonParams); i++)
        *p++ = EEPROM.read(address++);
      // Vérification des paramètres communs
      StaticCtrlCommonParams();
    }
    // Calcul de l'adresse de lecture en fonction du profil sélectionné
    address = MAXSIZEPROFILE + (MAXSIZEPROFILE * commonParams.iSelProfile);
    // Lecture du mot de controle présent au début des paramètres d'un profil
    p = (char *)&uiControle;
    for (int i = 0; i<(int)sizeof(uint16_t); i++)
      *p++ = EEPROM.read(address++);
    // Lecture des paramètres opérateur en fonction du profil sélectionné
    p = (char *)&paramsOperateur;
    for (int i = 0; i<(int)sizeof(ParamsOperator); i++)
      *p++ = EEPROM.read(address++);
    // Controle des paramètres
    StaticControlParams();
  }
  else
  {
    // EEPROM vierge ou ancienne version, préparation par défaut
    Serial.printf("StaticReadParams new teensy or old version\n");
    SetDefault();
    PreparationEEPROM( paramsOperateur.sPrefixe, LENGLISH, commonParams.iScreenType);
  }
  return true;
}

//-------------------------------------------------------------------------
//! \brief Control Common Params
bool CModeGeneric::StaticCtrlCommonParams()
{
  char *pBool;
  pBool = (char *)&commonParams.bLumiereMin;
  if (*pBool < 0 or *pBool > 1)
    commonParams.bLumiereMin = false;
  if (commonParams.iLanguage < 0 or commonParams.iLanguage >= MAXLANGUAGE)
    commonParams.iLanguage = LFRENCH;
  if (commonParams.iScreenType < 0 or commonParams.iScreenType >= ST_MAX)
    commonParams.iScreenType = ST_SSD1306;
  if (commonParams.iUtilLed < NOLED or commonParams.iUtilLed >= MAXLED)
    commonParams.iUtilLed = NOLED;
  if (commonParams.iUserLevel < ULBEGINNER or commonParams.iUserLevel >= ULMAX)
    commonParams.iUserLevel = ULBEGINNER;
  if (commonParams.iSelProfile < 0 or commonParams.iSelProfile >= MAXPROFILES)
    commonParams.iSelProfile = 0;
  for (int i=0; i<MAXPROFILES; i++)
  {
    if (strlen(commonParams.ProfilesNames[i]) > MAXCHARPROFILE)
      sprintf( commonParams.ProfilesNames[i], "Profile%01d", i+1);
  }
  if (commonParams.dCorrectBat != commonParams.dCorrectBat or commonParams.dCorrectBat < -3.0 or commonParams.dCorrectBat > 3.0)
    commonParams.dCorrectBat = 0.0;
  if (commonParams.dCorrectExt != commonParams.dCorrectExt or commonParams.dCorrectExt < -8.0 or commonParams.dCorrectExt > 8.0)
    commonParams.dCorrectExt = 0.0;
#ifdef WITHBLUETOOTH
  pBool = (char *)&commonParams.bBluetooth;
  if (*pBool < 0 or *pBool > 1)
    commonParams.bBluetooth = false;
#endif
  return true;
}

//-------------------------------------------------------------------------
//! \brief Control Profil Params
bool CModeGeneric::StaticControlParams()
{
  char *pBool;
  // Vérification des paramètres
  if (paramsOperateur.iModeRecord < RECAUTO or paramsOperateur.iModeRecord >= MAXPROT)
  {
    // 1er paramètre hors des limites, passage aux paramètres par défaut
    Serial.printf("StaticControlParams set default\n");
    SetDefault();
    PreparationEEPROM( paramsOperateur.sPrefixe, LENGLISH, commonParams.iScreenType);
  }
  else
  {
    if (paramsOperateur.iSeuilDet < 5 or paramsOperateur.iSeuilDet > 99)
      paramsOperateur.iSeuilDet = 20;
    if (paramsOperateur.uiFreqMin < MinInterestFreq or paramsOperateur.uiFreqMin > MaxInterestFreq)
    {
      Serial.printf("uiFreqMin %d, MinInterestFreq %d, MaxInterestFreq %d\n", paramsOperateur.uiFreqMin, MinInterestFreq, MaxInterestFreq);
      paramsOperateur.uiFreqMin = 15000;
    }
    if (paramsOperateur.uiFreqMax < MinInterestFreq or paramsOperateur.uiFreqMax > MaxInterestFreq)
      paramsOperateur.uiFreqMax = 120000;
    if (paramsOperateur.uiFreqMin > paramsOperateur.uiFreqMax and paramsOperateur.uiFreqMin < 50000)
      paramsOperateur.uiFreqMax = paramsOperateur.uiFreqMin + 20000;
    else if (paramsOperateur.uiFreqMin > paramsOperateur.uiFreqMax)
      paramsOperateur.uiFreqMin = paramsOperateur.uiFreqMax - 20000;
    if (paramsOperateur.uiFreqMinA < MinInterestFreq or paramsOperateur.uiFreqMinA > MaxInterestFreqA)
      paramsOperateur.uiFreqMinA = 100;
    if (paramsOperateur.uiFreqMaxA < MinInterestFreq or paramsOperateur.uiFreqMaxA > MaxInterestFreqA)
      paramsOperateur.uiFreqMaxA = 20000;
    if (paramsOperateur.uiDurMin < 1 or paramsOperateur.uiDurMin  > 99)
      paramsOperateur.uiDurMin = 3;
    if (paramsOperateur.uiDurMax < 1 or paramsOperateur.uiDurMax > 999)  
      paramsOperateur.uiDurMax = 30;
    if (paramsOperateur.uiDurMin > paramsOperateur.uiDurMax)
      paramsOperateur.uiDurMax = paramsOperateur.uiDurMin;
    int iMaxTRECRecord = 3600;
    if (CModeGeneric::GetRecorderType() >= PASSIVE_STEREO)
    {
      switch (paramsOperateur.uiFe)
      {
      case FE24KHZ : iMaxTRECRecord = 3600*24   ; break;
      case FE48KHZ : iMaxTRECRecord = 3600*12   ; break;
      case FE96KHZ : iMaxTRECRecord = 3600*6    ; break;
      case FE192KHZ: iMaxTRECRecord = 3600*3    ; break;
      case FE250KHZ: iMaxTRECRecord = 3600*2    ; break;
      case FE384KHZ: iMaxTRECRecord = 3600+30*60; break;
      case FE500KHZ: iMaxTRECRecord = 3600      ; break;
      }
    }
    else
    {
      switch (paramsOperateur.uiFe)
      {
      case FE24KHZ : iMaxTRECRecord = 3600*24; break;
      case FE48KHZ : iMaxTRECRecord = 3600*24; break;
      case FE96KHZ : iMaxTRECRecord = 3600*12; break;
      case FE192KHZ: iMaxTRECRecord = 3600*6 ; break;
      case FE250KHZ: iMaxTRECRecord = 3600*4 ; break;
      case FE384KHZ: iMaxTRECRecord = 3600*3 ; break;
      case FE500KHZ: iMaxTRECRecord = 3600*2 ; break;
      }
    }
    if (paramsOperateur.uiTRECRecord < 1)
      paramsOperateur.uiTRECRecord = 1;
    if (paramsOperateur.uiTRECRecord > iMaxTRECRecord)
      paramsOperateur.uiTRECRecord = iMaxTRECRecord;
    if (paramsOperateur.uiTRECWait < 1)
      paramsOperateur.uiTRECWait = 1;
    if (paramsOperateur.uiTRECWait > 3600)
      paramsOperateur.uiTRECWait = 540;
    int iMaxFE  = MAXFE;
    int iDefFE  = FE384KHZ;
    if (F_CPU < 144000000)
    {
      iMaxFE = FE384KHZ;
      iDefFE = FE250KHZ;
    }
    if (paramsOperateur.uiFe < FE24KHZ or paramsOperateur.uiFe >= iMaxFE)
      paramsOperateur.uiFe = iDefFE;
    if (paramsOperateur.uiFeA < FE24KHZ or paramsOperateur.uiFeA >= FE192KHZ)
      paramsOperateur.uiFeA = FE48KHZ;
    if (paramsOperateur.uiGainNum < GAIN0dB or paramsOperateur.uiGainNum >= MAXGAIN)
      paramsOperateur.uiGainNum = GAIN0dB;
    if (paramsOperateur.iFreqFiltreHighPass < 0 or paramsOperateur.iFreqFiltreHighPass > MAXHIHGPASS)
      paramsOperateur.iFreqFiltreHighPass = 0;
    if (isnan(paramsOperateur.fFreqFiltreHighPass) or paramsOperateur.fFreqFiltreHighPass < 0.0 or paramsOperateur.fFreqFiltreHighPass > (float)MAXHIHGPASS)
      paramsOperateur.fFreqFiltreHighPass = 0.0;
    if (paramsOperateur.bTypeSeuil != false and paramsOperateur.bTypeSeuil != true)
      paramsOperateur.bTypeSeuil  = false;
    if (paramsOperateur.iSeuilAbs < -110 or paramsOperateur.iSeuilAbs > -30)
      paramsOperateur.iSeuilAbs   = -90;
    if (paramsOperateur.bSaveNoise != false and paramsOperateur.bSaveNoise != true)
      paramsOperateur.bSaveNoise = false;
    if (paramsOperateur.bPermanentTempH != false and paramsOperateur.bPermanentTempH != true)
      paramsOperateur.bPermanentTempH = false;
    if (paramsOperateur.bRLAffPerm != false and paramsOperateur.bRLAffPerm != true)
      paramsOperateur.bRLAffPerm = false;
    if (paramsOperateur.fRefreshGRH != paramsOperateur.fRefreshGRH or paramsOperateur.fRefreshGRH < 0.2 or paramsOperateur.fRefreshGRH > 2.0)
      paramsOperateur.fRefreshGRH = 1.0;
    if (paramsOperateur.fHetSignalLevel != paramsOperateur.fHetSignalLevel or paramsOperateur.fHetSignalLevel < 0.1 or paramsOperateur.fHetSignalLevel > 0.9)
      paramsOperateur.fHetSignalLevel = 0.5;
    // Init de la langue des modificateurs
    CGenericModifier::SetLanguage( commonParams.iLanguage);
    // Test mode d'enregistrement
    // Par défaut, on est toujours sans protocole au démarrage sauf pour le protocole Point Fixe
    //Serial.printf("CModeGeneric::StaticControlParams iModeRecord %d\n", paramsOperateur.iModeRecord);
    switch (paramsOperateur.iModeRecord)
    {
    default:
    case PHETER   :  // Mode hétérodyne
    case RECAUTO  :  // Mode enregistreur automatique
    case PRHINOLOG:  // Mode RhinoLogger
    case PROTFIXE :  // Protocole Vigie-Chiro point fixe
    case AUDIOREC :  // Mode enregistreur audio
    case TIMEDREC :  // Timed recording
      // Mode autorisé au démarrage
      break;
    case PROTPED  :  // Protocole Vigie-Chiro pédestre
    case PROTROUT :  // Protocole Vigie-Chiro routier
    case PTSTMICRO:  // Protocole test micro
      // Pour ces modes, on impose toujours AUTO ou hétérodyne
      if (CModeGeneric::GetRecorderType() == ACTIVE_RECORDER)
        paramsOperateur.iModeRecord = PHETER;
      else
        paramsOperateur.iModeRecord = RECAUTO;
    }
    if (ReadRecorderType() != ACTIVE_RECORDER and (paramsOperateur.iModeRecord == PHETER or paramsOperateur.iModeRecord == AUDIOREC))
      // Modes réservés uniquement à l'Active Recorder
      paramsOperateur.iModeRecord = RECAUTO;
    if (paramsOperateur.iAutoHeter < HAM_MANUAL and paramsOperateur.iAutoHeter >= HAM_MAX)
      paramsOperateur.iAutoHeter  = HAM_MANUAL;
    if (paramsOperateur.bAutoRecH != false and paramsOperateur.bAutoRecH != true)
      paramsOperateur.bAutoRecH  = false;
    pBool = (char *)&paramsOperateur.bFiltre;
    if (*pBool < 0 or *pBool > 1 or F_CPU < 140000000)
      paramsOperateur.bFiltre = false;
    pBool = (char *)&paramsOperateur.bExp10;
    if (*pBool < 0 or *pBool > 1)
      paramsOperateur.bExp10 = false;
    if (paramsOperateur.iNbDetect < 1 or paramsOperateur.iNbDetect > 8)
      paramsOperateur.iNbDetect = 4;
    if (paramsOperateur.iPeriodTemp < 10 or paramsOperateur.iPeriodTemp > 3600)
      paramsOperateur.iPeriodTemp = 600;
    if (paramsOperateur.bBatcorderLog != false and paramsOperateur.bBatcorderLog != true)
      paramsOperateur.bBatcorderLog = false;
    if (strlen(paramsOperateur.sHDebut) != 5 or paramsOperateur.sHDebut[2] != ':')
      strcpy( paramsOperateur.sHDebut,  "22:00");
    if (strlen(paramsOperateur.sHFin) != 5 or paramsOperateur.sHFin[2] != ':')
      strcpy( paramsOperateur.sHFin,    "06:00");
    if (strlen(paramsOperateur.sPrefixe) != 5)
      strcpy( paramsOperateur.sPrefixe, "PRec_");
    if (CModeGeneric::GetRecorderType() >= PASSIVE_STEREO)
    {
      // On a PRS, if profile by default, we force the stereo mode
      if (commonParams.iSelProfile == 0 and paramsOperateur.iStereoRecMode != SMSTEREO)
        paramsOperateur.iStereoRecMode = SMSTEREO;
      if (paramsOperateur.iStereoRecMode < 0 or paramsOperateur.iStereoRecMode >= SMMAX)
        paramsOperateur.iStereoRecMode = SMSTEREO;   // For PRS, stereo recording mode
    }
    else
    {
      if (paramsOperateur.iStereoRecMode != SMRIGHT)
        paramsOperateur.iStereoRecMode = SMRIGHT;    // Mono recording on right chanel
    }
    if (paramsOperateur.iMicrophoneType < 0 or paramsOperateur.iMicrophoneType >= MTMAX)
      paramsOperateur.iMicrophoneType = MTICS40730;
    if (paramsOperateur.iMasterSlave < 0 or paramsOperateur.iMasterSlave >= MAXMS)
      paramsOperateur.iMasterSlave = MASTER;
    if (paramsOperateur.iTopAudioFreq < 0 or paramsOperateur.iTopAudioFreq > 50)
      paramsOperateur.iTopAudioFreq    = 8;
    if (paramsOperateur.iDurTop < TS256 or paramsOperateur.iDurTop > TSMAX)
      paramsOperateur.iDurTop          = TS256;
    if (paramsOperateur.iTopPer < 0 or paramsOperateur.iTopPer > 10)
      paramsOperateur.iTopPer          = 0;
    if (paramsOperateur.iLEDSynchro < NOLEDSYNCH or paramsOperateur.iLEDSynchro >= MAXLEDSYNCH)
      paramsOperateur.iLEDSynchro      = LEDRECSYNCH;
    if (paramsOperateur.iPreTrigDurAuto  < 0  or paramsOperateur.iPreTrigDurAuto  > 10)
      paramsOperateur.iPreTrigDurAuto  = 1;
    if (paramsOperateur.iPreTrigDurHeter <= 0 or paramsOperateur.iPreTrigDurHeter > 10)
      paramsOperateur.iPreTrigDurHeter = 3;
    if (paramsOperateur.iHeterSel < HSF_NOSEL or paramsOperateur.iHeterSel >= HSF_MAX)
      paramsOperateur.iHeterSel = HSF_NOSEL;
    if (paramsOperateur.bAutoPlay != false and paramsOperateur.bAutoPlay != true)
      paramsOperateur.bAutoPlay = false;
    if (paramsOperateur.bGraphHet != false and paramsOperateur.bGraphHet != true)
      paramsOperateur.bGraphHet = true;
    if (paramsOperateur.bAGCHet != false and paramsOperateur.bAGCHet != true)
      paramsOperateur.bAGCHet = true;
    if (CModeGeneric::GetRecorderType() >= PASSIVE_SYNCHRO and commonParams.iSelProfile == 0)
    {
      // On a PRS-S, if profile by default, we force the synchro parameters
      paramsOperateur.iTopAudioFreq = 8;
      paramsOperateur.iDurTop       = TS256;
      paramsOperateur.iTopPer       = 0;
    }
    // Vérification des bandes
    int iNbBd = 0;
    for (int b=0; b<MAXBD; b++)
    {
      paramsOperateur.batBand[b].sName[MAXNAMEBD] = 0;
      if (strlen(paramsOperateur.batBand[b].sName) != MAXNAMEBD)
        strcpy( paramsOperateur.batBand[b].sName, txtNamesBD[b]);
      if (paramsOperateur.batBand[b].fMin < FMINBD or paramsOperateur.batBand[b].fMin > FMAXBD)
        paramsOperateur.batBand[b].fMin = 40.00;
      if (paramsOperateur.batBand[b].fMax < FMINBD or paramsOperateur.batBand[b].fMax > FMAXBD)
        paramsOperateur.batBand[b].fMax = 60.00;
      if (paramsOperateur.batBand[b].iType < 0 or paramsOperateur.batBand[b].iType >= BDTYPEMAX)
        paramsOperateur.batBand[b].iType        = BDINVALID;
      if (paramsOperateur.batBand[b].iMinDuration < 1 or paramsOperateur.batBand[b].iMinDuration > 99)
        paramsOperateur.batBand[b].iMinDuration = 24;
      if (paramsOperateur.batBand[b].iMaxDuration < 1 or paramsOperateur.batBand[b].iMaxDuration > 99)
        paramsOperateur.batBand[b].iMaxDuration = 96;
      if (paramsOperateur.batBand[b].fMinFWidth < 0.5 or paramsOperateur.batBand[b].fMinFWidth > 99)
        paramsOperateur.batBand[b].fMinFWidth   = 20.0;
      /*if (CModeGeneric::bDebug)
        Serial.printf("StaticReadParams Band %d %d FMin %06.2fkHz FMax %06.2fkHz\n", b+1, paramsOperateur.batBand[b].sName, paramsOperateur.batBand[b].fMin, paramsOperateur.batBand[b].fMax);*/
      if (paramsOperateur.batBand[b].iType != BDINVALID)
        iNbBd++;
    }
    if (iNbBd == 0)
      // Initialisation des bandes par défaut
      SetDefaultRLBands();
  }
  return true;
}
  
//-------------------------------------------------------------------------
// Ecriture des paramètres sauvegardés en EEPROM
void CModeGeneric::WriteParams()
{
  //Serial.println("CModeGeneric::WriteParams");
  StaticWriteParams();
}

//-------------------------------------------------------------------------
// Writing parameters to save them in static function
void CModeGeneric::StaticWriteParams()  
{
  //Serial.printf("StaticWriteParams write profile %d [%s], paramsOperateur.uiFreqMaxA %d Hz\n", commonParams.iSelProfile, commonParams.ProfilesNames[commonParams.iSelProfile], paramsOperateur.uiFreqMaxA);
  //Serial.printf("StaticWriteParams Profile %d, Name %s, FE %s, Mode %s\n", commonParams.iSelProfile, commonParams.ProfilesNames[commonParams.iSelProfile], sSFValues[paramsOperateur.uiFe], sOMValues[paramsOperateur.iModeRecord]);
  //Serial.printf("CModeGeneric::StaticWriteParams iModeRecord %d\n", paramsOperateur.iModeRecord);
  //Serial.printf("sizeof(CommonParams)=%d, sizeof(ParamsOperator)=%d\n", sizeof(CommonParams), sizeof(ParamsOperator));
  //Serial.printf("CModeGeneric::StaticWriteParams dCorrectBat %f, dCorrectExt %f\n", commonParams.dCorrectBat, commonParams.dCorrectExt);
  /*
   * L'EEPROM est découpée en zones de 512 octets pour pouvoir ajouter des paramètres sans trop de problème
   *    0-   3 Mot de controle de version CModeGeneric::uiCtrlVersion
   *    4- 511 commonParams (taille 84 avec la V0.92)
   *  512-1023 paramsOperateur profil 1 (taille 384 avec la V0.92)
   * 1024-1535 paramsOperateur profil 2
   * 1536-2047 paramsOperateur profil 3
   * 2048-2559 paramsOperateur profil 4
   * 2560-3071 paramsOperateur profil 5
   */
  // Sauvegarde systématique en utilisant la fonction update qui ne fait une écriture que si la valeur présente en EEPROM est différente de celle à mémoriser
  int address = 0;
  // Sauvegarde du mot de controle de version
  // A partir de la V0.92 le mot de controle de version est composé de 1 suivit de la version 092
  // 0xABCD A = VPARAMS, B, C et D = Version du logiciel (V0.92=092)
  // Et est mémorisé uniquement au début des paramètres communs
  uint16_t uiControle = CModeGeneric::uiCtrlVersion;
  byte *p = (byte *)&uiControle;
  for (int i = 0; i<(int)sizeof(uint16_t); i++)
    EEPROM.update(address++, *p++);
  // Sauvegarde des paramètres communs à la suite du mot de controle de version
  p = (byte *)&commonParams;
  for (int i = 0; i<(int)sizeof(CommonParams); i++)
    EEPROM.update(address++, *p++);
  // Calcul de l'adresse en fonction du profil sélectionné
  address = MAXSIZEPROFILE + (MAXSIZEPROFILE * commonParams.iSelProfile);
  // Sauvegarde des paramètres opérateur du profil sélectionné
  p = (byte *)&paramsOperateur;
  for (int i = 0; i<(int)sizeof(ParamsOperator); i++)
    EEPROM.update(address++, *p++);
}

//-------------------------------------------------------------------------
//! \brief Preparation of EEPROM with default profiles
//! \param pPrefixe Previous or default prefix
//! \param iLangue  Previous language or default language
//! \param iLangue  Previous screen type or default type
void CModeGeneric::PreparationEEPROM(
  char *pPrefixe,
  int iLangue,
  int iScreenType)
{
  // Mémorisation du profil sélectionné
  int iMemo = commonParams.iSelProfile;
  // Initialisation des paramètres opérateurs aux valeurs par défaut pour les différents profils (de 1 à 4)
  for (int i=1; i<MAXPROFILES; i++)
  {
    SetDefault();
    strcpy( paramsOperateur.sPrefixe, pPrefixe);
    commonParams.iSelProfile = i;
    switch(i)
    {
    default:
      // 0 and 1, default params
      paramsOperateur.uiFe        = FE384KHZ;
      paramsOperateur.iModeRecord = RECAUTO;
      break;
    case 2:
      // 500 kHz
      paramsOperateur.uiFe = FE500KHZ;
      break;
    case 3:
      // RhinoLogger
      paramsOperateur.uiFe        = FE250KHZ;
      paramsOperateur.iModeRecord = PRHINOLOG;
      break;
    case 4:
      // Audio recorder
      paramsOperateur.uiFe        = FE48KHZ;
      if (GetRecorderType() == ACTIVE_RECORDER)
        paramsOperateur.iModeRecord = AUDIOREC;
      else
        paramsOperateur.iModeRecord = RECAUTO;
      paramsOperateur.bExp10      = false;
      break;
    }
    // Mémorisation du profil
//Serial.println("StaticWriteParams on CModeGeneric::PreparationEEPROM A");
    StaticWriteParams();
    //Serial.printf("PreparationEEPROM Profile %d, Name %s, FE %s, Mode %s\n", i+1, commonParams.ProfilesNames[i], sSFValues[paramsOperateur.uiFe], sOMValues[paramsOperateur.iModeRecord]);
  }
  // Mémorisation du profil 0 et des paramètres communs
  SetDefault();
  commonParams.iScreenType = iScreenType;
  commonParams.iLanguage   = iLangue;
//Serial.println("StaticWriteParams on CModeGeneric::PreparationEEPROM B");
  StaticWriteParams();
  //Serial.printf("PreparationEEPROM Profile 1, Name %d, FE %s, Mode %s\n", commonParams.ProfilesNames[0], sSFValues[paramsOperateur.uiFe], sOMValues[paramsOperateur.iModeRecord]);
  // Restitution du profil sélectionné
  commonParams.iSelProfile = iMemo;
}
    
//-------------------------------------------------------------------------
//! \brief Function test of the SD card. Resets the card if necessary and goes into error if the card stops responding
//! \return true if OK, false if SD error
bool CModeGeneric::isSDInit()
{
  bool bOK = true;
  // Warning, possible SD error (freeClusterCount = 0!)
  if (sd.vol()->freeClusterCount() == 0)
  {
    //Serial.println("freeClusterCount = 0");
    // Trace of the problem
    for (int i=0; i<4; i++)
    {
      digitalWriteFast( 13, HIGH);
      delay(200);
      digitalWriteFast( 13, LOW);
      delay(200);
    }
    sd.end();
    if (!sd.begin(SdioConfig(FIFO_SDIO)))
    {
      //Serial.println("Erreur sd.begin");
      bOK = false;
      if (sd.card()->errorCode() == SD_CARD_ERROR_ACMD41)
        bOK = LogFile::AddLog( LLOG, txtSDFATTO[CModeGeneric::GetCommonsParams()->iLanguage]);
      else
        bOK = LogFile::AddLog( LLOG, txtSDFATBEGIN[CModeGeneric::GetCommonsParams()->iLanguage], sd.card()->errorCode());
    }
  }
  if (!bOK)
  {
    // SD access problem
    CModeError::SetTxtError((char *)txtSDERROR[CModeGeneric::GetCommonsParams()->iLanguage]);
    iNewMode = MERROR;
  }
  return bOK;
}

//-------------------------------------------------------------------------
// Update screen (standard behavior = pDisplay->sendBuffer())
void CModeGeneric::UpdateScreen()
{
  // A full display requires 33ms, the time between two sample processing interrupts (32ms to 384kHz).
  // Interruptions disrupts screen update
  // If interrupts are stopped, you may lose a sample buffer
  // By cutting the update in 4 zones for about 10ms each and blocking the interrupts on the display of each zone, we do not lose buffer
  // Stop interrupts, Display 1/4, Enables interrupts, delays 1ms ...
  // Warning, updateDisplayArea uses tile coordinates (8x8 pixels)
#if defined(__IMXRT1062__) // Teensy 4.1
  // For Teensy 4.1
  // Classic UpdateScreen = 37ms max at 280MHz
  if (GetRecorderType() >= PASSIVE_STEREO and paramsOperateur.iStereoRecMode == SMSTEREO)
  {
    // In stereo, need less than 665µs for stereo mode (256 samples at 384kHz) for one updateDisplayArea
    // This UpdateScreen = 118µs for one updateDisplayArea
    //unsigned long uiTimePrint = micros();
    __disable_irq();
    pDisplay->updateDisplayArea( 0, 0, 4, 4);
    __enable_irq();
    //unsigned long uiTimePrintB = micros();
    delay(1); // To process a possible DMA interruption
    __disable_irq();
    pDisplay->updateDisplayArea( 4, 0, 4, 4);
    __enable_irq();
    delay(1); // To process a possible DMA interruption
    __disable_irq();
    pDisplay->updateDisplayArea( 8, 0, 4, 4);
    __enable_irq();
    delay(1); // To process a possible DMA interruption
    __disable_irq();
    pDisplay->updateDisplayArea(12, 0, 4, 4);
    __enable_irq();
    delay(1); // To process a possible DMA interruption
    __disable_irq();
    pDisplay->updateDisplayArea( 0, 4, 4, 4);
    __enable_irq();
    delay(1); // To process a possible DMA interruption
    __disable_irq();
    pDisplay->updateDisplayArea( 4, 4, 4, 4);
    __enable_irq();
    delay(1); // To process a possible DMA interruption
    __disable_irq();
    pDisplay->updateDisplayArea( 8, 4, 4, 4);
    __enable_irq();
    delay(1); // To process a possible DMA interruption
    __disable_irq();
    pDisplay->updateDisplayArea(12, 4, 4, 4);
    __enable_irq();
    //Serial.printf("updateDisplayArea T4.1 4x4 Elem %dµs, total %dµs\n", uiTimePrintB-uiTimePrint, micros()-uiTimePrint);
  }
  else
  {
    // This Update Screen, 860µs for one updateDisplayArea
    //unsigned long uiTimePrint = micros();
    __disable_irq();
    pDisplay->updateDisplayArea( 0, 0, 8, 4);
    __enable_irq();
    //unsigned long uiTimePrintB = micros();
    delay(1); // To process a possible DMA interruption
    __disable_irq();
    pDisplay->updateDisplayArea( 8, 0, 8, 4);
    __enable_irq();
    delay(1); // To process a possible DMA interruption
    __disable_irq();
    pDisplay->updateDisplayArea( 0, 4, 8, 4);
    __enable_irq();
    delay(1); // To process a possible DMA interruption
    __disable_irq();
    pDisplay->updateDisplayArea( 8, 4, 8, 4);
    __enable_irq();
    //Serial.printf("updateDisplayArea T4.1 8x4 Elem %dµs, total %dµs\n", uiTimePrintB-uiTimePrint, micros()-uiTimePrint);
  }
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  // For Teensy 3.6
  // Classic UpdateScreen = 33ms max at 144MHz
  // This UpdateScreen = 7ms at 144MHz !!!
  // For Teensy 4.1
  // Classic UpdateScreen = 37ms max at 280MHz
  // This UpdateScreen = 6ms at 280MHz !!! One element 100µs
  /*unsigned long uiTimePrint = micros();
  CGenericMode::UpdateScreen();
  Serial.printf("Classic UpdateScreen %dµs\n", micros()-uiTimePrint);*/
  //unsigned long uiTimePrint = micros();
  unsigned char sreg_backup = SREG;
  cli();
  pDisplay->updateDisplayArea( 0, 0, 8, 4);
  SREG = sreg_backup;
  //unsigned long uiTimePrintB = micros();
  delay(1); // To process a possible DMA interruption
  cli();
  pDisplay->updateDisplayArea( 8, 0, 8, 4);
  SREG = sreg_backup;
  delay(1); // To process a possible DMA interruption
  cli();
  pDisplay->updateDisplayArea( 0, 4, 8, 4);
  SREG = sreg_backup;
  delay(1); // To process a possible DMA interruption
  cli();
  pDisplay->updateDisplayArea( 8, 4, 8, 4);
  SREG = sreg_backup;
  //Serial.printf("updateDisplayArea T3.6 8x4 Elem %dµs, total %dµs\n", uiTimePrintB-uiTimePrint, micros()-uiTimePrint);
#endif
}

//-------------------------------------------------------------------------
// Calcul des minutes de début et fin d'enregistrement
void CModeGeneric::CalcTimeDebFin()
{
  int iDebH, iDebM, iFinH, iFinM;
  sscanf( paramsOperateur.sHDebut, "%d:%d", &iDebH, &iDebM);
  sscanf( paramsOperateur.sHFin  , "%d:%d", &iFinH, &iFinM);
  // Calcul du nombre de Minutes de début et de fin d'acquisition/enregistrement par rapport à minuit
  iDebMinutes = iDebH * 60 + iDebM;
  iFinMinutes = iFinH * 60 + iFinM;
  //Serial.printf("CalcTimeDebFin sHDebut=%s, iDebMinutes=%d, sHFin=%s, iFinMinutes=%d\n", paramsOperateur.sHDebut, iDebMinutes, paramsOperateur.sHFin, iFinMinutes);
}
  
//-------------------------------------------------------------------------
// Retourne true si on doit passer en veille
bool CModeGeneric::IsTimeVeille()
{
  // Calcul éventuel des minutes de début et fin d'enregistrement
  if (iFinMinutes < 0 or iDebMinutes < 0)
    CalcTimeDebFin();
  // Calcul du nombre de minutes par rapport à minuit pour l'heure courante
  int iCurrentNbMinutes = hour() * 60 + minute();
  // Test si on est dans la période de veille
  bool bVeille = true;
  if (iFinMinutes == iDebMinutes)
  {
    // Heure de début et de fin identiques, pas de veille possible
    //Serial.println("Cas ou l'heure de début et de fin sont identique");
    bVeille = false;
  }
  else if (iFinMinutes > iDebMinutes and iCurrentNbMinutes >= iDebMinutes and iCurrentNbMinutes <= iFinMinutes)
  {
    // Cas ou l'heure de début est plus petite que l'heure de fin
    //Serial.println("Cas ou l'heure de début est plus petite que l'heure de fin");
    bVeille = false;
  }
  else if (iFinMinutes < iDebMinutes and (iCurrentNbMinutes >= iDebMinutes or iCurrentNbMinutes <= iFinMinutes))
  {
    // Cas ou l'heure de début et de fin sont de chaque coté de minuit
    //Serial.println("Cas ou l'heure de début et de fin sont de chaque coté de minuit");
    bVeille = false;
  }
  /*Serial.printf("IsTimeVeille %02d:%02d, Début %s, Fin %s\n", hour(), minute(), paramsOperateur.sHDebut, paramsOperateur.sHFin);
  if (bVeille)
    Serial.printf("Passage en veille iCurrentNbMinutes %d, iDebMinutes %d, iFinMinutes %d\n", iCurrentNbMinutes, iDebMinutes, iFinMinutes);
  else
    Serial.printf("Reste en Record iCurrentNbMinutes %d, iDebMinutes %d, iFinMinutes %d\n", iCurrentNbMinutes, iDebMinutes, iFinMinutes);*/
  return bVeille;
}

const char *txtTYPERECM[]      = {"PASSIVE_RECORDER",
                                  "ACTIVE_RECORDER" ,
                                  "PASSIVE_STEREO"  ,
                                  "PASSIVE_SYNCHRO" ,
                                  "PASSIVE_MSYNCHRO"};

//-------------------------------------------------------------------------
// Read options bits to decode the type of the recorder
// Return recorder type
int CModeGeneric::ReadRecorderType()
{
  // Reading bits of the type
  // D0 D1 D2
  //  1  1  1 Passive Recorder (PR)
  //  0  1  1 Active  Recorder (AR)
  //  1  0  1 Passive Recorder Stereo (PS)
  //  0  0  1 Passive Recorder Stereo Synchro (PRS)
  //  0  0  0 Passive Recorder Mono   Synchro (PMS)
  int iB0 = 0, iB1 = 0, iB2 = 0;
  delay(80);
  iB0 = digitalRead(OPT_B0);
  iB1 = digitalRead(OPT_B1);
  iB2 = digitalRead(OPT_B2);
  // By default Passive Recorder
  iRecorderType = PASSIVE_RECORDER;
  if      (iB2 == HIGH and iB1 == HIGH and iB0 == HIGH)
    iRecorderType = PASSIVE_RECORDER;
  else if (iB2 == HIGH and iB1 == HIGH and iB0 == LOW)
    iRecorderType = ACTIVE_RECORDER;
  else if (iB2 == HIGH and iB1 == LOW  and iB0 == HIGH)
    iRecorderType = PASSIVE_STEREO;
  else if (iB2 == HIGH and iB1 == LOW  and iB0 == LOW)
    iRecorderType = PASSIVE_SYNCHRO;
  else if (iB2 == LOW  and iB1 == LOW  and iB0 == LOW)
    iRecorderType = PASSIVE_MSYNCHRO;
  //LogFile::AddLog( LLOG, "ReadRecorderType D0 %d, D1 %d, D2 %d", iB0, iB1, iB2);
  // Vérification fréquence processeur
  if (iRecorderType >= PASSIVE_STEREO and F_CPU < 179000000)
  {
    // Impossible de faire de la stéréo à moins de 180MHz de fréquence processeur
    iRecorderType = PASSIVE_RECORDER;
    LogFile::AddLog( LLOG, txtLogCPULOW[CModeGeneric::GetCommonsParams()->iLanguage]);
    Serial.println(txtLogCPULOW[CModeGeneric::GetCommonsParams()->iLanguage]);
  }
//iRecorderType = PASSIVE_STEREO;
//iRecorderType = PASSIVE_MSYNCHRO;
//iRecorderType = PASSIVE_RECORDER;
  return iRecorderType;
}

//-------------------------------------------------------------------------
// Initialization function and calibration of the ADC0 to read the heterodyne frequency
void CModeGeneric::ADC0Init(
  bool bRefVCC  // true for VCC Ref, false for internal 1V2 ref
  )
{
#if defined(__IMXRT1062__) // Teensy 4.1 *** ATTENTION sur T4.1 ADC1 = ADC0 T3.6 ***
  // Init de l'ADC1
  // 66.8.6 Configuration register (ADCx_CFG)
  ADC1_CFG = 0;                                                     // OVWREN disable overwriting, ADTRG Software trigger, REFSEL Selects VREFH/VREFL as reference voltage
                                                                    // ADHSC Normal conversion selected, ADLPC ADC hard block not in low power mode
  ADC1_CFG = ADC_CFG_MODE(2) | ADC_CFG_ADSTS(3) | ADC_CFG_ADLSMP    // 12 bits, 25 sample period, long sample time 
           | ADC_CFG_ADICLK(1) | ADC_CFG_ADIV(1) | ADC_CFG_AVGS(1); // IPG clock divided by 2, Input clock / 2, 8 samples averaged
  ADC1_CFG |= ADC_CFG_OVWREN;                                       // OVWREN enable overwriting
  ADC1_CFG |= ADC_CFG_ADLPC;                                        // ADC hard block in low power mode
  // 66.8.7 General control register (ADCx_GC)
  ADC1_GC &= ~ADC_GC_ACFE;                                          // Compare function disable
  ADC1_GC &= ~ADC_GC_ACFGT;                                         // Compare Function Greater Than disable
  ADC1_GC &= ~ADC_GC_ACREN;                                         // Range function disable
  ADC1_GC |= ADC_GC_AVGE;                                           // Averaging
  ADC1_GC &= ~ADC_GC_ADCO;                                          // Continuous Conversion disable
  ADC2_GC &= ~ADC_GC_DMAEN;                                         // DMA disable
  ADC2_GC &= ~ADC_GC_ADACKEN;                                       // ADACKEN Asynchronous clock output disable

  // Calibration ADC1
  ADC1_GC &= ~ADC_GC_CAL;                                           // Reset Cal bit
  ADC1_GS = ADC_GS_CALF;                                            // Reset calibration status
  ADC1_GC = ADC_GC_CAL;                                             // Start calibration
  while (ADC1_GC & ADC_GC_CAL);                                     // Wait for calibration
  if ((ADC1_GS & ADC_GS_CALF) > 0) Serial.println("CModeGeneric::ADC0Init Teensy 4.1 Error calibration");
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  // Init de l'ADC0
  SIM_SCGC6 |= SIM_SCGC6_ADC0;
  ADC0_CFG1  = ADC_CFG1_ADIV(2) | ADC_CFG1_ADICLK(1) |                // Single-ended 12 bits, long sample time
               ADC_CFG1_MODE(1) | ADC_CFG1_ADLSMP;
  ADC0_CFG2  = ADC_CFG2_ADLSTS(0);                                    // Select channels ADxxxa and Default longest sample time; 20 extra ADCK cycles; 24 ADCK cycles total
  if (bRefVCC)
    ADC0_SC2   = ADC_SC2_REFSEL(0);                                   // Voltage reference = Vcc
  else
    ADC0_SC2   = ADC_SC2_REFSEL(1);                                   // Voltage ref internal (1V2), software trigger
  ADC0_SC3   = ADC_SC3_AVGE | ADC_SC3_AVGS(3);                        // Enable averaging, 32 samples
  // Calibration
  uint16_t sum;

  // Begin calibration
  ADC0_SC3 = ADC_SC3_CAL | ADC_SC3_AVGE | ADC_SC3_AVGS(3);
  // Wait for calibration
  while (ADC0_SC3 & ADC_SC3_CAL);

  // Plus side gain
  sum = ADC0_CLPS + ADC0_CLP4 + ADC0_CLP3 + ADC0_CLP2 + ADC0_CLP1 + ADC0_CLP0;
  sum = (sum / 2) | 0x8000;
  ADC0_PG = sum;

  // Minus side gain (not used in single-ended mode)
  sum = ADC0_CLMS + ADC0_CLM4 + ADC0_CLM3 + ADC0_CLM2 + ADC0_CLM1 + ADC0_CLM0;
  sum = (sum / 2) | 0x8000;
  ADC0_MG = sum;
#endif
}

//-------------------------------------------------------------------------
// Single-shot playback function of an analog input with ADC 0 to read the heterodyne frequency
// Returns the value read (0-4095)
uint16_t CModeGeneric::ADC0Read(
  int iPin  // Pin to read 
  )
{
  uint16_t value = 0;
#if defined(__IMXRT1062__) // Teensy 4.1 *** ATTENTION sur T4.1 ADC1 = ADC0 T3.6 ***
	PROGMEM static const uint8_t adcT4_pin_to_channel[] = {
		7,      // 0/A0   AD_B1_02
		8,      // 1/A1   AD_B1_03
		12,     // 2/A2   AD_B1_07
		11,     // 3/A3   AD_B1_06
		6,      // 4/A4   AD_B1_01
		5,      // 5/A5   AD_B1_00
		15,     // 6/A6   AD_B1_10
		0,      // 7/A7   AD_B1_11
		13,     // 8/A8   AD_B1_08
		14,     // 9/A9   AD_B1_09
		255,	  // 10/A10 AD_B0_12 - only on ADC1, 1 - can't use for audio
		255,	  // 11/A11 AD_B0_13 - only on ADC1, 2 - can't use for audio
		3,      // 12/A12 AD_B1_14
		4,      // 13/A13 AD_B1_15
		7,      // 14/A0  AD_B1_02
		8,      // 15/A1  AD_B1_03
		12,     // 16/A2  AD_B1_07
		11,     // 17/A3  AD_B1_06
		6,      // 18/A4  AD_B1_01
		5,      // 19/A5  AD_B1_00
		15,     // 20/A6  AD_B1_10
		0,      // 21/A7  AD_B1_11
		13,     // 22/A8  AD_B1_08
		14,     // 23/A9  AD_B1_09
		255,    // 24/A10 AD_B0_12 - only on ADC1, 1 - can't use for audio
		255,    // 25/A11 AD_B0_13 - only on ADC1, 2 - can't use for audio
		255,    // 26/A12 AD_B1_14 - only on ADC2, do not use analogRead()
		255,    // 27/A13 AD_B1_15 - only on ADC2, do not use analogRead()
		255,    // 28
		255,    // 29
		255,    // 30
		255,    // 31
		255,    // 32
		255,    // 33
		255,    // 34
		255,    // 35
		255,    // 36
		255,    // 37
		255,    // 38/A14 AD_B1_12 - only on ADC2, do not use analogRead()
		255,    // 39/A15 AD_B1_13 - only on ADC2, do not use analogRead()
		9,      // 40/A16 AD_B1_04
		10,     // 41/A17 AD_B1_05
	};
/*unsigned long uiTestTime1, uiTestTime2;
uint16_t vMin = 32767, vMax = 0;
uiTestTime1 = micros();
int iMax = 8192;
for (int i=0; i<iMax; i++)
{*/
  // Lecture ADC 1
  ADC1_HC0 = adcT4_pin_to_channel[iPin];                            // Select pin and start conversion
  while (!(ADC1_HS & ADC_HS_COCO0));                                // Wait end conversion
  value = ADC1_R0;
/*if (value > vMax)
  vMax = value;
if (value < vMin)
  vMin = value;
}*/
//Serial.printf("CModeGeneric::ADC0Read(%d) ch %d, value %d\n", iPin, adcT4_pin_to_channel[iPin], value);
/*uiTestTime2 = micros();
uiTestTime1 = uiTestTime2 - uiTestTime1;
double dT = (double)uiTestTime1 / (double)iMax;
double dF = 1.0 / (dT / 1000.0);
Serial.printf("Temps %d boucles %dµs, 1 boucle %fµs F %f kHz, min %d, max %d, écart %d\n", iMax, uiTestTime1, dT, dF, vMin, vMax, vMax-vMin);*/
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  // Teensy to ARM Pin Conversion Chart
  // ADC0, pas de restriction - ATTENTION, ADC1, seulement A2 et A3
  //                                A0  A1  A2  A3  A4  A5  A6  A7  A8  A9
  const uint8_t TsyADCtoARMpin[] = { 5, 14,  8,  9, 13, 12,  6,  7, 15,  4};
  const uint8_t TMuxSel[]        = { 1,  1,  0,  0,  1,  1,  1,  0,  0,  0};
  ADC0_CFG2 = ADC_CFG2_ADLSTS(0);         // Select channels ADxxxa and Default longest sample time; 20 extra ADCK cycles; 24 ADCK cycles total
  if (TMuxSel[iPin] == 0)
    ADC0_CFG2 |= ADC_CFG2_MUXSEL;         // ADxxB channels are selected (See table 39.1.3.1)
  // Selecting the Teensy pin and launching the conversion
  ADC0_SC1A = TsyADCtoARMpin[iPin];       // Channel selection, DIFF=0, Single-ended mode, Disable interrupts
  // Waiting end of conversion
  while ((ADC0_SC1A & ADC_SC1_COCO) == 0) ;
  // Reading the conversion
  value = ADC0_RA;
#endif
  return value;
}

//-------------------------------------------------------------------------
// Mesure des batteries
// iMode Mode de mesure si MAX17043
// Retourne false si les batteries sont trop faibles
bool CModeGeneric::CheckBatteries(
  int iMode
  )
{
  //Serial.printf("CModeGeneric::CheckBatteries iMode %d\n", iMode);
  bool bReturn = false;
  bool bOK = false;
  uint16_t uiVal;
  bool bADCInit = false;
  short TbPourcent[] = {100,  90,   80,   70,   60,   50,   40,   30,   20,   10,   5,    0};
  // Lecture de la tension batterie interne
  if (bADCBat)
  {
    // Présence d'un MCP3221, lecture de la tension
    fNivBatLiPo = ADCBat.getVoltage();
    fNivBatLiPo += commonParams.dCorrectBat;
    //Serial.printf("CModeGeneric::CheckBatteries MCP3221 %fV, cor %fV\n", fNivBatLiPo, commonParams.dCorrectBat);
  }
  else if (max17043.isOK())
  {
    // Présence d'un MAX17043, lecture après passage en mode Quick Start
    //Serial.println("CModeGeneric::CheckBatteries MAX17043");
    if (iMode <= MAX_POWER)
    {
      //Serial.println("max17043.setPowerOn");
      max17043.setPowerOn();
      max17043.setWakeUp();
      bMAXPowerOn = true;
    }
    if (iMode == MAX_WAITMESURE)
      delay(125);
    if (iMode == MAX_WAITMESURE or iMode == MAX_MESURE)
    {
      int iNb = 0;
      while(!bOK)
      {
        iNb++;
        fNivBatLiPo = max17043.readVoltage();
        max17043.setSleep();
        //Serial.printf("max17043.readVoltage fNivBatLiPo %fV, dCorrectBat %fV\n", fNivBatLiPo, commonParams.dCorrectBat);
        if (fNivBatLiPo < 3.0)
        {
          //Serial.printf("*** Lecture %d MAX HS, %2.2fV, %d%%\n", iNb, fNivBatLiPo, iNivBatLiPo);
          if (iNb == 0 and iMode == MAX_WAITMESURE)
          {
            // La première lecture donne une tension erronée, on refait une seconde lecture
            max17043.setSleep();
            delay(50);
            max17043.setPowerOn();
            max17043.setWakeUp();
            delay(200);
            iNb++;
          }
          else
          {
            bReturn = false;
            bOK = true;
          }
        }
        else
          // Mesure OK
          bOK = true;
      }
      fNivBatLiPo += commonParams.dCorrectBat;
      bMAXPowerOn = false;
      //Serial.printf("CheckBatteries avec MAX17043, %2.2fV, %d%% Corection %f\n", fNivBatLiPo, iNivBatLiPo, commonParams.dCorrectBat);
    }
    if (iMode == MAX_POWER)
      // Mesure sur le prochain appel
      return true;
    else
      // Passe le max en mode sommeil
      max17043.setSleep();
  }
  else
  {
    //Serial.println("CModeGeneric::CheckBatteries via ADC interne");
    // Initialisation de l'ADC0 avec la référence 1V2
    ADC0Init( false);
    bADCInit = true;
    // Lecture via le pont de résistance et l'ADC interne
    if (GetRecorderType() >= PASSIVE_STEREO)
      uiVal = ADC0Read( PINA0_LIPO);
    else
      uiVal = ADC0Read( PINA2_LIPO);
#if defined(__IMXRT1062__) // Teensy 4.1
    // Calcul de la tension réelle (3.3/ 4096 * Diviseur pont de résistances (100k + 27k) / 27k)
    // Sur Teensy 4.1, pas de référence interne, c'est forcément le 3.3V qui sert de référence
    // donc problème pour les faibles tensions lorsque les batteries seront plus faibles que 3.3V !
    // Du fait de l'utilisation d'un pont de résistance non adapté et de la chute de tension dans les diodes, on ajoute 0.6V
    // La tension ne baisse plus sous 3.7V et le CPU stoppe sous 3V
    // Test si pont de résistance spécifique T4.1
    fNivBatLiPo = ((double)uiVal * 0.0037896) + commonParams.dCorrectBat;
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
    // Calcul de la tension réelle (1.2 / 4096 * Diviseur pont de résistances (100k + 27k) / 27k)
    // Utilisation de la référence interne sinon la mesure n'est pas bonne avec la référence VCC (on n'utilise pas comme référence ce qu'on veut mesurer !)
    // Du fait de la chute de tension dans les diodes, on ajoute 0.2V
    // Le CPU stoppe sous 2.6V
    fNivBatLiPo = ((double)uiVal * 0.0013780) + commonParams.dCorrectBat;
#endif
  }
  // Tableaux de charge par rapport à la tension d'une batterie LiPo
  // Source: Courbe de décharge d'une batterie Panasonic NRC18650B 3500mAh à 0.2C
  //                    100%  90%   80%   70%   60%   50%   40%   30%   20%   10%   5%    <1%
  //                    4,2   4.0   3,85  3,8   3,7   3.6   3.52  3.45  3.40  3.20  3.00  <2.90
  float TbVLiPo   [] = {4.18, 4.0,  3.85, 3.80, 3.70, 3.60, 3.52, 3.45, 3.40, 3.20, 3.00, 2.9};
  iNivBatLiPo = 0;
  for (int i=0; i<12; i++)
  {
    if (fNivBatLiPo >= TbVLiPo[i])
    {
      iNivBatLiPo = TbPourcent[i];
      break;
    }
  }
    //iNivBatLiPo = uiVal;
  //  Serial.printf("CheckBatteries, mesure LiPo %fV, %d%%\n", fNivBatLiPo, iNivBatLiPo);

  if (!bADCInit)
    ADC0Init( false);
  // Lecture de la tension batterie externe
  uiVal = ADC0Read( PINA1_EXT);
#if defined(__IMXRT1062__) // Teensy 4.1 
  // Calcul de la tension réelle (1.2 / 4096 * Diviseur pont de résistance (100k + 5.6k) / 5.6k)
  // Sur Teensy 4.1, pas de référence interne, c'est forcément le 3.3V qui sert de référence
  fNivBatExt = (double)uiVal * 0.0151925;
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
  // Calcul de la tension réelle (1.2 / 4096 * Diviseur pont de résistance (100k + 5.6k) / 5.6k)
  // Utilisation de la référence interne sinon la mesure n'est pas bonne avec la référence VCC (on n'utilise pas comme référence ce qu'on veut mesurer !)
  fNivBatExt = ((double)uiVal * 0.0055246) + commonParams.dCorrectExt;
#endif
  // Tableaux de charge par rapport à la tension d'une batterie plomb externe
  // 100%  90%   80%   70%   60%   50%  40%   30%   20%   10%   1%   <1%
  // 14    13,66 13,32 12,98 12,64 12,3 11,94 11,58 11,22 10,86 10,5 <10.5
  double TbVPlomb [] = {14, 13.66, 13.32, 12.98, 12.64, 12.3, 11.94, 11.58, 11.22, 10.86, 10.5, 0};
  iNivBatExt = 0;
  for (int i=0; i<12; i++)
  {
    if (fNivBatExt >= TbVPlomb[i])
    {
      iNivBatExt = TbPourcent[i];
      break;
    }
  }
  //Serial.printf("fNivBatLiPo %f, fNivBatExt %f\n", fNivBatLiPo, fNivBatExt);
  // Retourne true uniquement si une batterie est à plus de 10% de charge
  if (iNivBatLiPo > 10 or iNivBatExt > 10)
    bReturn = true;
//Serial.println("CheckBatteries OK");
  return bReturn;
}

//-------------------------------------------------------------------------
//! \brief Print Ram usage
//! \param pTxt Sting to print
bool CModeGeneric::PrintRamUsage(
  const char *pTxt
  )
{
  bool bOK = true;
#if defined(__MK66FX1M0__) // Teensy 3.6
  // Print ram usage (for Teensy 3.6), if used heap is not identical after a same mode, delete is missing....
  //uint32_t TotalRam  = 262144;
  uint32_t TotalRam    = ramMonitor.total();
  uint32_t FreeRam     = ramMonitor.free();
  uint32_t TotalHeap   = ramMonitor.heap_total();
  uint32_t HeapUsed    = ramMonitor.heap_used();
  uint32_t HeapFree    = ramMonitor.heap_free();
  uint32_t StackUsed   = ramMonitor.stack_used();
  uint32_t FreeRamP    = FreeRam * 100 / TotalRam;
  int32_t  unallocated = ramMonitor.unallocated();
  bool bMemo = logPR.GetbConsole();
  logPR.SetbConsole( true);
  bOK = LogFile::AddLog( LLOG, "%s TOTAL RAM %d, FREE RAM %d (%d%%), HEAP total %d, used %d, free %d, STACK used %d, unallocated %d",
    pTxt, TotalRam, FreeRam, FreeRamP, TotalHeap, HeapUsed, HeapFree, StackUsed, unallocated);
  logPR.SetbConsole( bMemo);
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  /*size_t total, totalUser, free;
  int blocks;
  sm_malloc_stats_pool(&extmem_smalloc_pool, &total, &totalUser, &free, &blocks);

  Serial.printf(
      "Total used:       %u bytes\n"
      "Total user vars:  %u bytes\n"
      "Free space:       %.4f MB\n"
      "Allocated blocks: %u\n\n",
      total, totalUser, free / 1024 / 1024.0f, blocks);*/
#endif
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief Retourne true si la sonde de température est présente
bool CModeGeneric::IsTempSensor()
{
  if (!bTestSonde)
  {
    // Test présence sonde de température
    bTestSonde = true;
    bSonde = GestSensor.Begin();
  }
  return bSonde;
}

//-------------------------------------------------------------------------
// Lecture du capteur de température
void CModeGeneric::LectureTemperature()
{
  // Mémorisation des anciennes valeurs
  float fTemp     = cTemp;
  float fHumidity = humidity;
  if (bSonde)
  {
    humidity = GestSensor.ReadHumidity();
    cTemp    = GestSensor.ReadTemperature();
  }

  // Test si valeurs OK
  if (cTemp < -50.0 or cTemp > 80.0)
    // On garde l'ancienne valeur
    cTemp = fTemp;
  if (humidity <= 0.0 or humidity > 100.0)
    // On garde l'ancienne valeur
    humidity = fHumidity;

  // Output data to serial monitor
  //if (CModeGeneric::bDebug)
  //  Serial.printf(" Temperature %f C, Humidity %f %%\n", cTemp, humidity);
}

//-------------------------------------------------------------------------
// Mémorisation de la température en début d'enregistrement d'un fichier wav
void CModeGeneric::MemoTemperature()
{
  // Création du nom du fichier csv au standard Prefixe_AAAAMMJJ.csv
  char sFileName[40];
  char sLine[60];
  char sTemp[60];
  FsFile dataFile;  // Fichier de sauvegarde des infos
  sprintf( sFileName, "%s%s_THLog.csv", paramsOperateur.sPrefixe, sSerialName);
  // Préparation de la ligne
  if (cTemp > -50.0 and cTemp < 80.0)
    // Mémorisation de la date, de l'heure et de la température
    sprintf(sLine, "%02d/%02d/%04d\t%02d:%02d:%02d\t%+04.1f", day(), month(), year(), hour(), minute(), second(), cTemp);
  else
    // Pas de mesure disponible
    sprintf(sLine, "%02d/%02d/%04d\t%02d:%02d:%02d\t ", day(), month(), year(), hour(), minute(), second());
  if (humidity > 5.0 and humidity <= 100.0)
    // Mémorisation de l'humidité
    sprintf(sTemp, "\t%d", (int)humidity);
  else
    // Pas de mesure disponible
    sprintf(sTemp, "\t ");
  strcat(sLine , sTemp);
  strcat(sLine , "\n");
  //Serial.print( sLine);

  // Test si le fichier existe
  bool bEntete = true;
  if (sd.exists(sFileName))
    bEntete = false;
  // Ouverture du fichier (création s'il n'existe pas)
  dataFile = sd.open(sFileName, FILE_WRITE);
  if (! dataFile)
  {
    CGenericMode::uiSDError = sd.card()->errorCode();
    LogFile::AddLog( LLOG, txtLogFileTemp[CModeGeneric::GetCommonsParams()->iLanguage], sFileName);
  }
  else
  {
    if (bEntete)
      // Création de la 1ère ligne d'entête
      //dataFile.println("Date\tHour\tTemperature\tHumidity\thPa");
      dataFile.println("Date\tHour\tTemperature\tHumidity");
    // Mémorisation de la mesure
    dataFile.printf(sLine);
    // Vide les buffers et ferme le fichier
    dataFile.flush();
    dataFile.close();
  }
}

//-------------------------------------------------------------------------
//! \brief Export profiles on SD in Profiles.ini file
void CModeGeneric::ExportProfiles()
{
  SdFile dataFile;
  if (dataFile.open( "Profiles.ini", O_CREAT | O_WRITE))
  {
    // Introductory comments
    dataFile.printf( "; Teensy Recorder profiles file, version 0.8\n");
    dataFile.printf( "; The order of the lines is essential, do not add, delete or move the lines\n");
    dataFile.printf( "; For each parameter, the comment line above specifies the valid values\n");
    dataFile.printf( "; For a numeric parameter, the limits and the step are specified\n");
    dataFile.printf( "; For a string parameter, character are between "" and the maximum number of characters is indicated. Any additional characters will be truncated\n");
    dataFile.printf( "; For listed type parameters, valid strings are indicated between "". Any other value will imply the default value\n");
    dataFile.printf( "; Attention, profile 1 is always in beginner mode, whatever the value of UserLevel\n");
    dataFile.printf( " \n");
    // Creation of the common parameters section
    dataFile.printf( "[Common]\n");
    dataFile.printf( "; User level (\"Beginner\", \"Expert\", \"Debug\")\n");
    dataFile.printf( "UserLevel=\"%s\"\n", sULValues[commonParams.iUserLevel]);
    // Memorization of the selected profile
    int iMemoProfile = commonParams.iSelProfile;
    dataFile.printf( "; Selected profile (0 to 4, step 1)\n");
    dataFile.printf( "SelectedProfile=%d\n", iMemoProfile);
    dataFile.printf( " \n");
    bReadCommon = false;
    // For each profile
    for (int i=0; i<MAXPROFILES; i++)
    {
      // Sélection et lecture du profil
      commonParams.iSelProfile = i;
      StaticReadParams();
      // Creation of the section
      dataFile.printf( "[Profile_%01d]\n", i+1);
      // Storage of profile parameters in the ini file
      dataFile.printf( ";P%01d Profile name (maximum 11 char)\n", i+1, i+1);
      dataFile.printf( "ProfileName=\"%s\"\n", commonParams.ProfilesNames[i]);
      dataFile.printf( ";P%01d Operating mode (\"Auto record\", \"Walkin. Protoc.\", \"Road Protocol\", \"Fixed P. Proto.\", \"RhinoLogger\", \"Heterodyne\", \"Timed recording\", \"Audio Rec.\" or \"Synchro\")\n", i+1);
      dataFile.printf( "OpMode=\"%s\"\n", sOMValues[paramsOperateur.iModeRecord]);
      dataFile.printf( ";P%01d Auto recording start time (hh:mm)\n", i+1);
      dataFile.printf( "StartTime=\"%s\"\n",     paramsOperateur.sHDebut);
      dataFile.printf( ";P%01d Auto recording end time (hh:mm)\n", i+1);
      dataFile.printf( "EndTime=\"%s\"\n",       paramsOperateur.sHFin);
      dataFile.printf( ";P%01d Recording time in Timed Recording mode in second (1 to 3600 step 10)\n", i+1);
      dataFile.printf( "RecTime=%d\n",           paramsOperateur.uiTRECRecord);
      dataFile.printf( ";P%01d Waiting time in Timed Recording mode (1 to 3600 step 10)\n", i+1);
      dataFile.printf( "WaitTime=%d\n",          paramsOperateur.uiTRECWait);
      dataFile.printf( ";P%01d Minimum frequency of interest in Hz (100 to 150000 step 100) in ultrasound (sample frequency >= 192kHz)\n", i+1);
      dataFile.printf( "MinFreqUS=%d\n",         paramsOperateur.uiFreqMin);
      dataFile.printf( ";P%01d Maximum frequency of interest in Hz (100 to 150000 step 100) in ultrasound (sample frequency >= 192kHz)\n", i+1);
      dataFile.printf( "MaxFreqUS=%d\n",         paramsOperateur.uiFreqMax);
      dataFile.printf( ";P%01d Minimum frequency of interest in Hz (100 to 96000 step 100) in audio (sample frequency < 192kHz)\n", i+1);
      dataFile.printf( "MinFreqA=%d\n",          paramsOperateur.uiFreqMinA);
      dataFile.printf( ";P%01d Maximum frequency of interest in Hz (100 to 96000 step 100) in audio (sample frequency < 192kHz)\n", i+1);
      dataFile.printf( "MaxFreqA=%d\n",          paramsOperateur.uiFreqMaxA);
      dataFile.printf( ";P%01d Minimum duration in seconds (1 to 99 step 1)\n", i+1);
      dataFile.printf( "MinDuration=%d\n",       paramsOperateur.uiDurMin);
      dataFile.printf( ";P%01d Maximum duration in seconds (1 to 999 step 1)\n", i+1);
      dataFile.printf( "MaxDuration=%d\n",       paramsOperateur.uiDurMax);
      dataFile.printf( ";P%01d Threshold type (0=relative, 1=absolute)\n", i+1);
      dataFile.printf( "ThresholdType=%d\n",       paramsOperateur.bTypeSeuil);
      dataFile.printf( ";P%01d Relative detection threshold in dB (5 to 99 step 1)\n", i+1);
      dataFile.printf( "RelativeThreshold=%d\n", paramsOperateur.iSeuilDet);
      dataFile.printf( ";P%01d Absolute detection threshold in dB (-110 to -30 step 1)\n", i+1);
      dataFile.printf( "AbsoluteThreshold=%d\n",   paramsOperateur.iSeuilAbs);
      dataFile.printf( ";P%01d Number of detections on 8 FFTs to declare an activity (1 to 8 step 1)\n", i+1);
      dataFile.printf( "NbDetect=%d\n",            paramsOperateur.iNbDetect);
      dataFile.printf( ";P%01d Sampling frequency in kHz (\"24\", \"48\", \"96\", \"192\", \"250\", \"384\" or \"500\")\n", i+1);
      dataFile.printf( "SampFreqU=\"%s\"\n", sSFValues[paramsOperateur.uiFe]);
      dataFile.printf( ";P%01d Sampling frequency for audio mode (\"24\", \"48\", \"96\", \"192\")\n", i+1);
      dataFile.printf( "SampFreqA=\"%s\"\n", sSFValues[paramsOperateur.uiFeA]);
      dataFile.printf( ";P%01d Numeric gain in dB (\"0\", \"6\", \"12\", \"18\" or \"24\")\n", i+1);
      dataFile.printf( "NumericGain=\"%s\"\n", sNGValues[paramsOperateur.uiGainNum]);
      dataFile.printf( ";P%01d Low pass filter (SampFr<384) or linear filter (SampFr>=384) (0 for no or 1 for yes)\n", i+1);
      dataFile.printf( "LowpassFilter=%d\n",       paramsOperateur.bFiltre);
      dataFile.printf( ";P%01d High pass filter in kHz (0 to 25 step 1) for SF >= 192kHz\n", i+1);
      dataFile.printf( "HighpassFilter=%d\n",      paramsOperateur.iFreqFiltreHighPass);
      dataFile.printf( ";P%01d High pass filter in kHz (0.0 to 25.0 step 0.1) for SF < 192kHz\n", i+1);
      dataFile.printf( "fHighpassFilter=%f\n",     paramsOperateur.fFreqFiltreHighPass);
      dataFile.printf( ";P%01d Time expansion x10 (0 for no or 1 for yes)\n", i+1);
      dataFile.printf( "Exp10=%d\n",               paramsOperateur.bExp10);
      dataFile.printf( ";P%01d Wav file prefix (5 char max)\n", i+1);
      dataFile.printf( "WavPrefix=\"%s\"\n",       paramsOperateur.sPrefixe);
      dataFile.printf( ";P%01d Period of the temperature/humidity measurement (10 to 3600 seconds step 10)\n", i+1);
      dataFile.printf( "TemperaturePeriod=%d\n",   paramsOperateur.iPeriodTemp);
      dataFile.printf( ";P%01d Record environmental variables continuously (0=no, 1=yes)\n", i+1);
      dataFile.printf( "ContMesTemp=%d\n",         paramsOperateur.bPermanentTempH);
      dataFile.printf( ";P%01d To save noise mesurement (0=no, 1=yes)\n", i+1);
      dataFile.printf( "SaveNoise=%d\n",           paramsOperateur.bSaveNoise);
      dataFile.printf( ";P%01d Batcorder log and Wave file name type (0=no, 1=yes)\n", i+1);
      dataFile.printf( "BatcorderMode=%d\n",       paramsOperateur.bBatcorderLog);
      dataFile.printf( ";P%01d Heterodyne mode (0=manual, 1=auto)\n", i+1);
      dataFile.printf( "HeterodyneMode=%d\n",      paramsOperateur.iAutoHeter);
      dataFile.printf( ";P%01d Heterodyne recording mode (0=manual, 1=auto)\n", i+1);
      dataFile.printf( "AutoRecHeter=%d\n",        paramsOperateur.bAutoRecH);
      dataFile.printf( ";P%01d Refresh period of the heterodyne graph (0.2 to 2.0 step 0.2)\n", i+1);
      dataFile.printf( "RefreshGraphe=%f\n",       paramsOperateur.fRefreshGRH);
      dataFile.printf( ";P%01d Heterodyne signal level (0.1 to 0.9 step 0.1)\n", i+1);
      dataFile.printf( "HeterLevel=%f\n",          paramsOperateur.fHetSignalLevel);
      dataFile.printf( ";P%01d Stereo recording mode (\"Stereo\", \"MonoRight\", \"MonoLeft\")\n", i+1);
      dataFile.printf( "StereoMode=\"%s\"\n",      sSMValues[paramsOperateur.iStereoRecMode]);
      dataFile.printf( ";P%01d Microphone Type (\"SPU0410\", \"ICS40730\", \"FG23329\")\n", i+1);
      dataFile.printf( "MicrophoneType=\"%s\"\n",  sMTValues[paramsOperateur.iMicrophoneType]);
      dataFile.printf( ";P%01d Master or Slave n (0 for Master or 1 to 9 for slave)\n", i+1);
      dataFile.printf( "MasterSlave=%d\n",         paramsOperateur.iMasterSlave);
      dataFile.printf( ";P%01d Audio top frequency (1 to 50kHz step 1)\n", i+1);
      dataFile.printf( "TopAudioFreq=%d\n",        paramsOperateur.iTopAudioFreq);
      dataFile.printf( ";P%01d Top audio duration in samples (256, 512 or 1024)\n", i+1);
      dataFile.printf( "TopDuration=\"%s\"\n",     sTDValues[paramsOperateur.iDurTop]);
      dataFile.printf( ";P%01d Top audio period (0 to 10 step 1)\n", i+1);
      dataFile.printf( "TopPeriod=%d\n",           paramsOperateur.iTopPer);
      dataFile.printf( ";P%01d Using LED in synchro (\"NO\", \"REC\", \"3 REC\")\n", i+1);
      dataFile.printf( "LEDSynchro=%s\n",          sLEDValues[paramsOperateur.iLEDSynchro]);
      dataFile.printf( ";P%01d To keep the display permanently in RhinoLogger mode (0=no, 1=yes)\n", i+1);
      dataFile.printf( "AffRLPerm=%d\n",           paramsOperateur.bRLAffPerm);
      dataFile.printf( ";P%01d Pre-Trigger duration in second for Teensy 4.1 with extended RAM in automatic recording\n", i+1);
      dataFile.printf( "Pre-TriggerAuto=%d\n",     paramsOperateur.iPreTrigDurAuto);
      dataFile.printf( ";P%01d Pre-Trigger duration in second for Teensy 4.1 with extended RAM in heterodyne mode\n", i+1);
      dataFile.printf( "Pre-TriggerHeter=%d\n",    paramsOperateur.iPreTrigDurHeter);
      dataFile.printf( "HeterSelectiveFilter=%d\n",paramsOperateur.iHeterSel);
      dataFile.printf( "HeterAutoPlay=%d\n",       paramsOperateur.bAutoPlay);
      dataFile.printf( "HeterWithGraph=%d\n",      paramsOperateur.bGraphHet);
      dataFile.printf( "HeterAGC=%d\n",            paramsOperateur.bAGCHet);
      for (int b=0; b<MAXBD; b++)
      {
        dataFile.printf( ";P%01d Rhinologger Band %01d name (maximum 3 char)\n", i+1, b+1);
        dataFile.printf( "BandName%01d=\"%s\"\n", b+1, paramsOperateur.batBand[b].sName);
        dataFile.printf( ";P%01d Rhinologger Band %01d type (\"Invalid\", \"FM\", \"FC\" or \"QFC\")\n", i+1, b+1);
        dataFile.printf( "BandType%01d=\"%s\"\n", b+1, sBRValues[paramsOperateur.batBand[b].iType]);
        dataFile.printf( ";P%01d Rhinologger Band %01d Minimum duration in ms (4 to 96ms step 4)\n", i+1, b+1);
        dataFile.printf( "MinDuration%01d=%d\n", b+1, paramsOperateur.batBand[b].iMinDuration);
        dataFile.printf( ";P%01d Rhinologger Band %01d Maximum duration in ms (4 to 96ms step 4)\n", i+1, b+1);
        dataFile.printf( "MaxDuration%01d=%d\n", b+1, paramsOperateur.batBand[b].iMaxDuration);
        dataFile.printf( ";P%01d Rhinologger Band %01d Minimum frequency width in kHz (0.5 to 99, step 0.5)\n", i+1, b+1);
        dataFile.printf( "MinWidth%01d=%f\n", b+1, paramsOperateur.batBand[b].fMinFWidth);
        dataFile.printf( ";P%01d Rhinologger Band %01d Minimum frequency of the band in kHz (8.0 to 125.0, step 0.25)\n", i+1, b+1);
        dataFile.printf( "MinFrequency%01d=%f\n", b+1, paramsOperateur.batBand[b].fMin);
        dataFile.printf( ";P%01d Rhinologger Band %01d Maximum frequency of the band in kHz (8.0 to 125.0, step 0.25)\n", i+1, b+1);
        dataFile.printf( "MaxFrequency%01d=%f\n", b+1, paramsOperateur.batBand[b].fMax);
        dataFile.printf( ";P%01d Rhinologger Band %01d Minimum detections to consider a positive second (1 to 20, step 1)\n", i+1, b+1);
        dataFile.printf( "NbDectection%01d=%d\n", b+1, paramsOperateur.batBand[b].iNbDetections);
      }
      dataFile.printf( " \n");
    }
    // Close file
    //dataFile.flush();
    dataFile.close();
    // Restitution du profil sélectionné
    commonParams.iSelProfile = iMemoProfile;
    StaticReadParams();
    bReadCommon = true;
 }
}

//-------------------------------------------------------------------------
//! \brief Seek to a section of a .ini file
//! \param *f         .ini file
//! \param *pFileName Ini file name
//! \param *pSection  Section name
//! \return true if seek is OK
bool CModeGeneric::SeekToSection(
  SdFile *f, char *pFileName, char *pSection
  )
{
  bool bOK = false;
  char sLine[180];
  // Seek to begin of the file
  f->seekSet( 0);
  while ( f->fgets(sLine, 180) > 0)
  {
    if (strstr( sLine, pSection) == sLine)
    {
      bOK = true;
      //Serial.printf("CModeGeneric::SeekToSection [%s] trouvée [%s]\n", pSection, sLine);
      break;
    }
  }
  if (!bOK)
    LogFile::AddLog( LLOG, txtLogSection[CModeGeneric::GetCommonsParams()->iLanguage], pFileName, pSection);
  return bOK;
}
    
//-------------------------------------------------------------------------
//! \brief Decode an int for a .ini line
//! \param *f         .ini file
//! \param *pFileName Ini file name
//! \param *pSection  Section name
//! \param *pCode     String of the code
//! \param *pInt      integer to initialize
//! \param iMin       minimum value
//! \param iMax       maximum value
//! \param iDefault   Default value
//! \return true if decode is OK
bool CModeGeneric::DecodeInt(
  SdFile *f, char *pFileName, char *pSection, char *sCode, int *pInt, int iMin, int iMax, int iDefault
  )
{
  bool bOK = false;
  char sLine[180];
  //Serial.printf("CModeGeneric::DecodeInt sCode [%s]\n", sCode);
  while ( f->fgets(sLine, 180) > 0)
  {
    if (strstr( sLine, sCode) == sLine)
    {
      //Serial.printf("CModeGeneric::DecodeInt sCode [%s] trouvé, décodage [%s]\n", sCode, &sLine[strlen(sCode)+1]);
      // test=12
      int iTest;
      sscanf( &sLine[strlen(sCode)+1], "%d", &iTest);
      if (iTest >= iMin and iTest <= iMax)
      {
        bOK = true;
        *pInt = iTest;
      }
      else
        *pInt = iDefault;
      break;
    }
    else if (sLine[0] == '[')
      // New section !
      break;
  }
  if (!bOK)
    LogFile::AddLog( LLOG, txtLogDecode[CModeGeneric::GetCommonsParams()->iLanguage], pFileName, sCode);
  if (!bOK)
    SeekToSection( f, pFileName, pSection);
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief Decode a bool in a .ini line
//! \param *f         .ini file
//! \param *pFileName Ini file name
//! \param *pSection  Section name
//! \param *pCode     String of the code
//! \param *pBool     bool to initialize
//! \param bDefault Default value
//! \return true if decode is OK
bool CModeGeneric::DecodeBool(
  SdFile *f, char *pFileName, char *pSection, char *sCode, bool *pBool, bool bDefault
  )
{
  bool bOK = false;
  char sLine[180];
  //Serial.printf("CModeGeneric::DecodeBool sCode [%s]\n", sCode);
  while ( f->fgets(sLine, 180) > 0)
  {
    if (strstr( sLine, sCode) == sLine)
    {
      // test=12
      int iTest;
      sscanf( &sLine[strlen(sCode)+1], "%d", &iTest);
      if (iTest >= 0 and iTest <= 1)
      {
        bOK = true;
        *pBool = iTest;
      }
      else
        *pBool = bDefault;
      break;
    }
    else if (sLine[0] == '[')
      // New section !
      break;
  }
  if (!bOK)
    LogFile::AddLog( LLOG, txtLogDecode[CModeGeneric::GetCommonsParams()->iLanguage], pFileName, sCode);
  if (!bOK)
    SeekToSection( f, pFileName, pSection);
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief Decode a float in a .ini line
//! \param *f         .ini file
//! \param *pFileName Ini file name
//! \param *pSection  Section name
//! \param *pCode     String of the code
//! \param *pFloat    float to initialize
//! \param fMin       minimum value
//! \param fMax       maximum value
//! \param fDefault   Default value
//! \return true if decode is OK
bool CModeGeneric::DecodeFloat(
  SdFile *f, char *pFileName, char *pSection, char *sCode, float *pFloat, float fMin, float fMax, float fDefault
  )
{
  bool bOK = false;
  char sLine[180];
  //Serial.printf("CModeGeneric::DecodeFloat sCode [%s]\n", sCode);
  while ( f->fgets(sLine, 180) > 0)
  {
    if (strstr( sLine, sCode) == sLine)
    {
      // test=12
      float fTest;
      sscanf( &sLine[strlen(sCode)+1], "%f", &fTest);
      if (fTest >= fMin and fTest <= fMax)
      {
        bOK = true;
        *pFloat = fTest;
      }
      else
        *pFloat = fDefault;
      break;
    }
    else if (sLine[0] == '[')
      // New section !
      break;
  }
  if (!bOK)
    LogFile::AddLog( LLOG, txtLogDecode[CModeGeneric::GetCommonsParams()->iLanguage], pFileName, sCode);
  if (!bOK)
    SeekToSection( f, pFileName, pSection);
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief Decode a string in a .ini line
//! \param *f         .ini file
//! \param *pFileName Ini file name
//! \param *pSection  Section name
//! \param *pCode     String of the code
//! \param *pChar     String to initialize
//! \param iSize      maximum char
//! \param *pDefault  Default value
//! \return true if decode is OK
bool CModeGeneric::DecodeString(
  SdFile *f, char *pFileName, char *pSection, char *sCode, char *pChar, int iMax, const char *pDefault
  )
{
  bool bOK = false;
  char sLine[180];
  //Serial.printf("CModeGeneric::DecodeString sCode [%s]\n", sCode);
  while ( f->fgets(sLine, 180) > 0)
  {
    if (strstr( sLine, sCode) == sLine)
    {
      // 0123456
      // test="toto"
      char *p1 = &(sLine[strlen(sCode)+2]);
      char *p2 = strstr( p1, "\"");
      if (p2 != NULL)
        *p2 = 0;
      if ((int)strlen(p1) <= iMax)
      {
        strcpy( pChar, p1);
        bOK = true;
      }
      else
        strcpy( pChar, pDefault);
      break;
    }
    else if (sLine[0] == '[')
      // New section !
      break;
  }
  if (!bOK)
    LogFile::AddLog( LLOG, txtLogDecode[CModeGeneric::GetCommonsParams()->iLanguage], pFileName, sCode);
  if (!bOK)
    SeekToSection( f, pFileName, pSection);
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief Decode enum value in a .ini line
//! \param *f         .ini file
//! \param *pFileName Ini file name
//! \param *pSection  Section name
//! \param *pCode     String of the code
//! \param **pChar    Table of the enum values
//! \param iMax       maximum enum
//! \param *pInt      integer to initialize
//! \param iDef       Default value
//! \return true if decode is OK
bool CModeGeneric::DecodeEnum(
  SdFile *f, char *pFileName, char *pSection, char *sCode, const char **pChar, int iMax, int *pInt, int iDef
  )
{
  bool bOK = false;
  char sLine[180];
  *pInt = iDef;
  //Serial.printf("CModeGeneric::DecodeEnum sCode [%s]\n", sCode);
  while ( f->fgets(sLine, 180) > 0)
  {
    if (strstr( sLine, sCode) == sLine)
    {
      // 0123456
      // test="toto"
      //Serial.printf("CModeGeneric::DecodeEnum sCode [%s] trouvé, iMax %d, décodage [%s]\n", sCode, iMax, &(sLine[strlen(sCode)+2]));
      char *p1 = &(sLine[strlen(sCode)+2]);
      char *p2 = strstr( p1, "\"");
      if (p2 != NULL)
        *p2 = 0;
      if ((int)strlen(p1) > 0 and (int)strlen(p1) < 49)
      {
        for (int i=0; i<iMax; i++)
        {
          if (strstr( p1, pChar[i]) == p1)
          {
            *pInt = i;
            bOK = true;
            break;
          }
        }
      }
      break;
    }
    else if (sLine[0] == '[')
      // New section !
      break;
  }
  if (!bOK)
    LogFile::AddLog( LLOG, txtLogDecode[CModeGeneric::GetCommonsParams()->iLanguage], pFileName, sCode);
  if (!bOK)
    SeekToSection( f, pFileName, pSection);
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief Import profiles from SD in Profiles.ini file
bool CModeGeneric::ImportProfiles(char *pFile)
{
  bool bOK = true;
  int iMemoProfile = 0;
  char sSection[20];
  char sCode[20];
  // Select root SD
  sd.chdir( "/");
  SdFile f;
  if (f.open( pFile, O_READ))
  {
    // Reading the common parameters section
    if (!SeekToSection( &f, pFile, (char *)"[Common]")) bOK = false;
    else
    {
      // User level (Beginner, Expert, Debug)
      if (!DecodeEnum( &f, pFile, (char *)"[Common]", (char *)"UserLevel", sULValues, ULMAX, &commonParams.iUserLevel, ULBEGINNER)) bOK = false;
      // Selected profile (0 to 4, step 1)
      if (!DecodeInt( &f, pFile, (char *)"[Common]", (char *)"SelectedProfile", &iMemoProfile, 0, MAXPROFILES-1, 0)) bOK = false;
      // Pour chaque profil
      for (int i=0; i<MAXPROFILES; i++)
      {
        // Recherche de la section
        sprintf( sSection, "[Profile_%01d]\n", i+1);
        if (!SeekToSection( &f, pFile, sSection)) bOK = false;
        else
        {
          // Profiles names (max 11 chars)
          if (!DecodeString( &f, pFile, sSection, (char *)"ProfileName", commonParams.ProfilesNames[i], MAXCHARPROFILE, txtDefProfilesNames[commonParams.iLanguage][i])) bOK = false;
          // Operating mode (Auto record, Walkin. Protoc., Road Protocol, Fixed P. Proto., RhinoLogger, Microphone test, Heterodyne, Audio Rec., Synchro)
          if (!DecodeEnum( &f, pFile, sSection, (char *)"OpMode", sOMValues, MAXPROT, &paramsOperateur.iModeRecord, RECAUTO)) bOK = false;
          // Auto recording start time (hh:mm)
          if (!DecodeString( &f, pFile, sSection, (char *)"StartTime", paramsOperateur.sHDebut, MAXH, "22:00")) bOK = false;
          // Auto recording end time (hh:mm)
          if (!DecodeString( &f, pFile, sSection, (char *)"EndTime", paramsOperateur.sHFin, MAXH, "06:00")) bOK = false;
          // Recording time in Timed Recording mode in second (1 to 3600 step 10)
          if (!DecodeInt( &f, pFile, sSection, (char *)"RecTime", &paramsOperateur.uiTRECRecord, 1, 3600*12, 1)) bOK = false;
          // Waiting time in Timed Recording mode (1 to 3600 step 10)
          if (!DecodeInt( &f, pFile, sSection, (char *)"WaitTime", &paramsOperateur.uiTRECWait, 1, 3600*24, 1)) bOK = false;
          // Minimum frequency of interest in Hz (100 to 150000 step 100) in ultrasound (sample frequency >= 192kHz)
          if (!DecodeInt( &f, pFile, sSection, (char *)"MinFreqUS", &paramsOperateur.uiFreqMin, 100, 150000, 10000)) bOK = false;
          // Maximum frequency of interest in Hz (100 to 150000 step 100) in ultrasound (sample frequency >= 192kHz)
          if (!DecodeInt( &f, pFile, sSection, (char *)"MaxFreqUS", &paramsOperateur.uiFreqMax, 100, 150000, 120000)) bOK = false;
          // Minimum frequency of interest in Hz (100 to 96000 step 100) in audio (sample frequency < 192kHz)
          if (!DecodeInt( &f, pFile, sSection, (char *)"MinFreqA", &paramsOperateur.uiFreqMinA, 100, 96000, 100)) bOK = false;
          // Maximum frequency of interest in Hz (100 to 96000 step 1) in audio (sample frequency < 192kHz)
          if (!DecodeInt( &f, pFile, sSection, (char *)"MaxFreqA", &paramsOperateur.uiFreqMaxA, 100, 96000, 48000)) bOK = false;
          // Minimum duration in seconds (1 to 10 step 1)
          if (!DecodeInt( &f, pFile, sSection, (char *)"MinDuration", &paramsOperateur.uiDurMin, 1, 99, 1)) bOK = false;
          // Maximum duration in seconds (1 to 999 step 1)
          if (!DecodeInt( &f, pFile, sSection, (char *)"MaxDuration", &paramsOperateur.uiDurMax, 1, 999, 10)) bOK = false;
          // Threshold type (0=relative, 1=absolute)
          if (!DecodeBool( &f, pFile, sSection, (char *)"ThresholdType", &paramsOperateur.bTypeSeuil, false)) bOK = false;
          // Relative detection threshold in dB (5 to 99 step 1)
          if (!DecodeInt( &f, pFile, sSection, (char *)"RelativeThreshold", &paramsOperateur.iSeuilDet, 5, 99, 15)) bOK = false;
          // Absolute detection threshold in dB (-110 to -30 step 1)
          if (!DecodeInt( &f, pFile, sSection, (char *)"AbsoluteThreshold", &paramsOperateur.iSeuilAbs, -110, -30, -80)) bOK = false;
          // Number of detections on 8 FFTs to declare an activity (1 to 8 step 1)
          if (!DecodeInt( &f, pFile, sSection, (char *)"NbDetect", &paramsOperateur.iNbDetect, 1, 8, 3)) bOK = false;
          // Sampling frequency in kHz (48, 96, 192, 250 or 384 or 500)
          if (!DecodeEnum( &f, pFile, sSection, (char *)"SampFreqU", sSFValues, MAXFE, &paramsOperateur.uiFe, FE384KHZ)) bOK = false;
          //Serial.printf("ImportProfiles Profile %d, SampFreqU = %s (%d)\n", i, sSFValues[paramsOperateur.uiFe], paramsOperateur.uiFe);
          // Sampling frequency for audio mode (48, 96, 192, 250 or 384 or 500)
          if (!DecodeEnum( &f, pFile, sSection, (char *)"SampFreqA", sSFValues, MAXFE, &paramsOperateur.uiFeA, FE48KHZ)) bOK = false;
          // Numeric gain in dB (0, 6, 12, 18 or 24)
          if (!DecodeEnum( &f, pFile, sSection, (char *)"NumericGain", sNGValues, MAXGAIN, &paramsOperateur.uiGainNum, GAIN12dB)) bOK = false;
          // Low pass filter (SampFr<384) or linear filter (SampFr>=384) (0 for no or 1 for yes)
          if (!DecodeBool( &f, pFile, sSection, (char *)"LowpassFilter", &paramsOperateur.bFiltre, false)) bOK = false;
          // High pass filter in kHz (0 to 25 step 1) for SF >= 192kHz
          if (!DecodeInt( &f, pFile, sSection, (char *)"HighpassFilter", &paramsOperateur.iFreqFiltreHighPass, 0, 25, 0)) bOK = false;
          // High pass filter in kHz (0.0 to 25.0 step 0.1) for SF < 192kHz
          if (!DecodeFloat( &f, pFile, sSection, (char *)"fHighpassFilter", &paramsOperateur.fFreqFiltreHighPass, 0.0, 25.0, 0.1)) bOK = false;
          // Time expansion x10 (0 for no or 1 for yes)
          if (!DecodeBool( &f, pFile, sSection, (char *)"Exp10", &paramsOperateur.bExp10, true)) bOK = false;
          // Wav file prefix (5 char max)
          if (!DecodeString( &f, pFile, sSection, (char *)"WavPrefix", paramsOperateur.sPrefixe, MAXPREFIXE, "PRec_")) bOK = false;
          // Period of the temperature/humidity measurement (10 to 3600 seconds step 10)
          if (!DecodeInt( &f, pFile, sSection, (char *)"TemperaturePeriod", &paramsOperateur.iPeriodTemp, 10, 3600, 60)) bOK = false;
          // Record environmental variables continuously (0=no, 1=yes)
          if (!DecodeBool( &f, pFile, sSection, (char *)"ContMesTemp", &paramsOperateur.bPermanentTempH, false)) bOK = false;
          // To save noise mesurement (0=no, 1=yes)
          if (!DecodeBool( &f, pFile, sSection, (char *)"SaveNoise", &paramsOperateur.bSaveNoise, false)) bOK = false;
          // Batcorder log and Wave file name type (0=no, 1=yes)
          if (!DecodeBool( &f, pFile, sSection, (char *)"BatcorderMode", &paramsOperateur.bBatcorderLog, false)) bOK = false;
          // Heterodyne mode (0=manual, 1=auto)
          if (!DecodeInt( &f, pFile, sSection, (char *)"HeterodyneMode", (int *)&paramsOperateur.iAutoHeter, (int)HAM_MANUAL, (int)HAM_MAX-1, (int)HAM_MANUAL)) bOK = false;
          // Heterodyne recording mode (0=manual, 1=auto)
          if (!DecodeBool( &f, pFile, sSection, (char *)"AutoRecHeter", &paramsOperateur.bAutoRecH, false)) bOK = false;
          // Refresh period of the heterodyne graph (0.2 to 2.0 step 0.2)
          if (!DecodeFloat( &f, pFile, sSection, (char *)"RefreshGraphe", &paramsOperateur.fRefreshGRH, 0.2, 2.0, 1.0)) bOK = false;
          // Heterodyne signal level (0.1 to 0.9 step 0.1)
          if (!DecodeFloat( &f, pFile, sSection, (char *)"HeterLevel", &paramsOperateur.fHetSignalLevel, 0.1, 0.9, 0.1)) bOK = false;
          // Stereo recording mode (\"Stereo\", \"MonoRight\", \"MonoLeft\")
          DecodeEnum( &f, pFile, sSection, (char *)"StereoMode", sSMValues, SMMAX, &paramsOperateur.iStereoRecMode, SMSTEREO);
          if (CModeGeneric::GetRecorderType() < PASSIVE_STEREO and paramsOperateur.iStereoRecMode == SMSTEREO)
            paramsOperateur.iStereoRecMode = SMRIGHT;    // Mono recording on right chanel
          // Microphone Type (\"SPU0410\", \"ICS40730\", \"FG23329\")
          DecodeEnum( &f, pFile, sSection, (char *)"MicrophoneType", sMTValues, MTMAX, &paramsOperateur.iMicrophoneType, MTICS40730);
          // Master or Slave n (0 for Master or 1 to 9 for slave)
          DecodeInt( &f, pFile, sSection, (char *)"MasterSlave", &paramsOperateur.iMasterSlave, 0, 9, 0);
          // Audio top frequency (1 to 50kHz step 1)
          DecodeInt( &f, pFile, sSection, (char *)"TopAudioFreq", &paramsOperateur.iTopAudioFreq, 1, 50, 2);
          // Top audio duration in samples (256, 512 or 1024
          DecodeEnum( &f, pFile, sSection, (char *)"TopDuration", sTDValues, TSMAX, &paramsOperateur.iDurTop, TS256);
          // Top audio period (0 to 10 step 1)
          DecodeInt( &f, pFile, sSection, (char *)"TopPeriod", &paramsOperateur.iTopPer, 0, 10, 0);
          // Using LED in synchro (\"NO\", \"REC\", \"3 REC\")
          DecodeEnum( &f, pFile, sSection, (char *)"LEDSynchro", sLEDValues, MAXLEDSYNCH, &paramsOperateur.iLEDSynchro, LEDRECSYNCH);
          // To keep the display permanently in RhinoLogger mode (0=no, 1=yes)
          if (!DecodeBool( &f, pFile, sSection, (char *)"AffRLPerm", &paramsOperateur.bRLAffPerm, false)) bOK = false;
          // Pre-Trigger duration in second for Teensy 4.1 with extended RAM in automatic recording
          DecodeInt( &f, pFile, sSection, (char *)"Pre-TriggerAuto", &paramsOperateur.iPreTrigDurAuto, 0, 15, 1);
          // Pre-Trigger duration in second for Teensy 4.1 with extended RAM in heterodyne mode
          DecodeInt( &f, pFile, sSection, (char *)"Pre-TriggerHeter", &paramsOperateur.iPreTrigDurHeter, 1, 15, 1);
          // Heterodyne with selective filter or not (Tennsy 4.1 only)
          DecodeInt( &f, pFile, sSection, (char *)"Pre-HeterSelectiveFilter", (int *)&paramsOperateur.iHeterSel, (int)HSF_NOSEL, (int)HSF_MAX-1, (int)HSF_NOSEL);
          DecodeBool( &f, pFile, sSection, (char *)"HeterAutoPlay", &paramsOperateur.bAutoPlay, false);
          DecodeBool( &f, pFile, sSection, (char *)"HeterWithGraph", &paramsOperateur.bGraphHet, true);
          DecodeBool( &f, pFile, sSection, (char *)"HeterAGC", &paramsOperateur.bAGCHet, true);
          for (int b=0; b<MAXBD; b++)
          {
            // Band name (max 3 chars)
            sprintf( sCode, "BandName%01d", b+1);
            char sName[5];
            strcpy( sName, "No ");
            if (!DecodeString( &f, pFile, sSection, sCode, paramsOperateur.batBand[b].sName, MAXNAMEBD, sName)) bOK = false;
            // Band type (Invalid, FM, FC or QFC)
            sprintf( sCode, "BandType%01d", b+1);
            if (!DecodeEnum( &f, pFile, sSection, sCode, sBRValues, BDTYPEMAX, &paramsOperateur.batBand[b].iType, BDINVALID)) bOK = false;
            // Minimum duration in ms (4 to 96ms step 4)
            sprintf( sCode, "MinDuration%01d", b+1);
            if (!DecodeInt( &f, pFile, sSection, sCode, &paramsOperateur.batBand[b].iMinDuration, 4, 96, 4)) bOK = false;
            // Maximum duration in ms (4 to 96ms step 4)
            sprintf( sCode, "MaxDuration%01d", b+1);
            if (!DecodeInt( &f, pFile, sSection, sCode, &paramsOperateur.batBand[b].iMaxDuration, 4, 96, 16)) bOK = false;
            // Minimum frequency width in kHz (0.5 to 99, step 0.5)
            sprintf( sCode, "MinWidth%01d", b+1);
            if (!DecodeFloat( &f, pFile, sSection, sCode, &paramsOperateur.batBand[b].fMinFWidth, 0.5, 99.0, 1)) bOK = false;
            // Minimum frequency of the band in kHz (8.0 to 125.0, step 0.25)
            sprintf( sCode, "MinFrequency%01d", b+1);
            if (!DecodeFloat( &f, pFile, sSection, sCode, &paramsOperateur.batBand[b].fMin, 8.0, 125.0, 76)) bOK = false;
            // Maximum frequency of the band in kHz (8.0 to 125.0, step 0.25)
            sprintf( sCode, "MaxFrequency%01d", b+1);
            if (!DecodeFloat( &f, pFile, sSection, sCode, &paramsOperateur.batBand[b].fMax, 8.0, 125.0, 86)) bOK = false;
            // Minimum detections to consider a positive second (1 to 20, step 1)
            sprintf( sCode, "NbDectection%01d", b+1);
            if (!DecodeInt( &f, pFile, sSection, sCode, &paramsOperateur.batBand[b].iNbDetections, 1, 20, 10)) bOK = false;
          }
        }
        // Memorisation des paramètres
        commonParams.iSelProfile = i;
//Serial.println("StaticWriteParams on CModeGeneric::ImportProfiles A");
        StaticWriteParams();
      }
    }
    // Close file
    f.close();
    // Restitution de profil sélectionné dans le fichier ini
    commonParams.iSelProfile = iMemoProfile;
    // Récupération des paramètres du profil sélectionné
    bReadCommon = false;
    StaticReadParams();
    bReadCommon = true;
    //Serial.printf("ImportProfiles A iMemoProfile %d, SampFreqU = %s (%d)\n", iMemoProfile, sSFValues[paramsOperateur.uiFe], paramsOperateur.uiFe);
    // Mémorisation du profil sélectionné
//Serial.println("StaticWriteParams on CModeGeneric::ImportProfiles B");
    StaticWriteParams();
    //Serial.printf("ImportProfiles B iMemoProfile %d, SampFreqU = %s (%d)\n", iMemoProfile, sSFValues[paramsOperateur.uiFe], paramsOperateur.uiFe);
  }
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief Read AutoProfiles.ini if exist
//! \return true if time is set
bool CModeGeneric::AutoProfiles()
{
  bool bOK = false;
  SdFile f;
  // Select root SD
  sd.chdir( "/");

  if (sd.exists("AutoProfiles.ini"))
    // Read ini files
    bOK = ImportProfiles((char *)"AutoProfiles.ini");
  if (bOK)
  {
    // Set old name to ini file
    sd.rename( "AutoProfiles.ini", "OldProfiles.ini");
    LogFile::AddLog( LLOG, txtLogAutoProfiles[CModeGeneric::GetCommonsParams()->iLanguage]);
  }
  return bOK;
}

//-------------------------------------------------------------------------
//! \brief Read AutoTime.ini if exist
//! \return true if time is set
bool CModeGeneric::AutoTime()
{
  bool bOK = false;
  int iDay, iMonth, iYear, iHour, iMinute, iSecond;
  SdFile f;
  // Select root SD
  sd.chdir( "/");
  if (sd.exists("AutoTime.ini") and f.open( "AutoTime.ini", O_READ))
  {
    bOK = true;
    // Reading the section
    if (SeekToSection( &f, (char *)"AutoTime.ini", (char *)"[AutoTime]"))
    {
      if (bOK and !DecodeInt( &f, (char *)"AutoTime.ini", (char *)"[AutoTime]", (char *)"Day",    &iDay,     1,   31, 1)) bOK = false;
      if (bOK and !DecodeInt( &f, (char *)"AutoTime.ini", (char *)"[AutoTime]", (char *)"Month",  &iMonth,   1,   12, 1)) bOK = false;
      if (bOK and !DecodeInt( &f, (char *)"AutoTime.ini", (char *)"[AutoTime]", (char *)"Year",   &iYear, 2020, 4000, 1)) bOK = false;
      if (bOK and !DecodeInt( &f, (char *)"AutoTime.ini", (char *)"[AutoTime]", (char *)"Hour",   &iHour,    0,   23, 1)) bOK = false;
      if (bOK and !DecodeInt( &f, (char *)"AutoTime.ini", (char *)"[AutoTime]", (char *)"Minute", &iMinute,  0,   59, 1)) bOK = false;
      if (bOK and !DecodeInt( &f, (char *)"AutoTime.ini", (char *)"[AutoTime]", (char *)"Second", &iSecond,  0,   59, 1)) bOK = false;
      if (bOK)
      {
        // Update to the system date
        setTime( iHour, iMinute, iSecond, iDay, iMonth, iYear);
        // Set internal Teensy 3.6 clock
        Teensy3Clock.set(now());
        LogFile::AddLog( LLOG, txtLogAutoTime[CModeGeneric::GetCommonsParams()->iLanguage], iDay, iMonth, iYear, iHour, iMinute, iSecond);
      }
    }
    // Close file
    f.close();
  }
  if (bOK)
    // Set old name to ini file
    sd.rename( "AutoTime.ini", "OldTime.ini");
  return bOK;
}


// Indique un démarrage en mode debug (touche Up pressée)
bool CModeGeneric::bDebug = false;

//! Paramètres communs aux différents profils
CommonParams CModeGeneric::commonParams;
  
// Paramètres opérateurs
ParamsOperator CModeGeneric::paramsOperateur;

// Tension de la batterie LiPo interne en V
double CModeGeneric::fNivBatLiPo;
// Tension de la batterie externe en V
double CModeGeneric::fNivBatExt;
// Niveau de charge de la batterie LiPo interne en %
int    CModeGeneric::iNivBatLiPo;
// Niveau de charge de la batterie externe en %
int    CModeGeneric::iNivBatExt;

// Nombre de Minutes de début et de fin d'acquisition/enregistrement par rapport à minuit
int CModeGeneric::iDebMinutes = -1;
int CModeGeneric::iFinMinutes = -1;

// Recorder type
int CModeGeneric::iRecorderType = PASSIVE_RECORDER;

// Indique que le teste de présence de la sonde a été effectué
bool CModeGeneric::bTestSonde = false;
// Indique la présence de la sonde de température
bool CModeGeneric::bSonde = false;
//! Gestionnaire de lecture de la sonde
CTHSensor CModeGeneric::GestSensor;
//! Gestionnaire d'un éventuel MAX17043 pour la tension batterie
MAX17043 CModeGeneric::max17043;
//! Indique que le MAX est power on
bool CModeGeneric::bMAXPowerOn = false;
//! Indique que le module MAX est en erreur
bool CModeGeneric::bMAXError = false;
//! Indique qu'un MCP3221 est présent pour la meure de la tension batterie
bool CModeGeneric::bADCBat = false;
//! Gestionnaire d'un éventuel MCP3221 pour la mesure de la tension batterie
CADCVoltage CModeGeneric::ADCBat;
//! Pour lire ou non les paramètres communs
bool CModeGeneric::bReadCommon = true;
#ifdef WITHBLUETOOTH
//! Indique si le Bluetooth est actif ou non
bool CModeGeneric::bBluetooth = false;
#endif
// Mot de controle des versions de paramètres
uint16_t CModeGeneric::uiCtrlVersion=0;

/*-----------------------------------------------------------------------------
 Classe de gestion du mode veille
 Passe en veille après 15s et, sur fin de veille, repasse en mode enregistrement
-----------------------------------------------------------------------------*/

//-------------------------------------------------------------------------
// Constructeur
CModeVeille::CModeVeille()
{
  // Init du temps d'affichage avant de passer en veille
  iDecompteurAttVeille = TIMEATTENTEAFF;
}

//-------------------------------------------------------------------------
// Début du mode
void CModeVeille::BeginMode()
{
  if (CModeGeneric::bDebug)
    Serial.println("CModeVeille::BeginMode");
  // Appel de la méthode parente
  CModeGeneric::BeginMode();
  // Init de la seconde courante à 61 pour forcer le réaffichage
  iCurrentSecond = 61;
  // Init du temps d'affichage avant de passer en veille
  iDecompteurAttVeille = TIMEATTENTEAFF;
#if defined(__IMXRT1062__) // Teensy 4.1
#ifdef SNOOZE_WITH_RESET
  if (iWhoWakeUp == PINPUSH or iWhoWakeUp == PINPUSH_ART41)
    LogFile::AddLog( LLOG, "CModeVeille::BeginMode Wake up by PINPUSH");
  else if (iWhoWakeUp == ALARM_WAKE)
    LogFile::AddLog( LLOG, "CModeVeille::BeginMode Wake up by ALARM");
  else if (iWhoWakeUp == UNKNOW_WAKE)
    LogFile::AddLog( LLOG, "CModeVeille::BeginMode Unknow Wake up !");
  if (iWhoWakeUp == ALARM_WAKE or iWhoWakeUp == UNKNOW_WAKE)
    // Pour ces cas, on passe en veille sans attendre
    iDecompteurAttVeille = 0;
#endif
#endif
}

//-------------------------------------------------------------------------
// Affichage du mode sur l'écran
// Cette méthode est appelée régulièrement par le loop principal
// à charge des différents modes d'afficher les informations nécessaires
void CModeVeille::PrintMode()
{
  // Affichage toutes les secondes
  if (iCurrentSecond != second())
  {
    iCurrentSecond = second();
    char Message[25];
    // Mode attente veille pendant 15s
    pDisplay->clearBuffer();
    pDisplay->setCursor(0,0);
    pDisplay->println(txtVeilleA[commonParams.iLanguage]);
    sprintf( Message, txtVeilleB[commonParams.iLanguage], iDecompteurAttVeille);
    pDisplay->setCursor(0,9);
    pDisplay->println( Message);
    sprintf( Message, txtVeilleC[commonParams.iLanguage], paramsOperateur.sHDebut);
    pDisplay->setCursor(0,18);
    pDisplay->println( Message);
    sprintf( Message, "%02d:%02d:%02d", hour(), minute(), second());
    pDisplay->setCursor(0,27);
    pDisplay->println( Message);
    pDisplay->setCursor(0,36);
    pDisplay->println( txtVeilleD[commonParams.iLanguage]);
    pDisplay->setCursor(0,45);
    pDisplay->println( txtVeilleE[commonParams.iLanguage]);
    pDisplay->sendBuffer();
    // Mode Attente Veille pendant 15s
    iDecompteurAttVeille--;
    if (iDecompteurAttVeille <= 0)
      // Passage en mode veille
      MiseEnVeille();
  }
}

//-------------------------------------------------------------------------
// Traitement des ordres claviers
// Si la touche est une touche de changement de mode, retourne le mode demandé
// Cette méthode est appelée régulièrement par le loop principal
// à charge des différents modes de traiter les actions opérateurs
int CModeVeille::KeyManager(
  unsigned short key  // Touche sollicitée par l'opérateur
  )
{
  int iReturn = iNewMode;
  if (key != K_NO)
  {
    if (GetRecorderType() == ACTIVE_RECORDER)
      // In Active Recorder change mode : Record mode -> Player -> Parameters -> Record mode
      iNewMode = MPLAYER;
    else
      // Sur toutes les touches en mode veille, on passe en mode paramètres
      // (possible que lors des 15 1ère secondes ou après un réveil)
      iReturn = MPARAMS;
  }
  return iReturn;
}

//-------------------------------------------------------------------------
// Reset du logiciel sur timer si les fonctions SD au réveil durent trop longtemps
void RestartCPU()
{
  // Reset CPU
  SCB_AIRCR = 0x05FA0004;
  while (1) ;
}

//-------------------------------------------------------------------------
// Mise en veille du processeur. Réveil à l'heure demandée ou sur clic
void CModeVeille::MiseEnVeille()
{
  // Mode veille, effacement écran
  pDisplay->clearDisplay();
  pDisplay->display();
  // Mesure des batteries
  CModeGeneric::CheckBatteries(MAX_WAITMESURE);
  // Calcul de la durée de la veille
  int iCurrentNbMinutes=0, iAttenteMinutes=0, iAttenteSecondes=0;
  uint8_t iH, iM, iS;
  // Clignottement pour informer l'opérateur
  if (commonParams.iUtilLed != NOLED)
  {
    for (int i = 0; i < 4; i++)
    {
        digitalWriteFast(LED_BUILTIN, HIGH);
        delay(200);
        digitalWriteFast(LED_BUILTIN, LOW);
        delay(200);
    }
  }
  if (GetRecorderType() != ACTIVE_RECORDER)
    LogFile::AddLog( LLOG, txtLogStandbyPR[CModeGeneric::GetCommonsParams()->iLanguage], paramsOperateur.sHDebut, fNivBatLiPo, iNivBatLiPo, fNivBatExt, iNivBatExt);
  else      
    LogFile::AddLog( LLOG, txtLogStandbyAR[CModeGeneric::GetCommonsParams()->iLanguage], paramsOperateur.sHDebut, fNivBatLiPo, iNivBatLiPo);
  
  // Boucle de veille pour la prise de température en veille
  bool bSonde = IsTempSensor();
  while (true)
  {
    // Init du bouton de sortie de veille
#if defined(__IMXRT1062__) // Teensy 4.1
    if (GetRecorderType() == ACTIVE_RECORDER)
      digital.pinMode(PINPUSH_ART41 , INPUT_PULLUP, RISING); // Push
    else if (GetRecorderType() >= PASSIVE_STEREO)
      digital.pinMode(PINPUSH_PRST41, INPUT_PULLUP, RISING); // Push
    else
      digital.pinMode(PINPUSH       , INPUT_PULLUP, RISING); // Push
#else
    digital.pinMode(PINPUSH         , INPUT_PULLUP, RISING); // Push
#endif
    //digital.pinMode(PINDOWN, INPUT_PULLUP, RISING); // Down
    // Calcul du nombre de minutes par rapport à minuit pour l'heure courante
    iCurrentNbMinutes = hour() * 60 + minute();
    iAttenteMinutes = 0;
    if (iCurrentNbMinutes < iDebMinutes)
      // Cas ou l'heure courante est plus petite que l'heure de début
      iAttenteMinutes = iDebMinutes - iCurrentNbMinutes;
    else
      // Cas ou l'heure courante et de début sont de chaque coté de minuit
      iAttenteMinutes = iDebMinutes + 24*60 - iCurrentNbMinutes;
    iH = iAttenteMinutes / 60;
    iM = iAttenteMinutes - iH*60;
    iS = second();
    iAttenteSecondes = iH*3600 + iM*60 - iS;
#if defined(__MK66FX1M0__) // Teensy 3.6
    if (bSonde and paramsOperateur.bPermanentTempH)
    {
      iAttenteSecondes = iH*3600 + iM*60 - iS;
      //LogFile::AddLog( LLOG, "Mesures T en veille, iAttenteSecondes %d, iPeriodTemp %d", iAttenteSecondes, paramsOperateur.iPeriodTemp);
      if (iAttenteSecondes > paramsOperateur.iPeriodTemp)
      {
        // Période de mesure de la température et humidité
        iAttenteSecondes = paramsOperateur.iPeriodTemp;
        iH = iAttenteSecondes / 3600;
        iM = (iAttenteSecondes - iH*3600) / 60;
        iS = iAttenteSecondes - iH*3600 - iM*60;
        //LogFile::AddLog( LLOG, "iH %d, iM %d, iS %d", iH, iM, iS);
      }
    }
#endif
    //LogFile::AddLog( LLOG, "Veille pendant %d H, %d Mn, %d S, %02d:%02d:%02d", iH, iM, iS, hour(), minute(), second());
    //delay(200);
    // ATTENTION, ne pas mettre l'heure du réveil mais le temps en H/Mn/secondes avant le réveil
    alarm.setRtcTimer( iH, iM, iS);
    //LogFile::AddLog( LLOG, "Réveil à %02d:%02d:%02d", hour(), minute(), second());
    // =====     Et veille    =====
    int who = 0;
#if defined(__IMXRT1062__) // Teensy 4.1
  iS = iAttenteSecondes;
#ifdef SNOOZE_WITH_RESET
    // For Teensy 4.1, modified version of Snooze make a CPU restart with who make wake up in SRC_GPR5
    Snooze.hibernate( configTeensy);
    // End of sleep mode by Reset CPU
    while (1) ;
#else
    // For Teensy 4.1, modified version of Snooze but work only less than 10. After, you need a reset CPU
    who = Snooze.deepSleep( configTeensy);
#endif
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
    who = Snooze.hibernate( configTeensy);
#endif
    delay(200);
    Serial.begin(115200);
    delay(200);
    Serial.println("*** Sortie de veille");
    // ===== Sortie de veille =====
    // Initialise l'heure de la libairie Time avec la RTC du Teensy 3.0
    setSyncProvider(getTeensy3Time);
    // Initialisation de la carte SD
    if (!isSDInit())
    {
      Serial.println("Erreur SD en sortie de veille !");
      // Reset du logiciel
      RestartCPU();
    }
    else
      sd.vol()->freeClusterCount();
    // Test si sortie via bouton
    if (who == PINPUSH or iWhoWakeUp == PINPUSH_ART41)
      break;
    // Test si prise de température
    if (!bSonde or paramsOperateur.bPermanentTempH == false)
      break;
    else
    {
      // Prise de la température et de l'humidité et mémo
      //LogFile::AddLog( LLOG, "Lecture et mémo température");
      LectureTemperature();
      MemoTemperature();
      // Test s'il fin de veille
      if (IsTimeVeille() == false)
        break;
    }
  }
  // Fin de veille  
  if (commonParams.iUtilLed != NOLED)
  {
    for (int i = 0; i < 4; i++)
    {
        digitalWriteFast(LED_BUILTIN, HIGH);
        delay(200);
        digitalWriteFast(LED_BUILTIN, LOW);
        delay(200);
    }
  }
  // Information de l'opérateur, l'opération de relecture de la carte SD est longue pour les cartes > 16Go
  pDisplay->clearBuffer();
  pDisplay->setCursor(0,0);
  pDisplay->println(txtLogEndSleep[commonParams.iLanguage]);
  pDisplay->setCursor(0,10);
  pDisplay->println(txtLogSDWait[commonParams.iLanguage]);
  pDisplay->sendBuffer();
  iDecompteurAttVeille = TIMEATTENTEAFF;
  bool bOK = true;
  if (GetRecorderType() != ACTIVE_RECORDER)
    bOK = LogFile::AddLog( LLOG, txtLogSWakeupPR[CModeGeneric::GetCommonsParams()->iLanguage], fNivBatLiPo, iNivBatLiPo, fNivBatExt, iNivBatExt);
  else
    bOK = LogFile::AddLog( LLOG, txtLogSWakeupAR[CModeGeneric::GetCommonsParams()->iLanguage], fNivBatLiPo, iNivBatLiPo);
  // Réinitialisation des interruptions des bouttons
  CKeyManager::GetKeyManager()->SetInterrupt();
  // En fonction du mode
  if (!bOK)
    isSDInit();
  else if (paramsOperateur.iModeRecord == PROTFIXE)
    // Passage en mode enregistrement point fixe
    iNewMode = MPROPFX;
  else if (paramsOperateur.iModeRecord == TIMEDREC)
    // Passage en mode enregistrement cadencé
    iNewMode = MTRECORD;
  else if (paramsOperateur.iModeRecord == PRHINOLOG)
    // Passage en mode RhinoLogger
    iNewMode = MRHINOL;
  else if (paramsOperateur.iModeRecord == SYNCHRO)
    // Passage en mode Stéréo synchro
    iNewMode = MSYNCHRO;
  else
    // Passage en mode enregistrement auto
    iNewMode = MRECORD;
}

//-------------------------------------------------------------------------
//! For Teensy 4.1, indicate who wake up
int16_t CModeVeille::iWhoWakeUp = 0;

//-------------------------------------------------------------------------
// Classe de test de la carte SD

//                                 12345678901234567890
const char formIDSD[]          =  "                & ";
const char formIDXFORMAT[]     =  "               &$$$ ";
const char formIDXSDRET[]      =  "[&$$]";
extern const char *txtIDSD[];
extern const char *txtIDXFORMAT[];
extern const char *txtFYesNo[MAXLANGUAGE][5];
extern const char *txtIDXSDRET[];
extern const char *txtIDXSDTST[];
#define IDXTSTSD    0
#define IDXFORMAT   1
#define IDXRAZSD    2
#define IDXSDRETURN 3

//-------------------------------------------------------------------------
//! \brief Constructeur
CModeTestSD::CModeTestSD()
{
  bRAZ = false;
  iFormat = 0;
  // IDXTSTSD          Test carte SD
  LstModifParams.push_back(new CPushButtonModifier ( formIDXSDRET, false, txtIDXSDTST,  0, 27));
  // IDXFORMAT         Formatage carte SD (O/N)
  LstModifParams.push_back(new CEnumModifier       ( formIDXFORMAT, false, false, txtIDXFORMAT, (int *)&iFormat, 5, (const char **)txtFYesNo, 0, 36));
  // IDXRAZSD          RAZ carte SD (O/N)
  LstModifParams.push_back(new CSDModifier         ( formIDSD    , false, txtIDSD, "*", 0, 45));
  // IDXSDRETURN       Retour au mode paramètres
  LstModifParams.push_back(new CPushButtonModifier ( formIDXSDRET, false, txtIDXSDRET,  0, 54));
  ((CEnumModifier *)LstModifParams[IDXFORMAT])->SetElemValidity( 2 , false);
  ((CEnumModifier *)LstModifParams[IDXFORMAT])->SetElemValidity( 3 , false);
  ((CEnumModifier *)LstModifParams[IDXFORMAT])->SetElemValidity( 4 , false);
  pInstance = this;
}

//-------------------------------------------------------------------------
//! \brief Début du mode
void CModeTestSD::BeginMode()
{
  // Appel de la méthode parente
  CModeGeneric::BeginMode();
  LstModifParams[IDXRAZSD]->SetbCalcul( true);
  // Test de la carte SD
  TestSD();
}

//-------------------------------------------------------------------------
// To display information outside of modifiers
// Test SD screen
//   12345678901234567890  y
// 1 SD 128GO FAT32        0
// 2 Open  123ms  . .      9
// 3 Write  52ms   |       18
// 4 Close 100ms  ---      27
// 5                       36
// 6 Free 22.5GO 99%       45
// 7 Retour parametres     54
// X 000000000000000001111
//   001123344566778990012
//   062840628406284062840
void CModeTestSD::AddPrint()
{
  char txt[30];
  int y = 0;
  pDisplay->setCursor(0,y);
  sprintf( txt, "Open  %03d ms", (int)uiOpen);
  pDisplay->println(txt);
  y += HLINEPIX;
  pDisplay->setCursor(0,y);
  if (uiWrite > 1000)
    sprintf( txt, "Write %03d ms", (int)uiWrite/1000);
  else
    sprintf( txt, "Write %03d us", (int)uiWrite);
  pDisplay->println(txt);
  y += HLINEPIX;
  pDisplay->setCursor(0,y);
  sprintf( txt, "Close %03d ms", (int)uiClose);
  pDisplay->println(txt);
  sprintf( sSizeSD, txtIDXSDTST[commonParams.iLanguage], (int)iSizeSD, (int)sd.vol()->fatType());
  ((CPushButtonModifier *)LstModifParams[IDXTSTSD])->SetString( sSizeSD);
  // 64GO 12 4 8 ms   32GO 351 5 16 ms
  // Fonte emoticon
  pDisplay->setFont(u8g2_font_emoticons21_tr);
  txt[0] = 39;     // :-((
  txt[1] = 0;
  if (uiOpen <= 30 and uiWrite < 10000 and uiClose < 20)
    txt[0] = 33;   // :-))
  else if (uiOpen <= 60 and uiWrite <= 15000 and uiClose <= 50)
    txt[0] = 36;   // :-||
  pDisplay->setCursor(80,2);
  pDisplay->println(txt);
  // Restitution de la fonte normale
  pDisplay->setFont(u8g2_font_6x10_mf);
}

//-------------------------------------------------------------------------
//! \brief Affichage pendant le formatage
void CModeTestSD::AffFormatting()
{
  iFormat ++;
  if (iFormat > 4)
    iFormat = 2;
  pInstance->PrintMode();
}

//-------------------------------------------------------------------------
// Called on end of change of a modifier
// If a mode change, returns the requested mode
int CModeTestSD::OnEndChange(
  int idxModifier // Index of affected modifier
  )
{
  if (idxModifier == IDXSDRETURN)
    // Retour vers le mode paramètres
    iNewMode = MPARAMS;
  else if (idxModifier == IDXTSTSD)
    // Nouveau test SD
    TestSD();
  else if (idxModifier == IDXFORMAT)
  {
    // Init du modificateur pour afficher "..."
    ((CEnumModifier *)LstModifParams[IDXFORMAT])->SetElemValidity( 2 , true);
    ((CEnumModifier *)LstModifParams[IDXFORMAT])->SetElemValidity( 3 , true);
    ((CEnumModifier *)LstModifParams[IDXFORMAT])->SetElemValidity( 4 , true);
    iFormat = 2;
    timerAff.begin( AffFormatting, 400000);
    PrintMode();
    // Formate la carte SD
    if (!FormattingSD())
    {
      if (CModeGeneric::GetCommonsParams()->iLanguage == LFRENCH)
        CModeError::SetTxtError((char *)"Erreur forma. SD!");
      else
        CModeError::SetTxtError((char *)"Error format SD!");
      iNewMode = MERROR;
    }
    else if (!isSDInit())
    {
      pDisplay->clearBuffer();
      pDisplay->setCursor(0,0);
      pDisplay->println(txtRestFormating[commonParams.iLanguage]);
      pDisplay->sendBuffer();
      delay(800);
      // Need to restart SPU after formating
      RestartCPU();
    }
    else
      LstModifParams[IDXRAZSD]->SetbCalcul( true);
    timerAff.end();
    // Init du modificateur pour afficher " No"
    ((CEnumModifier *)LstModifParams[IDXFORMAT])->SetElemValidity( 2 , false);
    ((CEnumModifier *)LstModifParams[IDXFORMAT])->SetElemValidity( 3 , false);
    ((CEnumModifier *)LstModifParams[IDXFORMAT])->SetElemValidity( 4 , false);
    iFormat = 0;
  }
  return iNewMode;
}

//-------------------------------------------------------------------------
// Test des performances de la carte SD
void CModeTestSD::TestSD()
{
  // Calculate total space (volume free clusters * blocks per clusters / 2)
  iSizeSD  = sd.vol()->clusterCount();
  iSizeSD *= sd.vol()->sectorsPerCluster()/2;
  iSizeSD /= 1000000;
  //Serial.printf("TestSD iSizeSD %d\n", iSizeSD);
  // Size adjustment to 2/4/8/16/32/64/128/256/512GO standard sizes.
  if (iSizeSD <= 2)
    iSizeSD = 2;
  else if (iSizeSD <= 4)
    iSizeSD = 4;
  else if (iSizeSD <= 8)
    iSizeSD = 8;
  else if (iSizeSD <= 16)
    iSizeSD = 16;
  else if (iSizeSD <= 32)
    iSizeSD = 32;
  else if (iSizeSD <= 64)
    iSizeSD = 64;
  else if (iSizeSD <= 128)
    iSizeSD = 128;
  else if (iSizeSD <= 256)
    iSizeSD = 256;
  else
    iSizeSD = 512;
  //Serial.printf("Ajustement iSizeSD %d\n", iSizeSD);
  // Buffer de test *** Attention, sur T4.1, nécessité d'utiliser un new de façon que le buffer soit dans la RAM2 plus "lente" que RAM1
  int16_t *pSamples = new int16_t[MAXSAMPLE];
  int16_t val = -2048;
  for (int i=0; i<MAXSAMPLE; i++)
  {
    pSamples[i] = val;
    val++;
    if (val > 2047)
      val = -2048;
  }
  // Test vitesse
  bool bOK = true;
  CWaveFile wave;
  unsigned long uiTimeA = micros();
  wave.OpenWaveFileForWrite( 384000, "Test.wav", true);
  unsigned long uiTimeB = micros();
  for (int i=0; i<10; i++)
  {
    if (wave.WavfileWrite( pSamples, MAXSAMPLE) == 0)
      bOK = false;
  }
  unsigned long uiTimeC = micros();
  wave.CloseWavfile();
  unsigned long uiTimeD = micros();
  delete pSamples;
  uiOpen  = (uiTimeB - uiTimeA) / 1000;
  if (bOK)
    uiWrite = ((uiTimeC-uiTimeB)/10);
  else
    uiWrite = 999;
  uiClose = (uiTimeD - uiTimeC) / 1000;
  sd.remove("Test.wav");
}

//-------------------------------------------------------------------------
//! \brief Formatage de la carte SD
bool CModeTestSD::FormattingSD()
{
  return sd.format();
}

//! Indicateur de formatage de la carte SD
int CModeTestSD::iFormat = 0;

//! Pointeur sur l'instance
CModeGeneric *CModeTestSD::pInstance = NULL;
