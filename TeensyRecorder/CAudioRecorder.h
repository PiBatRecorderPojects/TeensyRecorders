//-------------------------------------------------------------------------
//! \file CAudioRecorder.h
//! \brief Classe de gestion du mode Enregistreur Audio
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/* 
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "ModesModifiers.h"
#include "CModeRecorder.h"

#ifndef MODEAUDIOREC
#define MODEAUDIOREC

//! \enum IDXAPARAMS
//! \brief Index of Audio Recorder mode parameters
enum IDXAPARAMS {
  IDXARECORD, //!< Record status
  IDXATIMER , //!< Record time
  IDXASEUILR, //!< Relative detection threshold in dB (5-99)
  IDXAFME   , //!< Detected frequency of maximum energy
  IDXANIVA  , //!< Maximum level detected in numbers
  IDAHBAT   , //!< Internal battery charge level in %
  IDXAHOUR  , //!< Hour
  IDXBARG   , //!< Barregraph for max level
  IDXMAXAREC
};

//-------------------------------------------------------------------------
//! \class CGrapheXkHz
//! \brief Management class of a graph from 0 to X kHz on n lines
//! 24 pixels high, 14 pixels for the scale and n pixels for the graph
class CGrapheXkHz
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  //! \param ife Sample frequency (48, 96 or 192 kHz)
  //! \param iNLines Nb lines
  //! \param y Position y from the top of the graph
  CGrapheXkHz(
    int ife,
    int iNLines,
    int y       
    );

  //-------------------------------------------------------------------------
  //! \brief Destructor
  ~CGrapheXkHz();

  //-------------------------------------------------------------------------
  //! \brief Set the sample frequency
  //! \param ife Sample frequency (48, 96 or 192 kHz)
  void SetFe(
    int ife
    );
  
  //-------------------------------------------------------------------------
  //! \brief Set the min max frequencies of the scale
  //! \param iFMin Min frequency in Hz
  //! \param iFMax Max frequency in Hz
  void SetMinMaxScale(
    int iFMin,
    int iFMax 
    );
  
  //-------------------------------------------------------------------------
  //! \brief Initialization of the scrolling time of the graph (1s by default)
  //! \param iScroll Time between two scrolls in ms
  void SetTimeScroll(
    int iScroll
    );
  
  //-------------------------------------------------------------------------
  //! \brief Initialization of a point in the graph
  //! \param iIdx Frequency index
  void SetFrequence(
    int iIdx
    );
  
  //-------------------------------------------------------------------------
  //! \brief Print graph
  void PrintGraphe();
  
protected:
  //! Position y from the top of the graph
  uint8_t iY;
  
  //! Time between two scrolls in ms
  unsigned long uiTimeScroll;

  //! Next scroll time
  unsigned long uiNextScroll;
  
  //! Bitmap for the graph 128 x n points is 16 x n bytes
  uint8_t *pTbGraphe;

  //! Nb lines
  int iNbLines;

  //! Channel width in kHz
  float fChannel;

  //! Scale in kHz
  float fScale;

  //! Min and Max graph frequency in kHz
  float fMinFR, fMaxFr;

  //! Pixel width in kHz
  float fPixel;

  //! Sample frequency (48, 96 or 192 kHz)
  int iFe;

  //! Min scale frequencies in kHz
  int iFrMin;

  //! Max scale frequencies in kHz
  int iFrMax;

  //! Size of graphe in bytes
  int iSizeGraph;
};

//-------------------------------------------------------------------------
//! \class CAudioRecorder
//! \brief Audio Recorder mode management class
class CAudioRecorder: public CModeGenericRec
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructor (initialization of parameters to default values)
  CAudioRecorder();

  //-------------------------------------------------------------------------
  //! \brief Creation du gestionnaire d'enregistrement
  virtual void CreateRecorder();
  
  //-------------------------------------------------------------------------
  //! \brief Beginning of the mode
  virtual void BeginMode();
  
  //-------------------------------------------------------------------------
  //! \brief End of the mode
  virtual void EndMode();

  //-------------------------------------------------------------------------
  //! \brief  Reading saved settings
  //! \return true if OK
  virtual bool ReadParams();
  
  //-------------------------------------------------------------------------
  //! \brief To display information outside of modifiers
  virtual void AddPrint();
  
  //-------------------------------------------------------------------------
  //! \brief Keyboard order processing
  //! If the key is a mode change key, returns the requested mode
  //! This method is called regularly by the main loop
  //! By default, handles modifiers
  //! \param key Touch to treat
  virtual int KeyManager(
    unsigned short key
    );

protected:
  //! Maximum energie frequency in kHz
  float fFME;

  //! Maximum level in dB of FME in dB
  int iMaxLevel;
  //! Maximum level in dB
  int iMemoMaxLevel;

  //! Record status (PBREAK or PREC)
  int iRecordMode;

  //! Recording time in seconds
  int iRecordTime;
  
  //! Frequency graph
  CGrapheXkHz graphe;

  //! FME holding time
  unsigned long uiFMEHoldingTime;
  
  //! Time to check battery
  unsigned long uiTimeCheck;

  //! Width of a FFT channel in Hz
  uint32_t uiFFTChannel;

  //! Counter for visibility of thresold
  uint8_t iCptTh;

  //! Current second
  uint8_t iCurrentSecond;

  //! Max level
  int iNivMaxBG;
  
  //! Minimum interest frequency in kHz
  float fMinIntFreq;

  //! Maximum interest frequency in kHz
  float fMaxIntFreq;

  //! Pour init du Bluetooth
  int uiBLETime;

  // Indique que le seuil est en cours de modification
  bool bModifSeuil;
};
#endif // MODEHETERODYNE
