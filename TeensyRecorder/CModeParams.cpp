/* 
 * File:   CModeParams.cpp
   PassiveRecorder Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <U8g2lib.h>
#include <algorithm>
#include "Const.h"
#include "TimeLib.h"
#include "CModeParams.h"
#include "CSynchroLink.h"

 /*
  * Classes :
  * - CModeParams
  * - CModeModifProfiles
  * - CModeLoadProfiles
  * - CModeCopyright
  */

// Screen manager
extern U8G2 *pDisplay;

// Gestionnaire de carte SD
extern SdFs sd;

#if defined(__IMXRT1062__) // Teensy 4.1
// Taille de l'éventuelle RAM supplémentaire
extern "C" uint8_t external_psram_size;
#endif

// Chaines de booléen type Oui/Non
extern const char *txtOuiNon[MAXLANGUAGE][2];
extern const char *txtYesNo[MAXLANGUAGE][2];
extern const char *txtPPPNon[MAXLANGUAGE][2];
// Chaines de l'énuméré des fréquences d'échantillonnage
const char *txtFreqEch[MAXLANGUAGE][7] = {{" 24", " 48", " 96", "192", "250", "384", "500"},
                                          {" 24", " 48", " 96", "192", "250", "384", "500"},
                                          {" 24", " 48", " 96", "192", "250", "384", "500"},
                                          {" 24", " 48", " 96", "192", "250", "384", "500"}};
// Chaines de l'énuméré des gains numériques
const char *txtGainNum[MAXLANGUAGE][5] = {{" 0", " 6", "12", "18", "24"},
                                          {" 0", " 6", "12", "18", "24"},
                                          {" 0", " 6", "12", "18", "24"},
                                          {" 0", " 6", "12", "18", "24"}};
// Chaines de l'énuméré des modes de fonctionnement
extern const char *txtProtocoles[MAXLANGUAGE][10];
// Chaines de l'énuméré des cas d'utilisation de la LED
extern const char *txtLED[MAXLANGUAGE][8];
// Chaines des types stéréo
extern const char *txtStereoMode[MAXLANGUAGE][3];
/* Audio Top duration in ms
Fe (kHz) 256   512   1024   2048  4096 – Nb Ech
192      1,33  2,67  5,33  10,67  21,33
250      1,02  2,05  4,10   8,19  16,38
384      0,67  1,33  2,67   5,33  10,67
*/
// Chaines des durées du top audio
const char *txtDurTop192[MAXLANGUAGE][5] = {{" 1.3ms", " 2.7ms", " 5.3ms", "10.7ms", "21.3ms"},
                                            {" 1.3ms", " 2.7ms", " 5.3ms", "10.7ms", "21.3ms"},
                                            {" 1.3ms", " 2.7ms", " 5.3ms", "10.7ms", "21.3ms"},
                                            {" 1.3ms", " 2.7ms", " 5.3ms", "10.7ms", "21.3ms"}};
const char *txtDurTop250[MAXLANGUAGE][5] = {{" 1.0ms", " 2.0ms", " 4.1ms", " 8.2ms", "16.4ms"},
                                            {" 1.0ms", " 2.0ms", " 4.1ms", " 8.2ms", "16.4ms"},
                                            {" 1.0ms", " 2.0ms", " 4.1ms", " 8.2ms", "16.4ms"},
                                            {" 1.0ms", " 2.0ms", " 4.1ms", " 8.2ms", "16.4ms"}};
const char *txtDurTop384[MAXLANGUAGE][5] = {{" 0.7ms", " 1.3ms", " 2.7ms", " 5.3ms", "10.7ms"},
                                            {" 0.7ms", " 1.3ms", " 2.7ms", " 5.3ms", "10.7ms"},
                                            {" 0.7ms", " 1.3ms", " 2.7ms", " 5.3ms", "10.7ms"},
                                            {" 0.7ms", " 1.3ms", " 2.7ms", " 5.3ms", "10.7ms"}};
// Chaines des Master ou Slaves
extern const char *txtMasterSlave[MAXLANGUAGE][10];
// Chaines des types des micros
const char *txtMicType[MAXLANGUAGE][3] = {{"SPU0410 ", "ICS40730", "FG23329 "},
                                          {"SPU0410 ", "ICS40730", "FG23329 "},
                                          {"SPU0410 ", "ICS40730", "FG23329 "},
                                          {"SPU0410 ", "ICS40730", "FG23329 "}};
// Chaines de l'énuméré des langues
extern const char *txtLanguage[MAXLANGUAGE][2];
// Chaines de l'énuméré des niveaux utilisateurs
extern const char *txtLevelUser[MAXLANGUAGE][3];
// Chaines du boolen du type de seuil
extern const char *txtModeSeuil[MAXLANGUAGE][2];
// Strings for heterodyne recording mode
extern const char *txtModeHetRec[MAXLANGUAGE][2];
// Chaines de l'énuméré des utilisations de la LED en mode synchro
extern const char *txtLEDSynch[MAXLANGUAGE][3];
// Chaines des formats des paramètres
const char formIDXMODER[]      =  " &$$ ";
extern const char *txtIDXMODER[];
const char formIDXMODEF[]      =  "      &$$$$$$$$$$$$$$";
extern const char *txtIDXMODEF[];
const char formIDXULEV[]       =  "            &$$$$$$$";
extern const char *txtIDXULEV[];
const char formIDXBAT[]        =  " &$ ";
extern const char *txtIDXBAT[];
const char formIDXEXT[]        =  " &$ ";
extern const char *txtIDXEXT[];
//                                  Del      9999wav %c
//                                  Effacem. 9999wav %c
const char formIDXSD[]         =  "                  & ";
extern const char *txtIDXSD[];
const char formIDXLUM[]        =  "              &$$ ";
extern const char *txtIDXLUM[];
const char formIDXBLUT[]       =  "              &$$ ";
extern const char *txtIDXBLUT[];
const char formIDXINITBL[]     =  "                 &$$ ";
extern const char *txtIDXINITBL[];
const char formIDXDATE[]       =  "       &$/&$/&$ ";
extern const char *txtIDXDATE[];
const char formIDXHEURE[]      =  "       &$:&$:&$ ";
extern const char *txtIDXHEURE[];
extern const char *txtIDXPRETRIG[];
const char formIDXPRETRIG[]    =  " Pre-Trigger   &$ ";
const char formIDXTSEUIL[]     =  " Threshold: &$$$$$$$";
extern const char *txtIDXTSEUIL[];
const char formIDXSEUIL[]      =  "                &$ ";
extern const char *txtIDXSEUILR[];
extern const char *txtIDXSEUILA[];
const char formIDXTHRECM[]     =  " Het. Record &$$$$$";
extern const char *txtIDXTHRECM[];
const char formIDXHGRAPH[]     =  " Het. Graphe &$$";
extern const char *txtIDXHGRAPH[];
const char formIDXHDEB[]       =  "             &$:&$ ";
extern const char *txtIDXHDEB[];
const char formIDXHFIN[]       =  "             &$:&$ ";
extern const char *txtIDXHFIN[];
const char formIDXFMIN[]       =  "               &$$ ";
extern const char *txtIDXFMIN[];
const char formIDXFMAX[]       =  "               &$$ ";
extern const char *txtIDXFMAX[];
const char formIDXFMINA[]      =  "              &$$$";
extern const char *txtIDXFMINA[];
const char formIDXFMAXA[]      =  "              &$$$";
extern const char *txtIDXFMAXA[];
const char formIDXDMIN[]       =  "               &$ ";
extern const char *txtIDXDMIN[];
const char formIDXDMAX[]       =  "              &$$ ";
extern const char *txtIDXDMAX[];
const char formIDXFECH[]       =  "               &$$ ";
extern const char *txtIDXFECH[];
const char formIDXGNUM[]       =  "                 &$ ";
extern const char *txtIDXGNUM[];
const char formIDXFILT[]       =  "                 &$$";
extern const char *txtIDXFILTL[];
extern const char *txtIDXFILT192[];
extern const char *txtIDXFILT96[];
extern const char *txtIDXFILT48[];
extern const char *txtIDXFILT24[];
const char formIDXHPFILT[]     =  "                &$ ";
extern const char *txtIDXHPFILT[];
//                                   High p.filt. 22.1kHz
const char formIDXFHPFILT[]     =  "              &$$$ ";
extern const char *txtIDXFHPFILT[];
const char formIDXEXP[]        =  "                  &$$";
extern const char *txtIDXEXP[];
const char formIDXPREF[]       =  "              &&&&& ";
extern const char *txtIDXPREF[];
const char formIDXNDET[]       =  "                & ";
extern const char *txtIDXNDET[];
const char formIDXLANG[]       =  "           &$$$$$$$ ";
extern const char *txtIDXLANG[];
const char formIDXLED[]        =  "           &$$$$$$$$";
extern const char *txtIDXLED[];
const char formIDXDEF[]        =  "                  &$$";
extern const char *txtIDXDEF[];
const char formIDXBCOR[]       =  "                &$$";
extern const char *txtIDXBCOR[];
extern const char *txtLogStartPar[];
extern const char *txtLogSDPWait[];
const char formIDXPTEM[]       =  "             &$$$ ";
extern const char *txtIDXPTEM[];
const char formIDXBDRL[]       =  " &$$$ ";
extern const char *txtIDXBDRL[];
const char formIDXTSTSD[]      = " ";
extern const char *txtIDXTSTSD[];
extern const char *txtIDXPROFILE[];
const char formIDXTHV[]        =  "                    &";
extern const char *txtIDXTHV[];
const char formIDXSNOISE[]     =  "               &$$";
extern const char *txtIDXSNOISE[];
const char formIDXRLPERM[]     =  "                  & ";
extern const char *txtIDXRLPERM[];
const char formIDXBOOT[]       =  "                 &$$";
extern const char *txtIDXBOOT[];
const char formIDXRAFGRAPH[]   =  "                &$$ ";
extern const char *txtIDXRAFGRAPH[];
const char formIDXLEVELH[]     =  "                &$$ ";
extern const char *txtIDXLEVELH[];
                                 // Profile RhinoLogger
const char formIDXPROFIL[]     =  "         &$$$$$$$$$$ ";
extern const char *txtIDXPROFIL[];
extern const char *txtIDXRECON[];
extern const char *txtIDXRECOFF[];
//                                 012345678901234567890
//                                  Enregis. Mono Gauche
const char formIDXSMODE[]      =  "          &$$$$$$$$$$";
extern const char *txtIDXSMODE[];
const char formIDXCOPYR[]      =  " &$$ ";
extern const char *txtIDXCOPYR[];
//                                 012345678901234567890
//                                  Type mic. ICS40730
const char formIDXMICT[]       =  "           &$$$$$$$";
extern const char *txtIDXMICT[];
const char formIDXMSLAVE[]     =  " &$$$$$$$$";
extern const char *txtIDXMSLAVE[];
const char formIDXHSELF[]      =  "               &$$$$$";
extern const char *txtIDXHSELF[];
extern const char *txtHSYesNo[MAXLANGUAGE][4];
const char formIDXHAUTOMAN[]      =  "           &$$$$$$$$$";
extern const char *txtIDXHAUTOMAN[];
extern const char *txtHMANAUTO[MAXLANGUAGE][4];
const char formIDXFRTOP[]      =  "               &$ ";
extern const char *txtIDXFRTOP[];
const char formIDXTOPDUR[]     =  "            &$$$$$";
extern const char *txtIDXTOPDUR[];
const char formIDXTOPPER[]     =  "               &$ ";
extern const char *txtIDXTOPPER[];
const char formIDXLEDS[]       =  "           &$$$$$$$";
extern const char *txtIDXLEDS[];
const char formIDXAUTOPLAY[]   =  "                 &$$";
extern const char *txtIDXAUTOPLAY[];
const char formIDXHETAGC[]   =  "             &$$";
extern const char *txtIDXHETAGC[];
const char formIDXTEM[]        =  " &$ ";
extern const char *txtIDXTEMP[];
//                                " Correc. Batterie %s"
//                                " Mesure batterie %3.1V"
const char formIDXCORBATX[]    =  "                  &";
const char formIDXCORBAT[]     =  "                 &$$ ";
extern const char *txtIDXCORBAT[];
extern const char *txtIDXCORBATX[];
//                                " Mesure externe %04.1V"
const char formIDXCOREXTX[]    =  "                  &";
const char formIDXCOREXT[]     =  "                &$$$ ";
extern const char *txtIDXCOREXT[];
extern const char *txtIDXCOREXTX[];

/*-----------------------------------------------------------------------------
 Classe de gestion du mode paramètres
 Affiche les différents paramètres et permets de les modifier
-----------------------------------------------------------------------------*/
//-------------------------------------------------------------------------
// Constructeur
#ifdef USBSDCARD
CModeParams::CModeParams(): mtpd(&storage)
#else
CModeParams::CModeParams()
#endif
{
  // Initialize profiles pointers
  for (int i=0; i<MAXPROFILES; i++)
  {
    pStrProfiles[LENGLISH][i] = commonParams.ProfilesNames[i];
    pStrProfiles[LFRENCH ][i] = commonParams.ProfilesNames[i];
  }
  // Création des lignes de modificateurs des paramètres
  //                                                                                0123456789012345678901
  // IDXMODER     Passage en mode enregistrement                                     Mode enregistrement
  LstModifParams.push_back(new CPushButtonModifier ( formIDXMODER , true, txtIDXMODER  , 0, 0));
  // IDXPROFILE,  Selected profile
  LstModifParams.push_back(new CEnumModifier ( formIDXPROFIL, true, false, txtIDXPROFIL, (int *)&commonParams.iSelProfile, MAXPROFILES, (const char **)pStrProfiles, 0, 0));
  //                                                                              0123456789012345678901
  // IDXMODEF   Mode de fonctionnement                                             Mode Test micro
  LstModifParams.push_back(new CEnumModifier ( formIDXMODEF , true, false, txtIDXMODEF  , (int *)&paramsOperateur.iModeRecord, MAXPROT, (const char **)txtProtocoles, 0, 0));
  //                                                                              0123456789012345678901
  // IDXSYNCHRO Master or slave n                                                  Master
  LstModifParams.push_back(new CEnumModifier ( formIDXMSLAVE, true, false, txtIDXMSLAVE, (int *)&paramsOperateur.iMasterSlave, MAXMS, (const char **)txtMasterSlave, 0, 0));
  //                                                                                0123456789012345678901
  // IDXBAT       Niveau de charge batterie interne en % et V                        Batt. Int. 80% 4V2
  LstModifParams.push_back(new CPushButtonModifier ( formIDXBAT   , true, txtIDXBAT    , 0, 0));
  //                                                                                0123456789012345678901
  // IDXEXT       Niveau de charge batterie externe en % et V                        Batt. Ext. 80% 12V0
  LstModifParams.push_back(new CPushButtonModifier ( formIDXEXT   , true, txtIDXEXT    , 0, 0));
  //                                                                                0123456789012345678901
  // IDXSD        Taille libre carte SD                                              SD 128GO 99% 9999wav
  //                                                                                 Effacem. 9999 wav O
  LstModifParams.push_back(new CSDModifier   ( formIDXSD    , true, txtIDXSD, "wav", 0, 0));
  // IDXTEMP      Temperature and humidity probe reading
  //                                                                                0123456789012345678901
  LstModifParams.push_back(new CPushButtonModifier ( formIDXBAT   , true, txtIDXTEMP    , 0, 0));
  //LstModifParams.push_back(new CCharModifier ( formIDXTEMP  , true, txtIDXTEMP   , TempH, 15, false, false, false, false, true, 0, 0));
  // IDXLUM       Lumière max (O/N)                                                  Lumiere min. Oui
  LstModifParams.push_back(new CBoolModifier ( formIDXLUM   , true, true, txtIDXLUM    , &commonParams.bLumiereMin, (const char **)txtOuiNon, 0, 0));
  //                                                                                0123456789012345678901
  // IDXDATE      Date courante                                                      Date  26/03/18
  LstModifParams.push_back(new CDateModifier ( formIDXDATE  , true, txtIDXDATE   , 0, 0));
  //                                                                                0123456789012345678901
  // IDXHEURE     Heure courante                                                     Heure 09/12/22
  LstModifParams.push_back(new CHourModifier( formIDXHEURE , true, txtIDXHEURE  , NULL, 0, 0));
  //                                                                              0123456789012345678901
  // IDXUSERLEVEL User level                                                         User level (Beginner, Confirmed, Expert)
  LstModifParams.push_back(new CEnumModifier ( formIDXULEV, true, false, txtIDXULEV, (int *)&commonParams.iUserLevel, ULMAX, (const char **)txtLevelUser, 0, 0));
#ifdef WITHBLUETOOTH
  // IDXBLUETTH  Bluetooth On/Off
  LstModifParams.push_back(new CBoolModifier ( formIDXBLUT  , true, true, txtIDXBLUT   , &commonParams.bBluetooth, (const char **)txtOuiNon, 0, 0));
  // IDXBLUEINIT Bluetooth init
  LstModifParams.push_back(new CBoolModifier ( formIDXINITBL, true, true, txtIDXINITBL, &bInitBluetooth, (const char **)txtPPPNon, 0, 0));
#endif
  //                                                                                0123456789012345678901
  // IDXTSEUIL    Type de seuil (false relatif, true absolue)                        Threshold: relatif
  LstModifParams.push_back(new CBoolModifier ( formIDXTSEUIL   , true, true, txtIDXTSEUIL, &paramsOperateur.bTypeSeuil, (const char **)txtModeSeuil, 0, 0));
  //                                                                                0123456789012345678901
  // IDXSEUILR    Seuil de détection relatif en dB (5-99)                            Seuil rel.     20dB
  LstModifParams.push_back(new CModificatorInt  ( formIDXSEUIL, true, txtIDXSEUILR, (int *)&paramsOperateur.iSeuilDet, 5, 99, 1, 0, 0));
  //                                                                                0123456789012345678901
  // IDXSEUILA    Seuil de détection absolu en dB (-110/-30)                         Seuil abs.     20dB
  LstModifParams.push_back(new CModificatorInt  ( formIDXSEUIL, true, txtIDXSEUILA, (int *)&paramsOperateur.iSeuilAbs, -110, -30, 1, 0, 0));
  // IDXHRECM     Heterodyne recording mode                                          Het. Record Manual
  LstModifParams.push_back(new CBoolModifier ( formIDXTHRECM   , true, true, txtIDXTHRECM, &paramsOperateur.bAutoRecH, (const char **)txtModeHetRec, 0, 0));
  // IDXHAUTOMAN  Manual (false) / Auto (true) heterodyne mode
  LstModifParams.push_back(new CEnumModifier ( formIDXHAUTOMAN, true, false, txtIDXHAUTOMAN, (int *)&paramsOperateur.iAutoHeter, HAM_MAX, (const char **)txtHMANAUTO, 0, 0));
  // IDXHSELF     Heterodyne with selective filter or not (Tennsy 4.1 only)
  LstModifParams.push_back(new CEnumModifier ( formIDXHSELF, true, false, txtIDXHSELF, (int *)&paramsOperateur.iHeterSel, HSF_MAX, (const char **)txtHSYesNo, 0, 0));
  // IDXHGRAPH     Heterodyne with graphe or not
  LstModifParams.push_back(new CBoolModifier ( formIDXHGRAPH, true, true, txtIDXHGRAPH, &paramsOperateur.bGraphHet, (const char **)txtOuiNon, 0, 0));
  // IDXHGRAPH    Refresh period of the heterodyne graph (0.2/3)                     Graph Refresh 3.0s
  LstModifParams.push_back(new CFloatModifier ( formIDXRAFGRAPH, true, txtIDXRAFGRAPH, (float *)&paramsOperateur.fRefreshGRH, 0.2, 2.0, 0.2, 0, 0));
  // IDXAUTOPLAY  Automatic play (X10) after recording in heterodyne mode
  LstModifParams.push_back(new CBoolModifier ( formIDXAUTOPLAY, true, true, txtIDXAUTOPLAY, &paramsOperateur.bAutoPlay, (const char **)txtOuiNon, 0, 0));
  // IDXHETAGC    AGC On Off
  LstModifParams.push_back(new CBoolModifier ( formIDXHETAGC, true, true, txtIDXHETAGC, &paramsOperateur.bAGCHet, (const char **)txtOuiNon, 0, 0));
  //                                                                                0123456789012345678901
  // IDXHDEB      Heure de début                                                     Heure debut 21:30
  LstModifParams.push_back(new CHourModifier( formIDXHDEB  , true, txtIDXHDEB   , (char *)&paramsOperateur.sHDebut, 0, 0));
  //                                                                                0123456789012345678901
  // IDXHFIN      Heure de fin                                                       Heure fin   02:15
  LstModifParams.push_back(new CHourModifier( formIDXHFIN  , true, txtIDXHFIN   , (char *)&paramsOperateur.sHFin, 0, 0));
  //                                                                                0123456789012345678901
  // IDXRECON     Recording time in Timed Recording mode in second (Depending SF)   Recording time 02:00:00
  LstModifParams.push_back(new CSecondModifier  ( SECOND_HMS, true, txtIDXRECON, (int *)&paramsOperateur.uiTRECRecord, 1, 18000, 0, 0));
  //                                                                                0123456789012345678901
  // IDXRECOFF    Waiting time in Timed Recording mode (1 to 12h step 1)             Waiting time  02:00:00
  LstModifParams.push_back(new CSecondModifier  ( SECOND_HMS, true, txtIDXRECOFF, (int *)&paramsOperateur.uiTRECWait, 1, 3600*12, 0, 0));
  //                                                                                0123456789012345678901
  // IDXFMIN      Fréquence d'intéret minimale en kHz (1-FMax)                       Frequence min 120kHz
  LstModifParams.push_back(new CModificatorInt  ( formIDXFMIN  , true, txtIDXFMIN   , (int *)&iFreqMinkHz, 1, 150, 1, 0, 0));
  //                                                                                0123456789012345678901
  // IDXFMAX      Fréquence d'intérêt maximale en kHz (FMin-Fe/2)                    Frequence max 120kHz
  LstModifParams.push_back(new CModificatorInt  ( formIDXFMAX  , true, txtIDXFMAX   , (int *)&iFreqMaxkHz, 1, 150, 1, 0, 0));
  //                                                                                0123456789012345678901
  // IDXFMINA     Fréquence d'intéret minimale en Hz (1-FMax) du mode audio          Frequence min 120kHz
  LstModifParams.push_back(new CFloatModifier  ( formIDXFMINA , true, txtIDXFMINA  , &fFreqMinAkHz, 0.1, 48.0, 0.1, 0, 0));
  //                                                                                0123456789012345678901
  // IDXFMAXA     Fréquence d'intérêt maximale en Hz (FMin-Fe/2) du mode audio       Frequence max 120kHz
  LstModifParams.push_back(new CFloatModifier  ( formIDXFMAXA , true, txtIDXFMAXA  , &fFreqMaxAkHz, 0.1, 48.0, 0.1, 0, 0));
  //                                                                              0123456789012345678901
  // IDXPRETRIGAUT Pre-trigger duration for Teensy 4.1 with extended RAM (1-8) Auto  Pre-Trig 1s
  LstModifParams.push_back(new CModificatorInt  ( formIDXPRETRIG, true, txtIDXPRETRIG, (int *)&paramsOperateur.iPreTrigDurAuto, 0, 10, 1, 0, 0));
  // IDXPRETRIGHET Pre-trigger duration for Teensy 4.1 with extended RAM (1-8) Heter Pre-Trig 1s
  LstModifParams.push_back(new CModificatorInt  ( formIDXPRETRIG, true, txtIDXPRETRIG, (int *)&paramsOperateur.iPreTrigDurHeter, 1, 10, 1, 0, 0));
  //                                                                                0123456789012345678901
  // IDXDMIN      Durée min en secondes (1-10)                                       Duree min  10 s
  LstModifParams.push_back(new CModificatorInt  ( formIDXDMIN  , true, txtIDXDMIN   , (int *)&paramsOperateur.uiDurMin, 1, 99, 1, 0, 0));
  //                                                                                0123456789012345678901
  // IDXDMAX      Durée max en secondes (1-999)                                      Duree max 999 s
  LstModifParams.push_back(new CModificatorInt  ( formIDXDMAX  , true, txtIDXDMAX   , (int *)&paramsOperateur.uiDurMax, 1, 999, 1, 0, 0));
  //                                                                                0123456789012345678901
  // IDXFECH      Fréquence d'échantillonnage                                        Freq. Echant. 384kHz
  LstModifParams.push_back(new CEnumModifier ( formIDXFECH  , true, false, txtIDXFECH   , (int *)&paramsOperateur.uiFe, MAXFE, (const char **)txtFreqEch, 0, 0));
  //                                                                                0123456789012345678901
  // IDXFECHA     Fréquence d'échantillonnage Audio                                  Freq. Echant.  48kHz
  LstModifParams.push_back(new CEnumModifier ( formIDXFECH  , true, false, txtIDXFECH   , (int *)&paramsOperateur.uiFeA, MAXFE, (const char **)txtFreqEch, 0, 0));
  //                                                                                0123456789012345678901
  // IDXSMODE,    For PRS, stereo recording mode (Stereo, Mono Right or Mono Left)
  LstModifParams.push_back(new CEnumModifier ( formIDXSMODE, true, false, txtIDXSMODE , (int *)&paramsOperateur.iStereoRecMode, SMMAX, (const char **)txtStereoMode, 0, 0));
  //                                                                                0123456789012345678901
  // IDXGNUM      Gain numérique                                                     Gain numerique +24dB
  LstModifParams.push_back(new CEnumModifier ( formIDXGNUM  , true, false, txtIDXGNUM    , (int *)&paramsOperateur.uiGainNum, MAXGAIN, (const char **)txtGainNum, 0, 0));
  //                                                                                0123456789012345678901
  // IDXFILT      Filtre lin. (O/N)                                                  Filtre lineaire Oui
  LstModifParams.push_back(new CBoolModifier ( formIDXFILT  , true, true, txtIDXFILTL   , &paramsOperateur.bFiltre, (const char **)txtOuiNon, 0, 0));
  //                                                                                0123456789012345678901 
  // IDXHPFILT    Filtre pa.haut 22kHz                                               Filtre pa.haut 22kHz
  LstModifParams.push_back(new CModificatorInt  ( formIDXHPFILT, true, txtIDXHPFILT  , &paramsOperateur.iFreqFiltreHighPass, 0, MAXHIHGPASS, 1, 0, 0));
  //LstModifParams.push_back(new CModificatorInt  ( formIDXHPFILT, true, txtIDXHPFILT  , &paramsOperateur.iFreqFiltreHighPass, 0, 50, 1, 0, 0));
  //                                                                                0123456789012345678901 
  // IDXFHPFILT   Filtre pa.haut 22.0kHz                                             Filtre pa.h. 22.0kHz
  LstModifParams.push_back(new CFloatModifier  ( formIDXFHPFILT, true, txtIDXFHPFILT, (float *)&paramsOperateur.fFreqFiltreHighPass, 0.0, MAXHIHGPASS, 0.1, 0, 0));
  //                                                                                0123456789012345678901
  // IDXMICTYPE,   //!< Microphone type for linear filter at 250 and 384kHz          Mic. type ICS40730
  LstModifParams.push_back(new CEnumModifier ( formIDXMICT  , true, false, txtIDXMICT, (int *)&paramsOperateur.iMicrophoneType, MTMAX, (const char **)txtMicType, 0, 0));
  //                                                                                0123456789012345678901
  // IDXEXP       Expansion de temps x10 (O/N)                                       T. Expansion X10 Oui
  LstModifParams.push_back(new CBoolModifier ( formIDXEXP   , true, true, txtIDXEXP  , &paramsOperateur.bExp10, (const char **)txtOuiNon, 0, 0));
  //                                                                                0123456789012345678901
  // IDXBATCORDER Batcorder log and Wave file name type                              Batcorder file Oui
  LstModifParams.push_back(new CBoolModifier ( formIDXBCOR  , true, false, txtIDXBCOR , &paramsOperateur.bBatcorderLog, (const char **)txtOuiNon, 0, 0));
  //                                                                                0123456789012345678901
  // IDXPREF      Préfixe des fichiers Wav                                           Prefixe wav [PRec ]
  LstModifParams.push_back(new CCharModifier ( formIDXPREF  , true, txtIDXPREF       , (char *)&paramsOperateur.sPrefixe, MAXPREFIXE, false, true, false, true, true, 0, 0));
  //                                                                                0123456789012345678901
  // IDXNDET      Nombre de détections (1-8)                                         Nb. Detections 4/8
  LstModifParams.push_back(new CModificatorInt  ( formIDXNDET  , true, txtIDXNDET    , (int *)&paramsOperateur.iNbDetect, 1, 8, 1, 0, 0));
  //                                                                                0123456789012345678901
  // IDXPETM      Période de mesure température (10-3600 step 10)                    Période T/H 3600 s
  LstModifParams.push_back(new CModificatorInt  ( formIDXPTEM  , true, txtIDXPTEM   , (int *)&paramsOperateur.iPeriodTemp, 10, 3600, 10, 0, 0));
  //                                                                                0123456789012345678901
  // IDXTHV       Mesure T/H y compris en veille                                     Mes. T/H en veille O
  LstModifParams.push_back(new CBoolModifier ( formIDXTHV   , true, false, txtIDXTHV, &paramsOperateur.bPermanentTempH, (const char **)txtYesNo, 0, 0));
  //                                                                                0123456789012345678901
  // IDXBDRL      Modification des bandes RhinoLogger                                Bandes RhinoLogger
  LstModifParams.push_back(new CPushButtonModifier( formIDXBDRL, true, txtIDXBDRL, 0, 0));
  //                                                                                0123456789012345678901
  // IDXAFFRL     Affichage RhinoLogger permanent                                    Aff RL permanent O
  LstModifParams.push_back(new CBoolModifier ( formIDXRLPERM, true, false, txtIDXRLPERM, &paramsOperateur.bRLAffPerm, (const char **)txtYesNo, 0, 0));
  // IDXFRTOP     Top audio frequency in kHz (1 to 50 kHz)
  LstModifParams.push_back(new CModificatorInt( formIDXFRTOP  , true, txtIDXFRTOP    , (int *)&paramsOperateur.iTopAudioFreq, 1, 50, 1, 0, 0));
  // IDXTOPDUR    Top audio duration in samples (256, 512 or 1024)
  LstModifParams.push_back(new CEnumModifier  ( formIDXTOPDUR  , true, false, txtIDXTOPDUR, (int *)&paramsOperateur.iDurTop, TSMAX, (const char **)txtDurTop192, 0, 0));
  // IDXTOPPE     Top audio period (0 for one at the start of the recording, 1 to 10 for one every 1 to 10 seconds)
  LstModifParams.push_back(new CModificatorInt( formIDXTOPPER, true, txtIDXTOPPER, (int *)&paramsOperateur.iTopPer, 0, 10, 1, 0, 0));
  // IDXLEDSYNCH  Using LED in synchro mode
  LstModifParams.push_back(new CEnumModifier ( formIDXLEDS   , true, false, txtIDXLEDS, (int *)&paramsOperateur.iLEDSynchro, MAXLEDSYNCH, (const char **)txtLEDSynch, 0, 0));
  // IDXSWCORBAT  To switch in modification of the measurement correction of the internal batteries
  bMesureBat = false;
  LstModifParams.push_back(new CBoolModifier ( formIDXCORBATX, true, false, txtIDXCORBATX, &bMesureBat, (const char **)txtYesNo, 0, 0));
  // IDXCORBAT    Modification of the measurement correction of the internal batteries
  LstModifParams.push_back(new CFloatModifier( formIDXCORBAT, true, txtIDXCORBAT, (float *)&fMesureBat, 3.0, 4.4, 0.1, 0, 0));
  fMesureBat = 4.2;
  // IDXSWCOREXT  To switch in modification of the measurement correction of the external batteries
  bMesureExt = false;
  LstModifParams.push_back(new CBoolModifier ( formIDXCOREXTX, true, false, txtIDXCOREXTX, &bMesureExt, (const char **)txtYesNo, 0, 0));
  // IDXCOREXT    Modification of the measurement correction of the external batteries
  LstModifParams.push_back(new CFloatModifier( formIDXCOREXT, true, txtIDXCOREXT, (float *)&fMesureExt, 10.0, 16.0, 0.1, 0, 0));
  fMesureExt = 12.0;
  // IDXLANG      Langue                                                             Language: Francais English
  LstModifParams.push_back(new CEnumModifier ( formIDXLANG  , true, false, txtIDXLANG, (int *)&commonParams.iLanguage, MAXLANGUAGE, (const char **)txtLanguage, 0, 0));
  // IDXTSTSD     Test SD
  LstModifParams.push_back(new CPushButtonModifier( formIDXTSTSD, true, txtIDXTSTSD, 0, 0));
  //                                                                                0123456789012345678901
  // IDXDEF       Params par défaut                                                  Params. p. defaut Oui
  LstModifParams.push_back(new CBoolModifier ( formIDXDEF   , true, false, txtIDXDEF  , &bDefault, (const char **)txtOuiNon, 0, 0));
  // IDXPROFILES  Goto modification of operator profiles
  LstModifParams.push_back(new CPushButtonModifier( formIDXTSTSD, true, txtIDXPROFILE, 0, 0));
  //                                                                                0123456789012345678901
  // IDXSNOISE    Save noise                                                         Fichier bruit Oui
  LstModifParams.push_back(new CBoolModifier ( formIDXSNOISE, true, false, txtIDXSNOISE, &paramsOperateur.bSaveNoise, (const char **)txtOuiNon, 0, 0));
  //                                                                                0123456789012345678901
  // IDXLED       Utilisation de la LED                                              Util. LED "Enregist."
  LstModifParams.push_back(new CEnumModifier( formIDXLED , true, false, txtIDXLED     , (int *)&commonParams.iUtilLed, MAXLED, (const char **)txtLED, 0, 0));
  //                                                                                0123456789012345678901
  // IDXBOOT      Goto Bootloader to load a new version                              Bootloader Y
  LstModifParams.push_back(new CBoolModifier ( formIDXBOOT, true, false, txtIDXBOOT, &bBoot, (const char **)txtOuiNon, 0, 0));
  //                                                                                0123456789012345678901
  // IDXCOPYR     Menu Copyright                                                     Copyright Jean-Do.
  LstModifParams.push_back(new CPushButtonModifier ( formIDXCOPYR , true, txtIDXCOPYR  , 0, 0));

  ((CEnumModifier *)LstModifParams[IDXFECHA])->SetElemValidity( FE250KHZ, false);
  ((CEnumModifier *)LstModifParams[IDXFECHA])->SetElemValidity( FE384KHZ, false);
  ((CEnumModifier *)LstModifParams[IDXFECHA])->SetElemValidity( FE500KHZ, false);
  ((CEnumModifier *)LstModifParams[IDXLANG]) ->SetElemValidity( LGERMAN, false);
  ((CEnumModifier *)LstModifParams[IDXLANG]) ->SetElemValidity( LDUTCH, false);
  bDefault = false;
  bBoot    = false;
  //Serial.printf("CModeParams::CModeParams iModeRecord %d\n", paramsOperateur.iModeRecord);
  // Dans le cas d'un PR Stéréo Synchro, le passage en mode paramètres reset la configuration valide
  CSynchroLink::ResetValidConfig();
#ifdef WITHBLUETOOTH
  uiBLETime = 0;
#endif
  LstModifParams[IDXHSELF]->SetbValid(false );
#if defined(__IMXRT1062__) // Teensy 4.1
#ifdef SELFILTERH20KHZ
  LstModifParams[IDXHSELF]->SetbValid(true );
#endif
#ifdef SELFILTERH10KHZ
  LstModifParams[IDXHSELF]->SetbValid(true );
#endif
#endif
}

//-------------------------------------------------------------------------
// Début du mode
void CModeParams::BeginMode()
{
  //Serial.printf("CModeParams::BeginMode A iModeRecord %d\n", paramsOperateur.iModeRecord);
  // Lecture des paramètres
  ReadParams();
  // On mémorise le mode d'enregistrement courant pour le garder si l'opérateur passe en mode test micro
  iMemoRecordMode = paramsOperateur.iModeRecord;
  // Information de l'opérateur pour tenir compte du temps de lecture long des cartes supérieures à 16Go
  pDisplay->clearBuffer();
  pDisplay->setCursor(0,0);
  pDisplay->println(txtLogStartPar[commonParams.iLanguage]);
  pDisplay->setCursor(0,10);
  pDisplay->println(txtLogSDPWait[commonParams.iLanguage]);
  pDisplay->sendBuffer();
  // Initialisation
  iSecondesInactives = 0;
  iCurrentSecond = second();
  // Controle de la carte SD
  LstModifParams[IDXSD]->SetbCalcul( true);
  // Vérification mode d'enregistrement
  switch (paramsOperateur.iModeRecord)
  {
  default:
  case PHETER   :  // Mode hétérodyne
  case RECAUTO  :  // Mode enregistreur automatique
  case AUDIOREC :  // Mode enregistreur audio
  case PRHINOLOG:  // Mode RhinoLogger
  case PROTFIXE :  // Protocole Vigie-Chiro point fixe
  case TIMEDREC :  // Mode enregistrement cadencé
  case SYNCHRO  :  // Mode enregistrement auto synchro
    // Mode autorisé
    break;
  case PROTPED  :  // Protocole Vigie-Chiro pédestre
  case PROTROUT :  // Protocole Vigie-Chiro routier
  case PTSTMICRO:  // Protocole test micro
    // Pour ces modes, on impose toujours AUTO ou hétérodyne
    if (CModeGeneric::GetRecorderType() == ACTIVE_RECORDER)
      paramsOperateur.iModeRecord = PHETER;
    else
      paramsOperateur.iModeRecord = RECAUTO;
  }
  // Initialisation des noms des profils
  for (int i=0; i<MAXPROFILES; i++)
  {
    strcpy( strProfiles[LENGLISH][i], commonParams.ProfilesNames[i]);
    strcpy( strProfiles[LFRENCH ][i], commonParams.ProfilesNames[i]);
    //Serial.printf("Profil %d, strProfiles[E] %s, strProfiles[F] %s, ProfilesNames %s\n", i, strProfiles[0][i], strProfiles[1][i], commonParams.ProfilesNames[i]);
  }
  // Init des bandes d'intérets
  iFreqMinkHz = paramsOperateur.uiFreqMin / 1000;
  iFreqMaxkHz = paramsOperateur.uiFreqMax / 1000;
  fFreqMinAkHz = (float)paramsOperateur.uiFreqMinA / 1000.0;
  fFreqMaxAkHz = (float)paramsOperateur.uiFreqMaxA / 1000.0;
  //Serial.printf("CModeParams::BeginMode Avant Coherenceparams profile %d [%s], paramsOperateur.uiFreqMaxA %d Hz\n", commonParams.iSelProfile, commonParams.ProfilesNames[commonParams.iSelProfile], paramsOperateur.uiFreqMaxA);
  // Cohérence entre les paramètres
  Coherenceparams();
  // Si pas en mode debug, on cache la gestion de la LED
  if (CModeGeneric::bDebug == false)
    LstModifParams[IDXLED]->SetbValid(false);
  // Appel de la méthode parente sans lecture des paramètres
  bReadParams = false;
  //Serial.println("CModeParams::BeginMode B");
  CModeGeneric::BeginMode();
  //Serial.printf("CModeParams::BeginMode iYFirstLine %d\n", iYFirstLine);
  //Serial.printf("CModeParams::BeginMode A iModeRecord %d\n", paramsOperateur.iModeRecord);
  //Serial.printf("CModeParams::BeginMode OK profile %d [%s], paramsOperateur.uiFreqMaxA %d Hz\n", commonParams.iSelProfile, commonParams.ProfilesNames[commonParams.iSelProfile], paramsOperateur.uiFreqMaxA);
  // Mesure de la batterie
  CheckBatterie();
  // Init température et humidité
  FormatTH();
}

//-------------------------------------------------------------------------
// Fin du mode
void CModeParams::EndMode()
{
  // Préparation des données pour passage en mode Veille/Enregistrement
  CalcTimeDebFin();
  // Sauvegarde des paramètres en EEPROM
  // Si on est en mode protocole ou test micro, on ne mémorise pas le mode
  int iMemoMode = paramsOperateur.iModeRecord;
  switch (paramsOperateur.iModeRecord)
  {
  default:
  case PHETER   :  // Mode hétérodyne
  case RECAUTO  :  // Mode enregistreur automatique
  case AUDIOREC :  // Mode enregistreur audio
  case PRHINOLOG:  // Mode RhinoLogger
  case TIMEDREC :  // Mode enregistrement cadencé
  case SYNCHRO  :  // Mode enregistrement auto synchro
  case PROTFIXE :  // Protocole Vigie-Chiro point fixe
    // On mémorise bien le mode
    break;
  case PROTPED  :  // Protocole Vigie-Chiro pédestre
  case PROTROUT :  // Protocole Vigie-Chiro routier
  case PTSTMICRO:  // Protocole test micro
    if (CModeGeneric::GetRecorderType() == ACTIVE_RECORDER)
      // C'est le mode hétérodyne qui est mémorisé
      paramsOperateur.iModeRecord = PHETER;
    else
      // C'est le mode auto qui est mémorisé
      paramsOperateur.iModeRecord = RECAUTO;
  }
  //Serial.printf("CModeParams::EndMode Write mode %d, current mode %d\n", paramsOperateur.iModeRecord, iMemoMode);
  // Appel de la méthode parente (mémorisation des paramètres)
  CGenericMode::EndMode();
  // On restitue le mode réel
  paramsOperateur.iModeRecord = iMemoMode;
  //digitalWriteFast( OPT_INIT_BLE, HIGH);  // Bluetooth init off
  //Serial.printf("CModeParams::EndMode Mode %d\n", paramsOperateur.iModeRecord);
}

//-------------------------------------------------------------------------
// Affichage du mode sur l'écran
// Formatage chaine température et humidité
void CModeParams::FormatTH()
{
  // Si sonde présente, préparation chaine temérature et humidité
  char sTemp[20];
  strcpy( TempH, txtIDXTEMP[commonParams.iLanguage]);
  if (IsTempSensor())
  {
    LectureTemperature();
    // Temperature with BMP280 and BME280
    sprintf( sTemp, "%+02d%c", (int)GetTemperature(), 176);
    strcat(TempH, sTemp);
    if (GetHumidity() > 5.0)
    {
      // Humidity only on BME280
      sprintf( sTemp, " %2d%%", GetHumidity());
      strcat(TempH, sTemp);
    }
  }
  else
    strcat( TempH, "--");
  ((CPushButtonModifier *)LstModifParams[IDXTEMP])->SetString( TempH);
  //Serial.printf("CModeParams::FormatTH %s\n", TempH);
}

//-------------------------------------------------------------------------
// Affichage du mode sur l'écran
// Cette méthode est appelée régulièrement par le loop principal
// à charge des différents modes d'afficher les informations nécessaires
void CModeParams::PrintMode()
{
  // Test si inactivité
  if (second() != iCurrentSecond)
  {
    //Serial.printf("CModeParams::PrintMode iYFirstLine %d, iFirstVisibleIdxLine %d, iLastVisibleIdxLine %d, iNbLines %d, iFirstIdxLine %d, iLastIdxLine %d\n", iYFirstLine, iFirstVisibleIdxLine, iLastVisibleIdxLine, iNbLines, iFirstIdxLine, iLastIdxLine);
    // Chaque seconde, on incrémente le compteur d'inactivité
    iCurrentSecond = second();
    iSecondesInactives++;
    if (iSecondesInactives >= 60*15)
      // 15mn sans activité, on bascule automatiquement en enregistrement
      iNewMode = MRECORD;
  }
#ifdef WITHBLUETOOTH
  if (bInitBluetooth and uiBLETime > 0 and millis() > uiBLETime)
  {
    // Fin de l'init Bluetooth
    bInitBluetooth = false;
    digitalWriteFast(OPT_INIT_BLE, HIGH);
    uiBLETime = 0;
  }
#endif
  // Appel de la fonction de base
  CGenericMode::PrintMode();
}

//-------------------------------------------------------------------------
// Traitement des ordres claviers
int CModeParams::KeyManager(
  unsigned short key  // Touche sollicitée par l'opérateur
  )
{
  if (key != K_NO)
    // RAZ du compteur d'inactivité
    iSecondesInactives = 0;
  int newMode = NOMODE;
  if (key == K_MODEA and CModeGeneric::GetRecorderType() == ACTIVE_RECORDER)
  {
    //Serial.printf("CModeParams::KeyManager mode %d\n", paramsOperateur.iModeRecord);
    // In Active Recorder K_MODEA change mode : Record mode -> Player -> Parameters -> Record mode
    switch (paramsOperateur.iModeRecord)
    {
    default:
    case RECAUTO  : newMode = MRECORD;   break; // Mode enregistreur automatique
    case SYNCHRO  : newMode = MSYNCHRO;  break; // Mode enregistreur automatique synchro
    case PROTPED  : newMode = MPROPED;   break; // Protocole Vigie-Chiro pédestre
    case PROTROUT : newMode = MPROROU;   break; // Protocole Vigie-Chiro routier
    case PROTFIXE : newMode = MPROPFX;   break; // Protocole Vigie-Chiro point fixe
    case PRHINOLOG: newMode = MRHINOL;   break; // Mode RhinoLogger
    case PTSTMICRO: newMode = MPROTST;   break; // Protocole test micro
    case PHETER   : newMode = MHETER;    break; // Mode hétérodyne
    case AUDIOREC : newMode = MAUDIOREC; break; // Mode hétérodyne
    case TIMEDREC : newMode = MTRECORD;  break; // Mode enregistrement cadencé
    }
    if (paramsOperateur.iModeRecord == PTSTMICRO)
      // On restitue le mode d'enregistrement précédent pour rester dans ce mode à la sortie du test micro
      paramsOperateur.iModeRecord = iMemoRecordMode;
    //Serial.printf("CModeParams::KeyManager Mode %d, mémo %d\n", paramsOperateur.iModeRecord, iMemoRecordMode);
  }
  else
  {
    // Appel de la méthode parente
    newMode = CModeGeneric::KeyManager( key);
  }
  // Si l'heure courante est visible et non en modification
  if (!LstModifParams[IDXHEURE]->IsModif() and iFirstVisibleIdxLine < IDXHEURE)
    // Pour le réaffichage de l'heure
    bRedraw = true;
  return newMode;
}

//-------------------------------------------------------------------------
// Called on end of change of a modifier
// If a mode change, returns the requested mode
int CModeParams::OnEndChange(
  int idxModifier // Index of affected modifier
  )
{
  // Selon le paramètre modifié
  switch (idxSelParam)
  {
  case IDXPROFILE:
    // Sélection d'un nouveau profil, lecture des paramètres associés sans lecture des paramètres communs
    bReadCommon = false;
    StaticReadParams();
    bReadCommon = true;
    // Init des bandes d'intérets après lecture du profil
    iFreqMinkHz = paramsOperateur.uiFreqMin / 1000;
    iFreqMaxkHz = paramsOperateur.uiFreqMax / 1000;
    fFreqMinAkHz = (float)paramsOperateur.uiFreqMinA / 1000.0;
    fFreqMaxAkHz = (float)paramsOperateur.uiFreqMaxA / 1000.0;
    if (commonParams.iLanguage == LFRENCH)
      LogFile::AddLog( LLOG, "Sélection du niveau utilisateur %s", commonParams.ProfilesNames[commonParams.iSelProfile]);
    else
      LogFile::AddLog( LLOG, "User level selection %s", commonParams.ProfilesNames[commonParams.iSelProfile]);
    break;
  case IDXUSERLEVEL:
    if (commonParams.iLanguage == LFRENCH)
      LogFile::AddLog( LLOG, "Sélection du profil %s", txtLevelUser[commonParams.iLanguage][commonParams.iUserLevel]);
    else
      LogFile::AddLog( LLOG, "Profile selection %s", txtLevelUser[commonParams.iLanguage][commonParams.iUserLevel]);
    if (commonParams.iUserLevel == ULDEBUG)
      CModeGeneric::bDebug = true;
    else
      CModeGeneric::bDebug = false;
    break;
  case IDXLUM  :
    if (commonParams.bLumiereMin)
      pDisplay->setContrast(1);
    else
      pDisplay->setContrast(255);
    break;
  case IDXMODER:
    if (((CPushButtonModifier *)LstModifParams[IDXMODER])->GetbPush())
    {
      switch (paramsOperateur.iModeRecord)
      {
      default:
      case RECAUTO  : iNewMode = MRECORD;   break; // Mode enregistreur automatique
      case PROTPED  : iNewMode = MPROPED;   break; // Protocole Vigie-Chiro pédestre
      case PROTROUT : iNewMode = MPROROU;   break; // Protocole Vigie-Chiro routier
      case PROTFIXE : iNewMode = MPROPFX;   break; // Protocole Vigie-Chiro point fixe
      case PRHINOLOG: iNewMode = MRHINOL;   break; // Mode RhinoLogger
      case PTSTMICRO: iNewMode = MPROTST;   break; // Mode test micro
      case PHETER   : iNewMode = MHETER;    break; // Mode hétérodyne
      case AUDIOREC : iNewMode = MAUDIOREC; break; // Mode enregistreur audio
      case TIMEDREC : iNewMode = MTRECORD;  break; // Mode enregistrement cadencé
      case SYNCHRO  : iNewMode = MSYNCHRO;  break; // Mode enregistrement auto synchro
      }
    }
    if (paramsOperateur.iModeRecord == PTSTMICRO)
      // On restitue le mode d'enregistrement précédent pour rester dans ce mode à la sortie du test micro
      paramsOperateur.iModeRecord = iMemoRecordMode;
    //Serial.printf("CModeParams::OnEndChange A Mode %d, mémo %d\n", paramsOperateur.iModeRecord, iMemoRecordMode);
    break;
  case IDXMODEF:
    if (paramsOperateur.iModeRecord != PTSTMICRO)
      // On mémorise le mode d'enregistrement courant pour le garder si l'opérateur passe en mode test micro
      iMemoRecordMode = paramsOperateur.iModeRecord;
    break;
  case IDXBAT:
    CheckBatterie();
    break;
  case IDXEXT:
    CheckBatterie();
    break;
  case IDXLANG:
    // Init de la langue des modificateurs
    CGenericModifier::SetLanguage( commonParams.iLanguage);
    // Mesure de la batterie
    CheckBatterie();
    // Controle de la carte SD
    LstModifParams[IDXSD]->SetbCalcul( true);
    // Init température et humidité
    FormatTH();
    break;
  case IDXDEF:
    if (bDefault)
      // Initialisation des paramètres aux valeurs par défaut
      SetDefault();
    bDefault = false;
    break;
  case IDXBDRL:
    // Passage en mode modification des bandes RhinoLogger
    iNewMode = MBDRL;
    break;
  case IDXTSTSD:
    // Passage en mode test de la carte SD
    iNewMode = MTSTSD;
    break;
  case IDXTEMP:
    if (((CPushButtonModifier *)LstModifParams[IDXTEMP])->GetbPush())
      // Init de la température et humidité
      FormatTH();
    break;
 case IDXBOOT:
    if (bBoot)
    {
      // Goto bootloader mode
      pDisplay->clearBuffer();
      pDisplay->setCursor(0,0);
      pDisplay->println("Bootloader mode...");
      pDisplay->sendBuffer();
#if defined(__MK66FX1M0__) // Teensy 3.6
      _reboot_Teensyduino_();
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
       asm("bkpt #251"); // run bootloader
#endif
    }
    break;
  case IDXPROFILES:
    // Goto modification of operator profiles
    iNewMode = MPROFILES;
    break;
  case IDXCOPYR:
    // Goto Copyright mode
    iNewMode = MCOPYRIGHT;
    break;
  case IDXFMIN:
    // Minimum frequency of interest in kHz
    paramsOperateur.uiFreqMin = iFreqMinkHz * 1000;
    break;
  case IDXFMAX:
    // Maximum frequency of interest in kHz
    paramsOperateur.uiFreqMax = iFreqMaxkHz * 1000;
    break;
  case IDXFMINA:
    // Minimum frequency of interest in kHz for audio mode
    paramsOperateur.uiFreqMinA = (int)(fFreqMinAkHz * 1000.0);
    break;
  case IDXFMAXA:
    // Minimum frequency of interest in kHz for audio mode
    paramsOperateur.uiFreqMaxA = (int)(fFreqMaxAkHz * 1000.0);
    break;
#ifdef WITHBLUETOOTH
  case IDXBLUETTH:
    // Bluetooth On/Off
    break;
  case IDXBLUEINIT:
    // Bluetooth init
    if (bInitBluetooth)
    {
      digitalWriteFast(OPT_ALIM_BLE, LOW);
      delay(100);
      digitalWriteFast(OPT_INIT_BLE, LOW);
      uiBLETime = millis() + 15000;
    }
    else
      digitalWriteFast(OPT_INIT_BLE, HIGH);
    break;
#endif
  case IDXSWCORBAT:
    if (bMesureBat)
    {
      // Passage en mode correction de la mesure batterie
      LstModifParams[IDXSWCORBAT]->StopModif();
      LstModifParams[IDXCORBAT  ]->StartModif();
      // Deselect the old one and select the new one
      LstModifParams[IDXSWCORBAT]->SetbSelect( false);
      LstModifParams[IDXCORBAT  ]->SetbSelect( true);
      bModifParam = true;
      idxSelParam = IDXCORBAT;
    }
    else
    {
      // Deselect the old one and select the new one
      LstModifParams[IDXCORBAT  ]->SetbSelect( false);
      LstModifParams[IDXSWCORBAT]->SetbSelect( true);
      idxSelParam = IDXSWCORBAT;
    }
    break;
  case IDXCORBAT:
    // Mesure de la batterie sans correction
    commonParams.dCorrectBat = 0.0;
    CheckBatteries(MAX_MESURE);
    // Calcul de la correction
    commonParams.dCorrectBat = fMesureBat - fNivBatLiPo;
    // Mesure de la batterie avec correction
    CheckBatterie();
    bMesureBat = false;
    // Deselect the old one and select the new one
    LstModifParams[IDXCORBAT  ]->SetbSelect( false);
    LstModifParams[IDXSWCORBAT]->SetbSelect( true);
    idxSelParam = IDXSWCORBAT;
    LogFile::AddLog( LLOG, "çç Correction batteries %fV", commonParams.dCorrectBat);
    break;
  case IDXSWCOREXT:
    if (bMesureExt)
    {
      // Passage en mode correction de la mesure batterie
      LstModifParams[IDXSWCOREXT]->StopModif();
      LstModifParams[IDXCOREXT  ]->StartModif();
      // Deselect the old one and select the new one
      LstModifParams[IDXSWCOREXT]->SetbSelect( false);
      LstModifParams[IDXCOREXT  ]->SetbSelect( true);
      bModifParam = true;
      idxSelParam = IDXCOREXT;
    }
    else
    {
      // Deselect the old one and select the new one
      LstModifParams[IDXCOREXT  ]->SetbSelect( false);
      LstModifParams[IDXSWCOREXT]->SetbSelect( true);
      idxSelParam = IDXSWCOREXT;
    }
    break;
  case IDXCOREXT:
    // Mesure de la tension externe sans correction
    commonParams.dCorrectExt = 0.0;
    CheckBatteries(MAX_MESURE);
    // Calcul de la correction
    commonParams.dCorrectExt = fMesureExt - fNivBatExt;
    // Mesure de la tension externe avec correction
    CheckBatterie();
    bMesureExt = false;
    // Deselect the old one and select the new one
    LstModifParams[IDXCOREXT  ]->SetbSelect( false);
    LstModifParams[IDXSWCOREXT]->SetbSelect( true);
    idxSelParam = IDXSWCOREXT;
    break;
  }
//Serial.println("StaticWriteParams on CModeParams::OnEndChange");
  // Cohérence entre les paramètres
  Coherenceparams();
  // Mémorisation des paramètres
  WriteParams();
  //Serial.printf("CModeParams::OnEndChange Mode %d, mémo %d\n", paramsOperateur.iModeRecord, iMemoRecordMode);
  return iNewMode;
}

//-------------------------------------------------------------------------
// Cohérence entre les différents paramètres
/*
 *              ----------------------------------------------------------------------|
 *              | Recording  | Heterodyne | Protocole | Microphone Test | RhinoLogger |
 *              |    PR/AR   |  AR only   |   PR/AR   |     PR/AR       |   PR/AR     |
 * |----------------------------------------------------------------------------------|
 * |CPU  48 MHz | Max 250kHz |     No     |    No     |      Yes        |    Yes      |
 * |CPU 144 MHz |    Yes     | Max 250kHz |    Yes    |      Yes        |    Yes      |
 * |CPU 180 MHz |    Yes     |     Yes    |    Yes    |      Yes        |    Yes      |
 * |----------------------------------------------------------------------------------|
 * 
 *             -----------------------------------------------------------------------------------------------------------
 *             |            |            |           |                 |             |           |      User level       |
 *             |            |            |           |                 |             |           | Beginner/Débutant     |
 *             |   AutoRec  | Heterodyne | Protocole | Microphone Test | RhinoLogger | AudioRec  | Expert                |
 *             |    PR/AR   |  AR only   |   PR/AR   |     PR/AR       |   PR/AR     | AR only   | Debug                 |
 *             |  Beginner  |  Beginner  | Confirmed |   Beginner      |  Confirmed  | Confirmed |                       |
 *             -----------------------------------------------------------------------------------------------------------
 * IDXMODER    |     OK     |    OK      |    OK     |      OK         |     OK      |    OK     | All                   | Entering recording mode (Go)
 * IDXMODEF    |     OK     |    OK      |    OK     |      OK         |     OK      |    OK     | All but               | Operating mode (Auto record, Walkin. Protoc., Road Protocol, Fixed P. Proto., RhinoLogger, Microphone test, Heterodyne, Audio Rec.)
 * IDXBAT      |     OK     |    OK      |    OK     |      OK         |     OK      |    OK     | All                   | Internal battery charge level in %
 * IDXEXT      |     OK     |    OK      |    OK     |      OK         |     OK      |    OK     | All                   | External battery charge level in %
 * IDXSD       |     OK     |    OK      |    OK     |      OK         |     OK      |    OK     | All                   | SD card occupancy
 * IDXLUM      |     OK     |    OK      |    OK     |      OK         |     OK      |    OK     | All                   | Max light (Y/N)
 * IDXDATE     |     OK     |    OK      |    OK     |      OK         |     OK      |    OK     | All                   | Current date
 * IDXHEURE    |     OK     |    OK      |    OK     |      OK         |     OK      |    OK     | All                   | Current hour
 * IDXUSERLEVEL|     OK     |    OK      |    OK     |      OK         |     OK      |    OK     | All                   | User level (Beginner, Confirmed, Expert)
 * IDXTSEUIL   |     OK     |    OK      | NO-false  |      NO         |  NO-false   |    OK     | Expert, other=relative| Threshold type (false relative, true absolute)
 * IDXSEUILR   |     OK     |    OK      |    NO     |      NO         |    OK       |    OK     | All                   | Relative detection threshold in dB (5-99)
 * IDXSEUILA   |     OK     |    OK      |    NO     |      NO         |    NO       |    OK     | -                     | Absolute detection threshold in dB (-110/-30)
 * IDXHRECM    |     NO     |    OK      |    NO     |      NO         |    NO       |    NO     | Expert, minus=Manual  | Heterodyne recording mode (false manual, true auto)
 * IDXHREFGR   |     NO     |    OK      |    NO     |      NO         |    NO       |    OK     | Expert, minus=1       | Refresh period of the heterodyne graph (0.2/2.0)
 * IDXHDEB     |     OK     |    NO      |    ??     |      NO         |    OK       |    NO     | All                   | Auto recording start time (hh:mm)
 * IDXHFIN     |     OK     |    NO      |    ??     |      NO         |    OK       |    NO     | All                   | Auto recording end time (hh:mm)
 * IDXFMIN     |     OK     |    OK      |    OK     |      OK         |    NO       |    OK     | Expert, minus=15      | Minimum frequency of interest in kHz (1-150)
 * IDXFMAX     |     OK     |    OK      |    OK     |      OK         |    NO       |    OK     | Expert, minus=120     | Maximum frequency of interest in kHz (1-150)
 * IDXFMINA    |     NO     |    NO      |    NO     |      NO         |    NO       |    NO     | Expert, minus=1       | Minimum frequency of interest in kHz (1-18) for audio mode
 * IDXFMAXA    |     NO     |    NO      |    NO     |      NO         |    NO       |    NO     | Expert, minus=48      | Maximum frequency of interest in kHz (1/48) for audio mode
 * IDXDMIN     |     OK     |    OK      |    NO     |      NO         |    NO       |    OK     | Expert, minus=1       | Minimum duration in seconds (1-10)
 * IDXDMAX     |     OK     |    OK      |    NO     |      NO         |    NO       |    OK     | Expert, minus=10      | Maximum duration in seconds (10-999)
 * IDXFECH     |     OK     |    OK      |    NO     |      NO         |    NO       |    NO     | Expert, minus=384     | Sampling frequency in kHz (48, 96, 192, 250, 384, 500)
 * IDXFECHA    |     NO     |    NO      |    NO     |      NO         |    NO       |    OK     | Expert, minus=48      | Sampling frequency for audio mode in kHz (48, 96, 192)
 * IDXSMODE    |     OK     |    NO      |    OK     |      OK         |    NO       |    NO     | Expert                | For PRS, stereo recording mode (Stereo, Mono Right or Mono Left)
 * IDXGNUM     |     OK     |    OK      |    NO     |      NO         |    NO       |    NO     | Expert, minus=12      | Numeric gain (0, 6, 12, 18, 24dB)
 * IDXFILT     |     OK     |    NO      |    NO     |      NO         |    NO       |    NO     | Expert, minus=no      | Low pass filter (SFr<384) or linear filter (SFr>= 384) (Y/N)
 * IDXHPFILT   |     OK     |    OK      |    NO     |      NO         |    NO       |    NO     | Expert, minus=0       | High pass filter in kHz (0-25)
 * IDXMICTYPE  |     OK     |    NO      |    NO     |      NO         |    NO       |    NO     | Expert, minus=0       | Microphone type for linear filter at 250 and 384kHz
 * IDXEXP      |     OK     |    OK      |    NO     |      NO         |    NO       |    NO     | Expert, minus=Yes     | Time expansion x10 (Y/N)
 * IDXBATCORDER|     OK     |    OK      |    OK     |      OK         |    OK       |    OK     | Expert, minus=wildlife| Batcorder log and Wave file name type (false=wildlife, true=batcorder)
 * IDXPREF     |     OK     |    OK      |    NO     |      NO         |    NO       |    OK     | All                   | Wav file prefix (PRec_)
 * IDXNDET     |     OK     |    OK      |    NO     |      NO         |    NO       |    OK     | Expert, minus=3       | Number of detections (1-8)
 * IDXPETM     |     OK     |    OK      |    OK     |      NO         |    NO       |    OK     | Expert, minus=10mn    | Temperature measurement period in second (10-3600)
 * IDXTHV      |     OK     |    NO      |    OK     |      NO         |    NO       |    NO     | Expert, minus=no      | T/H measurement including in standby (Y/N)
 * IDXBDRL     |     NO     |    NO      |    NO     |      NO         |    OK       |    NO     | Expert, minus=no      | Editing RhinoLogger bands (Go)
 * IDXAFFRL    |     NO     |    NO      |    NO     |      NO         |    OK       |    NO     | Expert, minus=no      | Permanent RhinoLogger display (Y/N)
 * IDXLANG     |     NO     |    NO      |    NO     |      NO         |    OK       |    NO     | All                   | Language used (English/French)
 * IDXTSTSD    |     NO     |    NO      |    NO     |      NO         |    OK       |    NO     | All                   | SD card test
 * IDXDEF      |     NO     |    NO      |    NO     |      NO         |    OK       |    NO     | All                   | Default settings (Y/N)
 * IDXSNOISE   |     OK     |    NO      |    NO     |      NO         |    OK       |    NO     | Expert, minus=no      | Save noise file (Y/N)
 * IDXLED      |     OK     |    OK      |    OK     |      OK         |    OK       |    OK     | Expert, minus=no      | Using the LED (No, Recordining, Acquisition, IS DMA, Tr. Loop, Calc Noise, Det. Act., Buf. Los.)
 * IDXBOOT     |     NO     |    NO      |    NO     |      NO         |    OK       |    NO     | All                   | Goto Bootloader to load a new version in Active Recorder (Yes/No)
 *             -----------------------------------------------------------------------------------------------------------
*/
void CModeParams::Coherenceparams()
{
  // Par défaut, on met tout valide
  for (int i=0; i<(int)LstModifParams.size(); i++)
    LstModifParams[i]->SetbValid(true);
  if (!bMesureBat)
  {
    LstModifParams[IDXSWCORBAT]->SetbValid(true);
    LstModifParams[IDXCORBAT  ]->SetbValid(false);
  }
  else
  {
    LstModifParams[IDXSWCORBAT]->SetbValid(false);
    LstModifParams[IDXCORBAT  ]->SetbValid(true);
  }
  if (CModeGeneric::GetRecorderType() == ACTIVE_RECORDER)
  {
    LstModifParams[IDXSWCOREXT]->SetbValid(false);
    LstModifParams[IDXCOREXT  ]->SetbValid(false);
  }
  else
  {
    if (!bMesureExt)
    {
      LstModifParams[IDXSWCOREXT]->SetbValid(true);
      LstModifParams[IDXCOREXT  ]->SetbValid(false);
    }
    else
    {
      LstModifParams[IDXSWCOREXT]->SetbValid(false);
      LstModifParams[IDXCOREXT  ]->SetbValid(true);
    }
  }
  // Par défaut fréquence d'enchantillonnage ultrason
  LstModifParams[IDXFECH ]->SetbValid(true);
  LstModifParams[IDXFECHA]->SetbValid(false);
  // Par défaut, bande d'intérêt audio invalide
  LstModifParams[IDXFMIN  ]->SetbValid(true );
  LstModifParams[IDXFMAX  ]->SetbValid(true );
  LstModifParams[IDXFMINA ]->SetbValid(false);
  LstModifParams[IDXFMAXA ]->SetbValid(false);
  LstModifParams[IDXHAUTOMAN]->SetbValid(false);
  LstModifParams[IDXHGRAPH  ]->SetbValid(false);
  LstModifParams[IDXHETAGC  ]->SetbValid(false);
  LstModifParams[IDXHSELF   ]->SetbValid(false);
  LstModifParams[IDXHREFGR  ]->SetbValid(false);
  LstModifParams[IDXAUTOPLAY]->SetbValid(false);
  // Ligne température/humidité uniquement sur PR et PRS
  if (CModeGeneric::GetRecorderType() == ACTIVE_RECORDER)// or IsTempSensor() == false)
    LstModifParams[IDXTEMP]->SetbValid(false);
  else
    LstModifParams[IDXTEMP]->SetbValid(true);
#ifdef WITHBLUETOOTH
  // Test si option bluetooth
  if (CModeGeneric::GetRecorderType() == ACTIVE_RECORDER and digitalRead(OPT_BLUETOOTH) == 0)
  {
    // Sur AR, si option Bluetooth, on débloque la fonction
    Serial.println("Avec Bluetooth");
    LstModifParams[IDXBLUETTH ]->SetbValid(true );
    LstModifParams[IDXBLUEINIT]->SetbValid(true );
  }
  else
  {
    Serial.println("Sans Bluetooth");
    LstModifParams[IDXBLUETTH ]->SetbValid(false );
    LstModifParams[IDXBLUEINIT]->SetbValid(false );
  }
#endif
  // Par défaut, durées du mode cadencé invalides
  LstModifParams[IDXRECON ]->SetbValid(false); // Recording time in Timed Recording mode in second (1 to 3600 step 10)
  LstModifParams[IDXRECOFF]->SetbValid(false); // Waiting time in Timed Recording mode (1 to 3600 step 10)
  // Autorisation des modes d'enregistrement en fonction de la période CPU et du type d'enregistreur
  if (F_CPU < 144000000)
  {
    ((CEnumModifier *)LstModifParams[IDXMODEF])->SetElemValidity( RECAUTO  , true );
    ((CEnumModifier *)LstModifParams[IDXMODEF])->SetElemValidity( PROTPED  , false);
    ((CEnumModifier *)LstModifParams[IDXMODEF])->SetElemValidity( PROTROUT , false);
    ((CEnumModifier *)LstModifParams[IDXMODEF])->SetElemValidity( PROTFIXE , false);
    ((CEnumModifier *)LstModifParams[IDXMODEF])->SetElemValidity( PRHINOLOG, true );
    ((CEnumModifier *)LstModifParams[IDXMODEF])->SetElemValidity( PTSTMICRO, true );
    ((CEnumModifier *)LstModifParams[IDXMODEF])->SetElemValidity( PHETER   , false);
    ((CEnumModifier *)LstModifParams[IDXMODEF])->SetElemValidity( AUDIOREC , false);
    ((CEnumModifier *)LstModifParams[IDXMODEF])->SetElemValidity( SYNCHRO  , false);
  }
  else
  {
    if (CModeGeneric::GetRecorderType() < PASSIVE_SYNCHRO)
      ((CEnumModifier *)LstModifParams[IDXMODEF])->SetElemValidity( SYNCHRO  , false);
    if (CModeGeneric::GetRecorderType() == PASSIVE_RECORDER or CModeGeneric::GetRecorderType() >= PASSIVE_STEREO)
    {
      ((CEnumModifier *)LstModifParams[IDXMODEF])->SetElemValidity( PHETER  , false);
      ((CEnumModifier *)LstModifParams[IDXMODEF])->SetElemValidity( AUDIOREC, false);
    }
    else
    {
      ((CEnumModifier *)LstModifParams[IDXMODEF])->SetElemValidity( PHETER  , true);
      ((CEnumModifier *)LstModifParams[IDXMODEF])->SetElemValidity( AUDIOREC, true);
    }
    ((CEnumModifier *)LstModifParams[IDXMODEF])->SetElemValidity( RECAUTO  , true );
    ((CEnumModifier *)LstModifParams[IDXMODEF])->SetElemValidity( PROTPED  , true );
    ((CEnumModifier *)LstModifParams[IDXMODEF])->SetElemValidity( PROTROUT , true );
    ((CEnumModifier *)LstModifParams[IDXMODEF])->SetElemValidity( PROTFIXE , true );
    ((CEnumModifier *)LstModifParams[IDXMODEF])->SetElemValidity( PRHINOLOG, true );
    ((CEnumModifier *)LstModifParams[IDXMODEF])->SetElemValidity( PTSTMICRO, true );
  }

  // Durée max d'enregistrement en mode cadencé
  //     Fe    24    48    96   192   250   384   500 kHz
  //   Mono 24:00 12:00 06:00 03:00 02:00 01:30 01:00
  // Stéréo 12:00 06:00 03:00 01:30 01:00 00:45 00:30
  if (CModeGeneric::GetRecorderType() >= PASSIVE_STEREO)
  {
    switch (paramsOperateur.uiFe)
    {
    case FE24KHZ : ((CModificatorInt *)LstModifParams[IDXRECON])->SetMax(3600*12   ); break;
    case FE48KHZ : ((CModificatorInt *)LstModifParams[IDXRECON])->SetMax(3600*6    ); break;
    case FE96KHZ : ((CModificatorInt *)LstModifParams[IDXRECON])->SetMax(3600*3    ); break;
    case FE192KHZ: ((CModificatorInt *)LstModifParams[IDXRECON])->SetMax(3600+30*60); break;
    case FE250KHZ: ((CModificatorInt *)LstModifParams[IDXRECON])->SetMax(3600      ); break;
    case FE384KHZ: ((CModificatorInt *)LstModifParams[IDXRECON])->SetMax(45*60     ); break;
    case FE500KHZ: ((CModificatorInt *)LstModifParams[IDXRECON])->SetMax(30*60     ); break;
    }
  }
  else
  {
    switch (paramsOperateur.uiFe)
    {
    case FE24KHZ : ((CModificatorInt *)LstModifParams[IDXRECON])->SetMax(3600*24   ); break;
    case FE48KHZ : ((CModificatorInt *)LstModifParams[IDXRECON])->SetMax(3600*12   ); break;
    case FE96KHZ : ((CModificatorInt *)LstModifParams[IDXRECON])->SetMax(3600*6    ); break;
    case FE192KHZ: ((CModificatorInt *)LstModifParams[IDXRECON])->SetMax(3600*3    ); break;
    case FE250KHZ: ((CModificatorInt *)LstModifParams[IDXRECON])->SetMax(3600*2    ); break;
    case FE384KHZ: ((CModificatorInt *)LstModifParams[IDXRECON])->SetMax(3600+30*60); break;
    case FE500KHZ: ((CModificatorInt *)LstModifParams[IDXRECON])->SetMax(3600      ); break;
    }
  }
  // Si profil 0 (défaut), seul Débutant est autorisé
  if (commonParams.iSelProfile == 0)
  {
    commonParams.iUserLevel = ULBEGINNER;
    LstModifParams[IDXUSERLEVEL]->SetbValid(false);
    ((CEnumModifier *)LstModifParams[IDXUSERLEVEL])->SetElemValidity( ULEXPERT, false);
    ((CEnumModifier *)LstModifParams[IDXUSERLEVEL])->SetElemValidity( ULDEBUG , false);
  }
  else
  {
    LstModifParams[IDXUSERLEVEL]->SetbValid(true );
    ((CEnumModifier *)LstModifParams[IDXUSERLEVEL])->SetElemValidity( ULEXPERT, true );
    ((CEnumModifier *)LstModifParams[IDXUSERLEVEL])->SetElemValidity( ULDEBUG , true );
  }

  // Si modèle stéréo
  if (CModeGeneric::GetRecorderType() >= PASSIVE_STEREO)
  {
    // Si expert, autorise l'enregistrement mono
    LstModifParams[IDXSMODE ]->SetbValid(commonParams.iUserLevel > ULBEGINNER);
    // Si débutant et profil par défaut, on force le mode stéréo
    if (commonParams.iSelProfile == 0 and paramsOperateur.iStereoRecMode != SMSTEREO)
      paramsOperateur.iStereoRecMode = SMSTEREO;
  }
  else
    LstModifParams[IDXSMODE ]->SetbValid(false);

  // Si mode enregistrement SYNCHRO (uniquement si type >= PASSIVE_STEREO
  if (paramsOperateur.iModeRecord == SYNCHRO)
  {
    LstModifParams[IDXSYNCHRO]->SetbValid(true); // Sélection Maître ou Esclave n
    if (paramsOperateur.iMasterSlave == MASTER)
    {
      // Fréquences d'échantillonnage uniquement 192, 250 et 384kHz
      ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE24KHZ , false);
      ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE48KHZ , false);
      ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE96KHZ , false);
      ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE500KHZ, false);
      // Paramètres spécifiques du mode
      LstModifParams[IDXFRTOP  ]->SetbValid(commonParams.iUserLevel > ULBEGINNER);
      LstModifParams[IDXTOPDUR ]->SetbValid(commonParams.iUserLevel > ULBEGINNER);
      LstModifParams[IDXTOPPER ]->SetbValid(commonParams.iUserLevel > ULBEGINNER);
      LstModifParams[IDXLEDSYNCH]->SetbValid(true);
      // Durée en fonction de la fréquence d'échantillonnage
      switch (paramsOperateur.uiFe)
      {
      case FE192KHZ: ((CEnumModifier *)LstModifParams[IDXTOPDUR])->SetStringArray((const char **)txtDurTop192); break;
      case FE250KHZ: ((CEnumModifier *)LstModifParams[IDXTOPDUR])->SetStringArray((const char **)txtDurTop250); break;
      case FE384KHZ: ((CEnumModifier *)LstModifParams[IDXTOPDUR])->SetStringArray((const char **)txtDurTop384); break;
      }
      // Ensuite, même tests qu'en mode auto
      // En fonction du type de seuil, on valide le bon seuil et on cache l'autre
      if (paramsOperateur.bTypeSeuil)
      {
        LstModifParams[IDXSEUILA]->SetbValid( true);
        LstModifParams[IDXSEUILR]->SetbValid(false);
      }
      else
      {
        LstModifParams[IDXSEUILA]->SetbValid(false);
        LstModifParams[IDXSEUILR]->SetbValid( true);
      }
      LstModifParams[IDXHRECM  ]->SetbValid(false); // Heterodyne recording mode
      LstModifParams[IDXHREFGR ]->SetbValid(false); // Refresh period of the heterodyne graph
      LstModifParams[IDXHETAGC ]->SetbValid(false); // Heterodyne AGC
      LstModifParams[IDXHDEB   ]->SetbValid(true ); // Heure de début
      LstModifParams[IDXHFIN   ]->SetbValid(true ); // Heure de fin
      LstModifParams[IDXFMIN   ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Fréquence d'intéret minimale en Hz (1-FMax)
      LstModifParams[IDXFMAX   ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Fréquence d'intérêt maximale en Hz (FMin-Fe/2)
      LstModifParams[IDXFMINA  ]->SetbValid(false);
      LstModifParams[IDXFMAXA  ]->SetbValid(false);
      LstModifParams[IDXDMIN   ]->SetbValid(true ); // Durée min en secondes (1-10)
      LstModifParams[IDXDMAX   ]->SetbValid(true ); // Durée max en secondes (10-999)
      LstModifParams[IDXFECH   ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Fréquence d'échantillonnage
      LstModifParams[IDXGNUM   ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Gain numérique (6, 12, 18, 24dB)
      LstModifParams[IDXFILT   ]->SetbValid(false); // Filtre linéaire (O/N)
      LstModifParams[IDXMICTYPE]->SetbValid(false); // Type du micro
      LstModifParams[IDXHPFILT ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Filtre passe haut (0-25kHz)
      LstModifParams[IDXFHPFILT]->SetbValid(false);
      LstModifParams[IDXEXP    ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Expansion de temps x10 (O/N)
      LstModifParams[IDXBATCORDER]->SetbValid(false); // Batcorder log and Wave file name type
      LstModifParams[IDXPREF   ]->SetbValid(false); // Préfixe des fichiers Wav (PRec_)
      LstModifParams[IDXNDET   ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Nombre de détections (1-8)
      if (IsTempSensor())
      {
        LstModifParams[IDXPETM]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Période de mesure température
        LstModifParams[IDXTHV] ->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Mesure température en veille
      }
      else
      {
        LstModifParams[IDXPETM]->SetbValid(false); // Période de mesure température
        LstModifParams[IDXTHV] ->SetbValid(false); // Mesure température en veille
      }
      LstModifParams[IDXBDRL  ]->SetbValid(false); // Modification des bandes RhinoLogger
      LstModifParams[IDXAFFRL ]->SetbValid(false); // Affichage permanent ou non en mode RhinoLogger
    }
    else
    {
      // En mode esclave, pas de paramètre visible, y compris la date et l'heure mises à jour par le maître
      LstModifParams[IDXDATE     ]->SetbValid(false);
      LstModifParams[IDXHEURE    ]->SetbValid(false);
      LstModifParams[IDXPROFILE  ]->SetbValid(false);
      LstModifParams[IDXUSERLEVEL]->SetbValid(false);
      LstModifParams[IDXTSEUIL   ]->SetbValid(false);
      LstModifParams[IDXSEUILR   ]->SetbValid(false);
      LstModifParams[IDXSEUILA   ]->SetbValid(false);
      LstModifParams[IDXHRECM    ]->SetbValid(false);
      LstModifParams[IDXHREFGR   ]->SetbValid(false);
      LstModifParams[IDXHETAGC   ]->SetbValid(false);
      LstModifParams[IDXHDEB     ]->SetbValid(false);
      LstModifParams[IDXHFIN     ]->SetbValid(false);
      LstModifParams[IDXRECON    ]->SetbValid(false);
      LstModifParams[IDXRECOFF   ]->SetbValid(false);
      LstModifParams[IDXFMIN     ]->SetbValid(false);
      LstModifParams[IDXFMAX     ]->SetbValid(false);
      LstModifParams[IDXFMINA    ]->SetbValid(false);
      LstModifParams[IDXFMAXA    ]->SetbValid(false);
      LstModifParams[IDXDMIN     ]->SetbValid(false);
      LstModifParams[IDXDMAX     ]->SetbValid(false);
      LstModifParams[IDXFECH     ]->SetbValid(false);
      LstModifParams[IDXFECHA    ]->SetbValid(false);
      LstModifParams[IDXSMODE    ]->SetbValid(false);
      LstModifParams[IDXGNUM     ]->SetbValid(false);
      LstModifParams[IDXFILT     ]->SetbValid(false);
      LstModifParams[IDXHPFILT   ]->SetbValid(false);
      LstModifParams[IDXFHPFILT  ]->SetbValid(false);
      LstModifParams[IDXMICTYPE  ]->SetbValid(false);
      LstModifParams[IDXEXP      ]->SetbValid(false);
      LstModifParams[IDXBATCORDER]->SetbValid(false);
      LstModifParams[IDXPREF     ]->SetbValid(false);
      LstModifParams[IDXNDET     ]->SetbValid(false);
      LstModifParams[IDXPETM     ]->SetbValid(false);
      LstModifParams[IDXTHV      ]->SetbValid(false);
      LstModifParams[IDXBDRL     ]->SetbValid(false);
      LstModifParams[IDXAFFRL    ]->SetbValid(false);
      LstModifParams[IDXFRTOP    ]->SetbValid(false);
      LstModifParams[IDXTOPDUR   ]->SetbValid(false);
      LstModifParams[IDXTOPPER   ]->SetbValid(false);
      LstModifParams[IDXLEDSYNCH ]->SetbValid(false);
      LstModifParams[IDXDEF      ]->SetbValid(false);
      LstModifParams[IDXPROFILES ]->SetbValid(false);
      LstModifParams[IDXSNOISE   ]->SetbValid(false);
      LstModifParams[IDXLED      ]->SetbValid(false);
    }
  }
  // Si mode enregistrement RECAUTO
  else if (paramsOperateur.iModeRecord == RECAUTO)
  {
    LstModifParams[IDXSYNCHRO]->SetbValid(false);
    LstModifParams[IDXFRTOP  ]->SetbValid(false);
    LstModifParams[IDXTOPDUR ]->SetbValid(false);
    LstModifParams[IDXTOPPER ]->SetbValid(false);
    LstModifParams[IDXLEDSYNCH]->SetbValid(false);
    LstModifParams[IDXTSEUIL]->SetbValid(commonParams.iUserLevel > ULBEGINNER); // Type de seuil (false relatif, true absolu)
    // En fonction du type de seuil, on valide le bon seuil et on cache l'autre
    if (paramsOperateur.bTypeSeuil)
    {
      LstModifParams[IDXSEUILA]->SetbValid( true);
      LstModifParams[IDXSEUILR]->SetbValid(false);
    }
    else
    {
      LstModifParams[IDXSEUILA]->SetbValid(false);
      LstModifParams[IDXSEUILR]->SetbValid( true);
    }
    LstModifParams[IDXHRECM ]->SetbValid(false); // Heterodyne recording mode
    LstModifParams[IDXHREFGR]->SetbValid(false); // Refresh period of the heterodyne graph
    LstModifParams[IDXHDEB  ]->SetbValid(true ); // Heure de début
    LstModifParams[IDXHFIN  ]->SetbValid(true ); // Heure de fin
    if (paramsOperateur.uiFe >= FE192KHZ)
    {
      LstModifParams[IDXFMIN  ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Fréquence d'intéret minimale en Hz (1-FMax)
      LstModifParams[IDXFMAX  ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Fréquence d'intérêt maximale en Hz (FMin-Fe/2)
      LstModifParams[IDXFMINA ]->SetbValid(false);
      LstModifParams[IDXFMAXA ]->SetbValid(false);
    }
    else
    {
      LstModifParams[IDXFMIN  ]->SetbValid(false); // Fréquence d'intéret minimale en Hz (1-FMax)
      LstModifParams[IDXFMAX  ]->SetbValid(false); // Fréquence d'intérêt maximale en Hz (FMin-Fe/2)
      LstModifParams[IDXFMINA ]->SetbValid(commonParams.iUserLevel > ULBEGINNER);
      LstModifParams[IDXFMAXA ]->SetbValid(commonParams.iUserLevel > ULBEGINNER);
    }
    LstModifParams[IDXDMIN  ]->SetbValid(true ); // Durée min en secondes (1-10)
    LstModifParams[IDXDMAX  ]->SetbValid(true ); // Durée max en secondes (10-999)
    LstModifParams[IDXFECH  ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Fréquence d'échantillonnage
    //LstModifParams[IDXFECH  ]->SetbValid(true ); // Fréquence d'échantillonnage
    LstModifParams[IDXGNUM  ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Gain numérique (6, 12, 18, 24dB)
    if (F_CPU < 144000000 or paramsOperateur.uiFe == FE500KHZ or paramsOperateur.uiFe < FE192KHZ or CModeGeneric::GetRecorderType() >= PASSIVE_STEREO)
    {
       LstModifParams[IDXFILT   ]->SetbValid(false); // Filtre linéaire (O/N)
      LstModifParams[IDXMICTYPE]->SetbValid(false); // Type du micro
    }
    else
    {
      LstModifParams[IDXFILT   ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Filtre passe bas (O/N)
      LstModifParams[IDXMICTYPE]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Type du micro
    }
    if (paramsOperateur.uiFe >= FE192KHZ) // Filtre passe haut (0-25kHz)
    {
      LstModifParams[IDXHPFILT ]->SetbValid(commonParams.iUserLevel > ULBEGINNER);
      LstModifParams[IDXFHPFILT]->SetbValid(false);
    }
    else
    {
      LstModifParams[IDXHPFILT ]->SetbValid(false);
      LstModifParams[IDXFHPFILT]->SetbValid(commonParams.iUserLevel > ULBEGINNER);
    }
    LstModifParams[IDXEXP   ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Expansion de temps x10 (O/N)
    LstModifParams[IDXBATCORDER]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Batcorder log and Wave file name type
    LstModifParams[IDXPREF  ]->SetbValid(true ); // Préfixe des fichiers Wav (PRec_)
    LstModifParams[IDXNDET  ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Nombre de détections (1-8)
    if (IsTempSensor())
    {
      LstModifParams[IDXPETM]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Période de mesure température
      LstModifParams[IDXTHV] ->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Mesure température en veille
    }
    else
    {
      LstModifParams[IDXPETM]->SetbValid(false); // Période de mesure température
      LstModifParams[IDXTHV] ->SetbValid(false); // Mesure température en veille
    }
    LstModifParams[IDXBDRL  ]->SetbValid(false); // Modification des bandes RhinoLogger
    LstModifParams[IDXAFFRL ]->SetbValid(false); // Affichage permanent ou non en mode RhinoLogger
    ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE24KHZ , true);
    ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE48KHZ , true);
    ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE96KHZ , true);
    ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE192KHZ, true);
    ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE250KHZ, true);
    if (F_CPU < 144000000)
    {
      ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE384KHZ, false);
      ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE500KHZ, false);
    }
    else
    {
      ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE384KHZ, true);
      if (CModeGeneric::GetRecorderType() >= PASSIVE_STEREO and paramsOperateur.iStereoRecMode == SMSTEREO)
        ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE500KHZ, false);
      else
        ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE500KHZ, true);
    }
    // Filtre linéaire ou passe bas en fonction de la fréquence d'échantillonnage
    switch (paramsOperateur.uiFe)
    {
    case FE24KHZ : LstModifParams[IDXFILT]->SetFormat(txtIDXFILT24 ); break;
    case FE48KHZ : LstModifParams[IDXFILT]->SetFormat(txtIDXFILT48 ); break;
    case FE96KHZ : LstModifParams[IDXFILT]->SetFormat(txtIDXFILT96 ); break;
    case FE192KHZ: LstModifParams[IDXFILT]->SetFormat(txtIDXFILT192); break;
    default      : LstModifParams[IDXFILT]->SetFormat(txtIDXFILTL  ); break;
    }
  }
  // Si mode enregistrement hétérodyne
  else if (paramsOperateur.iModeRecord == PHETER)
  {
    LstModifParams[IDXSYNCHRO ]->SetbValid(false);
    LstModifParams[IDXFRTOP   ]->SetbValid(false);
    LstModifParams[IDXTOPDUR  ]->SetbValid(false);
    LstModifParams[IDXTOPPER  ]->SetbValid(false);
    LstModifParams[IDXLEDSYNCH]->SetbValid(false);
    LstModifParams[IDXHAUTOMAN]->SetbValid(true);
    LstModifParams[IDXAUTOPLAY]->SetbValid(true);
    LstModifParams[IDXHGRAPH  ]->SetbValid(true);
    LstModifParams[IDXHETAGC  ]->SetbValid(true);
#if defined(__IMXRT1062__) // Teensy 4.1
#ifdef SELFILTERH10KHZ
    LstModifParams[IDXHSELF   ]->SetbValid(true);
#endif
#ifdef SELFILTERH20KHZ
    LstModifParams[IDXHSELF   ]->SetbValid(true);
#endif
#endif
    LstModifParams[IDXTSEUIL]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Type de seuil (false relatif, true absolu)
    // En fonctiondu type de seuil, on valide le bon seuil et on cache l'autre
    if (paramsOperateur.bTypeSeuil)
    {
      LstModifParams[IDXSEUILA]->SetbValid( commonParams.iUserLevel > ULBEGINNER);
      LstModifParams[IDXSEUILR]->SetbValid(false);
    }
    else
    {
      LstModifParams[IDXSEUILA]->SetbValid(false);
      LstModifParams[IDXSEUILR]->SetbValid( commonParams.iUserLevel > ULBEGINNER);
    }
    // 500kHz non autorisée en hétérodyne
    ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE500KHZ, false);
    if (F_CPU < 179000000)
      ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE384KHZ, false);
    else
      ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE384KHZ, true);
    LstModifParams[IDXHRECM ]->SetbValid(false ); // Heterodyne recording mode
    LstModifParams[IDXHREFGR]->SetbValid(false ); // Refresh period of the heterodyne graph
    LstModifParams[IDXHDEB  ]->SetbValid(false); // Heure de début
    LstModifParams[IDXHFIN  ]->SetbValid(false); // Heure de fin
    LstModifParams[IDXFMIN  ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Fréquence d'intéret minimale en Hz (1-FMax)
    LstModifParams[IDXFMAX  ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Fréquence d'intérêt maximale en Hz (FMin-Fe/2)
    LstModifParams[IDXDMIN  ]->SetbValid(true ); // Durée min en secondes (1-10)
    LstModifParams[IDXDMAX  ]->SetbValid(true ); // Durée max en secondes (10-999)
    LstModifParams[IDXFECH  ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Fréquence d'échantillonnage
    LstModifParams[IDXGNUM  ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Gain numérique (6, 12, 18, 24dB)
    LstModifParams[IDXFILT  ]->SetbValid(false); // Filtre linéaire (O/N)
    if (paramsOperateur.uiFe >= FE192KHZ) // Filtre passe haut (0-25kHz)
    {
      LstModifParams[IDXHPFILT ]->SetbValid(commonParams.iUserLevel > ULBEGINNER);
      LstModifParams[IDXFHPFILT]->SetbValid(false);
    }
    else
    {
      LstModifParams[IDXHPFILT ]->SetbValid(false);
      LstModifParams[IDXFHPFILT]->SetbValid(commonParams.iUserLevel > ULBEGINNER);
    }
    LstModifParams[IDXMICTYPE]->SetbValid(false); // Type de micro
    LstModifParams[IDXEXP   ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Expansion de temps x10 (O/N)
    LstModifParams[IDXBATCORDER]->SetbValid(false ); // Batcorder log and Wave file name type
    LstModifParams[IDXPREF  ]->SetbValid(true ); // Préfixe des fichiers Wav (PRec_)
    LstModifParams[IDXNDET  ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Nombre de détections (1-8)
    if (IsTempSensor())
    {
      LstModifParams[IDXPETM]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Période de mesure température
      LstModifParams[IDXTHV] ->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Mesure température en veille
    }
    else
    {
      LstModifParams[IDXPETM]->SetbValid(false); // Période de mesure température
      LstModifParams[IDXTHV] ->SetbValid(false); // Mesure température en veille
    }
    LstModifParams[IDXBDRL  ]->SetbValid(false); // Modification des bandes RhinoLogger
    LstModifParams[IDXAFFRL ]->SetbValid(false); // Affichage permanent ou non en mode RhinoLogger
    ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE24KHZ , false);
    ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE48KHZ , false);
    ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE96KHZ , false);
    ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE192KHZ, false);
    ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE250KHZ, true );
    if (F_CPU < 180000000)
    {
      ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE384KHZ, false);
      ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE500KHZ, false);
    }
    else
    {
      ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE500KHZ, true );
      ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE384KHZ, true );
    }
  }
  // Si mode enregistrement audio
  else if (paramsOperateur.iModeRecord == AUDIOREC)
  {
    LstModifParams[IDXSYNCHRO ]->SetbValid(false);
    LstModifParams[IDXFRTOP   ]->SetbValid(false);
    LstModifParams[IDXTOPDUR  ]->SetbValid(false);
    LstModifParams[IDXTOPPER  ]->SetbValid(false);
    LstModifParams[IDXLEDSYNCH]->SetbValid(false);
    LstModifParams[IDXFMIN    ]->SetbValid(false);
    LstModifParams[IDXFMAX    ]->SetbValid(false);
    LstModifParams[IDXFMINA   ]->SetbValid(commonParams.iUserLevel > ULBEGINNER );
    LstModifParams[IDXFMAXA   ]->SetbValid(commonParams.iUserLevel > ULBEGINNER );
    LstModifParams[IDXFECH    ]->SetbValid(false);
    LstModifParams[IDXFECHA   ]->SetbValid(commonParams.iUserLevel > ULBEGINNER );
    LstModifParams[IDXTSEUIL  ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Type de seuil (false relatif, true absolu)
    // En fonctiondu type de seuil, on valide le bon seuil et on cache l'autre
    if (paramsOperateur.bTypeSeuil)
    {
      LstModifParams[IDXSEUILA]->SetbValid( commonParams.iUserLevel > ULBEGINNER);
      LstModifParams[IDXSEUILR]->SetbValid(false);
    }
    else
    {
      LstModifParams[IDXSEUILA]->SetbValid(false);
      LstModifParams[IDXSEUILR]->SetbValid( commonParams.iUserLevel > ULBEGINNER);
    }
    LstModifParams[IDXHDEB  ]->SetbValid(false); // Heure de début
    LstModifParams[IDXHFIN  ]->SetbValid(false); // Heure de fin
    LstModifParams[IDXDMIN  ]->SetbValid(true ); // Durée min en secondes (1-10)
    LstModifParams[IDXDMAX  ]->SetbValid(true ); // Durée max en secondes (10-999)
    LstModifParams[IDXGNUM  ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Gain numérique (6, 12, 18, 24dB)
    if (F_CPU < 144000000 or paramsOperateur.uiFeA == FE500KHZ or paramsOperateur.uiFeA < FE192KHZ or CModeGeneric::GetRecorderType() >= PASSIVE_STEREO)
    {
      LstModifParams[IDXFILT   ]->SetbValid(false); // Filtre linéaire (O/N)
      LstModifParams[IDXMICTYPE]->SetbValid(false); // Type du micro
    }
    else
    {
      LstModifParams[IDXFILT   ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Filtre passe bas (O/N)
      LstModifParams[IDXMICTYPE]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Type du micro
    }
    if (paramsOperateur.uiFe >= FE192KHZ) // Filtre passe haut (0-25kHz)
    {
      LstModifParams[IDXHPFILT ]->SetbValid(commonParams.iUserLevel > ULBEGINNER);
      LstModifParams[IDXFHPFILT]->SetbValid(false);
    }
    else
    {
      LstModifParams[IDXHPFILT ]->SetbValid(false);
      LstModifParams[IDXFHPFILT]->SetbValid(commonParams.iUserLevel > ULBEGINNER);
    }
    LstModifParams[IDXMICTYPE]->SetbValid(false); // Type de micro
    LstModifParams[IDXEXP   ]->SetbValid(false); // Expansion de temps x10 (O/N)
    LstModifParams[IDXBATCORDER]->SetbValid(false ); // Batcorder log and Wave file name type
    LstModifParams[IDXPREF  ]->SetbValid(true ); // Préfixe des fichiers Wav (PRec_)
    LstModifParams[IDXNDET  ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Nombre de détections (1-8)
    if (IsTempSensor())
    {
      LstModifParams[IDXPETM]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Période de mesure température
      LstModifParams[IDXTHV] ->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Mesure température en veille
    }
    else
    {
      LstModifParams[IDXPETM]->SetbValid(false); // Période de mesure température
      LstModifParams[IDXTHV] ->SetbValid(false); // Mesure température en veille
    }
    LstModifParams[IDXBDRL  ]->SetbValid(false); // Modification des bandes RhinoLogger
    LstModifParams[IDXAFFRL ]->SetbValid(false); // Affichage permanent ou non en mode RhinoLogger
  }
  // Si mode enregistrement un des protocoles
  else if (paramsOperateur.iModeRecord >= PROTPED and paramsOperateur.iModeRecord <= PROTFIXE)
  {
    LstModifParams[IDXSYNCHRO]->SetbValid(false);
    LstModifParams[IDXFRTOP  ]->SetbValid(false);
    LstModifParams[IDXTOPDUR ]->SetbValid(false);
    LstModifParams[IDXTOPPER ]->SetbValid(false);
    LstModifParams[IDXLEDSYNCH]->SetbValid(false);
    LstModifParams[IDXTSEUIL]->SetbValid(false); // Type de seuil (false relatif, true absolu)
    LstModifParams[IDXSEUILA]->SetbValid(false);
    LstModifParams[IDXSEUILR]->SetbValid(false);
    LstModifParams[IDXHRECM ]->SetbValid(false); // Heterodyne recording mode
    LstModifParams[IDXHREFGR]->SetbValid(false); // Refresh period of the heterodyne graph
    if (paramsOperateur.iModeRecord == PROTFIXE)
    {
      LstModifParams[IDXHDEB  ]->SetbValid(true ); // Heure de début
      LstModifParams[IDXHFIN  ]->SetbValid(true ); // Heure de fin
    }
    else
    {
      LstModifParams[IDXHDEB  ]->SetbValid(false); // Heure de début
      LstModifParams[IDXHFIN  ]->SetbValid(false); // Heure de fin
    }
    LstModifParams[IDXFMIN  ]->SetbValid(false); // Fréquence d'intéret minimale en Hz (1-FMax)
    LstModifParams[IDXFMAX  ]->SetbValid(false); // Fréquence d'intérêt maximale en Hz (FMin-Fe/2)
    LstModifParams[IDXDMIN  ]->SetbValid(false); // Durée min en secondes (1-10)
    LstModifParams[IDXDMAX  ]->SetbValid(false); // Durée max en secondes (10-999)
    LstModifParams[IDXFECH  ]->SetbValid(false); // Fréquence d'échantillonnage
    LstModifParams[IDXGNUM  ]->SetbValid(false); // Gain numérique (6, 12, 18, 24dB)
    LstModifParams[IDXFILT  ]->SetbValid(false); // Filtre linéaire (O/N)
    LstModifParams[IDXHPFILT]->SetbValid(false); // Filtre passe haut (0-25kHz)
    LstModifParams[IDXFHPFILT]->SetbValid(false); // Filtre passe haut (0-25kHz)
    LstModifParams[IDXMICTYPE]->SetbValid(false); // Type de micro
    LstModifParams[IDXEXP   ]->SetbValid(false); // Expansion de temps x10 (O/N)
    LstModifParams[IDXBATCORDER]->SetbValid(false ); // Batcorder log and Wave file name type
    LstModifParams[IDXPREF  ]->SetbValid(false); // Préfixe des fichiers Wav (PRec_)
    LstModifParams[IDXNDET  ]->SetbValid(false); // Nombre de détections (1-8)
    if (IsTempSensor())
    {
      LstModifParams[IDXPETM]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Période de mesure température
      LstModifParams[IDXTHV] ->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Mesure température en veille
    }
    else
    {
      LstModifParams[IDXPETM]->SetbValid(false); // Période de mesure température
      LstModifParams[IDXTHV] ->SetbValid(false); // Mesure température en veille
    }
    LstModifParams[IDXBDRL  ]->SetbValid(false); // Modification des bandes RhinoLogger
    LstModifParams[IDXAFFRL ]->SetbValid(false); // Affichage permanent ou non en mode RhinoLogger
  }
  // Si mode enregistrement Test micro
  else if (paramsOperateur.iModeRecord == PTSTMICRO)
  {
    LstModifParams[IDXSYNCHRO]->SetbValid(false);
    LstModifParams[IDXFRTOP  ]->SetbValid(false);
    LstModifParams[IDXTOPDUR ]->SetbValid(false);
    LstModifParams[IDXTOPPER ]->SetbValid(false);
    LstModifParams[IDXLEDSYNCH]->SetbValid(false);
    LstModifParams[IDXTSEUIL]->SetbValid(false); // Type de seuil (false relatif, true absolu)
    LstModifParams[IDXSEUILA]->SetbValid(false);
    LstModifParams[IDXSEUILR]->SetbValid(false);
    LstModifParams[IDXHRECM ]->SetbValid(false); // Heterodyne recording mode
    LstModifParams[IDXHREFGR]->SetbValid(false); // Refresh period of the heterodyne graph
    LstModifParams[IDXHDEB  ]->SetbValid(false); // Heure de début
    LstModifParams[IDXHFIN  ]->SetbValid(false); // Heure de fin
    LstModifParams[IDXFMIN  ]->SetbValid(false); // Fréquence d'intéret minimale en Hz (1-FMax)
    LstModifParams[IDXFMAX  ]->SetbValid(false); // Fréquence d'intérêt maximale en Hz (FMin-Fe/2)
    LstModifParams[IDXDMIN  ]->SetbValid(false); // Durée min en secondes (1-10)
    LstModifParams[IDXDMAX  ]->SetbValid(false); // Durée max en secondes (10-999)
    LstModifParams[IDXFECH  ]->SetbValid(false); // Fréquence d'échantillonnage
    LstModifParams[IDXGNUM  ]->SetbValid(false); // Gain numérique (6, 12, 18, 24dB)
    LstModifParams[IDXFILT  ]->SetbValid(false); // Filtre linéaire (O/N)
    LstModifParams[IDXHPFILT]->SetbValid(false); // Filtre passe haut (0-25kHz)
    LstModifParams[IDXFHPFILT]->SetbValid(false); // Filtre passe haut (0-25kHz)
    LstModifParams[IDXMICTYPE]->SetbValid(false); // Type de micro
    LstModifParams[IDXEXP   ]->SetbValid(false); // Expansion de temps x10 (O/N)
    LstModifParams[IDXBATCORDER]->SetbValid(false ); // Batcorder log and Wave file name type
    LstModifParams[IDXPREF  ]->SetbValid(false); // Préfixe des fichiers Wav (PRec_)
    LstModifParams[IDXNDET  ]->SetbValid(false); // Nombre de détections (1-8)
    LstModifParams[IDXPETM  ]->SetbValid(false); // Période de mesure température
    LstModifParams[IDXTHV   ]->SetbValid(false); // Mesure température en veille
    LstModifParams[IDXBDRL  ]->SetbValid(false); // Modification des bandes RhinoLogger
    LstModifParams[IDXAFFRL ]->SetbValid(false); // Affichage permanent ou non en mode RhinoLogger
  }
  // Si mode enregistrement RhinoLogger
  else if (paramsOperateur.iModeRecord == PRHINOLOG)
  {
    //Serial.println("Coherenceparams Mode RhinoLogger");
    LstModifParams[IDXSYNCHRO]->SetbValid(false);
    LstModifParams[IDXFRTOP  ]->SetbValid(false);
    LstModifParams[IDXTOPDUR ]->SetbValid(false);
    LstModifParams[IDXTOPPER ]->SetbValid(false);
    LstModifParams[IDXLEDSYNCH]->SetbValid(false);
    LstModifParams[IDXTSEUIL]->SetbValid(false); // Type de seuil (false relatif, true absolu)
    LstModifParams[IDXSEUILR]->SetbValid(true ); // Seuil de détection absolu en dB (-110/-30)
    LstModifParams[IDXSEUILA]->SetbValid(false); // Seuil de détection absolu en dB (-110/-30)
    LstModifParams[IDXHRECM ]->SetbValid(false); // Heterodyne recording mode
    LstModifParams[IDXHREFGR]->SetbValid(false); // Refresh period of the heterodyne graph
    LstModifParams[IDXHDEB  ]->SetbValid(true ); // Heure de début
    LstModifParams[IDXHFIN  ]->SetbValid(true ); // Heure de fin
    LstModifParams[IDXFMIN  ]->SetbValid(false); // Fréquence d'intéret minimale en Hz (1-FMax)
    LstModifParams[IDXFMAX  ]->SetbValid(false); // Fréquence d'intérêt maximale en Hz (FMin-Fe/2)
    LstModifParams[IDXDMIN  ]->SetbValid(false); // Durée min en secondes (1-10)
    LstModifParams[IDXDMAX  ]->SetbValid(false); // Durée max en secondes (10-999)
    LstModifParams[IDXFECH  ]->SetbValid(false); // Fréquence d'échantillonnage
    LstModifParams[IDXSMODE ]->SetbValid(false); // For PRS, stereo recording mode
    LstModifParams[IDXGNUM  ]->SetbValid(false); // Gain numérique (6, 12, 18, 24dB)
    LstModifParams[IDXFILT  ]->SetbValid(false); // Filtre (O/N)
    LstModifParams[IDXHPFILT]->SetbValid(false); // Filtre passe haut (0-25kHz)
    LstModifParams[IDXFHPFILT]->SetbValid(false); // Filtre passe haut (0-25kHz)
    LstModifParams[IDXMICTYPE]->SetbValid(false); // Type de micro
    LstModifParams[IDXEXP   ]->SetbValid(false); // Expansion de temps x10 (O/N)
    LstModifParams[IDXBATCORDER]->SetbValid(false ); // Batcorder log and Wave file name type
    LstModifParams[IDXPREF  ]->SetbValid(false); // Préfixe des fichiers Wav (PRec_)
    LstModifParams[IDXNDET  ]->SetbValid(false); // Nombre de détections (1-8)
    LstModifParams[IDXPETM  ]->SetbValid(false); // Période de mesure température
    LstModifParams[IDXTHV   ]->SetbValid(false); // Mesure température en veille
    LstModifParams[IDXBDRL  ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Modification des bandes RhinoLogger
    LstModifParams[IDXAFFRL ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Affichage permanent ou non en mode RhinoLogger
  }
  // Si mode enregistrement cadencé
  else if (paramsOperateur.iModeRecord == TIMEDREC)
  {
    LstModifParams[IDXSYNCHRO]->SetbValid(false);
    LstModifParams[IDXFRTOP  ]->SetbValid(false);
    LstModifParams[IDXTOPDUR ]->SetbValid(false);
    LstModifParams[IDXTOPPER ]->SetbValid(false);
    LstModifParams[IDXLEDSYNCH]->SetbValid(false);
    LstModifParams[IDXTSEUIL]->SetbValid(false); // Type de seuil (false relatif, true absolu)
    LstModifParams[IDXSEUILA]->SetbValid(false);
    LstModifParams[IDXSEUILR]->SetbValid(false);
    LstModifParams[IDXHRECM ]->SetbValid(false); // Heterodyne recording mode
    LstModifParams[IDXHREFGR]->SetbValid(false); // Refresh period of the heterodyne graph
    LstModifParams[IDXHDEB  ]->SetbValid(true ); // Heure de début
    LstModifParams[IDXHFIN  ]->SetbValid(true ); // Heure de fin
    LstModifParams[IDXFMIN  ]->SetbValid(false); // Fréquence d'intéret minimale en Hz (1-FMax)
    LstModifParams[IDXFMAX  ]->SetbValid(false); // Fréquence d'intérêt maximale en Hz (FMin-Fe/2)
    LstModifParams[IDXRECON ]->SetbValid(true ); // Recording time in Timed Recording mode in second (1 to 3600 step 10)
    LstModifParams[IDXRECOFF]->SetbValid(true ); // Waiting time in Timed Recording mode (1 to 3600 step 10)
    LstModifParams[IDXDMIN  ]->SetbValid(false); // Durée min en secondes (1-10)
    LstModifParams[IDXDMAX  ]->SetbValid(false); // Durée max en secondes (10-999)
    LstModifParams[IDXFECH  ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Fréquence d'échantillonnage
    LstModifParams[IDXGNUM  ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Gain numérique (6, 12, 18, 24dB)
    if (F_CPU < 144000000 or paramsOperateur.uiFe == FE500KHZ or paramsOperateur.uiFe < FE192KHZ or CModeGeneric::GetRecorderType() >= PASSIVE_STEREO)
    {
      LstModifParams[IDXFILT   ]->SetbValid(false); // Filtre linéaire (O/N)
      LstModifParams[IDXMICTYPE]->SetbValid(false); // Type du micro
    }
    else
    {
      LstModifParams[IDXFILT   ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Filtre passe bas (O/N)
      LstModifParams[IDXMICTYPE]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Type du micro
    }
    if (paramsOperateur.uiFe >= FE192KHZ) // Filtre passe haut (0-25kHz)
    {
      LstModifParams[IDXHPFILT ]->SetbValid(commonParams.iUserLevel > ULBEGINNER);
      LstModifParams[IDXFHPFILT]->SetbValid(false);
    }
    else
    {
      LstModifParams[IDXHPFILT ]->SetbValid(false);
      LstModifParams[IDXFHPFILT]->SetbValid(commonParams.iUserLevel > ULBEGINNER);
    }
    LstModifParams[IDXEXP   ]->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Expansion de temps x10 (O/N)
    LstModifParams[IDXBATCORDER]->SetbValid(false ); // Batcorder log and Wave file name type
    LstModifParams[IDXPREF  ]->SetbValid(true ); // Préfixe des fichiers Wav (PRec_)
    LstModifParams[IDXNDET  ]->SetbValid(false); // Nombre de détections (1-8)
    if (IsTempSensor())
    {
      LstModifParams[IDXPETM]->SetbValid(true ); // Période de mesure température
      LstModifParams[IDXTHV] ->SetbValid(commonParams.iUserLevel > ULBEGINNER ); // Mesure température en veille
    }
    else
    {
      LstModifParams[IDXPETM]->SetbValid(false); // Période de mesure température
      LstModifParams[IDXTHV] ->SetbValid(false); // Mesure température en veille
    }
    LstModifParams[IDXBDRL  ]->SetbValid(false); // Modification des bandes RhinoLogger
    LstModifParams[IDXAFFRL ]->SetbValid(false); // Affichage permanent ou non en mode RhinoLogger
    ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE24KHZ , true);
    ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE48KHZ , true);
    ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE96KHZ , true);
    ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE192KHZ, true);
    ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE250KHZ, true);
    if (F_CPU < 144000000)
    {
      ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE384KHZ, false);
      ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE500KHZ, false);
    }
    else
    {
      ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE384KHZ, true);
      if (CModeGeneric::GetRecorderType() >= PASSIVE_STEREO and paramsOperateur.iStereoRecMode == SMSTEREO)
        ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE500KHZ, false);
      else
        ((CEnumModifier *)LstModifParams[IDXFECH])->SetElemValidity( FE500KHZ, true);
    }
    // Filtre linéaire ou passe bas en fonction de la fréquence d'échantillonnage
    switch (paramsOperateur.uiFe)
    {
    case FE24KHZ : LstModifParams[IDXFILT]->SetFormat(txtIDXFILT24 ); break;
    case FE48KHZ : LstModifParams[IDXFILT]->SetFormat(txtIDXFILT48 ); break;
    case FE96KHZ : LstModifParams[IDXFILT]->SetFormat(txtIDXFILT96 ); break;
    case FE192KHZ: LstModifParams[IDXFILT]->SetFormat(txtIDXFILT192); break;
    default      : LstModifParams[IDXFILT]->SetFormat(txtIDXFILTL  ); break;
    }
  }
  if (CModeGeneric::GetRecorderType() != ACTIVE_RECORDER)
    LstModifParams[IDXBOOT  ]->SetbValid(false); // Goto Bootloader to load a new version in Active Recorder
  else
    LstModifParams[IDXEXT   ]->SetbValid(false); // No external batterie in Active Recorder
  LstModifParams[IDXPROFILES]->SetbValid(true ); // Modification of operator profiles
  if (commonParams.iUserLevel == ULDEBUG)
  {
    LstModifParams[IDXSNOISE]->SetbValid(true ); // Save noise file (Y/N)
    LstModifParams[IDXLED   ]->SetbValid(true ); // Using the LED (No, Recordin., Acquisit., IS DMA, Tr. Loop, Cal Noise, Det. Act., Buf. Los.)
  }
  else
  {
    LstModifParams[IDXSNOISE]->SetbValid(false); // Save noise file (Y/N)
    LstModifParams[IDXLED   ]->SetbValid(false); // Using the LED (No, Recordin., Acquisit., IS DMA, Tr. Loop, Cal Noise, Det. Act., Buf. Los.)
  }
#if defined(__MK66FX1M0__) // Teensy 3.6
  LstModifParams[IDXPRETRIGAUT]->SetbValid(false);
  LstModifParams[IDXPRETRIGHET]->SetbValid(false);
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  // Autorisation de la durée du pré-trigger
  if (external_psram_size > 0 and (paramsOperateur.iModeRecord == RECAUTO
      or paramsOperateur.iModeRecord == AUDIOREC or paramsOperateur.iModeRecord == SYNCHRO))
  {
    // Pre-trigger for autoatic mode
    LstModifParams[IDXPRETRIGAUT]->SetbValid(true);
    LstModifParams[IDXPRETRIGHET]->SetbValid(false);
  }
  else if (external_psram_size > 0 and paramsOperateur.iModeRecord == PHETER)
  {
    // Pre-trigger for heterodyne mode
    LstModifParams[IDXPRETRIGAUT]->SetbValid(false);
    LstModifParams[IDXPRETRIGHET]->SetbValid(true);
  }
  else
  {
    LstModifParams[IDXPRETRIGAUT]->SetbValid(false);
    LstModifParams[IDXPRETRIGHET]->SetbValid(false);
  }
#endif
}
  
//-------------------------------------------------------------------------
// Mesure de la batterie
void CModeParams::CheckBatterie()
{
  char temp[80];
  // Mesure des batteries
  CModeGeneric::CheckBatteries(MAX_WAITMESURE);
  // Init du paramètre batterie interne
  if (fNivBatLiPo < 2.9)
    sprintf( FormatBatInt, txtIDXBAT[commonParams.iLanguage], "--");
  else
  {
    sprintf( temp, "%d%% %3.1lfV", iNivBatLiPo, fNivBatLiPo);
    sprintf( FormatBatInt, txtIDXBAT[commonParams.iLanguage], temp);
  }
  ((CPushButtonModifier *)LstModifParams[IDXBAT])->SetString( FormatBatInt);
  // Init du paramètre batterie externe
  if (fNivBatExt < 10.0)
    sprintf( FormatBatExt, txtIDXEXT[commonParams.iLanguage], "--");
  else
  {
    if (iNivBatExt == 100)
      sprintf( temp, "%d%%%4.1lfV", iNivBatExt, fNivBatExt);
    else
      sprintf( temp, "%d%% %4.1lfV", iNivBatExt, fNivBatExt);
    sprintf( FormatBatExt, txtIDXEXT[commonParams.iLanguage], temp);
  }
//sprintf(FormatBatExt, " MAX Version %X", max17043.getVersion()); //$$$$$$$$$$$$$
  ((CPushButtonModifier *)LstModifParams[IDXEXT])->SetString( FormatBatExt);
}

//-------------------------------------------------------------------------
// Possible management of fund tasks
void CModeParams::OnLoop()
{
#ifdef USBSDCARD
  mtpd.loop();
#endif
}

//-------------------------------------------------------------------------
//! \class CModeModifProfiles
//! \brief Modification of operator profiles

const char formIDXMPRETURN[]      =  " &$$ ";
const char formIDXMPPROF[]        =  " Profil 1 &&&&&&&&&&&";
extern const char *txtIDXMPRETURN[];
extern const char *txtIDXMPWRITE[];
extern const char *txtIDXMPREAD[];
extern const char *txtIDXMPPROF1[];
extern const char *txtIDXMPPROF2[];
extern const char *txtIDXMPPROF3[];
extern const char *txtIDXMPPROF4[];
extern const char *txtErrorRProfiles[];

//-------------------------------------------------------------------------
//! \brief Constructeur
CModeModifProfiles::CModeModifProfiles()
{
  // Create lines
  // IDXMPRETURN Return to parameters mode
  LstModifParams.push_back(new CPushButtonModifier ( formIDXMPRETURN, true, txtIDXMPRETURN, 0, 0));
  // IDXMPPROF1  Changing the name of profile 1
  LstModifParams.push_back(new CCharModifier ( formIDXMPPROF, true, txtIDXMPPROF1, (char *)&ProfilesNames[1], MAXCHARPROFILE, false, true, false, true, true, 0, 0));
  // IDXMPPROF2  Changing the name of profile 2
  LstModifParams.push_back(new CCharModifier ( formIDXMPPROF, true, txtIDXMPPROF2, (char *)&ProfilesNames[2], MAXCHARPROFILE, false, true, false, true, true, 0, 0));
  // IDXMPPROF3  Changing the name of profile 3
  LstModifParams.push_back(new CCharModifier ( formIDXMPPROF, true, txtIDXMPPROF3, (char *)&ProfilesNames[3], MAXCHARPROFILE, false, true, false, true, true, 0, 0));
  // IDXMPPROF4  Changing the name of profile 4
  LstModifParams.push_back(new CCharModifier ( formIDXMPPROF, true, txtIDXMPPROF4, (char *)&ProfilesNames[4], MAXCHARPROFILE, false, true, false, true, true, 0, 0));
  // IDXMPWRITE  Writes Profiles in the Profiles.ini file
  LstModifParams.push_back(new CPushButtonModifier ( formIDXMPRETURN, true, txtIDXMPWRITE, 0, 0));
  // IDXMPREAD   Read Profiles from the Profiles.ini file
  LstModifParams.push_back(new CPushButtonModifier ( formIDXMPRETURN, true, txtIDXMPREAD, 0, 0));
}

//-------------------------------------------------------------------------
//! \brief Début du mode
void CModeModifProfiles::BeginMode()
{
  // Appel de la méthode parente
  CModeGeneric::BeginMode();
  // Initialisation des noms
  for (int i=0; i<MAXPROFILES; i++)
  {
    strcpy( ProfilesNames[i], commonParams.ProfilesNames[i]);
    /*if (strstr(ProfilesNames[i], "Audio") != NULL)
      strcpy( ProfilesNames[i], "Audio");*/
    while (strlen(ProfilesNames[i]) < MAXCHARPROFILE)
      strcat( ProfilesNames[i], " ");
    //Serial.printf("CModeModifProfiles::BeginMode %d, [%s]\n", i, ProfilesNames[i]);
  }
  // Search ini files in SD
  bool bIniFiles = false;
  char name[80];
  FsFile file;
  FsFile root;
  // Select root directory
  root.open("/");
  // Directory files are searched
  while (file.openNext(&root, O_RDONLY))
  {
    // Recovering the file name
    file.getName( name, 80);
    strlwr( name);
    // We do not take into account the "System Volume Information" directory nor directories and files other than wav
    if (file.isDir() == false and strstr(name, ".ini") != NULL)
      bIniFiles = true;
    file.close();
    if (bIniFiles)
      break;
  }
  if (bIniFiles)
    LstModifParams[IDXMPREAD]->SetbValid(true );
  else
    LstModifParams[IDXMPREAD]->SetbValid(false);
  iScreen = PMNORMAL;
}

//-------------------------------------------------------------------------
//! \brief Called on end of change of a modifier
//! If a mode change, returns the requested mode
//! \param idxModifier Index of affected modifier
int CModeModifProfiles::OnEndChange(
  int idxModifier
  )
{
  switch (idxModifier)
  {
  case IDXMPRETURN:  // Return to parameters mode
    iNewMode = MPARAMS;
    break;
  case IDXMPPROF1 :  // Changing the name of profile 1
  case IDXMPPROF2 :  // Changing the name of profile 2
  case IDXMPPROF3 :  // Changing the name of profile 3
  case IDXMPPROF4 :  // Changing the name of profile 4
    // Copy profile name
    strcpy( commonParams.ProfilesNames[idxModifier-IDXMPPROF1+1], ProfilesNames[idxModifier-IDXMPPROF1+1]);
    //Serial.printf("Profil %d [%s], new [%s]\n", idxModifier-IDXMPPROF1+1, commonParams.ProfilesNames[idxModifier-IDXMPPROF1+1], ProfilesNames[idxModifier-IDXMPPROF1+1]);
    // Save profiles names
Serial.println("StaticWriteParams on CModeModifProfiles::OnEndChange");
    StaticWriteParams();
    break;
  case IDXMPWRITE :  // Writes Profiles in the Profiles.ini file
    LstModifParams[IDXMPRETURN]->SetbValid(false);
    LstModifParams[IDXMPPROF1 ]->SetbValid(false);
    LstModifParams[IDXMPPROF2 ]->SetbValid(false);
    LstModifParams[IDXMPPROF3 ]->SetbValid(false);
    LstModifParams[IDXMPPROF4 ]->SetbValid(false);
    LstModifParams[IDXMPREAD  ]->SetbValid(false);
    ExportProfiles();
    iScreen = PMWRITE;
    iSecond = second() + 1;
    if (iSecond > 59)
      iSecond = 0;
    break;
  case IDXMPREAD  :  // Read Profiles ini file
    iNewMode = MREADINI;
    break;
  }
  if (iScreen == PMNORMAL)
  {
    // Test si un fichier Profiles.ini existe
    if (sd.exists( "Profiles.ini"))
      LstModifParams[IDXMPREAD]->SetbValid(true );
    else
      LstModifParams[IDXMPREAD]->SetbValid(false);
  }
  return iNewMode;
}

//-------------------------------------------------------------------------
//! \brief To display information outside of modifiers
void CModeModifProfiles::AddPrint()
{
  if (iScreen != PMNORMAL and second() == iSecond)
  {
    iScreen = PMNORMAL;
    LstModifParams[IDXMPRETURN]->SetbValid(true);
    LstModifParams[IDXMPPROF1 ]->SetbValid(true);
    LstModifParams[IDXMPPROF2 ]->SetbValid(true);
    LstModifParams[IDXMPPROF3 ]->SetbValid(true);
    LstModifParams[IDXMPPROF4 ]->SetbValid(true);
    LstModifParams[IDXMPWRITE ]->SetbValid(true);
    // Test si un fichier Profiles.ini existe
    if (sd.exists( "Profiles.ini"))
      LstModifParams[IDXMPREAD]->SetbValid(true );
    else
      LstModifParams[IDXMPREAD]->SetbValid(false);
  }
}

//------------------------------------------------------------------------
//! \brief Traitement des ordres claviers
//! Si la touche est une touche de changement de mode, retourne le mode demandé
//! Cette méthode est appelée régulièrement par le loop principal
//! à charge des différents modes de traiter les actions opérateurs
//! \param key Touche sollicitée par l'opérateur
int CModeModifProfiles::KeyManager(
  unsigned short key
  )
{
  int iReturn = iNewMode;
  if (iScreen == PMNORMAL)
    iReturn = CModeGeneric::KeyManager( key);
  return iReturn;
}

//-------------------------------------------------------------------------
//! \class CModeLoadProfiles
//! \brief Profile file selection class

const char *txtStrIni[]    = {"%s",
                              "%s"};
const char *txtNoIniFile[] = {" No file!            ",
                              " Pas de fichier !    "};
const char *txtLoading     = "  Loading...";

//-------------------------------------------------------------------------
//! \brief Constructor (initialization of parameters to default values)
CModeLoadProfiles::CModeLoadProfiles()
{
  // IDXLPRETURN Return to parameters mode
  LstModifParams.push_back(new CPushButtonModifier ( formIDXMPRETURN, true, txtIDXMPRETURN, 0, 0));
  // IDXLPINIA to IDXLPINIF  Ini file A to F
  char temp[MAX_LINEPARAM+1];
  for (int i=0; i<MAXLINESINI; i++)
  {
    //LstModifParams.push_back(new CCharModifier( " &$$$$$$$$$$$$$$$$$$$", true, txtStrIni, (char *)LstFilesScreen[i], MAX_LINEPARAM, false, false, false, false, false, 0, 0));
    LstModifParams.push_back(new CPushButtonModifier( formIDXMPRETURN, true, (const char **)pLstFilesScreen[i], 0, 0));
    sprintf( temp, " Profile%d.ini", i+1);
    strcpy( LstFilesScreen[i], temp);
  }
  // Initialization
  for (int i=0; i<MAXLINESINI; i++)
  {
    pLstFilesScreen[i][LENGLISH] = LstFilesScreen[i];
    pLstFilesScreen[i][LFRENCH ] = LstFilesScreen[i];
  }
}

//-------------------------------------------------------------------------
//! \brief Beginning of the mode
void CModeLoadProfiles::BeginMode()
{
  // Call of the parent method
  CModeGeneric::BeginMode();
  // List the ini files in the root directory
  ListIniFiles();
  // First line position
  iYFirstLine = 0;
  // Set lines files
  InitLstAffFiles();
}

//-------------------------------------------------------------------------
//! \brief End of the mode
void CModeLoadProfiles::EndMode()
{
  lstIniFiles.clear();
  CModeGeneric::EndMode();
}

//-------------------------------------------------------------------------
//! \brief Called on end of change of a modifier
//! If a mode change, returns the requested mode
//! \param idxModifier Index of affected modifier
int CModeLoadProfiles::OnEndChange(
  int idxModifier
  )
{
  switch (idxModifier)
  {
  case IDXLPRETURN:  // Return to parameters mode
    //Serial.print("OnEndChange IDXLPRETURN");
    iNewMode = MPARAMS;
    break;
  case IDXLPINIA:
  case IDXLPINIB:
  case IDXLPINIC:
  case IDXLPINID:
  case IDXLPINIE:
  case IDXLPINIF:
    //Serial.printf("OnEndChange lecture fichier %s\n", lstIniFiles[iSelectedFile].c_str());
    ((CPushButtonModifier *)LstModifParams[idxModifier])->SetString( (char *)txtLoading);
    PrintMode();
    if (!ImportProfiles( (char *)lstIniFiles[iSelectedFile].c_str()))
    {
      LogFile::AddLog( LLOG, "Error on read %s !", lstIniFiles[iSelectedFile].c_str());
      CModeError::SetTxtError( (char *)txtErrorRProfiles[commonParams.iLanguage]);
      iNewMode = MERROR;
    }
    else
    {
      if (commonParams.iLanguage == LFRENCH)
        LogFile::AddLog( LLOG, "Chargement fichier %s", lstIniFiles[iSelectedFile].c_str());
      else
        LogFile::AddLog( LLOG, "%s file loading", lstIniFiles[iSelectedFile].c_str());
      iNewMode = MPARAMS;
    }
    ((CPushButtonModifier *)LstModifParams[idxModifier])->SetString( (char *)lstIniFiles[iSelectedFile].c_str());
    break;
  }
  return iNewMode;
}
  
//-------------------------------------------------------------------------
//! \brief Keyboard order processing
//! If the key is a mode change key, returns the requested mode
//! This method is called regularly by the main loop
//! By default, handles modifiers
//! \param key Touch to treat
int CModeLoadProfiles::KeyManager(
  unsigned short key
  )
{
  int iNewMode = NOMODE;
  switch( key)
  {
  case K_UP  :
    // Select files
    iSelectedFile++;
    if (iSelectedFile >= (int)lstIniFiles.size())
      iSelectedFile = -1;
    InitLstAffFiles();
    SetIdxLines();
    break;
  case K_DOWN:
    // Select files
    iSelectedFile--;
    if (iSelectedFile < -1)
      iSelectedFile = (int)lstIniFiles.size() - 1;
    InitLstAffFiles();
    SetIdxLines();
    break;
  default:
    // To manage selected param
    iNewMode = CGenericMode::KeyManager( key);
  }
  return iNewMode;
}
  
//-------------------------------------------------------------------------
//! \brief Returns the string to display, taking only the n most significant characters
//! 123456789012345678901
//!  PR001_190126_221812.ini
//!  PR001_190126_221812
//! \param str string to verify
String CModeLoadProfiles::PrepareStrFile(
  String str
  )
{
  String newStr = str;
  // Copie des 20 derniers caractères (sauf .ini)
  int max  = MAX_LINEPARAM - 1;
  if ((int)newStr.length() > max)
    // On enlève les caractères en trop
    newStr = newStr.substring( 0, max);
  if ((int)newStr.length() < max)
  {
    // On complète a n caractéres éventuellement
    while ((int)newStr.length() < max)
      newStr += " ";
  }
  //Serial.printf("PrepareStrFile(%s)=[%s]\n", str.c_str(), newStr.c_str());
  return newStr;
}

//-------------------------------------------------------------------------
//! \brief Initializes the list of displayed files according to the 1st selected
void CModeLoadProfiles::InitLstAffFiles()
{
  int iOldSelect = idxSelParam;
  //Serial.printf("InitLstAffFiles idxSelParam %d, iSelectedFile %d\n", idxSelParam, iSelectedFile);
  if (lstIniFiles.size() == 0)
  {
    // No files !
    strcpy( LstFilesScreen[0], txtNoIniFile[GetCommonsParams()->iLanguage]);
    LstModifParams[IDXLPINIA]->SetbValid( true);
    for (int i=1; i<MAXLINESINI; i++)
      LstModifParams[IDXLPINIA+i]->SetbValid( false);
    if (iOldSelect >= 0)
      LstModifParams[iOldSelect ]->SetbSelect(false);
    idxSelParam = 0;
    LstModifParams[IDXLPRETURN]->SetbSelect(true);
  }
  else
  {
    // Files, Attention, the first file is the last of the list (sorted in ascending alphabetical order)
    int iFirst, iLast, iNb;
    iNb = (int)lstIniFiles.size();
    if (iSelectedFile < 0)
    {
      idxSelParam = 0;
      iFirst = iNb - 1;
      iLast  = iFirst - 6;
      if (iLast < 0)
        iLast = 0;
    }
    else if ((iNb - iSelectedFile - 1) < MIDDLELINEINI)
    {
      iFirst = iNb - 1;
      iLast  = iFirst - MAXLINESINI + 1;
      if (iLast < 0)
        iLast = 0;
      idxSelParam = IDXLPINIA + iNb - iSelectedFile - 1;
      //Serial.printf("A iNb %d, iFirst %d, iLast %d, iSelectedFile %d, idxSelParam %d, iOldSelect %d, iFirstIdxLine %d, iLastIdxLine %d\n", iNb, iFirst, iLast, iSelectedFile, idxSelParam, iOldSelect, iFirstIdxLine, iLastIdxLine);
    }
    else if (iSelectedFile < MIDDLELINEINI)
    {
      iFirst = MAXLINESINI - 1;
      iLast  = 0;
      if (iFirst > (iNb-1))
        iFirst = iNb - 1;
      idxSelParam = IDXLPINIA + (iFirst - iSelectedFile);
      //Serial.printf("B iNb %d, iFirst %d, iLast %d, iSelectedFile %d, idxSelParam %d, iOldSelect %d\n", iNb, iFirst, iLast, iSelectedFile, idxSelParam, iOldSelect);
    }
    else
    {
      iFirst = iSelectedFile + MIDDLELINEINI - 1;
      iLast  = iSelectedFile - MIDDLELINEINI + 1;
      idxSelParam = IDXLPINIA + MIDDLELINEINI - 1;
      //Serial.printf("C iNb %d, iFirst %d, iLast %d, iSelectedFile %d, idxSelParam %d, iOldSelect %d\n", iNb, iFirst, iLast, iSelectedFile, idxSelParam, iOldSelect);
    }
    //Serial.printf("iFirstVisibleIdxLine %d, iLastVisibleIdxLine %d\n", iFirstVisibleIdxLine, iLastVisibleIdxLine);
    for (int i=0, j=iFirst; i<MAXLINESINI; i++, j--)
    {
      if (j >= iLast)
      {
        // Prepare first char
        strcpy( LstFilesScreen[i], " ");
        String str = PrepareStrFile( lstIniFiles[j]);
        char sTemp[MAX_LINEPARAM+1];
        str.toCharArray( sTemp, MAX_LINEPARAM);
        strcat( LstFilesScreen[i], sTemp);
        // File, line is valid
        LstModifParams[IDXLPINIA+i]->SetbValid( true);
        iLastVisibleIdxLine = IDXLPINIA + i;
        //Serial.printf("ligne %d wav %d [%s]\n", i, j, LstFilesScreen[i]);
      }
      else
      {
        //                          12345678901234567890
        strcpy( LstFilesScreen[i], "                   ");
        // No file, no valid line
        LstModifParams[IDXLPINIA+i]->SetbValid( false);
        //Serial.printf("ligne %d vide\n", i);
      }
    }
  }
  if (iOldSelect >= IDXLPRETURN and iOldSelect <= IDXLPINIF)
    LstModifParams[iOldSelect ]->SetbSelect(false);
  if (idxSelParam >= IDXLPRETURN and idxSelParam <= IDXLPINIF)
    LstModifParams[idxSelParam]->SetbSelect( true);
  //Serial.printf("InitLstAffFiles Fin idxSelParam %d, iSelectedFile %d\n", idxSelParam, iSelectedFile);
}
  
//-------------------------------------------------------------------------
//! \brief List the ini files in the root directory
void CModeLoadProfiles::ListIniFiles()
{
  lstIniFiles.clear();
  char name[80];
  char nameminus[80];
  FsFile file;
  FsFile root;
  // Select root directory
  if (!root.open("/"))
    Serial.printf("CModeGeneric::ListFilesErreur sd.chdir(/)\n");
  // Directory files are searched
  while (file.openNext(&root, O_RDONLY))
  {
    // Recovering the file name
    file.getName( name, 80);
    strcpy(nameminus, name);
    strlwr( nameminus);
    // We do not take into account the "System Volume Information" directory nor directories and files other than wav
    if (strcmp( name, "System Volume Information") != 0 and file.isDir() == false and strstr(nameminus, ".ini") != NULL)
      lstIniFiles.push_back( name);
    file.close();
  }
  if (lstIniFiles.size() > 0)
  {
    // Sort ascending alphabetically
    std::sort (lstIniFiles.begin(), lstIniFiles.end());
    // Select last file
    iSelectedFile = (int)lstIniFiles.size() - 1;
  }
  /*Serial.println("CModeLoadProfiles::ListFiles");
  for (unsigned int i=0; i<lstIniFiles.size(); i++)
    Serial.printf("%d [%s]\n", i, lstIniFiles[i].c_str());*/
}

//-------------------------------------------------------------------------
//! \class CModeCopyright
//! \brief Classe d'affichage des copyright

//-------------------------------------------------------------------------
//! \brief Constructeur
CModeCopyright::CModeCopyright()
{
  pEsp32Link = NULL;
  bVersionESP32 = false;
  iNbSecond = 0;
  bPlus = false;
  bESPVersionOK = false;
}

//-------------------------------------------------------------------------
//! \brief Destructeur
CModeCopyright::~CModeCopyright()
{
  if (pEsp32Link != NULL)
    delete pEsp32Link;
}

//-------------------------------------------------------------------------
//! \brief Début du mode
void CModeCopyright::BeginMode()
{
  // Appel de la méthode parente
  CModeGeneric::BeginMode();
  // Init de la seconde courante
  iCurrentS = 61;
  LogFile::AddLog( LLOG, "TeensyRecorder %s, Copyright 2018, Jean-Do. VRIGNAULT,  https://framagit.org/PiBatRecorderPojects/TeensyRecorders", VERSION);
  iTimeESP32 = 0;
#if defined(__MK66FX1M0__) // Teensy 3.6
  if (GetRecorderType() > PASSIVE_STEREO)
  {
    // Création de la liaison ESP32 pour la récupération de la version du logiciel ESP32
    pEsp32Link = new CMasterLink();
    pEsp32Link->SetLinkType( MASTER, NULL);
    iTimeESP32 = millis() + 1000; // Attente de 800ms avant d'envoyer la demande de version
  }
#endif
}

//-------------------------------------------------------------------------
//! \brief Affichage du mode sur l'écran
//! Cette méthode est appelée régulièrement par le loop principal
//! à charge des différents modes d'afficher les informations nécessaires
void CModeCopyright::PrintMode()
{
  // Affichage toutes les secondes
  if (second() != iCurrentS)
  {
    char Message[50];
    int iWidth;
    iCurrentS = second();
    pDisplay->clearBuffer();
    pDisplay->setFont(u8g2_font_6x13B_mf);  // Hauteur 12
    if (bPlus)
      sprintf(Message, "SyncTopESP32 %s", CSynchroLink::GetESP32Version());
    else
      sprintf(Message, "TeensyRecorder %s", VERSION);
    iWidth = pDisplay->getStrWidth(Message);
    pDisplay->setCursor((128-iWidth)/2,0);
    pDisplay->println(Message);
    pDisplay->setFont(u8g2_font_courB08_tf);  // Hauteur 6
    sprintf(Message, "%dMHz Copyright 2018", F_CPU/1000000);
    iWidth = pDisplay->getStrWidth(Message);
    pDisplay->setCursor((128-iWidth)/2,13);
    pDisplay->println(Message);
    strcpy(Message, "Jean-Do. VRIGNAULT" );
    iWidth = pDisplay->getStrWidth(Message);
    pDisplay->setCursor((128-iWidth)/2,22);
    pDisplay->println(Message);
    switch (GetRecorderType())
    {
    //                                      123456789012345678901
    case PASSIVE_RECORDER: strcpy(Message, "PASSIVE RECORDER"   ); break;
    case ACTIVE_RECORDER : strcpy(Message, "ACTIVE RECORDER"    ); break;
    case PASSIVE_STEREO  : strcpy(Message, "P.R. STEREO"        ); break;
    case PASSIVE_SYNCHRO : strcpy(Message, "P.R. STEREO SYNCHRO"); break;
    default              : strcpy(Message, "UNKNOW RECORDER"    ); break;
    }
    iWidth = pDisplay->getStrWidth(Message);
    pDisplay->setCursor((128-iWidth)/2,30);
    pDisplay->println(Message);
    // https://framagit.org/PiBatRecorderPojects/TeensyRecorders
    pDisplay->setFont(u8g2_font_tinytim_tf);  // Hauteur 5
    strcpy(Message, "https://framagit.org/" );
    iWidth = pDisplay->getStrWidth(Message);
    pDisplay->setCursor((128-iWidth)/2,41);
    pDisplay->println(Message);
    strcpy(Message, "/PiBatRecorderPojects" );
    iWidth = pDisplay->getStrWidth(Message);
    pDisplay->setCursor((128-iWidth)/2,49);
    pDisplay->println(Message);
    strcpy(Message, "/TeensyRecorders" );
    iWidth = pDisplay->getStrWidth(Message);
    pDisplay->setCursor((128-iWidth)/2,57);
    pDisplay->println(Message);
    pDisplay->sendBuffer();
#if defined(__MK66FX1M0__) // Teensy 3.6
    if (GetRecorderType() > PASSIVE_STEREO)
    {
      iNbSecond++;
      if (iNbSecond > 3)
      {
        bPlus = !bPlus;
        iNbSecond = 0;
      }
    }
#endif
  }
  if (pEsp32Link != NULL)
  {
    if (iTimeESP32 != 0 and millis() > iTimeESP32)
    {
      // Emission du message de demande de version
      pEsp32Link->VersionRequest();
      iTimeESP32 = 0;
      iTimeout = millis() + 500;
    }
    else if (iTimeESP32 == 0)
    {
      if (pEsp32Link->OnReceive() == LMSVERSION)
      {
        pEsp32Link->MessageProcessing(LMSVERSION);
        Serial.printf("Version ESP32 [%s]\n", CSynchroLink::GetESP32Version());
        bVersionESP32 = true; 
      }
      else if (!bESPVersionOK and millis() > iTimeout)
      {
        // Emission du message de demande de version
        pEsp32Link->VersionRequest();
        iTimeout = millis() + 500;
      }
    }
  }
}

//-------------------------------------------------------------------------
//! \brief  Keyboard order processing
//! If the key is a mode change key, returns the requested mode
//! This method is called regularly by the main loop
//! By default, handles modifiers
//! \param[in] key Touch to treat
//! \return If a mode change, returns the requested mode, else return NOMODE
int CModeCopyright::KeyManager(
  unsigned short key
  )
{
  int iReturn = iNewMode;
  if (key != K_NO)
    // Sur toutes les touches, on passe en mode paramètres
    iReturn = MPARAMS;
  return iReturn;
}
