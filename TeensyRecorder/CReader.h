//-------------------------------------------------------------------------
//! \file CReader.h
//! \brief Classe de gestion de la lecture d'un fichier wav
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/* 
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <arduino.h>
#include "CWaveFile.h"
#include "Restitution.h"
#include "CRecorder.h"

#ifndef CREADER_H
#define CREADER_H

//-------------------------------------------------------------------------
//! \class CReader
//! \brief Classe pour la gestion de la lecture d'un fichier wav
//! Gère la lecture du fichier wav et l'envoi des échantillons vers le DAC0
class CReader
{
public:
  //-------------------------------------------------------------------------
  //! \brief Constructeur (initialisation de la classe)
  //! \param pParams Pointeur sur les paramètres opérateur
  CReader(
    ParamsOperator *pParams
    );

  //-------------------------------------------------------------------------
  //! \brief Destructeur
  virtual ~CReader();

  //-------------------------------------------------------------------------
  //! \brief Initialisation du fichier à lire (ouverture du fichier)
  //! Retourne true si ouverture OK
  //! \param pPathWavFile Pointeur vers le nom du fichier
  bool SetWaveFile(
    char *pPathWavFile
    );

  //-------------------------------------------------------------------------
  //! \brief Initialisation du mode de lecture
  //! \param iFeRead Fréquence d'échantillonnage de lecture
  //! \param BHeterodyne Indique si lecture en hétérodyne (true)
  void SetReadMode(
    int iFeRead,
    bool BHeterodyne
    );

  //-------------------------------------------------------------------------
  //! \brief Lance la lecture du fichier sélectionné
  //! Retourne true si lecture OK
  bool StartRead();

  //-------------------------------------------------------------------------
  //! \brief Stoppe la lecture du fichier en lecture
  //! Retourne true si arret OK
  bool StopRead();

  //-------------------------------------------------------------------------
  //! \brief Retourne true si une lecture est en cours
  bool IsReading() {return bReading;};

  //-------------------------------------------------------------------------
  //! \brief Retourne la durée en secondes du fichier wav sélectionné
  unsigned int GetWaveDuration() {return iWaveDuration;};

  //-------------------------------------------------------------------------
  //! \brief Retourne la position de lecture en secondes
  unsigned int GetWavePos() {return iWavePos;};

  //-------------------------------------------------------------------------
  //! \brief Retourne la position de lecture en % de la durée complète du fichier
  unsigned short GetPosLecture() {return iPosLecture;};

  //-------------------------------------------------------------------------
  //! \brief Retourne la fréquence d'échantillonnage du fichier wav sélectionné en Hz
  //! Tient compte de l'expansion de temps éventuelle
  //! Retourne 0 si aucun fichier sélectionné
  int GetFeFile() {return waveFile.GetSampleRate();};

  //-------------------------------------------------------------------------
  //! \brief Retourne la fréquence d'échantillonnage de lecture en Hz
  //! Tient compte de l'expansion de temps éventuelle
  //! Retourne 0 si aucun fichier sélectionné
  int GetFe() {return iFe;};

  //-------------------------------------------------------------------------
  //! \brief Fonction d'initialisation d'un buffer avec les échantillons du fichier wave
  //! Retourne true si OK
  //! \param pBuffer Pointeur sur le buffer à initialiser
  bool SetBuffer(
    uint16_t *pBuffer
    );
  
  //-------------------------------------------------------------------------
  //! \brief Fonction de traitement des interruption DMA pour remplir les buffers
  void OnITDMA();
  
  //-------------------------------------------------------------------------
  //! \brief Positionne le fichier en lecture à +/- Coef de sa durée totale
  //! Retourne false si la durée du fichier est dépassée ou inférieure à 0
  //! \param iNext +1 pour avancer, -1 pour reculer
  //! \param iCoef Coefficient d'avancement
  bool ReadNext(
    int iNext,
    int iCoef=10
    );
  
  //-------------------------------------------------------------------------
  //! \brief Retourne le nom du fichier courant éventuellement modifié suite à optimisation
  char *GetWaveFileName() {return sPathWaveFile;};
  
  //-------------------------------------------------------------------------
  //! \brief Initialisation de la fréquence hétérodyne
  //! \param fH Fréquence hétérodyne en kHz
  void SetFreqHeterodyne(
    float fH
    );

  //! Pointeur sur l'instance unique de la classe
  static CReader *pInstance;
  
#if defined(__IMXRT1062__) // Teensy 4.1, filtre hétérodyne sélectif
  //----------------------------------------------------
  //! \brief Initialisation du filtre sélectif
  //! \param fH Fréquence hétérodyne en kHz
  //! \return Retourne false en cas d'erreur
  bool InitFiltreSel(
    float fFreqH
    );
#endif

protected:
  //! Paramètres opérateurs
  ParamsOperator *pParamsOp;

  //! Indique si une lecture est en cours
  bool bReading;

  //! Indique si la lecture est en expansion de temps
  bool bExp10;

  //! Indique si la lecture est en hétérodyne
  bool bHet;

  //! Fréquence d'échantillonnage de la lecture en Hz
  int iFe;

  //! Indique si le fichier en lecture est stéréo
  bool bStereo;

  //! Nom du fichier à lire
  char sPathWaveFile[255];

  //! Nombre d'échantillons du fichier wav sélectionné
  uint32_t lWaveLength;

  //! Nombre d'échantillons lus
  uint32_t lWaveEchRead;

  //! Durée en secondes du fichier wav sélectionné
  uint32_t iWaveDuration;

  //! Indique en secondes la position de lecture
  uint32_t iWavePos;

  //! Indique la position de la lecture en % par rapport au fichier complet
  unsigned short iPosLecture;

  //! Taille en octet d'un buffer
  uint32_t sizeBuffer;

#if defined(__MK66FX1M0__) // Teensy 3.6
  //! Double buffers des échantillons DMA
  volatile uint16_t samplesDMA[MAXSAMPLE_READ*2];
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  //! Sur Teensy 4.1, buffer stéréo
  static DMAMEM int16_t samplesDMA[MAXSAMPLE_READ*4];
#endif

  //! Pointeurs sur le buffer 1 et 2
  volatile uint16_t *P1, *P2;

  //! Buffer de lecture du fichier wav (avec prise en compte stéréo)
  int16_t samplesWave[MAXSAMPLE_READ*2];
  
  //! Gestionnaire du fichier wave
  CWaveFile waveFile;

  //! Gestionnaire de restitution audio sur le DAC0
  Restitution restitution;

  //! Décalage éventuel pour la gestion de l'atténuation
  int iShift;

  // Gestionnaire de calcul hétérodyne
  CHeterodyne heterodyne;
  
  // Gestionnaire d'un éventuel filtre FIR
  CFIRFilter firFilter;
  
  //! Un buffer temporaire pour le filtre FIR
  volatile q15_t    samplesFirIn [BLOCK_SAMPLES];
  volatile q15_t    samplesFirOut[BLOCK_SAMPLES];    

#if defined(__IMXRT1062__) // Teensy 4.1, filtre hétérodyne sélectif
  // Indice du filtre sélectif utilisé
  int idxSelFilter;
  //! Buffers pour le filtre sélectif
  volatile q15_t samplesFirSelIn [BLOCK_SAMPLES];
  volatile q15_t samplesFirSelOut[BLOCK_SAMPLES];
  // Gestionnaire du filtre FIR sélectif
  CFIRFilter SelectiveFilter;
#endif
};

#endif // CREADER_H
