/* 
 * File:   CRecorder.cpp
   PassiveRecorder Copyright (c) 2018 Vrignault Jean-Do.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "arduino.h"
#include <Wire.h>
#include "Const.h"
#include "TimeLib.h"
#include "CModeGeneric.h"
#include "CRecorderRL.h"

// Gestionnaire de carte SD
extern SdFs sd;

// Serial nuber string with recorder name
extern char sSerialName[MAXNAMERECORDER];

#if defined(__IMXRT1062__) // Teensy 4.1
  // Buffer pour copier le buffer DMA avant traitement
  extern uint16_t samplesMem[];
#endif

//-------------------------------------------------------------------------
// Classe rassemblant toutes les opérations nécessaire pour l'enregistrement
// - Acquisition (ADC via DMA cadencé par un PDB, traitement dans l'IT DMA)
// - Traitements (dans l'IT DMA)
// - FFT pour vérifier la présence d'un signal (dans l'IT DMA)
// - Enregistrement dans un fichier wav si activité (dans le loop du programme principal)
//-------------------------------------------------------------------------
// Constructeur (initialisation de la classe)
CRecorderRL::CRecorderRL(
  ParamsOperator *pParams // Pointeur sur les paramètres opérateur
  )
  :CGenericRecorder( pParams, FFTLENRL),
  acquisition( (uint16_t *)samplesDMA, MAXSAMPLE, PINA3_AUDIO, 250000)
{
  bSonde = false;
  bNoiseOK = false;
  // Initialisation du pointeur de moitié du buffer DMA
  pHalfDMASamples = (uint32_t *)&(samplesDMA[MAXSAMPLE]);
  // Initialisation du nombre d'échantillon à traiter dans une FFT en fonction du mode zeropadding ou non
#ifdef TESTRL256
  // Sans mode Zero padding et FFT 256 points
  iNbEchFFT = FFTLENRL;
#else
  #if defined(__MK66FX1M0__) // Teensy 3.6
    if (F_CPU < 50000000)
      // Sans mode Zero padding
      iNbEchFFT = FFTLENRL;
    else
      // Avec zeropadding 256
      iNbEchFFT = 256;
  #endif
  #if defined(__IMXRT1062__) // Teensy 4.1
    // Avec zeropadding 256
    iNbEchFFT = 256;
  #endif
#endif
  
  // Test if the CF memory occupation is too important
  bFCtooLarge  = false;
  int iNbCnxFC = 0;
  // Width of a channel in Hz
  int iCanal   = (250000 / 2) / (FFTLENRL / 2);
  idxFMin = 5000;
  idxFMax = 0;
  for (int b=0; b<MAXBD; b++)
  {
    if (pParamsOp->batBand[b].iType != BDINVALID)
    {
      // Calculation of the min and max indices of the band GR and min RE-PR to avoid hramonics
      float fIdx = pParamsOp->batBand[b].fMin * 1000.0 / (float)iCanal;
      int idxMin = (short)fIdx;
      fIdx       = pParamsOp->batBand[b].fMax * 1000.0 / (float)iCanal;
      int idxMax = (short)fIdx;
      if (pParamsOp->batBand[b].iType == BDFC or pParamsOp->batBand[b].iType == BDQFC)
        // Add number of channel for this band
        iNbCnxFC += idxMax - idxMin + 1;
      idxFMin = min(idxFMin, idxMin);
      idxFMax = max(idxFMax, idxMax);
      pParamsOp->uiFreqMin = min(pParamsOp->uiFreqMin, pParamsOp->batBand[b].fMin * 1000.0);
      pParamsOp->uiFreqMax = min(pParamsOp->uiFreqMax, pParamsOp->batBand[b].fMax * 1000.0);
      //Serial.printf("CRecorderRL::CRecorderRL Bande %d, idxMin %d, idxMax %d\n", b, idxMin, idxMax);
    }
  }
  // Size of FC channels (2 DetectionFC by channel)
  int iFCSize = sizeof(DetectionFC) * iNbCnxFC * 2;
  Serial.printf("RhinoLogger, FC %d canaux, size %d\n", iNbCnxFC, iFCSize);
  if (iFCSize > 150000)
  {
    bFCtooLarge = true;
    if (CModeGeneric::GetCommonsParams()->iLanguage == LFRENCH)
      LogFile::AddLog( LLOG, "Mode RhinoLogger, le nombre de canaux des bandes RH/FC (%d) dépasse la capacité mémoire", iNbCnxFC);
    else
      LogFile::AddLog( LLOG, "RhinoLogger mode, the number of RH/FC band channels (%d) exceeds the memory capacity", iNbCnxFC);
  }
  // Create analyser bands
  for (int b=0; b<MAXBD; b++)
  {
    LstAnalysers[b] = NULL;
    switch (pParamsOp->batBand[b].iType)
    {
    case BDFM : LstAnalysers[b] = new CFMAnalyzerRL()   ; iNbBands++; break;
    case BDFC : if (bFCtooLarge == false)
                  LstAnalysers[b] = new CFCAnalyzerRL() ; iNbBands++;
                break;
    case BDQFC : if (bFCtooLarge == false)
                  LstAnalysers[b] = new CQFCAnalyzerRL(); iNbBands++;
                break;
    }
  }
  //Serial.printf("Taille CRecorderRL %d, CFMAnalyzerRL %d, CFCAnalyzerRL %d\n", sizeof(CRecorderRL), sizeof(CFMAnalyzerRL), sizeof(CFCAnalyzerRL));
  // Initialisation de la FFT
  arm_cfft_radix4_init_q15( &cpx_fft_inst, iFFTLen, 0, 1);
}
  
//-------------------------------------------------------------------------
// Destructeur
CRecorderRL::~CRecorderRL()
{
  //Serial.printf("Destructeur CRecorderRL\n");
  for (int b=MAXBD-1; b>=0; b--)
    if (LstAnalysers[b] != NULL)
      delete LstAnalysers[b];
  //Serial.printf("Destructeur CRecorderRL OK\n");
}
  
//-------------------------------------------------------------------------
// Démarre le mode acquisition
// Retourne true si démarrage OK
bool CRecorderRL::StartAcquisition()
{
  if (CModeGeneric::bDebug)
    Serial.println("CRecorderRL::StartAcquisition");
  // Initialisation des tables
  THBMinute.iTempMin = 100;
  THBMinute.iTempMax = -100;
  THBMinute.iHygroMin = 100;
  THBMinute.iHygroMax = 0;
  THBMinute.fBatInt = 0;
  THBMinute.fBatExt = 0;
  THBHour = THBMinute;
  THBDay  = THBMinute;
  int iFMin=250, iFMax=0;
  iNbBands = 0;
  // Calcul de la fenêtre de Han (1024 sans zeropadding ou 256 avec zeropadding)
  for (int n=0; n<iNbEchFFT; n++)
    TbWinHan[n] = 0.54 + 1 - cos( 2 * PI * n / (iNbEchFFT-1));
  if (iNbEchFFT != iFFTLen)
  {
    // En mode zeropadding on complète avec des 1
    for (int n=iNbEchFFT; n<iFFTLen; n++)
      TbWinHan[n] = 1;
    //Serial.printf("CRecorderRL::StartAcquisition FFT %d points, zero padding %d points\n", iFFTLen, iNbEchFFT);
  }
  //else
  //  Serial.printf("CRecorderRL::StartAcquisition FFT %d points, sans zero padding\n", iFFTLen);
  // Initialize analyser bands
  for (int b=0; b<MAXBD; b++)
  {
    if (LstAnalysers[b] != NULL)
    {
      LstAnalysers[b]->InitializingAnalysis( uiFeWav, &(pParamsOp->batBand[b]), pParamsOp->bDebugBand);
      // Init de l'index min et max des bandes à traiter
      iFMin = min(iFMin , pParamsOp->batBand[b].fMin);
      iFMax = max(iFMax , pParamsOp->batBand[b].fMax);
    }
  }
  // Test if the CF memory occupation is too important
  // Prise en compte bande GR si absente
  pParamsOp->uiFreqMin = min(iFMin , 76);
  pParamsOp->uiFreqMax = max(iFMax , 86);
  // Appel de la méthode de base
  CGenericRecorder::StartAcquisition();
  // Calcul du coefficient du filtre passe haut
  int iFreqHighPass = 8000;
  if (iFreqHighPass == 0)
    fDCBlockingFactor = 0.999;
  else
    // 1 / (2 * PI * cut_off_frequency * (1 / sampling_frequency) + 1)  (Frank DD4WH)
    fDCBlockingFactor = 1.0 / (2.0 * PI * (float)iFreqHighPass * (1.0 / (float)uiFeWav) + 1.0);
  // RAZ Watchdog pour surveiller le fonctionnement (RAZ dans le loop, incrément dans les IT, si trop grand alors RESET)
  iWatchDogITTimer = 0;
  iWatchDogITDMA   = 0;
  //CAnalyzerRL::SetDebug( CModeGeneric::bDebug);
  // Prise en compte de l'heure courante
  iCurrentSecond = second();
  iCurrentMinute = minute();
  iCurrentHour   = hour();
  bMinuteOK = false;
  bHourOK   = false;
  bDayOK    = false;
  bActivite = false;
  bErrDMA = false;
  // Init des noms des fichiers de sauvegarde
  sprintf( FileNameJ , "JJ%04d_%s.csv", year(), sSerialName);
  sprintf( FileNameH , "HH%04d%02d%02d_%s.csv", year(), month(), day(), sSerialName);
  sprintf( FileNameMN, "MM%04d%02d%02d_%s.csv", year(), month(), day(), sSerialName);
  // Init de l'heure de début des mesures
  char  sTime[MAXTIMEMES];
  sprintf( sTime, "%02d:%02d", hour(), minute());
  strcpy(  sTimeCurrentMinute, sTime);
  strcpy(  sTimeCurrentHour  , sTime);
  strcpy(  sTimeCurrentDay   , sTime);
  strcpy(  sTimeSaveMinute   , sTime);
  strcpy(  sTimeSaveHour     , sTime);
  strcpy(  sTimeSaveDay      , sTime);
  // Init de la date de début des mesures
  sprintf( sCurrentDateFile, "%02d/%02d/%04d", day(), month(), year());
  strcpy(  sSaveDateFile   , sCurrentDateFile);
  // Initialisation des bandes
  //pParamsOp->bDebugBand = true;
  pParamsOp->bDebugBand = false;
  //CAnalyzerRL::SetDebug( true);
  // Lancement de l'acquisition
  memset( (void *)samplesDMA, 0, sizeof(uint16_t)*MAXSAMPLE*2);
  pBufferATraiter = NULL;
  acquisition.setFe( uiFeAcq);
  acquisition.start();
  bStarting = true;
  // On lance le timer
  TimerPeriod.begin(OnTimer, PERIODETIMERRL);
  if (CModeGeneric::bDebug)
    Serial.println("CRecorderRL::StartAcquisition OK");
  return true;
}
  
//-------------------------------------------------------------------------
// Stoppe le mode acquisition
// Retourne true si arrêt OK
bool CRecorderRL::StopAcquisition()
{
  if (CModeGeneric::bDebug)
    Serial.println("CRecorderRL::StopAcquisition");
  // On simule une fin de période timer pour écrire les infos de la dernière période et du jour
  iCurrentSecond = 61;
  iCurrentMinute = 61;
  iCurrentHour   = 11;
  OnPeriodeTimer();
  // Sauvegarde des fichiers
  SaveCSVFiles();
  if (bStarting)
  {
    // Arret de l'acquisition
    acquisition.stop();
    bStarting = false;
  }
  TimerPeriod.end();
  return true;
}

//-------------------------------------------------------------------------
// Stoppe ou lance l'acquisition
void CRecorderRL::SetAcquisition(
  bool bAcq   // true pour lancer l'acquisition, false pour la suspendre
  )
{
  if (bAcq and !acquisition.IsAcquisition())
  {
    acquisition.start();
    TimerPeriod.begin(OnTimer, PERIODETIMERRL);
  }
  else if (!bAcq and acquisition.IsAcquisition())
  {
    TimerPeriod.end();
    acquisition.stop();
    delay(50);
  }
}

//-------------------------------------------------------------------------
// Gestion de l'IT DMA lorsqu'un buffer d'acquisition est plein
void CRecorderRL::OnDMA_ISR()
{
  //uint32_t uiTimeISR = micros();
  // Lecture de l'adresse courante d'écriture du DMA
  uint32_t daddr = acquisition.GetCurrentDMAaddr();
  // Par défaut, 1er buffer
  pBufferATraiter = (uint16_t *)samplesDMA;
  //Serial.printf("DMA ISR %d (%d)\n", daddr, pHalfDMASamples);
  if (daddr < (uint32_t)pHalfDMASamples)
  {
    // Le DMA est dans le 1er buffer, on s'occupe du second
    pBufferATraiter = (uint16_t *)&(samplesDMA[MAXSAMPLE]);
    //Serial.println("Flip");
  }
  //else
  //  Serial.println("Flop");
#if defined(__IMXRT1062__) // Teensy 4.1
  // Sur Teensy 4.1, il est nécessaire de récupérer le buffer qui est éventuellement dans le cache
  // Delete data from the cache, without touching memory
  //
  // WARNING: This function is DANGEROUS!!  The address must be
  // 32 byte aligned and the size must be a multiple of 32 bytes.
  //
  // DO NOT USE this function with arbitrarily aligned data,
  // especially pointers from malloc() or C++ new.  The ARM cache
  // can only delete with granularity of 32 byte cache rows.  If
  // you attempt to delete improperly aligned data, any other
  // cached variables shared within the same 32 byte cache row(s)
  // will become collateral damage!
  //
  // If you wish to assure some variable or array or other data
  // is not cached, use arm_dcache_flush_delete().  This
  // arm_dcache_delete() should only be used for very special
  // cases like DMA buffers or hardware testing & benchmarks.
  //
  // See this forum thread for more detail:
  // https://forum.pjrc.com/threads/68100-BUG-in-arm_dcache_delete
  //
  // Normally arm_dcache_delete() is used before receiving data via
  // DMA or from bus-master peripherals which write to memory.  You
  // want to delete anything the cache may have stored, so your next
  // read is certain to access the physical memory.
  arm_dcache_delete((void*)pBufferATraiter, sizeof(uint16_t)*MAXSAMPLE);
  // Copie rapide dans un buffer
  memmove( samplesMem, (int16_t *)pBufferATraiter, sizeof(uint16_t)*MAXSAMPLE);
  pBufferATraiter = samplesMem;
#endif
  TraiteBuffer();
#if defined(__IMXRT1062__) // Teensy 4.1
  // RAZ bit d'interruption du DMA
  acquisition.ClearDMAIsr();
#endif
  // Temps traitement ISR, Fe 250kHz, 1 Ech = 0.004ms, 8192 Ech = 32.768ms
  //              FFT1024   ZeroPadding512  ZeroPadding256
  // T3.6  48MHz  23.910ms  (39.971ms)      (72.070ms)
  // T3.6 144MHz   8.109ms   13.595ms        24.568ms
  // T3.6 180MHz   6.712ms   10.962ms        19.924ms
  // T4.1 150MHz   8.180ms    8.480ms        14.200ms
  //Serial.printf("OnDMA_ISR %dµs\n", micros()-uiTimeISR);
}

//-------------------------------------------------------------------------
// Traitement dans l'IT DMA d'un buffer d'acquisition plein
inline void CRecorderRL::TraiteBuffer()
{
  //Serial.printf("CRecorderRL::TraiteBuffer iFFTLen %d, idxFMin %d, idxFMax %d\n", iFFTLen, idxFMin, idxFMax);
  // Traitement du buffer
  bActivite = false;

  // Passage du domaine 0/4096 non signe vers le domaine -2048/+2048 signé
  // Et FFT de recherche de signal
  for (iSample=0; iSample<MAXSAMPLE;) // Incrément effectué dans la préparation des FFT
  {
    // Prépare les échantillons d'une FFT
    PrepareFFTWithoutFilter();
    //uint32_t uiTimeFFT = micros();
    // Calcul d'une FFT 1024 points (T3.6 48MHz 1.828ms)
    arm_cfft_radix4_q15(&cpx_fft_inst, (q15_t *)bufferCpxFFT);
    //Serial.printf("FFT %dµs\n", micros()-uiTimeFFT);
    if (bNoiseOK)
      // Test si des détections
      TraitementDetections();
    else
      // Traitement du bruit
      NoiseTreatment();
  }
}

//-------------------------------------------------------------------------
// Préparation du traitement FFT sans filtre
inline void CRecorderRL::PrepareFFTWithoutFilter()
{
  int16_t samplesEch;
  // Sans zero padding, FFT 1024 echantillon, sinon zeropadding 256 échantillons
  for (int f=0; f<iNbEchFFT; f++, iSample++)
  {
    // Echantillon signé pour enregistrement et application gain numérique
    samplesEch = (pBufferATraiter[iSample] - 2048) << shift;
    if (fDCBlockingFactor > 0.001)
    {
      // here DC is removed, Frank DD4WH 2018_10_02
      scaledPreviousFilterOutput = (int32_t)(fDCBlockingFactor * previousFilterOutput);
      filteredOutput = samplesEch - previousSample + scaledPreviousFilterOutput;
      previousSample = samplesEch;
      samplesEch = (int16_t)filteredOutput;
      previousFilterOutput = filteredOutput;
    }
    // Application de la fenêtre de Han
    float data = (float)samplesEch * TbWinHan[f];
    samplesEch = (int16_t) data;
    // Echantillon signé pour la FFT (imaginaire à 0 et réel à la valeur de l'échantillon)
    bufferCpxFFT[f] = samplesEch;
  }
  if (iNbEchFFT == 256)
    // On complète avec 768 zéros
    memset( (void *)&(bufferCpxFFT[256]), 0, sizeof(uint16_t)*(iFFTLen-256));
}

//-------------------------------------------------------------------------
// Traitement détections
inline void CRecorderRL::TraitementDetections()
{
  //Serial.println("CRecorderRL::TraitementDetections");
  if (iNbBands > 0)
    // Traitements harmoniques
    CAnalyzerRL::HarmonicProcessing( iTbSeuilCnx, bufferCpxFFT);
  // Pour toutes les bandes valides
  for (int b=0; b<MAXBD; b++)
  {
    if (LstAnalysers[b] != NULL)
    {
      // Traitement des bandes
      LstAnalysers[b]->FFTProcessing( iTbSeuilCnx, bufferCpxFFT);
    }
  }
  //Serial.println("CRecorderRL::TraitementDetections OK");
}

//----------------------------------------------------------------------------
// Fonction de traitement du timer
void CRecorderRL::OnTimer()
{
  ((CRecorderRL *)pInstance)->OnPeriodeTimer();
}
    
//----------------------------------------------------------------------------
// Test si activité sur les bandes toutes les période du timer
void CRecorderRL::OnPeriodeTimer()
{
  // Mesure de temps d'exécution: avec 7 bandes et activité uniquement sur bande FM 120µs en moyenne et 150µs chaque seconde !
  // Avec activité GR max 180µs
  if (iNbBands > 0)
    // Avant analyse des bandes, on analyse les éventuelles harmoniques en masquant des canaux susceptible de contenir des harmoniques des cris de Rhino
    CAnalyzerRL::HarmonicAnalysis();
  // Pour toutes les bandes valides
  for (int b=0; b<MAXBD; b++)
    if (LstAnalysers[b] != NULL)
    {
      // Band Analysis
      LstAnalysers[b]->OnTimerProcessing();
      // Update current second
      LstAnalysers[b]->UpdateCurrentSecond();
      // Prepare new timer period
      LstAnalysers[b]->RAZBeforeTimer();
    }
  if (iCurrentSecond != second())
  {
    iCurrentSecond = second();
    // Update current minute
    for (int b=0; b<MAXBD; b++)
    {
      if (LstAnalysers[b] != NULL)
        LstAnalysers[b]->UpdateCurrentMinute();
    }
    if (iCurrentMinute != minute())
    {
      char  sTime[MAXTIMEMES];
      iCurrentMinute = minute();
      bMinuteOK      = true;
      // Mémo du texte de la minute précédente
      strcpy(  sTimeSaveMinute   , sTimeCurrentMinute);
      strcpy(  sTimeSaveHour     , sTimeCurrentHour);
      strcpy(  sTimeSaveDay      , sTimeCurrentDay);
      // Init du texte de la nouvelle minute
      sprintf( sTime, "%02d:%02d", hour(), minute());
      strcpy(  sTimeCurrentMinute, sTime);
      strcpy(  sTimeCurrentHour  , sTime);
      strcpy(  sTimeCurrentDay   , sTime);
      // Lecture de la température, hygro et tension batterie de la minute courante
      SetTHB( &THBMinute);
      // Mise à jour de la température, hygro et tension batterie de l'heure courante
      THBHour.iTempMin  = min(THBHour.iTempMin,  THBMinute.iTempMin);
      THBHour.iTempMax  = max(THBHour.iTempMax,  THBMinute.iTempMax);
      THBHour.iHygroMin = min(THBHour.iHygroMin, THBMinute.iHygroMin);
      THBHour.iHygroMax = max(THBHour.iHygroMax, THBMinute.iHygroMax);
      if (THBHour.fBatInt < 0.1)
        THBHour.fBatInt   = THBMinute.fBatInt;
      else
        THBHour.fBatInt   = min(THBHour.fBatInt,   THBMinute.fBatInt);
      if (THBHour.fBatExt < 0.1)
        THBHour.fBatExt   = THBMinute.fBatExt;
      else
        THBHour.fBatExt   = min(THBHour.fBatExt,   THBMinute.fBatExt);
      // Mise à jour de la température, hygro et tension batterie du jour courant
      THBDay.iTempMin  = min(THBDay.iTempMin,  THBMinute.iTempMin);
      THBDay.iTempMax  = max(THBDay.iTempMax,  THBMinute.iTempMax);
      THBDay.iHygroMin = min(THBDay.iHygroMin, THBMinute.iHygroMin);
      THBDay.iHygroMax = max(THBDay.iHygroMax, THBMinute.iHygroMax);
      if (THBDay.fBatInt < 0.1)
        THBDay.fBatInt   = THBMinute.fBatInt;
      else
        THBDay.fBatInt   = min(THBDay.fBatInt,   THBMinute.fBatInt);
      if (THBDay.fBatExt < 0.1)
        THBDay.fBatExt   = THBMinute.fBatExt;
      else
        THBDay.fBatExt   = min(THBDay.fBatExt,   THBMinute.fBatExt);
      for (int b=0; b<MAXBD; b++)
      {
        if (LstAnalysers[b] != NULL)
        {
          // Save current minute
          LstAnalysers[b]->StoreCurrentMinute();
          // Update current hour
          LstAnalysers[b]->UpdateCurrentHour();
        }
      }
      if (iCurrentHour != hour())
      {
        bool bEndPeriode = false;
        if (iCurrentHour == 11)
          bEndPeriode = true;
        iCurrentHour  = hour();
        bHourOK       = true;
        // Save current hour
        for (int b=0; b<MAXBD; b++)
        {
          if (LstAnalysers[b] != NULL)
          {
            // Save current hour
            LstAnalysers[b]->StoreCurrentHour();
            // Update current day
            LstAnalysers[b]->UpdateCurrentDay();
          }
        }
        if (iCurrentHour == 12 or bEndPeriode == true)
        {
          bDayOK    = true;
          // Init du texte du jour précédent
          strcpy(  sSaveDateFile   , sCurrentDateFile);
          // Init du texte du nouveau jour
          sprintf( sCurrentDateFile, "%02d/%02d/%04d", day(), month(), year());
          // Save current day
          for (int b=0; b<MAXBD; b++)
            if (LstAnalysers[b] != NULL)
              LstAnalysers[b]->StoreCurrentDay();
        }
      }
    }
  }
  // Test WatchDog
  iWatchDogITTimer++;
  if (iWatchDogITTimer > 100)
  {
    // Le traitement de fond ne fonctionne plus ! RESET
    LogFile::AddLog( LLOG, "Watchdog in RhinoLogger mode ! Reset...");
    WRITE_RESTART(0x5FA0004);
  }
}

//----------------------------------------------------------------------------
// Récupération des infos de la minute courante d'un bande
void CRecorderRL::GetCurentMinute( 
  int iBand,              // Rang de la bande
  ResultAnalyse *pResult  // Pointeur à renseigner
  )
{
  if (LstAnalysers[iBand] != NULL)
    *pResult = *(LstAnalysers[iBand]->GetCurrentMinute());
  else
    memset( pResult, 0, sizeof(ResultAnalyse));
}

//-------------------------------------------------------------------------
// Traitements dans la boucle principale du programme
void CRecorderRL::OnLoop()
{
  // On teste une éventuelle erreur DMA
  uint32_t DMAErr = acquisition.GetDMAError();
  if (DMAErr > 0)
    OnErreurDMA( DMAErr);
  else
  {
    SaveCSVFiles();
#if defined(__IMXRT1062__) // Teensy 4.1
    __disable_irq();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
    unsigned char sreg_backup = SREG;     // Sauvegarde de l'état des interruptions
    cli();                                // On stoppe les interruptions
#endif
    // RAZ des Watchdogs pour surveiller le fonctionnement (RAZ dans le loop, incrément dans les IT, si trop grand alors RESET)
    iWatchDogITTimer = 0;
    iWatchDogITDMA   = 0;
#if defined(__IMXRT1062__) // Teensy 4.1
    __enable_irq();
#endif
#if defined(__MK66FX1M0__) // Teensy 3.6
    SREG = sreg_backup;                   // On retaure les interruptions
#endif
  }
}

//----------------------------------------------------------------------------
// Sauvegarde éventuelle des résultats dans les fichiers CSV
void CRecorderRL::SaveCSVFiles()
{
  // Table des sauvegardes
  ResultAnalyse tbSave[MAXBD];
  // Tets si une sauvegarde est à faire
  if (bMinuteOK)
  {
    // Récupération des infos à sauvegarder
    for (int b=0; b<MAXBD; b++)
      if (LstAnalysers[b] != NULL)
        tbSave[b] = *(LstAnalysers[b]->GetSavedMinute());
      else
        memset( &(tbSave[b]), 0, sizeof(ResultAnalyse));
    // Sauvegarde des résultats dans le fichier des minutes
    MemoSD( (char *)FileNameMN, tbSave, &THBMinute, sTimeSaveMinute, sSaveDateFile, false);
    bMinuteOK = false;
  }
  if (bHourOK)
  {
    // Récupération des infos à sauvegarder
    for (int b=0; b<MAXBD; b++)
      if (LstAnalysers[b] != NULL)
        tbSave[b] = *(LstAnalysers[b]->GetSavedHour());
      else
        memset( &(tbSave[b]), 0, sizeof(ResultAnalyse));
    // Sauvegarde des résultats dans le fichier des heures
    MemoSD( (char *)FileNameH, tbSave, &THBHour, sTimeSaveHour, sSaveDateFile, false);
    bHourOK = false;
  }
  if (bDayOK)
  {
    // Récupération des infos à sauvegarder
    for (int b=0; b<MAXBD; b++)
      if (LstAnalysers[b] != NULL)
        tbSave[b] = *(LstAnalysers[b]->GetSavedDay());
      else
        memset( &(tbSave[b]), 0, sizeof(ResultAnalyse));
    // Sauvegarde des résultats dans le fichier des jours
    MemoSD( (char *)FileNameJ, tbSave, &THBDay, sTimeSaveDay, sSaveDateFile, true);
    bDayOK = false;
    // Init des noms des fichiers de sauvegarde
    sprintf( FileNameJ , "JJ%04d_%s.csv", year(), sSerialName);
    sprintf( FileNameH , "HH%04d%02d%02d_%s.csv", year(), month(), day(), sSerialName);
    sprintf( FileNameMN, "MM%04d%02d%02d_%s.csv", year(), month(), day(), sSerialName);
  }
}

//----------------------------------------------------
// Initialisation de la température, de l'hygrométrie et des tensions batteries dans une structure de mémorisation des activités
void CRecorderRL::SetTHB(
  MemoTempHygroBat *pMemo   // Pointeur sur la structure de mémorisation
  )
{
  pMemo->iTempMin  = (int)cTemp;
  pMemo->iTempMax  = (int)cTemp;
  if (humidity > 5.0)
  {
    pMemo->iHygroMin = (int)humidity;
    pMemo->iHygroMax = (int)humidity;
  }
  else
  {
    pMemo->iHygroMin = 0;
    pMemo->iHygroMax = 0;
  }
  pMemo->fBatInt   = fBatInt;
  pMemo->fBatExt   = fBatExt;
}

//----------------------------------------------------
// Fonction de mémorisation des infos dans un fichier csv
void CRecorderRL::MemoSD(
  char *pFileName,        // Nom du fichier
  ResultAnalyse *data,    // Table des résultats à sauver dans le fichier
  MemoTempHygroBat *pTHB, // Mémo température, hygro et batteries
  char *pHour,            // Heure de début des mesures
  char *pDate,            // Date de début des mesures
  bool bJour              // Indique si c'est un fichier jour
  )
{
  FsFile dataFile;  // Fichier de sauvegarde des infos
  char sTempA[300], sTempB[128];
  int iSizeA = 0;
  int iSizeB = 0;
  unsigned long uiTime = millis();

  // Informe l'opérateur de l'écriture
  digitalWriteFast(LED_BUILTIN, HIGH);

  // Stoppe l'acquisition momentanément
  SetAcquisition( false);
  
  // Test si le fichier existe
  bool bEntete = true;
  if (sd.exists(pFileName))
    bEntete = false;
  // Ouverture du fichier (création s'il n'existe pas)
  dataFile = sd.open(pFileName, FILE_WRITE);
  if (! dataFile)
    LogFile::AddLog( LLOG, "Creation du fichier %s impossible !", pFileName);
  else
  {
    if (bEntete)
    {
      // Entête style RhinoLogger des fichiers minutes
      // Date Heure SFM MFM CFM NFM fFM FFM SGR MGR CHR NGR fGR FGR SRE MRE CRE NRE fRE FRE SEP MEP CEP NEP fEP FEP SPR MPR CPR NPR fPR FPR BRUIT T H B. Ext. B. Int.
      // Entête style RhinoLogger des fichiers heures
      // Date Heure SFM MFM CFM NFM fFM FFM SGR MGR CHR NGR fGR FGR SRE MRE CRE NRE fRE FRE SEP MEP CEP NEP fEP FEP SPR MPR CPR NPR fPR FPR BRUIT T H B. Ext. B. Int.
      // Entête style RhinoLogger des fichiers jours
      // Date Heure SFM MFM CFM NFM fFM FFM SGR MGR CGR NGR fGR FGR SRE MRE CRE NRE fRE FRE SEP MEP CEP NEP fEP FEP SPR MPR CPR NPR fPR FPR BRUIT TMin  TMax  HMin  HMax  B. Ext. B. Int. MinBr MaxBr NbPB  FrMax
      strcpy( sTempA, "Date\tHeure\t");
      // Pour toutes les bandes valides
      for (int b=0; b<MAXBD; b++)
      {
        if (pParamsOp->batBand[b].iType != BDINVALID)
        {
          sprintf( sTempB, "S%s\tM%s\tC%s\tN%s\tf%s\tF%s\t", pParamsOp->batBand[b].sName, pParamsOp->batBand[b].sName, pParamsOp->batBand[b].sName, pParamsOp->batBand[b].sName, pParamsOp->batBand[b].sName, pParamsOp->batBand[b].sName);
          iSizeB = max(iSizeB, (int)strlen(sTempB));
          strcat ( sTempA, sTempB);
        }
      }
      if (!bJour)
        strcat(    sTempA, "BRUIT\tT\tH\tB. Ext.\tB. Int");
      else
        strcat(    sTempA, "BRUIT\tTMin\tTMax\tHMin\tHMax\tB. Ext.\tB. Int.\tMinBr\tMaxBr\tNbPB\tFrMax");
      iSizeB = max(iSizeA, (int)strlen(sTempA));
      dataFile.println(sTempA);
    }

    // Ecriture de la ligne
    // Date, Heure
    sprintf(sTempA, "%s\t%s\t", pDate, pHour);
    dataFile.print( sTempA);
    // Infos des différentes bandes
    float fFMin, fFMax;
    short iNivDB;
    for (int b=0; b<MAXBD; b++)
    {
      if (pParamsOp->batBand[b].iType != BDINVALID)
      {
        if (data[b].iIdxMin == (iFFTLen/2)-1)
          fFMin = 0.0;
        else
          fFMin = GetFreqIdx(data[b].iIdxMin);
        if (data[b].iIdxMax == 0)
          fFMax = 0.0;
        else
          fFMax = GetFreqIdx(data[b].iIdxMax);
        if (data[b].iMagMax == 0)
          iNivDB = -120;
        else
          iNivDB = GetNivDB(data[b].iMagMax) / 10;
        // SGR MGR CHR NGR fGR FGR
        sprintf(sTempA, "%d\t%d\t%d\t%d\t%06.3f\t%06.3f\t", data[b].iNbSec, data[b].iNbMin, data[b].iNbCtx, iNivDB, fFMin, fFMax);
        dataFile.print(sTempA);
        iSizeA = max(iSizeA, (int)strlen(sTempA));
      }
    }
    if (!bJour)
    {
      // Infos complémentaires BRUIT T H B. Ext. B. Int.
      if (bSonde)
        sprintf(sTempA, "%d\t%d\t%d\t%4.1f\t%4.1f", (int)GetMaxNoise(), pTHB->iTempMin, pTHB->iHygroMin, pTHB->fBatExt, pTHB->fBatInt);
      else
        sprintf(sTempA, "%d\t \t \t%4.1f\t%4.1f", (int)GetMaxNoise(), pTHB->fBatInt, pTHB->fBatExt);
    }
    else
    {
      // Infos complémentaires BRUIT TMin  TMax  HMin  HMax  B. Ext. B. Int.
      if (bSonde)
        sprintf(sTempA, "%d\t%d\t%d\t%d\t%d\t%4.1f\t%4.1f", (int)GetMaxNoise(), pTHB->iTempMin, pTHB->iTempMax, pTHB->iHygroMin, pTHB->iHygroMax, pTHB->fBatExt, pTHB->fBatInt);
      else
        sprintf(sTempA, "%d\t \t \t \t \t%4.1f\t%4.1f", (int)GetMaxNoise(), pTHB->fBatExt, pTHB->fBatInt);
    }
    iSizeB = max(iSizeA, (int)strlen(sTempA));
    dataFile.println(sTempA);
    // Vide les buffers et ferme le fichier
    dataFile.flush();
    dataFile.close();
  }
  // Relance l'acquisition 
  SetAcquisition( true);
  
  digitalWriteFast(LED_BUILTIN, LOW);
  if (iSizeA >= 300 or iSizeB >= 128)
    LogFile::AddLog( LLOG, "CRecorderRL::MemoSD Débordement mémoire Taille max string A %d, B %d", iSizeA, iSizeB);
  if (CModeGeneric::bDebug)
    Serial.printf("CRecorderRL::MemoSD en %d ms\n", millis() - uiTime);
}

// Watchdog pour surveiller le fonctionnement (RAZ dans le loop, incrément dans les IT, si trop grand alors RESET)
short   CRecorderRL::iWatchDogITTimer;
short   CRecorderRL::iWatchDogITDMA;

int32_t CRecorderRL::previousSample;
int32_t CRecorderRL::previousFilterOutput;  
