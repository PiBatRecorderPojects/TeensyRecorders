//-------------------------------------------------------------------------
//! \file CSynchroLink.cpp
//! \brief Classe de gestion de la liaison avec l'ESP32 pour les enregistrements stéréo synchro
//! \details class ESP32Link
//! \author Jean-Do. Vrignault
//!  Copyright (c) 2018 Vrignault Jean-Do. All rights reserved.
/*
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
   AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
   OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
   OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "CSynchroLink.h"
#include "ModesModifiers.h"
#if defined(__IMXRT1062__) // Teensy 4.1
#include <wire.h>
#endif

/*
 * CSynchroLink
 *   CMasterLink
 *   CSlaveLink
 */

// Time (ms) between salves to send Slave OK message
/*
 * Temps de transmission (Time On Air) d'un message LORA sur https://www.loratools.nl/#/airtime
 * Paramètres SPF 7 à 12, Band 125kHz, Coding Rate 5, Preambule 8, No CRC, No Explicit header
 * Taille des messages LoRa (octets) et temps de transmission avec les paramètres LoRa (ms) en fonction du Spreading Factor
 *        octets  SF 7   SF 8    SF 9
 * LMSLAVE     7, 30,98  51,71  103,42
 */
#define TIMEBETWEENSLAVES 150
 
//-------------------------------------------------------------------------
//! \class CSynchroLink
//! \brief Classe de gestion de la liaison avec l'ESP32 pour les enregistrements stéréo synchro
//! \brief Gère le cas Master comme les cas Slave n. Gère la liaison série et les liaisons par fils

//-------------------------------------------------------------------------
//! \brief Constructor (initialization of parameters to default values)
CSynchroLink::CSynchroLink()
{
  bDebug = false;
  //bDebug = true;
  if (bDebug) Serial.println("CSynchroLink::CSynchroLink");
  iPRType = 0;
  iESP32Status = ES_OK;
  for (int i=0; i<MAXMS; i++)
    iStatus[i] = MSSINIT;
  iPos = 0;
  // No test link
  iTimeTest = 0;
#if defined(__MK66FX1M0__) // Teensy 3.6
  // To test link
  //iTimeTest = millis() + 1000;
#endif
  bFirst = false;
  bEnd = false;
  iTimeLoop = millis();
  iTimePrintLoop = millis()+5000;
  iTimeOut = 0;
  iTimeSecondmessage = 0;
#if defined(__IMXRT1062__) // Teensy 4.1
  pLink = this;
#endif
}

//-------------------------------------------------------------------------
//! \brief Destructor
CSynchroLink::~CSynchroLink()
{
#if defined(__MK66FX1M0__) // Teensy 3.6
  Serial2.end();
  digitalWriteFast( PIN23ESPON,  LOW); // EPS32 OFF
#endif
}

//-------------------------------------------------------------------------
//! \brief Set type of the recorder and begin link
//! \param iType Master or Slave n (enum MASTERSLAVES)
//! \param pPar  Parameter to send if Master or to modify if Slave
void CSynchroLink::SetLinkType(
  int iType,
  ParamsOperator *pPar
  )
{
  iPRType = iType;
  pParams = pPar;
  if (bDebug) Serial.printf("CSynchroLink::SetLinkType(%d)\n", iType);
#if defined(__MK66FX1M0__) // Teensy 3.6
  // Set serial link
  Serial2.begin(19200);
  Serial2.clear();
#endif
  iPos = 0;
  bFirst = false;
  bEnd = false;
#if defined(__MK66FX1M0__) // Teensy 3.6
  digitalWriteFast( PIN23ESPON,  HIGH); // EPS32 ON
#endif
  iTimeStatus = millis() + TIMESTATUSESP + TIMESTATUSESP / 2;
#if defined(__IMXRT1062__) // Teensy 4.1
  // Init de la fréquence du top
  iFreqTop = pParams->iTopAudioFreq * 1000;
  // Init de la durée du top
  switch(pParams->iDurTop)
  {
  default:
  case TS256 :  //!<  256 samples
  case TS512 :  //!<  512 samples
    iDurTop = 1;
    break;
  case TS1024:  //!< 1024 samples
    iDurTop = 3;
    break;
  case TS2048:  //!< 2048 samples
    iDurTop = 5;
    break;
  case TS4096:  //!< 4096 samples
    iDurTop = 10;
    break;
  }
#endif
}

//-------------------------------------------------------------------------
//! \brief Reset Slaves status
void CSynchroLink::ResetStatus()
{
  iStatus[MASTER] = MSWAITINGESP32;
  for (int i=SLAVE01; i<MAXMS; i++)
    iStatus[i] = MSSINIT;
}

//-------------------------------------------------------------------------
//! \brief Set status
//! \param iNewStatus New status of the PR Type
void CSynchroLink::SetStatus(
  int iNewStatus
  )
{
  if (bDebug) Serial.printf("CSynchroLink::SetStatus(%d)\n", iNewStatus);
  iStatus[iPRType] = iNewStatus;
}

//-------------------------------------------------------------------------
//! \brief Test link on loop
void CSynchroLink::OnLoop()
{
  //if (bDebug) Serial.println("CSynchroLink::OnLoop");
#if defined(__MK66FX1M0__) // Teensy 3.6
  if (iTimeStatus != 0 and millis() > iTimeStatus)
  {
    if (millis()-iTimeLoop > TIMESTATUSESP)
      Serial.printf("Appel loop pas assez rapide ! %dms\n", millis()-iTimeLoop);
    else
    {
      // Timeout on ESP32 status message
      if (iESP32Status != ES_TIMEOUT)
        LogFile::AddLog( LLOG, "ESP32 Error time out !");
      iESP32Status = ES_TIMEOUT;
    }
  }
  if (iTimeTest != 0 and millis() > iTimeTest)
  {
    char *pMessParams = link.CodeTest();
    Serial.printf("CSlaveLink::OnLoop send Test [%s]\n", pMessParams);
    Serial2.println( pMessParams);
    iTimeTest = millis() + 1000;
  }
  if (iTimeSecondmessage != 0 and millis() > iTimeSecondmessage)
  {
    char *pMessStopMode = link.CodeStopMode();
    if (bDebug) Serial.printf("CSynchroLink::StopMode second message [%s]\n", pMessStopMode);
    Serial2.println( pMessStopMode);
    iTimeSecondmessage = 0;
  }
#endif
  /*iTimeLoop = millis();
  if (bDebug and millis() > iTimePrintLoop)
  {
    Serial.println("CSynchroLink::OnLoop");
    iTimePrintLoop = millis()+5000;
  }*/
}

//-------------------------------------------------------------------------
//! \brief Send Stop Mode
void CSynchroLink::StopMode()
{
  char *pMessStopMode = link.CodeStopMode();
  if (bDebug) Serial.printf("CSynchroLink::StopMode [%s]\n", pMessStopMode);
#if defined(__MK66FX1M0__) // Teensy 3.6
  Serial2.println( pMessStopMode);
  // To send a second message
  iTimeSecondmessage = millis() + 200;
  LogFile::AddLog( LLOG, "Synchro mode, Send Stop mode");
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  // With I2C, send a message for every slave
  for (int i=1; i<MAXMS; i++)
  {
    if (bSlaveOK[i])
    {
      Wire2.beginTransmission( I2CSLAVEADR+i);
      Wire2.write(pMessStopMode);
      Wire2.endTransmission();
    }
    bSlaveOK[i] = false;
  }
#endif
}

//-------------------------------------------------------------------------
//! \brief Requesting the software version of the ESP32
void CSynchroLink::VersionRequest()
{
  iStatus[iPRType] = MSWAITINGVERS;
  char *pMessVersion = link.CodeVersionRequest();
  if (bDebug) Serial.printf("CMasterLink::VersionRequest [%s]\n", pMessVersion);
#if defined(__MK66FX1M0__) // Teensy 3.6
  Serial2.println( pMessVersion);
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  // With I2C, send a message for every slave
  for (int i=1; i<MAXMS; i++)
  {
    if (bSlaveOK[i])
    {
      Wire2.beginTransmission( I2CSLAVEADR+i);
      Wire2.write(pMessVersion);
      Wire2.endTransmission();
    }
  }
#endif
}
  
//-------------------------------------------------------------------------
//! \brief Processing message receptions
//! \return Returns the type of the received message (LMUNKNOW if not)
int CSynchroLink::OnReceive()
{
  int iReturn = LMUNKNOW;
  //Serial.println("CSynchroLink::OnReceive");
#if defined(__MK66FX1M0__) // Teensy 3.6
  while (Serial2.available())
  {
    //Serial.println("CSynchroLink::OnReceive read");
    char cCar = (char)Serial2.read();
    if (cCar == '!' and bFirst)
    {
      // End of a message
      bEnd = true;
      sMessageReception[iPos]   = cCar;
      sMessageReception[iPos+1] = 0;
      break;
    }
    else if (cCar == '$')
    {
      bFirst = true;
      iPos   = 0;
    }
    //Serial.printf("Serial %c, iPos %d, bFirst %d, bEnd %d\n", cCar, iPos, bFirst, bEnd);
    if (bFirst)
    {
      sMessageReception[iPos] = cCar;
      iPos++;
      if (iPos > 120)
      {
        // RAZ message en cours
        Serial2.clear();
        iPos   = 0;
        bFirst = false;
        bEnd   = false;
        break;
      }
    }
  }
#endif
  if (bEnd)
  {
    //Serial.println(" ");
    if (bDebug) Serial.printf("CSynchroLink::OnReceive [%s], iPos %d\n", sMessageReception, iPos);
    // Decoding the message
    iReturn = link.DecodeMessageType( sMessageReception);
    // Préparation prochain message
    iPos   = 0;
    bFirst = false;
    bEnd   = false;
  }
  return iReturn;
}

//-------------------------------------------------------------------------
//! \brief Processing message receptions
//! \param iMessage the type of the received message (LinkMessages)
void CSynchroLink::MessageProcessing(
  int iMessage
  )
{
  int iNewESP32Status;
  char *pVersion;
  bool bMaster = false;
  switch (iMessage)
  {
  case LMTEST:      // Test
    Serial.println("CSynchroLink::MessageProcessing Test message");
    break;
  case LMSVERSION: // ESP32 Software version
    pVersion = link.DecodeVersion( sMessageReception);
    if (strlen(pVersion) > 10) pVersion[9] = 0;
    strcpy(sESP32Version, pVersion);
    Serial.printf("CSynchroLink::MessageProcessing ESP32 Software version: %s\n", sESP32Version);
    break;
  case LMSTATUS:
    if (iPRType == MASTER)
      bMaster = true;
    iNewESP32Status = link.DecodeESP32Status( bMaster, sMessageReception);
    if (iNewESP32Status > ES_OK and iNewESP32Status != iESP32Status)
    {
      switch(iNewESP32Status)
      {
      case ES_ERLORA   : LogFile::AddLog( LLOG, "ESP32 Error on LORA !"); break;
      case ES_ERTOP    : LogFile::AddLog( LLOG, "ESP32 Error on DAC Top !"); break;
      case ES_ERPARAM  : LogFile::AddLog( LLOG, "ESP32 Error decoding params message !"); break;
      case ES_ERSLAVE  : LogFile::AddLog( LLOG, "ESP32 Error decoding Slave message !"); break;
      case ES_ERCRC    : LogFile::AddLog( LLOG, "ESP32 Error CRC !"); break;
      case ES_ERSERMESS: LogFile::AddLog( LLOG, "ESP32 Error decoding num message Serial Link !"); break;
      case ES_ERLORMESS: LogFile::AddLog( LLOG, "ESP32 Error decoding num message Lora Link !"); break;
      case ES_TIMEOUT  : LogFile::AddLog( LLOG, "ESP32 Error time out !"); break;
      case ES_MSSTATUS : LogFile::AddLog( LLOG, "ESP32 Inconsistency on the master or slave state !"); break;
      default          :
      case ES_UNKNOWER : LogFile::AddLog( LLOG, "ESP32 Unknow error %d! [%s]", iNewESP32Status, sMessageReception); break;
      }
    }
    iESP32Status = iNewESP32Status;
    iTimeStatus = millis() + TIMESTATUSESP + TIMESTATUSESP / 2;
    break;
  }
}

#if defined(__IMXRT1062__) // Teensy 4.1
//-------------------------------------------------------------------------
//! \brief Start record
void CSynchroLink::StartRecord() {;};

//-------------------------------------------------------------------------
//! \brief StopRecord
void CSynchroLink::StopRecord() {;};

//-------------------------------------------------------------------------
//! \brief Send Top Synchro to each slave
void CSynchroLink::SendTopSynchro()
{
  char *pMessTop = link.CodeTopSynchro();
  for (int i=1; i<MAXMS; i++)
  {
    if (bSlaveOK[i])
    {
      // Esclave présent, émission start rec
      //Serial.printf("Top Es %d [%s] %d char\n", i, pMessTop, strlen(pMessTop));
      Wire2.beginTransmission( I2CSLAVEADR+i);
      Wire2.write(pMessTop);
      Wire2.endTransmission();
    }
  }
}

//-------------------------------------------------------------------------
//! \brief On Timer Top synchro
void CSynchroLink::OnTimer()
{
  //Serial.println("TopSynchro");
  pLink->SendTopSynchro();
  // Génération du top sur l'enregistrement du Master
  tone(PIN_TOP_T41, iFreqTop, iDurTop);
}
#endif

//! false after switch on, indicate if config is validate by operator
bool CSynchroLink::bValidConfig = false;

// ESP32 software version
char CSynchroLink::sESP32Version[10];

//! Link manager
CCodeLink CSynchroLink::link;

#if defined(__IMXRT1062__) // Teensy 4.1
//! Durée du top synchro
int CSynchroLink::iDurTop = 1;

//! Fréquence du top synchro
int CSynchroLink::iFreqTop = 1;

//! Pointeur sur l'instance pour les fonctions statiques
CSynchroLink *CSynchroLink::pLink = NULL;
#endif

//-------------------------------------------------------------------------
//! \class CMasterLink
//! \brief Gère le cas Master

//-------------------------------------------------------------------------
//! \brief Constructor (initialization of parameters to default values)
CMasterLink::CMasterLink():CSynchroLink()
{
}

//-------------------------------------------------------------------------
//! \brief Destructor
CMasterLink::~CMasterLink()
{
  
}

//-------------------------------------------------------------------------
//! \brief Set type of the recorder and begin link
//! \param iType Master or Slave n (enum MASTERSLAVES)
//! \param pPar  Parameter to send if Master or to modify if Slave
void CMasterLink::SetLinkType(
  int iType,
  ParamsOperator *pPar
  )
{
  // Set ESP32 Master
#if defined(__MK66FX1M0__) // Teensy 3.6
  digitalWriteFast( PIN04MASTER, HIGH); // Master
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  // Open I2C Master link
  Wire2.begin();
#endif
  // Call basic function (ESP32 On)
  CSynchroLink::SetLinkType( iType, pPar);
  // Wait for ESP32 ready and prepare timeout 3s
  iStatus[iPRType] = MSWAITINGESP32;
  iTimeOut = millis() + 3000;
#if defined(__IMXRT1062__) // Teensy 4.1
  // Par défaut, en mode I2C, le maître est en attente des esclaves
  iStatus[iPRType] = MSWAITINGSLAVES;
  // Test de la présence des liaisons I2C esclaves
  for (int i=1; i<MAXMS; i++)
  {
    bSlaveOK[i] = false;
    Wire2.beginTransmission( I2CSLAVEADR+i);
    byte error = Wire2.endTransmission();
    if (error == 0)
    {
      Serial.printf("Esclave %d présent\n", i);
      // Esclave présent
      bSlaveOK[i] = true;
      iStatus [i] = MSWAITINGSLAVES;
      // Emission des paramètres vers l'esclave
      char *pMessParams = link.CodeParams(pParams);
      Serial.printf("Params vers Es %d [%s] %d char\n", i, pMessParams, strlen(pMessParams));
      Wire2.beginTransmission( I2CSLAVEADR+i);
      Wire2.write(pMessParams);
      Wire2.endTransmission();
      // Attente réponse de l'esclave (message LMSLAVE 12 octets)
      char sReceived[32];
      Wire2.requestFrom((int)I2CSLAVEADR+i, (int)12);
      int j = 0;
      while(Wire2.available())
      {
        sReceived[j] = Wire2.read();
        j++;
        sReceived[j] = 0;
      }
      // Décodage de la réponse de l'esclave
      int  iSlave = 0;
      bool bSlave = false;
      Serial.printf("Réception [%s] %d char\n", sReceived, strlen(sReceived));
      if (link.DecodeMessageType( sReceived) == LMSLAVE)
        link.DecodeSlave( &iSlave, &bSlave, sReceived);
      if (iSlave == i and bSlave)
        iStatus[i] = MSSLAVEOK;
      else
        iStatus[i] = MSSLAVEERROR;
    }
  }
#endif
}

//-------------------------------------------------------------------------
//! \brief Set valid configuration by operator
void CMasterLink::SetValidConfig()
{
  for (int i=SLAVE01; i<MAXMS; i++)
    if (iStatus[i] ==  MSSLAVEOK)
      bConfig[i] = true;
  bValidConfig = true;
}

#if defined(__MK66FX1M0__) // Teensy 3.6
unsigned long iDiff;
#endif

//-------------------------------------------------------------------------
//! \brief Test link on loop
void CMasterLink::OnLoop()
{
#if defined(__MK66FX1M0__) // Teensy 3.6
  //Serial.printf("CMasterLink::OnLoop, iPRType %d, iStatus %d\n", iPRType, iStatus[iPRType]);
  if (iStatus[iPRType] == MSWAITINGESP32)
  {
    //Serial.println("CMasterLink::OnLoop");
    if (digitalReadFast(PIN22ESPREADY) == LOW)
    {
      Serial.println("Détection ESP32 ON OK");
      iTimeStatus = 0;
      // Wait 1s
      iStatus[iPRType] = MSWAITEESP32;
      iTimeOut = millis() + 1000;
      iDiff = millis();
      if (bDebug) Serial.printf("Attente 1s %d\n", iDiff);
    }
    else if (iTimeOut != 0 and millis() > iTimeOut)
    {
      // Timeout waiting ESP32 on
      iStatus[iPRType] = MSTIMEOUTESP32;
      iTimeOut = 0;
    }
  }
  else if (iStatus[iPRType] == MSWAITEESP32 and iTimeOut != 0 and millis() > iTimeOut)
  {
    iDiff = millis() - iDiff;
    if (bDebug)
      Serial.printf("Attente 300ms OK %d, diff %dms\n", millis(), iDiff);
    //Serial.printf("Emission params iLEDSynchro %d\n", pParams->iLEDSynchro);
    // Send parameters for all Slaves and prepare timeout for 3 minutes
    char *pMessParams = link.CodeParams(pParams);
    if (bDebug) Serial.printf("CMasterLink::OnLoop ESP32 Ready, send params [%s]\n", pMessParams);
    Serial2.println( pMessParams);
    iStatus[iPRType] = MSWAITINGSLAVES;
    // Time out waiting slaves and operator (3mn)
    iTimeOut = millis() + 60*3*1000;
    // Time to send a second message
    iTimeSecondmessage = millis() + 10 * TIMEBETWEENSLAVES;
    LogFile::AddLog( LLOG, "Synchro mode, Send parameters [%s]", pMessParams);
  }
  else if (iStatus[iPRType] == MSWAITINGSLAVES)
  {
    // Waiting slaves messages
    int iMessage = OnReceive();
    if (iMessage != LMUNKNOW)
      MessageProcessing( iMessage);
    else if (iTimeOut != 0 and millis() > iTimeOut)
    {
      // Timeout waiting slaves
      iStatus[iPRType] = MSTIMEOUTSLAVES;
      iTimeOut = 0;
    }
    else if (iTimeSecondmessage != 0 and millis() > iTimeSecondmessage)
    {
      // Send second params message
      iTimeSecondmessage = 0;
      char *pMessParams = link.CodeParams(pParams);
      if (bDebug) Serial.printf("CMasterLink::OnLoop ESP32 Ready, send second params [%s]\n", pMessParams);
      Serial2.println( pMessParams);
    }
    // Test si configuration complète
    if (bValidConfig)
    {
      bool bOK = true;
      for (int i=SLAVE01; i<MAXMS; i++)
        // Si l'esclave fait parti de la config et qu'il n'est pas OK, alors config non complète
        if (bConfig[i] and iStatus[i] != MSSLAVEOK)
          bOK = false;
      if (bOK)
        // Master OK
        iStatus[iPRType] = MSMASTEROK;
    }
  }
  else
  {
    int iMessage = OnReceive();
    if (iMessage != LMUNKNOW)
      MessageProcessing( iMessage);
  }
  CSynchroLink::OnLoop();
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  // To do
#endif
}

//-------------------------------------------------------------------------
//! \brief Processing message receptions
//! \param iMessage the type of the received message (LinkMessages)
void CMasterLink::MessageProcessing(
  int iMessage
  )
{
#if defined(__MK66FX1M0__) // Teensy 3.6
  int iSlave;
  bool bOK;
  switch (iMessage)
  {
  case LMSLAVE :  // Slave OK
    iSlave = -1;
    if (link.DecodeSlave( &iSlave, &bOK, sMessageReception))
    {
      if (bOK and iSlave > -1)
        iStatus[iSlave] = MSSLAVEOK;
      else if (iStatus[iSlave] != MSSLAVEOK)
        iStatus[iSlave] = MSSLAVEERROR;
    }
    else if (iSlave > -1 and iStatus[iSlave] != MSSLAVEOK)
      iStatus[iSlave] = MSSLAVEERROR;
    LogFile::AddLog( LLOG, "Synchro mode, Receiving the status of a slave [%s]", sMessageReception);
    break;
  default      :
    CSynchroLink::MessageProcessing( iMessage);
    break;
  }
#endif
}

#if defined(__IMXRT1062__) // Teensy 4.1
//-------------------------------------------------------------------------
//! \brief Start record
void CMasterLink::StartRecord()
{
  for (int i=1; i<MAXMS; i++)
  {
    if (iStatus[i] >= MSSLAVEOK)
    {
      char *pMess = link.CodeStartRec();
      Serial.printf("StartRecord Es %d [%s] %d char\n", i, pMess, strlen(pMess));
      Wire2.beginTransmission( I2CSLAVEADR+i);
      Wire2.write(pMess);
      Wire2.endTransmission();
    }
  }
  // Start top timer
  if (pParams->iTopPer == 0)
    pParams->iTopPer = 1;
  //Serial.printf("Start Timer %dµs\n", pParams->iTopPer*1000000);
  timerTop.begin(CMasterLink::OnTimer, pParams->iTopPer*1000000); 
}

//-------------------------------------------------------------------------
//! \brief StopRecord
void CMasterLink::StopRecord()
{
  // Stop top timer
  timerTop.end(); 
  for (int i=1; i<MAXMS; i++)
  {
    if (iStatus[i] >= MSSLAVEOK)
    {
      char *pMess = link.CodeStopRec();
      Serial.printf("StopRecord Es %d [%s] %d char\n", i, pMess, strlen(pMess));
      Wire2.beginTransmission( I2CSLAVEADR+i);
      Wire2.write(pMess);
      Wire2.endTransmission();
    }
  }
}
#endif

//! Esclaves présents en fonctions de la configuration validée
bool CMasterLink::bConfig[MAXMS] = {false, false, false, false, false, false, false, false, false, false};

//-------------------------------------------------------------------------
//! \class CSlaveLink
//! \brief Gère le cas Slave

//-------------------------------------------------------------------------
//! \brief Constructor (initialization of parameters to default values)
CSlaveLink::CSlaveLink():CSynchroLink()
{
  if (bDebug) Serial.println("CSlaveLink::CSlaveLink");
  bParamsOK = false;
#if defined(__IMXRT1062__) // Teensy 4.1
  sSlaveSend[0] = 0;
#endif
}

//-------------------------------------------------------------------------
//! \brief Destructor
CSlaveLink::~CSlaveLink()
{
}

//-------------------------------------------------------------------------
//! \brief Set type of the recorder and begin link
//! \param iType Master or Slave n (enum MASTERSLAVES)
//! \param pPar  Parameter to send if Master or to modify if Slave
void CSlaveLink::SetLinkType(
  int iType,
  ParamsOperator *pPar
  )
{
  if (bDebug) Serial.printf("CSlaveLink::SetLinkType(%d)\n", iType);
  // Set ESP32 Salve
#if defined(__MK66FX1M0__) // Teensy 3.6
  digitalWriteFast( PIN04MASTER, LOW); // Slave
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  // Open I2C Slave link
  Wire2.begin(I2CSLAVEADR+pPar->iMasterSlave);
  Wire2.onReceive(CSlaveLink::i2cReceive);
  Wire2.onRequest(CSlaveLink::i2cRequest);
#endif
  // Call basic function (ESP32 On)
  CSynchroLink::SetLinkType( iType, pPar);
#if defined(__MK66FX1M0__) // Teensy 3.6
  // Wait for ESP32 ready and prepare timeout 2s
  iStatus[iPRType] = MSWAITINGESP32;
  iTimeOut = millis() + 2000;
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  // Par défaut, en mode I2C, les esclaves sont en attente de paramètres
  iStatus[iPRType] = MSWAITINGPARAMS;
#endif
}

//-------------------------------------------------------------------------
//! \brief Test link on loop
void CSlaveLink::OnLoop()
{
  //if (bDebug) Serial.println("CSlaveLink::OnLoop");
#if defined(__MK66FX1M0__) // Teensy 3.6
  if (iStatus[iPRType] == MSWAITINGESP32)
  {
    if (digitalReadFast(PIN22ESPREADY) == LOW)
    {
      if (bDebug) Serial.println("CSlaveLink::OnLoop ESP32 ready");
      iTimeStatus = 0;
      // Waiting parameters, no timeout
      iStatus[iPRType] = MSWAITINGPARAMS;
      iTimeOut = 0;
    }
    else if (iTimeOut != 0 and millis() > iTimeOut)
    {
      if (bDebug) Serial.println("CSlaveLink::OnLoop Timeout waiting ESP32");
      // Timeout waiting ESP32
      iStatus[iPRType] = MSTIMEOUTESP32;
      iTimeOut = 0;
    }
  }
  else if (iStatus[iPRType] == MSSLAVESTOP and iTimeOut != 0 and millis() > iTimeOut)
  {
    // Return to init waiting parameters from master
    iStatus[iPRType] = MSWAITINGPARAMS;
    iTimeOut = 0;
    bParamsOK = false;
  }
  else if (iStatus[iPRType] == MSWAITINGSEND and iTimeOut != 0 and millis() > iTimeOut)
  {
    char *pMessSlaveOK = link.CodeSlave( iPRType, bParamsOK);
    if (bDebug) Serial.printf("CSlaveLink::OnLoop Send Slave OK [%s]\n", pMessSlaveOK);
    // Send Slave OK
    Serial2.println( pMessSlaveOK);
    if (bParamsOK)
      iStatus[iPRType] = MSSLAVEOK;
    else
      iStatus[iPRType] = MSSLAVEERROR;
    iTimeOut = 0;
    LogFile::AddLog( LLOG, "Synchro mode, Sending the status of a slave [%s]", pMessSlaveOK);
  }
  else
  {
    // Slave waiting message
    int iMessage = OnReceive();
    if (iMessage != LMUNKNOW)
      MessageProcessing( iMessage);
  }
#endif
  CSynchroLink::OnLoop();
  //if (bDebug) Serial.println("CSlaveLink::OnLoop OK");
}

//-------------------------------------------------------------------------
//! \brief Processing message receptions
//! \param iMessage the type of the received message (LinkMessages)
void CSlaveLink::MessageProcessing(
  int iMessage
  )
{
  if (bDebug) Serial.println("CSlaveLink::MessageProcessing");
#if defined(__MK66FX1M0__) // Teensy 3.6
  switch (iMessage)
  {
  case LMPARAMS:  // Receive parameters
    if (bDebug) Serial.printf("CSlaveLink::MessageProcessing LMPARAMS [%s]\n", sMessageReception);
    if (link.DecodeParams( pParams, sMessageReception))
      bParamsOK = true;
    else
      Serial.print(link.GetErrorString());
    Serial.printf("Réception params iLEDSynchro %d\n", pParams->iLEDSynchro);
    // Timeout to send SLAVEOK (250ms * nSlave, one message every 250ms for 9 slaves maximum = 2.5s)
    iStatus[iPRType] = MSWAITINGSEND;
    iTimeOut = millis() + TIMEBETWEENSLAVES*iPRType;
    LogFile::AddLog( LLOG, "Synchro mode, Receiving parameters [%s]", sMessageReception);
    break;
  case LMSTOPMODE:  // Stop mode
    if (bDebug) Serial.println("CSlaveLink::MessageProcessing MSSLAVESTOP");
    iStatus[iPRType] = MSSLAVESTOP;
    iTimeOut = millis() + 1000;
    LogFile::AddLog( LLOG, "Synchro mode, Receiving Stop mode");
    break;
  default      :
    if (bDebug)
      Serial.printf("CSlaveLink::MessageProcessing = %d\n", iMessage);
    CSynchroLink::MessageProcessing( iMessage);
    break;
  }
#endif
#if defined(__IMXRT1062__) // Teensy 4.1
  int iDur = 0;
  char *pMessSlaveOK;
  switch (iMessage)
  {
  case LMTOP:     // Top synchro
    if (iPRType == 1)
      delayMicroseconds(383);
    // Génération du top sur l'enregistrement
    tone(PIN_TOP_T41, pParams->iTopAudioFreq*1000, iDurTop);
    break;
  case LMPARAMS:  // Receive parameters
    //if (bDebug) Serial.printf("CSlaveLink::MessageProcessing LMPARAMS [%s]\n", sMessageReception);
    if (link.DecodeParams( pParams, sMessageReception))
      bParamsOK = true;
    else
      Serial.print(link.GetErrorString());
    // Préparation message slave OK
    pMessSlaveOK = link.CodeSlave( iPRType, bParamsOK);
    strcpy( sSlaveSend, pMessSlaveOK); 
    //LogFile::AddLog( LLOG, "Synchro mode, Receiving parameters [%s]", sMessageReception);
    iStatus[iPRType] = MSSLAVEOK;
    // Init de la fréquence du top
    iFreqTop = pParams->iTopAudioFreq * 1000;
    // Init de la durée du top
    switch(pParams->iDurTop)
    {
    default:
    case TS256 :  //!<  256 samples
    case TS512 :  //!<  512 samples
      iDurTop = 1;
      break;
    case TS1024:  //!< 1024 samples
      iDurTop = 3;
      break;
    case TS2048:  //!< 2048 samples
      iDurTop = 5;
      break;
    case TS4096:  //!< 4096 samples
      iDurTop = 10;
      break;
    }
    break;
  case LMSTOPMODE:  // Stop mode
    if (bDebug) Serial.println("CSlaveLink::MessageProcessing MSSLAVESTOP");
    iStatus[iPRType] = MSSLAVESTOP;
    //LogFile::AddLog( LLOG, "Synchro mode, Receiving Stop mode");
    break;
  case LMSTARTREC:  // Start Record
    if (bDebug) Serial.println("CSlaveLink::MessageProcessing LMSTARTREC");
    iStatus[iPRType] = MSSLAVEWAITREC;
    //LogFile::AddLog( LLOG, "Synchro mode, Receiving Start Record");
    break;
  case LMSTOPREC:  // Stop Record
    if (bDebug) Serial.println("CSlaveLink::MessageProcessing LMSTOPREC");
    iStatus[iPRType] = MSSLAVEWAITSREC;
    //LogFile::AddLog( LLOG, "Synchro mode, Receiving Stop Record");
    break;
  default      :
    if (bDebug) Serial.printf("CSlaveLink::MessageProcessing = %d [%s] (%d char)\n", iMessage, sMessageReception, strlen(sMessageReception));
    CSynchroLink::MessageProcessing( iMessage);
    break;
  }
#endif
}

#if defined(__IMXRT1062__) // Teensy 4.1
//----------------------------------------------------------------------------
//! \brief Fonction de réception des octets de la liaison I2C esclave
//! \param bytesReceived Nombre d'octets à recevoir
void CSlaveLink::i2cReceive(int bytesReceived)
{
  if (pLink->bDebug) Serial.printf("CSlaveLink::i2cReceive(%d) ", bytesReceived);
  char *pBuff = pLink->GetReceptionBuffer();
  int i = 0;
  while(Wire2.available())  // Read Any Received Data
  {
    char c = Wire2.read();
    if (pLink->bDebug) Serial.print(c);
    if (c == '$')
      idxMes = 0;
    if (idxMes >= 0)
    {
      pBuff[idxMes] = c;
      idxMes++;
      pBuff[idxMes] = 0;
      if (c == '!')
      {
        // Fin de réception d'un message
        if (pLink->bDebug) Serial.println(" ");
        idxMes = -1;
        pLink->MessageProcessing( link.DecodeMessageType( pBuff));
      }
    }
  }
  if (pLink->bDebug) Serial.println(" ");
}

//----------------------------------------------------------------------------
//! \brief Fonction sollicitée pour l'émission de la liaison I2C. Un esclave ne peut émettre que s'il est sollicité par le maître
void CSlaveLink::i2cRequest()
{
  if (pLink->bDebug) Serial.println("CSlaveLink::i2cRequest");
  if (strlen(sSlaveSend) > 0)
    Wire2.write(sSlaveSend);
  else
    Serial.println("CSlaveLink::i2cRequest rien à émettre !");
  sSlaveSend[0] = 0;
}

//! Message à émettre
char CSlaveLink::sSlaveSend[127];

//! Indice réception message
int CSlaveLink::idxMes = -1;
#endif

