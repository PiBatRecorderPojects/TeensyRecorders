/*!
 * @file MAX17043.h
 *
 * @copyright   Copyright (c) 2010 DFRobot Co.Ltd (http://www.dfrobot.com)
 * @license     The MIT License (MIT)
 * @author [ouki.wang](ouki.wang@dfrobot.com)
 * @version  V1.1 (Modifier by Jean-Do for chinese clone of MAX17043)
 * @date  2018-4-14
 * @url https://github.com/DFRobot/MAX17043
 */

#ifndef __MAX17043_H
#define __MAX17043_H

#include "Arduino.h"
#include "Wire.h"

#define MAX17043_ADDRESS_A      0x36  // Véritable MAX17043
#define MAX17043_ADDRESS_B      0x32  // Clone chinois

#define MAX17043_VCELL          0x02
#define MAX17043_SOC            0x04
#define MAX17043_MODE           0x06
#define MAX17043_VERSION        0x08
#define MAX17043_CONFIG         0x0c
#define MAX17043_COMMAND        0xfe

class MAX17043 {
public:
  /**
   * @fn MAX17043
   * @brief create MAX17043 object
   * @return MAX17043 object
   */
  MAX17043();
  /**
   * @fn begin
   * @brief MAX17043 begin and test moudle
   *
   * @return initialization result
   * @retval  0     successful
   * @retval -1     faild
   */
  int         begin();
  /**
   * @fn readVoltage
   * @brief read battery voltage in V
   * @return voltage in V
   */
  float       readVoltage();
  /**
   * @fn readPercentage
   * @brief read battery remaining capacity in percentage
   *
   * @return battery remaining capacity in percentage
   */
  float       readPercentage();
  /**
   * @fn setInterrupt
   * @brief set MAX17043 interrput threshold
   *
   * @param per       interrupt threshold as %1 - 32% (integer)
   */
  void        setSleep();
  /**
   * @fn setWakeUp
   * @brief weak up MAX17043
   *
   */
  void        setWakeUp();
  /**
   * @fn set mode Quick Start
   * @brief start MAX17043. Attention, with the Chinese model, you must read the voltage and the percentage after 125ms and before 400ms after a Quick Start order
   *
   */
  void        setPowerOn();
  /**
   * @fn Indicates whether a MAX17043 is present or not
   * @brief Return true if MAX17043 is here
   *
   */
  bool isOK() {return bOK;};
  /**
   * @fn Return module version
   * @brief Return version
   *
   */
  uint16_t getVersion();

  private:
    void write16(uint8_t reg, uint16_t dat) {
      Wire.begin();
      Wire.beginTransmission(address);
      Wire.write(reg);
      Wire.write(dat >> 8);
      Wire.write(dat);
      Wire.endTransmission();
    }

    uint16_t read16(uint8_t reg) {
      uint16_t        temp;
      Wire.begin();
      Wire.beginTransmission(address);
      Wire.write(reg);
      Wire.endTransmission();
      Wire.requestFrom(address, (uint8_t)2);
      temp = (uint16_t)Wire.read() << 8;
      temp |= (uint16_t)Wire.read();
      Wire.endTransmission();
      return temp;
    }

    void writeRegBits(uint8_t reg, uint16_t dat, uint16_t bits, uint8_t offset) {
      uint16_t        temp;
      temp = read16(reg);
      temp = (temp & (~(bits << offset))) | (dat << offset);
      write16(reg, temp);
    }

    byte address;

    bool bOK;
};

#endif
